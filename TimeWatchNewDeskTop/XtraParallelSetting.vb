﻿Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Data.SqlClient
Imports System.IO
Imports System.Resources
Imports System.Globalization
Imports MySql.Data.MySqlClient
Imports Oracle.DataAccess.Client

Public Class XtraParallelSetting   
    Dim ulf As UserLookAndFeel
    Dim ds As DataSet = New DataSet

    Dim ConnectionString As String = ""
    Dim DBName As String
    Dim DBType As String
    Dim AuthMode As String
    Dim ServerName As String
    Dim UserName As String
    Dim Pwd As String
    Dim TblName As String
    Dim PreCardNo As String
    Dim EmpName As String
    Dim PreIsPrifix As String
    Dim PreLength As String
    Dim PreText As String
    Dim Paycode As String
    Dim PayIsPrefix As String
    Dim PayLength As String
    Dim PayText As String
    Dim PunchDate As String
    Dim PunchDateFormat As String
    Dim PunchTime As String
    Dim PunchTimeFormat As String
    Dim PunchDateTime As String
    Dim PunchDateTimeFormat As String
    Dim DeviceId As String
    Dim PunchDirection As String
    Dim InValue As String
    Dim OutVaue As String
    Dim IsManual As String
    Dim YesValue As String
    Dim NoValue As String
    Dim Rfield1 As String
    Dim Rfield1Value As String
    Dim Rfield2 As String
    Dim Rfield2Value As String
    Dim Rfield3 As String
    Dim Rfield3Value As String


    Private Sub XtraDBBackUpSetting_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100
        'Common.res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraCompany).Assembly)
        'Common.cul = CultureInfo.CreateSpecificCulture("en")
        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraDB).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")
        Dim sSql = "select * from ParallelDB"
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            setDefault()
        Else
            setValue(ds)
        End If
        DateEditFrmDate.DateTime = Now
    End Sub
    Private Sub ToggleParallel_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleParallel.Toggled
        If ToggleParallel.IsOn = False Then
            PanelControl3.Enabled = False
            GroupControl1.Enabled = False
            btnTest.Enabled = False
        Else
            PanelControl3.Enabled = True
            GroupControl1.Enabled = True
            btnTest.Enabled = True
        End If
        If ComboDBType.SelectedIndex = 0 Then
            ComboAuthMode.Enabled = True
            If ComboAuthMode.SelectedIndex = 0 Then
                TextUser.Enabled = False
                TextPwd.Enabled = False
            Else
                TextUser.Enabled = True
                TextPwd.Enabled = True
            End If
        Else
            ComboAuthMode.Enabled = False
            TextUser.Enabled = True
            TextPwd.Enabled = True
        End If
    End Sub
    Private Sub setDefault()
        ToggleParallel.IsOn = False
        If ToggleParallel.IsOn = False Then
            PanelControl3.Enabled = False
            GroupControl1.Enabled = False
            btnTest.Enabled = False
        Else
            PanelControl3.Enabled = True
            GroupControl1.Enabled = True
            btnTest.Enabled = True
        End If
        ComboDBType.SelectedIndex = 0
        ComboAuthMode.Enabled = False
        TextUser.Enabled = True
        TextPwd.Enabled = True

        ComboAuthMode.SelectedIndex = 0
        TextUser.Enabled = False
        TextPwd.Enabled = False

        CheckPrefixPreCard.Checked = False
        TextPriFixLenghtPre.Enabled = False
        TextPriTextPre.Enabled = False

        CheckPrefixPrePaycode.Checked = False
        TextPriFixLenghtPay.Enabled = False
        TextPriTextPay.Enabled = False
        ''clear all textboxes 'not working .. go to XtraCustomisedReportEdit->findTopTextBox
        'Dim a As Control
        'For Each a In Me.Controls
        '    If TypeOf a Is TextBox Then
        '        a.Text = ""
        '    End If
        'Next
        ''end clear all textboxes

    End Sub
    Private Sub setValue(ds As DataSet)
        If ds.Tables(0).Rows(0).Item("IsParallel").ToString.Trim = "Y" Then
            ToggleParallel.IsOn = True
        Else
            ToggleParallel.IsOn = False
            setDefault()
            Exit Sub
        End If
        If ToggleParallel.IsOn = False Then
            PanelControl3.Enabled = False
            GroupControl1.Enabled = False
            btnTest.Enabled = False
        Else
            PanelControl3.Enabled = True
            GroupControl1.Enabled = True
            btnTest.Enabled = True
        End If
        ComboDBType.EditValue = ds.Tables(0).Rows(0).Item("DBType").ToString.Trim
        ComboAuthMode.EditValue = ds.Tables(0).Rows(0).Item("AuthMode").ToString.Trim
        TextServerName.Text = ds.Tables(0).Rows(0).Item("ServerName").ToString.Trim
        TextUser.Text = ds.Tables(0).Rows(0).Item("UserName").ToString.Trim
        TextPwd.Text = ds.Tables(0).Rows(0).Item("Pwd").ToString.Trim
        TextDBName.Text = ds.Tables(0).Rows(0).Item("DBName").ToString.Trim

        TextTblName.Text = ds.Tables(0).Rows(0).Item("TblName").ToString.Trim
        TextPresentCardNo.Text = ds.Tables(0).Rows(0).Item("PreCardNo").ToString.Trim
        CheckPrefixPreCard.EditValue = ds.Tables(0).Rows(0).Item("PreIsPrifix").ToString.Trim
        TextPriFixLenghtPre.Text = ds.Tables(0).Rows(0).Item("PreLength").ToString.Trim
        TextPriTextPre.Text = ds.Tables(0).Rows(0).Item("PreText").ToString.Trim

        TextPayCode.Text = ds.Tables(0).Rows(0).Item("Paycode").ToString.Trim
        CheckPrefixPrePaycode.EditValue = ds.Tables(0).Rows(0).Item("PayIsPrefix").ToString.Trim
        TextPriFixLenghtPay.Text = ds.Tables(0).Rows(0).Item("PayLength").ToString.Trim
        TextPriTextPay.Text = ds.Tables(0).Rows(0).Item("PayText").ToString.Trim
        TextPunchDate.Text = ds.Tables(0).Rows(0).Item("PunchDate").ToString.Trim
        TextDateFormat.Text = ds.Tables(0).Rows(0).Item("PunchDateFormat").ToString.Trim
        TextPunchTime.Text = ds.Tables(0).Rows(0).Item("PunchTime").ToString.Trim
        TextTimeFormat.Text = ds.Tables(0).Rows(0).Item("PunchTimeFormat").ToString.Trim
        TextPunchDateTime.Text = ds.Tables(0).Rows(0).Item("PunchDateTime").ToString.Trim
        TextDateTimeFormat.Text = ds.Tables(0).Rows(0).Item("PunchDateTimeFormat").ToString.Trim
        TextDeviceId.Text = ds.Tables(0).Rows(0).Item("DeviceId").ToString.Trim
        TextPunchDirection.Text = ds.Tables(0).Rows(0).Item("PunchDirection").ToString.Trim
        TextInValue.Text = ds.Tables(0).Rows(0).Item("InValue").ToString.Trim
        TextOutValue.Text = ds.Tables(0).Rows(0).Item("OutVaue").ToString.Trim
        TextManual.Text = ds.Tables(0).Rows(0).Item("IsManual").ToString.Trim
        TextMYesValue.Text = ds.Tables(0).Rows(0).Item("YesValue").ToString.Trim
        TextMNoValue.Text = ds.Tables(0).Rows(0).Item("NoValue").ToString.Trim
        TextRes1.Text = ds.Tables(0).Rows(0).Item("Rfield1").ToString.Trim
        TextDafVal1.Text = ds.Tables(0).Rows(0).Item("Rfield1Value").ToString.Trim
        TextRes2.Text = ds.Tables(0).Rows(0).Item("Rfield2").ToString.Trim
        TextDafVal2.Text = ds.Tables(0).Rows(0).Item("Rfield2Value").ToString.Trim
        TextRes3.Text = ds.Tables(0).Rows(0).Item("Rfield3").ToString.Trim
        TextDafVal3.Text = ds.Tables(0).Rows(0).Item("Rfield3Value").ToString.Trim
        TextName.Text = ds.Tables(0).Rows(0).Item("EmpName").ToString.Trim
        If ComboDBType.SelectedIndex = 0 Then
            ComboAuthMode.Enabled = True
            If ComboAuthMode.SelectedIndex = 0 Then
                TextUser.Enabled = False
                TextPwd.Enabled = False
            Else
                TextUser.Enabled = True
                TextPwd.Enabled = True
            End If
        Else
            ComboAuthMode.Enabled = False
            TextUser.Enabled = True
            TextPwd.Enabled = True
        End If
        If ComboAuthMode.SelectedIndex = 0 Then
            TextUser.Enabled = False
            TextPwd.Enabled = False
        Else
            TextUser.Enabled = True
            TextPwd.Enabled = True
        End If
        If CheckPrefixPreCard.Checked Then
            TextPriFixLenghtPre.Enabled = True
            TextPriTextPre.Enabled = True
        Else
            TextPriFixLenghtPre.Enabled = False
            TextPriTextPre.Enabled = False
        End If
        If CheckPrefixPrePaycode.Checked Then
            TextPriFixLenghtPay.Enabled = True
            TextPriTextPay.Enabled = True
        Else
            TextPriFixLenghtPay.Enabled = False
            TextPriTextPay.Enabled = False
        End If
    End Sub
    Private Sub ComboDBType_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboDBType.SelectedIndexChanged
        If ComboDBType.SelectedIndex = 0 Then
            ComboAuthMode.Enabled = True
            If ComboAuthMode.SelectedIndex = 0 Then
                TextUser.Enabled = False
                TextPwd.Enabled = False
            Else
                TextUser.Enabled = True
                TextPwd.Enabled = True
            End If
        Else
            ComboAuthMode.Enabled = False
            TextUser.Enabled = True
            TextPwd.Enabled = True
        End If
    End Sub
    Private Sub ComboAuthMode_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboAuthMode.SelectedIndexChanged
        If ComboAuthMode.SelectedIndex = 0 Then
            TextUser.Enabled = False
            TextPwd.Enabled = False
        Else
            TextUser.Enabled = True
            TextPwd.Enabled = True
        End If
    End Sub
    Private Sub CheckPrefixPreCard_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckPrefixPreCard.CheckedChanged
        If CheckPrefixPreCard.Checked Then
            TextPriFixLenghtPre.Enabled = True
            TextPriTextPre.Enabled = True
        Else
            TextPriFixLenghtPre.Enabled = False
            TextPriTextPre.Enabled = False
        End If
    End Sub
    Private Sub CheckPrefixPrePaycode_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckPrefixPrePaycode.CheckedChanged
        If CheckPrefixPrePaycode.Checked Then
            TextPriFixLenghtPay.Enabled = True
            TextPriTextPay.Enabled = True
        Else
            TextPriFixLenghtPay.Enabled = False
            TextPriTextPay.Enabled = False
        End If
    End Sub
    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Me.Cursor = Cursors.WaitCursor
        Dim sSql As String = ""
        Dim IsParallel As String
        If ToggleParallel.IsOn = True Then
            IsParallel = "Y"           
            DBName = TextDBName.Text.Trim
            DBType = ComboDBType.EditValue
            AuthMode = ComboAuthMode.EditValue
            ServerName = TextServerName.Text.Trim
            UserName = TextUser.Text.Trim
            Pwd = TextPwd.Text.Trim
            TblName = TextTblName.Text.Trim
            PreCardNo = TextPresentCardNo.Text.Trim
            PreIsPrifix = CheckPrefixPreCard.EditValue
            PreLength = TextPriFixLenghtPre.Text.Trim
            PreText = TextPriTextPre.Text.Trim
            Paycode = TextPayCode.Text.Trim
            PayIsPrefix = CheckPrefixPrePaycode.EditValue
            PayLength = TextPriFixLenghtPay.Text.Trim
            PayText = TextPriTextPay.Text.Trim
            PunchDate = TextPunchDate.Text.Trim
            PunchDateFormat = TextDateFormat.Text.Trim
            PunchTime = TextPunchTime.Text.Trim
            PunchTimeFormat = TextTimeFormat.Text.Trim
            PunchDateTime = TextPunchDateTime.Text.Trim
            PunchDateTimeFormat = TextDateTimeFormat.Text.Trim
            DeviceId = TextDeviceId.Text.Trim
            PunchDirection = TextPunchDirection.Text.Trim
            InValue = TextInValue.Text.Trim
            OutVaue = TextOutValue.Text.Trim
            IsManual = TextManual.Text.Trim
            YesValue = TextMYesValue.Text.Trim
            NoValue = TextMNoValue.Text.Trim
            Rfield1 = TextRes1.Text.Trim
            Rfield1Value = TextDafVal1.Text.Trim
            Rfield2 = TextRes2.Text.Trim
            Rfield2Value = TextDafVal2.Text.Trim
            Rfield3 = TextRes3.Text.Trim
            Rfield3Value = TextDafVal3.Text.Trim
            EmpName = TextName.Text.Trim
            If TblName = "" Then
                Me.Cursor = Cursors.Default
                XtraMessageBox.Show(ulf, "<size=10>Table Name cannot be Empty</size>", "<size=9>Error</size>")
                TextTblName.Select()
                Exit Sub
            End If
            If ComboDBType.SelectedIndex = 0 And ComboAuthMode.SelectedIndex = 0 Then
                UserName = ""
                Pwd = ""
            ElseIf UserName = "" Or Pwd = "" Then
                Me.Cursor = Cursors.Default
                XtraMessageBox.Show(ulf, "<size=10>User Name or Password cannot be Empty</size>", "<size=9>Error</size>")
                TextUser.Select()
                Exit Sub
            End If
            If PreCardNo = "" Then
                PreIsPrifix = "N"
                PreLength = ""
                PreText = ""
            End If
            If Paycode = "" Then
                PayIsPrefix = "N"
                PayLength = ""
                PayText = ""
            End If
            If PreIsPrifix = "N" Then
                PreLength = ""
                PreText = ""
            End If
            If PayIsPrefix = "N" Then
                PayLength = ""
                PayText = ""
            End If
            Dim tmpdate As String = ""
            Dim tmpdate1 As DateTime
            If PunchDate = "" Then
                PunchDateFormat = ""
            Else
                Try
                    tmpdate = Format(System.DateTime.Now, PunchDateFormat)
                    tmpdate1 = Convert.ToDateTime(tmpdate)
                Catch ex As Exception
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Date Format</size>", "<size=9>Error</size>")
                    TextDateFormat.Select()
                    Exit Sub
                End Try
            End If
            If PunchTime = "" Then
                PunchTimeFormat = ""
            Else
                Try
                    tmpdate = Format(System.DateTime.Now, PunchTimeFormat)
                    tmpdate1 = Convert.ToDateTime(tmpdate)
                Catch ex As Exception
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Time Format</size>", "<size=9>Error</size>")
                    TextTimeFormat.Select()
                    Exit Sub
                End Try
            End If
            If PunchDateTime = "" Then
                PunchDateTimeFormat = ""
            Else
                Try
                    tmpdate = Format(System.DateTime.Now, PunchDateTimeFormat)
                    tmpdate1 = Convert.ToDateTime(tmpdate)
                Catch ex As Exception
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Date Time Format</size>", "<size=9>Error</size>")
                    TextDateTimeFormat.Select()
                    Exit Sub
                End Try
            End If
            If PunchDirection = "" Then
                InValue = ""
                OutVaue = ""
            End If
            If IsManual = "" Then
                YesValue = ""
                NoValue = ""
            End If
            If Rfield1 = "" Then
                Rfield1Value = ""
            End If
            If Rfield2 = "" Then
                Rfield2Value = ""
            End If
            If Rfield3 = "" Then
                Rfield3Value = ""
            End If
            Dim IsDB As Boolean
            Dim IsTable As Boolean
            If checkConnection(ConnectionString, IsDB, IsTable) = False Then
                If XtraMessageBox.Show(ulf, "<size=10>Failed to Connect " & vbCrLf & "Are you still want to save the setting?</size>", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.Yes Then
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            End If
            If IsDB = False Or IsTable = False Then
                If XtraMessageBox.Show(ulf, "<size=10>Cannot find Database" & vbCrLf & "Do you want to create Database?</size>", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    createDBTable(IsDB)
                End If
            End If
            Dim Query As String = ""
            createInsertQuery(Query)
            sSql = "update ParallelDB set " & _
             "[IsParallel] = '" & IsParallel & "'" & _
             ",[DBName] = '" & DBName & "'" & _
             ",[DBType] = '" & DBType & "'" & _
             ",[AuthMode] = '" & AuthMode & "'" & _
             ",[ServerName] = '" & ServerName & "'" & _
             ",[UserName] = '" & UserName & "'" & _
             ",[Pwd] = '" & Pwd & "'" & _
             ",[TblName] = '" & TblName & "'" & _
             ",[PreCardNo] = '" & PreCardNo & "'" & _
             ",[PreIsPrifix] = '" & PreIsPrifix & "'" & _
             ",[PreLength] = '" & PreLength & "'" & _
             ",[PreText] = '" & PreText & "'" & _
             ",[Paycode] = '" & Paycode & "'" & _
             ",[PayIsPrefix] = '" & PayIsPrefix & "'" & _
             ",[PayLength] = '" & PayLength & "'" & _
             ",[PayText] = '" & PayText & "'" & _
             ",[PunchDate] = '" & PunchDate & "'" & _
             ",[PunchDateFormat] = '" & PunchDateFormat & "'" & _
             ",[PunchTime] = '" & PunchTime & "'" & _
             ",[PunchTimeFormat] = '" & PunchTimeFormat & "'" & _
             ",[PunchDateTime] = '" & PunchDateTime & "'" & _
             ",[PunchDateTimeFormat] = '" & PunchDateTimeFormat & "'" & _
             ",[DeviceId] = '" & DeviceId & "'" & _
             ",[PunchDirection] = '" & PunchDirection & "'" & _
             ",[InValue] = '" & InValue & "'" & _
             ",[OutVaue] = '" & OutVaue & "'" & _
             ",[IsManual] = '" & IsManual & "'" & _
             ",[YesValue] = '" & YesValue & "'" & _
             ",[NoValue] = '" & NoValue & "'" & _
             ",[Rfield1] = '" & Rfield1 & "'" & _
             ",[Rfield1Value] = '" & Rfield1Value & "'" & _
             ",[Rfield2] = '" & Rfield2 & "'" & _
             ",[Rfield2Value] = '" & Rfield2Value & "'" & _
             ",[Rfield3] = '" & Rfield3 & "'" & _
             ",[Rfield3Value] = '" & Rfield3Value & "'" & _
             ",[EmpName] = '" & EmpName & "'" & _
             ",[ConnectionString] = '" & ConnectionString & "'" & _
             ",[Query] = '" & Query & "'"
        Else
            IsParallel = "N"
            sSql = "update ParallelDB set IsParallel ='N'"
        End If
        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Common.LoadParallelDB()
        Common.LogPost("Parallel Data Setting Save")
        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>iAS</size>")
    End Sub
    Private Sub btnTest_Click(sender As System.Object, e As System.EventArgs) Handles btnTest.Click
        DBType = ComboDBType.EditValue
        Dim connectionString As String = ""
        Dim IsDB As Boolean
        Dim IsTable As Boolean
        If checkConnection(connectionString, IsDB, IsTable) Then
            XtraMessageBox.Show(ulf, "<size=10>Connection Successful</size>", "<size=9>iAS</size>")
        Else
            XtraMessageBox.Show(ulf, "<size=10>Failed to Connect</size>", "<size=9>iAS</size>")
        End If
    End Sub
    Private Function checkConnection(ByRef connectionString As String, ByRef IsDB As Boolean, ByRef IsTable As Boolean) As Boolean
        If DBType = "MY SQL" Then
            connectionString = "server=" & ServerName & "; uid=" & UserName & "; pwd=" & Pwd & ";database=" & DBName & ""
            Dim ConMySql As MySqlConnection = New MySqlConnection(connectionString)
            Try
                If ConMySql.State <> ConnectionState.Open Then
                    ConMySql.Open()
                    ConMySql.Close()
                End If
                IsDB = True
                Return True
            Catch ex As Exception
                If ex.Message = "Unknown database '" & DBName & "'" Then
                    IsDB = False
                Else
                    IsDB = True
                End If
                Return False
            End Try
        ElseIf DBType = "SQL Server" Then
            If AuthMode = "Windows" Then
                connectionString = "Data Source=" & ServerName & ";Initial Catalog=" & DBName & ";Integrated Security=True;MultipleActiveResultSets=true;"
            Else
                connectionString = "Data Source=" & ServerName & ";Initial Catalog=" & DBName & ";User Id=" & UserName & ";Password=" & Pwd & ";MultipleActiveResultSets=true;"
            End If
            Dim comSql As SqlConnection = New SqlConnection(connectionString)
            Try
                comSql.Open()
                comSql.Close()
                IsDB = True
                If IsDB = True Then
                    Try
                        Dim sSql As String = "select top 1 * from " & TextTblName.Text.Trim
                        Dim adapP As SqlDataAdapter = New SqlDataAdapter(sSql, comSql)
                        Dim dsP As DataSet = New DataSet
                        adapP.Fill(dsP)
                    Catch ex As Exception
                        If ex.Message.ToString = "Invalid object name '" & TextTblName.Text.Trim & "'." Then
                            IsTable = False
                            Return False
                        End If
                    End Try                   
                End If
                Return True
            Catch ex As Exception
                If ex.Message.Contains("Cannot open database """ & DBName & """ requested by the login. The login failed.") Then
                    IsDB = False
                    IsTable = False
                Else
                    IsDB = True
                    IsTable = False
                End If
                Return False
            End Try
        ElseIf DBType = "Oracle" Then
            connectionString = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & ServerName & ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & DBName & ")));user id=" & UserName & ";password=" & Pwd & ";"
            IsDB = True
            IsTable = False
            Return True
            'Try
            '    Dim OrlConn As OracleConnection = New OracleConnection(connectionString)
            '    OrlConn.Open()
            '    OrlConn.Close()
            '    IsDB = True
            '    Return True
            'Catch ex As Exception
            '    Return False
            'End Try
        End If
        Return False
    End Function
    Private Function createDBTable(IsDB As Boolean) As Boolean
        Dim tableCreate As String = "CREATE table " & TblName & " ("
        Dim feildsStr As String = ""
        If PreCardNo <> "" Then
            feildsStr = feildsStr & " " & PreCardNo & " varchar(max)"
        End If
        If Paycode <> "" Then
            feildsStr = feildsStr & ", " & Paycode & " varchar(max)"
        End If
        If PunchDate <> "" Then
            feildsStr = feildsStr & ",  " & PunchDate & " varchar(max)" '" datetime"
        End If
        If PunchTime <> "" Then
            feildsStr = feildsStr & ", " & PunchTime & " varchar(max)" '" datetime"
        End If
        If PunchDateTime <> "" Then
            feildsStr = feildsStr & ", " & PunchDateTime & " varchar(max)" '" datetime"
        End If
        If DeviceId <> "" Then
            feildsStr = feildsStr & ",  " & DeviceId & " varchar(max)"
        End If
        If PunchDirection <> "" Then
            feildsStr = feildsStr & ",  " & PunchDirection & " varchar(max)"
        End If
        If IsManual <> "" Then
            feildsStr = feildsStr & ", " & IsManual & " varchar(max)"
        End If
        If Rfield1 <> "" Then
            feildsStr = feildsStr & ", " & Rfield1 & " varchar(max)"
        End If
        If Rfield2 <> "" Then
            feildsStr = feildsStr & ",  " & Rfield2 & " varchar(max)"
        End If
        If Rfield3 <> "" Then
            feildsStr = feildsStr & ",  " & Rfield3 & " varchar(max)"
        End If
        If EmpName <> "" Then
            feildsStr = feildsStr & ",  " & EmpName & " varchar(max)"
        End If
        tableCreate = tableCreate & feildsStr.TrimStart(",") & ")"

        If DBType = "MY SQL" Then
            Dim cmdText As String = "CREATE DATABASE `" & DBName & "`;"
            Dim connString As String = "server=" & ServerName & ";user id=" & UserName & ";password=" & Pwd & ";database="
            Dim conMysql As MySqlConnection = New MySqlConnection(connString)

            Dim mCmD As MySqlCommand = New MySqlCommand()
            Try
                conMysql.Open()
                mCmD = New MySqlCommand(cmdText, conMysql)
                mCmD.ExecuteNonQuery()
                conMysql.Close()

                tableCreate = tableCreate.Replace("varchar(max)", "varchar(255)")
                conMysql = New MySqlConnection(ConnectionString)
                conMysql.Open()
                mCmD = New MySqlCommand(tableCreate, conMysql)
                mCmD.ExecuteNonQuery()
                conMysql.Close()
                Return True
            Catch ex As Exception
                If conMysql.State <> ConnectionState.Closed Then
                    conMysql.Close()
                End If
                Return False
            End Try
        ElseIf DBType = "SQL Server" Then
            Dim connString As String ' = ("Data Source=" & server & ";Initial Catalog=master;Integrated Security=True;")
            If AuthMode = "Windows" Then
                connString = "Data Source=" & ServerName & ";Initial Catalog=master;Integrated Security=True;"
            Else
                connString = "Data Source='" & ServerName & "';Initial Catalog=master;User Id=" & UserName & ";Password=" & Pwd & ";"
            End If
            Dim connection As SqlConnection = New SqlConnection(connString)
            Dim cmd As New SqlClient.SqlCommand
            Dim sSql As String = "CREATE DATABASE " & DBName
            If IsDB = False Then
                connection.Open()
                cmd = New SqlCommand(sSql, connection)
                cmd.ExecuteNonQuery()
                connection.Close()
            End If
            connection = New SqlConnection(ConnectionString)
            If tableCreate <> "CREATE table " & TblName & " ()" Then
                connection.Open()
                cmd = New SqlCommand(tableCreate, connection)
                cmd.ExecuteNonQuery()
                connection.Close()
            End If
        ElseIf DBType = "Oracle" Then
            'first check exception in checkConnection
        End If
        Return False
    End Function
    Private Function createInsertQuery(ByRef insert As String)
        Dim InsertQuery As String = "Insert into " & TblName & " ("
        Dim InsertQueryValues As String = " values ("
        If PreCardNo <> "" Then
            InsertQuery = InsertQuery & " " & PreCardNo
            InsertQueryValues = InsertQueryValues & "'PreCardNoVal'"
        End If
        If Paycode <> "" Then
            InsertQuery = InsertQuery & ", " & Paycode
            InsertQueryValues = InsertQueryValues & ",'PaycodeVal'"
        End If
        If PunchDate <> "" Then
            InsertQuery = InsertQuery & ",  " & PunchDate
            InsertQueryValues = InsertQueryValues & ",'PunchDateVal'"
        End If
        If PunchTime <> "" Then
            InsertQuery = InsertQuery & ", " & PunchTime
            InsertQueryValues = InsertQueryValues & ",'PunchTimeVal'"
        End If
        If PunchDateTime <> "" Then
            InsertQuery = InsertQuery & ", " & PunchDateTime
            InsertQueryValues = InsertQueryValues & ",'PunchDateTimeVal'"
        End If
        If DeviceId <> "" Then
            InsertQuery = InsertQuery & ",  " & DeviceId
            InsertQueryValues = InsertQueryValues & ",'DeviceIdVal'"
        End If
        If PunchDirection <> "" Then
            InsertQuery = InsertQuery & ",  " & PunchDirection
            InsertQueryValues = InsertQueryValues & ",'PunchDirectionVal'"
        End If
        If IsManual <> "" Then
            InsertQuery = InsertQuery & ", " & IsManual
            InsertQueryValues = InsertQueryValues & ",'IsManualVal'"
        End If
        If Rfield1 <> "" Then
            InsertQuery = InsertQuery & ", " & Rfield1
            InsertQueryValues = InsertQueryValues & ",'Rfield1Val'"
        End If
        If Rfield2 <> "" Then
            InsertQuery = InsertQuery & ",  " & Rfield2
            InsertQueryValues = InsertQueryValues & ",'Rfield2Val'"
        End If
        If Rfield3 <> "" Then
            InsertQuery = InsertQuery & ",  " & Rfield3
            InsertQueryValues = InsertQueryValues & ",'Rfield3Val'"
        End If
        If EmpName <> "" Then
            InsertQuery = InsertQuery & ", " & EmpName
            InsertQueryValues = InsertQueryValues & ",'EmpNameVal'"
        End If
        'InsertQuery = InsertQuery & ")"
        'InsertQueryValues = InsertQueryValues & ")"
        'insert = InsertQuery & InsertQueryValues.Replace("'", "''")

        InsertQuery = InsertQuery & ")"
        InsertQuery = InsertQuery.Replace("(,", "(")
        InsertQueryValues = InsertQueryValues & ")"
        InsertQueryValues = InsertQueryValues.Replace("(,", "(")
        insert = InsertQuery & InsertQueryValues.Replace("'", "''")

    End Function

    Private Sub BtnManualAdd_Click(sender As Object, e As EventArgs) Handles BtnManualAdd.Click
        If Common.IsParallel = "Y" Then

            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim sSql As String = "select * from MachineRawPunchAll, tblEmployee where machinerawpunchAll.PAYCODE=TblEmployee.PAYCODE and OFFICEPUNCH >='" & DateEditFrmDate.DateTime.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Try

                        Dim sdwEnrollNumber As String = ds.Tables(0).Rows(i).Item("CARDNO").ToString
                        If IsNumeric(sdwEnrollNumber) Then
                            sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber)
                        End If
                        Dim IN_OUT As String = ds.Tables(0).Rows(i).Item("INOUT").ToString
                        Dim PunchDate As String = ds.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString
                        Dim ISMANUAL As String = ds.Tables(0).Rows(i).Item("ISMANUAL").ToString
                        Dim ID_NO As String = ds.Tables(0).Rows(i).Item("MC_NO").ToString
                        XtraMasterTest.LabelControlStatus.Text = "Inserting " & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & " " & Convert.ToDateTime(PunchDate).ToString("yyyy-MM-dd HH:mm:ss")
                        Application.DoEvents()
                        Common.parallelInsert(sdwEnrollNumber, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, Convert.ToDateTime(PunchDate), IN_OUT, ISMANUAL, ds.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim, ID_NO)
                    Catch ex As Exception

                    End Try
                Next
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                XtraMessageBox.Show(ulf, "<size=10>Manual Insert Success</size>", "<size=9>iAS</size>")
            Else
                XtraMessageBox.Show(ulf, "<size=10>No Records Available</size>", "<size=9>iAS</size>")
            End If
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub
End Class
