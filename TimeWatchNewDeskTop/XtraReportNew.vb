﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraSplashScreen
Public Class XtraReportNew
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim WhichReport As Integer
    Dim g_WhereClause As String
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub XtraReportNew_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        Me.Width = My.Computer.Screen.WorkingArea.Width
        SplitContainerControl1.Width = My.Computer.Screen.WorkingArea.Width
        SplitContainerControl1.SplitterPosition = (My.Computer.Screen.WorkingArea.Width) * 90 / 100

        DateEdit1.DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
        DateEdit2.EditValue = Now
        If Common.servername = "" Then
            Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            GridControlComp.DataSource = SSSDBDataSet.tblCompany1

            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee1

            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment1

            Me.TblCatagory1TableAdapter1.Fill(Me.SSSDBDataSet.tblCatagory1)
            GridControlCat.DataSource = SSSDBDataSet.tblCatagory1

            Me.TblShiftMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblShiftMaster1)
            GridControlShift.DataSource = SSSDBDataSet.tblShiftMaster1

            Me.TblGrade1TableAdapter1.Fill(Me.SSSDBDataSet.tblGrade1)
            GridControlGrade.DataSource = SSSDBDataSet.tblGrade1
        Else
            TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            GridControlComp.DataSource = SSSDBDataSet.tblCompany

            'TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee

            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment

            TblCatagoryTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblCatagoryTableAdapter.Fill(Me.SSSDBDataSet.tblCatagory)
            GridControlCat.DataSource = SSSDBDataSet.tblCatagory

            TblShiftMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblShiftMasterTableAdapter.Fill(Me.SSSDBDataSet.tblShiftMaster)
            GridControlShift.DataSource = SSSDBDataSet.tblShiftMaster

            TblGradeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblGradeTableAdapter.Fill(Me.SSSDBDataSet.tblGrade)
            GridControlGrade.DataSource = SSSDBDataSet.tblGrade
        End If
        GridControlEmp.DataSource = Common.EmpNonAdmin

        DateEdit1.DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
        DateEdit2.EditValue = Now
        Common.SetGridFont(GridViewComp, New Font("Tahoma", 10))
        Common.SetGridFont(GridViewEmp, New Font("Tahoma", 10))
    End Sub
    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditEmp.QueryResultValue
        Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("PAYCODE"))
        Next
        e.Value = sb.ToString
    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim c As CommonReport = New CommonReport
        If CheckPerformance.Checked = True Then
            WhichReport = 1
        End If
        Set_Filter_Crystal()
        c.Monthly_Performance_ow("", DateEdit1.DateTime.ToString("yyyy-MM-dd HH:mm:ss"), DateEdit2.DateTime.ToString("yyyy-MM-dd HH:mm:ss"), g_WhereClause)
    End Sub

    Private Sub Set_Filter_Crystal() 'Optional ByVal Selection As String
        'On Error GoTo ErrorGen
        'Dim i As Integer
        Dim mCompanyString As String, mDepartmentString As String, mCatagoryString As String
        Dim mEmployeeString As String, r_CompantStr As String, mShiftString As String
        Dim mDivisionString As String, mGradeString As String, mActive As String

        mCompanyString = ""
        mActive = ""
        mDepartmentString = ""
        mCatagoryString = ""
        mEmployeeString = ""
        mShiftString = ""
        mDivisionString = ""
        mGradeString = ""
        Dim g_CompanyNames As String = ""
        'For CompanyCode
        Dim selectedRowsComp As Integer() = GridViewComp.GetSelectedRows()
        Dim resultComp As Object() = New Object(selectedRowsComp.Length - 1) {}
        For i As Integer = 0 To selectedRowsComp.Length - 1
            Dim rowHandle As Integer = selectedRowsComp(i)
            If Not GridViewComp.IsGroupRow(rowHandle) Then
                resultComp(i) = GridViewComp.GetRowCellValue(rowHandle, "COMPANYCODE")
                If i <> 0 Then
                    mCompanyString = mCompanyString + " OR "
                End If
                mCompanyString = mCompanyString + "tblEmployee.CompanyCode = "
                mCompanyString = mCompanyString + "'" + resultComp(i) + "'"
                g_CompanyNames = g_CompanyNames & GridViewComp.GetRowCellValue(rowHandle, "COMPANYNAME") & ", "
            End If
        Next
        'For i = 0 To (LstCompanyTarget.ListCount - 1)
        '    LstCompanyTarget.ListIndex = i
        '    If i <> 0 Then
        '        mCompanyString = mCompanyString + " OR "
        '    End If
        '    mCompanyString = mCompanyString + "tblEmployee.CompanyCode = "
        '    mCompanyString = mCompanyString + "'" + Left(LstCompanyTarget.Text, 3) + "'"
        '    g_CompanyNames = g_CompanyNames & Trim(Mid(LstCompanyTarget.Text, 6)) & ", "
        'Next i

        'For DepartmentCode
        Dim selectedRowsDept As Integer() = GridViewDept.GetSelectedRows()
        Dim resultDept As Object() = New Object(selectedRowsDept.Length - 1) {}
        For i As Integer = 0 To selectedRowsDept.Length - 1
            Dim rowHandle As Integer = selectedRowsDept(i)
            If Not GridViewDept.IsGroupRow(rowHandle) Then
                resultDept(i) = GridViewDept.GetRowCellValue(rowHandle, "DEPARTMENTCODE")
                If i <> 0 Then
                    mDepartmentString = mDepartmentString + " OR "
                End If
                mDepartmentString = mDepartmentString + "tblEmployee.DepartmentCode = "
                mDepartmentString = mDepartmentString + "'" + resultDept(i) + "'"
            End If
        Next

        'For CatagoryCode
        Dim selectedRowsCat As Integer() = GridViewCat.GetSelectedRows()
        Dim resultCat As Object() = New Object(selectedRowsCat.Length - 1) {}
        For i As Integer = 0 To selectedRowsCat.Length - 1
            Dim rowHandle As Integer = selectedRowsCat(i)
            If Not GridViewCat.IsGroupRow(rowHandle) Then
                resultCat(i) = GridViewCat.GetRowCellValue(rowHandle, "CAT")
                If i <> 0 Then
                    mCatagoryString = mCatagoryString + " OR "
                End If
                mCatagoryString = mCatagoryString + "tblEmployee.CAT = "
                mCatagoryString = mCatagoryString + "'" + resultCat(i) + "'"
            End If
        Next

        ' For Division Code
        'For i = 0 To (lstDivisionTarget.ListCount - 1)
        '    lstDivisionTarget.ListIndex = i
        '    If i <> 0 Then
        '        mDivisionString = mDivisionString + " OR "
        '    End If
        '    mDivisionString = mDivisionString + "tblEmployee.DivisionCode = "
        '    mDivisionString = mDivisionString + "'" + Left(lstDivisionTarget.Text, 3) + "'"
        'Next i

        ' For Grade Code
        Dim selectedRowsGrade As Integer() = GridViewGrade.GetSelectedRows()
        Dim resultGrade As Object() = New Object(selectedRowsGrade.Length - 1) {}
        For i As Integer = 0 To selectedRowsGrade.Length - 1
            Dim rowHandle As Integer = selectedRowsGrade(i)
            If Not GridViewGrade.IsGroupRow(rowHandle) Then
                resultGrade(i) = GridViewGrade.GetRowCellValue(rowHandle, "GradeCode")
                If i <> 0 Then
                    mGradeString = mGradeString + " OR "
                End If
                mGradeString = mGradeString + "tblEmployee.GradeCode = "
                mGradeString = mGradeString + "'" + resultGrade(i) + "'"
            End If
        Next

        'For EmployeeCode  - Check for late arrival
        Dim selectedRowsEmp As Integer() = GridViewEmp.GetSelectedRows()
        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
        For i As Integer = 0 To selectedRowsEmp.Length - 1
            Dim rowHandle As Integer = selectedRowsEmp(i)
            If Not GridViewEmp.IsGroupRow(rowHandle) Then
                resultEmp(i) = GridViewEmp.GetRowCellValue(rowHandle, "PAYCODE")
                Dim emp As String = resultEmp(i)
                If i <> 0 Then
                    mEmployeeString = mEmployeeString + " OR "
                End If
                mEmployeeString = mEmployeeString & "tblEmployee.Paycode  = "
                mEmployeeString = mEmployeeString & "'" & emp & "'"
            End If
        Next

        'For Shift
        Dim selectedRowsShift As Integer() = GridViewShift.GetSelectedRows()
        Dim resultShift As Object() = New Object(selectedRowsShift.Length - 1) {}
        For i As Integer = 0 To selectedRowsShift.Length - 1
            Dim rowHandle As Integer = selectedRowsShift(i)
            If Not GridViewShift.IsGroupRow(rowHandle) Then
                resultShift(i) = GridViewShift.GetRowCellValue(rowHandle, "SHIFT")
                If i <> 0 Then
                    mShiftString = mShiftString + " OR "
                End If
                If WhichReport = 1 Then
                    mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
                Else
                    mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
                End If
                mShiftString = mShiftString + "'" + resultShift(i) + "'"
            End If
        Next
        'For i = 0 To (LstShiftTarget.ListCount - 1)
        '    LstShiftTarget.ListIndex = i
        '    If i <> 0 Then
        '        mShiftString = mShiftString + " OR "
        '    End If
        '    If WhichReport = 1 Then
        '        'If g_Report_view Then
        '        mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
        '        'Else
        '        '    mShiftString = mShiftString + "tblTimeRegisterd.ShiftAttended = "
        '        'End If
        '    Else
        '        'If g_Report_view Then
        '        mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
        '        'Else
        '        '    mShiftString = mShiftString + "tblTimeRegisterd.ShiftAttended = "
        '        'End If
        '    End If
        '    mShiftString = mShiftString + "'" + Left(LstShiftTarget.Text, 3) + "'"
        'Next i

        If CheckEdit1.Checked And CheckEdit2.Checked Then
            mActive = ""
        Else
            If CheckEdit1.Checked Then
                mActive = "tblEmployee.Active = 'Y'"
            End If
            If CheckEdit2.Checked Then
                mActive = "tblEmployee.Active = 'N'"
            End If
        End If
        Dim mCondition As String
        'Dim g_WhereClause As String
        mCondition = IIf(Len(Trim(mEmployeeString)) = 0, "(", "(" & Trim(mEmployeeString) & ") AND (") & _
                     IIf(Len(Trim(mCatagoryString)) = 0, "", " " & Trim(mCatagoryString) & ") AND (") & _
                     IIf(Len(Trim(mDepartmentString)) = 0, "", " " & Trim(mDepartmentString) & ") AND (") & _
                     IIf(Len(Trim(mCompanyString)) = 0, "", " " & Trim(mCompanyString) & ") AND (") & _
                     IIf(Len(Trim(mShiftString)) = 0, "", " " & Trim(mShiftString) & ") AND (") & _
                     IIf(Len(Trim(mGradeString)) = 0, "", " " & Trim(mGradeString) & ") AND (") & _
                     IIf(Len(Trim(mActive)) = 0, "", " " & Trim(mActive) & ") AND (")

        If mCondition = "(" Then
            mCondition = ""
        End If
        If Not Len(Trim(mCondition)) = 0 Then
            mCondition = mCondition.Remove(mCondition.Trim.Length - 5)
            'mCondition = LTrim(mCondition, Len(Trim(mCondition)) - 5)
        End If
        g_WhereClause = mCondition
        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation, "TimeWatch")
    End Sub
End Class
