﻿Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.Utils
Imports DevExpress.LookAndFeel
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports System.Net
Imports PdfSharp.Pdf  'for pdf report
Imports PdfSharp.Drawing 'for pdf report
Imports System.IO
Imports System.Runtime.InteropServices
Imports PdfSharp.Pdf.IO 'for pdf image 
Imports System.Threading
Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json
Imports System.Net.NetworkInformation
Imports System.Text
Imports CMITech.UMXClient
Imports MySql.Data.MySqlClient
Imports System.Text.RegularExpressions
Public Class ZKController
    <DllImport("C:\WINDOWS\system32\plcommpro.dll", EntryPoint:="Connect")>
    Public Shared Function Connect(ByVal Parameters As String) As IntPtr
    End Function
    <DllImport("plcommpro.dll", EntryPoint:="Disconnect")>
    Public Shared Sub Disconnect(ByVal h As IntPtr)
    End Sub
    <DllImport("plcommpro.dll", EntryPoint:="GetDeviceDataCount")>
    Public Shared Function GetDeviceDataCount(ByVal h As IntPtr, ByVal tablename As String, ByVal filter As String, ByVal options As String) As Integer
    End Function
    <DllImport("plcommpro.dll", EntryPoint:="SetDeviceData")>
    Public Shared Function SetDeviceData(ByVal h As IntPtr, ByVal tablename As String, ByVal data As String, ByVal options As String) As Integer
    End Function
    <DllImport("plcommpro.dll", EntryPoint:="GetDeviceData")>
    Public Shared Function GetDeviceData(ByVal h As IntPtr, ByRef buffer As Byte, ByVal buffersize As Integer, ByVal tablename As String, ByVal filename As String, ByVal filter As String, ByVal options As String) As Integer
    End Function
    <DllImport("plcommpro.dll", EntryPoint:="GetRTLog")>
    Public Shared Function GetRTLog(ByVal h As IntPtr, ByRef buffer As Byte, ByVal buffersize As Integer) As Integer
    End Function
    <DllImport("plcommpro.dll", EntryPoint:="SearchDevice")>
    Public Shared Function SearchDevice(ByVal commtype As String, ByVal address As String, ByRef buffer As Byte) As Integer
    End Function

    <DllImport("plcommpro.dll", EntryPoint:="PullLastError")>
    Public Shared Function PullLastError() As Integer
    End Function

End Class
