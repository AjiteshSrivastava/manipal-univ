﻿Imports System.Data.SqlClient
Imports DevExpress.LookAndFeel
Imports System.Data.OleDb
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Base

Public Class XtraOTMaintenance
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim datamaintain As Boolean = True
    Public Shared DateEditAtt As String
    Public Shared paycodeformaintaince As String
    Public Shared cardnoformaintaince As String
    Public Shared nameformaintaince As String
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'Me.LeaveApplication1TableAdapter1.Fill(Me.SSSDBDataSet.LeaveApplication1)
            'GridControl1.DataSource = SSSDBDataSet.EmployeeGroup1
        Else
            'LeaveApplicationTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            'Me.LeaveApplicationTableAdapter.Fill(Me.SSSDBDataSet.LeaveApplication)
            'GridControl1.DataSource = SSSDBDataSet.EmployeeGroup
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraDataMaintenance_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        'If Common.servername = "Access" Then
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        '    LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee1
        'Else
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        '    LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee
        'End If
        LookUpEdit1.Properties.DataSource = Common.EmpNonAdmin
        setDefault()
    End Sub
    Private Sub setDefault()
        TextEdit1.Text = Now.Year
        ComboBoxEdit2.SelectedIndex = Now.Month - 1
        TextEdit2.Text = ""
        LookUpEdit1.EditValue = ""
        lblCardNum.Text = ""
        lblCat.Text = ""
        'lblComp.Text = ""
        lblDept.Text = ""
        lblDesi.Text = ""
        lblGrade.Text = ""
        lblName.Text = ""
        lblEmpGrp.Text = ""

        If Common.IsNepali = "Y" Then
            TextEdit1.Visible = False
            ComboBoxEdit2.Visible = False
            ComboBoxEditNepaliYear.Visible = True
            ComboBoxEditNEpaliMonth.Visible = True
            Dim DC As New DateConverter()
            'Dim Vstart As DateTime = DC.ToBS(New Date(Now.Year, Now.Month, Now.Day))
            Dim Vstart As String = DC.ToBS(New Date(Now.Year, Now.Month, Now.Day))
            Dim dojTmp() As String = Vstart.Split("-")
            ComboBoxEditNepaliYear.EditValue = dojTmp(0)
            ComboBoxEditNEpaliMonth.SelectedIndex = dojTmp(1) - 1
        Else
            TextEdit1.Visible = True
            ComboBoxEdit2.Visible = True
            ComboBoxEditNepaliYear.Visible = False
            ComboBoxEditNEpaliMonth.Visible = False
        End If
    End Sub
    Private Sub setSelectValue()
        If Not datamaintain Then Return
        datamaintain = False

        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim ds1 As DataSet

        If LookUpEdit1.EditValue.ToString.Trim <> "" Then
            Dim sSql1 As String = "select EMPNAME, PRESENTCARDNO, (select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp, (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, (select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
            sSql1 = "select EMPNAME, PRESENTCARDNO, " &
"(SELECT COMPANYNAME from tblCompany WHERE tblCompany.COMPANYCODE = TblEmployee.COMPANYCODE) as CompName, " &
"(select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp," &
"(select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, " &
"(select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, " &
"(select GradeName from tblGrade WHERE tblGrade.GradeCode = TblEmployee.GradeCode) as GrdName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
            If Common.servername = "Access" Then
                sSql1 = "select T.EMPNAME, T.PRESENTCARDNO, C.COMPANYNAME  as CompName,  T.DESIGNATION, E.GroupName as EmpGrp, D.DEPARTMENTNAME as DEPName, cat.CATAGORYNAME as CATName, grd.GradeName as GrdName  from TblEmployee T,  tblCompany C, EmployeeGroup E, tblDepartment D, tblCatagory cat, tblGrade grd where C.COMPANYCODE = T.COMPANYCODE  and E.GroupId = T.EmployeeGroupId and D.DEPARTMENTCODE = T.DEPARTMENTCODE and cat.CAT = T.CAT and grd.GradeCode = T.GradeCode and T.Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
                adapA1 = New OleDbDataAdapter(sSql1, Common.con1)
                ds1 = New DataSet
                adapA1.Fill(ds1)
            Else
                adap1 = New SqlDataAdapter(sSql1, Common.con)
                ds1 = New DataSet
                adap1.Fill(ds1)
            End If
            If ds1.Tables(0).Rows.Count = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Invalid paycode</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                TextEdit1.Select()
                setDefault()
                datamaintain = True
                Exit Sub
            Else
                Dim sSQL As String = "select ISROUNDTHECLOCKWORK from tblEmployeeShiftMaster where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "' "
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSQL, Common.con1)
                    ds = New DataSet
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSQL, Common.con)
                    ds = New DataSet
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK") = "Y" Then
                    ToggleSwitch1.IsOn = True
                Else
                    ToggleSwitch1.IsOn = False
                End If
                lblName.Text = ds1.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim
                lblCardNum.Text = ds1.Tables(0).Rows(0).Item("PRESENTCARDNO").ToString.Trim
                'lblComp.Text = ds1.Tables(0).Rows(0).Item("CompName").ToString.Trim
                lblCat.Text = ds1.Tables(0).Rows(0).Item("CATName").ToString.Trim
                lblDept.Text = ds1.Tables(0).Rows(0).Item("DEPName").ToString.Trim
                lblEmpGrp.Text = ds1.Tables(0).Rows(0).Item("EmpGrp").ToString.Trim
                lblDesi.Text = ds1.Tables(0).Rows(0).Item("DESIGNATION").ToString.Trim
                lblGrade.Text = ds1.Tables(0).Rows(0).Item("GrdName").ToString.Trim
                '    LoadLeaveHistoryGrid()
                '    LoadLeaveQuota()
                '    SimpleButton1.Enabled = True
            End If
        End If
        datamaintain = True
    End Sub
    Private Sub LookUpEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles LookUpEdit1.Leave
        setSelectValue()
        setGridData()
    End Sub
    Private Sub setGridData()
        Me.Cursor = Cursors.WaitCursor
        'Dim dt As DataTable = New DataTable
        'dt.Columns.Add("Date")
        'dt.Columns.Add("Shift")
        'dt.Columns.Add("Status")
        'dt.Columns.Add("Late Arr")
        'dt.Columns.Add("Earlt Dep")
        'dt.Columns.Add("In Time")
        'dt.Columns.Add("Lunch Out")
        'dt.Columns.Add("Lunch In")
        'dt.Columns.Add("Out Time")
        'dt.Columns.Add("Over Time")
        Dim startdate As DateTime
        Dim enddate As DateTime
        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'Dim tmp As DateTime = DC.ToAD(New Date(ComboBoxEditNepaliYear.EditValue, ComboBoxEditNEpaliMonth.SelectedIndex + 1, 1))
                Dim tmp As DateTime = DC.ToAD(ComboBoxEditNepaliYear.EditValue & "-" & ComboBoxEditNEpaliMonth.SelectedIndex + 1 & "-" & 1)
                TextEdit1.Text = tmp.Year
                ComboBoxEdit2.SelectedIndex = tmp.Month
                'startdate = DC.ToAD(New Date(ComboBoxEditNepaliYear.EditValue, ComboBoxEditNEpaliMonth.SelectedIndex + 1, 1)) 'Convert.ToDateTime(tmp.Year & "-" & tmp.Month & "-01 00:00:00")
                startdate = DC.ToAD(ComboBoxEditNepaliYear.EditValue & "-" & ComboBoxEditNEpaliMonth.SelectedIndex + 1 & "-" & 1)
                enddate = startdate.AddMonths(1) 'DC.ToAD(New Date(ComboBoxEditNepaliYear.EditValue, ComboBoxEditNEpaliMonth.SelectedIndex + 2, 1)) 'Convert.ToDateTime(tmp.Year & "-" & tmp.Month + 1 & "-01 00:00:00")
                '
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Process From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboBoxEditNepaliYear.Select()
                Exit Sub
            End Try
        Else
            'MsgBox(TextEdit1.Text.Trim & "-" & ComboBoxEdit2.SelectedIndex + 1 & "-01 00:00:00")
            startdate = Convert.ToDateTime(TextEdit1.Text.Trim & "-" & ComboBoxEdit2.SelectedIndex + 1 & "-01 00:00:00")
            'enddate = Convert.ToDateTime(TextEdit1.Text.Trim & "-" & ComboBoxEdit2.SelectedIndex + 1 & "-01 00:00:00")
            'MsgBox(startdate.ToString("yyyy-MM-dd HH:mm:ss"))
            enddate = startdate.AddDays(System.DateTime.DaysInMonth(TextEdit1.Text.Trim, ComboBoxEdit2.SelectedIndex + 1)) '.AddDays(-1)
        End If

        'Dim sSql As String = "select DateOFFICE, SHIFT, STATUS, LATEARRIVAL, EARLYDEPARTURE, IN1, OUT1, IN2, OUT2, OTDURATION  from tblTimeRegister where PAYCODE='" & TextEdit2.Text.Trim & "' and DateOFFICE >='" & startdate.ToString("yyyy-MM-dd HH:mm:ss") & "' and DateOFFICE <'" & enddate.ToString("yyyy-MM-dd HH:mm:ss") & "' ORDER by DateOFFICE ASC"
        'Dim adap As SqlDataAdapter
        'Dim adapA As OleDbDataAdapter
        'Dim ds As DataSet = New DataSet
        'If Common.servername = "Access" Then
        '    sSql = "select DateOFFICE, SHIFT, STATUS, LATEARRIVAL, EARLYDEPARTURE, IN1, OUT1, IN2, OUT2, OTDURATION  from tblTimeRegister where PAYCODE='" & TextEdit2.Text.Trim & "' and FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00') >='" & startdate.ToString("yyyy-MM-dd HH:mm:ss") & "' and FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00') < '" & enddate.ToString("yyyy-MM-dd HH:mm:ss") & "' ORDER by DateOFFICE ASC"
        '    adapA = New OleDbDataAdapter(sSql, Common.con1)
        '    adapA.Fill(ds)
        'Else
        '    adap = New SqlDataAdapter(sSql, Common.con)
        '    adap.Fill(ds)
        'End If
        'If ds.Tables(0).Rows.Count = 0 Then
        '    Exit Sub
        'End If
        'For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
        '    dt.Rows.Add(ds.Tables(0).Rows(i).Item("DateOFFICE").ToString.Split(" ")(0), ds.Tables(0).Rows(i).Item("SHIFT").ToString, ds.Tables(0).Rows(i).Item("STATUS").ToString, ds.Tables(0).Rows(i).Item("LATEARRIVAL").ToString, ds.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString, ds.Tables(0).Rows(i).Item("IN1").ToString, ds.Tables(0).Rows(i).Item("OUT1").ToString, ds.Tables(0).Rows(i).Item("IN2").ToString, ds.Tables(0).Rows(i).Item("OUT2").ToString, ds.Tables(0).Rows(i).Item("OTDURATION").ToString)
        'Next
        'Dim datase As DataSet = New DataSet()
        'datase.Tables.Add(dt)
        'GridControl1.DataSource = dt

        Dim gridtblregisterselet As String
        If Common.servername = "Access" Then
            gridtblregisterselet = "select DateOFFICE, SHIFT, STATUS, LATEARRIVAL, EARLYDEPARTURE, IN1, OUT1, IN2, OUT2, OTDURATION ,[ManOTDuration],[OTApprove]  from tblTimeRegister where PAYCODE='" & LookUpEdit1.EditValue.ToString.Trim & "' and FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00') >='" & startdate.ToString("yyyy-MM-dd HH:mm:ss") & "' and FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00') < '" & enddate.ToString("yyyy-MM-dd HH:mm:ss") & "' order	BY DateOFFICE ASC "
        Else
            gridtblregisterselet = "select DateOFFICE, SHIFT, STATUS, LATEARRIVAL, EARLYDEPARTURE, IN1, OUT1, IN2, OUT2, OTDURATION ,[ManOTDuration],[OTApprove]  from tblTimeRegister where PAYCODE='" & LookUpEdit1.EditValue.ToString.Trim & "' and DateOFFICE >='" & startdate.ToString("yyyy-MM-dd HH:mm:ss") & "' and DateOFFICE <'" & enddate.ToString("yyyy-MM-dd HH:mm:ss") & "'"
        End If
        Dim WTDataTable As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
            WTDataTable = New DataTable("tblTimeRegister")
            dataAdapter.Fill(WTDataTable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
            WTDataTable = New DataTable("tblTimeRegister")
            dataAdapter.Fill(WTDataTable)
        End If
        GridControl1.DataSource = WTDataTable
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub TextEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles TextEdit1.Leave
        setSelectValue()
        setGridData()
    End Sub
    Private Sub ComboBoxEdit2_Leave(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit2.Leave
        setSelectValue()
        setGridData()
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        If Common.DataMaintenance = "N" Then
            e.Allow = False
            Exit Sub
        End If
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        DateEditAtt = row("DateOFFICE").ToString.Trim
        paycodeformaintaince = LookUpEdit1.EditValue.ToString.Trim
        cardnoformaintaince = lblCardNum.Text.Trim
        nameformaintaince = lblName.Text.Trim
        e.Allow = False
        'MsgBox(DateEditAtt)
        'XtraDataMaintenanceEdit.ShowDialog()
        setGridData()
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim view As ColumnView = TryCast(sender, ColumnView)
        'If e.Column.FieldName = "LATEARRIVAL" Then
        '    If row("LATEARRIVAL").ToString.Trim = "0" Or row("LATEARRIVAL").ToString.Trim = "" Then
        '        e.DisplayText = ""
        '    Else
        '        MsgBox(row("LATEARRIVAL").ToString.Trim)
        '        e.DisplayText = (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LATEARRIVAL").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LATEARRIVAL").ToString().Trim) Mod 60).ToString("00")
        '    End If
        'End If
        If e.Column.FieldName = "LATEARRIVAL" Then
            If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LATEARRIVAL").ToString().Trim = "0" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LATEARRIVAL").ToString().Trim = "" Then
                e.DisplayText = ""
            Else
                'MsgBox(row("LATEARRIVAL").ToString.Trim)
                e.DisplayText = (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LATEARRIVAL").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LATEARRIVAL").ToString().Trim) Mod 60).ToString("00")
            End If
        End If

        If e.Column.FieldName = "EARLYDEPARTURE" Then
            If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EARLYDEPARTURE").ToString().Trim = "0" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EARLYDEPARTURE").ToString().Trim = "" Then
                e.DisplayText = ""
            Else
                e.DisplayText = (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EARLYDEPARTURE").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EARLYDEPARTURE").ToString().Trim) Mod 60).ToString("00")
            End If
        End If

        If e.Column.FieldName = "OTDURATION" Then
            If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OTDURATION").ToString().Trim = "0" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OTDURATION").ToString().Trim = "" Then
                e.DisplayText = ""
            Else
                e.DisplayText = (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OTDURATION").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OTDURATION").ToString().Trim) Mod 60).ToString("00")
            End If
        End If

        If e.Column.FieldName = "ManOTDuration" Then
            If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ManOTDuration").ToString().Trim = "0" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ManOTDuration").ToString().Trim = "" Then
                e.DisplayText = ""
            Else
                e.DisplayText = (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ManOTDuration").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ManOTDuration").ToString().Trim) Mod 60).ToString("00")
            End If
        End If

        'If e.Column.FieldName = "HOURSWORKED" Then
        '    If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "HOURSWORKED").ToString().Trim = "0" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "HOURSWORKED").ToString().Trim = "" Then
        '        e.DisplayText = ""
        '    Else
        '        e.DisplayText = (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "HOURSWORKED").ToString().Trim) \ 60).ToString("00") & ":" & (Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "HOURSWORKED").ToString().Trim) Mod 60).ToString("00")
        '    End If
        'End If

        If Common.IsNepali = "Y" Then
            Me.Cursor = Cursors.WaitCursor
            If e.Column.FieldName = "DateOFFICE" Then
                If row("DateOFFICE").ToString.Trim <> "" Then
                    Dim DC As New DateConverter()
                    Dim dt As DateTime = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DateOFFICE").ToString().Trim)
                    Try
                        Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
                        Dim dojTmp() As String = Vstart.Split("-")
                        e.DisplayText = dojTmp(2) & "-" & ComboBoxEditNEpaliMonth.Properties.Items(dojTmp(1) - 1).ToString & "-" & dojTmp(0)
                    Catch ex As Exception
                    End Try
                End If
            End If
            Me.Cursor = Cursors.Default
        End If
    End Sub
    Private Sub ComboBoxEditNepaliYear_Leave(sender As System.Object, e As System.EventArgs) Handles ComboBoxEditNepaliYear.Leave
        setSelectValue()
        setGridData()
    End Sub
    Private Sub ComboBoxEditNEpaliMonth_Leave(sender As System.Object, e As System.EventArgs) Handles ComboBoxEditNEpaliMonth.Leave
        setSelectValue()
        setGridData()
    End Sub

    Private Sub SimpleButtonExcel_Click(sender As System.Object, e As System.EventArgs)
        XtraRosterUpload.ShowDialog()
    End Sub

    Private Sub GridView1_FocusedRowChanged(sender As Object, e As FocusedRowChangedEventArgs) Handles GridView1.FocusedRowChanged
        Dim Mot As String = GridView1.GetRowCellValue(e.FocusedRowHandle, "ManOTDuration").ToString.Trim
        If Mot = "" Or Mot = "0" Then
            TextMOT.Text = "00:00"
        Else
            TextMOT.Text = Convert.ToInt32(Mot \ 60).ToString("00") & ":" & Convert.ToInt32(Mot Mod 60).ToString("00")
        End If
    End Sub

    Private Sub SimpleButtonAdd_Click(sender As Object, e As EventArgs) Handles SimpleButtonAdd.Click
        Dim tmp() As String = TextMOT.Text.Trim.Split(":")
        Dim mot As Integer = tmp(0) * 60 + tmp(1)
        GridView1.SetFocusedRowCellValue("ManOTDuration", mot)
    End Sub

    Private Sub SimpleButtonSave_Click(sender As Object, e As EventArgs) Handles SimpleButtonSave.Click
        Dim tmpcount As Integer = 0
        For i As Integer = 0 To GridView1.DataRowCount - 1
            Dim Mot As String = GridView1.GetRowCellValue(i, "ManOTDuration").ToString.Trim
            If Mot <> "" Then
                tmpcount = tmpcount + 1
                Dim paycode As String = LookUpEdit1.EditValue.ToString.Trim
                Dim DateOFFICE As String = Convert.ToDateTime(GridView1.GetRowCellValue(i, "DateOFFICE").ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss")
                Dim ssql As String = "update tblTimeRegister set ManOTDuration='" & Mot & "', OTApprove='Y' where paycode ='" & paycode & "' and DateOFFICE='" & DateOFFICE & "'"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(ssql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        Next
        If tmpcount > 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>iAS</size>")
        End If
    End Sub
End Class
