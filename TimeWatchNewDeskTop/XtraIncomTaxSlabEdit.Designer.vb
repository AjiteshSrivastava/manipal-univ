﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraIncomTaxSlabEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraIncomTaxSlabEdit))
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditPercent = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditLowerLimit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditUpperLimit = New DevExpress.XtraEditors.TextEdit()
        Me.ComboBoxEditGender = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.TextEditPercent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditLowerLimit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditUpperLimit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditGender.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(68, 135)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 30
        Me.SimpleButtonSave.Text = "Save"
        '
        'TextEditPercent
        '
        Me.TextEditPercent.Location = New System.Drawing.Point(102, 75)
        Me.TextEditPercent.Name = "TextEditPercent"
        Me.TextEditPercent.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPercent.Properties.Appearance.Options.UseFont = True
        Me.TextEditPercent.Properties.Mask.EditMask = "^0*(100\.00|[0-9]?[0-9]\.[0-9]{2})$"
        Me.TextEditPercent.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditPercent.Properties.MaxLength = 6
        Me.TextEditPercent.Size = New System.Drawing.Size(156, 20)
        Me.TextEditPercent.TabIndex = 24
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(12, 77)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(43, 14)
        Me.LabelControl4.TabIndex = 26
        Me.LabelControl4.Text = "Percent"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(12, 51)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(62, 14)
        Me.LabelControl3.TabIndex = 23
        Me.LabelControl3.Text = "Upper Limit"
        '
        'TextEditLowerLimit
        '
        Me.TextEditLowerLimit.Location = New System.Drawing.Point(102, 23)
        Me.TextEditLowerLimit.Name = "TextEditLowerLimit"
        Me.TextEditLowerLimit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditLowerLimit.Properties.Appearance.Options.UseFont = True
        Me.TextEditLowerLimit.Properties.Mask.EditMask = "#########.##"
        Me.TextEditLowerLimit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditLowerLimit.Properties.MaxLength = 12
        Me.TextEditLowerLimit.Size = New System.Drawing.Size(156, 20)
        Me.TextEditLowerLimit.TabIndex = 20
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(13, 26)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl1.TabIndex = 19
        Me.LabelControl1.Text = "Lower Limit"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(149, 135)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 39
        Me.SimpleButton1.Text = "Close"
        '
        'TextEditUpperLimit
        '
        Me.TextEditUpperLimit.Location = New System.Drawing.Point(102, 49)
        Me.TextEditUpperLimit.Name = "TextEditUpperLimit"
        Me.TextEditUpperLimit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditUpperLimit.Properties.Appearance.Options.UseFont = True
        Me.TextEditUpperLimit.Properties.Mask.EditMask = "#########.##"
        Me.TextEditUpperLimit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditUpperLimit.Properties.MaxLength = 12
        Me.TextEditUpperLimit.Size = New System.Drawing.Size(156, 20)
        Me.TextEditUpperLimit.TabIndex = 40
        '
        'ComboBoxEditGender
        '
        Me.ComboBoxEditGender.EditValue = "Male"
        Me.ComboBoxEditGender.Location = New System.Drawing.Point(102, 101)
        Me.ComboBoxEditGender.Name = "ComboBoxEditGender"
        Me.ComboBoxEditGender.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditGender.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditGender.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditGender.Properties.Items.AddRange(New Object() {"Male", "Female"})
        Me.ComboBoxEditGender.Size = New System.Drawing.Size(100, 20)
        Me.ComboBoxEditGender.TabIndex = 42
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(13, 104)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(40, 14)
        Me.LabelControl23.TabIndex = 41
        Me.LabelControl23.Text = "Gender"
        '
        'XtraIncomTaxSlabEdit
        '
        Me.AcceptButton = Me.SimpleButtonSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 207)
        Me.ControlBox = False
        Me.Controls.Add(Me.ComboBoxEditGender)
        Me.Controls.Add(Me.LabelControl23)
        Me.Controls.Add(Me.TextEditUpperLimit)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.SimpleButtonSave)
        Me.Controls.Add(Me.TextEditPercent)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.TextEditLowerLimit)
        Me.Controls.Add(Me.LabelControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraIncomTaxSlabEdit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Income Tax Slab"
        CType(Me.TextEditPercent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditLowerLimit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditUpperLimit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditGender.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditPercent As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditLowerLimit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditUpperLimit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ComboBoxEditGender As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
End Class
