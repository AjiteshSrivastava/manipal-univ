﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraTimeZoneEditZK
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnTZCreate = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditTZCre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSunEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSunStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtMonEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtMonStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtTueEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtTueStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtWedEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtWedStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtThuEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtThuStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtFriEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtFriStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSatEndTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSatStrtTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.TxtSunStrtTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSunEndTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtMonStrtTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtMonEndTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtTueStrtTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtTueEndTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtWedStrtTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtWedEndTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtThuStrtTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtThuEndTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtFriStrtTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtFriEndTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSatStrtTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSatEndTime2 = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.TxtSunStrtTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSunEndTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtMonStrtTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtMonEndTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtTueStrtTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtTueEndTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtWedStrtTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtWedEndTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtThuStrtTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtThuEndTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtFriStrtTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtFriEndTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSatStrtTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtSatEndTime3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.TextEditTZCre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSunEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSunStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMonEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMonStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtTueEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtTueStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtWedEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtWedStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtThuEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtThuStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtFriEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtFriStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSatEndTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSatStrtTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.TxtSunStrtTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSunEndTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMonStrtTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMonEndTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtTueStrtTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtTueEndTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtWedStrtTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtWedEndTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtThuStrtTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtThuEndTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtFriStrtTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtFriEndTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSatStrtTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSatEndTime2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.TxtSunStrtTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSunEndTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMonStrtTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMonEndTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtTueStrtTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtTueEndTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtWedStrtTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtWedEndTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtThuStrtTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtThuEndTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtFriStrtTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtFriEndTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSatStrtTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtSatEndTime3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnTZCreate
        '
        Me.btnTZCreate.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnTZCreate.Appearance.Options.UseFont = True
        Me.btnTZCreate.Location = New System.Drawing.Point(884, 316)
        Me.btnTZCreate.Name = "btnTZCreate"
        Me.btnTZCreate.Size = New System.Drawing.Size(104, 23)
        Me.btnTZCreate.TabIndex = 16
        Me.btnTZCreate.Text = "Set TimeZone"
        '
        'TextEditTZCre
        '
        Me.TextEditTZCre.Location = New System.Drawing.Point(101, 12)
        Me.TextEditTZCre.Name = "TextEditTZCre"
        Me.TextEditTZCre.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditTZCre.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditTZCre.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditTZCre.Properties.MaxLength = 3
        Me.TextEditTZCre.Size = New System.Drawing.Size(53, 20)
        Me.TextEditTZCre.TabIndex = 1
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(10, 15)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl7.TabIndex = 72
        Me.LabelControl7.Text = "Time Zone ID 1"
        '
        'TxtSunEndTime
        '
        Me.TxtSunEndTime.EditValue = "23:59"
        Me.TxtSunEndTime.Location = New System.Drawing.Point(252, 16)
        Me.TxtSunEndTime.Name = "TxtSunEndTime"
        Me.TxtSunEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSunEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtSunEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSunEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSunEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSunEndTime.Properties.MaxLength = 5
        Me.TxtSunEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtSunEndTime.TabIndex = 3
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(224, 17)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl6.TabIndex = 70
        Me.LabelControl6.Text = "To"
        '
        'TxtSunStrtTime
        '
        Me.TxtSunStrtTime.EditValue = "00:00"
        Me.TxtSunStrtTime.Location = New System.Drawing.Point(106, 16)
        Me.TxtSunStrtTime.Name = "TxtSunStrtTime"
        Me.TxtSunStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSunStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtSunStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSunStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSunStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSunStrtTime.Properties.MaxLength = 5
        Me.TxtSunStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtSunStrtTime.TabIndex = 2
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(14, 19)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl5.TabIndex = 68
        Me.LabelControl5.Text = "SUN Start Time"
        '
        'TxtMonEndTime
        '
        Me.TxtMonEndTime.EditValue = "23:59"
        Me.TxtMonEndTime.Location = New System.Drawing.Point(252, 42)
        Me.TxtMonEndTime.Name = "TxtMonEndTime"
        Me.TxtMonEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMonEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtMonEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtMonEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtMonEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMonEndTime.Properties.MaxLength = 5
        Me.TxtMonEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtMonEndTime.TabIndex = 5
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(224, 43)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl1.TabIndex = 77
        Me.LabelControl1.Text = "To"
        '
        'TxtMonStrtTime
        '
        Me.TxtMonStrtTime.EditValue = "00:00"
        Me.TxtMonStrtTime.Location = New System.Drawing.Point(106, 42)
        Me.TxtMonStrtTime.Name = "TxtMonStrtTime"
        Me.TxtMonStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMonStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtMonStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtMonStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtMonStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMonStrtTime.Properties.MaxLength = 5
        Me.TxtMonStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtMonStrtTime.TabIndex = 4
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(14, 45)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl2.TabIndex = 75
        Me.LabelControl2.Text = "MON Start Time"
        '
        'TxtTueEndTime
        '
        Me.TxtTueEndTime.EditValue = "23:59"
        Me.TxtTueEndTime.Location = New System.Drawing.Point(252, 68)
        Me.TxtTueEndTime.Name = "TxtTueEndTime"
        Me.TxtTueEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtTueEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtTueEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtTueEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtTueEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtTueEndTime.Properties.MaxLength = 5
        Me.TxtTueEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtTueEndTime.TabIndex = 7
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(224, 69)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl3.TabIndex = 81
        Me.LabelControl3.Text = "To"
        '
        'TxtTueStrtTime
        '
        Me.TxtTueStrtTime.EditValue = "00:00"
        Me.TxtTueStrtTime.Location = New System.Drawing.Point(106, 68)
        Me.TxtTueStrtTime.Name = "TxtTueStrtTime"
        Me.TxtTueStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtTueStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtTueStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtTueStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtTueStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtTueStrtTime.Properties.MaxLength = 5
        Me.TxtTueStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtTueStrtTime.TabIndex = 6
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(14, 71)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl4.TabIndex = 79
        Me.LabelControl4.Text = "TUE Start Time"
        '
        'TxtWedEndTime
        '
        Me.TxtWedEndTime.EditValue = "23:59"
        Me.TxtWedEndTime.Location = New System.Drawing.Point(252, 94)
        Me.TxtWedEndTime.Name = "TxtWedEndTime"
        Me.TxtWedEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtWedEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtWedEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtWedEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtWedEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtWedEndTime.Properties.MaxLength = 5
        Me.TxtWedEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtWedEndTime.TabIndex = 9
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(224, 95)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl8.TabIndex = 85
        Me.LabelControl8.Text = "To"
        '
        'TxtWedStrtTime
        '
        Me.TxtWedStrtTime.EditValue = "00:00"
        Me.TxtWedStrtTime.Location = New System.Drawing.Point(106, 94)
        Me.TxtWedStrtTime.Name = "TxtWedStrtTime"
        Me.TxtWedStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtWedStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtWedStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtWedStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtWedStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtWedStrtTime.Properties.MaxLength = 5
        Me.TxtWedStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtWedStrtTime.TabIndex = 8
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(14, 97)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(89, 14)
        Me.LabelControl9.TabIndex = 83
        Me.LabelControl9.Text = "WED Start Time"
        '
        'TxtThuEndTime
        '
        Me.TxtThuEndTime.EditValue = "23:59"
        Me.TxtThuEndTime.Location = New System.Drawing.Point(252, 120)
        Me.TxtThuEndTime.Name = "TxtThuEndTime"
        Me.TxtThuEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtThuEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtThuEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtThuEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtThuEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtThuEndTime.Properties.MaxLength = 5
        Me.TxtThuEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtThuEndTime.TabIndex = 11
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(224, 121)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl10.TabIndex = 89
        Me.LabelControl10.Text = "To"
        '
        'TxtThuStrtTime
        '
        Me.TxtThuStrtTime.EditValue = "00:00"
        Me.TxtThuStrtTime.Location = New System.Drawing.Point(106, 120)
        Me.TxtThuStrtTime.Name = "TxtThuStrtTime"
        Me.TxtThuStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtThuStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtThuStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtThuStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtThuStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtThuStrtTime.Properties.MaxLength = 5
        Me.TxtThuStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtThuStrtTime.TabIndex = 10
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(14, 123)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl11.TabIndex = 87
        Me.LabelControl11.Text = "THU Start Time"
        '
        'TxtFriEndTime
        '
        Me.TxtFriEndTime.EditValue = "23:59"
        Me.TxtFriEndTime.Location = New System.Drawing.Point(252, 146)
        Me.TxtFriEndTime.Name = "TxtFriEndTime"
        Me.TxtFriEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtFriEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtFriEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtFriEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtFriEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtFriEndTime.Properties.MaxLength = 5
        Me.TxtFriEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtFriEndTime.TabIndex = 13
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(224, 147)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl12.TabIndex = 93
        Me.LabelControl12.Text = "To"
        '
        'TxtFriStrtTime
        '
        Me.TxtFriStrtTime.EditValue = "00:00"
        Me.TxtFriStrtTime.Location = New System.Drawing.Point(106, 146)
        Me.TxtFriStrtTime.Name = "TxtFriStrtTime"
        Me.TxtFriStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtFriStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtFriStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtFriStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtFriStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtFriStrtTime.Properties.MaxLength = 5
        Me.TxtFriStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtFriStrtTime.TabIndex = 12
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(14, 149)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(79, 14)
        Me.LabelControl13.TabIndex = 91
        Me.LabelControl13.Text = "FRI Start Time"
        '
        'TxtSatEndTime
        '
        Me.TxtSatEndTime.EditValue = "23:59"
        Me.TxtSatEndTime.Location = New System.Drawing.Point(252, 172)
        Me.TxtSatEndTime.Name = "TxtSatEndTime"
        Me.TxtSatEndTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSatEndTime.Properties.Appearance.Options.UseFont = True
        Me.TxtSatEndTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSatEndTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSatEndTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSatEndTime.Properties.MaxLength = 5
        Me.TxtSatEndTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtSatEndTime.TabIndex = 15
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(224, 173)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl14.TabIndex = 97
        Me.LabelControl14.Text = "To"
        '
        'TxtSatStrtTime
        '
        Me.TxtSatStrtTime.EditValue = "00:00"
        Me.TxtSatStrtTime.Location = New System.Drawing.Point(106, 172)
        Me.TxtSatStrtTime.Name = "TxtSatStrtTime"
        Me.TxtSatStrtTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSatStrtTime.Properties.Appearance.Options.UseFont = True
        Me.TxtSatStrtTime.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSatStrtTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSatStrtTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSatStrtTime.Properties.MaxLength = 5
        Me.TxtSatStrtTime.Size = New System.Drawing.Size(72, 20)
        Me.TxtSatStrtTime.TabIndex = 14
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(14, 175)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl15.TabIndex = 95
        Me.LabelControl15.Text = "SAT Start Time"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(994, 316)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(68, 23)
        Me.SimpleButton1.TabIndex = 17
        Me.SimpleButton1.Text = "Close"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.TxtSunStrtTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.TxtSunEndTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.TxtMonStrtTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.TxtMonEndTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.TxtTueStrtTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.TxtTueEndTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.TxtWedStrtTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.TxtWedEndTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.TxtThuStrtTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.TxtThuEndTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.TxtFriStrtTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.TxtFriEndTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl15)
        Me.PanelControl1.Controls.Add(Me.TxtSatStrtTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl14)
        Me.PanelControl1.Controls.Add(Me.TxtSatEndTime)
        Me.PanelControl1.Location = New System.Drawing.Point(14, 77)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(344, 213)
        Me.PanelControl1.TabIndex = 154
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.TxtSunStrtTime2)
        Me.PanelControl2.Controls.Add(Me.LabelControl16)
        Me.PanelControl2.Controls.Add(Me.LabelControl17)
        Me.PanelControl2.Controls.Add(Me.TxtSunEndTime2)
        Me.PanelControl2.Controls.Add(Me.LabelControl18)
        Me.PanelControl2.Controls.Add(Me.TxtMonStrtTime2)
        Me.PanelControl2.Controls.Add(Me.LabelControl19)
        Me.PanelControl2.Controls.Add(Me.TxtMonEndTime2)
        Me.PanelControl2.Controls.Add(Me.LabelControl20)
        Me.PanelControl2.Controls.Add(Me.TxtTueStrtTime2)
        Me.PanelControl2.Controls.Add(Me.LabelControl21)
        Me.PanelControl2.Controls.Add(Me.TxtTueEndTime2)
        Me.PanelControl2.Controls.Add(Me.LabelControl22)
        Me.PanelControl2.Controls.Add(Me.TxtWedStrtTime2)
        Me.PanelControl2.Controls.Add(Me.LabelControl23)
        Me.PanelControl2.Controls.Add(Me.TxtWedEndTime2)
        Me.PanelControl2.Controls.Add(Me.LabelControl24)
        Me.PanelControl2.Controls.Add(Me.TxtThuStrtTime2)
        Me.PanelControl2.Controls.Add(Me.LabelControl25)
        Me.PanelControl2.Controls.Add(Me.TxtThuEndTime2)
        Me.PanelControl2.Controls.Add(Me.LabelControl26)
        Me.PanelControl2.Controls.Add(Me.TxtFriStrtTime2)
        Me.PanelControl2.Controls.Add(Me.LabelControl27)
        Me.PanelControl2.Controls.Add(Me.TxtFriEndTime2)
        Me.PanelControl2.Controls.Add(Me.LabelControl28)
        Me.PanelControl2.Controls.Add(Me.TxtSatStrtTime2)
        Me.PanelControl2.Controls.Add(Me.LabelControl29)
        Me.PanelControl2.Controls.Add(Me.TxtSatEndTime2)
        Me.PanelControl2.Location = New System.Drawing.Point(364, 77)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(344, 213)
        Me.PanelControl2.TabIndex = 155
        '
        'TxtSunStrtTime2
        '
        Me.TxtSunStrtTime2.EditValue = "00:00"
        Me.TxtSunStrtTime2.Location = New System.Drawing.Point(106, 16)
        Me.TxtSunStrtTime2.Name = "TxtSunStrtTime2"
        Me.TxtSunStrtTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSunStrtTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtSunStrtTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSunStrtTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSunStrtTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSunStrtTime2.Properties.MaxLength = 5
        Me.TxtSunStrtTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtSunStrtTime2.TabIndex = 2
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(14, 19)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl16.TabIndex = 68
        Me.LabelControl16.Text = "SUN Start Time"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(224, 17)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl17.TabIndex = 70
        Me.LabelControl17.Text = "To"
        '
        'TxtSunEndTime2
        '
        Me.TxtSunEndTime2.EditValue = "23:59"
        Me.TxtSunEndTime2.Location = New System.Drawing.Point(252, 16)
        Me.TxtSunEndTime2.Name = "TxtSunEndTime2"
        Me.TxtSunEndTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSunEndTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtSunEndTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSunEndTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSunEndTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSunEndTime2.Properties.MaxLength = 5
        Me.TxtSunEndTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtSunEndTime2.TabIndex = 3
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(14, 45)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl18.TabIndex = 75
        Me.LabelControl18.Text = "MON Start Time"
        '
        'TxtMonStrtTime2
        '
        Me.TxtMonStrtTime2.EditValue = "00:00"
        Me.TxtMonStrtTime2.Location = New System.Drawing.Point(106, 42)
        Me.TxtMonStrtTime2.Name = "TxtMonStrtTime2"
        Me.TxtMonStrtTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMonStrtTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtMonStrtTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtMonStrtTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtMonStrtTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMonStrtTime2.Properties.MaxLength = 5
        Me.TxtMonStrtTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtMonStrtTime2.TabIndex = 4
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(224, 43)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl19.TabIndex = 77
        Me.LabelControl19.Text = "To"
        '
        'TxtMonEndTime2
        '
        Me.TxtMonEndTime2.EditValue = "23:59"
        Me.TxtMonEndTime2.Location = New System.Drawing.Point(252, 42)
        Me.TxtMonEndTime2.Name = "TxtMonEndTime2"
        Me.TxtMonEndTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMonEndTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtMonEndTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtMonEndTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtMonEndTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMonEndTime2.Properties.MaxLength = 5
        Me.TxtMonEndTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtMonEndTime2.TabIndex = 5
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Location = New System.Drawing.Point(14, 71)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl20.TabIndex = 79
        Me.LabelControl20.Text = "TUE Start Time"
        '
        'TxtTueStrtTime2
        '
        Me.TxtTueStrtTime2.EditValue = "00:00"
        Me.TxtTueStrtTime2.Location = New System.Drawing.Point(106, 68)
        Me.TxtTueStrtTime2.Name = "TxtTueStrtTime2"
        Me.TxtTueStrtTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtTueStrtTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtTueStrtTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtTueStrtTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtTueStrtTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtTueStrtTime2.Properties.MaxLength = 5
        Me.TxtTueStrtTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtTueStrtTime2.TabIndex = 6
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(224, 69)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl21.TabIndex = 81
        Me.LabelControl21.Text = "To"
        '
        'TxtTueEndTime2
        '
        Me.TxtTueEndTime2.EditValue = "23:59"
        Me.TxtTueEndTime2.Location = New System.Drawing.Point(252, 68)
        Me.TxtTueEndTime2.Name = "TxtTueEndTime2"
        Me.TxtTueEndTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtTueEndTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtTueEndTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtTueEndTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtTueEndTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtTueEndTime2.Properties.MaxLength = 5
        Me.TxtTueEndTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtTueEndTime2.TabIndex = 7
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Location = New System.Drawing.Point(14, 97)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(89, 14)
        Me.LabelControl22.TabIndex = 83
        Me.LabelControl22.Text = "WED Start Time"
        '
        'TxtWedStrtTime2
        '
        Me.TxtWedStrtTime2.EditValue = "00:00"
        Me.TxtWedStrtTime2.Location = New System.Drawing.Point(106, 94)
        Me.TxtWedStrtTime2.Name = "TxtWedStrtTime2"
        Me.TxtWedStrtTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtWedStrtTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtWedStrtTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtWedStrtTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtWedStrtTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtWedStrtTime2.Properties.MaxLength = 5
        Me.TxtWedStrtTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtWedStrtTime2.TabIndex = 8
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(224, 95)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl23.TabIndex = 85
        Me.LabelControl23.Text = "To"
        '
        'TxtWedEndTime2
        '
        Me.TxtWedEndTime2.EditValue = "23:59"
        Me.TxtWedEndTime2.Location = New System.Drawing.Point(252, 94)
        Me.TxtWedEndTime2.Name = "TxtWedEndTime2"
        Me.TxtWedEndTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtWedEndTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtWedEndTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtWedEndTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtWedEndTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtWedEndTime2.Properties.MaxLength = 5
        Me.TxtWedEndTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtWedEndTime2.TabIndex = 9
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl24.Appearance.Options.UseFont = True
        Me.LabelControl24.Location = New System.Drawing.Point(14, 123)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl24.TabIndex = 87
        Me.LabelControl24.Text = "THU Start Time"
        '
        'TxtThuStrtTime2
        '
        Me.TxtThuStrtTime2.EditValue = "00:00"
        Me.TxtThuStrtTime2.Location = New System.Drawing.Point(106, 120)
        Me.TxtThuStrtTime2.Name = "TxtThuStrtTime2"
        Me.TxtThuStrtTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtThuStrtTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtThuStrtTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtThuStrtTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtThuStrtTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtThuStrtTime2.Properties.MaxLength = 5
        Me.TxtThuStrtTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtThuStrtTime2.TabIndex = 10
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl25.Appearance.Options.UseFont = True
        Me.LabelControl25.Location = New System.Drawing.Point(224, 121)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl25.TabIndex = 89
        Me.LabelControl25.Text = "To"
        '
        'TxtThuEndTime2
        '
        Me.TxtThuEndTime2.EditValue = "23:59"
        Me.TxtThuEndTime2.Location = New System.Drawing.Point(252, 120)
        Me.TxtThuEndTime2.Name = "TxtThuEndTime2"
        Me.TxtThuEndTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtThuEndTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtThuEndTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtThuEndTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtThuEndTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtThuEndTime2.Properties.MaxLength = 5
        Me.TxtThuEndTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtThuEndTime2.TabIndex = 11
        '
        'LabelControl26
        '
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl26.Appearance.Options.UseFont = True
        Me.LabelControl26.Location = New System.Drawing.Point(14, 149)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(79, 14)
        Me.LabelControl26.TabIndex = 91
        Me.LabelControl26.Text = "FRI Start Time"
        '
        'TxtFriStrtTime2
        '
        Me.TxtFriStrtTime2.EditValue = "00:00"
        Me.TxtFriStrtTime2.Location = New System.Drawing.Point(106, 146)
        Me.TxtFriStrtTime2.Name = "TxtFriStrtTime2"
        Me.TxtFriStrtTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtFriStrtTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtFriStrtTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtFriStrtTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtFriStrtTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtFriStrtTime2.Properties.MaxLength = 5
        Me.TxtFriStrtTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtFriStrtTime2.TabIndex = 12
        '
        'LabelControl27
        '
        Me.LabelControl27.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl27.Appearance.Options.UseFont = True
        Me.LabelControl27.Location = New System.Drawing.Point(224, 147)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl27.TabIndex = 93
        Me.LabelControl27.Text = "To"
        '
        'TxtFriEndTime2
        '
        Me.TxtFriEndTime2.EditValue = "23:59"
        Me.TxtFriEndTime2.Location = New System.Drawing.Point(252, 146)
        Me.TxtFriEndTime2.Name = "TxtFriEndTime2"
        Me.TxtFriEndTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtFriEndTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtFriEndTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtFriEndTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtFriEndTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtFriEndTime2.Properties.MaxLength = 5
        Me.TxtFriEndTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtFriEndTime2.TabIndex = 13
        '
        'LabelControl28
        '
        Me.LabelControl28.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl28.Appearance.Options.UseFont = True
        Me.LabelControl28.Location = New System.Drawing.Point(14, 175)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl28.TabIndex = 95
        Me.LabelControl28.Text = "SAT Start Time"
        '
        'TxtSatStrtTime2
        '
        Me.TxtSatStrtTime2.EditValue = "00:00"
        Me.TxtSatStrtTime2.Location = New System.Drawing.Point(106, 172)
        Me.TxtSatStrtTime2.Name = "TxtSatStrtTime2"
        Me.TxtSatStrtTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSatStrtTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtSatStrtTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSatStrtTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSatStrtTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSatStrtTime2.Properties.MaxLength = 5
        Me.TxtSatStrtTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtSatStrtTime2.TabIndex = 14
        '
        'LabelControl29
        '
        Me.LabelControl29.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl29.Appearance.Options.UseFont = True
        Me.LabelControl29.Location = New System.Drawing.Point(224, 173)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl29.TabIndex = 97
        Me.LabelControl29.Text = "To"
        '
        'TxtSatEndTime2
        '
        Me.TxtSatEndTime2.EditValue = "23:59"
        Me.TxtSatEndTime2.Location = New System.Drawing.Point(252, 172)
        Me.TxtSatEndTime2.Name = "TxtSatEndTime2"
        Me.TxtSatEndTime2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSatEndTime2.Properties.Appearance.Options.UseFont = True
        Me.TxtSatEndTime2.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSatEndTime2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSatEndTime2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSatEndTime2.Properties.MaxLength = 5
        Me.TxtSatEndTime2.Size = New System.Drawing.Size(72, 20)
        Me.TxtSatEndTime2.TabIndex = 15
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.TxtSunStrtTime3)
        Me.PanelControl3.Controls.Add(Me.LabelControl30)
        Me.PanelControl3.Controls.Add(Me.LabelControl31)
        Me.PanelControl3.Controls.Add(Me.TxtSunEndTime3)
        Me.PanelControl3.Controls.Add(Me.LabelControl32)
        Me.PanelControl3.Controls.Add(Me.TxtMonStrtTime3)
        Me.PanelControl3.Controls.Add(Me.LabelControl33)
        Me.PanelControl3.Controls.Add(Me.TxtMonEndTime3)
        Me.PanelControl3.Controls.Add(Me.LabelControl34)
        Me.PanelControl3.Controls.Add(Me.TxtTueStrtTime3)
        Me.PanelControl3.Controls.Add(Me.LabelControl35)
        Me.PanelControl3.Controls.Add(Me.TxtTueEndTime3)
        Me.PanelControl3.Controls.Add(Me.LabelControl36)
        Me.PanelControl3.Controls.Add(Me.TxtWedStrtTime3)
        Me.PanelControl3.Controls.Add(Me.LabelControl37)
        Me.PanelControl3.Controls.Add(Me.TxtWedEndTime3)
        Me.PanelControl3.Controls.Add(Me.LabelControl38)
        Me.PanelControl3.Controls.Add(Me.TxtThuStrtTime3)
        Me.PanelControl3.Controls.Add(Me.LabelControl39)
        Me.PanelControl3.Controls.Add(Me.TxtThuEndTime3)
        Me.PanelControl3.Controls.Add(Me.LabelControl40)
        Me.PanelControl3.Controls.Add(Me.TxtFriStrtTime3)
        Me.PanelControl3.Controls.Add(Me.LabelControl41)
        Me.PanelControl3.Controls.Add(Me.TxtFriEndTime3)
        Me.PanelControl3.Controls.Add(Me.LabelControl42)
        Me.PanelControl3.Controls.Add(Me.TxtSatStrtTime3)
        Me.PanelControl3.Controls.Add(Me.LabelControl43)
        Me.PanelControl3.Controls.Add(Me.TxtSatEndTime3)
        Me.PanelControl3.Location = New System.Drawing.Point(714, 77)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(344, 213)
        Me.PanelControl3.TabIndex = 156
        '
        'TxtSunStrtTime3
        '
        Me.TxtSunStrtTime3.EditValue = "00:00"
        Me.TxtSunStrtTime3.Location = New System.Drawing.Point(106, 16)
        Me.TxtSunStrtTime3.Name = "TxtSunStrtTime3"
        Me.TxtSunStrtTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSunStrtTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtSunStrtTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSunStrtTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSunStrtTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSunStrtTime3.Properties.MaxLength = 5
        Me.TxtSunStrtTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtSunStrtTime3.TabIndex = 2
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl30.Appearance.Options.UseFont = True
        Me.LabelControl30.Location = New System.Drawing.Point(14, 19)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl30.TabIndex = 68
        Me.LabelControl30.Text = "SUN Start Time"
        '
        'LabelControl31
        '
        Me.LabelControl31.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl31.Appearance.Options.UseFont = True
        Me.LabelControl31.Location = New System.Drawing.Point(224, 17)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl31.TabIndex = 70
        Me.LabelControl31.Text = "To"
        '
        'TxtSunEndTime3
        '
        Me.TxtSunEndTime3.EditValue = "23:59"
        Me.TxtSunEndTime3.Location = New System.Drawing.Point(252, 16)
        Me.TxtSunEndTime3.Name = "TxtSunEndTime3"
        Me.TxtSunEndTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSunEndTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtSunEndTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSunEndTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSunEndTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSunEndTime3.Properties.MaxLength = 5
        Me.TxtSunEndTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtSunEndTime3.TabIndex = 3
        '
        'LabelControl32
        '
        Me.LabelControl32.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl32.Appearance.Options.UseFont = True
        Me.LabelControl32.Location = New System.Drawing.Point(14, 45)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl32.TabIndex = 75
        Me.LabelControl32.Text = "MON Start Time"
        '
        'TxtMonStrtTime3
        '
        Me.TxtMonStrtTime3.EditValue = "00:00"
        Me.TxtMonStrtTime3.Location = New System.Drawing.Point(106, 42)
        Me.TxtMonStrtTime3.Name = "TxtMonStrtTime3"
        Me.TxtMonStrtTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMonStrtTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtMonStrtTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtMonStrtTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtMonStrtTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMonStrtTime3.Properties.MaxLength = 5
        Me.TxtMonStrtTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtMonStrtTime3.TabIndex = 4
        '
        'LabelControl33
        '
        Me.LabelControl33.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl33.Appearance.Options.UseFont = True
        Me.LabelControl33.Location = New System.Drawing.Point(224, 43)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl33.TabIndex = 77
        Me.LabelControl33.Text = "To"
        '
        'TxtMonEndTime3
        '
        Me.TxtMonEndTime3.EditValue = "23:59"
        Me.TxtMonEndTime3.Location = New System.Drawing.Point(252, 42)
        Me.TxtMonEndTime3.Name = "TxtMonEndTime3"
        Me.TxtMonEndTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMonEndTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtMonEndTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtMonEndTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtMonEndTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMonEndTime3.Properties.MaxLength = 5
        Me.TxtMonEndTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtMonEndTime3.TabIndex = 5
        '
        'LabelControl34
        '
        Me.LabelControl34.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl34.Appearance.Options.UseFont = True
        Me.LabelControl34.Location = New System.Drawing.Point(14, 71)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl34.TabIndex = 79
        Me.LabelControl34.Text = "TUE Start Time"
        '
        'TxtTueStrtTime3
        '
        Me.TxtTueStrtTime3.EditValue = "00:00"
        Me.TxtTueStrtTime3.Location = New System.Drawing.Point(106, 68)
        Me.TxtTueStrtTime3.Name = "TxtTueStrtTime3"
        Me.TxtTueStrtTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtTueStrtTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtTueStrtTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtTueStrtTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtTueStrtTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtTueStrtTime3.Properties.MaxLength = 5
        Me.TxtTueStrtTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtTueStrtTime3.TabIndex = 6
        '
        'LabelControl35
        '
        Me.LabelControl35.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl35.Appearance.Options.UseFont = True
        Me.LabelControl35.Location = New System.Drawing.Point(224, 69)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl35.TabIndex = 81
        Me.LabelControl35.Text = "To"
        '
        'TxtTueEndTime3
        '
        Me.TxtTueEndTime3.EditValue = "23:59"
        Me.TxtTueEndTime3.Location = New System.Drawing.Point(252, 68)
        Me.TxtTueEndTime3.Name = "TxtTueEndTime3"
        Me.TxtTueEndTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtTueEndTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtTueEndTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtTueEndTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtTueEndTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtTueEndTime3.Properties.MaxLength = 5
        Me.TxtTueEndTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtTueEndTime3.TabIndex = 7
        '
        'LabelControl36
        '
        Me.LabelControl36.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl36.Appearance.Options.UseFont = True
        Me.LabelControl36.Location = New System.Drawing.Point(14, 97)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(89, 14)
        Me.LabelControl36.TabIndex = 83
        Me.LabelControl36.Text = "WED Start Time"
        '
        'TxtWedStrtTime3
        '
        Me.TxtWedStrtTime3.EditValue = "00:00"
        Me.TxtWedStrtTime3.Location = New System.Drawing.Point(106, 94)
        Me.TxtWedStrtTime3.Name = "TxtWedStrtTime3"
        Me.TxtWedStrtTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtWedStrtTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtWedStrtTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtWedStrtTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtWedStrtTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtWedStrtTime3.Properties.MaxLength = 5
        Me.TxtWedStrtTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtWedStrtTime3.TabIndex = 8
        '
        'LabelControl37
        '
        Me.LabelControl37.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl37.Appearance.Options.UseFont = True
        Me.LabelControl37.Location = New System.Drawing.Point(224, 95)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl37.TabIndex = 85
        Me.LabelControl37.Text = "To"
        '
        'TxtWedEndTime3
        '
        Me.TxtWedEndTime3.EditValue = "23:59"
        Me.TxtWedEndTime3.Location = New System.Drawing.Point(252, 94)
        Me.TxtWedEndTime3.Name = "TxtWedEndTime3"
        Me.TxtWedEndTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtWedEndTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtWedEndTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtWedEndTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtWedEndTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtWedEndTime3.Properties.MaxLength = 5
        Me.TxtWedEndTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtWedEndTime3.TabIndex = 9
        '
        'LabelControl38
        '
        Me.LabelControl38.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl38.Appearance.Options.UseFont = True
        Me.LabelControl38.Location = New System.Drawing.Point(14, 123)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl38.TabIndex = 87
        Me.LabelControl38.Text = "THU Start Time"
        '
        'TxtThuStrtTime3
        '
        Me.TxtThuStrtTime3.EditValue = "00:00"
        Me.TxtThuStrtTime3.Location = New System.Drawing.Point(106, 120)
        Me.TxtThuStrtTime3.Name = "TxtThuStrtTime3"
        Me.TxtThuStrtTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtThuStrtTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtThuStrtTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtThuStrtTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtThuStrtTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtThuStrtTime3.Properties.MaxLength = 5
        Me.TxtThuStrtTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtThuStrtTime3.TabIndex = 10
        '
        'LabelControl39
        '
        Me.LabelControl39.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl39.Appearance.Options.UseFont = True
        Me.LabelControl39.Location = New System.Drawing.Point(224, 121)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl39.TabIndex = 89
        Me.LabelControl39.Text = "To"
        '
        'TxtThuEndTime3
        '
        Me.TxtThuEndTime3.EditValue = "23:59"
        Me.TxtThuEndTime3.Location = New System.Drawing.Point(252, 120)
        Me.TxtThuEndTime3.Name = "TxtThuEndTime3"
        Me.TxtThuEndTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtThuEndTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtThuEndTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtThuEndTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtThuEndTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtThuEndTime3.Properties.MaxLength = 5
        Me.TxtThuEndTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtThuEndTime3.TabIndex = 11
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl40.Appearance.Options.UseFont = True
        Me.LabelControl40.Location = New System.Drawing.Point(14, 149)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(79, 14)
        Me.LabelControl40.TabIndex = 91
        Me.LabelControl40.Text = "FRI Start Time"
        '
        'TxtFriStrtTime3
        '
        Me.TxtFriStrtTime3.EditValue = "00:00"
        Me.TxtFriStrtTime3.Location = New System.Drawing.Point(106, 146)
        Me.TxtFriStrtTime3.Name = "TxtFriStrtTime3"
        Me.TxtFriStrtTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtFriStrtTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtFriStrtTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtFriStrtTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtFriStrtTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtFriStrtTime3.Properties.MaxLength = 5
        Me.TxtFriStrtTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtFriStrtTime3.TabIndex = 12
        '
        'LabelControl41
        '
        Me.LabelControl41.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl41.Appearance.Options.UseFont = True
        Me.LabelControl41.Location = New System.Drawing.Point(224, 147)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl41.TabIndex = 93
        Me.LabelControl41.Text = "To"
        '
        'TxtFriEndTime3
        '
        Me.TxtFriEndTime3.EditValue = "23:59"
        Me.TxtFriEndTime3.Location = New System.Drawing.Point(252, 146)
        Me.TxtFriEndTime3.Name = "TxtFriEndTime3"
        Me.TxtFriEndTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtFriEndTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtFriEndTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtFriEndTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtFriEndTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtFriEndTime3.Properties.MaxLength = 5
        Me.TxtFriEndTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtFriEndTime3.TabIndex = 13
        '
        'LabelControl42
        '
        Me.LabelControl42.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl42.Appearance.Options.UseFont = True
        Me.LabelControl42.Location = New System.Drawing.Point(14, 175)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl42.TabIndex = 95
        Me.LabelControl42.Text = "SAT Start Time"
        '
        'TxtSatStrtTime3
        '
        Me.TxtSatStrtTime3.EditValue = "00:00"
        Me.TxtSatStrtTime3.Location = New System.Drawing.Point(106, 172)
        Me.TxtSatStrtTime3.Name = "TxtSatStrtTime3"
        Me.TxtSatStrtTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSatStrtTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtSatStrtTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSatStrtTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSatStrtTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSatStrtTime3.Properties.MaxLength = 5
        Me.TxtSatStrtTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtSatStrtTime3.TabIndex = 14
        '
        'LabelControl43
        '
        Me.LabelControl43.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl43.Appearance.Options.UseFont = True
        Me.LabelControl43.Location = New System.Drawing.Point(224, 173)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl43.TabIndex = 97
        Me.LabelControl43.Text = "To"
        '
        'TxtSatEndTime3
        '
        Me.TxtSatEndTime3.EditValue = "23:59"
        Me.TxtSatEndTime3.Location = New System.Drawing.Point(252, 172)
        Me.TxtSatEndTime3.Name = "TxtSatEndTime3"
        Me.TxtSatEndTime3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtSatEndTime3.Properties.Appearance.Options.UseFont = True
        Me.TxtSatEndTime3.Properties.Mask.EditMask = "HH:mm"
        Me.TxtSatEndTime3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtSatEndTime3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtSatEndTime3.Properties.MaxLength = 5
        Me.TxtSatEndTime3.Size = New System.Drawing.Size(72, 20)
        Me.TxtSatEndTime3.TabIndex = 15
        '
        'LabelControl44
        '
        Me.LabelControl44.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl44.Appearance.Options.UseFont = True
        Me.LabelControl44.Location = New System.Drawing.Point(21, 57)
        Me.LabelControl44.Name = "LabelControl44"
        Me.LabelControl44.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl44.TabIndex = 157
        Me.LabelControl44.Text = "Time Slab 1"
        '
        'LabelControl45
        '
        Me.LabelControl45.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl45.Appearance.Options.UseFont = True
        Me.LabelControl45.Location = New System.Drawing.Point(364, 57)
        Me.LabelControl45.Name = "LabelControl45"
        Me.LabelControl45.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl45.TabIndex = 158
        Me.LabelControl45.Text = "Time Slab 2"
        '
        'LabelControl46
        '
        Me.LabelControl46.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl46.Appearance.Options.UseFont = True
        Me.LabelControl46.Location = New System.Drawing.Point(714, 57)
        Me.LabelControl46.Name = "LabelControl46"
        Me.LabelControl46.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl46.TabIndex = 159
        Me.LabelControl46.Text = "Time Slab 3"
        '
        'XtraTimeZoneEditZK
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1078, 351)
        Me.Controls.Add(Me.LabelControl46)
        Me.Controls.Add(Me.LabelControl45)
        Me.Controls.Add(Me.LabelControl44)
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.btnTZCreate)
        Me.Controls.Add(Me.TextEditTZCre)
        Me.Controls.Add(Me.LabelControl7)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraTimeZoneEditZK"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.TextEditTZCre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSunEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSunStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMonEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMonStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtTueEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtTueStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtWedEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtWedStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtThuEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtThuStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtFriEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtFriStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSatEndTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSatStrtTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.TxtSunStrtTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSunEndTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMonStrtTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMonEndTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtTueStrtTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtTueEndTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtWedStrtTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtWedEndTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtThuStrtTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtThuEndTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtFriStrtTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtFriEndTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSatStrtTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSatEndTime2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.TxtSunStrtTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSunEndTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMonStrtTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMonEndTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtTueStrtTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtTueEndTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtWedStrtTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtWedEndTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtThuStrtTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtThuEndTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtFriStrtTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtFriEndTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSatStrtTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtSatEndTime3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnTZCreate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditTZCre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSunEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSunStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtMonEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtMonStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtTueEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtTueStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtWedEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtWedStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtThuEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtThuStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtFriEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtFriStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSatEndTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSatStrtTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TxtSunStrtTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSunEndTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtMonStrtTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtMonEndTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtTueStrtTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtTueEndTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtWedStrtTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtWedEndTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtThuStrtTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtThuEndTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtFriStrtTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtFriEndTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSatStrtTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSatEndTime2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TxtSunStrtTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSunEndTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtMonStrtTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtMonEndTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtTueStrtTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtTueEndTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtWedStrtTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtWedEndTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtThuStrtTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtThuEndTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtFriStrtTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtFriEndTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSatStrtTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtSatEndTime3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
End Class
