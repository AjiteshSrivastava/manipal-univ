﻿Imports System.IO
Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors

Public Class XtraEmployeePayroll
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Shared EmpId As String
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraEmployeePayroll_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'Me.Width = My.Computer.Screen.WorkingArea.Width
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100

        'not to do in all pages
        Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        Common.SplitterPosition = SplitContainerControl1.SplitterPosition
        setEmpGrid()
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                'Me.Validate()
                'e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Me.Validate()
                e.Handled = True
                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim PAYCODE As String = row("PAYCODE").ToString.Trim
                Dim sSql As String = "Delete from Pay_Master WHERE PAYCODE='" & PAYCODE & "'"
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()

                    cmd1 = New OleDbCommand("DELETE from PAY_RESULT where PAYCODE = '" & PAYCODE & "'", Common.con1)
                    cmd1.ExecuteNonQuery()

                    cmd1 = New OleDbCommand("DELETE from PAY_REIMURSH where PAYCODE = '" & PAYCODE & "'", Common.con1)
                    cmd1.ExecuteNonQuery()

                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()

                    cmd = New SqlCommand("DELETE from PAY_RESULT where PAYCODE = '" & PAYCODE & "'", Common.con)
                    cmd.ExecuteNonQuery()

                    cmd = New SqlCommand("DELETE from PAY_REIMURSH where PAYCODE = '" & PAYCODE & "'", Common.con)
                    cmd.ExecuteNonQuery()

                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            End If
        End If
        setEmpGrid()
    End Sub
    Private Sub setEmpGrid()
        Dim ds1 As DataSet
        GridControl1.DataSource = Nothing
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("Paycode")
        dt.Columns.Add("Employee Name")
        dt.Columns.Add("Departent")
        dt.Columns.Add("VGross")
        dt.Columns.Add("VBasic")

        Dim sSql As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet

        If XtraFullPayrollMenuMaster.PayEmpMster = "E" Then
            If Common.USERTYPE = "A" Then
                If Common.servername = "Access" Then
                    sSql = "select T.Paycode, T.EMPNAME, D.DEPARTMENTNAME as DEPName from TblEmployee T, tblDepartment D where D.DEPARTMENTCODE = T.DEPARTMENTCODE order by T.PRESENTCARDNO"
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    sSql = "select PAYCODE,EMPNAME,  (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName from TblEmployee order by PRESENTCARDNO"
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
            Else
                Dim emp() As String = Common.Auth_Branch.Split(",")
                Dim ls As New List(Of String)()
                For x As Integer = 0 To emp.Length - 1
                    ls.Add(emp(x).Trim)
                Next
                If Common.servername = "Access" Then
                    sSql = "select T.Paycode, T.EMPNAME, D.DEPARTMENTNAME as DEPName from TblEmployee T, tblDepartment D where D.DEPARTMENTCODE = T.DEPARTMENTCODE and T.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') order by T.PRESENTCARDNO"
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    sSql = "select PAYCODE,EMPNAME,  (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName from TblEmployee where BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') order by PRESENTCARDNO"
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
            End If

            If ds.Tables(0).Rows.Count = 0 Then
                Exit Sub
            End If
            Dim VBASIC As String
            Dim VGross As String
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                sSql = "select VBASIC,VGross from Pay_Master where PAYCODE = '" & ds.Tables(0).Rows(i).Item("paycode").ToString.Trim & "'"
                ds1 = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds1)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds1)
                End If
                If ds1.Tables(0).Rows.Count = 0 Then
                    VBASIC = ""
                    VGross = ""
                Else
                    VBASIC = ds1.Tables(0).Rows(0).Item("VBASIC").ToString.Trim
                    VGross = ds1.Tables(0).Rows(0).Item("VGross").ToString.Trim
                End If
                dt.Rows.Add(ds.Tables(0).Rows(i).Item("Paycode").ToString.Split(" ")(0), ds.Tables(0).Rows(i).Item("EMPNAME").ToString, ds.Tables(0).Rows(i).Item("DEPName").ToString, VGross, VBASIC)
            Next
        ElseIf XtraFullPayrollMenuMaster.PayEmpMster = "I" Then
            If Common.USERTYPE = "A" Then
                If Common.servername = "Access" Then
                    sSql = "select T.Paycode, T.EMPNAME, D.DEPARTMENTNAME as DEPName from TblEmployee T, tblDepartment D where D.DEPARTMENTCODE = T.DEPARTMENTCODE order by T.PRESENTCARDNO"
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    sSql = "select PAYCODE,EMPNAME,  (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName from TblEmployee order by PRESENTCARDNO"
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
            Else
                Dim emp() As String = Common.Auth_Branch.Split(",")
                Dim ls As New List(Of String)()
                For x As Integer = 0 To emp.Length - 1
                    ls.Add(emp(x).Trim)
                Next
                If Common.servername = "Access" Then
                    sSql = "select T.Paycode, T.EMPNAME, D.DEPARTMENTNAME as DEPName from TblEmployee T, tblDepartment D where D.DEPARTMENTCODE = T.DEPARTMENTCODE and T.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') order by T.PRESENTCARDNO"
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    sSql = "select PAYCODE,EMPNAME,  (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName from TblEmployee where BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') order by PRESENTCARDNO"
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
            End If

            If ds.Tables(0).Rows.Count = 0 Then
                Exit Sub
            End If
            Dim VBASIC As String
            Dim VGross As String
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                sSql = "select VBASIC,VGross from Pay_Master where PAYCODE = '" & ds.Tables(0).Rows(i).Item("paycode").ToString.Trim & "'"
                ds1 = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds1)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds1)
                End If
                If ds1.Tables(0).Rows.Count = 0 Then
                    Continue For
                    VBASIC = ""
                    VGross = ""
                Else
                    VBASIC = ds1.Tables(0).Rows(0).Item("VBASIC").ToString.Trim
                    VGross = ds1.Tables(0).Rows(0).Item("VGross").ToString.Trim
                End If
                dt.Rows.Add(ds.Tables(0).Rows(i).Item("Paycode").ToString.Split(" ")(0), ds.Tables(0).Rows(i).Item("EMPNAME").ToString, ds.Tables(0).Rows(i).Item("DEPName").ToString, VGross, VBASIC)
            Next
        End If
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridControl1.DataSource = dt
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            EmpId = row("PAYCODE").ToString.Trim
        Catch ex As Exception
            EmpId = ""
        End Try
        e.Allow = False
        S.ShowDialog()
        setEmpGrid()
    End Sub
End Class
