﻿Imports DevExpress.XtraGrid.Views.Grid
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Base
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraSplashScreen
Imports DevExpress.Utils
Imports Riss.Devices 'bio2+
Imports iAS.HSeriesSampleCSharp
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Drawing.Imaging
Imports CMITech.UMXClient

Public Class XtraPieceMaster
    Dim adap1, adap As SqlDataAdapter
    Dim adapA As OleDbDataAdapter
    Dim ds As DataSet
    Dim ulf As UserLookAndFeel
    Public Shared PieceID As String
    Dim buttons As Dictionary(Of Integer, Repository.RepositoryItemButtonEdit) = New Dictionary(Of Integer, Repository.RepositoryItemButtonEdit)

    Public Sub New()
        InitializeComponent()
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
   
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
  
    Private Sub XtraDevice_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load        
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100
        loadPieceGrid()
    End Sub
    Private Sub loadPieceGrid()       
        GridControl1.DataSource = Nothing
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("Piece Type Code")
        dt.Columns.Add("Description")
        dt.Columns.Add("Rate")
        Dim sSql As String = "select * from tblPiece"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                dt.Rows.Add(ds.Tables(0).Rows(i).Item("Pcode").ToString.Trim, ds.Tables(0).Rows(i).Item("Pname").ToString, ds.Tables(0).Rows(i).Item("Prate").ToString)
            Next
        End If
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridControl1.DataSource = dt

    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
       
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            PieceID = row("Piece Type Code").ToString.Trim
        Catch ex As Exception
            PieceID = ""
        End Try
        e.Allow = False

        XtraPieceEdit.ShowDialog()
        loadPieceGrid()
       
    End Sub        
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then

            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Dim sSql As String
                Dim cmd As SqlCommand
                Dim cmd1 As OleDbCommand
                e.Handled = True
                Dim selectedRows As Integer() = GridView1.GetSelectedRows()
                Dim result As Object() = New Object(selectedRows.Length - 1) {}
                For i = 0 To selectedRows.Length - 1
                    Dim rowHandle As Integer = selectedRows(i)
                    If Not GridView1.IsGroupRow(rowHandle) Then
                        Dim ID_NO As String = GridView1.GetRowCellValue(rowHandle, "Piece Type Code").ToString.Trim
                        sSql = "DELETE from tblPiece where Pcode ='" & ID_NO & "'"
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                    End If
                Next
                loadPieceGrid()
                XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
            End If
        End If
    End Sub
    
End Class
