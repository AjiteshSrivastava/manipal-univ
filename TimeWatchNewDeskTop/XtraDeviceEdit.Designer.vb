﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraDeviceEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraDeviceEdit))
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextConId = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit3 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.TextIP = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEdit1 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblbranchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextSrNo = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.TblbranchTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter()
        Me.Tblbranch1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter()
        Me.ComboBoxEdit4 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonGetSr = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditComm = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.TextLED = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.BtnVlidate = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonUpload = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonDownload = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditUpload = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.AxFP_CLOCK1 = New AxFP_CLOCKLib.AxFP_CLOCK()
        CType(Me.TextConId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextIP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextSrNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditComm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextLED.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.TextEditUpload.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.AxFP_CLOCK1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Device ID *"
        '
        'TextConId
        '
        Me.TextConId.Location = New System.Drawing.Point(165, 9)
        Me.TextConId.Name = "TextConId"
        Me.TextConId.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextConId.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextConId.Properties.Appearance.Options.UseFont = True
        Me.TextConId.Properties.Mask.EditMask = "[0-9]*"
        Me.TextConId.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextConId.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextConId.Properties.MaxLength = 2
        Me.TextConId.Size = New System.Drawing.Size(179, 20)
        Me.TextConId.TabIndex = 3
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(12, 38)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(56, 14)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Protocol *"
        '
        'ComboBoxEdit1
        '
        Me.ComboBoxEdit1.EditValue = "TCP/IP"
        Me.ComboBoxEdit1.Location = New System.Drawing.Point(165, 35)
        Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
        Me.ComboBoxEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit1.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit1.Properties.Items.AddRange(New Object() {"TCP/IP", "USB"})
        Me.ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit1.Size = New System.Drawing.Size(179, 20)
        Me.ComboBoxEdit1.TabIndex = 4
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(12, 64)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(39, 14)
        Me.LabelControl3.TabIndex = 18
        Me.LabelControl3.Text = "Type *"
        '
        'ComboBoxEdit2
        '
        Me.ComboBoxEdit2.EditValue = "IN"
        Me.ComboBoxEdit2.Location = New System.Drawing.Point(165, 61)
        Me.ComboBoxEdit2.Name = "ComboBoxEdit2"
        Me.ComboBoxEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit2.Properties.Items.AddRange(New Object() {"IN", "OUT", "IN/OUT", "DEVICE DIRECTION"})
        Me.ComboBoxEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit2.Size = New System.Drawing.Size(179, 20)
        Me.ComboBoxEdit2.TabIndex = 5
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(12, 90)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(83, 14)
        Me.LabelControl4.TabIndex = 20
        Me.LabelControl4.Text = "Device Model *"
        '
        'ComboBoxEdit3
        '
        Me.ComboBoxEdit3.EditValue = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872"
        Me.ComboBoxEdit3.Location = New System.Drawing.Point(165, 87)
        Me.ComboBoxEdit3.Name = "ComboBoxEdit3"
        Me.ComboBoxEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit3.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit3.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit3.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit3.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit3.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit3.Properties.DropDownRows = 8
        Me.ComboBoxEdit3.Properties.Items.AddRange(New Object() {"ZK(TFT)", "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872", "Bio-1Pro/ATF305Pro/ATF686Pro", "TW-IR102", "Bio2+", "Bio1Eco", "EF45", "F9", "ATF686n", "TF-01", "ZK Controller"})
        Me.ComboBoxEdit3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit3.Size = New System.Drawing.Size(179, 20)
        Me.ComboBoxEdit3.TabIndex = 6
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(12, 116)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(62, 14)
        Me.LabelControl5.TabIndex = 22
        Me.LabelControl5.Text = "Device IP *"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(12, 142)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl6.TabIndex = 23
        Me.LabelControl6.Text = "Location *"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(12, 168)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl7.TabIndex = 24
        Me.LabelControl7.Text = "Serial No"
        '
        'TextIP
        '
        Me.TextIP.Location = New System.Drawing.Point(165, 113)
        Me.TextIP.Name = "TextIP"
        Me.TextIP.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextIP.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextIP.Properties.Appearance.Options.UseFont = True
        Me.TextIP.Size = New System.Drawing.Size(179, 20)
        Me.TextIP.TabIndex = 7
        '
        'GridLookUpEdit1
        '
        Me.GridLookUpEdit1.Location = New System.Drawing.Point(165, 139)
        Me.GridLookUpEdit1.Name = "GridLookUpEdit1"
        Me.GridLookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.GridLookUpEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.GridLookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit1.Properties.DataSource = Me.TblbranchBindingSource
        Me.GridLookUpEdit1.Properties.DisplayMember = "BRANCHNAME"
        Me.GridLookUpEdit1.Properties.NullText = ""
        Me.GridLookUpEdit1.Properties.ValueMember = "BRANCHNAME"
        Me.GridLookUpEdit1.Properties.View = Me.GridView7
        Me.GridLookUpEdit1.Size = New System.Drawing.Size(179, 20)
        Me.GridLookUpEdit1.TabIndex = 8
        '
        'TblbranchBindingSource
        '
        Me.TblbranchBindingSource.DataMember = "tblbranch"
        Me.TblbranchBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView7
        '
        Me.GridView7.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView7.Name = "GridView7"
        Me.GridView7.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView7.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Location Code"
        Me.GridColumn1.FieldName = "BRANCHCODE"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Location Name"
        Me.GridColumn2.FieldName = "BRANCHNAME"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        '
        'TextSrNo
        '
        Me.TextSrNo.Location = New System.Drawing.Point(165, 165)
        Me.TextSrNo.Name = "TextSrNo"
        Me.TextSrNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextSrNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextSrNo.Properties.Appearance.Options.UseFont = True
        Me.TextSrNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextSrNo.Properties.MaxLength = 50
        Me.TextSrNo.Size = New System.Drawing.Size(179, 20)
        Me.TextSrNo.TabIndex = 9
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(299, 411)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 15
        Me.SimpleButton2.Text = "Cancel"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(218, 411)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 14
        Me.SimpleButton1.Text = "Save"
        '
        'TblbranchTableAdapter
        '
        Me.TblbranchTableAdapter.ClearBeforeFill = True
        '
        'Tblbranch1TableAdapter1
        '
        Me.Tblbranch1TableAdapter1.ClearBeforeFill = True
        '
        'ComboBoxEdit4
        '
        Me.ComboBoxEdit4.EditValue = "Attendance"
        Me.ComboBoxEdit4.Location = New System.Drawing.Point(165, 191)
        Me.ComboBoxEdit4.Name = "ComboBoxEdit4"
        Me.ComboBoxEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit4.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit4.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit4.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit4.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit4.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit4.Properties.Items.AddRange(New Object() {"Attendance", "Canteen"})
        Me.ComboBoxEdit4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit4.Size = New System.Drawing.Size(179, 20)
        Me.ComboBoxEdit4.TabIndex = 11
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(12, 194)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(44, 14)
        Me.LabelControl8.TabIndex = 35
        Me.LabelControl8.Text = "Purpose"
        '
        'SimpleButtonGetSr
        '
        Me.SimpleButtonGetSr.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonGetSr.Appearance.Options.UseFont = True
        Me.SimpleButtonGetSr.Location = New System.Drawing.Point(350, 164)
        Me.SimpleButtonGetSr.Name = "SimpleButtonGetSr"
        Me.SimpleButtonGetSr.Size = New System.Drawing.Size(50, 23)
        Me.SimpleButtonGetSr.TabIndex = 10
        Me.SimpleButtonGetSr.Text = "Get"
        Me.SimpleButtonGetSr.Visible = False
        '
        'TextEditComm
        '
        Me.TextEditComm.Location = New System.Drawing.Point(165, 217)
        Me.TextEditComm.Name = "TextEditComm"
        Me.TextEditComm.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditComm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditComm.Properties.Appearance.Options.UseFont = True
        Me.TextEditComm.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditComm.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditComm.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditComm.Properties.MaxLength = 6
        Me.TextEditComm.Size = New System.Drawing.Size(179, 20)
        Me.TextEditComm.TabIndex = 12
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(12, 220)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl9.TabIndex = 38
        Me.LabelControl9.Text = "Comm Key"
        '
        'TextLED
        '
        Me.TextLED.Location = New System.Drawing.Point(165, 243)
        Me.TextLED.Name = "TextLED"
        Me.TextLED.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextLED.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextLED.Properties.Appearance.Options.UseFont = True
        Me.TextLED.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextLED.Properties.MaxLength = 50
        Me.TextLED.Size = New System.Drawing.Size(179, 20)
        Me.TextLED.TabIndex = 39
        Me.TextLED.Visible = False
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(12, 246)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(36, 14)
        Me.LabelControl10.TabIndex = 40
        Me.LabelControl10.Text = "LED IP"
        Me.LabelControl10.Visible = False
        '
        'BtnVlidate
        '
        Me.BtnVlidate.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.BtnVlidate.Appearance.Options.UseFont = True
        Me.BtnVlidate.Enabled = False
        Me.BtnVlidate.Location = New System.Drawing.Point(153, 5)
        Me.BtnVlidate.Name = "BtnVlidate"
        Me.BtnVlidate.Size = New System.Drawing.Size(75, 23)
        Me.BtnVlidate.TabIndex = 13
        Me.BtnVlidate.Text = "Activate"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.LabelControl19)
        Me.PanelControl1.Controls.Add(Me.LabelControl18)
        Me.PanelControl1.Controls.Add(Me.SimpleButtonUpload)
        Me.PanelControl1.Controls.Add(Me.SimpleButtonDownload)
        Me.PanelControl1.Controls.Add(Me.TextEditUpload)
        Me.PanelControl1.Controls.Add(Me.LabelControl17)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 269)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(465, 89)
        Me.PanelControl1.TabIndex = 50
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(5, 29)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(113, 14)
        Me.LabelControl19.TabIndex = 50
        Me.LabelControl19.Text = "Download Device file"
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(5, 57)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(96, 14)
        Me.LabelControl18.TabIndex = 47
        Me.LabelControl18.Text = "Upload Device file"
        '
        'SimpleButtonUpload
        '
        Me.SimpleButtonUpload.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonUpload.Appearance.Options.UseFont = True
        Me.SimpleButtonUpload.Location = New System.Drawing.Point(356, 53)
        Me.SimpleButtonUpload.Name = "SimpleButtonUpload"
        Me.SimpleButtonUpload.Size = New System.Drawing.Size(87, 23)
        Me.SimpleButtonUpload.TabIndex = 13
        Me.SimpleButtonUpload.Text = "Upload"
        '
        'SimpleButtonDownload
        '
        Me.SimpleButtonDownload.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDownload.Appearance.Options.UseFont = True
        Me.SimpleButtonDownload.Location = New System.Drawing.Point(153, 25)
        Me.SimpleButtonDownload.Name = "SimpleButtonDownload"
        Me.SimpleButtonDownload.Size = New System.Drawing.Size(87, 23)
        Me.SimpleButtonDownload.TabIndex = 14
        Me.SimpleButtonDownload.Text = "Download"
        '
        'TextEditUpload
        '
        Me.TextEditUpload.Location = New System.Drawing.Point(153, 54)
        Me.TextEditUpload.Name = "TextEditUpload"
        Me.TextEditUpload.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditUpload.Properties.Appearance.Options.UseFont = True
        Me.TextEditUpload.Size = New System.Drawing.Size(179, 20)
        Me.TextEditUpload.TabIndex = 12
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(5, 5)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl17.TabIndex = 46
        Me.LabelControl17.Text = "Offline mode"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.LabelControl11)
        Me.PanelControl2.Controls.Add(Me.BtnVlidate)
        Me.PanelControl2.Location = New System.Drawing.Point(12, 364)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(465, 36)
        Me.PanelControl2.TabIndex = 51
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(5, 9)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(69, 14)
        Me.LabelControl11.TabIndex = 51
        Me.LabelControl11.Text = "Online mode"
        '
        'AxFP_CLOCK1
        '
        Me.AxFP_CLOCK1.Enabled = True
        Me.AxFP_CLOCK1.Location = New System.Drawing.Point(350, 194)
        Me.AxFP_CLOCK1.Name = "AxFP_CLOCK1"
        Me.AxFP_CLOCK1.OcxState = CType(resources.GetObject("AxFP_CLOCK1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxFP_CLOCK1.Size = New System.Drawing.Size(56, 50)
        Me.AxFP_CLOCK1.TabIndex = 52
        Me.AxFP_CLOCK1.Visible = False
        '
        'XtraDeviceEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(540, 446)
        Me.Controls.Add(Me.AxFP_CLOCK1)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.TextLED)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.TextEditComm)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.SimpleButtonGetSr)
        Me.Controls.Add(Me.ComboBoxEdit4)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.TextSrNo)
        Me.Controls.Add(Me.GridLookUpEdit1)
        Me.Controls.Add(Me.TextIP)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.ComboBoxEdit3)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.ComboBoxEdit2)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.ComboBoxEdit1)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.TextConId)
        Me.Controls.Add(Me.LabelControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraDeviceEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.TextConId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextIP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextSrNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditComm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextLED.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.TextEditUpload.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.AxFP_CLOCK1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextConId As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit3 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextIP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridLookUpEdit1 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TextSrNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblbranchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblbranchTableAdapter As iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter
    Friend WithEvents Tblbranch1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter
    Friend WithEvents ComboBoxEdit4 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonGetSr As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditComm As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextLED As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BtnVlidate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonDownload As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonUpload As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditUpload As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AxFP_CLOCK1 As AxFP_CLOCKLib.AxFP_CLOCK
End Class
