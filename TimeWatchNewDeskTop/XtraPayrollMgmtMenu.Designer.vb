﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraPayrollMgmtMenu
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.NavigationPane1 = New DevExpress.XtraBars.Navigation.NavigationPane()
        Me.NavigationPage3 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage2 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage5 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage4 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage1 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage6 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage7 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage8 = New DevExpress.XtraBars.Navigation.NavigationPage()
        CType(Me.NavigationPane1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NavigationPane1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavigationPane1
        '
        Me.NavigationPane1.AllowTransitionAnimation = DevExpress.Utils.DefaultBoolean.[True]
        Me.NavigationPane1.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 18.0!)
        Me.NavigationPane1.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.Black
        Me.NavigationPane1.AppearanceButton.Hovered.Options.UseFont = True
        Me.NavigationPane1.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.NavigationPane1.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 13.0!)
        Me.NavigationPane1.AppearanceButton.Normal.ForeColor = System.Drawing.Color.Black
        Me.NavigationPane1.AppearanceButton.Normal.Options.UseFont = True
        Me.NavigationPane1.AppearanceButton.Normal.Options.UseForeColor = True
        Me.NavigationPane1.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 13.0!)
        Me.NavigationPane1.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.RoyalBlue
        Me.NavigationPane1.AppearanceButton.Pressed.Options.UseFont = True
        Me.NavigationPane1.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.NavigationPane1.Controls.Add(Me.NavigationPage3)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage2)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage5)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage4)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage1)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage6)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage7)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage8)
        Me.NavigationPane1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationPane1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.NavigationPane1.Location = New System.Drawing.Point(0, 0)
        Me.NavigationPane1.LookAndFeel.SkinName = "iMaginary"
        Me.NavigationPane1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.NavigationPane1.Name = "NavigationPane1"
        Me.NavigationPane1.PageProperties.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.NavigationPane1.PageProperties.AppearanceCaption.Options.UseFont = True
        Me.NavigationPane1.PageProperties.ShowCollapseButton = False
        Me.NavigationPane1.PageProperties.ShowExpandButton = False
        Me.NavigationPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.NavigationPage1, Me.NavigationPage2, Me.NavigationPage3, Me.NavigationPage4, Me.NavigationPage8, Me.NavigationPage5, Me.NavigationPage6, Me.NavigationPage7})
        Me.NavigationPane1.RegularSize = New System.Drawing.Size(1370, 660)
        Me.NavigationPane1.SelectedPage = Me.NavigationPage1
        Me.NavigationPane1.Size = New System.Drawing.Size(1370, 660)
        Me.NavigationPane1.TabIndex = 1
        Me.NavigationPane1.TransitionAnimationProperties.FrameCount = 750
        Me.NavigationPane1.TransitionAnimationProperties.FrameInterval = 5000
        Me.NavigationPane1.TransitionType = DevExpress.Utils.Animation.Transitions.Cover
        '
        'NavigationPage3
        '
        Me.NavigationPage3.Caption = "Formula Setup"
        Me.NavigationPage3.Name = "NavigationPage3"
        Me.NavigationPage3.Size = New System.Drawing.Size(1370, 660)
        '
        'NavigationPage2
        '
        Me.NavigationPage2.Caption = "Payroll Setup"
        Me.NavigationPage2.Name = "NavigationPage2"
        Me.NavigationPage2.Size = New System.Drawing.Size(1370, 660)
        '
        'NavigationPage5
        '
        Me.NavigationPage5.Caption = "Loan/Advance Setup"
        Me.NavigationPage5.Name = "NavigationPage5"
        Me.NavigationPage5.Size = New System.Drawing.Size(1370, 660)
        '
        'NavigationPage4
        '
        Me.NavigationPage4.Caption = "Payroll Processing"
        Me.NavigationPage4.Name = "NavigationPage4"
        Me.NavigationPage4.Size = New System.Drawing.Size(1370, 660)
        '
        'NavigationPage1
        '
        Me.NavigationPage1.Caption = "Employee Setup"
        Me.NavigationPage1.Name = "NavigationPage1"
        Me.NavigationPage1.Size = New System.Drawing.Size(1110, 602)
        '
        'NavigationPage6
        '
        Me.NavigationPage6.Caption = "Loan/Advance Adjustment"
        Me.NavigationPage6.Name = "NavigationPage6"
        Me.NavigationPage6.PageVisible = False
        Me.NavigationPage6.Size = New System.Drawing.Size(1370, 660)
        '
        'NavigationPage7
        '
        Me.NavigationPage7.Caption = "Reports"
        Me.NavigationPage7.Name = "NavigationPage7"
        Me.NavigationPage7.Size = New System.Drawing.Size(1110, 602)
        '
        'NavigationPage8
        '
        Me.NavigationPage8.Caption = "Performance Maintain"
        Me.NavigationPage8.Name = "NavigationPage8"
        Me.NavigationPage8.Size = New System.Drawing.Size(1370, 660)
        '
        'XtraPayrollMgmtMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.NavigationPane1)
        Me.Name = "XtraPayrollMgmtMenu"
        Me.Size = New System.Drawing.Size(1370, 660)
        CType(Me.NavigationPane1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NavigationPane1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NavigationPane1 As DevExpress.XtraBars.Navigation.NavigationPane
    Friend WithEvents NavigationPage2 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage3 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage5 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage4 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage1 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage6 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage7 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage8 As DevExpress.XtraBars.Navigation.NavigationPage

End Class
