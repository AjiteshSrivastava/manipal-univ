﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraDataMaintenanceEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupControlShift = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEditShift = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditOff = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridLookUpEdit1 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblShiftMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTARTTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHDURATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colORDERINF12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTSTARTAFTER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDEDUCTHRS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHDEDUCTION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTPOSITION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTDURATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDEDUCTAFTER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TblShiftMasterTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter()
        Me.TblShiftMaster1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblShiftMaster1TableAdapter()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colOFFICEPUNCH = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colOFFICEPUNCH1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colP_DAY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.ComboBoxEditNepaliYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEditNEpaliMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.TextEdit11 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEditNepaliDate = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.TextEdit10 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit9 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit8 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.MachineRawPunchTableAdapter = New iAS.SSSDBDataSetTableAdapters.MachineRawPunchTableAdapter()
        Me.MachineRawPunchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.btnUpdateLeave = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditReasonLeave = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEdit2 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckEdit6 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit5 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit4 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControlShift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControlShift.SuspendLayout()
        CType(Me.CheckEditShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditOff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit3.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.ComboBoxEditNepaliYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNepaliDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MachineRawPunchBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.TextEditReasonLeave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.CheckEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControlShift
        '
        Me.GroupControlShift.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControlShift.AppearanceCaption.Options.UseFont = True
        Me.GroupControlShift.Controls.Add(Me.CheckEditShift)
        Me.GroupControlShift.Controls.Add(Me.CheckEditOff)
        Me.GroupControlShift.Controls.Add(Me.SimpleButton1)
        Me.GroupControlShift.Controls.Add(Me.GridLookUpEdit1)
        Me.GroupControlShift.Controls.Add(Me.LabelControl1)
        Me.GroupControlShift.Location = New System.Drawing.Point(12, 12)
        Me.GroupControlShift.Name = "GroupControlShift"
        Me.GroupControlShift.Size = New System.Drawing.Size(721, 65)
        Me.GroupControlShift.TabIndex = 0
        Me.GroupControlShift.Text = "Shift"
        '
        'CheckEditShift
        '
        Me.CheckEditShift.EditValue = True
        Me.CheckEditShift.Location = New System.Drawing.Point(125, 30)
        Me.CheckEditShift.Name = "CheckEditShift"
        Me.CheckEditShift.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditShift.Properties.Appearance.Options.UseFont = True
        Me.CheckEditShift.Properties.Caption = "Select Shift"
        Me.CheckEditShift.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditShift.Properties.RadioGroupIndex = 0
        Me.CheckEditShift.Size = New System.Drawing.Size(126, 19)
        Me.CheckEditShift.TabIndex = 9
        '
        'CheckEditOff
        '
        Me.CheckEditOff.Location = New System.Drawing.Point(17, 30)
        Me.CheckEditOff.Name = "CheckEditOff"
        Me.CheckEditOff.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditOff.Properties.Appearance.Options.UseFont = True
        Me.CheckEditOff.Properties.Caption = "Mark Off"
        Me.CheckEditOff.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditOff.Properties.RadioGroupIndex = 0
        Me.CheckEditOff.Size = New System.Drawing.Size(75, 19)
        Me.CheckEditOff.TabIndex = 8
        Me.CheckEditOff.TabStop = False
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(531, 28)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(114, 23)
        Me.SimpleButton1.TabIndex = 7
        Me.SimpleButton1.Text = "Change Shift"
        '
        'GridLookUpEdit1
        '
        Me.GridLookUpEdit1.EditValue = ""
        Me.GridLookUpEdit1.Location = New System.Drawing.Point(325, 29)
        Me.GridLookUpEdit1.Name = "GridLookUpEdit1"
        Me.GridLookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.GridLookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit1.Properties.DataSource = Me.TblShiftMasterBindingSource
        Me.GridLookUpEdit1.Properties.DisplayMember = "SHIFT"
        Me.GridLookUpEdit1.Properties.ValueMember = "SHIFT"
        Me.GridLookUpEdit1.Properties.View = Me.GridLookUpEdit1View
        Me.GridLookUpEdit1.Size = New System.Drawing.Size(177, 20)
        Me.GridLookUpEdit1.TabIndex = 6
        '
        'TblShiftMasterBindingSource
        '
        Me.TblShiftMasterBindingSource.DataMember = "tblShiftMaster"
        Me.TblShiftMasterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSHIFT, Me.colSTARTTIME, Me.colENDTIME, Me.colLUNCHTIME, Me.colLUNCHDURATION, Me.colLUNCHENDTIME, Me.colORDERINF12, Me.colOTSTARTAFTER, Me.colOTDEDUCTHRS, Me.colLUNCHDEDUCTION, Me.colSHIFTPOSITION, Me.colSHIFTDURATION, Me.colOTDEDUCTAFTER, Me.colLastModifiedBy, Me.colLastModifiedDate})
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colSHIFT
        '
        Me.colSHIFT.FieldName = "SHIFT"
        Me.colSHIFT.Name = "colSHIFT"
        Me.colSHIFT.Visible = True
        Me.colSHIFT.VisibleIndex = 0
        '
        'colSTARTTIME
        '
        Me.colSTARTTIME.DisplayFormat.FormatString = "HH:mm"
        Me.colSTARTTIME.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colSTARTTIME.FieldName = "STARTTIME"
        Me.colSTARTTIME.Name = "colSTARTTIME"
        Me.colSTARTTIME.Visible = True
        Me.colSTARTTIME.VisibleIndex = 1
        '
        'colENDTIME
        '
        Me.colENDTIME.DisplayFormat.FormatString = "HH:mm"
        Me.colENDTIME.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colENDTIME.FieldName = "ENDTIME"
        Me.colENDTIME.Name = "colENDTIME"
        Me.colENDTIME.Visible = True
        Me.colENDTIME.VisibleIndex = 2
        '
        'colLUNCHTIME
        '
        Me.colLUNCHTIME.FieldName = "LUNCHTIME"
        Me.colLUNCHTIME.Name = "colLUNCHTIME"
        '
        'colLUNCHDURATION
        '
        Me.colLUNCHDURATION.FieldName = "LUNCHDURATION"
        Me.colLUNCHDURATION.Name = "colLUNCHDURATION"
        '
        'colLUNCHENDTIME
        '
        Me.colLUNCHENDTIME.FieldName = "LUNCHENDTIME"
        Me.colLUNCHENDTIME.Name = "colLUNCHENDTIME"
        '
        'colORDERINF12
        '
        Me.colORDERINF12.FieldName = "ORDERINF12"
        Me.colORDERINF12.Name = "colORDERINF12"
        '
        'colOTSTARTAFTER
        '
        Me.colOTSTARTAFTER.FieldName = "OTSTARTAFTER"
        Me.colOTSTARTAFTER.Name = "colOTSTARTAFTER"
        '
        'colOTDEDUCTHRS
        '
        Me.colOTDEDUCTHRS.FieldName = "OTDEDUCTHRS"
        Me.colOTDEDUCTHRS.Name = "colOTDEDUCTHRS"
        '
        'colLUNCHDEDUCTION
        '
        Me.colLUNCHDEDUCTION.FieldName = "LUNCHDEDUCTION"
        Me.colLUNCHDEDUCTION.Name = "colLUNCHDEDUCTION"
        '
        'colSHIFTPOSITION
        '
        Me.colSHIFTPOSITION.FieldName = "SHIFTPOSITION"
        Me.colSHIFTPOSITION.Name = "colSHIFTPOSITION"
        '
        'colSHIFTDURATION
        '
        Me.colSHIFTDURATION.FieldName = "SHIFTDURATION"
        Me.colSHIFTDURATION.Name = "colSHIFTDURATION"
        '
        'colOTDEDUCTAFTER
        '
        Me.colOTDEDUCTAFTER.FieldName = "OTDEDUCTAFTER"
        Me.colOTDEDUCTAFTER.Name = "colOTDEDUCTAFTER"
        '
        'colLastModifiedBy
        '
        Me.colLastModifiedBy.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy.Name = "colLastModifiedBy"
        '
        'colLastModifiedDate
        '
        Me.colLastModifiedDate.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate.Name = "colLastModifiedDate"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(267, 32)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(25, 14)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Shift"
        '
        'TblShiftMasterTableAdapter
        '
        Me.TblShiftMasterTableAdapter.ClearBeforeFill = True
        '
        'TblShiftMaster1TableAdapter1
        '
        Me.TblShiftMaster1TableAdapter1.ClearBeforeFill = True
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.PanelControl1)
        Me.GroupControl2.Controls.Add(Me.TextEdit10)
        Me.GroupControl2.Controls.Add(Me.LabelControl11)
        Me.GroupControl2.Controls.Add(Me.TextEdit9)
        Me.GroupControl2.Controls.Add(Me.LabelControl10)
        Me.GroupControl2.Controls.Add(Me.TextEdit5)
        Me.GroupControl2.Controls.Add(Me.TextEdit6)
        Me.GroupControl2.Controls.Add(Me.LabelControl6)
        Me.GroupControl2.Controls.Add(Me.TextEdit7)
        Me.GroupControl2.Controls.Add(Me.TextEdit8)
        Me.GroupControl2.Controls.Add(Me.LabelControl7)
        Me.GroupControl2.Controls.Add(Me.LabelControl8)
        Me.GroupControl2.Controls.Add(Me.LabelControl9)
        Me.GroupControl2.Controls.Add(Me.TextEdit4)
        Me.GroupControl2.Controls.Add(Me.TextEdit3)
        Me.GroupControl2.Controls.Add(Me.LabelControl5)
        Me.GroupControl2.Controls.Add(Me.TextEdit2)
        Me.GroupControl2.Controls.Add(Me.TextEdit1)
        Me.GroupControl2.Controls.Add(Me.LabelControl4)
        Me.GroupControl2.Controls.Add(Me.LabelControl3)
        Me.GroupControl2.Controls.Add(Me.LabelControl2)
        Me.GroupControl2.Location = New System.Drawing.Point(13, 85)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(720, 321)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Time"
        '
        'PanelControl1
        '
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl1.Controls.Add(Me.GridControl2)
        Me.PanelControl1.Controls.Add(Me.SidePanel1)
        Me.PanelControl1.Location = New System.Drawing.Point(248, 26)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(467, 290)
        Me.PanelControl1.TabIndex = 8
        '
        'GridControl2
        '
        Me.GridControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl2.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.GridControl2.Location = New System.Drawing.Point(2, 53)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit2, Me.RepositoryItemDateEdit3})
        Me.GridControl2.Size = New System.Drawing.Size(463, 235)
        Me.GridControl2.TabIndex = 26
        Me.GridControl2.UseEmbeddedNavigator = True
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colOFFICEPUNCH, Me.colOFFICEPUNCH1, Me.colP_DAY})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView2.OptionsBehavior.Editable = False
        '
        'colOFFICEPUNCH
        '
        Me.colOFFICEPUNCH.Caption = "Date"
        Me.colOFFICEPUNCH.ColumnEdit = Me.RepositoryItemDateEdit2
        Me.colOFFICEPUNCH.FieldName = "OFFICEPUNCH"
        Me.colOFFICEPUNCH.Name = "colOFFICEPUNCH"
        Me.colOFFICEPUNCH.Visible = True
        Me.colOFFICEPUNCH.VisibleIndex = 0
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'colOFFICEPUNCH1
        '
        Me.colOFFICEPUNCH1.Caption = "Punch Time"
        Me.colOFFICEPUNCH1.ColumnEdit = Me.RepositoryItemDateEdit3
        Me.colOFFICEPUNCH1.FieldName = "OFFICEPUNCH"
        Me.colOFFICEPUNCH1.Name = "colOFFICEPUNCH1"
        Me.colOFFICEPUNCH1.Visible = True
        Me.colOFFICEPUNCH1.VisibleIndex = 1
        '
        'RepositoryItemDateEdit3
        '
        Me.RepositoryItemDateEdit3.AutoHeight = False
        Me.RepositoryItemDateEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit3.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit3.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit3.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit3.Name = "RepositoryItemDateEdit3"
        '
        'colP_DAY
        '
        Me.colP_DAY.FieldName = "P_DAY"
        Me.colP_DAY.Name = "colP_DAY"
        Me.colP_DAY.Visible = True
        Me.colP_DAY.VisibleIndex = 2
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.ComboBoxEditNepaliYear)
        Me.SidePanel1.Controls.Add(Me.DateEdit1)
        Me.SidePanel1.Controls.Add(Me.SimpleButton2)
        Me.SidePanel1.Controls.Add(Me.LabelControl12)
        Me.SidePanel1.Controls.Add(Me.ComboBoxEditNEpaliMonth)
        Me.SidePanel1.Controls.Add(Me.TextEdit11)
        Me.SidePanel1.Controls.Add(Me.LabelControl13)
        Me.SidePanel1.Controls.Add(Me.ComboBoxEditNepaliDate)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel1.Location = New System.Drawing.Point(2, 2)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(463, 51)
        Me.SidePanel1.TabIndex = 25
        Me.SidePanel1.Text = "SidePanel1"
        '
        'ComboBoxEditNepaliYear
        '
        Me.ComboBoxEditNepaliYear.Enabled = False
        Me.ComboBoxEditNepaliYear.Location = New System.Drawing.Point(194, 17)
        Me.ComboBoxEditNepaliYear.Name = "ComboBoxEditNepaliYear"
        Me.ComboBoxEditNepaliYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYear.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboBoxEditNepaliYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboBoxEditNepaliYear.TabIndex = 37
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Enabled = False
        Me.DateEdit1.Location = New System.Drawing.Point(82, 17)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit1.Properties.Appearance.Options.UseFont = True
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Size = New System.Drawing.Size(110, 20)
        Me.DateEdit1.TabIndex = 4
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(379, 16)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 24
        Me.SimpleButton2.Text = "Post Punch"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(7, 20)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl12.TabIndex = 3
        Me.LabelControl12.Text = "Punch Date"
        '
        'ComboBoxEditNEpaliMonth
        '
        Me.ComboBoxEditNEpaliMonth.Enabled = False
        Me.ComboBoxEditNEpaliMonth.Location = New System.Drawing.Point(122, 17)
        Me.ComboBoxEditNEpaliMonth.Name = "ComboBoxEditNEpaliMonth"
        Me.ComboBoxEditNEpaliMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNEpaliMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboBoxEditNEpaliMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboBoxEditNEpaliMonth.TabIndex = 36
        '
        'TextEdit11
        '
        Me.TextEdit11.EditValue = "00:00"
        Me.TextEdit11.Location = New System.Drawing.Point(328, 17)
        Me.TextEdit11.Name = "TextEdit11"
        Me.TextEdit11.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit11.Properties.Appearance.Options.UseFont = True
        Me.TextEdit11.Properties.Mask.EditMask = "HH:mm"
        Me.TextEdit11.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit11.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit11.Properties.MaxLength = 5
        Me.TextEdit11.Size = New System.Drawing.Size(47, 20)
        Me.TextEdit11.TabIndex = 22
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(258, 20)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(65, 14)
        Me.LabelControl13.TabIndex = 23
        Me.LabelControl13.Text = "Punch Time"
        '
        'ComboBoxEditNepaliDate
        '
        Me.ComboBoxEditNepaliDate.Enabled = False
        Me.ComboBoxEditNepaliDate.Location = New System.Drawing.Point(74, 17)
        Me.ComboBoxEditNepaliDate.Name = "ComboBoxEditNepaliDate"
        Me.ComboBoxEditNepaliDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDate.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliDate.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDate.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliDate.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboBoxEditNepaliDate.Size = New System.Drawing.Size(42, 20)
        Me.ComboBoxEditNepaliDate.TabIndex = 35
        '
        'TextEdit10
        '
        Me.TextEdit10.Enabled = False
        Me.TextEdit10.Location = New System.Drawing.Point(75, 272)
        Me.TextEdit10.Name = "TextEdit10"
        Me.TextEdit10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit10.Properties.Appearance.Options.UseFont = True
        Me.TextEdit10.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit10.Size = New System.Drawing.Size(93, 20)
        Me.TextEdit10.TabIndex = 21
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(8, 275)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(35, 14)
        Me.LabelControl11.TabIndex = 20
        Me.LabelControl11.Text = "Status"
        '
        'TextEdit9
        '
        Me.TextEdit9.Enabled = False
        Me.TextEdit9.Location = New System.Drawing.Point(75, 246)
        Me.TextEdit9.Name = "TextEdit9"
        Me.TextEdit9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit9.Properties.Appearance.Options.UseFont = True
        Me.TextEdit9.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit9.Size = New System.Drawing.Size(93, 20)
        Me.TextEdit9.TabIndex = 19
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(8, 249)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl10.TabIndex = 18
        Me.LabelControl10.Text = "Over Time"
        '
        'TextEdit5
        '
        Me.TextEdit5.Enabled = False
        Me.TextEdit5.Location = New System.Drawing.Point(149, 178)
        Me.TextEdit5.Name = "TextEdit5"
        Me.TextEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit5.Properties.Appearance.Options.UseFont = True
        Me.TextEdit5.Properties.Mask.EditMask = "HH:mm"
        Me.TextEdit5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit5.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit5.Size = New System.Drawing.Size(93, 20)
        Me.TextEdit5.TabIndex = 17
        '
        'TextEdit6
        '
        Me.TextEdit6.Enabled = False
        Me.TextEdit6.Location = New System.Drawing.Point(50, 178)
        Me.TextEdit6.Name = "TextEdit6"
        Me.TextEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit6.Properties.Appearance.Options.UseFont = True
        Me.TextEdit6.Properties.Mask.EditMask = "HH:mm"
        Me.TextEdit6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit6.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit6.Size = New System.Drawing.Size(93, 20)
        Me.TextEdit6.TabIndex = 16
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(8, 181)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(27, 14)
        Me.LabelControl6.TabIndex = 15
        Me.LabelControl6.Text = "Time"
        '
        'TextEdit7
        '
        Me.TextEdit7.Enabled = False
        Me.TextEdit7.Location = New System.Drawing.Point(149, 152)
        Me.TextEdit7.Name = "TextEdit7"
        Me.TextEdit7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit7.Properties.Appearance.Options.UseFont = True
        Me.TextEdit7.Properties.Mask.EditMask = "d"
        Me.TextEdit7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit7.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit7.Size = New System.Drawing.Size(93, 20)
        Me.TextEdit7.TabIndex = 14
        '
        'TextEdit8
        '
        Me.TextEdit8.Enabled = False
        Me.TextEdit8.Location = New System.Drawing.Point(50, 152)
        Me.TextEdit8.Name = "TextEdit8"
        Me.TextEdit8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit8.Properties.Appearance.Options.UseFont = True
        Me.TextEdit8.Properties.Mask.EditMask = "d"
        Me.TextEdit8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit8.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit8.Size = New System.Drawing.Size(93, 20)
        Me.TextEdit8.TabIndex = 13
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(167, 131)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl7.TabIndex = 12
        Me.LabelControl7.Text = "Lunch Out"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(67, 131)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl8.TabIndex = 11
        Me.LabelControl8.Text = "Lunch In"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(8, 155)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(26, 14)
        Me.LabelControl9.TabIndex = 10
        Me.LabelControl9.Text = "Date"
        '
        'TextEdit4
        '
        Me.TextEdit4.Enabled = False
        Me.TextEdit4.Location = New System.Drawing.Point(149, 85)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit4.Properties.Appearance.Options.UseFont = True
        Me.TextEdit4.Properties.Mask.EditMask = "HH:mm"
        Me.TextEdit4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit4.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit4.Size = New System.Drawing.Size(93, 20)
        Me.TextEdit4.TabIndex = 9
        '
        'TextEdit3
        '
        Me.TextEdit3.Enabled = False
        Me.TextEdit3.Location = New System.Drawing.Point(50, 85)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit3.Properties.Appearance.Options.UseFont = True
        Me.TextEdit3.Properties.Mask.EditMask = "HH:mm"
        Me.TextEdit3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit3.Size = New System.Drawing.Size(93, 20)
        Me.TextEdit3.TabIndex = 8
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(8, 88)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(27, 14)
        Me.LabelControl5.TabIndex = 7
        Me.LabelControl5.Text = "Time"
        '
        'TextEdit2
        '
        Me.TextEdit2.Enabled = False
        Me.TextEdit2.Location = New System.Drawing.Point(149, 59)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.Mask.EditMask = "d"
        Me.TextEdit2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit2.Size = New System.Drawing.Size(93, 20)
        Me.TextEdit2.TabIndex = 6
        '
        'TextEdit1
        '
        Me.TextEdit1.Enabled = False
        Me.TextEdit1.Location = New System.Drawing.Point(50, 59)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.Mask.EditMask = "d"
        Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit1.Size = New System.Drawing.Size(93, 20)
        Me.TextEdit1.TabIndex = 5
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(167, 38)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(50, 14)
        Me.LabelControl4.TabIndex = 4
        Me.LabelControl4.Text = "Shift Out"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(67, 38)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(40, 14)
        Me.LabelControl3.TabIndex = 3
        Me.LabelControl3.Text = "Shift In"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(8, 62)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(26, 14)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Date"
        '
        'MachineRawPunchTableAdapter
        '
        Me.MachineRawPunchTableAdapter.ClearBeforeFill = True
        '
        'MachineRawPunchBindingSource
        '
        Me.MachineRawPunchBindingSource.DataMember = "MachineRawPunch"
        Me.MachineRawPunchBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.PanelControl3)
        Me.GroupControl3.Controls.Add(Me.PanelControl2)
        Me.GroupControl3.Controls.Add(Me.SimpleButton3)
        Me.GroupControl3.Location = New System.Drawing.Point(739, 13)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(280, 393)
        Me.GroupControl3.TabIndex = 2
        Me.GroupControl3.Text = "Leave"
        '
        'PanelControl3
        '
        Me.PanelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl3.Controls.Add(Me.btnUpdateLeave)
        Me.PanelControl3.Controls.Add(Me.LabelControl22)
        Me.PanelControl3.Controls.Add(Me.TextEditReasonLeave)
        Me.PanelControl3.Controls.Add(Me.GridLookUpEdit2)
        Me.PanelControl3.Controls.Add(Me.LabelControl23)
        Me.PanelControl3.Location = New System.Drawing.Point(9, 166)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(264, 146)
        Me.PanelControl3.TabIndex = 34
        '
        'btnUpdateLeave
        '
        Me.btnUpdateLeave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnUpdateLeave.Appearance.Options.UseFont = True
        Me.btnUpdateLeave.Location = New System.Drawing.Point(25, 90)
        Me.btnUpdateLeave.Name = "btnUpdateLeave"
        Me.btnUpdateLeave.Size = New System.Drawing.Size(105, 23)
        Me.btnUpdateLeave.TabIndex = 34
        Me.btnUpdateLeave.Text = "Update Leave"
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Location = New System.Drawing.Point(11, 35)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl22.TabIndex = 32
        Me.LabelControl22.Text = "Leave Code"
        '
        'TextEditReasonLeave
        '
        Me.TextEditReasonLeave.Location = New System.Drawing.Point(89, 58)
        Me.TextEditReasonLeave.Name = "TextEditReasonLeave"
        Me.TextEditReasonLeave.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditReasonLeave.Properties.Appearance.Options.UseFont = True
        Me.TextEditReasonLeave.Properties.MaxLength = 25
        Me.TextEditReasonLeave.Size = New System.Drawing.Size(166, 20)
        Me.TextEditReasonLeave.TabIndex = 31
        '
        'GridLookUpEdit2
        '
        Me.GridLookUpEdit2.Location = New System.Drawing.Point(89, 32)
        Me.GridLookUpEdit2.Name = "GridLookUpEdit2"
        Me.GridLookUpEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit2.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit2.Properties.NullText = ""
        Me.GridLookUpEdit2.Properties.View = Me.GridView1
        Me.GridLookUpEdit2.Size = New System.Drawing.Size(166, 20)
        Me.GridLookUpEdit2.TabIndex = 30
        '
        'GridView1
        '
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(11, 60)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(39, 14)
        Me.LabelControl23.TabIndex = 33
        Me.LabelControl23.Text = "Reason"
        '
        'PanelControl2
        '
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl2.Controls.Add(Me.CheckEdit6)
        Me.PanelControl2.Controls.Add(Me.CheckEdit5)
        Me.PanelControl2.Controls.Add(Me.CheckEdit4)
        Me.PanelControl2.Controls.Add(Me.CheckEdit3)
        Me.PanelControl2.Controls.Add(Me.CheckEdit2)
        Me.PanelControl2.Controls.Add(Me.CheckEdit1)
        Me.PanelControl2.Location = New System.Drawing.Point(9, 27)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(264, 133)
        Me.PanelControl2.TabIndex = 8
        '
        'CheckEdit6
        '
        Me.CheckEdit6.Location = New System.Drawing.Point(142, 66)
        Me.CheckEdit6.Name = "CheckEdit6"
        Me.CheckEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit6.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit6.Properties.Caption = "Second Half"
        Me.CheckEdit6.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit6.Properties.RadioGroupIndex = 1
        Me.CheckEdit6.Size = New System.Drawing.Size(100, 19)
        Me.CheckEdit6.TabIndex = 18
        Me.CheckEdit6.TabStop = False
        Me.CheckEdit6.Visible = False
        '
        'CheckEdit5
        '
        Me.CheckEdit5.EditValue = True
        Me.CheckEdit5.Location = New System.Drawing.Point(142, 43)
        Me.CheckEdit5.Name = "CheckEdit5"
        Me.CheckEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit5.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit5.Properties.Caption = "First Half"
        Me.CheckEdit5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit5.Properties.RadioGroupIndex = 1
        Me.CheckEdit5.Size = New System.Drawing.Size(100, 19)
        Me.CheckEdit5.TabIndex = 17
        Me.CheckEdit5.Visible = False
        '
        'CheckEdit4
        '
        Me.CheckEdit4.EditValue = True
        Me.CheckEdit4.Location = New System.Drawing.Point(24, 91)
        Me.CheckEdit4.Name = "CheckEdit4"
        Me.CheckEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit4.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit4.Properties.Caption = "Full Day"
        Me.CheckEdit4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit4.Properties.RadioGroupIndex = 0
        Me.CheckEdit4.Size = New System.Drawing.Size(116, 19)
        Me.CheckEdit4.TabIndex = 16
        '
        'CheckEdit3
        '
        Me.CheckEdit3.Location = New System.Drawing.Point(24, 66)
        Me.CheckEdit3.Name = "CheckEdit3"
        Me.CheckEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit3.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit3.Properties.Caption = "Three Forth"
        Me.CheckEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit3.Properties.RadioGroupIndex = 0
        Me.CheckEdit3.Size = New System.Drawing.Size(116, 19)
        Me.CheckEdit3.TabIndex = 15
        Me.CheckEdit3.TabStop = False
        '
        'CheckEdit2
        '
        Me.CheckEdit2.Location = New System.Drawing.Point(24, 43)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit2.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit2.Properties.Caption = "Half"
        Me.CheckEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit2.Properties.RadioGroupIndex = 0
        Me.CheckEdit2.Size = New System.Drawing.Size(116, 19)
        Me.CheckEdit2.TabIndex = 14
        Me.CheckEdit2.TabStop = False
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(24, 18)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit1.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit1.Properties.Caption = "Quarter"
        Me.CheckEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit1.Properties.RadioGroupIndex = 0
        Me.CheckEdit1.Size = New System.Drawing.Size(116, 19)
        Me.CheckEdit1.TabIndex = 13
        Me.CheckEdit1.TabStop = False
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton3.Appearance.Options.UseFont = True
        Me.SimpleButton3.Location = New System.Drawing.Point(531, 28)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(114, 23)
        Me.SimpleButton3.TabIndex = 7
        Me.SimpleButton3.Text = "Change Shift"
        '
        'XtraDataMaintenanceEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1027, 419)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControlShift)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraDataMaintenanceEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.GroupControlShift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControlShift.ResumeLayout(False)
        Me.GroupControlShift.PerformLayout()
        CType(Me.CheckEditShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditOff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit3.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        CType(Me.ComboBoxEditNepaliYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNepaliDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MachineRawPunchBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.TextEditReasonLeave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.CheckEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControlShift As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridLookUpEdit1 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTARTTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHDURATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORDERINF12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTSTARTAFTER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDEDUCTHRS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHDEDUCTION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTPOSITION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTDURATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDEDUCTAFTER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblShiftMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblShiftMasterTableAdapter As iAS.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter
    Friend WithEvents TblShiftMaster1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblShiftMaster1TableAdapter
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents TextEdit11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colOFFICEPUNCH As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents colOFFICEPUNCH1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents colP_DAY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents MachineRawPunchTableAdapter As iAS.SSSDBDataSetTableAdapters.MachineRawPunchTableAdapter
    Friend WithEvents MachineRawPunchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CheckEditShift As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditOff As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckEdit6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditReasonLeave As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridLookUpEdit2 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnUpdateLeave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ComboBoxEditNepaliYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNEpaliMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNepaliDate As DevExpress.XtraEditors.ComboBoxEdit
End Class
