﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraMasterMenu
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.NavigationPane1 = New DevExpress.XtraBars.Navigation.NavigationPane()
        Me.NavigationPage2 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage3 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage5 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage6 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage7 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage8 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage4 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage1 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage9 = New DevExpress.XtraBars.Navigation.NavigationPage()
        Me.NavigationPage10 = New DevExpress.XtraBars.Navigation.NavigationPage()
        CType(Me.NavigationPane1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NavigationPane1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavigationPane1
        '
        Me.NavigationPane1.AllowTransitionAnimation = DevExpress.Utils.DefaultBoolean.[True]
        Me.NavigationPane1.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 18.0!)
        Me.NavigationPane1.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.Black
        Me.NavigationPane1.AppearanceButton.Hovered.Options.UseFont = True
        Me.NavigationPane1.AppearanceButton.Hovered.Options.UseForeColor = True
        Me.NavigationPane1.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 13.0!)
        Me.NavigationPane1.AppearanceButton.Normal.ForeColor = System.Drawing.Color.Black
        Me.NavigationPane1.AppearanceButton.Normal.Options.UseFont = True
        Me.NavigationPane1.AppearanceButton.Normal.Options.UseForeColor = True
        Me.NavigationPane1.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 13.0!)
        Me.NavigationPane1.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.RoyalBlue
        Me.NavigationPane1.AppearanceButton.Pressed.Options.UseFont = True
        Me.NavigationPane1.AppearanceButton.Pressed.Options.UseForeColor = True
        Me.NavigationPane1.Controls.Add(Me.NavigationPage2)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage3)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage5)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage6)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage7)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage8)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage4)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage1)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage9)
        Me.NavigationPane1.Controls.Add(Me.NavigationPage10)
        Me.NavigationPane1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationPane1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.NavigationPane1.Location = New System.Drawing.Point(0, 0)
        Me.NavigationPane1.LookAndFeel.SkinName = "iMaginary"
        Me.NavigationPane1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.NavigationPane1.Name = "NavigationPane1"
        Me.NavigationPane1.PageProperties.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.NavigationPane1.PageProperties.AppearanceCaption.Options.UseFont = True
        Me.NavigationPane1.PageProperties.ShowCollapseButton = False
        Me.NavigationPane1.PageProperties.ShowExpandButton = False
        Me.NavigationPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.NavigationPage1, Me.NavigationPage2, Me.NavigationPage3, Me.NavigationPage4, Me.NavigationPage5, Me.NavigationPage6, Me.NavigationPage7, Me.NavigationPage8, Me.NavigationPage9, Me.NavigationPage10})
        Me.NavigationPane1.RegularSize = New System.Drawing.Size(1370, 660)
        Me.NavigationPane1.SelectedPage = Me.NavigationPage1
        Me.NavigationPane1.Size = New System.Drawing.Size(1370, 660)
        Me.NavigationPane1.TabIndex = 0
        Me.NavigationPane1.TransitionAnimationProperties.FrameCount = 750
        Me.NavigationPane1.TransitionAnimationProperties.FrameInterval = 5000
        Me.NavigationPane1.TransitionType = DevExpress.Utils.Animation.Transitions.Cover
        '
        'NavigationPage2
        '
        Me.NavigationPage2.Caption = "NavigationPage2"
        Me.NavigationPage2.Name = "NavigationPage2"
        Me.NavigationPage2.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText
        Me.NavigationPage2.Size = New System.Drawing.Size(1166, 602)
        '
        'NavigationPage3
        '
        Me.NavigationPage3.Caption = "NavigationPage3"
        Me.NavigationPage3.Name = "NavigationPage3"
        Me.NavigationPage3.Size = New System.Drawing.Size(1370, 660)
        '
        'NavigationPage5
        '
        Me.NavigationPage5.Caption = " "
        Me.NavigationPage5.Name = "NavigationPage5"
        Me.NavigationPage5.Size = New System.Drawing.Size(1370, 660)
        '
        'NavigationPage6
        '
        Me.NavigationPage6.Caption = " "
        Me.NavigationPage6.Name = "NavigationPage6"
        Me.NavigationPage6.Size = New System.Drawing.Size(1370, 660)
        '
        'NavigationPage7
        '
        Me.NavigationPage7.Caption = " "
        Me.NavigationPage7.Name = "NavigationPage7"
        Me.NavigationPage7.Size = New System.Drawing.Size(1370, 660)
        '
        'NavigationPage8
        '
        Me.NavigationPage8.Caption = " "
        Me.NavigationPage8.Name = "NavigationPage8"
        Me.NavigationPage8.Size = New System.Drawing.Size(1370, 660)
        '
        'NavigationPage4
        '
        Me.NavigationPage4.Caption = "NavigationPage4"
        Me.NavigationPage4.Name = "NavigationPage4"
        Me.NavigationPage4.Size = New System.Drawing.Size(1370, 660)
        '
        'NavigationPage1
        '
        Me.NavigationPage1.Caption = "Company"
        Me.NavigationPage1.Name = "NavigationPage1"
        Me.NavigationPage1.PageText = "Company"
        Me.NavigationPage1.Properties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.ImageAndText
        Me.NavigationPage1.Size = New System.Drawing.Size(1148, 602)
        '
        'NavigationPage9
        '
        Me.NavigationPage9.Caption = " "
        Me.NavigationPage9.Name = "NavigationPage9"
        Me.NavigationPage9.Size = New System.Drawing.Size(1370, 660)
        '
        'NavigationPage10
        '
        Me.NavigationPage10.Caption = "Employee"
        Me.NavigationPage10.Name = "NavigationPage10"
        Me.NavigationPage10.Size = New System.Drawing.Size(1370, 660)
        '
        'XtraMasterMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.NavigationPane1)
        Me.Name = "XtraMasterMenu"
        Me.Size = New System.Drawing.Size(1370, 660)
        CType(Me.NavigationPane1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NavigationPane1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NavigationPage2 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage3 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage4 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage5 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage6 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage7 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage8 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage1 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage9 As DevExpress.XtraBars.Navigation.NavigationPage
    Friend WithEvents NavigationPage10 As DevExpress.XtraBars.Navigation.NavigationPage
    Public WithEvents NavigationPane1 As DevExpress.XtraBars.Navigation.NavigationPane

End Class
