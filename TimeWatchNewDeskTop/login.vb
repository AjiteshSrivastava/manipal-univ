﻿Imports System.Resources
Imports System.Globalization
Imports System.IO
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Net.NetworkInformation
Imports System.Net
Imports System.Text
Imports System.Net.Http
Imports System.Web.Script.Serialization
Imports System.Web
Imports System.Net.Sockets
Imports System.Runtime.Serialization.Json
Imports Newtonsoft.Json

Public Class login
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info
    Public servername As String
    Dim con As SqlConnection
    Dim con1 As OleDbConnection
    Public ConnectionString As String
    Dim adap1, adap As SqlDataAdapter
    Dim ds As DataSet
    Dim ulf As UserLookAndFeel
    <STAThread()>
    Shared Sub Main()
        DevExpress.UserSkins.BonusSkins.Register()
        'WindowsFormsSettings.DefaultFont = New System.Drawing.Font("Tahoma", 9)
        'DevExpress.XtraBars.BarAndDockingController.Default.AppearancesBar.MainMenu.Font = New Font("Tahoma", 14)
        Application.Run(New login())
    End Sub
    Private Sub login_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'MsgBox(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.LongDatePattern() & _
        '      vbCrLf & System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern() & _
        '       vbCrLf & System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.LongTimePattern() & _
        '        vbCrLf & System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern())
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        Me.Top = (My.Computer.Screen.WorkingArea.Height / 2) - (Me.Height / 2)
        Me.Left = (My.Computer.Screen.WorkingArea.Width / 2) - (Me.Width / 2)

        Common.res_man = New ResourceManager("iAS.Res", GetType(login).Assembly)
        Common.cul = CultureInfo.CreateSpecificCulture("en")
        Me.Text = "Integrated Attendance System"
        LabelControl1.Text = Common.res_man.GetString("userid_lb", Common.cul)
        LabelControl2.Text = Common.res_man.GetString("password_lb", Common.cul)
        SimpleButton1.Text = Common.res_man.GetString("login_bt", Common.cul)
        SimpleButton2.Text = Common.res_man.GetString("close", Common.cul)
        SimpleButton3.Text = Common.res_man.GetString("db_setting", Common.cul)
        Application.DoEvents()

        'If System.DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss") >= Convert.ToDateTime("2019-09-01 00:00:00") Then
        '    XtraMessageBox.Show(ulf, "<size=10>License Expired</size>", "Error")
        '    Close()
        'End If

        Application.DoEvents()
        'Dim p() As Process
        'p = Process.GetProcessesByName("iAS")
        'If p.Count > 0 Then
        '    XtraMessageBox.Show(ulf, "<size=10>Application Already Running</size>", "Error")
        '    Exit Sub
        '    Application.Exit()
        'End If

        'Example #1
        'Dim DC As New DateConverter(New Date(2018, 6, 19))
        'MessageBox.Show("Date in BS : " & DC.ToBS())
        'MessageBox.Show("Unix Timestamp: " & DC.GetUnixTimestamp())

        ''Example #2
        'Dim DC As New DateConverter()
        'MessageBox.Show("1996/1/21 in BS : " & DC.ToBS(New Date(1996, 1, 21)))
        'MessageBox.Show("2018/06/19 in AD : " & DC.ToAD(New Date(2075, 3, 36)))

        Dim RealTimeImagePath As String = System.Environment.CurrentDirectory & "\RealTimeImages"
        If (Not System.IO.Directory.Exists(RealTimeImagePath)) Then
            System.IO.Directory.CreateDirectory(RealTimeImagePath)
        End If
        Dim datafile As String = System.Environment.CurrentDirectory & "\Data"
        If (Not System.IO.Directory.Exists(datafile)) Then
            System.IO.Directory.CreateDirectory(datafile)
        End If
        Application.DoEvents()
        Common.test = 10

        'Me.Text = Common.res_man.GetString("login_bt", Common.cul)

        If Not System.IO.File.Exists("db.txt") Then
            File.Create("db.txt").Dispose()
        End If
        Application.DoEvents()
        Dim fs As FileStream = New FileStream("db.txt", FileMode.Open, FileAccess.Read)
        Dim sr As StreamReader = New StreamReader(fs)
        Dim str As String
        Dim str1() As String
        'Dim dbname As String
        Do While sr.Peek <> -1
            str = sr.ReadLine
            str1 = str.Split(",")
            Common.servername = str1(0)
        Loop

        sr.Close()
        fs.Close()
        If Common.servername = "" Then
            Me.Hide()
            XtraDB.ShowDialog()
            Exit Sub
        End If
        If Common.servername = "Access" Then
            ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Common.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Common.con1 = New OleDbConnection(Common.ConnectionString)

            Try
                Common.con1.Open()
                Common.con1.Close()
            Catch ex As Exception
                Me.Hide()
                XtraDB.ShowDialog()
                Exit Sub
            End Try

        Else
            Try
                Common.DB = str1(1)
                If str1(2) = "Win" Then
                    ConnectionString = "Data Source=" & Common.servername & ";Initial Catalog=" & str1(1) & ";Integrated Security=True"
                    Common.ConnectionString = "Data Source=" & Common.servername & ";Initial Catalog=" & str1(1) & ";Integrated Security=True;MultipleActiveResultSets=true;"
                    Common.SQLUserId = ""
                    Common.SQLPassword = ""
                Else
                    ConnectionString = "Data Source=" & Common.servername & ";Initial Catalog=" & str1(1) & ";User Id=" & str1(3) & ";Password=" & str1(4) & ";"
                    Common.ConnectionString = "Data Source=" & Common.servername & ";Initial Catalog=" & str1(1) & ";User Id=" & str1(3) & ";Password=" & str1(4) & ";MultipleActiveResultSets=true;"
                    Common.SQLUserId = str1(3)
                    Common.SQLPassword = str1(4)
                End If
                Common.con = New SqlConnection(Common.ConnectionString)

                Try
                    Common.con.Open()
                    Common.con.Close()
                Catch ex As Exception
                    Me.Hide()
                    XtraDB.ShowDialog()
                    Exit Sub
                End Try
            Catch ex As Exception
                Me.Hide()
                XtraDB.ShowDialog()
                Exit Sub
            End Try
        End If
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "select * from InstallSystemInfo "
        If Common.servername = "Access" Then
            Try
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Catch ex As Exception
                Dim sSql2 As String = "CREATE TABLE [InstallSystemInfo](	[MAC] longtext ,	[ComName] longtext,	[ComAdd] longtext ,	[ComContact] longtext,	[ComEmail] longtext,	[UserKey] longtext,	[License] longtext, [InstalledDate] datetime ) "
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                Dim cmd1 As New OleDbCommand
                cmd1 = New OleDbCommand(sSql2, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            End Try
        Else
            Dim sSql1 As String = "select InstalledDate from InstallSystemInfo"
            Try
                adap = New SqlDataAdapter(sSql1, Common.con)
                adap.Fill(ds)
            Catch ex As Exception
                sSql1 = "ALTER TABLE InstallSystemInfo ADD InstalledDate datetime"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End Try
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            Dim sSql1 As String = "insert into InstallSystemInfo (InstalledDate) values ('" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
            ds = New DataSet
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                Dim cmd1 As OleDbCommand
                cmd1 = New OleDbCommand(sSql1, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Else
            sSql = "select * from InstallSystemInfo "
            ds = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If

            Dim InstalledDate As DateTime = ds.Tables(0).Rows(0).Item("InstalledDate")
            If ds.Tables(0).Rows(0).Item("MAC").ToString.Trim = "" And Now.Subtract(InstalledDate).TotalDays > 15 Then
                'Me.Hide()
                'XtraCompanyInfo.ShowDialog()
            End If
        End If



        'check iASSystemInfo
        sSql = "select * from iASSystemInfo"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "Create TABLE iASSystemInfo (HSerialNo longtext, ISD longtext, NOU longtext,NOD longtext, Lic longtext, DSerialNo longtext )"
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, Common.con1)
                cmd.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                sSql = "Create TABLE iASSystemInfo (HSerialNo varchar(max), ISD varchar(max), NOU varchar(max),NOD varchar(max), Lic varchar(max), DSerialNo varchar(max))"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try

        license.checkLicenseFile()
        license.getLicenseInfo()
        If license.TrialExpired Then
            XtraMessageBox.Show(ulf, "<size=10>Your Trial Period has expired</size>", "<size=9>iAS</size>")
            Me.Hide()
            XtraCompanyInfo.ShowDialog()
        End If
    End Sub
    Public Class VersionInfo
        Public Property GetVsersionResult As String
    End Class
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "select USER_R , PASSWORD, auth_comp, Auth_dept from tblUser where USER_R = '" & TextEdit1.Text.Trim & "' and PASSWORD = '" & TextEdit2.Text.Trim & "' "
        If Common.servername = "Access" Then
            'MsgBox("Access")
            'con1 = New OleDbConnection(ConnectionString)
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            'con = New SqlConnection(ConnectionString)
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        Dim com As Common = New Common
        If ds.Tables(0).Rows.Count = 1 Then
            ProgressBarControl1.Visible = True
            Common.USER_R = TextEdit1.Text.Trim
            'Me.Hide()
            ''XtraForm1.Show()
            ''XtraMaster.Show()
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()

            LabelStatus.Text = "Checking DB Structure..."
            Application.DoEvents()

            Common.CheckNewColumnsInDb()
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()

            'LabelStatus.Text = "Checking Real Time Ports..."
            'Application.DoEvents()

            Dim adapS As SqlDataAdapter
            Dim adapAc As OleDbDataAdapter
            Dim Rs As DataSet = New DataSet
            If Common.servername = "Access" Then
                sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
                adapAc = New OleDbDataAdapter(sSql, Common.con1)
                adapAc.Fill(Rs)
            Else
                sSql = " Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup )"
                adapS = New SqlDataAdapter(sSql, Common.con)
                adapS.Fill(Rs)
            End If
            Common.BioPort = Rs.Tables(0).Rows(0).Item("BioPort").ToString
            Common.ZKPort = Rs.Tables(0).Rows(0).Item("ZKPort").ToString
            Common.TWIR102Port = Rs.Tables(0).Rows(0).Item("TWIR102Port").ToString
            Common.online = Rs.Tables(0).Rows(0).Item("Online").ToString
            Common.IsNepali = Rs.Tables(0).Rows(0).Item("IsNepali").ToString
            Common.TimerDur = Rs.Tables(0).Rows(0).Item("TimerDur").ToString

            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()


            If Rs.Tables(0).Rows(0).Item("AutoDownload").ToString = "Y" Then
                If XtraMessageBox.Show(ulf, "<size=10>Do you want to download logs from devices now</size>", "<size=9>Confirmation</size>", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    Common.LogDownLoadCounter = 1
                    '    Common.Load_SMS_Policy()
                    '    XtraAutoDownloadLogs.ShowDialog()
                Else
                    Common.LogDownLoadCounter = 0
                    'Common.Load_Corporate_PolicySql()
                    'XtraMaster.Show()
                End If
            Else
                Common.LogDownLoadCounter = 0
            End If

            Common.LoadUserMgmt(TextEdit1.Text.Trim)
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()


            Common.Load_Corporate_PolicySql()
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()

            Common.GetNextBckTime()
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()

            Common.loadEmp()
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()

            Common.loadCompany()
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()

            Common.loadLocation()
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()

            Common.loadDevice()
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()

            Common.SetEmpGrpId()
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()

            Common.LoadGroupStruct()
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()

            'Common.getMacAddress()
            Common.getInstallSystemInfo()
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()

            Common.LoadParallelDB()
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()

            XtraFullPayrollMenuMaster.PayEmpMster = "E"
            'checkupdate 
            LabelStatus.Text = "Checking Application Update..."
            Application.DoEvents()

            'Try
            '    Dim proxy As WebClient = New WebClient
            '    Dim serviceURL As String = String.Format("http://137.59.201.60:5011/Service1.svc/GetVsersion/iASDesk")
            '    Dim data() As Byte = proxy.DownloadData(serviceURL)
            '    Dim stream As Stream = New MemoryStream(data)
            '    Dim OutPut As String = System.Text.Encoding.UTF8.GetString(data)
            '    Dim JsonCardInfoCount As VersionInfo = New VersionInfo()
            '    JsonCardInfoCount = JsonConvert.DeserializeObject(Of VersionInfo)(OutPut)
            '    Dim ServerVersion As String = JsonCardInfoCount.GetVsersionResult
            '    If XtraAppInfo.getVersion <> ServerVersion Then
            '        If XtraMessageBox.Show(ulf, "<size=10>Newer Version Is Available. " & vbCrLf & "Do you wish to download " & ServerVersion & "?</size>", "<size=9>Confirm</size>",
            '                          MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            '            Application.Exit()
            '            Dim procStartInfo As New ProcessStartInfo
            '            Dim procExecuting As New Process
            '            With procStartInfo
            '                .UseShellExecute = True
            '                .FileName = My.Application.Info.DirectoryPath & "\iASUpdater.exe"
            '                .WindowStyle = ProcessWindowStyle.Normal
            '                .Verb = "runas" 'add this to prompt for elevation
            '            End With
            '            procExecuting = Process.Start(procStartInfo)
            '            'Process.Start(My.Application.Info.DirectoryPath & "\iASUpdater.exe")
            '            Exit Sub
            '        End If
            '    End If
            'Catch ex As Exception

            'End Try


            'Dim errorMsg As String = ""
            'Dim newVersion As String = ""
            'If XtraAppInfo.checkUpdate(errorMsg, newVersion) Then
            '    If XtraMessageBox.Show(ulf, "<size=10>Newer Version Is Available. " & vbCrLf & "Do you wish to download " & newVersion & "?</size>", "<size=9>Confirm</size>", _
            '                      MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            '        Application.Exit()
            '        Dim procStartInfo As New ProcessStartInfo
            '        Dim procExecuting As New Process
            '        With procStartInfo
            '            .UseShellExecute = True
            '            .FileName = My.Application.Info.DirectoryPath & "\iASUpdater.exe"
            '            .WindowStyle = ProcessWindowStyle.Normal
            '            .Verb = "runas" 'add this to prompt for elevation
            '        End With
            '        procExecuting = Process.Start(procStartInfo)
            '        'Process.Start(My.Application.Info.DirectoryPath & "\iASUpdater.exe")
            '        Exit Sub
            '    End If
            'End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end checkupdate 


            'check for CMITech.UMXClient.dll
            LabelStatus.Text = "Checking Missing files..."
            Application.DoEvents()
            Dim outputStream As FileStream
            Dim SavePath As String = My.Application.Info.DirectoryPath & "\CMITech.UMXClient.dll"
            If System.IO.File.Exists(SavePath) = False Then
                'If XtraMessageBox.Show(ulf, "<size=10>Dll for EF45 Device Missing       " & vbCrLf & "Do you wish to download?</size>", "<size=9>Confirm</size>", _
                '                 MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\CMITech.UMXClient.dll", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/CMITech.UMXClient.dll")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    'XtraMessageBox.Show(ulf, "<size=10>" & ex2.Message.ToString.Trim & "</size>", "<size=10>Update Failed</size>")
                    outputStream.Close()
                    My.Computer.FileSystem.DeleteFile(SavePath)
                End Try
                'End If
                'XtraMessageBox.Show(ulf, "<size=10>Download Success</size>", "<size=10>iAS</size>")
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check for CMITech.UMXClient.dll


            'check EmployeeUpload file
            SavePath = My.Application.Info.DirectoryPath & "\Employee_Upload_New.xls"
            If System.IO.File.Exists(SavePath) = False Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\Employee_Upload_New.xls", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/Employee_Upload.xls")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    'XtraMessageBox.Show(ulf, "<size=10>" & ex2.Message.ToString.Trim & "</size>", "<size=10>Update Failed</size>")
                    Try
                        outputStream.Close()
                        My.Computer.FileSystem.DeleteFile(SavePath)
                    Catch ex As Exception
                    End Try
                End Try
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check EmployeeUpload file

            'check Oracle.DataAccess.dll file
            SavePath = My.Application.Info.DirectoryPath & "\Oracle.DataAccess.dll"
            If System.IO.File.Exists(SavePath) = False Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\Oracle.DataAccess.dll", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/Oracle.DataAccess.dll")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    outputStream.Close()
                    My.Computer.FileSystem.DeleteFile(SavePath)
                End Try
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check Oracle.DataAccess.dll file


            'check MySql.Data.dll file
            SavePath = My.Application.Info.DirectoryPath & "\MySql.Data.dll"
            If System.IO.File.Exists(SavePath) = False Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\MySql.Data.dll", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/MySql.Data.dll")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    'XtraMessageBox.Show(ulf, "<size=10>" & ex2.Message.ToString.Trim & "</size>", "<size=10>Update Failed</size>")
                    outputStream.Close()
                    My.Computer.FileSystem.DeleteFile(SavePath)
                End Try
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check Oracle.DataAccess.dll file


            'check tcpServer.dll file
            SavePath = My.Application.Info.DirectoryPath & "\tcpServer.dll"
            If System.IO.File.Exists(SavePath) = False Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\tcpServer.dll", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/tcpServer.dll")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    'XtraMessageBox.Show(ulf, "<size=10>" & ex2.Message.ToString.Trim & "</size>", "<size=10>Update Failed</size>")
                    outputStream.Close()
                    My.Computer.FileSystem.DeleteFile(SavePath)
                End Try
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check tcpServer.dll file

            'check ComMain.dll file
            SavePath = My.Application.Info.DirectoryPath & "\ComMain.dll"
            If System.IO.File.Exists(SavePath) = False Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\ComMain.dll", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/ComMain.dll")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    'XtraMessageBox.Show(ulf, "<size=10>" & ex2.Message.ToString.Trim & "</size>", "<size=10>Update Failed</size>")
                    outputStream.Close()
                    My.Computer.FileSystem.DeleteFile(SavePath)
                End Try
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check ComMain.dll file


            'check CoComDataLayermMain.dll file
            SavePath = My.Application.Info.DirectoryPath & "\ComDataLayer.dll"
            If System.IO.File.Exists(SavePath) = False Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\ComDataLayer.dll", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/ComDataLayer.dll")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    'XtraMessageBox.Show(ulf, "<size=10>" & ex2.Message.ToString.Trim & "</size>", "<size=10>Update Failed</size>")
                    outputStream.Close()
                    My.Computer.FileSystem.DeleteFile(SavePath)
                End Try
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check ComDataLayer.dll file


            'check ComFaceLayer.dll file
            SavePath = My.Application.Info.DirectoryPath & "\ComFaceLayer.dll"
            If System.IO.File.Exists(SavePath) = False Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\ComFaceLayer.dll", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/ComFaceLayer.dll")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    'XtraMessageBox.Show(ulf, "<size=10>" & ex2.Message.ToString.Trim & "</size>", "<size=10>Update Failed</size>")
                    outputStream.Close()
                    My.Computer.FileSystem.DeleteFile(SavePath)
                End Try
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check ComFaceLayer.dll file


            'check ComFingerConv.dll file
            SavePath = My.Application.Info.DirectoryPath & "\ComFingerConv.dll"
            If System.IO.File.Exists(SavePath) = False Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\ComFingerConv.dll", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/ComFingerConv.dll")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    'XtraMessageBox.Show(ulf, "<size=10>" & ex2.Message.ToString.Trim & "</size>", "<size=10>Update Failed</size>")
                    outputStream.Close()
                    My.Computer.FileSystem.DeleteFile(SavePath)
                End Try
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check ComFingerConv.dll file


            'check ComFingerKeeperDevice.dll file
            SavePath = My.Application.Info.DirectoryPath & "\ComFingerKeeperDevice.dll"
            If System.IO.File.Exists(SavePath) = False Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\ComFingerKeeperDevice.dll", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/ComFingerKeeperDevice.dll")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    'XtraMessageBox.Show(ulf, "<size=10>" & ex2.Message.ToString.Trim & "</size>", "<size=10>Update Failed</size>")
                    outputStream.Close()
                    My.Computer.FileSystem.DeleteFile(SavePath)
                End Try
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check ComFingerKeeperDevice.dll file


            'check ComFnLayer.dll file
            SavePath = My.Application.Info.DirectoryPath & "\ComFnLayer.dll"
            If System.IO.File.Exists(SavePath) = False Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\ComFnLayer.dll", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/ComFnLayer.dll")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    'XtraMessageBox.Show(ulf, "<size=10>" & ex2.Message.ToString.Trim & "</size>", "<size=10>Update Failed</size>")
                    outputStream.Close()
                    My.Computer.FileSystem.DeleteFile(SavePath)
                End Try
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check ComFnLayer.dll file


            'check ComLinuxDevice.dll file
            SavePath = My.Application.Info.DirectoryPath & "\ComLinuxDevice.dll"
            If System.IO.File.Exists(SavePath) = False Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\ComLinuxDevice.dll", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/ComLinuxDevice.dll")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    'XtraMessageBox.Show(ulf, "<size=10>" & ex2.Message.ToString.Trim & "</size>", "<size=10>Update Failed</size>")
                    outputStream.Close()
                    My.Computer.FileSystem.DeleteFile(SavePath)
                End Try
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check ComLinuxDevice.dll file



            'check ComLinuxMachine.dll file
            SavePath = My.Application.Info.DirectoryPath & "\ComLinuxMachine.dll"
            If System.IO.File.Exists(SavePath) = False Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\ComLinuxMachine.dll", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/ComLinuxMachine.dll")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    'XtraMessageBox.Show(ulf, "<size=10>" & ex2.Message.ToString.Trim & "</size>", "<size=10>Update Failed</size>")
                    outputStream.Close()
                    My.Computer.FileSystem.DeleteFile(SavePath)
                End Try
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check ComLinuxMachine.dll file


            'check RosterUpload.xls file
            SavePath = My.Application.Info.DirectoryPath & "\RosterUpload.xls"
            If System.IO.File.Exists(SavePath) = False Then
                Try
                    Application.DoEvents()
                    Dim reqFTP As FtpWebRequest
                    Dim filePath As String = My.Application.Info.DirectoryPath
                    outputStream = New FileStream(filePath & "\RosterUpload.xls", FileMode.Create)
                    'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                    reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/RosterUpload.xls")), FtpWebRequest)
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                    reqFTP.UseBinary = True
                    'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                    Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                    Dim ftpStream As Stream = response.GetResponseStream
                    Dim cl As Long = response.ContentLength
                    Dim bufferSize As Integer = 25000 ' 2048
                    Dim readCount As Integer
                    Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                    readCount = ftpStream.Read(buffer, 0, bufferSize)

                    While (readCount > 0)
                        outputStream.Write(buffer, 0, readCount)
                        readCount = ftpStream.Read(buffer, 0, bufferSize)
                        Application.DoEvents()
                    End While
                    ftpStream.Close()
                    outputStream.Close()
                    response.Close()
                Catch ex2 As Exception
                    'XtraMessageBox.Show(ulf, "<size=10>" & ex2.Message.ToString.Trim & "</size>", "<size=10>Update Failed</size>")
                    outputStream.Close()
                    My.Computer.FileSystem.DeleteFile(SavePath)
                End Try
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()
            'end check RosterUpload.xls file


            'check datetime format
            LabelStatus.Text = "Checking System DateTime..."
            Application.DoEvents()

            If System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.LongDatePattern() <> "dd MMMM yyyy" Or
                System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern() <> "dd/MM/yyyy" Or
                System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.LongTimePattern() <> "HH:mm:ss" Or
                System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern() <> "hh:mm tt" Then

                If XtraMessageBox.Show(ulf, "<size=10>System DateTime format is different than Required " & vbCrLf & "Do you wish to change it now?</size>", "<size=9>Confirm</size>", _
                                  MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Control Panel\International", True).SetValue("sShortDate", "dd/MM/yyyy")
                    Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Control Panel\International", True).SetValue("sLongDate", "dd MMMM yyyy")
                    Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Control Panel\International", True).SetValue("sShortTime", "hh:mm tt")
                    Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Control Panel\International", True).SetValue("sTimeFormat", "HH:mm:ss")
                    Application.Exit()
                    Process.Start(Application.ExecutablePath)
                    Exit Sub
                End If
            End If
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()




            ''check iASno encripted
            'Dim iASno As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\iASno.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            'Dim iASnoCon As OleDbConnection = New OleDbConnection(iASno)
            'Dim x As String = "Select * from SerialNo"
            'Dim adapNo As OleDbDataAdapter = New OleDbDataAdapter(x, iASnoCon)
            'Dim dsNo As DataSet = New DataSet
            'adapNo.Fill(dsNo)

            'If dsNo.Tables(0).Rows(0).Item("SerialNo").ToString.Trim.Length < 4 Then
            '    Try
            '        Dim reqFTP As FtpWebRequest
            '        Dim filePath As String = My.Application.Info.DirectoryPath
            '        outputStream = New FileStream(filePath & "\iASno_New.mdb", FileMode.Create)
            '        'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)

            '        reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/iASno.mdb")), FtpWebRequest)
            '        reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
            '        reqFTP.UseBinary = True
            '        'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
            '        Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
            '        Dim ftpStream As Stream = response.GetResponseStream
            '        Dim cl As Long = response.ContentLength
            '        Dim bufferSize As Integer = 25000 ' 2048
            '        Dim readCount As Integer
            '        Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
            '        readCount = ftpStream.Read(buffer, 0, bufferSize)

            '        While (readCount > 0)
            '            outputStream.Write(buffer, 0, readCount)
            '            readCount = ftpStream.Read(buffer, 0, bufferSize)
            '            Application.DoEvents()
            '        End While
            '        ftpStream.Close()
            '        outputStream.Close()
            '        response.Close()
            '        Dim FileDelete As String = filePath & "\iASno.mdb"
            '        If System.IO.File.Exists(FileDelete) = True Then
            '            System.IO.File.Delete(FileDelete)
            '        End If
            '        My.Computer.FileSystem.RenameFile(filePath & "\iASno_New.mdb", "iASno.mdb")
            '    Catch ex As Exception
            '        outputStream.Close()
            '        XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.ToString.Trim & "</size>", "<size=10>iAS</size>")
            '    End Try
            'End If
            'ProgressBarControl1.PerformStep()
            'ProgressBarControl1.Update()

            Common.HomeLoad = False
            'Common.getSerialNo()
            ProgressBarControl1.PerformStep()
            ProgressBarControl1.Update()


            Common.LogPost("Application Login")

            'end check datetime format
            LabelStatus.Text = ""
            Application.DoEvents()

            Me.Hide()
            XtraMasterTest.Show()
            'XtraMasterDashBoard.Show()
        Else
            XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("loginfail_msg", Common.cul) & "</size>", Common.res_man.GetString("error_title", Common.cul))
        End If
    End Sub
    Private Sub SimpleButton3_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton3.Click
        XtraDB.ShowDialog()
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub
End Class