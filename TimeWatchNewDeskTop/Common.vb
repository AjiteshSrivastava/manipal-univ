﻿Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.Utils
Imports DevExpress.LookAndFeel
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports System.Net
Imports PdfSharp.Pdf  'for pdf report
Imports PdfSharp.Drawing 'for pdf report
Imports System.IO
Imports System.Runtime.InteropServices
Imports PdfSharp.Pdf.IO 'for pdf image 
Imports System.Threading
Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json
Imports System.Net.NetworkInformation
Imports System.Text
Imports CMITech.UMXClient
Imports MySql.Data.MySqlClient
Imports System.Text.RegularExpressions

'Imports Oracle.ManagedDataAccess.Client

Public Class Common
    Dim conSQL As SqlConnection
    Dim con1Access As OleDbConnection
#Region "declare"
    Public Shared h As IntPtr = IntPtr.Zero
    Public Shared NextAutoBackUpDateTime As String
    Public Shared IsAutoBackUp As Boolean
    Public Shared AutoBackUpForDays As String
    Public Shared DeleteAfterAutoBackUp As Boolean = False
    Public Shared BackUpType As String
    Public Shared BackUpPath As String
    Public Shared AccessAutoBackUpPath As String

    Public Shared IOCL As Boolean
    Public Shared HomeLoad As Boolean
    Public Shared dashBoardClick As String
    'Public Shared SerialNo() As String
    Public Shared CryReportType As String  'for payrollreports
    Public Shared CrystalReportTitle As String ' 
    Public Shared SelectionFormula As String

    Public Shared g_MealApplicable As String 'for canteen
    Public Shared tmpwidth As Integer
    Public Shared tmpheight As Integer
    Public Shared LogDownLoadCounter As Integer
    Public Shared frodatetodatetoReportGrid As String = ""
    Public Shared tbl As New Data.DataTable()
    Public Shared ReportMail As Boolean   'to chech in Reportgrid page if it is calling for mail or for regular report
    Public Shared runningDateTime As String '
    Public Shared reportName As String    'for report mail
    Public Shared whereClauseEmail As String = ""  'for report mail
    Public Shared isPDF As Boolean ' for pdf report
    Public Shared BioPort As String
    Public Shared ZKPort As String
    Public Shared TWIR102Port As String
    Public Shared TF01Port As String
    Public Shared online As String
    Public Shared IsNepali As String
    Public Shared TimerDur As String
    Public Shared NepaliMonth() As String = {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"}
    Public Shared SQLUserId As String
    Public Shared SQLPassword As String
    'for user mgmt
    Public Shared USER_R As String
    Public Shared USERDESCRIPRION As String
    Public Shared PASSWORD As String
    Public Shared AdminPASSWORD As String
    Public Shared AutoProcess As String
    Public Shared DataProcess As String
    Public Shared Main As String
    Public Shared V_Transaction As String
    Public Shared Admin As String
    Public Shared Payroll As String
    Public Shared Reports As String
    Public Shared Leave As String
    Public Shared Company As String
    Public Shared Department As String
    Public Shared Section As String
    Public Shared Grade As String
    Public Shared Category As String
    Public Shared Shift As String
    Public Shared Employee As String
    Public Shared Visitor As String
    Public Shared Reason_Card As String
    Public Shared Manual_Attendance As String
    Public Shared OstoOt As String
    Public Shared ShiftChange As String
    Public Shared HoliDay As String
    Public Shared LeaveMaster As String
    Public Shared LeaveApplication As String
    Public Shared LeaveAccural As String
    Public Shared LeaveAccuralAuto As String
    Public Shared TimeOfficeSetup As String
    Public Shared UserPrevilege As String
    Public Shared Verification As String
    Public Shared InstallationSetup As String
    Public Shared EmployeeSetup As String
    Public Shared ArearEntry As String
    Public Shared Advance_Loan As String
    Public Shared Form16 As String
    Public Shared Form16Return As String
    Public Shared payrollFormula As String
    Public Shared PayrollSetup As String
    Public Shared LoanAdjustment As String
    Public Shared TimeOfficeReport As String
    Public Shared VisitorReport As String
    Public Shared PayrollReport As String
    Public Shared RegisterCreation As String
    Public Shared RegisterUpdation As String
    Public Shared BackDateProcess As String
    Public Shared ReProcess As String
    Public Shared OTCAL As String
    Public Shared auth_comp As String
    Public Shared Auth_dept As String
    Public Shared Auth_Branch As String
    Public Shared USERTYPE As String
    Public Shared paycode As String
    Public Shared CompAdd As String
    Public Shared CompModi As String
    Public Shared CompDel As String
    Public Shared DeptAdd As String
    Public Shared DeptModi As String
    Public Shared DeptDel As String
    Public Shared CatAdd As String
    Public Shared CatModi As String
    Public Shared CatDel As String
    Public Shared SecAdd As String
    Public Shared SecModi As String
    Public Shared SecDel As String
    Public Shared GrdAdd As String
    Public Shared GrdModi As String
    Public Shared GrdDel As String
    Public Shared SftAdd As String
    Public Shared SftModi As String
    Public Shared SftDel As String
    Public Shared EmpAdd As String
    Public Shared EmpModi As String
    Public Shared EmpDel As String
    Public Shared DataMaintenance As String
    Public Shared canteen As String
    Public Shared AutoDwnDur As String
    Public Shared MarkInactivDurDay As String
    Public Shared IsIOCL As String
    Public Shared IOCLInterval As Integer
    Public Shared IOCLLink As String
    Public Shared TWCloud As String
    Public Shared IsWDMS As String
    Public Shared iDMS As String
    Public Shared WDMSDBName As String
    Public Shared IsBioSecurity As String
    Public Shared BioSecurityDBName As String
    Public Shared IsZKAccess As String
    Public Shared ZKAccessDBName As String
    Public Shared IsTWAccess As String
    Public Shared TWAccessDBName As String
    Public Shared iDMSDB As String
    Public Shared TWCloudDB As String
    Public Shared OnlineStartUp As String

    Public Shared DeviceMgmt As String
    Public Shared DeviceAdd As String
    Public Shared DeviceModi As String
    Public Shared DeviceDelete As String
    Public Shared LogMgmt As String
    Public Shared UserSetupTemplate As String
    Public Shared DBSetting As String
    Public Shared SMSSetting As String
    Public Shared BulkSMS As String
    Public Shared EmailSetting As String
    Public Shared BackUpSetting As String
    Public Shared ParallelSetting As String
    Public Shared Branch As String
    Public Shared BranchAdd As String
    Public Shared BranchModi As String
    Public Shared BranchDel As String
    Public Shared MonthlyReport As String
    Public Shared DalyReport As String
    Public Shared LeaveReport As String

    'Public Shared SysyemMac As String
    Public Shared DbMac As String
    Public Shared UserKey As String
    Public Shared LicenseStr As String

    Public Shared InstalledDate As String
    Public Shared LastUpdated As String

    Public Shared IsFullPayroll As Boolean = False  'asigned in license.vb "getLicenseInfo" function
    Public Shared IsComplianceFromLic As Boolean = False

    'for PF bank XtraPfBank.vb
    Public Shared pfBankName As String = ""
    Public Shared BankName As String = ""
    Public Shared BankBranch As String = ""
    Public Shared ChequeAmt As String = ""
    Public Shared ChequeDate As DateTime
    Public Shared ChequeNo As String = ""
    Public Shared Depositor As String = ""
    'End for PF bank

    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim adap, adap1, adap2, adap3 As SqlDataAdapter
    Dim adapA, adapA1, adapA2, adapA3 As OleDbDataAdapter
    'Dim ds, RsCrEmp, RsEmpTemp As DataSet

    Public Shared servername As String
    Public Shared DB As String
    Public Shared ConnectionString As String
    Public Shared ConnectionStringWINPAKPRO As String    ' for ADI
    'Public Shared ulf As UserLookAndFeel
    Public Shared con As SqlConnection
    Public Shared con1 As OleDbConnection

    Public Shared res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    Public Shared cul As CultureInfo     'declare culture info


    Public Shared test As Integer
    Public Shared NavWidth As Integer
    Public Shared NavHeight As Integer
    Public Shared splitforMasterMenuWidth As Integer
    Public Shared SplitterPosition As Integer
    Public gnCommHandleIndex As Long 'calling for SDK connect 
    Dim sSql As String


    Dim mShiftStartTime As String = ""
    Dim mShiftEndTime As String = ""
    Dim mLunchStartTime As String = ""
    Dim mLunchEndTime As String = ""
    Dim mOutWorkDuration As Integer = 0
    Dim mShiftAttended As String = ""
    Dim arrShift As ArrayList = New ArrayList
    Dim mStatus As String = ""
    Dim mHoursworked As Integer = 0
    Dim mExcLunchHours As Integer = 0
    Dim mOtDuration As Integer = 0
    Dim mOSDuration As Integer = 0
    Dim motamount As Double = 0
    Dim mEarlyArrival As Integer = 0
    Dim mearlydeparture As Integer = 0
    Dim mLateArrival As Integer = 0
    Dim mLunchEarlyDeparture As Integer = 0
    Dim mLunchLateArrival As Integer = 0
    Dim mTotalLossHrs As Integer = 0
    Dim mLeaveValue As Double = 0
    Dim mPresentValue As Double = 0
    Dim mAbsentValue As Double = 0
    Dim mHoliday_Value As Double = 0
    Dim mWO_Value As Integer = 0
    Dim alHld As ArrayList = New ArrayList
    Dim g_OTMinus As String = "N"
    Dim HM As hourToMinute = New hourToMinute

    'for Load_Corporate_PolicySql
    Dim gInstalled As String = ""
    Dim gLCompany As String = ""
    Dim gLNoofUser As String = ""
    Dim gLDSNName As String = ""
    Dim gbDataServer As String = ""
    Dim gLDatabase As String = ""
    Public Shared gLDuplicate As String = "N"
    Dim gLInterface As String = ""
    Dim gLVisitor As String = ""
    Public Shared g_Inout As String = ""
    Dim gLPayroll As String = ""
    Public Shared gLTimeOffice As String = ""
    Dim gLLDate As String = ""
    Dim gLLicence As String = ""
    Dim gLPhone As String = ""
    Dim gLAddress3 As String = ""
    Dim gLAddress2 As String = ""
    Dim gLAddress1 As String = ""
    Dim gCompanyInfo As String = ""
    Dim g_AppliCation As Integer = 0
    Dim Tmp As String = ""
    Dim G_Reader As String = ""
    Dim G_Baud As String = ""

    Public Shared g_LinesPerPage As String = ""
    Public Shared g_Meals_Rate As String = ""
    Public Shared g_SkipAfterDept As Boolean = False
    Public Shared gLeaveFinancialYear As String = ""
    Public Shared IsCompliance As Boolean = True

    Dim g_Model As String = ""
    Dim g_auth_comp As String = ""
    Dim g_auth_dept As String = ""
    Dim gdeptclause As String = ""
    Dim gcompclause As String = ""
    Dim gcompclauseD As String = ""
    Dim gcompany As String = ""
    Dim gdepartment As String = ""
    Dim gdeptclauseD As String = ""
    Dim gusertype As String = ""
    'Public Shared g_As_Low As Integer = 0
    Dim minDate As DateTime = System.DateTime.MinValue
    Dim maxDate As DateTime = System.DateTime.MinValue

    Dim Dum As New Dummyform
    'end for Load_Corporate_PolicySql
    Dim mIn1 As String
    Dim mIn2 As String
    Dim mout1 As String
    Dim mout2 As String

    Dim mIn1M As String
    Dim mIn2M As String
    Dim mout1M As String
    Dim mout2M As String

    'for SMS
    Public Shared g_SMSApplicable As String = ""
    Public Shared g_isLate As String = ""
    Dim mLateSMS As String
    Dim LateDur As String
    Dim LateSMSContent As String
    Public Shared g_LateContent1 As String
    Public Shared g_LateContent2 As String
    Dim mInSMS As String
    Dim InSMSContent As String
    Dim InPunch As String
    Dim mOutSMS As String
    Dim OutSMSContent As String
    Dim OutPunch As String
    Dim mEmpName As String
    ' Dim url As String
    Dim Content As String
    Public Shared G_SmsKey As String
    Public Shared G_SenderID As String
    Public Shared g_isInSMS As String
    Public Shared g_InContent1 As String
    Public Shared g_InContent2 As String
    Public Shared g_isOutSMS As String
    Public Shared g_OutContent1 As String
    Public Shared g_OutContent2 As String
    Public Shared g_OutSMSAfter As Integer
    Public Shared g_isAbsentSMS As String
    Public Shared g_AbsContent1 As String
    Public Shared g_AbsContent2 As String
    Public Shared g_isAllSMS As String
    Public Shared g_AllContent1 As String
    Public Shared g_AllContent2 As String
    Public Shared g_AbsMsgFor As String
    Public Shared g_AbsMsgTime As Date
    Public Shared g_AbsSMSAfter As Integer
    Public Shared g_AbsName As String
    Public Shared g_AbsDate As String
    Public Shared g_DeviceWiseInOut As String
    Dim request As HttpWebRequest
    Dim response As HttpWebResponse = Nothing

    Dim PunchShiftEarly As DateTime 'for early
    Dim PunchShiftLate As DateTime 'for late
    'End sms

    'ParallelDB
    Public Shared IsParallel As String
    Public Shared DBType As String
    Public Shared PreIsPrifix As String
    Public Shared PreLength As String
    Public Shared PreText As String
    Public Shared PayIsPrefix As String
    Public Shared PayLength As String
    Public Shared PayText As String
    Public Shared PunchDateFormat As String
    Public Shared PunchTimeFormat As String
    Public Shared PunchDateTimeFormat As String
    Public Shared InValue As String
    Public Shared OutVaue As String
    Public Shared YesValue As String
    Public Shared NoValue As String
    Public Shared PConnectionString As String
    Public Shared PQuery As String
    Public Shared Rfield1Val As String
    Public Shared Rfield2Val As String
    Public Shared Rfield3Val As String
    ' end ParallelDB
#End Region
    Public Sub New()
        If Common.servername = "Access" Then
            con1Access = New OleDbConnection(Common.ConnectionString)
        Else
            conSQL = New SqlConnection(Common.ConnectionString)
        End If
    End Sub
    Public Shared EmpGrpArr() As EmpGrpStuct
    Public Structure EmpGrpStuct
        Dim GroupId As String
        Dim GroupName As String
        Dim Branch As String
        Dim SHIFT As String
        Dim SHIFTTYPE As String
        Dim SHIFTPATTERN As String
        Dim SHIFTREMAINDAYS As Double
        Dim LASTSHIFTPERFORMED As String
        Dim ISAUTOSHIFT As String
        Dim AUTH_SHIFTS As String
        Dim FIRSTOFFDAY As String
        Dim SECONDOFFTYPE As String
        Dim HALFDAYSHIFT As String
        Dim SECONDOFFDAY As String
        Dim ALTERNATE_OFF_DAYS As String
        Dim INONLY As String
        Dim ISPUNCHALL As String
        Dim ISOUTWORK As String
        Dim TWO As String
        Dim ISTIMELOSSALLOWED As String
        Dim CDAYS As Double
        Dim ISROUNDTHECLOCKWORK As String
        Dim ISOT As String
        Dim OTRATE As String
        Dim PERMISLATEARRIVAL As Double
        Dim PERMISEARLYDEPRT As Double
        Dim MAXDAYMIN As Double
        Dim ISOS As String
        Dim TIME As Double
        Dim SHORT1 As Double
        Dim HALF As Double
        Dim ISHALFDAY As String
        Dim ISSHORT As String
        Dim LateVerification As String
        Dim EveryInterval As String
        Dim DeductFrom As String
        Dim FromLeave As String
        'Dim LateDays As Double
        'Dim DeductDay As Double
        'Dim MaxLateDur As Double
        Dim PunchRequiredInDay As Double
        Dim SinglePunchOnly As Double
        Dim LastModifiedBy As String
        Dim LastModifiedDate As DateTime
        Dim MARKMISSASHALFDAY As String
        Dim Department As String
        Dim Id As Double


        'newly declared 
        Dim g_s_end As String '= ""
        Dim g_o_end As String '= ""
        Dim g_PermisEarlydep As String '= ""
        Dim g_PermisLateArr As String '= ""
        Dim g_MaxHlfAbsDur As String '= ""
        Dim g_PreMarkingDur As String '= ""
        Dim g_SrtMarkingDur As String '= ""
        Dim g_isHalfDay As String '= ""
        Dim g_isShort As String '= ""
        Dim g_Two As String '= ""
        Dim g_Wo_Include As String '= ""
        Dim g_OwMinus As String '= ""
        Dim g_isPresentOnWOPresent As String '= ""
        Dim g_isPresentOnHLDPresent As String '= ""
        Dim g_AbsentOnWO As String '= ""
        Dim g_As_Up As Integer '= 0
        Dim g_AutoShift As String '= ""
        Dim g_isOverTime As String '= ""
        Dim g_isOverStay As String '= ""
        Dim g_NightShiftFourPunch As String '= ""
        Dim g_LinesPerPage As String '= ""
        Dim g_Meals_Rate As String '= ""
        Dim g_SkipAfterDept As Boolean '= False
        Dim g_OTFormulae As Integer '= 0
        Dim g_OTChkEarly As String '= ""
        Dim g_DupCheckMin As String '= ""
        Dim g_isAutoShift As String '= ""
        'Public Shared g_Day_Hour As String'= ""
        Dim g_isOutWork As String '= ""
        'Public Shared g_TIME1 As String'= ""
        'Public Shared G_CHECKLATE As String'= ""
        'Public Shared g_CHECKEARLY As String'= ""
        'Public Shared g_Smart As String'= ""
        'Public Shared g_Help As Boolean'= False
        Dim g_attacc As Boolean '= False
        Dim g_MIS As Boolean '= False
        Dim g_As_Low As Integer '= 0
        Dim g_OTEarly As Integer '= 0
        Dim g_OTLate As Integer '= 0
        Dim g_OTEnd As Integer '= 0
        Dim g_OtRound As String '= ""
        Dim g_Wo_OT As Integer '= 0
        Dim g_Hld_OT As Integer '= 0
        Dim g_Inout As String '= ""

        Dim MSHIFT As String
        Dim MISCOMPOFF As String
        Dim ISCOMPOFF As String

        Dim g_ISAWA As String
        Dim g_ISAW As String
        Dim g_ISWA As String
        Dim g_ISAWP As String
        Dim g_ISPWA As String
        Dim g_isprewo As String
        Dim g_PREWO As String
    End Structure
    Public Shared Function LoadGroupStruct()
        'Array.Clear(EmpGrpArr, 0, EmpGrpArr.Length)
        Dim sSql As String = "Select * from EmployeeGroup  ORDER BY ID"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim dsE As DataSet = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(dsE)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(dsE)
        End If
        Dim EmpGrpList As New List(Of EmpGrpStuct)

        For i As Integer = 0 To dsE.Tables(0).Rows.Count - 1
            Dim x As EmpGrpStuct = New EmpGrpStuct
            x.GroupId = dsE.Tables(0).Rows(i).Item("GroupId").ToString.Trim
            x.GroupName = dsE.Tables(0).Rows(i).Item("GroupName").ToString.Trim
            x.GroupId = dsE.Tables(0).Rows(i).Item("GroupId").ToString.Trim
            x.GroupName = dsE.Tables(0).Rows(i).Item("GroupName").ToString.Trim
            x.Branch = dsE.Tables(0).Rows(i).Item("Branch").ToString.Trim
            x.SHIFT = dsE.Tables(0).Rows(i).Item("SHIFT").ToString.Trim
            x.SHIFTTYPE = dsE.Tables(0).Rows(i).Item("SHIFTTYPE").ToString.Trim
            x.SHIFTPATTERN = dsE.Tables(0).Rows(i).Item("SHIFTPATTERN").ToString.Trim
            x.SHIFTREMAINDAYS = dsE.Tables(0).Rows(i).Item("SHIFTREMAINDAYS").ToString.Trim
            x.LASTSHIFTPERFORMED = dsE.Tables(0).Rows(i).Item("LASTSHIFTPERFORMED").ToString.Trim
            x.ISAUTOSHIFT = dsE.Tables(0).Rows(i).Item("ISAUTOSHIFT").ToString.Trim
            x.AUTH_SHIFTS = dsE.Tables(0).Rows(i).Item("AUTH_SHIFTS").ToString.Trim
            x.FIRSTOFFDAY = dsE.Tables(0).Rows(i).Item("FIRSTOFFDAY").ToString.Trim
            x.SECONDOFFTYPE = dsE.Tables(0).Rows(i).Item("SECONDOFFTYPE").ToString.Trim
            x.HALFDAYSHIFT = dsE.Tables(0).Rows(i).Item("HALFDAYSHIFT").ToString.Trim
            x.SECONDOFFDAY = dsE.Tables(0).Rows(i).Item("SECONDOFFDAY").ToString.Trim
            x.ALTERNATE_OFF_DAYS = dsE.Tables(0).Rows(i).Item("ALTERNATE_OFF_DAYS").ToString.Trim
            x.INONLY = dsE.Tables(0).Rows(i).Item("INONLY").ToString.Trim
            x.ISPUNCHALL = dsE.Tables(0).Rows(i).Item("ISPUNCHALL").ToString.Trim
            x.ISOUTWORK = dsE.Tables(0).Rows(i).Item("ISOUTWORK").ToString.Trim
            x.TWO = dsE.Tables(0).Rows(i).Item("TWO").ToString.Trim
            x.ISTIMELOSSALLOWED = dsE.Tables(0).Rows(i).Item("ISTIMELOSSALLOWED").ToString.Trim
            x.CDAYS = dsE.Tables(0).Rows(i).Item("CDAYS").ToString.Trim
            x.ISROUNDTHECLOCKWORK = dsE.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString.Trim
            x.ISOT = dsE.Tables(0).Rows(i).Item("ISOT").ToString.Trim
            x.OTRATE = dsE.Tables(0).Rows(i).Item("OTRATE").ToString.Trim
            x.PERMISLATEARRIVAL = dsE.Tables(0).Rows(i).Item("PERMISLATEARRIVAL").ToString.Trim
            x.PERMISEARLYDEPRT = dsE.Tables(0).Rows(i).Item("PERMISEARLYDEPRT").ToString.Trim
            x.MAXDAYMIN = dsE.Tables(0).Rows(i).Item("MAXDAYMIN").ToString.Trim
            x.ISOS = dsE.Tables(0).Rows(i).Item("ISOS").ToString.Trim
            If Common.servername = "Access" Then
                x.TIME = dsE.Tables(0).Rows(i).Item("TIME1").ToString.Trim
                x.SHORT1 = dsE.Tables(0).Rows(i).Item("SHORT1").ToString.Trim
            Else
                x.TIME = dsE.Tables(0).Rows(i).Item("TIME").ToString.Trim
                x.SHORT1 = dsE.Tables(0).Rows(i).Item("SHORT").ToString.Trim
            End If


            x.HALF = dsE.Tables(0).Rows(i).Item("HALF").ToString.Trim
            x.ISHALFDAY = dsE.Tables(0).Rows(i).Item("ISHALFDAY").ToString.Trim
            x.ISSHORT = dsE.Tables(0).Rows(i).Item("ISSHORT").ToString.Trim
            x.LateVerification = dsE.Tables(0).Rows(i).Item("LateVerification").ToString.Trim
            x.EveryInterval = dsE.Tables(0).Rows(i).Item("EveryInterval").ToString.Trim
            x.DeductFrom = dsE.Tables(0).Rows(i).Item("DeductFrom").ToString.Trim
            x.FromLeave = dsE.Tables(0).Rows(i).Item("FromLeave").ToString.Trim
            'x.LateDays = dsE.Tables(0).Rows(i).Item("LateDays").ToString.Trim
            'x.DeductDay = dsE.Tables(0).Rows(i).Item("DeductDay").ToString.Trim
            'x.MaxLateDur = dsE.Tables(0).Rows(i).Item("MaxLateDur").ToString.Trim
            x.PunchRequiredInDay = dsE.Tables(0).Rows(i).Item("PunchRequiredInDay").ToString.Trim
            x.SinglePunchOnly = dsE.Tables(0).Rows(i).Item("SinglePunchOnly").ToString.Trim
            x.LastModifiedBy = dsE.Tables(0).Rows(i).Item("LastModifiedBy").ToString.Trim
            x.LastModifiedDate = dsE.Tables(0).Rows(i).Item("LastModifiedDate").ToString.Trim
            x.MARKMISSASHALFDAY = dsE.Tables(0).Rows(i).Item("MARKMISSASHALFDAY").ToString.Trim
            'x.Department = dsE.Tables(0).Rows(i).Item("Department").ToString.Trim
            x.Id = dsE.Tables(0).Rows(i).Item("Id").ToString.Trim





            x.g_s_end = dsE.Tables(0).Rows(i)("s_end").ToString.Trim
            x.g_o_end = dsE.Tables(0).Rows(i)("s_out").ToString.Trim
            x.g_PermisEarlydep = dsE.Tables(0).Rows(i)("PermisEarlydep").ToString.Trim
            x.g_PermisLateArr = dsE.Tables(0).Rows(i)("PermisLateArr").ToString.Trim
            x.g_MaxHlfAbsDur = dsE.Tables(0).Rows(i)("Half").ToString.Trim
            'If servername = "Access" Then
            '    x.g_PreMarkingDur = dsE.Tables(0).Rows(0)("Time_1").ToString.Trim
            'Else
            '    x.g_PreMarkingDur = dsE.Tables(0).Rows(0)("Time").ToString.Trim
            'End If
            If servername = "Access" Then
                x.g_SrtMarkingDur = dsE.Tables(0).Rows(i)("Short1").ToString.Trim
            Else
                x.g_SrtMarkingDur = dsE.Tables(0).Rows(i)("Short").ToString.Trim
            End If
            x.g_isHalfDay = dsE.Tables(0).Rows(i)("isHalfDay").ToString.Trim
            x.g_isShort = dsE.Tables(0).Rows(i)("isShort").ToString.Trim
            x.g_Two = dsE.Tables(0).Rows(i)("Two").ToString.Trim
            x.g_Wo_Include = dsE.Tables(0).Rows(i)("WOInclude").ToString.Trim
            x.g_OwMinus = dsE.Tables(0).Rows(i)("OwMinus").ToString.Trim
            x.g_isPresentOnWOPresent = dsE.Tables(0).Rows(i)("isPresentOnWOPresent").ToString.Trim
            x.g_isPresentOnHLDPresent = dsE.Tables(0).Rows(i)("isPresentOnHLDPresent").ToString.Trim
            x.g_AbsentOnWO = dsE.Tables(0).Rows(i)("isAutoAbsent").ToString.Trim
            If dsE.Tables(0).Rows(i)("AUTOSHIFT_LOW").ToString.Trim <> "" Then
                x.g_As_Low = Convert.ToInt32(dsE.Tables(0).Rows(i)("AUTOSHIFT_LOW").ToString.Trim)
            Else
                x.g_As_Low = 0
            End If

            If dsE.Tables(0).Rows(i)("AUTOSHIFT_UP").ToString.Trim <> "" Then
                x.g_As_Up = Convert.ToInt32(dsE.Tables(0).Rows(i)("AUTOSHIFT_UP").ToString.Trim)
            Else
                x.g_As_Up = 0
            End If


            x.g_AutoShift = dsE.Tables(0).Rows(i)("ISAUTOSHIFT").ToString.Trim
            x.g_isOverTime = dsE.Tables(0).Rows(i)("isOtAll").ToString.Trim
            x.g_isOverStay = dsE.Tables(0).Rows(i)("isOverStay").ToString.Trim
            x.g_NightShiftFourPunch = dsE.Tables(0).Rows(i)("NightShiftFourPunch").ToString.Trim



            If (dsE.Tables(0).Rows(i)("ISOTOUTMINUSSHIFTENDTIME").ToString.Trim = "Y") Then
                x.g_OTFormulae = 1
            ElseIf (dsE.Tables(0).Rows(i)("ISOTWRKGHRSMINUSSHIFTHRS").ToString.Trim = "Y") Then
                x.g_OTFormulae = 2
            Else
                x.g_OTFormulae = 3
            End If

            If (x.g_OTFormulae = 3) Then
                x.g_OTLate = dsE.Tables(0).Rows(i)("OTLATECOMINGDUR").ToString.Trim
                x.g_OTChkEarly = dsE.Tables(0).Rows(i)("ISOTEARLYCOMING").ToString.Trim
                x.g_OTEarly = dsE.Tables(0).Rows(i)("OTEARLYDUR").ToString.Trim
                x.g_OTEnd = dsE.Tables(0).Rows(i)("OTRESTRICTENDDUR").ToString.Trim
            End If

            x.g_OtRound = dsE.Tables(0).Rows(i)("OTROUND").ToString.Trim
            x.g_Wo_OT = dsE.Tables(0).Rows(i)("DEDUCTWOOT").ToString.Trim
            x.g_Hld_OT = dsE.Tables(0).Rows(i)("DEDUCTHOLIDAYOT").ToString.Trim
            x.g_DupCheckMin = dsE.Tables(0).Rows(i)("DuplicateCheckMin").ToString.Trim
            x.g_isAutoShift = dsE.Tables(0).Rows(i)("isAutoShift").ToString.Trim
            'x.g_Day_Hour = dsE.Tables(0).rows(i)("MAXWRKDURATION").ToString.Trim
            x.g_isOutWork = dsE.Tables(0).Rows(i)("IsOutWork").ToString.Trim
            ' Missing Report Variables
            'x.g_TIME1 = dsE.Tables(0).rows(i)("Time1").ToString.Trim
            'x.G_CHECKLATE = dsE.Tables(0).rows(i)("CHECKLATE").ToString.Trim
            'x.g_CHECKEARLY = dsE.Tables(0).rows(i)("CHECKEARLY").ToString.Trim
            'x.g_Smart = dsE.Tables(0).rows(i)("smart").ToString.Trim

            If Not String.IsNullOrEmpty(gLTimeOffice) Then
                x.g_Inout = gLTimeOffice.Substring(1, 1)
            End If

            'If (dsE.Tables(0).rows(i)("ishelp").ToString.Trim = "Y") Then
            '    x.g_Help = True
            'Else
            '    x.g_Help = False
            'End If

            If (dsE.Tables(0).Rows(i)("MIS").ToString.Trim = "Y") Then
                x.g_MIS = True
            Else
                x.g_MIS = False
            End If

            x.MISCOMPOFF = dsE.Tables(0).Rows(i)("MISCOMPOFF").ToString.Trim
            x.ISCOMPOFF = dsE.Tables(0).Rows(i)("ISCOMPOFF").ToString.Trim
            x.MSHIFT = dsE.Tables(0).Rows(i)("MSHIFT").ToString.Trim

            x.g_ISAWA = dsE.Tables(0).Rows(i)("ISAWA").ToString.Trim
            x.g_ISAW = dsE.Tables(0).Rows(i)("ISAW").ToString.Trim
            x.g_ISWA = dsE.Tables(0).Rows(i)("ISWA").ToString.Trim
            x.g_ISAWP = dsE.Tables(0).Rows(i)("ISAWP").ToString.Trim
            x.g_ISPWA = dsE.Tables(0).Rows(i)("ISPWA").ToString.Trim
            x.g_isprewo = dsE.Tables(0).Rows(i)("isprewo").ToString.Trim
            x.g_PREWO = dsE.Tables(0).Rows(i)("PREWO").ToString.Trim
            EmpGrpList.Add(x)
        Next
        EmpGrpArr = EmpGrpList.ToArray

    End Function
    Public Shared Function SetEmpGrpId()
        Dim sSql As String = "select GroupId from EmployeeGroup ORDER BY LastModifiedDate"
        Dim ds As DataSet = New DataSet
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            sSql = "Update EmployeeGroup set Id = " & i & " where GroupId = '" & ds.Tables(0).Rows(i).Item("GroupId").ToString.Trim & "'"
            If servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, Common.con1)
                cmd.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        Next
    End Function

    'Public Shared Function getMacAddress() ' As String
    '    Dim nics() As NetworkInterface = NetworkInterface.GetAllNetworkInterfaces()
    '    Dim mac As String = nics(1).GetPhysicalAddress.ToString.Trim
    '    If mac = "" Then
    '        mac = nics(0).GetPhysicalAddress.ToString.Trim
    '    End If
    '    SysyemMac = mac
    '    'Return mac
    'End Function
    Public Shared Sub getInstallSystemInfo()
        Dim DsInst As DataSet = New DataSet
        Dim sSql As String = "select * from InstallSystemInfo"
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(sSql, Common.con1)
            dataAdapter.Fill(DsInst)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(sSql, Common.con)
            dataAdapter.Fill(DsInst)
        End If
        DbMac = DsInst.Tables(0).Rows(0)("MAC").ToString.Trim
        UserKey = DsInst.Tables(0).Rows(0)("UserKey").ToString.Trim
        LicenseStr = DsInst.Tables(0).Rows(0)("License").ToString.Trim
        InstalledDate = DsInst.Tables(0).Rows(0)("InstalledDate").ToString.Trim
        LastUpdated = DsInst.Tables(0).Rows(0)("LastUpdated").ToString.Trim
    End Sub
    Public Shared Sub CheckNewColumnsInDb()
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim ds As DataSet
        Dim con1Tmp As OleDbConnection '= New OleDbConnection(Common.ConnectionString)
        'login load start
        'check LocationColumn in tblUSer
        Dim sSql As String = "select Auth_Branch from tblUser"
        ds = New DataSet
        If Common.servername = "Access" Then
            con1Tmp = New OleDbConnection(Common.ConnectionString)
            If con1Tmp.State <> ConnectionState.Open Then
                con1Tmp.Open()
            End If
        End If
        Thread.Sleep(500)
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblUser ADD Auth_Branch longtext"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE tblUser ADD Auth_Branch varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try

        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        If Common.servername = "Access" Then
            sSql = "ALTER TABLE TBLEmployee ALTER COLUMN DEPARTMENTCODE TEXT(3)"
            'If con1Tmp.State <> ConnectionState.Open Then
            '    con1Tmp.Open()
            'End If
            Dim cmd As OleDbCommand
            cmd = New OleDbCommand(sSql, con1Tmp)
            cmd.ExecuteNonQuery()

            sSql = "ALTER TABLE TBLEmployee ALTER COLUMN COMPANYCODE TEXT(5)"
            'If con1Tmp.State <> ConnectionState.Open Then
            '    con1Tmp.Open()
            'End If
            cmd = New OleDbCommand(sSql, con1Tmp)
            cmd.ExecuteNonQuery()

            sSql = "ALTER TABLE TBLCompany ALTER COLUMN COMPANYCODE TEXT(5)"
            'If con1Tmp.State <> ConnectionState.Open Then
            '    con1Tmp.Open()
            'End If
            cmd = New OleDbCommand(sSql, con1Tmp)
            cmd.ExecuteNonQuery()

            sSql = "ALTER TABLE TBLDepartment ALTER COLUMN DEPARTMENTCODE TEXT(3)"
            'If con1Tmp.State <> ConnectionState.Open Then
            '    con1Tmp.Open()
            'End If
            cmd = New OleDbCommand(sSql, con1Tmp)
            cmd.ExecuteNonQuery()
            'If con1Tmp.State <> ConnectionState.Closed Then
            '    con1Tmp.Close()
            'End If
        End If
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'check ZKTimeZone
        sSql = "select * from ZKAccessTimeZone  "
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then

            Else
                sSql = "CREATE TABLE [dbo].[ZKAccessTimeZone](
	[TimeZoneID] [numeric](18, 0) NOT NULL,
	[SunIn1] [varchar](50) NULL,
	[SunIn2] [varchar](50) NULL,
	[SunIn3] [varchar](50) NULL,
	[SunOut1] [varchar](50) NULL,
	[SunOut2] [varchar](50) NULL,
	[SunOut3] [varchar](50) NULL,
	[MonIn1] [varchar](50) NULL,
	[MonIn2] [varchar](50) NULL,
	[MonIn3] [varchar](50) NULL,
	[MonOut1] [varchar](50) NULL,
	[MonOut2] [varchar](50) NULL,
	[MonOut3] [varchar](50) NULL,
	[TueIn1] [varchar](50) NULL,
	[TueIn2] [varchar](50) NULL,
	[TueIn3] [varchar](50) NULL,
	[Tueout1] [varchar](50) NULL,
	[Tueout2] [varchar](50) NULL,
	[Tueout3] [varchar](50) NULL,
	[WedIn1] [varchar](50) NULL,
	[WedIn2] [varchar](50) NULL,
	[WedIn3] [varchar](50) NULL,
	[WedOut1] [varchar](50) NULL,
	[WedOut2] [varchar](50) NULL,
	[WedOut3] [varchar](50) NULL,
	[ThuIn1] [varchar](50) NULL,
	[ThuIn2] [varchar](50) NULL,
	[ThuIn3] [varchar](50) NULL,
	[ThuOut1] [varchar](50) NULL,
	[ThuOut2] [varchar](50) NULL,
	[ThuOut3] [varchar](50) NULL,
	[FriIn1] [varchar](50) NULL,
	[FriIn2] [varchar](50) NULL,
	[FriIn3] [varchar](50) NULL,
	[FriOut1] [varchar](50) NULL,
	[FriOut2] [varchar](50) NULL,
	[FriOut3] [varchar](50) NULL,
	[Satin1] [varchar](50) NULL,
	[Satin2] [varchar](50) NULL,
	[Satin3] [varchar](50) NULL,
	[SatOut1] [varchar](50) NULL,
	[SatOut2] [varchar](50) NULL,
	[SatOut3] [varchar](50) NULL,
 CONSTRAINT [PK_ZKAccessTimeZone] PRIMARY KEY CLUSTERED 
(
	[TimeZoneID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()






        ' Check TF-01 Port In Database (tblsetup)



        sSql = "select TF01Port from tblSetup"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblSetup ADD TF01Port longtext"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE tblSetup ADD TF01Port  varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()




        'check LEDIP in tblMachine
        sSql = "select LEDIP from tblMachine"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblMachine ADD LEDIP longtext"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE tblMachine ADD LEDIP varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'check ZKATTLOGStamp in tblMachine
        sSql = "select * from ZKATTLOGStamp"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "Create TABLE ZKATTLOGStamp (ATTLOGStamp longtext, SN longtext, OPERLOGStamp longtext, USERINFOStamp longtext, ATTPHOTOStamp longtext, fingertmpStamp longtext)"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "Create TABLE ZKATTLOGStamp (ATTLOGStamp varchar(50), SN varchar(50),  OPERLOGStamp varchar(50), USERINFOStamp varchar(50), ATTPHOTOStamp varchar(50), fingertmpStamp varchar(50))"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'check TWIR102Port in tblMachine
        sSql = "select TWIR102Port from tblSetup"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            Dim sSql1 As String = "update tblSetup set TWIR102Port='15000'"
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblSetup ADD TWIR102Port longtext"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()

                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()

                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE tblSetup ADD TWIR102Port varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()

                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'check AutoDwnDur in tblMachine
        sSql = "select AutoDwnDur from tblSetup"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            Dim sSql1 As String = "update tblSetup set AutoDwnDur='0'"
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblSetup ADD AutoDwnDur longtext"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()

                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()

                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE tblSetup ADD AutoDwnDur varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()

                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        CheckNewColumnSMS()
        'login load end 
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'master load start
        'CreateEmpPunchDwnld
        Dim Rs As DataSet = New DataSet
        sSql = " Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tb" & _
        "lsetup )"
        If Common.servername = "Access" Then
            sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
            adapA = New OleDbDataAdapter(sSql, con1Tmp)
            adapA.Fill(Rs)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(Rs)
        End If
        Try
            Dim tmp = Rs.Tables(0).Rows(0).Item("CreateEmpPunchDwnld").ToString.Trim
        Catch ex As Exception
            sSql = "ALTER TABLE tblsetup ADD CreateEmpPunchDwnld char(1)"
            If Common.servername = "Access" Then
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd1 As OleDbCommand
                cmd1 = New OleDbCommand(sSql, con1Tmp)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand("update tblSetup set CreateEmpPunchDwnld = 'N'", con1Tmp)
                cmd1.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("update tblSetup set CreateEmpPunchDwnld = 'N'", Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'StartRealTime
        Try
            XtraMasterTest.realTIme = Rs.Tables(0).Rows(0).Item("StartRealTime").ToString.Trim
        Catch ex As Exception
            sSql = "ALTER TABLE tblsetup ADD StartRealTime char(1)"
            If Common.servername = "Access" Then
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd1 As OleDbCommand
                cmd1 = New OleDbCommand(sSql, con1Tmp)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand("update tblSetup set StartRealTime = 'Y'", con1Tmp)
                cmd1.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("update tblSetup set StartRealTime = 'Y'", Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            XtraMaster.realTIme = "Y"
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'shiftearly, shiftlate
        sSql = "select shiftearly, shiftlate from tblShiftMaster"
        Dim Rs1 = New DataSet
        If Common.servername = "Access" Then
            Try
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(Rs1)
            Catch ex As Exception
                sSql = "ALTER TABLE tblShiftMaster ADD ShiftEarly int, ShiftLate int "
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd1 As OleDbCommand
                cmd1 = New OleDbCommand(sSql, con1Tmp)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand("update tblShiftMaster set ShiftEarly = 0, ShiftLate = 0", con1Tmp)
                cmd1.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            End Try
        Else
            Try
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(Rs1)
            Catch ex As Exception
                sSql = "ALTER TABLE tblShiftMaster ADD ShiftEarly int, ShiftLate int "
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("update tblShiftMaster set ShiftEarly = 0, ShiftLate = 0", Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End Try
        End If
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'CommKey
        If Common.servername = "Access" Then
            'If con1Tmp.State <> ConnectionState.Open Then
            '    con1Tmp.Open()
            'End If
            Dim cmd1 As OleDbCommand
            cmd1 = New OleDbCommand("update tblMachine set CommKey = 0 where (DeviceType ='ZK(TFT)' or DeviceType ='Bio-1Pro/ATF305Pro/ATF686Pro') and commkey is NULL", con1Tmp)
            cmd1.ExecuteNonQuery()
            'If con1Tmp.State <> ConnectionState.Closed Then
            '    con1Tmp.Close()
            'End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            Dim cmd As SqlCommand
            cmd = New SqlCommand("update tblMachine set CommKey = 0 where (DeviceType ='ZK(TFT)' or DeviceType ='Bio-1Pro/ATF305Pro/ATF686Pro') and commkey is NULL", Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'MARKMISSASHALFDAY
        sSql = "select top 1  MARKMISSASHALFDAY from tblEmployeeShiftMaster"
        Rs1 = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(Rs1)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(Rs1)
            End If
        Catch ex As Exception
            sSql = "ALTER TABLE tblEmployeeShiftMaster ADD MARKMISSASHALFDAY char(1)"
            Dim sSql1 As String = "ALTER TABLE EmployeeGroup ADD MARKMISSASHALFDAY char(1)"
            If Common.servername = "Access" Then
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd1 As OleDbCommand
                cmd1 = New OleDbCommand(sSql, con1Tmp)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(sSql1, con1Tmp)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand("update tblEmployeeShiftMaster set MARKMISSASHALFDAY='N'", con1Tmp)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand("update EmployeeGroup set MARKMISSASHALFDAY='N'", con1Tmp)
                cmd1.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("update tblEmployeeShiftMaster set MARKMISSASHALFDAY='N'", Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand("update EmployeeGroup set MARKMISSASHALFDAY='N'", Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        'master load end
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'check ISVEG in TBLCANTEEN
        sSql = "select ISVEG from TBLCANTEEN"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE TBLCANTEEN ADD ISVEG longtext"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE TBLCANTEEN ADD ISVEG varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'check WorkCode in TBLCANTEEN
        sSql = "select WorkCode from TBLCANTEEN"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE TBLCANTEEN ADD WorkCode longtext"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE TBLCANTEEN ADD WorkCode varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'check TblCanteenWorkCode  report
        sSql = "select * from TblCanteenWorkCode"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "Create TABLE TblCanteenWorkCode (WorkCode longtext PRIMARY KEY, WorkCodeName longtext)"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "Create TABLE TblCanteenWorkCode (WorkCode varchar(50), WorkCodeName varchar(MAX), PRIMARY KEY (WorkCode))"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'paycode in CustomisedReport
        'sSql = "select Pyacode from CustomisedReport"
        'ds = New DataSet
        Try
            'If Common.servername = "Access" Then
            '    adapA = New OleDbDataAdapter(sSql, con1Tmp)
            '    adapA.Fill(ds)
            'Else
            '    adap = New SqlDataAdapter(sSql, Common.con)
            '    adap.Fill(ds)
            'End If
            'If ds.Tables(0).Rows.Count > 0 Then
            'sSql = "ALTER TABLE CustomisedReport RENAME COLUMN Pyacode TO Paycode"

            If Common.servername = "Access" Then
                sSql = "alter table CustomisedReport drop column Pyacode"
                '"alter table test add column j integer"
                Dim sSql1 As String = "ALTER TABLE CustomisedReport add COLUMN Paycode longtext"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd1 As OleDbCommand
                cmd1 = New OleDbCommand(sSql, con1Tmp)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(sSql1, con1Tmp)
                cmd1.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "EXEC sp_RENAME 'CustomisedReport.Pyacode' , 'Paycode', 'COLUMN'"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            'End If
        Catch
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'tblmachine with details
        sSql = "select Status,UserCount,FPCount,FaceCount,AttLogCount,AdminCount  from tblMachine"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblMachine ADD Status longtext, UserCount longtext ,FPCount longtext,FaceCount longtext, AttLogCount longtext,AdminCount longtext, LastDownloaded DateTime"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE tblMachine ADD Status varchar(max), UserCount varchar(max),FPCount varchar(max),FaceCount varchar(max),AttLogCount varchar(max),AdminCount varchar(max), LastDownloaded DateTime"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'BackUpDB
        sSql = "select * from BackUpData"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "Create TABLE BackUpData (BackUpType longtext, BackUpPath longtext, IsAutoBackUp longtext, DeleteAfterAutoBackUp longtext, AutoBackUpTimerDays longtext, AutoBackUpTimerTime longtext, AutoBackUpForDays longtext, AccessAutoBackUpPath longtext, LastAutoBackUpDate DateTime, LastBackUpDate DateTime)"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "Create TABLE BackUpData (BackUpType varchar(Max), BackUpPath varchar(Max), IsAutoBackUp varchar(Max), DeleteAfterAutoBackUp varchar(Max), AutoBackUpTimerDays varchar(Max), AutoBackUpTimerTime varchar(Max), AutoBackUpForDays varchar(Max), AccessAutoBackUpPath varchar(Max), LastAutoBackUpDate DateTime, LastBackUpDate DateTime)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'check Id in EmployeeGroup
        sSql = "select Id from EmployeeGroup"
        ds = New DataSet
        Dim SetEmpGrpIdBool As Boolean = False
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE EmployeeGroup ADD Id int ," & _
                        " PERMISLATEARR int , " & _
                        " PERMISEARLYDEP Number , " & _
                        " DUPLICATECHECKMIN Number , " & _
                        " ISOVERSTAY char(1) , " & _
                        " S_END datetime , " & _
                        " S_OUT datetime , " & _
                        " ISOTOUTMINUSSHIFTENDTIME char(1) , " & _
                        " ISOTWRKGHRSMINUSSHIFTHRS char(1) , " & _
                        " ISOTEARLYCOMEPLUSLATEDEP char(1) , " & _
                        " ISOTALL char(1) , " & _
                        " ISOTEARLYCOMING char(1) , " & _
                        " OTEARLYDUR Number , " & _
                        " OTLATECOMINGDUR Number , " & _
                        " OTRESTRICTENDDUR Number , " & _
                        " OTEARLYDEPARTUREDUR Number , " & _
                        " DEDUCTWOOT Number , " & _
                        " DEDUCTHOLIDAYOT Number , " & _
                        " ISPRESENTONWOPRESENT char(1) , " & _
                        " ISPRESENTONHLDPRESENT char(1) , " & _
                        " ISAUTOABSENT char(1) , " & _
                        " AUTOSHIFT_LOW Number , " & _
                        " AUTOSHIFT_UP Number , " & _
                        " WOINCLUDE char(1) , " & _
                        " NightShiftFourPunch char(1) , " & _
                        " OTROUND char(1) , " & _
                        " PREWO Number , " & _
                        " ISAWA char(1) , " & _
                        " ISPREWO char(1) , " & _
                        " LeaveFinancialYear char(1) , " & _
                        " MIS char(1) , " & _
                        " OwMinus char(1) "
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE EmployeeGroup ADD Id int ," & _
                    " PERMISLATEARR int , " & _
                    " PERMISEARLYDEP int , " & _
                    " DUPLICATECHECKMIN int , " & _
                    " ISOVERSTAY char(1) , " & _
                    " S_END datetime , " & _
                    " S_OUT datetime , " & _
                    " ISOTOUTMINUSSHIFTENDTIME char(1) , " & _
                    " ISOTWRKGHRSMINUSSHIFTHRS char(1) , " & _
                    " ISOTEARLYCOMEPLUSLATEDEP char(1) , " & _
                    " ISOTALL char(1) , " & _
                    " ISOTEARLYCOMING char(1) , " & _
                    " OTEARLYDUR int , " & _
                    " OTLATECOMINGDUR int , " & _
                    " OTRESTRICTENDDUR int , " & _
                    " OTEARLYDEPARTUREDUR int , " & _
                    " DEDUCTWOOT int , " & _
                    " DEDUCTHOLIDAYOT int , " & _
                    " ISPRESENTONWOPRESENT char(1) , " & _
                    " ISPRESENTONHLDPRESENT char(1) , " & _
                    " ISAUTOABSENT char(1) , " & _
                    " AUTOSHIFT_LOW int , " & _
                    " AUTOSHIFT_UP int , " & _
                    " WOINCLUDE char(1) , " & _
                    " NightShiftFourPunch char(1) , " & _
                    " OTROUND char(1) , " & _
                    " PREWO int , " & _
                    " ISAWA char(1) , " & _
                    " ISPREWO char(1) , " & _
                    " LeaveFinancialYear char(1) , " & _
                    " MIS char(1) , " & _
                    " OwMinus char(1) "

                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

            Dim S_END As DateTime = Convert.ToDateTime(Rs.Tables(0).Rows(0).Item("S_END")) '.ToString("yyyy/MM/dd HH:mm:ss")
            Dim S_OUT As DateTime = Convert.ToDateTime(Rs.Tables(0).Rows(0).Item("S_OUT")) '.ToString("yyyy/MM/dd HH:mm:ss")

            sSql = "Update EmployeeGroup set " & _
                 "PERMISLATEARR=" & Rs.Tables(0).Rows(0).Item("PERMISLATEARR") & ", " & _
                    "PERMISEARLYDEP=" & Rs.Tables(0).Rows(0).Item("PERMISEARLYDEP") & ", " & _
                    "DUPLICATECHECKMIN=" & Rs.Tables(0).Rows(0).Item("DUPLICATECHECKMIN") & ", " & _
                    "ISOVERSTAY='" & Rs.Tables(0).Rows(0).Item("ISOVERSTAY").ToString.Trim & "',  " & _
                    "S_END='" & S_END.ToString("yyyy/MM/dd HH:mm:ss") & "',  " & _
                    "S_OUT='" & S_OUT.ToString("yyyy/MM/dd HH:mm:ss") & "',  " & _
                    "ISOTOUTMINUSSHIFTENDTIME='" & Rs.Tables(0).Rows(0).Item("ISOTOUTMINUSSHIFTENDTIME").ToString.Trim & "',  " & _
                    "ISOTWRKGHRSMINUSSHIFTHRS='" & Rs.Tables(0).Rows(0).Item("ISOTWRKGHRSMINUSSHIFTHRS").ToString.Trim & "', " & _
                    "ISOTEARLYCOMEPLUSLATEDEP='" & Rs.Tables(0).Rows(0).Item("ISOTEARLYCOMEPLUSLATEDEP").ToString.Trim & "',  " & _
                    "ISOTALL='" & Rs.Tables(0).Rows(0).Item("ISOTALL").ToString.Trim & "',  " & _
                    "ISOTEARLYCOMING='" & Rs.Tables(0).Rows(0).Item("ISOTEARLYCOMING").ToString.Trim & "',  " & _
                    "OTEARLYDUR= '" & Rs.Tables(0).Rows(0).Item("OTEARLYDUR").ToString.Trim & "',  " & _
                    "OTLATECOMINGDUR= " & Rs.Tables(0).Rows(0).Item("OTLATECOMINGDUR") & ",  " & _
                    "OTRESTRICTENDDUR= " & Rs.Tables(0).Rows(0).Item("OTRESTRICTENDDUR") & ",  " & _
                    "OTEARLYDEPARTUREDUR= " & Rs.Tables(0).Rows(0).Item("OTEARLYDEPARTUREDUR") & ",  " & _
                    "DEDUCTWOOT= " & Rs.Tables(0).Rows(0).Item("DEDUCTWOOT") & ",  " & _
                    "DEDUCTHOLIDAYOT= " & Rs.Tables(0).Rows(0).Item("DEDUCTHOLIDAYOT") & ",  " & _
                    "ISPRESENTONWOPRESENT='" & Rs.Tables(0).Rows(0).Item("ISPRESENTONWOPRESENT").ToString.Trim & "',  " & _
                    "ISPRESENTONHLDPRESENT='" & Rs.Tables(0).Rows(0).Item("ISPRESENTONHLDPRESENT").ToString.Trim & "',  " & _
                    "ISAUTOABSENT='" & Rs.Tables(0).Rows(0).Item("ISAUTOABSENT").ToString.Trim & "',  " & _
                    "AUTOSHIFT_LOW=" & Rs.Tables(0).Rows(0).Item("AUTOSHIFT_LOW") & ", " & _
                    "AUTOSHIFT_UP=" & Rs.Tables(0).Rows(0).Item("AUTOSHIFT_UP") & ", " & _
                    "WOINCLUDE='" & Rs.Tables(0).Rows(0).Item("WOINCLUDE").ToString.Trim & "',  " & _
                    "NightShiftFourPunch='" & Rs.Tables(0).Rows(0).Item("NightShiftFourPunch").ToString.Trim & "',  " & _
                    "OTROUND='" & Rs.Tables(0).Rows(0).Item("OTROUND").ToString.Trim & "',  " & _
                    "PREWO=" & Rs.Tables(0).Rows(0).Item("PREWO") & ",  " & _
                    "ISAWA='" & Rs.Tables(0).Rows(0).Item("ISAWA").ToString.Trim & "',  " & _
                    "ISPREWO='" & Rs.Tables(0).Rows(0).Item("ISPREWO").ToString.Trim & "',  " & _
                    "LeaveFinancialYear='" & Rs.Tables(0).Rows(0).Item("LeaveFinancialYear").ToString.Trim & "',  " & _
                    "MIS='" & Rs.Tables(0).Rows(0).Item("MIS").ToString.Trim & "',  " & _
                    "OwMinus='" & Rs.Tables(0).Rows(0).Item("OwMinus").ToString.Trim & "'  "


            If servername = "Access" Then
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            login.ProgressBarControl1.PerformStep()
            login.ProgressBarControl1.Update()
            Application.DoEvents()
            SetEmpGrpIdBool = True
            login.ProgressBarControl1.PerformStep()
            login.ProgressBarControl1.Update()
            Application.DoEvents()
        End Try
        ' for MSHIFT in tblEmployeeShiftMaster
        sSql = "select top 1  MSHIFT from tblEmployeeShiftMaster"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblEmployeeShiftMaster ADD MSHIFT char(3)"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE tblEmployeeShiftMaster ADD MSHIFT char(3)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'for MSHIFTSTARTTIME in tblTimeRegister  'Multi shift
        sSql = "select top 1 MSHIFTSTARTTIME from tblTimeRegister"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            sSql = "ALTER TABLE tblTimeRegister ADD " & _
                    "[MSHIFTSTARTTIME]  datetime " & _
                    ",[MSHIFTENDTIME] datetime " & _
                    ",[MLUNCHSTARTTIME] datetime " & _
                    ",[MLUNCHENDTIME] datetime " & _
                    ",[MHOURSWORKED] int " & _
                    ",[MEXCLUNCHHOURS] int " & _
                    ",[MOTDURATION] int  " & _
                    ",[MOSDURATION] int  " & _
                    ",[MOTAMOUNT] float " & _
                    ",[MEARLYARRIVAL] int " & _
                    ",[MEARLYDEPARTURE] int " & _
                    ",[MLATEARRIVAL] int " & _
                    ",[MLUNCHEARLYDEPARTURE] int " & _
                    ",[MLUNCHLATEARRIVAL] int " & _
                    ",[MTOTALLOSSHRS] int " & _
                    ",[MSTATUS] char(6) " & _
                    ",[MSHIFT] char(3) " & _
                    ",[MSHIFTATTENDED] char(3) " & _
                    ",[MIN1] datetime " & _
                    ",[MIN2] datetime " & _
                    ",[MOUT1] datetime " & _
                    ",[MOUT2] datetime " & _
                    ",[MIN1MANNUAL] char(1) " & _
                    ",[MIN2MANNUAL] char(1) " & _
                    ",[MOUT1MANNUAL] char(1) " & _
                    ",[MOUT2MANNUAL] char(1)    " & _
                    ",[MPRESENTVALUE] float " & _
                    ",[MABSENTVALUE] float " & _
                    ",[MOUTWORKDURATION] int "
            If Common.servername = "Access" Then

                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                '          sSql = "ALTER TABLE tblTimeRegister ADD " & _
                '"[MSHIFTSTARTTIME]  datetime " & _
                '",[MSHIFTENDTIME] datetime " & _
                '",[MLUNCHSTARTTIME] datetime " & _
                '",[MLUNCHENDTIME] datetime " & _
                '",[MHOURSWORKED] int " & _
                '",[MEXCLUNCHHOURS] int " & _
                '",[MOTDURATION] int  " & _
                '",[MOSDURATION] int  " & _
                '",[MOTAMOUNT] float " & _
                '",[MEARLYARRIVAL] int " & _
                '",[MEARLYDEPARTURE] int " & _
                '",[MLATEARRIVAL] int " & _
                '",[MLUNCHEARLYDEPARTURE] int " & _
                '",[MLUNCHLATEARRIVAL] int " & _
                '",[MTOTALLOSSHRS] int " & _
                '",[MSTATUS] char(6) " & _
                '",[MSHIFT] char(3) " & _
                '",[MSHIFTATTENDED] char(3) " & _
                '",[MIN1] datetime " & _
                '",[MIN2] datetime " & _
                '",[MOUT1] datetime " & _
                '",[MOUT2] datetime " & _
                '",[MIN1MANNUAL] char(1) " & _
                '",[MIN2MANNUAL] char(1) " & _
                '",[MOUT1MANNUAL] char(1) " & _
                '",[MOUT2MANNUAL] char(1)    " & _
                '",[MPRESENTVALUE] float " & _
                '",[MABSENTVALUE] float " & _
                '",[MOUTWORKDURATION] int "
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'check COFUsed in tblTimeRegister
        sSql = "select top 1  COFUsed from tblTimeRegister"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblTimeRegister ADD COFUsed char(1), COFUsedAgainst datetime"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE tblTimeRegister ADD COFUsed char(1),  COFUsedAgainst datetime"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check MSHIFT in EmployeeGroup
        sSql = "select MSHIFT from EmployeeGroup"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE EmployeeGroup ADD MSHIFT char(3), MISCOMPOFF char(1), ISCOMPOFF char(1)"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE EmployeeGroup ADD MSHIFT char(3), MISCOMPOFF char(1), ISCOMPOFF char(1)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'COFUsedAgainst in LeaveApplication
        sSql = "select COFUsedAgainst from LeaveApplication"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE LeaveApplication ADD COFUsedAgainst datetime"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE LeaveApplication ADD COFUsedAgainst datetime"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'MachineRawPunchAll 
        sSql = "select top 1 * from MachineRawPunchAll"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "CREATE TABLE MachineRawPunchAll(" & _
"	CARDNO char(12)," & _
"	OFFICEPUNCH datetime," & _
"	P_DAY char(1)," & _
"	ISMANUAL char(1)," & _
"	ReasonCode char(3)," & _
"	MC_NO char(3)," & _
"	INOUT char(1)," & _
"	PAYCODE char(10)," & _
"	Purpose char(1)," & _
"	Remark longtext )"

                Dim sSql1 As String = "ALTER TABLE MachineRawPunchAll ADD CONSTRAINT your_pk_name PRIMARY KEY(CARDNO, OFFICEPUNCH)"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "CREATE TABLE [dbo].[MachineRawPunchAll](" & _
"	[CARDNO] [char](12) NOT NULL," & _
"	[OFFICEPUNCH] [datetime] NOT NULL," & _
"	[P_DAY] [char](1) NULL," & _
"	[ISMANUAL] [char](1) NULL," & _
"	[ReasonCode] [char](3) NULL," & _
"	[MC_NO] [char](3) NULL," & _
"	[INOUT] [char](1) NULL," & _
"	[PAYCODE] [char](12) NULL," & _
"	[Purpose] [char](1) NULL," & _
"	[Remark] [varchar](500) NULL," & _
" CONSTRAINT [PK_MRPA] PRIMARY KEY CLUSTERED " & _
"(" & _
"	[CARDNO] ASC," & _
"	[OFFICEPUNCH] ASC" & _
")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & _
") ON [PRIMARY]"

                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()



        'check Parallel DB
        sSql = "select * from ParallelDB"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            sSql = "CREATE TABLE ParallelDB( " & _
"IsParallel varchar(50) , " & _
"DBName varchar(max) , " & _
"DBType varchar(50) , " & _
"AuthMode varchar(50) , " & _
"ServerName varchar(max) , " & _
"UserName varchar(max) , " & _
"Pwd varchar(max) , " & _
"TblName varchar(max) , " & _
"EmpName varchar(max) , " & _
"PreCardNo varchar(max) , " & _
"PreIsPrifix varchar(50) , " & _
"PreLength varchar(50) , " & _
"PreText varchar(50) , " & _
"Paycode varchar(max) , " & _
"PayIsPrefix varchar(50) , " & _
"PayLength varchar(50) , " & _
"PayText varchar(50) , " & _
"PunchDate varchar(max) , " & _
"PunchDateFormat varchar(max) , " & _
"PunchTime varchar(max) , " & _
"PunchTimeFormat varchar(max) , " & _
"PunchDateTime varchar(max) , " & _
"PunchDateTimeFormat varchar(max) , " & _
"DeviceId varchar(max) , " & _
"PunchDirection varchar(max) , " & _
"InValue varchar(max) , " & _
"OutVaue varchar(max) , " & _
"IsManual varchar(max) , " & _
"YesValue varchar(max) , " & _
"NoValue varchar(max) , " & _
"Rfield1 varchar(max) , " & _
"Rfield1Value varchar(max) , " & _
"Rfield2 varchar(max) , " & _
"Rfield2Value varchar(max) , " & _
"Rfield3 varchar(max) , " & _
"Rfield3Value varchar(max) ," & _
"ConnectionString varchar(max), " & _
"Query varchar(max) " & _
") "
            Dim sSql1 As String = "insert into ParallelDB  (IsParallel) values ('N')"
            If Common.servername = "Access" Then
                sSql = sSql.Replace("varchar(max)", "longtext").Replace("varchar(50)", "longtext")
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()

                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()



        'ISAW, ISWA, ISAWP, ISPWA in EmployeeGroup
        sSql = "select ISAW from EmployeeGroup"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            sSql = "ALTER TABLE EmployeeGroup ADD ISAW char(1),ISWA char(1), ISAWP char(1),ISPWA char(1)"
            Dim sSql1 As String = "update EmployeeGroup set ISAW='N', ISWA='N', ISAWP='N', ISPWA='N'"
            If Common.servername = "Access" Then
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check MarkInactivDurDay in tblSetup
        sSql = "select MarkInactivDurDay from tblSetup"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            Dim sSql1 As String = "update tblSetup set MarkInactivDurDay='0'"
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblSetup ADD MarkInactivDurDay longtext"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()

                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE tblSetup ADD MarkInactivDurDay varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()

                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check CheckPreFix, CheckPostFix in tblLeaveMaster
        sSql = "select CheckPreFix, CheckPostFix from tblLeaveMaster"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            Dim sSql1 As String = "update tblLeaveMaster set CheckPreFix='N',CheckPostFix='N' "
            sSql = "ALTER TABLE tblLeaveMaster ADD CheckPreFix char(1), CheckPostFix char(1)"
            If Common.servername = "Access" Then
                'Using resource As FileShare = New FileShare
                'con1 = New OleDbConnection(Common.ConnectionString)
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()

                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                '    'con1Tmp.Dispose()
                'End If
                'End Using
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()

                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'chwnge STATUS longtext
        'sSql = "select STATUS from tblTimeRegister"        
        'ds = New DataSet
        'If Common.servername = "Access" Then
        '    adapA = New OleDbDataAdapter(sSql, con1Tmp)
        '    adapA.Fill(ds)
        'Else
        '    adap = New SqlDataAdapter(sSql, Common.con)
        '    adap.Fill(ds)
        'End If       
        'MsgBox(ds.Tables(0).Columns(0).DataType.ToString)
        If Common.servername = "Access" Then
            ''Dim schemaTable As DataTable = _
            ''con1Tmp.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, _
            ''New Object() {Nothing, Nothing, "tblemployee", "EMPNAME"})
            ''sSql = "ALTER TABLE tblTimeRegister ALTER COLUMN STATUS char(10)"
            'sSql = "ALTER TABLE tblTimeRegister ADD STATUS1 longtext"
            'Dim sSql1 = "update tblTimeRegister set STATUS1=STATUS"
            'Dim sSql2 = "alter table tblTimeRegister drop column STATUS"
            'Dim sSql3 = "ALTER TABLE tblTimeRegister ADD STATUS longtext"
            'Dim sSql4 = "update tblTimeRegister set STATUS=STATUS1"
            'Dim sSql5 = "alter table tblTimeRegister drop column STATUS1"           
            'Dim cmd As OleDbCommand
            'cmd = New OleDbCommand(sSql, con1Tmp)
            'cmd.ExecuteNonQuery()
            'cmd = New OleDbCommand(sSql1, con1Tmp)
            'cmd.ExecuteNonQuery()
            'cmd = New OleDbCommand(sSql2, con1Tmp)
            'cmd.ExecuteNonQuery()
            'cmd = New OleDbCommand(sSql3, con1Tmp)
            'cmd.ExecuteNonQuery()
            'cmd = New OleDbCommand(sSql4, con1Tmp)
            'cmd.ExecuteNonQuery()
            'cmd = New OleDbCommand(sSql5, con1Tmp)
            'cmd.ExecuteNonQuery()            
        Else
            'sSql = "ALTER TABLE tblTimeRegister ALTER COLUMN STATUS varchar(max)"
            'If Common.con.State <> ConnectionState.Open Then
            '    Common.con.Open()
            'End If
            'Dim cmd As SqlCommand
            'cmd = New SqlCommand(sSql, Common.con)
            'cmd.ExecuteNonQuery()
            'If Common.con.State <> ConnectionState.Closed Then
            '    Common.con.Close()
            'End If
        End If
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check Bio1EcoPort in tblMachine
        sSql = "select Bio1EcoPort from tblSetup"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            Dim sSql1 As String = "update tblSetup set Bio1EcoPort='9000'"
            If Common.servername = "Access" Then
                'con1 = New OleDbConnection(Common.ConnectionString)
                sSql = "ALTER TABLE tblSetup ADD Bio1EcoPort longtext"
                'If con1.State <> ConnectionState.Open Then
                '    con1.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()

                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                'If con1.State <> ConnectionState.Closed Then
                '    con1.Close()
                '    con1.Dispose()
                'End If
            Else
                sSql = "ALTER TABLE tblSetup ADD Bio1EcoPort varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()

                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'new in CustomisedReport
        sSql = "select GuardianName from CustomisedReport"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            sSql = "ALTER TABLE CustomisedReport ADD " & _
                "GuardianName varchar(50) " & _
                ",Designation varchar(50) " & _
                ",DateOfJoining varchar(50) " & _
                ",Gender varchar(50) " & _
                ",PaycodeCap varchar(50) " & _
                ",NameCap varchar(50) " & _
                ",CardNumberCap varchar(50) " & _
                ",CompanyCap varchar(50) " & _
                ",LocationCap varchar(50) " & _
                ",DepartmentCap varchar(50) " & _
                ",GradeCap varchar(50) " & _
                ",CatagoryCap varchar(50) " & _
                ",EmpGrpCap varchar(50) " & _
                ",SHIFTATTENDEDCap varchar(50) " & _
                ",SHIFTSTARTTIMECap varchar(50) " & _
                ",SHIFTENDTIMECap varchar(50) " & _
                ",IN1Cap varchar(50) " & _
                ",OUT1Cap varchar(50) " & _
                ",IN2Cap varchar(50) " & _
                ",OUT2Cap varchar(50) " & _
                ",STATUSCap varchar(50) " & _
                ",HOURSWORKEDCap varchar(50) " & _
                ",LATEARRIVALCap varchar(50) " & _
                ",EARLYDEPARTURECap varchar(50) " & _
                ",OTDURATIONCap varchar(50) " & _
                ",OSDURATIONCap varchar(50) " & _
                ",GuardianNameCap varchar(50) " & _
                ",DesignationCap varchar(50) " & _
                ",DateOfJoiningCap varchar(50) " & _
                ",GenderCap varchar(50) " & _
                ",PaycodePos varchar(50) " & _
                ",NamePos varchar(50) " & _
                ",CardNumberPos varchar(50) " & _
                ",CompanyPos varchar(50) " & _
                ",LocationPos varchar(50) " & _
                ",DepartmentPos varchar(50) " & _
                ",GradePos varchar(50) " & _
                ",CatagoryPos varchar(50) " & _
                ",EmpGrpPos varchar(50) " & _
                ",SHIFTATTENDEDPos varchar(50) " & _
                ",SHIFTSTARTTIMEPos varchar(50) " & _
                ",SHIFTENDTIMEPos varchar(50) " & _
                ",IN1Pos varchar(50) " & _
                ",OUT1Pos varchar(50) " & _
                ",IN2Pos varchar(50) " & _
                ",OUT2Pos varchar(50) " & _
                ",STATUSPos varchar(50) " & _
                ",HOURSWORKEDPos varchar(50) " & _
                ",LATEARRIVALPos varchar(50) " & _
                ",EARLYDEPARTUREPos varchar(50) " & _
                ",OTDURATIONPos varchar(50) " & _
                ",OSDURATIONPos varchar(50) " & _
                ",GuardianNamePos varchar(50) " & _
                ",DesignationPos varchar(50) " & _
                ",DateOfJoiningPos varchar(50) " & _
                ",GenderPos varchar(50) " & _
                ",Format varchar(50) " & _
                ",DateFormat varchar(50) " & _
                ",PreIsPrifix varchar(50) " & _
                ",PreLength varchar(50) " & _
                ",PreText varchar(50) " & _
                ",PayIsPrefix varchar(50) " & _
                ",PayLength varchar(50) " & _
                ",PayText varchar(50) "

            If Common.servername = "Access" Then
                'sSql = "ALTER TABLE tblMachine ADD LEDIP longtext"
                sSql = sSql.Replace("varchar(50)", "longtext")
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd.Dispose()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                'sSql = "ALTER TABLE tblMachine ADD LEDIP varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'new in CustomisedReport
        sSql = "select DateCol from CustomisedReport"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            sSql = "ALTER TABLE CustomisedReport ADD DateCol varchar(50), DateColCap varchar(50),DateColPos varchar(50) "
            If Common.servername = "Access" Then
                sSql = sSql.Replace("varchar(50)", "longtext")
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd.Dispose()
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try

        'EXCLUNCHHOURS from CustomisedReport
        sSql = "select EXCLUNCHHOURS from CustomisedReport"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            sSql = "ALTER TABLE CustomisedReport ADD EXCLUNCHHOURS varchar(50), EXCLUNCHHOURSCap varchar(50),EXCLUNCHHOURSPos varchar(50) "
            If Common.servername = "Access" Then
                sSql = sSql.Replace("varchar(50)", "longtext")
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd.Dispose()
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try


        'check LastUpdated in InstallSystemInfo
        sSql = "select LastUpdated from InstallSystemInfo"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                'con1 = New OleDbConnection(Common.ConnectionString)
                sSql = "ALTER TABLE InstallSystemInfo ADD LastUpdated datetime"
                'If con1.State <> ConnectionState.Open Then
                '    con1.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1.State <> ConnectionState.Closed Then
                '    con1.Close()
                '    con1.Dispose()
                'End If
            Else
                sSql = "ALTER TABLE InstallSystemInfo ADD LastUpdated datetime"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'new in tblUser
        sSql = "select DeviceMgmt from tblUser"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            sSql = "ALTER TABLE tblUser ADD " & _
               "DeviceMgmt varchar(50) " & _
                ",DeviceAdd varchar(50) " & _
                ",DeviceModi varchar(50) " & _
                ",DeviceDelete varchar(50) " & _
               ",LogMgmt varchar(50) " & _
               ",UserSetupTemplate varchar(50) " & _
               ",DBSetting varchar(50) " & _
               ",SMSSetting varchar(50) " & _
               ",BulkSMS varchar(50) " & _
               ",EmailSetting varchar(50) " & _
               ",BackUpSetting varchar(50) " & _
               ",ParallelSetting varchar(50) " & _
                ",Branch varchar(50) " & _
                ",BranchAdd varchar(50) " & _
                ",BranchModi varchar(50) " & _
                ",BranchDel varchar(50) " & _
                ",MonthlyReport varchar(50) " & _
                ",DalyReport varchar(50) " & _
                ",LeaveReport varchar(50) "

            Dim sSql1 As String = "update tblUser set " & _
                "DeviceMgmt='Y' " & _
                ",DeviceAdd='Y' " & _
                ",DeviceModi='Y' " & _
                ",DeviceDelete='Y' " & _
                ",LogMgmt='Y' " & _
                ",UserSetupTemplate='Y' " & _
                ",DBSetting='Y' " & _
                ",SMSSetting='Y' " & _
                ",BulkSMS='Y' " & _
                ",EmailSetting='Y' " & _
                ",BackUpSetting='Y' " & _
                ",ParallelSetting='Y' " & _
                ",Branch='Y' " & _
                ",BranchAdd='Y' " & _
                ",BranchModi='Y' " & _
                ",BranchDel='Y' " & _
                ",MonthlyReport='Y' " & _
                ",DalyReport='Y' " & _
                ",LeaveReport='Y' "
            If Common.servername = "Access" Then
                sSql = sSql.Replace("varchar(50)", "longtext")
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd.Dispose()
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try


        'check LeftEye, RightEye in fptable
        sSql = "select LeftEye, RightEye from fptable"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE fptable ADD LeftEye OLEObject, RightEye OLEObject"
                Dim sSql1 As String = "ALTER TABLE fptable ALTER COLUMN Cardnumber longtext"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE fptable ADD LeftEye image, RightEye image"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check LastUpdated in TblEmployee
        sSql = "select  top 1 MachineCard from TblEmployee"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE TblEmployee ADD MachineCard longtext"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE TblEmployee ADD MachineCard varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'remove primary key from leaveapplication
        If Common.servername = "Access" Then
        Else
            Try
                sSql = "ALTER TABLE leaveapplication DROP CONSTRAINT PK_LeaveApplication"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            Catch ex As Exception
            End Try
        End If
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'check IsIOCLCheck in MachineRawPunch
        sSql = "select  top 1 IsIOCLCheck from MachineRawPunch"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE MachineRawPunch ADD IsIOCLCheck longtext"
                'If con1Tmp.State <> ConnectionState.Open Then
                '    con1Tmp.Open()
                'End If
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                'If con1Tmp.State <> ConnectionState.Closed Then
                '    con1Tmp.Close()
                'End If
            Else
                sSql = "ALTER TABLE MachineRawPunch ADD IsIOCLCheck varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check IsIOCL in tblSetup
        sSql = "select IsIOCL, IOCLInterval, IOCLLink from tblSetup"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblSetup ADD IsIOCL longtext, IOCLInterval longtext, IOCLLink longtext"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE tblSetup ADD IsIOCL varchar(max), IOCLInterval varchar(max), IOCLLink varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'EXCLUNCHHOURS from CustomisedReport
        sSql = "select EXCLUNCHHOURS from CustomisedReport"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            sSql = "ALTER TABLE CustomisedReport ADD EXCLUNCHHOURS varchar(50), EXCLUNCHHOURSCap varchar(50),EXCLUNCHHOURSPos varchar(50) "
            If Common.servername = "Access" Then
                sSql = sSql.Replace("varchar(50)", "longtext")
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd.Dispose()
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'TELEPHONE2 from CustomisedReport  ADHAR 
        sSql = "select TELEPHONE2 from CustomisedReport"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            sSql = "ALTER TABLE CustomisedReport ADD TELEPHONE2 varchar(50), TELEPHONE2Cap varchar(50),TELEPHONE2Pos varchar(50) "
            If Common.servername = "Access" Then
                sSql = sSql.Replace("varchar(50)", "longtext")
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd.Dispose()
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check IsWDMS, TWCloud, iDMS in tblSetup
        sSql = "select IsWDMS, TWCloud, iDMS from tblSetup"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblSetup ADD IsWDMS longtext, TWCloud longtext, iDMS longtext"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE tblSetup ADD IsWDMS varchar(max), TWCloud varchar(max), iDMS varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check WDMSDBName, iDMSDBName in tblSetup
        sSql = "select WDMSDBName, IsBioSecurity, BioSecurityDBName, ZKAccessDBName from tblSetup"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblSetup ADD  WDMSDBName longtext, IsBioSecurity longtext, BioSecurityDBName longtext, IsZKAccess longtext, ZKAccessDBName longtext"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE tblSetup ADD WDMSDBName varchar(max), IsBioSecurity varchar(max), BioSecurityDBName varchar(max), IsZKAccess varchar(max), ZKAccessDBName varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check IsTWAccess, TWAccessDBName in tblSetup
        sSql = "select  IsTWAccess, TWAccessDBName from tblSetup"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblSetup ADD  IsTWAccess longtext, TWAccessDBName longtext, iDMSDB longtext, TWCloudDB longtext"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE tblSetup ADD IsTWAccess varchar(max), TWAccessDBName varchar(max), iDMSDB varchar(max), TWCloudDB varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()



        'check  in MIN_MAX_DATE
        sSql = "select  PUNCH_DATE_TWCloud,PUNCH_DATE_iDMS,PUNCH_DATE_WDMS,PUNCH_DATE_BioSecurity,PUNCH_DATE_ZKAccess,PUNCH_DATE_TWAccess from MIN_MAX_DATE"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            Dim sSql1 As String = "DROP TABLE MIN_MAX_DATE"
            If Common.servername = "Access" Then
                sSql = "Create TABLE MIN_MAX_DATE (PUNCH_DATE_TWCloud datetime,PUNCH_DATE_iDMS datetime,PUNCH_DATE_WDMS datetime,PUNCH_DATE_BioSecurity datetime,PUNCH_DATE_ZKAccess datetime,PUNCH_DATE_TWAccess datetime)"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()

                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "Create TABLE MIN_MAX_DATE (PUNCH_DATE_TWCloud datetime,PUNCH_DATE_iDMS datetime,PUNCH_DATE_WDMS datetime,PUNCH_DATE_BioSecurity datetime,PUNCH_DATE_ZKAccess datetime,PUNCH_DATE_TWAccess datetime)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()

                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check OnlineStartUp in tblSetup
        sSql = "select  OnlineStartUp from tblSetup"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblSetup ADD  OnlineStartUp longtext"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                sSql = "update tblsetup set OnlineStartUp ='Y'"
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE tblSetup ADD OnlineStartUp varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                sSql = "update tblsetup set OnlineStartUp ='Y'"
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()



        'check tblDivision
        sSql = "select Divisioncode from tblDivision"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                sSql = "insert into tblDivision (Divisioncode,DivisionName)  values ('001', 'General')"
                If Common.servername = "Access" Then
                    Dim cmd As OleDbCommand
                    cmd = New OleDbCommand(sSql, con1Tmp)
                    cmd.ExecuteNonQuery()
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    Dim cmd As SqlCommand
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()



        'check ManualRoster in tblTimeRegister
        sSql = "select  ManualRoster from tblTimeRegister"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblTimeRegister ADD  ManualRoster char(1)"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                sSql = "update tblsetup set OnlineStartUp ='Y'"
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE tblTimeRegister ADD ManualRoster varchar(1)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                sSql = "update tblTimeRegister set ManualRoster ='N'"
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()



        'check DownloadedTime in MachineRawPunch
        sSql = "select  DownloadedTime from MachineRawPunch"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE MachineRawPunch ADD  DownloadedTime DateTime"
                Dim sSql1 As String = "ALTER TABLE MachineRawPunchAll ADD  DownloadedTime DateTime"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE MachineRawPunch ADD DownloadedTime DateTime"
                Dim sSql1 As String = "ALTER TABLE MachineRawPunchAll ADD DownloadedTime DateTime"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()



        'check  in ApplicationLog
        sSql = "select top 1 * from ApplicationLog"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "Create TABLE ApplicationLog ([Id] AUTOINCREMENT, LogText longtext,LogTime datetime, LogedInUser longtext)"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "Create TABLE ApplicationLog ([Id] [int] IDENTITY(1,1) NOT NULL ,LogText varchar(max),LogTime datetime, LogedInUser varchar(max))"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check City in InstallSystemInfo
        sSql = "select  City from InstallSystemInfo"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE InstallSystemInfo ADD  City longtext, State longtext, Pincode longtext"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()                
            Else
                sSql = "ALTER TABLE InstallSystemInfo ADD City varchar(MAX), State varchar(Max), Pincode varchar(50)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check IsCompliance in TblSetup
        sSql = "select  IsCompliance from TblSetup"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE TblSetup ADD  IsCompliance longtext"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE TblSetup ADD IsCompliance varchar(MAX)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()

        'check Temperature in MachineRawPunch
        sSql = "select top 1 Temperature from MachineRawPunch"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE MachineRawPunch ADD  Temperature longtext"
                Dim sSql1 As String = "ALTER TABLE MachineRawPunchAll ADD  Temperature longtext"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE MachineRawPunch ADD Temperature varchar(MAX)"
                Dim sSql1 As String = "ALTER TABLE MachineRawPunchAll ADD Temperature varchar(MAX)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check IsCompliance in TblSetup
        sSql = "select  IsCompliance from tblUser"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblUser ADD  IsCompliance longtext"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE tblUser ADD IsCompliance varchar(MAX)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        ''check  in tbldepartment lenth 5
        'sSql = "select COL_LENGTH ( 'tbldepartment' , 'DEPARTMENTCODE' ) "
        'ds = New DataSet
        'Try
        '    If Common.servername <> "Access" Then
        '        adap = New SqlDataAdapter(sSql, Common.con)
        '        adap.Fill(ds)
        '    End If
        '    If ds.Tables(0).Rows(0)(0).ToString.Trim = "3" Then
        '        sSql = "ALTER TABLE tbldepartment DROP CONSTRAINT PK__tblDepartment__79A81403"
        '        If Common.con.State <> ConnectionState.Open Then
        '            Common.con.Open()
        '        End If
        '        Dim cmd As SqlCommand
        '        cmd = New SqlCommand(sSql, Common.con)
        '        cmd.ExecuteNonQuery()
        '        sSql = "ALTER TABLE tbldepartment ALTER COLUMN DEPARTMENTCODE varchar(5)"
        '        cmd = New SqlCommand(sSql, Common.con)
        '        cmd.ExecuteNonQuery()
        '        If Common.con.State <> ConnectionState.Closed Then
        '            Common.con.Close()
        '        End If
        '    End If
        'Catch ex As Exception

        'End Try
        'login.ProgressBarControl1.PerformStep()
        'login.ProgressBarControl1.Update()
        'Application.DoEvents()


        'check ManOTDuration in tblTimeRegister
        sSql = "select top 1 ManOTDuration,OTApprove from tblTimeRegister"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE tblTimeRegister ADD  ManOTDuration longtext, OTApprove longtext"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE tblTimeRegister ADD ManOTDuration varchar(MAX), OTApprove varchar(max)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        'check VerifyMode in MachineRawPunch
        sSql = "select  VerifyMode from MachineRawPunch"
        ds = New DataSet
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Tmp)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE MachineRawPunch ADD  VerifyMode longtext"
                Dim sSql1 As String = "ALTER TABLE MachineRawPunchAll ADD  VerifyMode longtext"
                Dim cmd As OleDbCommand
                cmd = New OleDbCommand(sSql, con1Tmp)
                cmd.ExecuteNonQuery()
                cmd = New OleDbCommand(sSql1, con1Tmp)
                cmd.ExecuteNonQuery()
            Else
                sSql = "ALTER TABLE MachineRawPunch ADD VerifyMode varchar(50)"
                Dim sSql1 As String = "ALTER TABLE MachineRawPunchAll ADD VerifyMode varchar(50)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
        login.ProgressBarControl1.PerformStep()
        login.ProgressBarControl1.Update()
        Application.DoEvents()


        If Common.servername = "Access" Then
            If con1Tmp.State <> ConnectionState.Closed Then
                con1Tmp.Close()
            End If
        End If
        If SetEmpGrpIdBool Then
            SetEmpGrpId()
        End If
    End Sub
    Public Shared Sub LoadUserMgmt(userName As String)
        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = " Select * from tblUser where USER_R = '" & userName & "'"
        If servername = "Access" Then
            adapAc = New OleDbDataAdapter(sSql, con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, con)
            adapS.Fill(Rs)
        End If
        USER_R = Rs.Tables(0).Rows(0).Item("USER_R").ToString.Trim
        USERDESCRIPRION = Rs.Tables(0).Rows(0).Item("USERDESCRIPRION").ToString.Trim
        PASSWORD = Rs.Tables(0).Rows(0).Item("PASSWORD").ToString.Trim
        AutoProcess = Rs.Tables(0).Rows(0).Item("AutoProcess").ToString.Trim
        DataProcess = Rs.Tables(0).Rows(0).Item("DataProcess").ToString.Trim
        Main = Rs.Tables(0).Rows(0).Item("Main").ToString.Trim
        V_Transaction = Rs.Tables(0).Rows(0).Item("V_Transaction").ToString.Trim
        Admin = Rs.Tables(0).Rows(0).Item("Admin").ToString.Trim
        Payroll = Rs.Tables(0).Rows(0).Item("Payroll").ToString.Trim
        Reports = Rs.Tables(0).Rows(0).Item("Reports").ToString.Trim
        Leave = Rs.Tables(0).Rows(0).Item("Leave").ToString.Trim
        Company = Rs.Tables(0).Rows(0).Item("Company").ToString.Trim
        Department = Rs.Tables(0).Rows(0).Item("Department").ToString.Trim
        Section = Rs.Tables(0).Rows(0).Item("Section").ToString.Trim
        Grade = Rs.Tables(0).Rows(0).Item("Grade").ToString.Trim
        Category = Rs.Tables(0).Rows(0).Item("Category").ToString.Trim
        Shift = Rs.Tables(0).Rows(0).Item("Shift").ToString.Trim
        Employee = Rs.Tables(0).Rows(0).Item("Employee").ToString.Trim
        Visitor = Rs.Tables(0).Rows(0).Item("Visitor").ToString.Trim
        Reason_Card = Rs.Tables(0).Rows(0).Item("Reason_Card").ToString.Trim
        Manual_Attendance = Rs.Tables(0).Rows(0).Item("Manual_Attendance").ToString.Trim
        OstoOt = Rs.Tables(0).Rows(0).Item("OstoOt").ToString.Trim
        ShiftChange = Rs.Tables(0).Rows(0).Item("ShiftChange").ToString.Trim
        HoliDay = Rs.Tables(0).Rows(0).Item("HoliDay").ToString.Trim
        LeaveMaster = Rs.Tables(0).Rows(0).Item("LeaveMaster").ToString.Trim
        LeaveApplication = Rs.Tables(0).Rows(0).Item("LeaveApplication").ToString.Trim
        LeaveAccural = Rs.Tables(0).Rows(0).Item("LeaveAccural").ToString.Trim
        LeaveAccuralAuto = Rs.Tables(0).Rows(0).Item("LeaveAccuralAuto").ToString.Trim
        TimeOfficeSetup = Rs.Tables(0).Rows(0).Item("TimeOfficeSetup").ToString.Trim
        UserPrevilege = Rs.Tables(0).Rows(0).Item("UserPrevilege").ToString.Trim
        Verification = Rs.Tables(0).Rows(0).Item("Verification").ToString.Trim
        InstallationSetup = Rs.Tables(0).Rows(0).Item("InstallationSetup").ToString.Trim
        EmployeeSetup = Rs.Tables(0).Rows(0).Item("EmployeeSetup").ToString.Trim
        ArearEntry = Rs.Tables(0).Rows(0).Item("ArearEntry").ToString.Trim
        Advance_Loan = Rs.Tables(0).Rows(0).Item("Advance_Loan").ToString.Trim
        Form16 = Rs.Tables(0).Rows(0).Item("Form16").ToString.Trim
        Form16Return = Rs.Tables(0).Rows(0).Item("Form16Return").ToString.Trim
        payrollFormula = Rs.Tables(0).Rows(0).Item("payrollFormula").ToString.Trim
        PayrollSetup = Rs.Tables(0).Rows(0).Item("PayrollSetup").ToString.Trim
        LoanAdjustment = Rs.Tables(0).Rows(0).Item("LoanAdjustment").ToString.Trim
        TimeOfficeReport = Rs.Tables(0).Rows(0).Item("TimeOfficeReport").ToString.Trim
        VisitorReport = Rs.Tables(0).Rows(0).Item("VisitorReport").ToString.Trim
        PayrollReport = Rs.Tables(0).Rows(0).Item("PayrollReport").ToString.Trim
        RegisterCreation = Rs.Tables(0).Rows(0).Item("RegisterCreation").ToString.Trim
        RegisterUpdation = Rs.Tables(0).Rows(0).Item("RegisterUpdation").ToString.Trim
        BackDateProcess = Rs.Tables(0).Rows(0).Item("BackDateProcess").ToString.Trim
        ReProcess = Rs.Tables(0).Rows(0).Item("ReProcess").ToString.Trim
        OTCAL = Rs.Tables(0).Rows(0).Item("OTCAL").ToString.Trim
        auth_comp = Rs.Tables(0).Rows(0).Item("auth_comp").ToString.Trim
        Auth_dept = Rs.Tables(0).Rows(0).Item("Auth_dept").ToString.Trim
        Auth_Branch = Rs.Tables(0).Rows(0).Item("Auth_Branch").ToString.Trim

        USERTYPE = Rs.Tables(0).Rows(0).Item("USERTYPE").ToString.Trim
        paycode = Rs.Tables(0).Rows(0).Item("paycode").ToString.Trim
        CompAdd = Rs.Tables(0).Rows(0).Item("CompAdd").ToString.Trim
        CompModi = Rs.Tables(0).Rows(0).Item("CompModi").ToString.Trim
        CompDel = Rs.Tables(0).Rows(0).Item("CompDel").ToString.Trim
        DeptAdd = Rs.Tables(0).Rows(0).Item("DeptAdd").ToString.Trim
        DeptModi = Rs.Tables(0).Rows(0).Item("DeptModi").ToString.Trim
        DeptDel = Rs.Tables(0).Rows(0).Item("DeptDel").ToString.Trim
        CatAdd = Rs.Tables(0).Rows(0).Item("CatAdd").ToString.Trim
        CatModi = Rs.Tables(0).Rows(0).Item("CatModi").ToString.Trim
        CatDel = Rs.Tables(0).Rows(0).Item("CatDel").ToString.Trim
        SecAdd = Rs.Tables(0).Rows(0).Item("SecAdd").ToString.Trim
        SecModi = Rs.Tables(0).Rows(0).Item("SecModi").ToString.Trim
        SecDel = Rs.Tables(0).Rows(0).Item("SecDel").ToString.Trim
        GrdAdd = Rs.Tables(0).Rows(0).Item("GrdAdd").ToString.Trim
        GrdModi = Rs.Tables(0).Rows(0).Item("GrdModi").ToString.Trim
        GrdDel = Rs.Tables(0).Rows(0).Item("GrdDel").ToString.Trim
        SftAdd = Rs.Tables(0).Rows(0).Item("SftAdd").ToString.Trim
        SftModi = Rs.Tables(0).Rows(0).Item("SftModi").ToString.Trim
        SftDel = Rs.Tables(0).Rows(0).Item("SftDel").ToString.Trim
        EmpAdd = Rs.Tables(0).Rows(0).Item("EmpAdd").ToString.Trim
        EmpModi = Rs.Tables(0).Rows(0).Item("EmpModi").ToString.Trim
        EmpDel = Rs.Tables(0).Rows(0).Item("EmpDel").ToString.Trim
        DataMaintenance = Rs.Tables(0).Rows(0).Item("DataMaintenance").ToString.Trim
        canteen = Rs.Tables(0).Rows(0).Item("canteen").ToString.Trim


        DeviceMgmt = Rs.Tables(0).Rows(0).Item("DeviceMgmt").ToString.Trim
        DeviceAdd = Rs.Tables(0).Rows(0).Item("DeviceAdd").ToString.Trim
        DeviceModi = Rs.Tables(0).Rows(0).Item("DeviceModi").ToString.Trim
        DeviceDelete = Rs.Tables(0).Rows(0).Item("DeviceDelete").ToString.Trim
        LogMgmt = Rs.Tables(0).Rows(0).Item("LogMgmt").ToString.Trim
        UserSetupTemplate = Rs.Tables(0).Rows(0).Item("UserSetupTemplate").ToString.Trim
        DBSetting = Rs.Tables(0).Rows(0).Item("DBSetting").ToString.Trim
        SMSSetting = Rs.Tables(0).Rows(0).Item("SMSSetting").ToString.Trim
        BulkSMS = Rs.Tables(0).Rows(0).Item("BulkSMS").ToString.Trim
        EmailSetting = Rs.Tables(0).Rows(0).Item("EmailSetting").ToString.Trim
        BackUpSetting = Rs.Tables(0).Rows(0).Item("BackUpSetting").ToString.Trim
        ParallelSetting = Rs.Tables(0).Rows(0).Item("ParallelSetting").ToString.Trim
        Branch = Rs.Tables(0).Rows(0).Item("Branch").ToString.Trim
        BranchAdd = Rs.Tables(0).Rows(0).Item("BranchAdd").ToString.Trim
        BranchModi = Rs.Tables(0).Rows(0).Item("BranchModi").ToString.Trim
        BranchDel = Rs.Tables(0).Rows(0).Item("BranchDel").ToString.Trim
        MonthlyReport = Rs.Tables(0).Rows(0).Item("MonthlyReport").ToString.Trim
        DalyReport = Rs.Tables(0).Rows(0).Item("DalyReport").ToString.Trim
        LeaveReport = Rs.Tables(0).Rows(0).Item("LeaveReport").ToString.Trim


        If Rs.Tables(0).Rows(0).Item("IsCompliance").ToString.Trim = "Y" Then
            IsCompliance = True
            gLDuplicate = "Y"
        Else
            IsCompliance = False
        End If
        If USER_R.ToLower = "admin" Then
            AdminPASSWORD = PASSWORD
        Else
            sSql = " Select * from tblUser where USER_R = 'admin'"
            Rs = New DataSet
            If servername = "Access" Then
                adapAc = New OleDbDataAdapter(sSql, con1)
                adapAc.Fill(Rs)
            Else
                adapS = New SqlDataAdapter(sSql, con)
                adapS.Fill(Rs)
            End If
            AdminPASSWORD = Rs.Tables(0).Rows(0).Item("PASSWORD").ToString.Trim
        End If

    End Sub
    'end for user mgmt



    Public Shared Sub LoadParallelDB()
        Dim sSql As String = "Select * from ParallelDB"
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim ds As DataSet = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            IsParallel = ds.Tables(0).Rows(0).Item("IsParallel").ToString.Trim()
            DBType = ds.Tables(0).Rows(0).Item("DBType").ToString.Trim()
            PreIsPrifix = ds.Tables(0).Rows(0).Item("PreIsPrifix").ToString.Trim()
            PreLength = ds.Tables(0).Rows(0).Item("PreLength").ToString.Trim()
            PreText = ds.Tables(0).Rows(0).Item("PreText").ToString.Trim()
            PayIsPrefix = ds.Tables(0).Rows(0).Item("PayIsPrefix").ToString.Trim()
            PayLength = ds.Tables(0).Rows(0).Item("PayLength").ToString.Trim()
            PayText = ds.Tables(0).Rows(0).Item("PayText").ToString.Trim()
            PunchDateFormat = ds.Tables(0).Rows(0).Item("PunchDateFormat").ToString.Trim()
            PunchTimeFormat = ds.Tables(0).Rows(0).Item("PunchTimeFormat").ToString.Trim()
            PunchDateTimeFormat = ds.Tables(0).Rows(0).Item("PunchDateTimeFormat").ToString.Trim()
            InValue = ds.Tables(0).Rows(0).Item("InValue").ToString.Trim()
            OutVaue = ds.Tables(0).Rows(0).Item("OutVaue").ToString.Trim()
            YesValue = ds.Tables(0).Rows(0).Item("YesValue").ToString.Trim()
            NoValue = ds.Tables(0).Rows(0).Item("NoValue").ToString.Trim()
            PConnectionString = ds.Tables(0).Rows(0).Item("ConnectionString").ToString.Trim()
            PQuery = ds.Tables(0).Rows(0).Item("Query").ToString.Trim()
            Rfield1Val = ds.Tables(0).Rows(0).Item("Rfield1Value").ToString.Trim()
            Rfield2Val = ds.Tables(0).Rows(0).Item("Rfield2Value").ToString.Trim()
            Rfield3Val = ds.Tables(0).Rows(0).Item("Rfield3Value").ToString.Trim()
        Else
            IsParallel = "N"
        End If
    End Sub
    Public Shared Function parallelInsert(PreCardNoVal As String, PaycodeVal As String, punchdatetime As DateTime, PunchDirectionVal As String, IsManualVal As String, EmpNameVal As String, DeviceidVal As String)
        Dim sSql As String = PQuery
        Dim PunchDateFormatTmp As String = PunchDateFormat
        Dim PunchTimeFormatTmp As String = PunchTimeFormat
        Dim PunchDateTimeFormatTmp As String = PunchDateTimeFormat

        If DBType = "SQL Server" Then
            Dim pcon As SqlConnection = New SqlConnection(PConnectionString)
            Dim pcmd As SqlCommand
            If PQuery.Contains("PreCardNoVal") Then
                If PreIsPrifix = "Y" Then
                    PreCardNoVal = PreCardNoVal.PadLeft(PreLength, PreText)
                End If
                sSql = sSql.Replace("PreCardNoVal", PreCardNoVal)
            End If
            If PQuery.Contains("PaycodeVal") Then
                If PayIsPrefix = "Y" Then
                    PaycodeVal = PreCardNoVal.PadLeft(PayLength, PayText)
                End If
                sSql = sSql.Replace("PaycodeVal", PaycodeVal)
            End If
            If PQuery.Contains("PunchDateVal") Then
                If PunchDateFormat = "" Then
                    PunchDateFormatTmp = "yyyy-MM-dd"
                End If
                sSql = sSql.Replace("PunchDateVal", punchdatetime.ToString(PunchDateFormatTmp))
            End If
            If PQuery.Contains("PunchTimeVal") Then
                If PunchTimeFormat = "" Then
                    PunchTimeFormatTmp = "HH:mm:ss"
                End If
                sSql = sSql.Replace("PunchTimeVal", punchdatetime.ToString(PunchTimeFormatTmp))
            End If
            If PQuery.Contains("PunchDateTimeVal") Then
                If PunchDateTimeFormat = "" Then
                    PunchDateTimeFormatTmp = "yyyy-MM-dd HH:mm:ss"
                End If
                sSql = sSql.Replace("PunchDateTimeVal", punchdatetime.ToString(PunchDateTimeFormatTmp))
            End If
            If PQuery.Contains("PunchDirectionVal") Then
                If PunchDirectionVal = "I" And InValue <> "" Then
                    PunchDirectionVal = InValue
                ElseIf PunchDirectionVal = "O" And OutVaue <> "" Then
                    PunchDirectionVal = OutVaue
                End If
                sSql = sSql.Replace("PunchDirectionVal", PunchDirectionVal)
            End If
            If PQuery.Contains("IsManualVal") Then
                If IsManualVal = "Y" And YesValue <> "" Then
                    IsManualVal = YesValue
                ElseIf IsManualVal = "O" And NoValue <> "" Then
                    IsManualVal = NoValue
                End If
                sSql = sSql.Replace("IsManualVal", IsManualVal)
            End If
            sSql = sSql.Replace("DeviceidVal", DeviceidVal)
            sSql = sSql.Replace("EmpNameVal", EmpNameVal)
            sSql = sSql.Replace("Rfield1Val", Rfield1Val)
            sSql = sSql.Replace("Rfield2Val", Rfield2Val)
            sSql = sSql.Replace("Rfield3Val", Rfield2Val)
            Try
                If pcon.State <> ConnectionState.Open Then
                    pcon.Open()
                End If
                pcmd = New SqlCommand(sSql, pcon)
                pcmd.ExecuteNonQuery()
                If pcon.State <> ConnectionState.Closed Then
                    pcon.Close()
                End If
            Catch ex As Exception
                Dim fs As FileStream = New FileStream("parallel.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.End)
                sw.WriteLine(vbCrLf & ex.Message & ", " & sSql)
                sw.Flush()
                sw.Close()
            End Try
        ElseIf DBType = "MY SQL" Then
            Dim pcon As MySqlConnection = New MySqlConnection(PConnectionString)
            Dim pcmd As MySqlCommand
            If PQuery.Contains("PreCardNoVal") Then
                If PreIsPrifix = "Y" Then
                    PreCardNoVal = PreCardNoVal.PadLeft(PreLength, PreText)
                End If
                sSql = sSql.Replace("PreCardNoVal", PreCardNoVal)
            End If
            If PQuery.Contains("PaycodeVal") Then
                If PayIsPrefix = "Y" Then
                    PaycodeVal = PreCardNoVal.PadLeft(PayLength, PayText)
                End If
                sSql = sSql.Replace("PaycodeVal", PaycodeVal)
            End If
            If PQuery.Contains("PunchDateVal") Then
                If PunchDateFormat = "" Then
                    PunchDateFormatTmp = "yyyy-MM-dd"
                End If
                sSql = sSql.Replace("PunchDateVal", punchdatetime.ToString(PunchDateFormatTmp))
            End If
            If PQuery.Contains("PunchTimeVal") Then
                If PunchTimeFormat = "" Then
                    PunchTimeFormatTmp = "HH:mm:ss"
                End If
                sSql = sSql.Replace("PunchTimeVal", punchdatetime.ToString(PunchTimeFormatTmp))
            End If
            If PQuery.Contains("PunchDateTimeVal") Then
                If PunchDateTimeFormat = "" Then
                    PunchDateTimeFormatTmp = "yyyy-MM-dd HH:mm:ss"
                End If
                sSql = sSql.Replace("PunchDateTimeVal", punchdatetime.ToString(PunchDateTimeFormatTmp))
            End If
            If PQuery.Contains("PunchDirectionVal") Then
                If PunchDirectionVal = "I" And InValue <> "" Then
                    PunchDirectionVal = InValue
                ElseIf PunchDirectionVal = "O" And OutVaue <> "" Then
                    PunchDirectionVal = OutVaue
                End If
                sSql = sSql.Replace("PunchDirectionVal", PunchDirectionVal)
            End If
            If PQuery.Contains("IsManualVal") Then
                If IsManualVal = "Y" And YesValue <> "" Then
                    IsManualVal = YesValue
                ElseIf IsManualVal = "O" And NoValue <> "" Then
                    IsManualVal = NoValue
                End If
                sSql = sSql.Replace("IsManualVal", IsManualVal)
            End If
            sSql = sSql.Replace("DeviceidVal", DeviceidVal)
            sSql = sSql.Replace("EmpNameVal", EmpNameVal)
            sSql = sSql.Replace("Rfield1Val", Rfield1Val)
            sSql = sSql.Replace("Rfield2Val", Rfield2Val)
            sSql = sSql.Replace("Rfield3Val", Rfield2Val)
            Try
                If pcon.State <> ConnectionState.Open Then
                    pcon.Open()
                End If
                pcmd = New MySqlCommand(sSql, pcon)
                pcmd.ExecuteNonQuery()
                If pcon.State <> ConnectionState.Closed Then
                    pcon.Close()
                End If
            Catch ex As Exception
                Dim fs As FileStream = New FileStream("parallel.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.End)
                sw.WriteLine(vbCrLf & ex.Message & ", " & sSql)
                sw.Flush()
                sw.Close()
            End Try
        ElseIf DBType = "Oracle" Then

            'Try
            '    Dim Constr As String = PConnectionString '"Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & HOST & ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & SERVICE_NAME & ")));user id=" & UserId & ";password=" & PASSWORD & ";"
            '    Dim conOr As OracleConnection = New OracleConnection(Constr)
            '    sSql = "Insert Into OracleAttendance (CARDNO,EDATE,PUNCH,INOUT,MACHINEIP)values ('" & PreCardNoVal & "',EDATE=to_date('" + punchdatetime.ToString("dd-MMM-yyyy") + "','dd-Mon-YYYY')),PUNCH=to_date('" + punchdatetime.ToString("HH:mm:ss") + "','HH:mm:ss')),'" & PunchDirectionVal & "','" & DeviceidVal & "' "
            '    If (conOr.State <> ConnectionState.Open) Then
            '        conOr.Open()
            '    End If
            '    Dim CmdOr As OracleCommand = New OracleCommand(sSql, conOr)
            '    Dim Result = CmdOr.ExecuteNonQuery
            '    If (conOr.State <> ConnectionState.Closed) Then
            '        conOr.Close()
            '    End If
            'Catch ex As Exception
            '    Dim fs As FileStream = New FileStream("parallel.txt", FileMode.OpenOrCreate, FileAccess.Write)
            '    Dim sw As StreamWriter = New StreamWriter(fs)
            '    sw.BaseStream.Seek(0, SeekOrigin.End)
            '    sw.WriteLine(vbCrLf & ex.Message & ", " & sSql)
            '    sw.Flush()
            '    sw.Close()
            'Finally

            'End Try


        End If
    End Function
    Public Shared EmpNonAdmin As New DataTable("TBLEmployee")
    Public Shared LocationNonAdmin As New DataTable("TBLBranch")
    Public Shared MachineNonAdmin As New DataTable("tblMachine")
    Public Shared CompanyNonAdmin As New DataTable("tblCompany")
    Public Shared EmpArr As String()
    Public Shared ComArr As String()
    Public Shared Sub loadEmp()
        EmpNonAdmin.Clear()
        If USERTYPE = "A" Then
            Dim gridselet As String = "select * from TBLEmployee order by PRESENTCARDNO"
            If Common.servername = "Access" Then
                Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
                dataAdapter.Fill(EmpNonAdmin)
            Else
                Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
                dataAdapter.Fill(EmpNonAdmin)
            End If
        Else
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next

            Dim com() As String = Common.auth_comp.Split(",")
            Dim lsc As New List(Of String)()
            For x As Integer = 0 To com.Length - 1
                lsc.Add(com(x).Trim)
            Next           
            Dim gridselet As String = "select * from TBLEmployee where BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and COMPANYCODE IN ('" & String.Join("', '", lsc.ToArray()) & "')  order by PRESENTCARDNO"
            If Common.servername = "Access" Then
                Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
                dataAdapter.Fill(EmpNonAdmin)
            Else
                Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
                dataAdapter.Fill(EmpNonAdmin)
            End If           
        End If
        Dim lsEmp As New List(Of String)()
        For i As Integer = 0 To EmpNonAdmin.Rows.Count - 1
            lsEmp.Add(EmpNonAdmin.Rows(i)("paycode").ToString.Trim)
        Next
        EmpArr = lsEmp.Distinct.ToArray
    End Sub
    Public Shared Sub loadLocation()
        LocationNonAdmin.Clear()
        If USERTYPE = "A" Then
            Dim gridselet As String = "select * from TBLBranch"
            If Common.servername = "Access" Then
                Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
                dataAdapter.Fill(LocationNonAdmin)
            Else
                Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
                dataAdapter.Fill(LocationNonAdmin)
            End If
        Else
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next
            Dim gridselet As String = "select * from TBLBranch where BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            If Common.servername = "Access" Then
                Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
                dataAdapter.Fill(LocationNonAdmin)
            Else
                Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
                dataAdapter.Fill(LocationNonAdmin)
            End If
        End If
    End Sub
    Public Shared Sub loadDevice()
        MachineNonAdmin.Clear()
        If USERTYPE = "A" Then
            Dim gridselet As String '= "select val(tblMachine.ID_NO), tblMachine.A_R, tblMachine.IN_OUT,tblMachine.DeviceType,tblMachine.LOCATION,tblMachine.branch,tblMachine.commkey,tblMachine.MAC_ADDRESS,tblMachine.Purpose,tblMachine.LastModifiedBy,tblMachine.LastModifiedDate from tblMachine"
            If Common.servername = "Access" Then
                gridselet = "select val(ID_NO) as ID_NO, A_R, IN_OUT,DeviceType,LOCATION,branch,commkey,MAC_ADDRESS,Purpose,LastModifiedBy,LastModifiedDate,Status,UserCount,FPCount,FaceCount,AttLogCount,AdminCount, LastDownloaded from tblMachine"
                Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
                dataAdapter.Fill(MachineNonAdmin)
            Else
                gridselet = "select cast(ID_NO as INT) as ID_NO, A_R,IN_OUT,DeviceType,LOCATION,branch,commkey,MAC_ADDRESS,Purpose,LastModifiedBy,LastModifiedDate,Status,UserCount,FPCount,FaceCount,AttLogCount,AdminCount, LastDownloaded from tblMachine"
                Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
                dataAdapter.Fill(MachineNonAdmin)
            End If
        Else
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next
            Dim gridselet As String '= "select cast(tblMachine.ID_NO as INT), tblMachine.A_R, tblMachine.IN_OUT,tblMachine.DeviceType,tblMachine.LOCATION,tblMachine.branch,tblMachine.commkey,tblMachine.MAC_ADDRESS,tblMachine.Purpose,tblMachine.LastModifiedBy,tblMachine.LastModifiedDate from tblMachine, tblbranch where tblMachine.branch=tblbranch.BRANCHNAME and tblbranch.BRANCHCODE in ('" & String.Join("', '", ls.ToArray()) & "')" '"select * from tblMachine where branch IN ('" & String.Join("', '", ls.ToArray()) & "')"

            If Common.servername = "Access" Then
                gridselet = "select val(tblMachine.ID_NO) as ID_NO, tblMachine.A_R, tblMachine.IN_OUT,tblMachine.DeviceType,tblMachine.LOCATION,tblMachine.branch,tblMachine.commkey,tblMachine.MAC_ADDRESS,tblMachine.Purpose,tblMachine.LastModifiedBy,tblMachine.LastModifiedDate, tblMachine.Status,tblMachine.UserCount,tblMachine.FPCount,tblMachine.FaceCount,tblMachine.AttLogCount,tblMachine.AdminCount, tblMachine.LastDownloaded from tblMachine, tblbranch where tblMachine.branch=tblbranch.BRANCHNAME and tblbranch.BRANCHCODE in ('" & String.Join("', '", ls.ToArray()) & "')"
                Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
                dataAdapter.Fill(MachineNonAdmin)
            Else
                gridselet = "select cast(tblMachine.ID_NO as INT) as ID_NO, tblMachine.A_R, tblMachine.IN_OUT,tblMachine.DeviceType,tblMachine.LOCATION,tblMachine.branch,tblMachine.commkey,tblMachine.MAC_ADDRESS,tblMachine.Purpose,tblMachine.LastModifiedBy,tblMachine.LastModifiedDate, tblMachine.Status,tblMachine.UserCount,tblMachine.FPCount,tblMachine.FaceCount,tblMachine.AttLogCount,tblMachine.AdminCount, tblMachine.LastDownloaded from tblMachine, tblbranch where tblMachine.branch=tblbranch.BRANCHNAME and tblbranch.BRANCHCODE in ('" & String.Join("', '", ls.ToArray()) & "')"
                Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
                dataAdapter.Fill(MachineNonAdmin)
            End If
        End If
        'Dim cmd As New SqlCommand
        'Dim cmd1 As New OleDbCommand
        'Dim sSql As String = "UPDATE tblMachine set  [Status] ='Offline' "
        'If Common.servername = "Access" Then
        '    If Common.con1.State <> ConnectionState.Open Then
        '        Common.con1.Open()
        '    End If
        '    cmd1 = New OleDbCommand(sSql, Common.con1)
        '    cmd1.ExecuteNonQuery()
        '    If Common.con1.State <> ConnectionState.Closed Then
        '        Common.con1.Close()
        '    End If
        'Else
        '    If Common.con.State <> ConnectionState.Open Then
        '        Common.con.Open()
        '    End If
        '    cmd = New SqlCommand(sSql, Common.con)
        '    cmd.ExecuteNonQuery()
        '    If Common.con.State <> ConnectionState.Closed Then
        '        Common.con.Close()
        '    End If
        'End If

    End Sub
    Public Shared Sub loadCompany()
        CompanyNonAdmin.Clear()
        If USERTYPE = "A" Then
            Dim gridselet As String = "select * from TBLCompany"
            If Common.servername = "Access" Then
                Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
                dataAdapter.Fill(CompanyNonAdmin)
            Else
                Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
                dataAdapter.Fill(CompanyNonAdmin)
            End If
        Else
            Dim emp() As String = Common.auth_comp.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next
            Dim gridselet As String = "select * from TBLCompany where COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            If Common.servername = "Access" Then
                Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
                dataAdapter.Fill(CompanyNonAdmin)
            Else
                Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
                dataAdapter.Fill(CompanyNonAdmin)
            End If
        End If
        Dim lsComp As New List(Of String)()
        For i As Integer = 0 To CompanyNonAdmin.Rows.Count - 1
            lsComp.Add(CompanyNonAdmin.Rows(i)("COMPANYCODE"))
        Next
        ComArr = lsComp.Distinct.ToArray
    End Sub
    Public Shared Function CheckDatabaseExists(ByVal server As String, ByVal database As String, ByVal auth As Integer, ByVal user As String, ByVal pwd As String) As Boolean
        Dim connString As String ' = ("Data Source=" & server & ";Initial Catalog=master;Integrated Security=True;")
        If auth = 0 Then
            connString = "Data Source=" & server & ";Initial Catalog=master;Integrated Security=True;"
        Else
            connString = "Data Source='" & server & "';Initial Catalog=master;User Id=" & user & ";Password=" & pwd & ";"
        End If
        Dim cmdText As String = ("select * from master.dbo.sysdatabases where name='" & database & "'")
        Dim bRet As Boolean = False
        Using sqlConnection As SqlConnection = New SqlConnection(connString)
            sqlConnection.Open()
            Using sqlCmd As SqlCommand = New SqlCommand(cmdText, sqlConnection)
                Using reader As SqlDataReader = sqlCmd.ExecuteReader
                    bRet = reader.HasRows
                End Using
            End Using
        End Using
        Return bRet
    End Function
    'Public Shared Function checkAndAttachDB(ByVal server As String, ByVal database As String, ByVal auth As Integer, ByVal user As String, ByVal pwd As String) As Boolean   'nitin
    '    Dim FileName1 As String = My.Application.Info.DirectoryPath & "\SSSDB.MDF"
    '    Dim Filename2 As String = My.Application.Info.DirectoryPath & "\SSSDB.LDF"
    '    Dim sSql As String = "sp_attach_db 'SSSDB', '" & FileName1 & "', '" & Filename2 & "'"
    '    Dim ConnectionStringTmp As String ' = "server = '" & server & "' ;Initial Catalog= 'master';Integrated Security=True"
    '    If auth = 0 Then
    '        ConnectionStringTmp = "Data Source=" & server & ";Initial Catalog=master;Integrated Security=True;"
    '    Else
    '        ConnectionStringTmp = "Data Source='" & server & "';Initial Catalog=master;User Id=" & user & ";Password=" & pwd & ";"
    '    End If
    '    Dim contmp As SqlConnection = New SqlConnection(ConnectionStringTmp)
    '    Dim cmdtmp As SqlCommand
    '    Try
    '        contmp.Open()
    '        cmdtmp = New SqlCommand(sSql, contmp)
    '        cmdtmp.ExecuteNonQuery()
    '        contmp.Close()
    '    Catch ex As Exception
    '        Return False
    '    End Try
    '    Return True
    '    'Dim ret1 As Boolean = AttachDatabase("SSSDB", FileName1, Filename2)
    '    'If ret1 = False Then
    '    '    XtraMessageBox.Show(ulf, "<size=10>DB Attach fail</size>", "Fail")
    '    '    Exit Sub
    '    'End If
    'End Function
    Public Shared Function checkAndAttachDB(ByVal server As String, ByVal database As String, ByVal auth As Integer, ByVal user As String, ByVal pwd As String) As Boolean   'nitin

        Dim RealTimeImagePath As String = System.Environment.CurrentDirectory & "\SQLDB"
        If (Not System.IO.Directory.Exists(RealTimeImagePath)) Then
            System.IO.Directory.CreateDirectory(RealTimeImagePath)
        End If


        'check SSSDB_Script.txt file
        Dim SavePath As String = My.Application.Info.DirectoryPath & "\SSSDB_Script.txt"
        If System.IO.File.Exists(SavePath) = False Then
            Dim outputStream As FileStream
            Try
                Application.DoEvents()
                Dim reqFTP As FtpWebRequest
                Dim filePath As String = My.Application.Info.DirectoryPath
                outputStream = New FileStream(filePath & "\SSSDB_Script.txt", FileMode.Create)
                'Dim outputStream As FileStream = New FileStream("F:\developement\TimeWatchNewDeskTop\iAS.exe", FileMode.Create)
                reqFTP = CType(FtpWebRequest.Create(New Uri("ftp://137.59.201.60/SSSDB_Script.txt")), FtpWebRequest)
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                reqFTP.UseBinary = True
                'reqFTP.Credentials = New NetworkCredential(ftpUserID, ftpPassword)
                Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                Dim ftpStream As Stream = response.GetResponseStream
                Dim cl As Long = response.ContentLength
                Dim bufferSize As Integer = 25000 ' 2048
                Dim readCount As Integer
                Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                readCount = ftpStream.Read(buffer, 0, bufferSize)

                While (readCount > 0)
                    outputStream.Write(buffer, 0, readCount)
                    readCount = ftpStream.Read(buffer, 0, bufferSize)
                    Application.DoEvents()
                End While
                ftpStream.Close()
                outputStream.Close()
                response.Close()
            Catch ex2 As Exception
                outputStream.Close()
                System.IO.File.Delete(SavePath)
                If ex2.Message.Trim = "Unable to connect to the remote server" Then
                    XtraMessageBox.Show("Unable to connect to the remote server. Please attach DataBase manually.")
                    Return False
                End If
               
            End Try
        End If
        'end check SSSDB_Script.txt file

        Dim ConnectionString As String = "server = '" & server & "' ;Initial Catalog= 'master';User Id=" & user & ";Password=" & pwd & ";"
        If auth = 0 Then
            ConnectionString = "server = '" & server & "' ;Initial Catalog= 'master';Integrated Security=True;"
        Else
            ConnectionString = "server = '" & server & "' ;Initial Catalog= 'master';User Id=" & user & ";Password=" & pwd & ";"
        End If
        Dim connection As SqlConnection = New SqlConnection(ConnectionString)
        Dim cmd As New SqlClient.SqlCommand
        Dim objReader As System.IO.StreamReader

        connection.Open()
        '"CREATE DATABASE " & database
        Dim sSql As String = "CREATE DATABASE [" & database & "] ON  PRIMARY ( NAME = N'" & database & "_Data', FILENAME = N'" & My.Application.Info.DirectoryPath & "\SQLDB\" & database & ".MDF')" & vbCrLf & _
        "LOG ON " & vbCrLf & _
        "( NAME = N'" & database & "_Log', FILENAME = N'" & My.Application.Info.DirectoryPath & "\SQLDB\" & database & ".LDF')" & vbCrLf & _
        "EXEC dbo.sp_dbcmptlevel @dbname=N'" & database & "', @new_cmptlevel=90" & vbCrLf & _
        "IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))" & vbCrLf & _
        "begin" & vbCrLf & _
        "EXEC [" & database & "].[dbo].[sp_fulltext_database] @action = 'enable'" & vbCrLf & _
        "end" & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET ANSI_NULL_DEFAULT OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET ANSI_NULLS OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET ANSI_PADDING OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET ANSI_WARNINGS OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET ARITHABORT OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET AUTO_CLOSE ON " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET AUTO_CREATE_STATISTICS ON " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET AUTO_SHRINK ON " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET AUTO_UPDATE_STATISTICS ON " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET CURSOR_CLOSE_ON_COMMIT OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET CURSOR_DEFAULT  GLOBAL " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET NUMERIC_ROUNDABORT OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET QUOTED_IDENTIFIER OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET RECURSIVE_TRIGGERS OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET  DISABLE_BROKER " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET AUTO_UPDATE_STATISTICS_ASYNC OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET DATE_CORRELATION_OPTIMIZATION OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET TRUSTWORTHY OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET ALLOW_SNAPSHOT_ISOLATION OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET PARAMETERIZATION SIMPLE " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET READ_COMMITTED_SNAPSHOT OFF " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET RECOVERY SIMPLE " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET  MULTI_USER " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET PAGE_VERIFY TORN_PAGE_DETECTION  " & vbCrLf & _
        "ALTER DATABASE [" & database & "] SET DB_CHAINING OFF "
        cmd = New SqlCommand(sSql, connection)
        cmd.ExecuteNonQuery()
        connection.Close()

        If auth = 0 Then
            ConnectionString = "server = '" & server & "' ;Initial Catalog= '" & database & "';Integrated Security=True;"
        Else
            ConnectionString = "server = '" & server & "' ;Initial Catalog= '" & database & "';User Id=" & user & ";Password=" & pwd & ";"
        End If

        connection = New SqlConnection(ConnectionString)
        connection.Open()
        cmd.CommandType = CommandType.Text
        cmd.Connection = connection

        objReader = New System.IO.StreamReader(My.Application.Info.DirectoryPath & "\SSSDB_Script.txt")
        cmd.CommandText = objReader.ReadToEnd
        cmd.ExecuteNonQuery()
        connection.Close()


        'ConnectionString = "server = 'DESKTOP-PG6NDV8' ;Initial Catalog= 'SSSDB_Temp';User Id=sa;Password=sss;"
        'connection = New SqlConnection(ConnectionString)
        'connection.Open()
        'cmd.CommandType = CommandType.Text
        'cmd.Connection = connection
        'objReader = New System.IO.StreamReader("F:\Temp_pro.sql")
        'cmd.CommandText = objReader.ReadToEnd
        'cmd.ExecuteNonQuery()
        'connection.Close()

        sSql = "CREATE PROCEDURE [dbo].[usp_update_device_conn_status] " & vbCrLf & _
"	-- Add the parameters for the stored procedure here " & vbCrLf & _
"	@dev_id varchar(24), " & vbCrLf & _
"	@dev_name varchar(24), " & vbCrLf & _
"	@tm_last_update datetime, " & vbCrLf & _
"	@fktm_last_update datetime, " & vbCrLf & _
"	@dev_info varchar(256) " & vbCrLf & _
"AS " & vbCrLf & _
"BEGIN " & vbCrLf & _
"	SET NOCOUNT ON; " & vbCrLf & _
"	declare @dev_registered int " & vbCrLf & _
"	if len(@dev_id) < 1  " & vbCrLf & _
"		return -1 " & vbCrLf & _
"	if len(@dev_name) < 1  " & vbCrLf & _
"		return -1 " & vbCrLf & _
"	begin transaction " & vbCrLf & _
"	SELECT @dev_registered = COUNT(device_id) from tbl_fkdevice_status WHERE device_id=@dev_id " & vbCrLf & _
"	if  @dev_registered = 0 " & vbCrLf & _
"	begin " & vbCrLf & _
"		INSERT INTO tbl_fkdevice_status(  " & vbCrLf & _
"				device_id,  " & vbCrLf & _
"				device_name,  " & vbCrLf & _
"				connected,  " & vbCrLf & _
"				last_update_time, " & vbCrLf & _
"				last_update_fk_time,  " & vbCrLf & _
"				device_info) " & vbCrLf & _
"			VALUES( " & vbCrLf & _
"				@dev_id, " & vbCrLf & _
"				@dev_name,  " & vbCrLf & _
"				1, " & vbCrLf & _
"				@tm_last_update, " & vbCrLf & _
"				@fktm_last_update, " & vbCrLf & _
"				@dev_info) " & vbCrLf & _
"	end	 " & vbCrLf & _
"	else -- if @@ROWCOUNT = 0 " & vbCrLf & _
"	begin " & vbCrLf & _
"		UPDATE tbl_fkdevice_status SET  " & vbCrLf & _
"				device_id=@dev_id,  " & vbCrLf & _
"				device_name=@dev_name,  " & vbCrLf & _
"				connected=1, " & vbCrLf & _
"				last_update_time=@tm_last_update, " & vbCrLf & _
"				last_update_fk_time=@fktm_last_update, " & vbCrLf & _
"				device_info=@dev_info " & vbCrLf & _
"			WHERE  " & vbCrLf & _
"				device_id=@dev_id " & vbCrLf & _
"	end " & vbCrLf & _
"	if @@error <> 0 " & vbCrLf & _
"	begin " & vbCrLf & _
"		rollback transaction " & vbCrLf & _
"		return -2 " & vbCrLf & _
"	end " & vbCrLf & _
"	commit transaction " & vbCrLf & _
"	return 0 " & vbCrLf & _
"END "
        'ConnectionString = "server = 'DESKTOP-PG6NDV8' ;Initial Catalog= 'SSSDB_Temp';User Id=sa;Password=sss;"
        'connection = New SqlConnection(ConnectionString)
        connection.Open()
        cmd = New SqlCommand(sSql, connection)
        cmd.ExecuteNonQuery()

        sSql = "CREATE PROCEDURE [dbo].[usp_set_cmd_result] " & vbCrLf & _
"	@dev_id varchar(24), " & vbCrLf & _
"	@trans_id varchar(16), " & vbCrLf & _
"	@return_code varchar(128), " & vbCrLf & _
"	@cmd_result_bin varbinary(max) " & vbCrLf & _
"AS " & vbCrLf & _
"BEGIN " & vbCrLf & _
"	SET NOCOUNT ON; " & vbCrLf & _
"	if @dev_id is null or len(@dev_id) = 0 " & vbCrLf & _
"		return -1 " & vbCrLf & _
"	if @trans_id is null or len(@trans_id) = 0 " & vbCrLf & _
"		return -1 " & vbCrLf & _
"	begin transaction " & vbCrLf & _
"	BEGIN TRY " & vbCrLf & _
"		select trans_id from tbl_fkcmd_trans where trans_id = @trans_id and status='RUN' " & vbCrLf & _
"		if @@ROWCOUNT != 1 " & vbCrLf & _
"		begin " & vbCrLf & _
"			return -2 " & vbCrLf & _
"		end " & vbCrLf & _
"		delete from tbl_fkcmd_trans_cmd_result where trans_id=@trans_id " & vbCrLf & _
"		if len(@cmd_result_bin) > 0 " & vbCrLf & _
"		begin " & vbCrLf & _
"			insert into tbl_fkcmd_trans_cmd_result (trans_id, device_id, cmd_result) values(@trans_id, @dev_id, @cmd_result_bin) " & vbCrLf & _
"		end " & vbCrLf & _
"		update tbl_fkcmd_trans set status='RESULT', return_code=@return_code, update_time = GETDATE() where trans_id=@trans_id and device_id=@dev_id and status='RUN' " & vbCrLf & _
"	END TRY " & vbCrLf & _
"    BEGIN CATCH " & vbCrLf & _
"		rollback transaction " & vbCrLf & _
"		return -3 " & vbCrLf & _
"   END CATCH " & vbCrLf & _
"	 " & vbCrLf & _
"	if @@error <> 0 " & vbCrLf & _
"	begin " & vbCrLf & _
"		rollback transaction " & vbCrLf & _
"		return -3 " & vbCrLf & _
"	end " & vbCrLf & _
"	commit transaction " & vbCrLf & _
"	return 0 " & vbCrLf & _
"END"
        cmd = New SqlCommand(sSql, connection)
        cmd.ExecuteNonQuery()

        sSql = "CREATE PROCEDURE [dbo].[usp_receive_cmd]  " & vbCrLf & _
"	@dev_id varchar(24), " & vbCrLf & _
"	@trans_id varchar(16) output, " & vbCrLf & _
"	@cmd_code varchar(32) output, " & vbCrLf & _
"	@cmd_param_bin varbinary(max) output " & vbCrLf & _
"AS " & vbCrLf & _
"BEGIN " & vbCrLf & _
"	SET NOCOUNT ON; " & vbCrLf & _
"	select @trans_id = '' " & vbCrLf & _
"	if @dev_id is null or len(@dev_id) = 0 " & vbCrLf & _
"		return -1	 " & vbCrLf & _
"	begin transaction " & vbCrLf & _
"	BEGIN TRY " & vbCrLf & _
"		declare @trans_id_tmp as varchar(16) " & vbCrLf & _
"		declare @csrTransId as cursor " & vbCrLf & _
"		set @csrTransId = Cursor For " & vbCrLf & _
"			 select trans_id " & vbCrLf & _
"			 from tbl_fkcmd_trans " & vbCrLf & _
"			 where device_id=@dev_id AND status='RUN' " & vbCrLf & _
"		Open @csrTransId " & vbCrLf & _
"		Fetch Next From @csrTransId	Into @trans_id_tmp " & vbCrLf & _
"		While(@@FETCH_STATUS = 0) " & vbCrLf & _
"		begin " & vbCrLf & _
"			DELETE FROM tbl_fkcmd_trans_cmd_param WHERE trans_id=@trans_id_tmp " & vbCrLf & _
"			DELETE FROM tbl_fkcmd_trans_cmd_result WHERE trans_id=@trans_id_tmp " & vbCrLf & _
"			Fetch Next From @csrTransId	Into @trans_id_tmp " & vbCrLf & _
"		end " & vbCrLf & _
"		close @csrTransId " & vbCrLf & _
"	END TRY " & vbCrLf & _
"    BEGIN CATCH " & vbCrLf & _
"		rollback transaction " & vbCrLf & _
"		select @trans_id='' " & vbCrLf & _
"		return -2 " & vbCrLf & _
"    END CATCH " & vbCrLf & _
"	UPDATE tbl_fkcmd_trans SET status='CANCELLED', update_time = GETDATE() WHERE device_id=@dev_id AND status='RUN' " & vbCrLf & _
"	if @@error <> 0 " & vbCrLf & _
"	begin " & vbCrLf & _
"		rollback transaction " & vbCrLf & _
"		return -2 " & vbCrLf & _
"	end " & vbCrLf & _
"	commit transaction " & vbCrLf & _
"		BEGIN TRY " & vbCrLf & _
"		SELECT @trans_id=trans_id, @cmd_code=cmd_code FROM tbl_fkcmd_trans " & vbCrLf & _
"		WHERE device_id=@dev_id AND status='WAIT' ORDER BY update_time DESC		 " & vbCrLf & _
"		if @@ROWCOUNT = 0 " & vbCrLf & _
"		begin " & vbCrLf & _
"			select @trans_id='' " & vbCrLf & _
"			return -3 " & vbCrLf & _
"		end " & vbCrLf & _
"		select @cmd_param_bin=cmd_param from tbl_fkcmd_trans_cmd_param " & vbCrLf & _
"		where trans_id=@trans_id " & vbCrLf & _
"		UPDATE tbl_fkcmd_trans SET status='RUN', update_time = GETDATE() WHERE trans_id=@trans_id " & vbCrLf & _
"	END TRY " & vbCrLf & _
"    BEGIN CATCH " & vbCrLf & _
"   	select @trans_id='' " & vbCrLf & _
"		return -2 " & vbCrLf & _
"	END CATCH " & vbCrLf & _
"	return 0 " & vbCrLf & _
"END"
        cmd = New SqlCommand(sSql, connection)
        cmd.ExecuteNonQuery()

        sSql = "CREATE PROCEDURE [dbo].[usp_check_reset_fk_cmd] " & vbCrLf & _
"	@dev_id varchar(24), " & vbCrLf & _
"	@trans_id varchar(16) output " & vbCrLf & _
"AS " & vbCrLf & _
"BEGIN " & vbCrLf & _
"	SET NOCOUNT ON; " & vbCrLf & _
"	select @trans_id='' " & vbCrLf & _
"   if @dev_id is null or len(@dev_id) = 0 " & vbCrLf & _
"		return -1 " & vbCrLf & _
"   SELECT @trans_id=trans_id FROM tbl_fkcmd_trans where device_id=@dev_id AND cmd_code='RESET_FK' AND status='WAIT' " & vbCrLf & _
"	if @@ROWCOUNT = 0 " & vbCrLf & _
"		return -2  " & vbCrLf & _
"	begin transaction " & vbCrLf & _
"	BEGIN TRY " & vbCrLf & _
"		declare @trans_id_tmp as varchar(16) " & vbCrLf & _
"		declare @csrTransId as cursor " & vbCrLf & _
"		set @csrTransId = Cursor For " & vbCrLf & _
"			 select trans_id " & vbCrLf & _
"			 from tbl_fkcmd_trans " & vbCrLf & _
"			 where device_id=@dev_id AND status='RUN' " & vbCrLf & _
"		Open @csrTransId " & vbCrLf & _
"		Fetch Next From @csrTransId	Into @trans_id_tmp " & vbCrLf & _
"		While(@@FETCH_STATUS = 0) " & vbCrLf & _
"		begin " & vbCrLf & _
"			DELETE FROM tbl_fkcmd_trans_cmd_param WHERE trans_id=@trans_id_tmp " & vbCrLf & _
"			DELETE FROM tbl_fkcmd_trans_cmd_result WHERE trans_id=@trans_id_tmp " & vbCrLf & _
"			Fetch Next From @csrTransId	Into @trans_id_tmp " & vbCrLf & _
"		end " & vbCrLf & _
"		close @csrTransId " & vbCrLf & _
"		UPDATE tbl_fkcmd_trans SET status='CANCELLED', update_time = GETDATE() WHERE device_id=@dev_id AND status='RUN' " & vbCrLf & _
"		UPDATE tbl_fkcmd_trans SET status='RESULT', update_time = GETDATE() WHERE device_id=@dev_id AND cmd_code='RESET_FK' " & vbCrLf & _
"	END TRY " & vbCrLf & _
"   BEGIN CATCH " & vbCrLf & _
"		rollback transaction " & vbCrLf & _
"		select @trans_id='' " & vbCrLf & _
"		return -2 " & vbCrLf & _
"    END CATCH " & vbCrLf & _
"	commit transaction " & vbCrLf & _
"	return 0 " & vbCrLf & _
"END"
        cmd = New SqlCommand(sSql, connection)
        cmd.ExecuteNonQuery()
        connection.Close()

        Return True
    End Function
    Public Shared Function XtraMsgBox(ByVal fr As UserControl, msg As String, title As String)
        Dim ulf As UserLookAndFeel
        ulf = New UserLookAndFeel(fr)
        ulf.SetSkinStyle("iMaginary")
        XtraMessageBox.AllowCustomLookAndFeel = True
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.Show(ulf, "<size=10>" & msg & "</size>", "<size=9>" & title & "</size>", DefaultBoolean.Default)
    End Function
    Public Shared Function IsValidIPAddress(ByVal strIPAddress As String) As Boolean
        On Error GoTo Handler
        Dim varAddress As Object, n As Long, lCount As Long
        varAddress = Split(strIPAddress, ".", , vbTextCompare)
        '//
        If IsArray(varAddress) Then
            For n = LBound(varAddress) To UBound(varAddress)
                lCount = lCount + 1
                varAddress(n) = CByte(varAddress(n))
            Next
            '//
            IsValidIPAddress = (lCount = 4)
        End If
        '//
Handler:
    End Function
    Public Shared Function pdfreport(mstrFile_Name As String, fontsize As Integer, ulf As UserLookAndFeel)
        Try
            Dim line As String
            'Dim readFile As System.IO.TextReader = New StreamReader("Text.txt")
            Dim readFile As System.IO.TextReader = New StreamReader(mstrFile_Name)
            Dim yPoint As Integer = 0
            Dim pdf As PdfDocument = New PdfDocument
            pdf.Info.Title = "Text File to PDF"
            Dim pdfPage As PdfPage = pdf.AddPage
            Dim graph As XGraphics = XGraphics.FromPdfPage(pdfPage)
            Dim font As XFont = New XFont("Consolas", fontsize, XFontStyle.Regular)
            While True
                line = readFile.ReadLine()
                If line Is Nothing Then
                    Exit While
                Else
                    graph.DrawString(line, font, XBrushes.Black, New XRect(20, yPoint, pdfPage.Width.Point, pdfPage.Height.Point), XStringFormat.TopLeft)
                    yPoint = yPoint + 10
                    '--------------------------- here is the part added ----------------------------------
                    If yPoint >= 750 Then '   If yPoint = 840 Then
                        pdfPage = pdf.AddPage
                        graph = XGraphics.FromPdfPage(pdfPage)
                        yPoint = 10
                    End If
                    '-------------------------------------------------------------------------------------
                End If
            End While
            Dim pdfFilename As String = mstrFile_Name & ".pdf" '"txttopdf.pdf"
            pdf.Save(pdfFilename)
            readFile.Close()
            readFile = Nothing

            'to add image
            Try
                Dim document As PdfDocument = PdfReader.Open(pdfFilename)
                Dim page As PdfPage = document.Pages(0)
                Dim gfx As XGraphics = XGraphics.FromPdfPage(page)

                DrawImage(gfx, My.Application.Info.DirectoryPath & "\TimeWatch_Logo_pdf.png", 0, 0, 100, 25)
                document.Save(pdfFilename)
            Catch ex As Exception

            End Try
            Process.Start(pdfFilename)
            'to add image

            'System.Diagnostics.Process.Start(pdfFilename)
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.ToString & "</size>", "<size=9>Error</size>")
        End Try
    End Function
    Private Shared Sub DrawImage(ByVal gfx As XGraphics, ByVal jpegSamplePath As String, ByVal x As Integer, ByVal y As Integer, ByVal width As Integer, ByVal height As Integer)
        Dim image As XImage = XImage.FromFile(jpegSamplePath)
        gfx.DrawImage(image, x, y, width, height)
    End Sub
    Public Shared Function ArrangeFormula(ByVal mFormula As String, ulf As UserLookAndFeel) As String
        Dim m As Integer
        Dim tempfor1 As String = mFormula
        Dim tempfor As String = ""
        m = 1
        For m = 1 To Len(Trim(tempfor1))
            If Mid(Trim(tempfor1), m, 1) <> " " Then
                tempfor = tempfor & Mid(Trim(tempfor1), m, 1)
            End If
        Next
        If Mid(Trim(tempfor1), 1, 1) = "(" Or Mid(Trim(tempfor1), 1, 1) = ")" Or Mid(Trim(tempfor1), 1, 1) = "+" Or Mid(Trim(tempfor1), 1, 1) = "-" Or Mid(Trim(tempfor1), 1, 1) = "/" Or Mid(Trim(tempfor1), 1, 1) = "*" Or Mid(Trim(tempfor1), 1, 1) = ">" Or Mid(Trim(tempfor1), 1, 1) = "<" Or Mid(Trim(tempfor1), 1, 1) = "=" Or Mid(Trim(tempfor1), 1, 1) = "I" Or Mid(Trim(tempfor1), 1, 1) = "i" Then
            tempfor1 = Trim(tempfor)
        Else
            tempfor1 = "(" & Trim(tempfor) & ")"
        End If
        tempfor = ""
        m = 1
        For m = 1 To Len(Trim(tempfor1))
            If Mid(Trim(tempfor1), m, 1) = "," Then
                If Mid(Trim(tempfor1), m + 1, 1) <> "[" Then
                    If Mid(Trim(tempfor1), m + 1, 1) <> "#" Then
                        XtraMessageBox.Show(ulf, "<size=10>Formula not correct, Please use [] or ## </size>", "<size=9>Error</size>")
                        tempfor = ""
                        Exit Function
                    ElseIf Mid(Trim(tempfor1), m + 1, 1) = "#" And Mid(Trim(tempfor1), m + 3, 1) <> "#" Then
                        XtraMessageBox.Show(ulf, "<size=10>Formula not correct, use ## as #A# where A->Formula</size>", "<size=9>Error</size>")
                        tempfor = ""
                        Exit Function
                    End If
                End If
            End If
            If Mid(Trim(tempfor1), m, 1) = "#" Then
                If Mid(Trim(tempfor1), m + 2, 1) <> "#" Then
                    XtraMessageBox.Show(ulf, "<size=10>Formula not correct, use ## as #A# where A->Formula </size>", "<size=9>Error</size>")
                    tempfor = ""
                    Exit Function
                Else
                    m = m + 2
                End If
            End If
        Next

        tempfor = ""
        m = 1

        For m = 1 To Len(Trim(tempfor1))
            If Mid(Trim(tempfor1), m, 1) = "(" Or Mid(Trim(tempfor1), m, 1) = ")" Or Mid(Trim(tempfor1), m, 1) = "+" Or Mid(Trim(tempfor1), m, 1) = "-" Or Mid(Trim(tempfor1), m, 1) = "/" Or Mid(Trim(tempfor1), m, 1) = "*" Or Mid(Trim(tempfor1), m, 1) = ">" Or Mid(Trim(tempfor1), m, 1) = "<" Or Mid(Trim(tempfor1), m, 1) = "=" Then
                If Mid(Trim(tempfor1), m, 1) = ">" Or Mid(Trim(tempfor1), m, 1) = "<" Then
                    m = m + 1
                    If Mid(Trim(tempfor1), m, 1) = "=" Then
                        tempfor = tempfor & " " & Mid(Trim(tempfor1), m - 1, 2) & " "
                    Else
                        m = m - 1
                        tempfor = tempfor & " " & Mid(Trim(tempfor1), m, 1) & " "
                    End If
                ElseIf Mid(Trim(tempfor1), m, 1) = "(" Then
                    m = m + 1
                    If Mid(Trim(tempfor1), m, 1) = "(" Then
                        m = m - 1
                        tempfor = tempfor & " " & Mid(Trim(tempfor1), m, 1)
                    Else
                        m = m - 1
                        tempfor = tempfor & " " & Mid(Trim(tempfor1), m, 1) & " "
                    End If
                ElseIf Mid(Trim(tempfor1), m, 1) = ")" Then
                    m = m + 1
                    If Mid(Trim(tempfor1), m, 1) = "*" Or Mid(Trim(tempfor1), m, 1) = "/" Or Mid(Trim(tempfor1), m, 1) = "+" Or Mid(Trim(tempfor1), m, 1) = "-" Then
                        m = m + 1
                        If Mid(Trim(tempfor1), m, 1) = "(" Then
                            tempfor = tempfor & " " & Mid(Trim(tempfor1), m - 2, 3) & " "
                        Else
                            m = m - 1
                            tempfor = tempfor & " " & Mid(Trim(tempfor1), m - 1, 2) & " "
                        End If
                    Else
                        m = m - 1
                        tempfor = tempfor & " " & Mid(Trim(tempfor1), m, 1) & " "
                    End If
                Else
                    tempfor = tempfor & " " & Mid(Trim(tempfor1), m, 1) & " "
                End If
            Else
                If Mid(Trim(tempfor1), m, 1) = "," Or Mid(Trim(tempfor1), m, 1) = "]" Or Mid(Trim(tempfor1), m, 1) = "[" Then
                    tempfor = tempfor & " " & Mid(Trim(tempfor1), m, 1) & " "
                Else
                    tempfor = tempfor & Mid(Trim(tempfor1), m, 1)
                End If
            End If
        Next
        Return tempfor
    End Function
    'get serial no from db
    'Public Shared Function getSerialNo()
    '    Dim iASno As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\iASno.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
    '    Dim iASnoCon As OleDbConnection = New OleDbConnection(iASno)


    '    Dim x As String = "Select * from SerialNo"
    '    Dim adapNo As OleDbDataAdapter = New OleDbDataAdapter(x, iASnoCon)
    '    Dim dsNo As DataSet = New DataSet
    '    adapNo.Fill(dsNo)

    '    If dsNo.Tables(0).Rows(0).Item("SerialNo").ToString.Trim.Length < 4 Then
    '        For i As Integer = 0 To dsNo.Tables(0).Rows.Count - 1
    '            seriallist.Add(dsNo.Tables(0).Rows(i).Item("SerialNo").ToString.Trim)
    '        Next
    '        x = "delete from SerialNo"
    '        iASnoCon.Open()
    '        Dim cmdiAS As OleDbCommand = New OleDbCommand(x, iASnoCon)
    '        cmdiAS.ExecuteNonQuery()
    '        For i As Integer = 0 To dsNo.Tables(0).Rows.Count - 1
    '            x = "Insert into SerialNo (SerialNo) values ('" & EncyDcry.Encrypt(seriallist(i).Trim, True) & "')"
    '            cmdiAS = New OleDbCommand(x, iASnoCon)
    '            cmdiAS.ExecuteNonQuery()
    '        Next
    '        x = "Insert into SerialNo (SerialNo) values ('TIP')"
    '        cmdiAS = New OleDbCommand(x, iASnoCon)
    '        cmdiAS.ExecuteNonQuery()
    '        iASnoCon.Close()
    '        x = "Select * from SerialNo"
    '        adapNo = New OleDbDataAdapter(x, iASnoCon)
    '        dsNo = New DataSet
    '        adapNo.Fill(dsNo)
    '    End If
    '    seriallist.Clear()
    '    For i As Integer = 0 To dsNo.Tables(0).Rows.Count - 1
    '        Try
    '            seriallist.Add(EncyDcry.Decrypt(dsNo.Tables(0).Rows(i).Item("SerialNo").ToString.Trim, True))
    '        Catch ex As Exception
    '        End Try
    '    Next
    '    SerialNo = seriallist.Distinct.ToArray
    'End Function

    Public Shared Sub SetGridFont(ByVal View As GridView, ByVal myFont As Font)
        For Each ap As AppearanceObject In View.Appearance
            ap.Font = myFont
        Next
        DevExpress.XtraGrid.Localization.GridLocalizer.Active = New MyLocalizer 'for e.error text for grid validation
    End Sub
    Public Shared Sub SetFont(ByVal control As Control, ByVal fontsize As Integer)
        For Each childControl As Control In control.Controls
            childControl.Font = New Font("Tahoma", fontsize, FontStyle.Regular)
            If childControl.Controls.Count > 0 Then
                SetFont(childControl, fontsize)
            End If
        Next childControl
    End Sub
    Public Sub DutyRoster(ByVal fCode As String, ByVal Cur_Year As DateTime, ByVal WI As String)
        XtraMasterTest.LabelControlStatus.Text = "Creating roster for Paycode " & fCode
        Application.DoEvents()

        Dim sSql As String = Nothing
        Dim RsCrEmp As DataSet = New DataSet()
        Dim RsEmpTemp As DataSet = New DataSet()
        Dim Pos As Integer = 0
        Dim mwDay As String = Nothing
        Dim mDate As DateTime = DateTime.MinValue
        Dim mDate1 As String = Nothing
        Dim mDt As DateTime = DateTime.MinValue
        Dim mRdays As Integer = 0
        Dim mLShift As String = Nothing
        Dim mPat As String = Nothing
        Dim mPatM As String = Nothing
        Dim mShift As String = Nothing
        Dim mShiftM As String = Nothing
        Dim mCnt As Integer = 0
        Dim mAbsentVal As Integer = 0
        Dim mWoVal As Integer = 0
        Dim mSTAT As String = Nothing
        Dim mSatCtr As Integer = 0
        Dim lastdate As String = ""
        Dim dtmCalDate As DateTime = DateTime.MinValue
        Dim rsCal As DataSet = New DataSet()
        Dim strsql As String = Nothing
        'strsql = "select * from tblcalander where DatePart(yy,mdate) = " & Cur_Year.Year
        rsCal = New DataSet
        If servername = "Access" Then
            strsql = "select * from tblcalander where Year(mdate) = " & Cur_Year.Year
            adapA = New OleDbDataAdapter(strsql, con1Access)
            adapA.Fill(rsCal)
        Else
            strsql = "select * from tblcalander where DatePart(yy,mdate) = " & Cur_Year.Year
            adap = New SqlDataAdapter(strsql, conSQL)
            adap.Fill(rsCal)
        End If
       
        If servername = "Access" Then
            If con1Access.State <> ConnectionState.Open Then
                con1Access.Open()
            End If
        Else
            If conSQL.State <> ConnectionState.Open Then
                conSQL.Open()
            End If
        End If
        If rsCal.Tables(0).Rows.Count < 2 Then
            dtmCalDate = Microsoft.VisualBasic.DateAndTime.DateSerial(Convert.ToInt32(Cur_Year.Year), 1, 1)
            While dtmCalDate.Year = Convert.ToInt32(Cur_Year.Year)
                strsql = "Insert into tblCalander(mDate,PROCess, NRTCPROC) Values('" & dtmCalDate.ToString("yyyy-MM-dd") & "','X','N')"
                dtmCalDate = dtmCalDate.AddDays(1)
                Try
                    If servername = "Access" Then
                        cmd1 = New OleDbCommand(strsql, con1Access)
                        cmd1.ExecuteNonQuery()
                    Else
                        cmd = New SqlCommand(strsql, conSQL)
                        cmd.ExecuteNonQuery()
                    End If
                Catch ex As Exception
                    Continue While
                End Try
                'Cn.Execute(strsql)
            End While          
        End If
       
        mSTAT = ""
        mAbsentVal = 0
        mWoVal = 0
        mSatCtr = 0
        Dim doj As DateTime = System.DateTime.MinValue
        sSql = "Select a.*,Convert(varchar(10),b.DateOfJoin,126) 'doj'  from tblemployeeshiftmaster a,tblemployee b " & "Where a.Paycode=b.Paycode And b.Active='Y' And  a.Paycode ='" & fCode & "'"
        RsCrEmp = New DataSet
        If servername = "Access" Then
            adapA1 = New OleDbDataAdapter("Select a.*,b.DateOfJoin as doj from tblemployeeshiftmaster a,tblemployee b " & _
               "Where a.Paycode=b.Paycode And b.Active='Y' And  a.Paycode ='" & fCode & "'", con1Access)          
            adapA1.Fill(RsCrEmp)
        Else
            adap1 = New SqlDataAdapter(sSql, conSQL)
            adap1.Fill(RsCrEmp)
        End If
       
        If RsCrEmp.Tables(0).Rows.Count > 0 Then
             doj = Convert.ToDateTime(RsCrEmp.Tables(0).Rows(0)("doj").ToString().Trim)
            If Cur_Year < doj Then
                mDate = doj
            Else
                mDate = Cur_Year
            End If

            mDate = Cur_Year
            mDate1 = Cur_Year.Year.ToString()
            lastdate = mDate1.ToString() & "-12-31"
            sSql = "Select * From tbltimeregister Where Paycode='" & RsCrEmp.Tables(0).Rows(0)("paycode").ToString() & "' And" & " DateOffice='" & lastdate.ToString() & "'"
            RsEmpTemp = New DataSet
            If servername = "Access" Then
                sSql = "Select * From tbltimeregister Where Paycode='" & RsCrEmp.Tables(0).Rows(0)("paycode").ToString() & "' And  FORMAT(DateOffice,'MMM DD YYYY')='" & Format("31/12/" & mDate1, "MMM DD yyyy") & "'"
                adapA2 = New OleDbDataAdapter(sSql, con1Access)
                adapA2.Fill(RsEmpTemp)
            Else
                adap2 = New SqlDataAdapter(sSql, conSQL)
                adap2.Fill(RsEmpTemp)
            End If
            If RsEmpTemp.Tables(0).Rows.Count > 0 Then
                mCnt = 1
            Else
                mCnt = 0
            End If

            If mCnt = 0 Then
tmp:            mRdays = 7
                mLShift = RsCrEmp.Tables(0).Rows(0)("Shift").ToString()
                mShiftM = RsCrEmp.Tables(0).Rows(0)("MShift").ToString()
                If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "R" Then
                    mRdays = Convert.ToInt32(RsCrEmp.Tables(0).Rows(0)("ShiftRemainDays"))
                    mPat = RsCrEmp.Tables(0).Rows(0)("SHIFTPATTERN").ToString()
                ElseIf RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "F" Then
                    mPat = RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," & RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," & RsCrEmp.Tables(0).Rows(0)("Shift").ToString()
                ElseIf RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                    mPat = RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," & RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," & RsCrEmp.Tables(0).Rows(0)("Shift").ToString()
                    mPatM = RsCrEmp.Tables(0).Rows(0)("MShift").ToString() & "," & RsCrEmp.Tables(0).Rows(0)("MShift").ToString() & "," & RsCrEmp.Tables(0).Rows(0)("MShift").ToString()
                ElseIf RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "L" Then
                    mPat = "FLX,FLX,FLX"
                    mLShift = "FLX"
                Else
                    mPat = "IGN,IGN,IGN"
                    mLShift = "IGN"
                End If
                Dim TempArray() As String = mPat.Trim.Split(",")
                Dim TempArrayM() As String
                If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                    TempArrayM = mPatM.Trim.Split(",")
                End If
                Dim i As Integer = 0
                Pos = (mPat.IndexOf(mLShift, 0) + 1)
                If Pos <= 0 Then
                    Pos = 1
                    i = 0
                End If
                'MsgBox("before while")
                mDt = mDate
                'Dim x As Integer = -1               

                While mDt.ToString("yyyy") = mDate.ToString("yyyy")
                    'x = x + 1
                    While mRdays > 0 AndAlso mDt.ToString("yyyy") = mDate.ToString("yyyy")
                        XtraMasterTest.LabelControlStatus.Text = "Creating roster for Paycode " & fCode & " " & mDt
                        Application.DoEvents()
                        If mDt.ToString("dd") = "01" Then
                            mSatCtr = 0
                        End If

                        mwDay = mDt.ToString("dddd").ToUpper().Substring(0, 3)

                        If mwDay = RsCrEmp.Tables(0).Rows(0)("FIRSTOFFDAY").ToString().Trim() Then
                            mShift = "OFF"
                            mShiftM = "OFF"
                            If WI <> "Y" Then
                                mRdays = mRdays + 1
                            End If
                        ElseIf mwDay = RsCrEmp.Tables(0).Rows(0)("SECONDOFFDAY").ToString().Trim() Then
                            mSatCtr = mSatCtr + 1
                            If (RsCrEmp.Tables(0).Rows(0)("ALTERNATE_OFF_DAYS").ToString().IndexOf(mSatCtr.ToString().Trim(), 0) + 1) > 0 Then
                                If RsCrEmp.Tables(0).Rows(0)("SECONDOFFTYPE").ToString().Trim() = "H" Then
                                    mShift = RsCrEmp.Tables(0).Rows(0)("HalfDayShift").ToString()
                                    If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                                        mShiftM = TempArrayM(i)
                                    End If
                                Else
                                    mShift = "OFF"
                                    mShiftM = "OFF"
                                End If

                                If WI <> "Y" Then
                                    mRdays = mRdays + 1
                                End If
                            Else
                                'mShift = mPat.Substring(Pos - 1, 3)
                                mShift = TempArray(i)
                                If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                                    mShiftM = TempArrayM(i)
                                End If
                            End If
                        Else
                            'mShift = mPat.Substring(Pos - 1, 3)
                            mShift = TempArray(i)
                            If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                                mShiftM = TempArrayM(i)
                            End If
                        End If

                        If mShift = "OFF" Then
                            mSTAT = "WO"
                            mWoVal = 1
                            mAbsentVal = 0
                        Else
                            mSTAT = "A"
                            mAbsentVal = 1
                            mWoVal = 0
                        End If

                        If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                            sSql = "Insert Into TBLTIMEREGISTER (Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue, MShift,MShiftAttended,MAbsentValue,Mpresentvalue, MStatus) Values('" & RsCrEmp.Tables(0).Rows(0)("paycode").ToString & "','" & mDt.ToString("yyyy-MM-dd") & "','" & mShift & "','" & mShift & "','" & mSTAT & "'," & mWoVal & "," & mAbsentVal & ",0, '" & mShiftM & "','" & mShiftM & "'," & mAbsentVal & ",0,'" & mSTAT & "')"
                        Else
                            sSql = "Insert Into TBLTIMEREGISTER (Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue,presentvalue) Values('" & RsCrEmp.Tables(0).Rows(0)("paycode").ToString & "','" & mDt.ToString("yyyy-MM-dd") & "','" & mShift & "','" & mShift & "','" & mSTAT & "'," & mWoVal & "," & mAbsentVal & ",0)"
                        End If
                        'MsgBox(sSql)
                        If servername = "Access" Then
                            Try
                                cmd1 = New OleDbCommand(sSql, con1Access)
                                cmd1.ExecuteNonQuery()
                            Catch
                            End Try
                        Else
                            Try
                                cmd = New SqlCommand(sSql, conSQL)
                                cmd.ExecuteNonQuery()
                            Catch ex As Exception
                            End Try
                            'con.Close()
                        End If
                        'Cn.Execute(sSql)
                        mRdays = mRdays - 1
                        mDt = mDt.AddDays(1)
                        ' i = i + 1
                    End While
                    'MsgBox("insert TBLTIMEREGISTER")
                    mRdays = Convert.ToInt32(RsCrEmp.Tables(0).Rows(0)("CDays"))
                    If mRdays <= 0 Then
                        mRdays = 7
                    End If

                    Pos = Pos + 4
                    i = i + 1
                    'If Pos > mPat.Length Then
                    '    Pos = 1
                    '    i = 0
                    'End If
                    If i > TempArray.Length - 1 Then ' Added By Nitin
                        'If mPat.Substring(Pos - 1, 3) = "   " Then
                        'Pos = 1
                        i = 0
                        'End If
                    End If
                End While
            Else
                'in case of change of date of joining of any employee
                sSql = "Select * From tbltimeregister Where Paycode='" & RsCrEmp.Tables(0).Rows(0)("paycode").ToString() & "' And" & " DateOffice='" & doj.ToString("yyyy-MM-dd") & "'"
                RsEmpTemp = New DataSet
                If servername = "Access" Then
                    sSql = "Select * From tbltimeregister Where Paycode='" & RsCrEmp.Tables(0).Rows(0)("paycode").ToString() & "' And  FORMAT(DateOffice,'yyyy-MM-dd')='" & doj.ToString("yyyy-MM-dd") & "'"
                    adapA2 = New OleDbDataAdapter(sSql, con1Access)
                    adapA2.Fill(RsEmpTemp)
                Else
                    adap2 = New SqlDataAdapter(sSql, conSQL)
                    adap2.Fill(RsEmpTemp)
                End If
                If RsEmpTemp.Tables(0).Rows.Count = 0 Then
                    GoTo tmp
                End If
            End If

            'Dim gLDuplicate As String = gLDuplicate
            If gLDuplicate.ToString().Trim().ToUpper() = "Y" Then
                mDate = Cur_Year
                mDate1 = Cur_Year.Year.ToString()
                lastdate = mDate1.ToString() & "-12-31"
                sSql = "Select * From tbltimeregisterD Where Paycode='" & RsCrEmp.Tables(0).Rows(0)("paycode").ToString.Trim & "' And" & " DateOffice='" & lastdate & "'"
                Dim adap4 As SqlDataAdapter
                Dim adapA4 As OleDbDataAdapter
                RsEmpTemp = New DataSet
                If servername = "Access" Then
                    adapA4 = New OleDbDataAdapter(sSql, con1Access)
                    adapA4.Fill(RsEmpTemp)
                Else
                    adap4 = New SqlDataAdapter(sSql, conSQL)
                    adap4.Fill(RsEmpTemp)
                End If
                'RsEmpTemp = Cn.FillDataSet(sSql)
                If RsEmpTemp.Tables(0).Rows.Count >= 1 Then
                    mCnt = 0
                Else
                    mCnt = 0
                End If

                If mCnt <= 0 Then
                    mRdays = 7
                    mLShift = RsCrEmp.Tables(0).Rows(0)("Shift").ToString().Trim()
                    If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "R" Then
                        mRdays = Convert.ToInt32(RsCrEmp.Tables(0).Rows(0)("ShiftRemainDays"))
                        mPat = RsCrEmp.Tables(0).Rows(0)("SHIFTPATTERN").ToString().Trim()
                    ElseIf RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "F" Then
                        mPat = RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," + RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," + RsCrEmp.Tables(0).Rows(0)("Shift").ToString()
                    ElseIf RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                        mPat = RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," + RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," + RsCrEmp.Tables(0).Rows(0)("Shift").ToString()
                        mShiftM = RsCrEmp.Tables(0).Rows(0)("MShift").ToString()
                    ElseIf RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "L" Then
                        mPat = "FLX,FLX,FLX"
                        mLShift = "FLX"
                    Else
                        mPat = "IGN,IGN,IGN"
                        mLShift = "IGN"
                    End If

                    Pos = (mPat.IndexOf(mLShift, 0) + 1)
                    If Pos <= 0 Then
                        Pos = 1
                    End If

                    mDt = mDate
                    While mDt.ToString("yyyy") = mDate.ToString("yyyy")
                        While mRdays > 0 AndAlso mDt.ToString("yyyy") = mDate.ToString("yyyy")
                            If mDt.ToString("dd") = "01" Then
                                mSatCtr = 0
                            End If

                            mwDay = mDt.ToString("dddd").ToUpper().Substring(0, 3)
                            If mwDay = RsCrEmp.Tables(0).Rows(0)("FIRSTOFFDAY").ToString().Trim() Then
                                mShift = "OFF"
                                If WI <> "Y" Then
                                    mRdays = mRdays + 1
                                End If
                            ElseIf mwDay = RsCrEmp.Tables(0).Rows(0)("SECONDOFFDAY").ToString().Trim() Then
                                mSatCtr = mSatCtr + 1
                                mSatCtr = Math.Truncate((mDt.Day + (CInt(mDt.DayOfWeek))) / 7) '+ 1   'nitin
                                If (RsCrEmp.Tables(0).Rows(0)("ALTERNATE_OFF_DAYS").ToString().Trim().IndexOf(mSatCtr.ToString(), 0) + 1) > 0 Then
                                    If RsCrEmp.Tables(0).Rows(0)("SECONDOFFTYPE").ToString().Trim() = "H" Then
                                        mShift = RsCrEmp.Tables(0).Rows(0)("HalfDayShift").ToString().Trim()
                                    Else
                                        mShift = "OFF"
                                    End If

                                    If WI <> "Y" Then
                                        mRdays = mRdays + 1
                                    End If
                                Else
                                    mShift = mPat.Substring(Pos - 1, 3)
                                End If
                            Else
                                mShift = mPat.Substring(Pos - 1, 3)
                            End If

                            If mShift = "OFF" Then
                                mSTAT = "WO"
                                mWoVal = 1
                                mAbsentVal = 0
                            Else
                                mSTAT = "A"
                                mAbsentVal = 1
                                mWoVal = 0
                            End If

                            sSql = "Insert Into TBLTIMEREGISTERD(Paycode,DateOffice,Shift,ShiftAttended,Status,WO_Value,AbsentValue) Values('" & RsCrEmp.Tables(0).Rows(0)("paycode").ToString.Trim & "','" & mDt.ToString("yyyy-MM-dd 00:00:00") & "','" & mShift & "','" & mShift & " ','" & mSTAT & "'," & mWoVal & "," & mAbsentVal & ")"
                            If servername = "Access" Then
                                Try
                                    cmd1 = New OleDbCommand(sSql, con1Access)
                                    cmd1.ExecuteNonQuery()
                                Catch
                                End Try
                            Else
                                Try
                                    cmd = New SqlCommand(sSql, conSQL)
                                    cmd.ExecuteNonQuery()
                                Catch ex As Exception
                                End Try
                                'con.Close()
                            End If
                            'Cn.Execute(sSql)
                            mRdays = mRdays - 1
                            mDt = Convert.ToDateTime(mDt.ToString("dd/MM/yyyy")).AddDays(1)
                        End While

                        mRdays = Convert.ToInt32(RsCrEmp.Tables(0).Rows(0)("CDays"))
                        If mRdays <= 0 Then
                            mRdays = 7
                        End If

                        Pos = Pos + 4
                        If Pos > mPat.Length Then
                            Pos = 1
                        End If

                        If mPat.Substring(Pos - 1, 3) = "   " Then
                            Pos = 1
                        End If
                    End While
                End If
            End If
        End If
        If servername = "Access" Then
            If con1Access.State <> ConnectionState.Closed Then
                con1Access.Close()
            End If
        Else
            If conSQL.State <> ConnectionState.Closed Then
                conSQL.Close()
            End If
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub
    Public Sub DutyRosterUpd(ByVal fCode As String, ByVal Cur_Year As DateTime, ByVal WI As String)
        XtraMasterTest.LabelControlStatus.Text = "Updating roster for paycode " & fCode
        Application.DoEvents()

        Dim sSql As String = Nothing
        Dim RsCrEmp As DataSet = New DataSet()
        Dim RsEmpTemp As DataSet = New DataSet()
        Dim Pos As Integer = 0
        Dim mwDay As String = Nothing
        Dim mDate As DateTime = DateTime.MinValue
        Dim mDate1 As String = Nothing
        Dim mDt As DateTime = DateTime.MinValue
        Dim mRdays As Integer = 0
        Dim mLShift As String = Nothing
        Dim mPat As String = Nothing
        Dim mLShiftM As String = Nothing
        Dim mPatM As String = Nothing
        Dim mShift As String = Nothing
        Dim mShiftM As String = Nothing
        Dim mCnt As Integer = 0
        Dim mAbsentVal As Integer = 0
        Dim mWoVal As Integer = 0
        Dim mSTAT As String = Nothing
        Dim mSatCtr As Integer = 0
        Dim lastdate As String = ""
        Dim dtmCalDate As DateTime = DateTime.MinValue
        Dim rsCal As DataSet = New DataSet()
        Dim strsql As String = Nothing
        strsql = "select * from tblcalander where DatePart(yy,mdate) = " & Cur_Year.Year
        rsCal = New DataSet
        If servername = "Access" Then
            strsql = "select * from tblcalander where Year(mdate) = " & Year(Cur_Year) & " "
            adapA = New OleDbDataAdapter(strsql, con1Access)
            adapA.Fill(rsCal)
        Else
            adap = New SqlDataAdapter(strsql, conSQL)
            adap.Fill(rsCal)
        End If
        'rsCal = Cn.FillDataSet(strsql)
        If servername = "Access" Then
            If con1Access.State <> ConnectionState.Open Then
                con1Access.Open()
            End If
        Else
            If conSQL.State <> ConnectionState.Open Then
                conSQL.Open()
            End If
        End If

        If rsCal.Tables(0).Rows.Count < 2 Then
            dtmCalDate = Microsoft.VisualBasic.DateAndTime.DateSerial(Convert.ToInt32(Cur_Year.Year), 1, 1)
            While dtmCalDate.Year = Convert.ToInt32(Cur_Year.Year)
                strsql = "Insert into tblCalander(mDate, PROCess, NRTCPROC) Values('" & dtmCalDate.ToString("yyyy-MM-dd") & "','X','N')"
                dtmCalDate = dtmCalDate.AddDays(1)
                If servername = "Access" Then
                    cmd1 = New OleDbCommand(strsql, con1Access)
                    cmd1.ExecuteNonQuery()                    
                Else
                    cmd = New SqlCommand(strsql, conSQL)
                    cmd.ExecuteNonQuery()                    
                End If
                'Cn.Execute(strsql)
            End While
        Else
            strsql = "Update  tblCalander set  PROCess='X',  NRTCPROC='N'"
            dtmCalDate = dtmCalDate.AddDays(1)
            If servername = "Access" Then
                cmd1 = New OleDbCommand(strsql, con1Access)
                cmd1.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(strsql, conSQL)
                cmd.ExecuteNonQuery()
            End If
        End If

        mSTAT = ""
        mAbsentVal = 0
        mWoVal = 0
        mSatCtr = 0
        Dim doj As DateTime = System.DateTime.MinValue
        sSql = "Select a.*,Convert(varchar(10),b.DateOfJoin,126) 'doj'  from tblemployeeshiftmaster a,tblemployee b Where a.Paycode=b.Paycode And b.Active='Y' And  a.Paycode ='" & fCode & "'"
        RsCrEmp = New DataSet
        If servername = "Access" Then
            adapA1 = New OleDbDataAdapter("Select a.*,b.DateOfJoin as doj from tblemployeeshiftmaster a,tblemployee b " & _
               "Where a.Paycode=b.Paycode And b.Active='Y' And  a.Paycode ='" & fCode & "'", con1Access)
            adapA1.Fill(RsCrEmp)
        Else
            adap1 = New SqlDataAdapter(sSql, conSQL)
            adap1.Fill(RsCrEmp)
        End If
        If RsCrEmp.Tables(0).Rows.Count > 0 Then
            doj = Convert.ToDateTime(RsCrEmp.Tables(0).Rows(0)("doj").ToString().Trim)
            If Cur_Year < doj Then
                mDate = doj
            Else
                mDate = Cur_Year
            End If
        End If

        mDate = Cur_Year
        mDate1 = Cur_Year.Year.ToString()
        lastdate = mDate1.ToString() & "-12-31"
        Dim lastdate1 As DateTime = Convert.ToDateTime(lastdate)
        sSql = "Select * From tbltimeregister Where Paycode='" & RsCrEmp.Tables(0).Rows(0)("paycode").ToString().Trim() & "' And DateOffice='" & lastdate & "'"
         RsEmpTemp = New DataSet
        If servername = "Access" Then
            adapA2 = New OleDbDataAdapter("Select * From tbltimeregister Where PAYCODE='" & RsCrEmp.Tables(0).Rows(0).Item("PAYCODE") & "' And" & _
            " FORMAT(DateOffice,'MMM DD YYYY')='" & Format("31/12/" & mDate1, "MMM DD yyyy") & "'", con1Access)
            adapA2.Fill(RsEmpTemp)
        Else
            adap2 = New SqlDataAdapter(sSql, conSQL)
            adap2.Fill(RsEmpTemp)
        End If
        If RsEmpTemp.Tables(0).Rows.Count > 0 Then
            mCnt = 0
        Else
            mCnt = 0
        End If

        If mCnt = 0 Then
            mRdays = 7
            mLShift = RsCrEmp.Tables(0).Rows(0)("Shift").ToString()
            mLShiftM = RsCrEmp.Tables(0).Rows(0)("MShift").ToString()
            If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "R" Then
                mRdays = Convert.ToInt32(RsCrEmp.Tables(0).Rows(0)("ShiftRemainDays"))
                mPat = RsCrEmp.Tables(0).Rows(0)("SHIFTPATTERN").ToString()
            ElseIf RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "F" Then
                mPat = RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," & RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," & RsCrEmp.Tables(0).Rows(0)("Shift").ToString()
            ElseIf RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                mPat = RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," & RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," & RsCrEmp.Tables(0).Rows(0)("Shift").ToString()
                mPatM = RsCrEmp.Tables(0).Rows(0)("MShift").ToString() & "," & RsCrEmp.Tables(0).Rows(0)("MShift").ToString() & "," & RsCrEmp.Tables(0).Rows(0)("MShift").ToString()
            ElseIf RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "L" Then
                mPat = "FLX,FLX,FLX"
                mLShift = "FLX"
            Else
                mPat = "IGN,IGN,IGN"
                mLShift = "IGN"
            End If
            Dim TempArray() As String = mPat.Trim.Split(",") ' Added By Nitin
            Dim TempArrayM() As String
            If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                TempArrayM = mPatM.Trim.Split(",")
            End If
            Dim i As Integer = 0 ' Added By Nitin
            Pos = (mPat.IndexOf(mLShift, 0) + 1)
            If Pos <= 0 Then
                Pos = 1
                i = 0 ' Added By Nitin
            End If

            mDt = mDate
            While mDt.ToString("yyyy") = mDate.ToString("yyyy")
                While mRdays > 0 AndAlso mDt.ToString("yyyy") = mDate.ToString("yyyy")
                    XtraMasterTest.LabelControlStatus.Text = "Updating roster for paycode " & fCode & " " & mDt
                    Application.DoEvents()
                    If mDt.ToString("dd") = "01" Then
                        mSatCtr = 0
                    End If
                    mwDay = mDt.ToString("dddd").ToUpper().Substring(0, 3)
                    If mwDay = RsCrEmp.Tables(0).Rows(0)("FIRSTOFFDAY").ToString().Trim() Then
                        mShift = "OFF"
                        mShiftM = "OFF"
                        If WI <> "Y" Then
                            mRdays = mRdays + 1
                        End If
                    ElseIf mwDay = RsCrEmp.Tables(0).Rows(0)("SECONDOFFDAY").ToString().Trim() Then

                        mSatCtr = mSatCtr + 1
                        mSatCtr = Math.Truncate((mDt.Day + (CInt(mDt.DayOfWeek))) / 7) '+ 1   'nitin
                        If (RsCrEmp.Tables(0).Rows(0)("ALTERNATE_OFF_DAYS").ToString().IndexOf(mSatCtr.ToString().Trim(), 0) + 1) > 0 Then
                            If RsCrEmp.Tables(0).Rows(0)("SECONDOFFTYPE").ToString().Trim() = "H" Then
                                mShift = RsCrEmp.Tables(0).Rows(0)("HalfDayShift").ToString()
                                If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                                    mShiftM = TempArrayM(i)
                                End If
                            Else
                                mShift = "OFF"
                                mShiftM = "OFF"
                            End If
                            If WI <> "Y" Then
                                mRdays = mRdays + 1
                            End If
                        Else
                            mShift = TempArray(i) ' Added By Nitin
                            If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                                mShiftM = TempArrayM(i)
                            End If
                        End If
                    Else
                        mShift = TempArray(i) ' Added By Nitin 
                        If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                            mShiftM = TempArrayM(i)
                        End If
                    End If
                    If mShift = "OFF" Then
                        mSTAT = "WO"
                        mWoVal = 1
                        mAbsentVal = 0
                    Else
                        mSTAT = "A"
                        mAbsentVal = 1
                        mWoVal = 0
                    End If

                    If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                        sSql = "update TBLTIMEREGISTER set Shift='" & mShift.Trim & "',MShift='" & mShiftM.Trim & "', ShiftAttended='" & mShift.Trim & "', MShiftAttended='" & mShiftM.Trim & "', Status='" & mSTAT & "',MStatus='" & mSTAT & "',WO_Value=" & mWoVal & ",AbsentValue=" & mAbsentVal & ",MABSENTVALUE=" & mAbsentVal & ",MPRESENTVALUE=0,leavevalue=0,HOLIDAY_VALUE=0,HOURSWORKED=0,OtDuration=0,OTAmount=0 where paycode='" & RsCrEmp.Tables(0).Rows(0)("paycode").ToString().Trim() & "' and Dateoffice='" & mDt.ToString("yyyy-MM-dd") & "'"
                    Else
                        sSql = "update TBLTIMEREGISTER set Shift='" & mShift.Trim & "',ShiftAttended='" & mShift.Trim & "',Status='" & mSTAT & "',WO_Value=" & mWoVal & ",AbsentValue=" & mAbsentVal & ",leavevalue=0,HOLIDAY_VALUE=0,HOURSWORKED=0,OtDuration=0,OTAmount=0 where paycode='" & RsCrEmp.Tables(0).Rows(0)("paycode").ToString().Trim() & "' and Dateoffice='" & mDt.ToString("yyyy-MM-dd") & "'"
                    End If

                    If servername = "Access" Then
                        If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "M" Then
                            sSql = "update TBLTIMEREGISTER set Shift='" & mShift.Trim & "',MShift='" & mShiftM.Trim & "', ShiftAttended='" & mShift.Trim & "', MShiftAttended='" & mShiftM.Trim & "', Status='" & mSTAT & "',MStatus='" & mSTAT & "',WO_Value=" & mWoVal & ",AbsentValue=" & mAbsentVal & ",MABSENTVALUE=" & mAbsentVal & ",MPRESENTVALUE=0,leavevalue=0,HOLIDAY_VALUE=0,HOURSWORKED=0,OtDuration=0,OTAmount=0 where paycode='" & RsCrEmp.Tables(0).Rows(0)("paycode") & "' and FORMAT(DateOffice,'yyyy-MM-dd')='" & Format(mDt, "yyyy-MM-dd") & "'"
                        Else
                            sSql = "update TBLTIMEREGISTER set Shift='" & mShift.Trim & "',ShiftAttended='" & mShift.Trim & "',Status='" & mSTAT & "',WO_Value=" & mWoVal & ",AbsentValue=" & mAbsentVal & ",leavevalue=0,HOLIDAY_VALUE=0,HOURSWORKED=0,OtDuration=0,OTAmount=0 where paycode='" & RsCrEmp.Tables(0).Rows(0)("paycode") & "' and FORMAT(DateOffice,'yyyy-MM-dd')='" & Format(mDt, "yyyy-MM-dd") & "'"
                        End If
                        cmd1 = New OleDbCommand(sSql, con1Access)
                        cmd1.ExecuteNonQuery()
                    Else
                        cmd = New SqlCommand(sSql, conSQL)
                        cmd.ExecuteNonQuery()
                    End If
                    mRdays = mRdays - 1
                    mDt = mDt.AddDays(1)
                End While

                mRdays = Convert.ToInt32(RsCrEmp.Tables(0).Rows(0)("CDays"))
                If mRdays <= 0 Then
                    mRdays = 7
                End If

                Pos = Pos + 4
                i = i + 1 ' Added By Nitin
                'If Pos > mPat.Length Then
                '    Pos = 1
                '    i = 0 ' Added By Nitin
                'End If
                If i > TempArray.Length - 1 Then
                    'If mPat.Substring(Pos - 1, 3) = "   " Then
                    Pos = 1
                    i = 0 ' Added By Nitin
                    'End If
                End If
            End While
        End If

        If gLDuplicate.ToString().Trim().ToUpper() = "Y" Then
            mDate = Cur_Year
            mDate1 = Cur_Year.Year.ToString()
            lastdate = mDate1.ToString() & "-12-31"
            sSql = "Select * From tbltimeregisterD Where Paycode='" & RsCrEmp.Tables(0).Rows(0)("paycode") & "' And" & " DateOffice='" & lastdate & "'"
            RsEmpTemp = New DataSet
            If servername = "Access" Then
                adapA3 = New OleDbDataAdapter(sSql, con1Access)
                adapA3.Fill(RsEmpTemp)
            Else
                adap3 = New SqlDataAdapter(sSql, conSQL)
                adap3.Fill(RsEmpTemp)
            End If
            'RsEmpTemp = Cn.FillDataSet(sSql)
            If RsEmpTemp.Tables(0).Rows.Count >= 1 Then
                mCnt = 0
            Else
                mCnt = 0
            End If

            If mCnt <= 0 Then
                mRdays = 7
                mLShift = RsCrEmp.Tables(0).Rows(0)("Shift").ToString().Trim()
                If RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "R" Then
                    mRdays = Convert.ToInt32(RsCrEmp.Tables(0).Rows(0)("ShiftRemainDays"))
                    mPat = RsCrEmp.Tables(0).Rows(0)("SHIFTPATTERN").ToString().Trim()
                ElseIf RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "F" Then
                    mPat = RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," + RsCrEmp.Tables(0).Rows(0)("Shift").ToString() & "," + RsCrEmp.Tables(0).Rows(0)("Shift").ToString()
                ElseIf RsCrEmp.Tables(0).Rows(0)("SHIFTTYPE").ToString().Trim() = "L" Then
                    mPat = "FLX,FLX,FLX"
                    mLShift = "FLX"
                Else
                    mPat = "IGN,IGN,IGN"
                    mLShift = "IGN"
                End If

                Pos = (mPat.IndexOf(mLShift, 0) + 1)
                If Pos <= 0 Then
                    Pos = 1
                End If

                mDt = mDate
                While mDt.ToString("yyyy") = mDate.ToString("yyyy")
                    While mRdays > 0 AndAlso mDt.ToString("yyyy") = mDate.ToString("yyyy")
                        If mDt.ToString("dd") = "01" Then
                            mSatCtr = 0
                        End If

                        mwDay = mDt.ToString("dddd").ToUpper().Substring(0, 3)
                        If mwDay = RsCrEmp.Tables(0).Rows(0)("FIRSTOFFDAY").ToString().Trim() Then
                            mShift = "OFF"
                            If WI <> "Y" Then
                                mRdays = mRdays + 1
                            End If
                        ElseIf mwDay = RsCrEmp.Tables(0).Rows(0)("SECONDOFFDAY").ToString().Trim() Then
                            mSatCtr = mSatCtr + 1
                            If (RsCrEmp.Tables(0).Rows(0)("ALTERNATE_OFF_DAYS").ToString().Trim().IndexOf(mSatCtr.ToString(), 0) + 1) > 0 Then
                                If RsCrEmp.Tables(0).Rows(0)("SECONDOFFTYPE").ToString().Trim() = "H" Then
                                    mShift = RsCrEmp.Tables(0).Rows(0)("HalfDayShift").ToString().Trim()
                                Else
                                    mShift = "OFF"
                                End If

                                If WI <> "Y" Then
                                    mRdays = mRdays + 1
                                End If
                            Else
                                mShift = mPat.Substring(Pos - 1, 3)
                            End If
                        Else
                            mShift = mPat.Substring(Pos - 1, 3)
                        End If

                        If mShift = "OFF" Then
                            mSTAT = "WO"
                            mWoVal = 1
                            mAbsentVal = 0
                        Else
                            mSTAT = "A"
                            mAbsentVal = 1
                            mWoVal = 0
                        End If
                        sSql = "update TBLTIMEREGISTERD set Shift='" & mShift & "',ShiftAttended='" & mShift & "',Status='" & mSTAT & "',WO_Value=" & mWoVal & ",AbsentValue=" & mAbsentVal & ",leavevalue=0,HOLIDAY_VALUE=0,HOURSWORKED=0,OtDuration=0,OTAmount=0 where paycode='" & RsCrEmp.Tables(0).Rows(0)("paycode").ToString().Trim() & "' and Dateoffice='" & mDt.ToString("yyyy-MM-dd") & "'"
                        If servername = "Access" Then
                            cmd1 = New OleDbCommand(sSql, con1Access)
                            cmd1.ExecuteNonQuery()
                        Else
                            cmd = New SqlCommand(sSql, conSQL)
                            cmd.ExecuteNonQuery()                           
                        End If
                        'Cn.Execute(sSql)
                        mRdays = mRdays - 1
                        mDt = mDt.AddDays(1)
                    End While
                    mRdays = Convert.ToInt32(RsCrEmp.Tables(0).Rows(0)("CDays"))
                    If mRdays <= 0 Then
                        mRdays = 7
                    End If
                    Pos = Pos + 4
                    If Pos > mPat.Length Then
                        Pos = 1
                    End If
                    If mPat.Substring(Pos - 1, 3) = "   " Then
                        Pos = 1
                    End If
                End While
            End If
        End If
        If servername = "Access" Then
            If con1Access.State <> ConnectionState.Closed Then
                con1Access.Close()
            End If
        Else
            If conSQL.State <> ConnectionState.Closed Then
                conSQL.Close()
            End If
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub
    Public Function ConnectToDevice(vnMachineNumber As Integer, vpszIPAddress As String, vpszNetPort As Integer, vnTimeOut As Long, vnProtocolType As Long, vpszNetPassword As Integer, vnLicense As Long) As Integer
        Return FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
    End Function
    Public Function funcGetGeneralLogData(vnMachineNumber As Integer, vpszIPAddress As String, vpszNetPort As Integer, vnTimeOut As Long, vnProtocolType As Long, vpszNetPassword As Integer, vnLicense As Long, vnReadMark As Integer, clearLog As Boolean, A_R As String, Purpose As String, IN_OUTDType As String, ID_NO As String, datafile As String, CreateEmpMaster As Boolean) As String
        'Dim datafile As String = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"

        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet

        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds, ds1 As DataSet

        Dim vStrEnrollNumber As String
        Dim vSEnrollNumber As Double
        Dim vVerifyMode As Integer
        Dim vInOutMode As Integer
        Dim vdwDate As Date
        Dim vnCount As Integer
        Dim vnResultCode As Long ' Update 2008/12/18 by CCH
        Dim vstrFileName As String
        Dim vdBeginDate As Date = Date.MinValue
        Dim vdEndDate As Date = Date.MinValue
        Dim vstrTmp As String = ""
        'Dim vnFileNum As Integer
        Dim vstrFileData As String = ""
        'Dim vnReadMark As Integer
        If A_R = "S" Then
            gnCommHandleIndex = FK_ConnectUSB(vnMachineNumber, vnLicense)
        Else
            gnCommHandleIndex = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
        End If
        If gnCommHandleIndex > 0 Then
            'MsgBox("Connection success")
        Else
            Return "Connection fail"
            Exit Function
        End If
        Dim vstrData As String = New String(CType(ChrW(32), Char), 256)
        Dim ret = FK_GetProductData(gnCommHandleIndex, CType(1, Integer), vstrData)
        'MsgBox(vstrData)
        If license.LicenseKey <> "" Then
            If license.DeviceSerialNo.Contains(vstrData.Trim) Then
            Else
                FK_DisConnect(gnCommHandleIndex)
                Return "Invalid Serial number " & vstrData
            End If
        End If
        vnResultCode = FK_EnableDevice(gnCommHandleIndex, 0)
        If vnResultCode = False Then
            FK_DisConnect(gnCommHandleIndex)
            Return gstrNoDevice
            Exit Function
        End If

        Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(xsSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(xsSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        'vnReadMark = 0    '1 for new log, 0 for all log
        vnResultCode = FK_LoadGeneralLogData(gnCommHandleIndex, vnReadMark)
        If vnResultCode <> RUN_SUCCESS Then
            'MsgBox(ReturnResultPrint(vnResultCode))
        Else
            vnCount = 1
            XtraMasterTest.LabelControlStatus.Text = "Downloading from " & vpszIPAddress & "..."
            Application.DoEvents()
            If FK_GetLogDataIsSupportStringID(gnCommHandleIndex) = RUN_SUCCESS Then
                Do
                    Dim MinOut As String
                    vStrEnrollNumber = Space(USER_ID_LEN)
                    vnResultCode = FK_GetGeneralLogData_StringID(gnCommHandleIndex, vStrEnrollNumber, vVerifyMode, vInOutMode, vdwDate)
                    Dim idwInOutMode
                    If vInOutMode = 1 Then
                        idwInOutMode = "0"
                    ElseIf vInOutMode = 2 Then
                        idwInOutMode = "1"
                    End If
                    If vnResultCode <> RUN_SUCCESS Then
                        If vnResultCode = RUNERR_DATAARRAY_END Then
                            vnResultCode = RUN_SUCCESS
                        End If
                        Exit Do
                    End If
                    Dim paycodelistForEmpMaster As New List(Of String)() ' for emp master
                    paycodelistForEmpMaster.Add(vStrEnrollNumber & ";")
                    If CreateEmpMaster = True Then
                        Dim payCodeArrEmpMaster() As String = paycodelistForEmpMaster.ToArray
                        Common.CreateEmployee(payCodeArrEmpMaster)
                    End If

                    Dim tmpstrEnrollNumber As String
                    If IsNumeric(vStrEnrollNumber) = True Then
                        tmpstrEnrollNumber = Convert.ToInt64(vStrEnrollNumber).ToString("000000000000")
                    Else
                        tmpstrEnrollNumber = Trim(vStrEnrollNumber)
                    End If

                    funcGetGeneralLogDataZK(tmpstrEnrollNumber, "", idwInOutMode, vdwDate.ToString("yyyy-MM-dd HH:mm:ss"), "", Purpose, IN_OUTDType, ID_NO, datafile)
                    vnCount = vnCount + 1
                Loop
            Else
                Do
                    Dim MinOut As String
                    vnResultCode = FK_GetGeneralLogData(gnCommHandleIndex, vSEnrollNumber, vVerifyMode, vInOutMode, vdwDate)

                    Dim idwInOutMode
                    If vInOutMode = 1 Then
                        idwInOutMode = "0"
                    ElseIf vInOutMode = 2 Then
                        idwInOutMode = "1"
                    End If
                    If vnResultCode <> RUN_SUCCESS Then
                        If vnResultCode = RUNERR_DATAARRAY_END Then
                            vnResultCode = RUN_SUCCESS
                        End If
                        Exit Do
                    End If
                    Dim paycodelistForEmpMaster As New List(Of String)() ' for emp master
                    paycodelistForEmpMaster.Add(vSEnrollNumber.ToString & ";")
                    If CreateEmpMaster = True Then
                        Dim payCodeArrEmpMaster() As String = paycodelistForEmpMaster.ToArray
                        Common.CreateEmployee(payCodeArrEmpMaster)
                    End If

                    Dim tmpstrEnrollNumber As String
                    If IsNumeric(vSEnrollNumber) = True Then
                        tmpstrEnrollNumber = Convert.ToInt64(vSEnrollNumber).ToString("000000000000")
                    Else
                        tmpstrEnrollNumber = Trim(vSEnrollNumber)
                    End If

                    funcGetGeneralLogDataZK(tmpstrEnrollNumber, "", idwInOutMode, vdwDate.ToString("yyyy-MM-dd HH:mm:ss"), "", Purpose, IN_OUTDType, ID_NO, datafile)
                    vnCount = vnCount + 1
                Loop
            End If
            FK_EnableDevice(gnCommHandleIndex, 1)
            Application.DoEvents()
            'MsgBox("Finish Downloading")
        End If
        If clearLog = True Then
            FK_EmptyGeneralLogData(1)
        End If
        FK_DisConnect(gnCommHandleIndex)
        Dim sSql As String = "select Rawdata.CARDNO, Rawdata.OFFICEPUNCH, TblEmployee.PAYCODE, TblEmployee.TELEPHONE1, TblEmployee.EMPNAME, Rawdata.ReasonCode, EmployeeGroup.Id  from Rawdata, TblEmployee, EmployeeGroup where Rawdata.CARDNO = TblEmployee.PRESENTCARDNO and TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId order by  Rawdata.OFFICEPUNCH"
        'Dim sSql As String = "select Rawdata.CARDNO, Rawdata.OFFICEPUNCH, TblEmployee.PAYCODE, TblEmployee.TELEPHONE1, TblEmployee.EMPNAME, Rawdata.ReasonCode  from Rawdata, TblEmployee where Rawdata.CARDNO = TblEmployee.PRESENTCARDNO order by  Rawdata.OFFICEPUNCH"
        ds = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If

        Dim sSql1 As String
        Dim paycodelist As New List(Of String)()

        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            XtraMasterTest.LabelControlStatus.Text = "Validating Punch " & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & " " & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss")
            Application.DoEvents()
            paycodelist.Add(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim)
            sSql1 = "select count (MachineRawPunch.CARDNO) from MachineRawPunch, Rawdata where Rawdata.CARDNO = MachineRawPunch.CARDNO and (MachineRawPunch.CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and MachineRawPunch.OFFICEPUNCH = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "')"
            ds1 = New DataSet
            If servername = "Access" Then
                sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd HH:mm:ss') = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                adapA1 = New OleDbDataAdapter(sSql1, con1)
                adapA1.Fill(ds1)
            Else
                adap1 = New SqlDataAdapter(sSql1, con)
                adap1.Fill(ds1)
            End If
            'MsgBox(ds1.Tables(0).Rows(0).Item(0))
            Dim IN_OUT As String = "I"
            If ds1.Tables(0).Rows(0).Item(0).ToString = 0 Then
                If IN_OUTDType = "B" Then
                    Dim ds2 As DataSet = New DataSet
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd") & "'"
                    If servername = "Access" Then
                        Dim adapAT As OleDbDataAdapter = New OleDbDataAdapter(sSql1, con1)
                        adapAT.Fill(ds2)
                    Else
                        sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd") & "'"
                        Dim adapT As SqlDataAdapter = New SqlDataAdapter(sSql1, con)
                        adapT.Fill(ds2)
                    End If
                    If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                        IN_OUT = "I"
                    Else
                        If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                            IN_OUT = "I"
                        Else
                            IN_OUT = "O"
                        End If
                    End If
                ElseIf IN_OUTDType = "I" Then
                    IN_OUT = "I"
                ElseIf IN_OUTDType = "O" Then
                    IN_OUT = "O"
                ElseIf IN_OUTDType = "D" Then
                    IN_OUT = ds.Tables(0).Rows(i).Item("ReasonCode").ToString.Trim
                End If
                sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                sSql1 = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                If servername = "Access" Then
                    Try
                        If con1.State <> ConnectionState.Open Then
                            con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, con1)
                        cmd1.ExecuteNonQuery()
                        cmd1 = New OleDbCommand(sSql1, con1)
                        cmd1.ExecuteNonQuery()
                        If con1.State <> ConnectionState.Closed Then
                            con1.Close()
                        End If
                    Catch
                        If con1.State <> ConnectionState.Closed Then
                            con1.Close()
                        End If
                    End Try
                Else
                    Try
                        If con.State <> ConnectionState.Open Then
                            con.Open()
                        End If
                        '"insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                        cmd = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand(sSql1, con)
                        cmd.ExecuteNonQuery()
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    Catch
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    End Try
                End If

                If Common.IsParallel = "Y" Then
                    Dim cardNo As String
                    If IsNumeric(ds.Tables(0).Rows(i).Item("CARDNO")) Then
                        cardNo = Convert.ToDouble(ds.Tables(0).Rows(i).Item("CARDNO"))
                    Else
                        cardNo = ds.Tables(0).Rows(i).Item("CARDNO")
                    End If
                    Common.parallelInsert(cardNo, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")), IN_OUT, "N", ds.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim, ID_NO)
                End If

                'sms
                If g_SMSApplicable = "" Then
                    Load_SMS_Policy()
                End If
                If g_SMSApplicable = "Y" Then
                    If g_isAllSMS = "Y" Then
                        Try
                            If ds.Tables(0).Rows(i).Item("TELEPHONE1").ToString.Trim <> "" Then
                                Dim SmsContent As String = ds.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim & " " & g_AllContent1 & " " & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm") & " " & g_AllContent2
                                sendSMS(ds.Tables(0).Rows(i).Item("TELEPHONE1").ToString.Trim, SmsContent)
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                End If
                'sms end
            End If
        Next

        If servername = "Access" Then
            If con1.State <> ConnectionState.Open Then
                con1.Open()
            End If
            cmd1 = New OleDbCommand("delete from Rawdata", con1)
            cmd1.ExecuteNonQuery()
            If con1.State <> ConnectionState.Closed Then
                con1.Close()
            End If
        Else
            If con.State <> ConnectionState.Open Then
                con.Open()
            End If
            cmd = New SqlCommand("delete from Rawdata", con)
            cmd.ExecuteNonQuery()
            If con.State <> ConnectionState.Closed Then
                con.Close()
            End If
        End If

        Dim paycodeArray = paycodelist.Distinct.ToArray
        If paycodeArray.Length > 0 Then
            'Load_Corporate_PolicySql()
            Dim mindate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Dim maxdate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Dim mindatetmp As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Do While mindate <= maxdate
                For i As Integer = 0 To paycodeArray.Length - 1
                    XtraMasterTest.LabelControlStatus.Text = "Removing Duplicate Punch for " & paycodeArray(i) & " " & mindate.ToString("yyyy-MM-dd")
                    Application.DoEvents()
                    Remove_Duplicate_Punches(mindate, paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                    mindate = mindate.AddDays(1)
                    If mindate > maxdate Then
                        Exit Do
                    End If
                Next
            Loop
            For i As Integer = 0 To paycodeArray.Length - 1
                XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & paycodeArray.Length
                Application.DoEvents()
                Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PAYCODE = '" & paycodeArray(i) & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSqltmp, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                        Process_AllRTC(mindatetmp.AddDays(-1), maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                    Else
                        Process_AllnonRTC(mindatetmp, maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                        If EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                            Process_AllnonRTCMulti(mindatetmp, maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                        End If
                    End If
                End If
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
            Next
            XtraMasterTest.LabelControlCount.Text = ""
            Application.DoEvents()
        End If
        Return "Success"
    End Function
    Public Shared Sub CreateEmployee(ByVal paycodeArray As String())
        Dim Shift As String, mComp As String, mDept As String, mBranch As String, mEmpGrp As String, mmgrade As String, mmcat As String,
                SHIFTTYPE As String, SHIFTPATTERN As String,
                SHIFTREMAINDAYS As String, LASTSHIFTPERFORMED As String, INONLY As String, ISPUNCHALL As String,
                ISTIMELOSSALLOWED As String, ALTERNATE_OFF_DAYS As String, CDAYS As String, ISROUNDTHECLOCKWORK As String,
                ISOT As String, OTRATE As String, FIRSTOFFDAY As String, SECONDOFFTYPE As String, HALFDAYSHIFT As String,
                SECONDOFFDAY As String, PERMISLATEARRIVAL As String, PERMISEARLYDEPRT As String, ISAUTOSHIFT As String,
                ISOUTWORK As String, MAXDAYMIN As String, ISOS As String, AUTH_SHIFTS As String, TIME1 As String,
                SHORT1 As String, HALF As String, ISHALFDAY As String, ISSHORT As String, TWO As String, MShift As String
        Dim mDateofjoin As Object, mdateofbirth As DateTime
        Dim Id As Integer
        LoadEmpOtherDetailsWhileDataDownload(Shift, mComp, mDept, mBranch, mEmpGrp, mmgrade, mmcat, mDateofjoin, mdateofbirth, SHIFTTYPE,
                SHIFTPATTERN, SHIFTREMAINDAYS, LASTSHIFTPERFORMED, INONLY, ISPUNCHALL, ISTIMELOSSALLOWED, ALTERNATE_OFF_DAYS, CDAYS, ISROUNDTHECLOCKWORK,
               ISOT, OTRATE, FIRSTOFFDAY, SECONDOFFTYPE, HALFDAYSHIFT, SECONDOFFDAY, PERMISLATEARRIVAL, PERMISEARLYDEPRT, ISAUTOSHIFT,
               ISOUTWORK, MAXDAYMIN, ISOS, AUTH_SHIFTS, TIME1, SHORT1, HALF, ISHALFDAY, ISSHORT, TWO, Id, MShift)

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet

        For i As Integer = 0 To paycodeArray.Length - 1
            'Dim sSqlXX As String = "select PAYCODE from TblEmployee where PAYCODE='" & paycodeArray(i) & "'"
            Dim tmpPay() As String = paycodeArray(i).ToString.Split(";")
            Dim tmpEmpPre As String
            Dim tmpEmpName As String = ""
            Try
                tmpEmpName = tmpPay(1)
            Catch ex As Exception
                tmpEmpName = ""
            End Try
            If IsNumeric(tmpPay(0)) = True Then
                tmpEmpPre = Convert.ToDouble(tmpPay(0)).ToString("000000000000")
            Else
                tmpEmpPre = tmpPay(0)
            End If
            If tmpEmpPre = "0" Or tmpEmpPre = "" Then
                Continue For
            End If
            Dim sSqlXX As String = "select PAYCODE from TblEmployee where PRESENTCARDNO='" & tmpEmpPre & "'"
            ds = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSqlXX, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSqlXX, Common.con)
                adap.Fill(ds)
            End If
            Dim com As Common = New Common
            If ds.Tables(0).Rows.Count = 0 Then
                InsertEmpOtherDetailsWhileDataDownload(tmpPay(0), Shift, mComp, mDept, mBranch, mEmpGrp, mmgrade, mmcat, mDateofjoin, mdateofbirth,
                   SHIFTTYPE, SHIFTPATTERN, SHIFTREMAINDAYS, LASTSHIFTPERFORMED, INONLY, ISPUNCHALL, ISTIMELOSSALLOWED, ALTERNATE_OFF_DAYS, CDAYS, ISROUNDTHECLOCKWORK,
                   ISOT, OTRATE, FIRSTOFFDAY, SECONDOFFTYPE, HALFDAYSHIFT, SECONDOFFDAY, PERMISLATEARRIVAL, PERMISEARLYDEPRT, ISAUTOSHIFT, ISOUTWORK, MAXDAYMIN, ISOS, AUTH_SHIFTS, TIME1,
                   SHORT1, HALF, ISHALFDAY, ISSHORT, TWO, tmpEmpName, MShift)
                com.DutyRoster(tmpPay(0), Now.ToString("01/" & Now.Month & "/" & Now.Year), EmpGrpArr(Id).g_Wo_Include)
            End If
            'end add employee while downloading logs
        Next
    End Sub

    Public Shared Sub CreateEmployeeZKAccess(ByVal paycodeArray As String())
        Dim Shift As String, mComp As String, mDept As String, mBranch As String, mEmpGrp As String, mmgrade As String, mmcat As String,
                SHIFTTYPE As String, SHIFTPATTERN As String,
                SHIFTREMAINDAYS As String, LASTSHIFTPERFORMED As String, INONLY As String, ISPUNCHALL As String,
                ISTIMELOSSALLOWED As String, ALTERNATE_OFF_DAYS As String, CDAYS As String, ISROUNDTHECLOCKWORK As String,
                ISOT As String, OTRATE As String, FIRSTOFFDAY As String, SECONDOFFTYPE As String, HALFDAYSHIFT As String,
                SECONDOFFDAY As String, PERMISLATEARRIVAL As String, PERMISEARLYDEPRT As String, ISAUTOSHIFT As String,
                ISOUTWORK As String, MAXDAYMIN As String, ISOS As String, AUTH_SHIFTS As String, TIME1 As String,
                SHORT1 As String, HALF As String, ISHALFDAY As String, ISSHORT As String, TWO As String, MShift As String
        Dim mDateofjoin As Object, mdateofbirth As DateTime
        Dim Id As Integer
        LoadEmpOtherDetailsWhileDataDownload(Shift, mComp, mDept, mBranch, mEmpGrp, mmgrade, mmcat, mDateofjoin, mdateofbirth, SHIFTTYPE,
                SHIFTPATTERN, SHIFTREMAINDAYS, LASTSHIFTPERFORMED, INONLY, ISPUNCHALL, ISTIMELOSSALLOWED, ALTERNATE_OFF_DAYS, CDAYS, ISROUNDTHECLOCKWORK,
               ISOT, OTRATE, FIRSTOFFDAY, SECONDOFFTYPE, HALFDAYSHIFT, SECONDOFFDAY, PERMISLATEARRIVAL, PERMISEARLYDEPRT, ISAUTOSHIFT,
               ISOUTWORK, MAXDAYMIN, ISOS, AUTH_SHIFTS, TIME1, SHORT1, HALF, ISHALFDAY, ISSHORT, TWO, Id, MShift)

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet

        For i As Integer = 0 To paycodeArray.Length - 1
            'Dim sSqlXX As String = "select PAYCODE from TblEmployee where PAYCODE='" & paycodeArray(i) & "'"
            Dim tmpPay() As String = paycodeArray(i).ToString.Split(";")
            Dim tmpEmpPre As String
            Dim tmpEmpName As String = ""
            Dim MCardNo = ""
            Try
                tmpEmpName = tmpPay(1)
            Catch ex As Exception
                tmpEmpName = ""
            End Try
            If IsNumeric(tmpPay(0)) = True Then
                tmpEmpPre = Convert.ToDouble(tmpPay(0)).ToString("000000000000")
            Else
                tmpEmpPre = tmpPay(0)
            End If
            If tmpEmpPre = "0" Or tmpEmpPre = "" Then
                Continue For
            End If
            'Try
            '    MCardNo = tmpPay(2)
            'Catch ex As Exception
            '    MCardNo = ""
            'End Try

            Dim sSqlXX As String = "select PAYCODE from TblEmployee where PRESENTCARDNO='" & tmpEmpPre & "'"
            ds = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSqlXX, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSqlXX, Common.con)
                adap.Fill(ds)
            End If
            Dim com As Common = New Common
            If ds.Tables(0).Rows.Count = 0 Then
                InsertEmpOtherDetailsWhileDataDownload(tmpPay(0), Shift, mComp, mDept, mBranch, mEmpGrp, mmgrade, mmcat, mDateofjoin, mdateofbirth,
                   SHIFTTYPE, SHIFTPATTERN, SHIFTREMAINDAYS, LASTSHIFTPERFORMED, INONLY, ISPUNCHALL, ISTIMELOSSALLOWED, ALTERNATE_OFF_DAYS, CDAYS, ISROUNDTHECLOCKWORK,
                   ISOT, OTRATE, FIRSTOFFDAY, SECONDOFFTYPE, HALFDAYSHIFT, SECONDOFFDAY, PERMISLATEARRIVAL, PERMISEARLYDEPRT, ISAUTOSHIFT, ISOUTWORK, MAXDAYMIN, ISOS, AUTH_SHIFTS, TIME1,
                   SHORT1, HALF, ISHALFDAY, ISSHORT, TWO, tmpEmpName, MShift)
                com.DutyRoster(tmpPay(0), Now.ToString("01/" & Now.Month & "/" & Now.Year), EmpGrpArr(Id).g_Wo_Include)
            End If
            'end add employee while downloading logs
        Next
    End Sub
    Public Shared Function LoadEmpOtherDetailsWhileDataDownload(ByRef Shift As String, ByRef mComp As String, _
            ByRef mDept As String, ByRef mBranch As String, ByRef mEmpGrp As String, ByRef mmgrade As String, ByRef mmcat As String, _
           ByRef mDateofjoin As Object, ByRef mdateofbirth As DateTime, ByRef SHIFTTYPE As String, ByRef SHIFTPATTERN As String, _
           ByRef SHIFTREMAINDAYS As String, ByRef LASTSHIFTPERFORMED As String, ByRef INONLY As String, ByRef ISPUNCHALL As String, _
          ByRef ISTIMELOSSALLOWED As String, ByRef ALTERNATE_OFF_DAYS As String, ByRef CDAYS As String, ByRef ISROUNDTHECLOCKWORK As String, _
          ByRef ISOT As String, ByRef OTRATE As String, ByRef FIRSTOFFDAY As String, ByRef SECONDOFFTYPE As String, ByRef HALFDAYSHIFT As String, _
          ByRef SECONDOFFDAY As String, ByRef PERMISLATEARRIVAL As String, ByRef PERMISEARLYDEPRT As String, ByRef ISAUTOSHIFT As String, _
          ByRef ISOUTWORK As String, ByRef MAXDAYMIN As String, ByRef ISOS As String, ByRef AUTH_SHIFTS As String, ByRef TIME1 As String, _
          ByRef SHORT1 As String, ByRef HALF As String, ByRef ISHALFDAY As String, ByRef ISSHORT As String, ByRef TWO As String, ByRef Id As Integer, _
        ByRef MShift As String)
        'Try
        mDateofjoin = Now.AddDays(-(Now.Day - 1)).ToString("yyyy-MM-dd HH:mm:ss")
        mDateofjoin = Now.ToString("yyyy-MM-01 HH:mm:ss")
        mdateofbirth = Convert.ToDateTime("1800-01-01 00:00:00")
        Dim sSql As String = "select top 1 SHIFT from tblShiftMaster"
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim ds As DataSet = New DataSet
        'If servername = "Access" Then
        '    adapA = New OleDbDataAdapter(sSql, con1)
        '    adapA.Fill(ds)
        'Else
        '    adap = New SqlDataAdapter(sSql, con)
        '    adap.Fill(ds)
        'End If
        'mShift = ds.Tables(0).Rows(0).Item("SHIFT").ToString.Trim
        sSql = "select top 1 COMPANYCODE from tblcompany"
        ds = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If
        mComp = ds.Tables(0).Rows(0).Item("companycode").ToString.Trim

        sSql = "select top 1 DEPARTMENTCODE from tbldepartment"
        ds = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If
        mDept = ds.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim

        sSql = "select top 1 BRANCHCODE from tblbranch"
        ds = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If
        mBranch = ds.Tables(0).Rows(0).Item("BRANCHCODE").ToString.Trim

        sSql = "select top 1 * from EmployeeGroup order by Id"
        ds = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If
        mEmpGrp = ds.Tables(0).Rows(0).Item("GroupId").ToString.Trim
        Id = ds.Tables(0).Rows(0).Item("Id")
        Shift = ds.Tables(0).Rows(0).Item("SHIFT").ToString.Trim()
        MShift = ds.Tables(0).Rows(0).Item("MSHIFT").ToString.Trim()
        SHIFTTYPE = ds.Tables(0).Rows(0).Item("SHIFTTYPE").ToString.Trim()
        SHIFTPATTERN = ds.Tables(0).Rows(0).Item("SHIFTPATTERN").ToString.Trim()
        SHIFTREMAINDAYS = ds.Tables(0).Rows(0).Item("SHIFTREMAINDAYS").ToString.Trim()
        LASTSHIFTPERFORMED = ds.Tables(0).Rows(0).Item("LASTSHIFTPERFORMED").ToString.Trim()
        INONLY = ds.Tables(0).Rows(0).Item("INONLY").ToString.Trim()
        ISPUNCHALL = ds.Tables(0).Rows(0).Item("ISPUNCHALL").ToString.Trim()
        ISTIMELOSSALLOWED = ds.Tables(0).Rows(0).Item("ISTIMELOSSALLOWED").ToString.Trim()
        ALTERNATE_OFF_DAYS = ds.Tables(0).Rows(0).Item("ALTERNATE_OFF_DAYS").ToString.Trim()
        CDAYS = ds.Tables(0).Rows(0).Item("CDAYS").ToString.Trim()
        ISROUNDTHECLOCKWORK = ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString.Trim()
        ISOT = ds.Tables(0).Rows(0).Item("ISOT").ToString.Trim()
        OTRATE = ds.Tables(0).Rows(0).Item("OTRATE").ToString.Trim()
        FIRSTOFFDAY = ds.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString.Trim()
        SECONDOFFTYPE = ds.Tables(0).Rows(0).Item("SECONDOFFTYPE").ToString.Trim()
        HALFDAYSHIFT = ds.Tables(0).Rows(0).Item("HALFDAYSHIFT").ToString.Trim()
        SECONDOFFDAY = ds.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString.Trim()
        PERMISLATEARRIVAL = ds.Tables(0).Rows(0).Item("PERMISLATEARRIVAL").ToString.Trim()
        PERMISEARLYDEPRT = ds.Tables(0).Rows(0).Item("PERMISEARLYDEPRT").ToString.Trim()
        ISAUTOSHIFT = ds.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString.Trim()
        ISOUTWORK = ds.Tables(0).Rows(0).Item("ISOUTWORK").ToString.Trim()
        MAXDAYMIN = ds.Tables(0).Rows(0).Item("MAXDAYMIN").ToString.Trim()
        ISOS = ds.Tables(0).Rows(0).Item("ISOS").ToString.Trim()
        AUTH_SHIFTS = ds.Tables(0).Rows(0).Item("AUTH_SHIFTS").ToString.Trim()
        If servername = "Access" Then
            TIME1 = ds.Tables(0).Rows(0).Item("TIME1").ToString.Trim()
            SHORT1 = ds.Tables(0).Rows(0).Item("SHORT1").ToString.Trim()
        Else
            TIME1 = ds.Tables(0).Rows(0).Item("TIME").ToString.Trim()
            SHORT1 = ds.Tables(0).Rows(0).Item("SHORT").ToString.Trim()
        End If
        HALF = ds.Tables(0).Rows(0).Item("HALF").ToString.Trim()
        ISHALFDAY = ds.Tables(0).Rows(0).Item("ISHALFDAY").ToString.Trim()
        ISSHORT = ds.Tables(0).Rows(0).Item("ISSHORT").ToString.Trim()
        TWO = ds.Tables(0).Rows(0).Item("TWO").ToString.Trim()


        sSql = "select top 1 GradeCode from tblgrade"
        ds = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If
        mmgrade = ds.Tables(0).Rows(0).Item("Gradecode").ToString.Trim

        sSql = "select top 1 CAT from tblcatagory"
        ds = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If
        mmcat = ds.Tables(0).Rows(0).Item("Cat").ToString.Trim
        'Catch ex As Exception

        'End Try
    End Function
    Public Shared Function InsertEmpOtherDetailsWhileDataDownload(EmpCode As String, mShift As String, mComp As String,
             mDept As String, mBranch As String, mEmpGrp As String, mmgrade As String, mmcat As String, mDateofjoin As Object, mdateofbirth As DateTime,
             SHIFTTYPE As String, SHIFTPATTERN As String, SHIFTREMAINDAYS As String, LASTSHIFTPERFORMED As String, INONLY As String, ISPUNCHALL As String,
           ISTIMELOSSALLOWED As String, ALTERNATE_OFF_DAYS As String, CDAYS As String, ISROUNDTHECLOCKWORK As String,
           ISOT As String, OTRATE As String, FIRSTOFFDAY As String, SECONDOFFTYPE As String, HALFDAYSHIFT As String,
           SECONDOFFDAY As String, PERMISLATEARRIVAL As String, PERMISEARLYDEPRT As String, ISAUTOSHIFT As String,
           ISOUTWORK As String, MAXDAYMIN As String, ISOS As String, AUTH_SHIFTS As String, TIME1 As String,
           SHORT1 As String, HALF As String, ISHALFDAY As String, ISSHORT As String, TWO As String, EmpNameMCard As String, MShiftM As String)

        Dim sSql As String, strsql As String, i As Integer, M_PAYCODE As String
        Dim m_ispunchall As String, m_InOnly As String, m_two As String, m_isoutwork As String
        Dim m_isautoshift As String, m_IsRoundtheclockwork As String, m_istimelossallowed
        Dim m_isot As String, m_isos As String, m_isHalfDay As String, m_isshort As String
        Dim m_time As String, m_half As String, m_short As String, m_Auth_Shifts As String
        Dim m_isosminus As String, m_Weekdays As String, M_secondwotype As String
        Dim M_DATEOFBIRTH As String, M_SHIFT As String, m_halfdayshift As String, m_shifttype As String, m_LeavingDate As String, m_reader As String, mUserType As String

        'For Employee Shift START
        'Dim tmpemp() As String = EmpCode.Split(":")
        'EmpCode = tmpemp(0)

        Dim mCARDNO As String '= EmpCode
        If IsNumeric(EmpCode) = True Then
            mCARDNO = Convert.ToInt64(EmpCode).ToString("000000000000")
        Else
            mCARDNO = EmpCode
        End If
        M_DATEOFBIRTH = mdateofbirth
        m_ispunchall = "Y"
        m_InOnly = "N"
        m_two = "N"
        m_isoutwork = "Y"
        m_IsRoundtheclockwork = "N"
        m_istimelossallowed = "Y"
        m_isHalfDay = "N"
        m_isshort = "N"
        m_half = 0
        m_short = 0
        m_time = 0
        m_isautoshift = "N"
        m_Auth_Shifts = ""
        m_reader = ""
        m_isot = "N"
        m_isos = "N"
        m_isosminus = "N"
        m_shifttype = "F"
        M_SHIFT = mShift
        M_secondwotype = ""
        m_halfdayshift = ""
        m_Weekdays = m_Weekdays & Str(i)
        M_PAYCODE = EmpCode
        Dim EmpName As String = ""
        Dim Mcard As String = ""
        Try
            Dim tmpArr() As String = EmpNameMCard.Split(",")
            If tmpArr.Length > 1 Then
                EmpName = tmpArr(0)
                Mcard = tmpArr(1)
            Else
                EmpName = EmpNameMCard
            End If
        Catch ex As Exception
            EmpName = EmpNameMCard
        End Try

        If EmpName.Trim = "" Then
            EmpName = M_PAYCODE
        Else
            EmpName = EmpName
        End If
        Dim BLOODGROUP As String = "N/A"
        Dim ValidityStartDate As DateTime = Convert.ToDateTime("2018-01-01 00:00:00")
        Dim ValidityEndDate As DateTime = Convert.ToDateTime("2029-12-31 00:00:00")
        Dim k As Integer
        Dim cmd1 As OleDbCommand
        Dim cmd As SqlCommand
        k = 0

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
        End If

        '  If frmBrowse.m_bModeNew Then
        sSql = "Insert Into tblEmployee(Paycode,presentcardno) Values('" & M_PAYCODE & "','" & mCARDNO & "')"
        'Cn.Execute (sSql), k
        Try
            If Common.servername = "Access" Then
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
            End If
            Common.LogPost("Employee Create; Paycode: " & M_PAYCODE)
        Catch ex As Exception
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            Exit Function
        End Try
        k = 0
        If Mcard <> "" Then
            sSql = "update tblemployee set EMPNAME = '" & EmpName &
              "',GUARDIANNAME = '',PRESENTCARDNO ='" & mCARDNO &
              "',ACTIVE = 'Y',COMPANYCODE = '" & mComp &
              "',DEPARTMENTCODE ='" & mDept & "',CAT = '" & mmcat & "',gradecode = '" & mmgrade & "',EmployeeGroupId='" & mEmpGrp & "' " &
              ", DATEOFJOIN ='" & mDateofjoin & "' ,DATEOFBIRTH ='" & mdateofbirth & "',DESIGNATION ='',SEX ='M',ADDRESS1 = '',PINCODE1 =''" &
              ",TELEPHONE1 ='',E_MAIL1 ='', BRANCHCODE='" & mBranch & "', TELEPHONE2 = '', PINCODE2='', BLOODGROUP='" & BLOODGROUP & "', ValidityStartDate='" & ValidityStartDate.ToString("yyyy-MM-dd 00:00:00") & "', ValidityEndDate='" & ValidityEndDate.ToString("yyyy-MM-dd 00:00:00") & "', Leavingdate='1800-01-01 00:00:00',MachineCard='" & Mcard & "'  where PayCode='" & M_PAYCODE & "'"
        Else
            sSql = "update tblemployee set EMPNAME = '" & EmpName &
              "',GUARDIANNAME = '',PRESENTCARDNO ='" & mCARDNO &
              "',ACTIVE = 'Y',COMPANYCODE = '" & mComp &
              "',DEPARTMENTCODE ='" & mDept & "',CAT = '" & mmcat & "',gradecode = '" & mmgrade & "',EmployeeGroupId='" & mEmpGrp & "' " &
              ", DATEOFJOIN ='" & mDateofjoin & "' ,DATEOFBIRTH ='" & mdateofbirth & "',DESIGNATION ='',SEX ='M',ADDRESS1 = '',PINCODE1 =''" &
              ",TELEPHONE1 ='',E_MAIL1 ='', BRANCHCODE='" & mBranch & "', TELEPHONE2 = '', PINCODE2='', BLOODGROUP='" & BLOODGROUP & "', ValidityStartDate='" & ValidityStartDate.ToString("yyyy-MM-dd 00:00:00") & "', ValidityEndDate='" & ValidityEndDate.ToString("yyyy-MM-dd 00:00:00") & "', Leavingdate='1800-01-01 00:00:00'  where PayCode='" & M_PAYCODE & "'"

        End If

        If Common.servername = "Access" Then
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
        Else
            'sSql = "update tblemployee set EMPNAME = '" & mName & _
            '    "',GUARDIANNAME = '" & mfather & "',PRESENTCARDNO ='" & mCARDNO & _
            '    "',ACTIVE = 'Y',COMPANYCODE = '" & mComp & _
            '    "',DEPARTMENTCODE ='" & mDept & "',CAT = '" & mCat & "',gradecode = '" & mgrade & "',EmployeeGroupId='" & EmpGrp & "' " & _
            '    ",DATEOFJOIN ='" & mDateofjoin & "',DATEOFBIRTH ='" & mdateofbirth & "',DESIGNATION ='" & Mid(mDesig, 1, 25) & "',SEX ='" & MMSEX & "',ADDRESS1 = '" & mAddress1 & "',PINCODE1 ='" & mpinCode & "'" & _
            '    ",TELEPHONE1 ='" & mtel1 & "',E_MAIL1 ='" & mEmail & "', BRANCHCODE='" & mBranch & "', TELEPHONE2 = '" & mUid & "', PINCODE2='" & mPan & "' where PayCode='" & mPaycode & "'"
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
        End If
        If Common.servername = "Access" Then
            sSql = "insert into tblEmployeeShiftMaster (PAYCODE,CARDNO,SHIFT,SHIFTTYPE,SHIFTPATTERN,SHIFTREMAINDAYS,LASTSHIFTPERFORMED,INONLY,ISPUNCHALL,ISTIMELOSSALLOWED,ALTERNATE_OFF_DAYS,CDAYS,ISROUNDTHECLOCKWORK,ISOT,OTRATE,FIRSTOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,SECONDOFFDAY,PERMISLATEARRIVAL,PERMISEARLYDEPRT,ISAUTOSHIFT,ISOUTWORK,MAXDAYMIN,ISOS,AUTH_SHIFTS,TIME1,SHORT1,HALF,ISHALFDAY,ISSHORT,TWO,MShift) values ( '" & M_PAYCODE & "','" & mCARDNO & "','" & M_SHIFT & "','" & SHIFTTYPE & "','" & SHIFTPATTERN & "','" & SHIFTREMAINDAYS & "','" & LASTSHIFTPERFORMED & "','" & INONLY & "','" & ISPUNCHALL & "','" & ISTIMELOSSALLOWED & "','" & ALTERNATE_OFF_DAYS & "','" & CDAYS & "','" & ISROUNDTHECLOCKWORK & "','" & ISOT & "','" & OTRATE & "','" & FIRSTOFFDAY & "','" & SECONDOFFTYPE & "','" & HALFDAYSHIFT & "','" & SECONDOFFDAY & "','" & PERMISLATEARRIVAL & "','" & PERMISEARLYDEPRT & "','" & ISAUTOSHIFT & "','" & ISOUTWORK & "','" & MAXDAYMIN & "','" & ISOS & "','" & AUTH_SHIFTS & "','" & TIME1 & "','" & SHORT1 & "','" & HALF & "','" & ISHALFDAY & "','" & ISSHORT & "','" & TWO & "','" & MShiftM & "')"
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
        Else
            sSql = "insert into tblEmployeeShiftMaster (PAYCODE,CARDNO,SHIFT,SHIFTTYPE,SHIFTPATTERN,SHIFTREMAINDAYS,LASTSHIFTPERFORMED,INONLY,ISPUNCHALL,ISTIMELOSSALLOWED,ALTERNATE_OFF_DAYS,CDAYS,ISROUNDTHECLOCKWORK,ISOT,OTRATE,FIRSTOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,SECONDOFFDAY,PERMISLATEARRIVAL,PERMISEARLYDEPRT,ISAUTOSHIFT,ISOUTWORK,MAXDAYMIN,ISOS,AUTH_SHIFTS,TIME,SHORT,HALF,ISHALFDAY,ISSHORT,TWO,MShift) values ( '" & M_PAYCODE & "','" & mCARDNO & "','" & M_SHIFT & "','" & SHIFTTYPE & "','" & SHIFTPATTERN & "','" & SHIFTREMAINDAYS & "','" & LASTSHIFTPERFORMED & "','" & INONLY & "','" & ISPUNCHALL & "','" & ISTIMELOSSALLOWED & "','" & ALTERNATE_OFF_DAYS & "','" & CDAYS & "','" & ISROUNDTHECLOCKWORK & "','" & ISOT & "','" & OTRATE & "','" & FIRSTOFFDAY & "','" & SECONDOFFTYPE & "','" & HALFDAYSHIFT & "','" & SECONDOFFDAY & "','" & PERMISLATEARRIVAL & "','" & PERMISEARLYDEPRT & "','" & ISAUTOSHIFT & "','" & ISOUTWORK & "','" & MAXDAYMIN & "','" & ISOS & "','" & AUTH_SHIFTS & "','" & TIME1 & "','" & SHORT1 & "','" & HALF & "','" & ISHALFDAY & "','" & ISSHORT & "','" & TWO & "','" & MShiftM & "')"
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
        End If

        If Common.servername = "Access" Then
            sSql = "UPDATE TBLEMPLOYEESHIFTMASTER SET  TIME1='" & m_time & "', short1 = '" & m_short & "' WHERE PAYCODE='" & M_PAYCODE & "'"
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
            End If
        End If
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
    End Function

    Public Function funcGetGeneralLogData_f9(vnMachineNumber As Integer, vpszIPAddress As String, vpszNetPort As Integer, vnTimeOut As Long, vnProtocolType As Long, vpszNetPassword As Integer, vnLicense As Long, vnReadMark As Integer, clearLog As Boolean, A_R As String, Purpose As String, IN_OUTDType As String, ID_NO As String, datafile As String, CreateEmpMaster As Boolean) As String
        'Dim datafile As String = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"

        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet

        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds, ds1 As DataSet

        Dim vStrEnrollNumber As String
        Dim vSEnrollNumber As Double
        Dim vVerifyMode As Integer
        Dim vInOutMode As Integer
        Dim vdwDate As Date
        Dim vnCount As Integer
        Dim vnResultCode As Long ' Update 2008/12/18 by CCH
        Dim vstrFileName As String
        Dim vdBeginDate As Date = Date.MinValue
        Dim vdEndDate As Date = Date.MinValue
        Dim vstrTmp As String = ""
        'Dim vnFileNum As Integer
        Dim vstrFileData As String = ""
        Dim F9 As mdlPublic_f9 = New mdlPublic_f9
        'Dim vnReadMark As Integer
        If A_R = "S" Then
            gnCommHandleIndex = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
        Else
            gnCommHandleIndex = F9.FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
        End If
        If gnCommHandleIndex > 0 Then
            'MsgBox("Connection success")
        Else
            Return "Connection fail"
            Exit Function
        End If
        vnResultCode = F9.FK_EnableDevice(gnCommHandleIndex, 0)
        If vnResultCode = False Then
            F9.FK_DisConnect(gnCommHandleIndex)
            Return gstrNoDevice
            Exit Function
        End If

        Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(xsSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(xsSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        'vnReadMark = 0    '1 for new log, 0 for all log
        vnResultCode = F9.FK_LoadGeneralLogData(gnCommHandleIndex, vnReadMark)
        If vnResultCode <> RUN_SUCCESS Then
            'MsgBox(ReturnResultPrint(vnResultCode))
        Else
            vnCount = 1
            XtraMasterTest.LabelControlStatus.Text = "Downloading from " & vpszIPAddress & "..."
            Application.DoEvents()
            If F9.FK_GetLogDataIsSupportStringID(gnCommHandleIndex) = RUN_SUCCESS Then
                Do
                    Dim MinOut As String
                    vStrEnrollNumber = Space(USER_ID_LEN)
                    vnResultCode = F9.FK_GetGeneralLogData_StringID(gnCommHandleIndex, vStrEnrollNumber, vVerifyMode, vInOutMode, vdwDate)
                    Dim idwInOutMode
                    If vInOutMode = 1 Then
                        idwInOutMode = "0"
                    ElseIf vInOutMode = 2 Then
                        idwInOutMode = "1"
                    End If
                    If vnResultCode <> RUN_SUCCESS Then
                        If vnResultCode = RUNERR_DATAARRAY_END Then
                            vnResultCode = RUN_SUCCESS
                        End If
                        Exit Do
                    End If
                    Dim paycodelistForEmpMaster As New List(Of String)() ' for emp master
                    paycodelistForEmpMaster.Add(vStrEnrollNumber & ";")
                    If CreateEmpMaster = True Then
                        Dim payCodeArrEmpMaster() As String = paycodelistForEmpMaster.ToArray
                        Common.CreateEmployee(payCodeArrEmpMaster)
                    End If

                    Dim tmpstrEnrollNumber As String
                    If IsNumeric(vStrEnrollNumber) = True Then
                        tmpstrEnrollNumber = Convert.ToInt64(vStrEnrollNumber).ToString("000000000000")
                    Else
                        tmpstrEnrollNumber = Trim(vStrEnrollNumber)
                    End If

                    funcGetGeneralLogDataZK(tmpstrEnrollNumber, "", idwInOutMode, vdwDate.ToString("yyyy-MM-dd HH:mm:ss"), "", Purpose, IN_OUTDType, ID_NO, datafile)
                    vnCount = vnCount + 1
                Loop
            Else
                Do
                    Dim MinOut As String
                    vnResultCode = F9.FK_GetGeneralLogData(gnCommHandleIndex, vSEnrollNumber, vVerifyMode, vInOutMode, vdwDate)

                    Dim idwInOutMode
                    If vInOutMode = 1 Then
                        idwInOutMode = "0"
                    ElseIf vInOutMode = 2 Then
                        idwInOutMode = "1"
                    End If
                    If vnResultCode <> RUN_SUCCESS Then
                        If vnResultCode = RUNERR_DATAARRAY_END Then
                            vnResultCode = RUN_SUCCESS
                        End If
                        Exit Do
                    End If
                    Dim paycodelistForEmpMaster As New List(Of String)() ' for emp master
                    paycodelistForEmpMaster.Add(vSEnrollNumber.ToString & ";")
                    If CreateEmpMaster = True Then
                        Dim payCodeArrEmpMaster() As String = paycodelistForEmpMaster.ToArray
                        Common.CreateEmployee(payCodeArrEmpMaster)
                    End If

                    Dim tmpstrEnrollNumber As String
                    If IsNumeric(vSEnrollNumber) = True Then
                        tmpstrEnrollNumber = Convert.ToInt64(vSEnrollNumber).ToString("000000000000")
                    Else
                        tmpstrEnrollNumber = Trim(vSEnrollNumber)
                    End If

                    funcGetGeneralLogDataZK(tmpstrEnrollNumber, "", idwInOutMode, vdwDate.ToString("yyyy-MM-dd HH:mm:ss"), "", Purpose, IN_OUTDType, ID_NO, datafile)
                    vnCount = vnCount + 1
                Loop
            End If
            F9.FK_EnableDevice(gnCommHandleIndex, 1)
            Application.DoEvents()
            'MsgBox("Finish Downloading")
        End If
        If clearLog = True Then
            F9.FK_EmptyGeneralLogData(1)
        End If
        F9.FK_DisConnect(gnCommHandleIndex)
        Dim sSql As String = "select Rawdata.CARDNO, Rawdata.OFFICEPUNCH, TblEmployee.PAYCODE, TblEmployee.TELEPHONE1, TblEmployee.EMPNAME, Rawdata.ReasonCode, EmployeeGroup.Id  from Rawdata, TblEmployee, EmployeeGroup where Rawdata.CARDNO = TblEmployee.PRESENTCARDNO and TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId order by  Rawdata.OFFICEPUNCH"
        'Dim sSql As String = "select Rawdata.CARDNO, Rawdata.OFFICEPUNCH, TblEmployee.PAYCODE, TblEmployee.TELEPHONE1, TblEmployee.EMPNAME, Rawdata.ReasonCode  from Rawdata, TblEmployee where Rawdata.CARDNO = TblEmployee.PRESENTCARDNO order by  Rawdata.OFFICEPUNCH"
        ds = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If

        Dim sSql1 As String
        Dim paycodelist As New List(Of String)()

        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            XtraMasterTest.LabelControlStatus.Text = "Validating Punch " & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & " " & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss")
            Application.DoEvents()
            paycodelist.Add(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim)
            sSql1 = "select count (MachineRawPunch.CARDNO) from MachineRawPunch, Rawdata where Rawdata.CARDNO = MachineRawPunch.CARDNO and (MachineRawPunch.CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and MachineRawPunch.OFFICEPUNCH = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "')"
            ds1 = New DataSet
            If servername = "Access" Then
                sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd HH:mm:ss') = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                adapA1 = New OleDbDataAdapter(sSql1, con1)
                adapA1.Fill(ds1)
            Else
                adap1 = New SqlDataAdapter(sSql1, con)
                adap1.Fill(ds1)
            End If
            'MsgBox(ds1.Tables(0).Rows(0).Item(0))
            Dim IN_OUT As String = "I"
            If ds1.Tables(0).Rows(0).Item(0).ToString = 0 Then
                If IN_OUTDType = "B" Then
                    Dim ds2 As DataSet = New DataSet
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd") & "'"
                    If servername = "Access" Then
                        Dim adapAT As OleDbDataAdapter = New OleDbDataAdapter(sSql1, con1)
                        adapAT.Fill(ds2)
                    Else
                        sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd") & "'"
                        Dim adapT As SqlDataAdapter = New SqlDataAdapter(sSql1, con)
                        adapT.Fill(ds2)
                    End If
                    If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                        IN_OUT = "I"
                    Else
                        If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                            IN_OUT = "I"
                        Else
                            IN_OUT = "O"
                        End If
                    End If
                ElseIf IN_OUTDType = "I" Then
                    IN_OUT = "I"
                ElseIf IN_OUTDType = "O" Then
                    IN_OUT = "O"
                ElseIf IN_OUTDType = "D" Then
                    IN_OUT = ds.Tables(0).Rows(i).Item("ReasonCode").ToString.Trim
                End If
                sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                sSql1 = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                If servername = "Access" Then
                    Try
                        If con1.State <> ConnectionState.Open Then
                            con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, con1)
                        cmd1.ExecuteNonQuery()
                        cmd1 = New OleDbCommand(sSql1, con1)
                        cmd1.ExecuteNonQuery()
                        If con1.State <> ConnectionState.Closed Then
                            con1.Close()
                        End If
                    Catch
                        If con1.State <> ConnectionState.Closed Then
                            con1.Close()
                        End If
                    End Try
                Else
                    Try
                        If con.State <> ConnectionState.Open Then
                            con.Open()
                        End If
                        '"insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                        cmd = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand(sSql1, con)
                        cmd.ExecuteNonQuery()
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    Catch
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    End Try
                End If

                If Common.IsParallel = "Y" Then
                    Dim cardNo As String
                    If IsNumeric(ds.Tables(0).Rows(i).Item("CARDNO")) Then
                        cardNo = Convert.ToDouble(ds.Tables(0).Rows(i).Item("CARDNO"))
                    Else
                        cardNo = ds.Tables(0).Rows(i).Item("CARDNO")
                    End If
                    Common.parallelInsert(cardNo, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")), IN_OUT, "N", ds.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim, ID_NO)
                End If

                'sms
                If g_SMSApplicable = "" Then
                    Load_SMS_Policy()
                End If
                If g_SMSApplicable = "Y" Then
                    If g_isAllSMS = "Y" Then
                        Try
                            If ds.Tables(0).Rows(i).Item("TELEPHONE1").ToString.Trim <> "" Then
                                Dim SmsContent As String = ds.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim & " " & g_AllContent1 & " " & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm") & " " & g_AllContent2
                                sendSMS(ds.Tables(0).Rows(i).Item("TELEPHONE1").ToString.Trim, SmsContent)
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                End If
                'sms end
            End If
        Next

        If servername = "Access" Then
            If con1.State <> ConnectionState.Open Then
                con1.Open()
            End If
            cmd1 = New OleDbCommand("delete from Rawdata", con1)
            cmd1.ExecuteNonQuery()
            If con1.State <> ConnectionState.Closed Then
                con1.Close()
            End If
        Else
            If con.State <> ConnectionState.Open Then
                con.Open()
            End If
            cmd = New SqlCommand("delete from Rawdata", con)
            cmd.ExecuteNonQuery()
            If con.State <> ConnectionState.Closed Then
                con.Close()
            End If
        End If

        Dim paycodeArray = paycodelist.Distinct.ToArray
        If paycodeArray.Length > 0 Then
            'Load_Corporate_PolicySql()
            Dim mindate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Dim maxdate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Dim mindatetmp As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Do While mindate <= maxdate
                For i As Integer = 0 To paycodeArray.Length - 1
                    XtraMasterTest.LabelControlStatus.Text = "Removing Duplicate Punch for " & paycodeArray(i) & " " & mindate.ToString("yyyy-MM-dd")
                    Application.DoEvents()
                    Remove_Duplicate_Punches(mindate, paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                    mindate = mindate.AddDays(1)
                    If mindate > maxdate Then
                        Exit Do
                    End If
                Next
            Loop
            For i As Integer = 0 To paycodeArray.Length - 1
                XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & paycodeArray.Length
                Application.DoEvents()
                Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PAYCODE = '" & paycodeArray(i) & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSqltmp, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                        Process_AllRTC(mindatetmp.AddDays(-1), maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                    Else
                        Process_AllnonRTC(mindatetmp, maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                        If EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                            Process_AllnonRTCMulti(mindatetmp, maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                        End If
                    End If
                End If
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
            Next
            XtraMasterTest.LabelControlCount.Text = ""
            Application.DoEvents()
        End If
        Return "Success"
    End Function
    Public Function funcGetGeneralLogData_ATF686n(vnMachineNumber As Integer, vpszIPAddress As String, vpszNetPort As Integer, vnTimeOut As Long, vnProtocolType As Long, vpszNetPassword As Integer, vnLicense As Long, vnReadMark As Integer, clearLog As Boolean, A_R As String, Purpose As String, IN_OUTDType As String, ID_NO As String, datafile As String, CreateEmpMaster As Boolean) As String
        'Dim datafile As String = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"

        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet

        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds, ds1 As DataSet

        Dim vStrEnrollNumber As String
        Dim vSEnrollNumber As Double
        Dim vVerifyMode As Integer
        Dim vInOutMode As Integer
        Dim vdwDate As Date
        Dim vnCount As Integer
        Dim vnResultCode As Long ' Update 2008/12/18 by CCH
        Dim vstrFileName As String
        Dim vdBeginDate As Date = Date.MinValue
        Dim vdEndDate As Date = Date.MinValue
        Dim vstrTmp As String = ""
        'Dim vnFileNum As Integer
        Dim vstrFileData As String = ""
        Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
        'Dim vnReadMark As Integer
        'If A_R = "S" Then
        '    gnCommHandleIndex = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
        'Else
        gnCommHandleIndex = atf686n.ConnectNet(vpszIPAddress)
        'End If
        If gnCommHandleIndex > 0 Then
            'MsgBox("Connection success")
        Else
            Return "Connection fail"
            Exit Function
        End If
        vnResultCode = atf686n.ST_EnableDevice(gnCommHandleIndex, 0)
        If vnResultCode = False Then
            atf686n.ST_DisConnect(gnCommHandleIndex)
            Return gstrNoDevice
            Exit Function
        End If

        Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(xsSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(xsSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        'vnReadMark = 0    '1 for new log, 0 for all log
        vnResultCode = atf686n.ST_LoadGeneralLogData(gnCommHandleIndex, vnReadMark)
        If vnResultCode <> RUN_SUCCESS Then
            'MsgBox(ReturnResultPrint(vnResultCode))
        Else
            vnCount = 1
            XtraMasterTest.LabelControlStatus.Text = "Downloading from " & vpszIPAddress & "..."
            Application.DoEvents()
            If atf686n.ST_GetLogDataIsSupportStringID(gnCommHandleIndex) = RUN_SUCCESS Then
                Do
                    Dim MinOut As String
                    vStrEnrollNumber = Space(USER_ID_LEN)
                    vnResultCode = atf686n.ST_GetGeneralLogData_SID(gnCommHandleIndex, vStrEnrollNumber, vVerifyMode, vInOutMode, vdwDate)
                    Dim idwInOutMode
                    If vInOutMode = 1 Then
                        idwInOutMode = "0"
                    ElseIf vInOutMode = 2 Then
                        idwInOutMode = "1"
                    End If
                    If vnResultCode <> RUN_SUCCESS Then
                        If vnResultCode = RUNERR_DATAARRAY_END Then
                            vnResultCode = RUN_SUCCESS
                        End If
                        Exit Do
                    End If
                    Dim paycodelistForEmpMaster As New List(Of String)() ' for emp master
                    paycodelistForEmpMaster.Add(vStrEnrollNumber & ";")
                    If CreateEmpMaster = True Then
                        Dim payCodeArrEmpMaster() As String = paycodelistForEmpMaster.ToArray
                        Common.CreateEmployee(payCodeArrEmpMaster)
                    End If

                    Dim tmpstrEnrollNumber As String
                    If IsNumeric(vStrEnrollNumber) = True Then
                        tmpstrEnrollNumber = Convert.ToInt64(vStrEnrollNumber).ToString("000000000000")
                    Else
                        tmpstrEnrollNumber = Trim(vStrEnrollNumber)
                    End If

                    funcGetGeneralLogDataZK(tmpstrEnrollNumber, "", idwInOutMode, vdwDate.ToString("yyyy-MM-dd HH:mm:ss"), "", Purpose, IN_OUTDType, ID_NO, datafile)
                    vnCount = vnCount + 1
                Loop
            Else
                'Do
                '    Dim MinOut As String
                '    vnResultCode = F9.FK_GetGeneralLogData(gnCommHandleIndex, vSEnrollNumber, vVerifyMode, vInOutMode, vdwDate)

                '    Dim idwInOutMode
                '    If vInOutMode = 1 Then
                '        idwInOutMode = "0"
                '    ElseIf vInOutMode = 2 Then
                '        idwInOutMode = "1"
                '    End If
                '    If vnResultCode <> RUN_SUCCESS Then
                '        If vnResultCode = RUNERR_DATAARRAY_END Then
                '            vnResultCode = RUN_SUCCESS
                '        End If
                '        Exit Do
                '    End If
                '    Dim paycodelistForEmpMaster As New List(Of String)() ' for emp master
                '    paycodelistForEmpMaster.Add(vSEnrollNumber.ToString & ";")
                '    If CreateEmpMaster = True Then
                '        Dim payCodeArrEmpMaster() As String = paycodelistForEmpMaster.ToArray
                '        Common.CreateEmployee(payCodeArrEmpMaster)
                '    End If

                '    Dim tmpstrEnrollNumber As String
                '    If IsNumeric(vSEnrollNumber) = True Then
                '        tmpstrEnrollNumber = Convert.ToInt64(vSEnrollNumber).ToString("000000000000")
                '    Else
                '        tmpstrEnrollNumber = Trim(vSEnrollNumber)
                '    End If

                '    funcGetGeneralLogDataZK(tmpstrEnrollNumber, "", idwInOutMode, vdwDate.ToString("yyyy-MM-dd HH:mm:ss"), "", Purpose, IN_OUTDType, ID_NO, datafile)
                '    vnCount = vnCount + 1
                'Loop
            End If
            atf686n.ST_EnableDevice(gnCommHandleIndex, 1)
            Application.DoEvents()
            'MsgBox("Finish Downloading")
        End If
        If clearLog = True Then
            atf686n.ST_EmptyGeneralLogData(1)
        End If
        atf686n.ST_DisConnect(gnCommHandleIndex)
        Dim sSql As String = "select Rawdata.CARDNO, Rawdata.OFFICEPUNCH, TblEmployee.PAYCODE, TblEmployee.TELEPHONE1, TblEmployee.EMPNAME, Rawdata.ReasonCode, EmployeeGroup.Id  from Rawdata, TblEmployee, EmployeeGroup where Rawdata.CARDNO = TblEmployee.PRESENTCARDNO and TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId order by  Rawdata.OFFICEPUNCH"
        'Dim sSql As String = "select Rawdata.CARDNO, Rawdata.OFFICEPUNCH, TblEmployee.PAYCODE, TblEmployee.TELEPHONE1, TblEmployee.EMPNAME, Rawdata.ReasonCode  from Rawdata, TblEmployee where Rawdata.CARDNO = TblEmployee.PRESENTCARDNO order by  Rawdata.OFFICEPUNCH"
        ds = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If

        Dim sSql1 As String
        Dim paycodelist As New List(Of String)()

        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            XtraMasterTest.LabelControlStatus.Text = "Validating Punch " & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & " " & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss")
            Application.DoEvents()
            paycodelist.Add(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim)
            sSql1 = "select count (MachineRawPunch.CARDNO) from MachineRawPunch, Rawdata where Rawdata.CARDNO = MachineRawPunch.CARDNO and (MachineRawPunch.CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and MachineRawPunch.OFFICEPUNCH = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "')"
            ds1 = New DataSet
            If servername = "Access" Then
                sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd HH:mm:ss') = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                adapA1 = New OleDbDataAdapter(sSql1, con1)
                adapA1.Fill(ds1)
            Else
                adap1 = New SqlDataAdapter(sSql1, con)
                adap1.Fill(ds1)
            End If
            'MsgBox(ds1.Tables(0).Rows(0).Item(0))
            Dim IN_OUT As String = "I"
            If ds1.Tables(0).Rows(0).Item(0).ToString = 0 Then
                If IN_OUTDType = "B" Then
                    Dim ds2 As DataSet = New DataSet
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd") & "'"
                    If servername = "Access" Then
                        Dim adapAT As OleDbDataAdapter = New OleDbDataAdapter(sSql1, con1)
                        adapAT.Fill(ds2)
                    Else
                        sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd") & "'"
                        Dim adapT As SqlDataAdapter = New SqlDataAdapter(sSql1, con)
                        adapT.Fill(ds2)
                    End If
                    If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                        IN_OUT = "I"
                    Else
                        If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                            IN_OUT = "I"
                        Else
                            IN_OUT = "O"
                        End If
                    End If
                ElseIf IN_OUTDType = "I" Then
                    IN_OUT = "I"
                ElseIf IN_OUTDType = "O" Then
                    IN_OUT = "O"
                ElseIf IN_OUTDType = "D" Then
                    IN_OUT = ds.Tables(0).Rows(i).Item("ReasonCode").ToString.Trim
                End If
                sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                sSql1 = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                If servername = "Access" Then
                    Try
                        If con1.State <> ConnectionState.Open Then
                            con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, con1)
                        cmd1.ExecuteNonQuery()
                        cmd1 = New OleDbCommand(sSql1, con1)
                        cmd1.ExecuteNonQuery()
                        If con1.State <> ConnectionState.Closed Then
                            con1.Close()
                        End If
                    Catch
                        If con1.State <> ConnectionState.Closed Then
                            con1.Close()
                        End If
                    End Try
                Else
                    Try
                        If con.State <> ConnectionState.Open Then
                            con.Open()
                        End If
                        '"insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                        cmd = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand(sSql1, con)
                        cmd.ExecuteNonQuery()
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    Catch
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    End Try
                End If

                If Common.IsParallel = "Y" Then
                    Dim cardNo As String
                    If IsNumeric(ds.Tables(0).Rows(i).Item("CARDNO")) Then
                        cardNo = Convert.ToDouble(ds.Tables(0).Rows(i).Item("CARDNO"))
                    Else
                        cardNo = ds.Tables(0).Rows(i).Item("CARDNO")
                    End If
                    Common.parallelInsert(cardNo, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")), IN_OUT, "N", ds.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim, ID_NO)
                End If

                'sms
                If g_SMSApplicable = "" Then
                    Load_SMS_Policy()
                End If
                If g_SMSApplicable = "Y" Then
                    If g_isAllSMS = "Y" Then
                        Try
                            If ds.Tables(0).Rows(i).Item("TELEPHONE1").ToString.Trim <> "" Then
                                Dim SmsContent As String = ds.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim & " " & g_AllContent1 & " " & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm") & " " & g_AllContent2
                                sendSMS(ds.Tables(0).Rows(i).Item("TELEPHONE1").ToString.Trim, SmsContent)
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                End If
                'sms end
            End If
        Next

        If servername = "Access" Then
            If con1.State <> ConnectionState.Open Then
                con1.Open()
            End If
            cmd1 = New OleDbCommand("delete from Rawdata", con1)
            cmd1.ExecuteNonQuery()
            If con1.State <> ConnectionState.Closed Then
                con1.Close()
            End If
        Else
            If con.State <> ConnectionState.Open Then
                con.Open()
            End If
            cmd = New SqlCommand("delete from Rawdata", con)
            cmd.ExecuteNonQuery()
            If con.State <> ConnectionState.Closed Then
                con.Close()
            End If
        End If

        Dim paycodeArray = paycodelist.Distinct.ToArray
        If paycodeArray.Length > 0 Then
            'Load_Corporate_PolicySql()
            Dim mindate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Dim maxdate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Dim mindatetmp As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Do While mindate <= maxdate
                For i As Integer = 0 To paycodeArray.Length - 1
                    XtraMasterTest.LabelControlStatus.Text = "Removing Duplicate Punch for " & paycodeArray(i) & " " & mindate.ToString("yyyy-MM-dd")
                    Application.DoEvents()
                    Remove_Duplicate_Punches(mindate, paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                    mindate = mindate.AddDays(1)
                    If mindate > maxdate Then
                        Exit Do
                    End If
                Next
            Loop
            For i As Integer = 0 To paycodeArray.Length - 1
                XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & paycodeArray.Length
                Application.DoEvents()
                Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PAYCODE = '" & paycodeArray(i) & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSqltmp, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                        Process_AllRTC(mindatetmp.AddDays(-1), maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                    Else
                        Process_AllnonRTC(mindatetmp, maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                        If EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                            Process_AllnonRTCMulti(mindatetmp, maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                        End If
                    End If
                End If
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
            Next
            XtraMasterTest.LabelControlCount.Text = ""
            Application.DoEvents()
        End If
        Return "Success"
    End Function
    Public Function DisableDevice(vnMachineNumber As Integer, AxFP_CLOCK1 As AxFP_CLOCKLib.AxFP_CLOCK) As Boolean

        Dim bRet As Boolean = AxFP_CLOCK1.EnableDevice(vnMachineNumber, 0)
        If bRet Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Structure GeneralLogInfo
        Public dwTMachineNumber As Integer
        Public dwEnrollNumber As Integer
        Public dwEMachineNumber As Integer
        Public dwVerifyMode As Integer
        Public dwInout As Integer
        Public dwEvent As Integer
        Public dwYear As Integer
        Public dwMonth As Integer
        Public dwDay As Integer
        Public dwHour As Integer
        Public dwMinute As Integer
        Public dwSecond As Integer
    End Structure


    Public Function funcGetGeneralLogData_ZKController(vnMachineNumber As Integer, vpszIPAddress As String, vpszNetPort As Integer, vnTimeOut As Long, vnProtocolType As Long, vpszNetPassword As Integer, vnLicense As Long, vnReadMark As Integer, clearLog As Boolean, A_R As String, Purpose As String, IN_OUTDType As String, ID_NO As String, datafile As String, CreateEmpMaster As Boolean, AxFP_CLOCK1 As AxFP_CLOCKLib.AxFP_CLOCK) As String
        Dim Rs As DataSet = New DataSet
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds, ds1 As DataSet
        Dim vnCount As Integer
        Dim vnResultCode As Long ' Update 2008/12/18 by CCH
        Dim vdBeginDate As Date = Date.MinValue
        Dim vdEndDate As Date = Date.MinValue
        Dim vstrTmp As String = ""
        Dim vstrFileData As String = ""
        Dim bRet As Boolean
        AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
        bRet = AxFP_CLOCK1.SetIPAddress(vpszIPAddress, vpszNetPort, vpszNetPassword)
        If Not bRet Then
            Return "Connection fail"
            Exit Function

        Else
            'Log Download Code
            bRet = AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
            bRet = DisableDevice(vnMachineNumber, AxFP_CLOCK1)
            If bRet Then
                If vnReadMark = 1 Then
                    bRet = AxFP_CLOCK1.ReadGeneralLogData(vnMachineNumber)
                Else
                    bRet = AxFP_CLOCK1.ReadAllGLogData(vnMachineNumber)
                End If
                If Not bRet Then
                    Return "Unable To Download"
                    Exit Function
                End If
                Dim gLogInfo As New GeneralLogInfo()
                While (bRet)
                    bRet = AxFP_CLOCK1.GetGeneralLogDataWithSecond(vnMachineNumber, gLogInfo.dwTMachineNumber, gLogInfo.dwEnrollNumber, gLogInfo.dwEMachineNumber, gLogInfo.dwVerifyMode, gLogInfo.dwInout, gLogInfo.dwEvent, gLogInfo.dwYear, gLogInfo.dwMonth, gLogInfo.dwDay, gLogInfo.dwHour, gLogInfo.dwMinute, gLogInfo.dwSecond)
                    If bRet Then
                        Dim tmpstrEnrollNumber As String = gLogInfo.dwEnrollNumber
                        If IsNumeric(tmpstrEnrollNumber) = True Then
                            tmpstrEnrollNumber = Convert.ToInt64(tmpstrEnrollNumber).ToString("000000000000")
                        Else
                            tmpstrEnrollNumber = Trim(tmpstrEnrollNumber)
                        End If
                        Dim punchdate = gLogInfo.dwYear.ToString() & "-" + gLogInfo.dwMonth.ToString("00") & "-" & gLogInfo.dwDay.ToString("00") & " " & gLogInfo.dwHour.ToString("00") & ":" & gLogInfo.dwMinute.ToString("00") & ":" & gLogInfo.dwSecond.ToString("00")
                        If CreateEmpMaster = True Then
                            Dim paycodelistForEmpMaster As New List(Of String)() ' for emp master
                            paycodelistForEmpMaster.Add(tmpstrEnrollNumber & ";")
                            Dim payCodeArrEmpMaster() As String = paycodelistForEmpMaster.ToArray
                            Common.CreateEmployee(payCodeArrEmpMaster)
                        End If
                        funcGetGeneralLogDataZK(tmpstrEnrollNumber, "", gLogInfo.dwInout, punchdate, "", Purpose, IN_OUTDType, ID_NO, datafile)
                        vnCount = vnCount + 1

                    End If

                End While

            End If
        End If

        Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(xsSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(xsSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        'If vnResultCode <> RUN_SUCCESS Then
        '    'MsgBox(ReturnResultPrint(vnResultCode))
        'Else
        '    vnCount = 1
        XtraMasterTest.LabelControlStatus.Text = "Downloading from " & vpszIPAddress & "..."
        Application.DoEvents()

        'End If
        If clearLog = True Then
            bRet = AxFP_CLOCK1.EmptyGeneralLogData(vnMachineNumber)
        End If
        AxFP_CLOCK1.EnableDevice(vnMachineNumber, 1)
        Dim sSql As String = "select Rawdata.CARDNO, Rawdata.OFFICEPUNCH, TblEmployee.PAYCODE, TblEmployee.TELEPHONE1, TblEmployee.EMPNAME, Rawdata.ReasonCode, EmployeeGroup.Id  from Rawdata, TblEmployee, EmployeeGroup where Rawdata.CARDNO = TblEmployee.PRESENTCARDNO and TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId order by  Rawdata.OFFICEPUNCH"
        ds = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If

        Dim sSql1 As String
        Dim paycodelist As New List(Of String)()

        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            XtraMasterTest.LabelControlStatus.Text = "Validating Punch " & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & " " & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss")
            Application.DoEvents()
            paycodelist.Add(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim)
            sSql1 = "select count (MachineRawPunch.CARDNO) from MachineRawPunch, Rawdata where Rawdata.CARDNO = MachineRawPunch.CARDNO and (MachineRawPunch.CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and MachineRawPunch.OFFICEPUNCH = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "')"
            ds1 = New DataSet
            If servername = "Access" Then
                sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd HH:mm:ss') = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                adapA1 = New OleDbDataAdapter(sSql1, con1)
                adapA1.Fill(ds1)
            Else
                adap1 = New SqlDataAdapter(sSql1, con)
                adap1.Fill(ds1)
            End If
            'MsgBox(ds1.Tables(0).Rows(0).Item(0))
            Dim IN_OUT As String = "I"
            If ds1.Tables(0).Rows(0).Item(0).ToString = 0 Then
                If IN_OUTDType = "B" Then
                    Dim ds2 As DataSet = New DataSet
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd") & "'"
                    If servername = "Access" Then
                        Dim adapAT As OleDbDataAdapter = New OleDbDataAdapter(sSql1, con1)
                        adapAT.Fill(ds2)
                    Else
                        sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd") & "'"
                        Dim adapT As SqlDataAdapter = New SqlDataAdapter(sSql1, con)
                        adapT.Fill(ds2)
                    End If
                    If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                        IN_OUT = "I"
                    Else
                        If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                            IN_OUT = "I"
                        Else
                            IN_OUT = "O"
                        End If
                    End If
                ElseIf IN_OUTDType = "I" Then
                    IN_OUT = "I"
                ElseIf IN_OUTDType = "O" Then
                    IN_OUT = "O"
                ElseIf IN_OUTDType = "D" Then
                    IN_OUT = ds.Tables(0).Rows(i).Item("ReasonCode").ToString.Trim
                End If
                sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                sSql1 = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                If servername = "Access" Then
                    Try
                        If con1.State <> ConnectionState.Open Then
                            con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, con1)
                        cmd1.ExecuteNonQuery()
                        cmd1 = New OleDbCommand(sSql1, con1)
                        cmd1.ExecuteNonQuery()
                        If con1.State <> ConnectionState.Closed Then
                            con1.Close()
                        End If
                    Catch
                        If con1.State <> ConnectionState.Closed Then
                            con1.Close()
                        End If
                    End Try
                Else
                    Try
                        If con.State <> ConnectionState.Open Then
                            con.Open()
                        End If
                        '"insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                        cmd = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand(sSql1, con)
                        cmd.ExecuteNonQuery()
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    Catch
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    End Try
                End If

                If Common.IsParallel = "Y" Then
                    Dim cardNo As String
                    If IsNumeric(ds.Tables(0).Rows(i).Item("CARDNO")) Then
                        cardNo = Convert.ToDouble(ds.Tables(0).Rows(i).Item("CARDNO"))
                    Else
                        cardNo = ds.Tables(0).Rows(i).Item("CARDNO")
                    End If
                    Common.parallelInsert(cardNo, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")), IN_OUT, "N", ds.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim, ID_NO)
                End If

                'sms
                If g_SMSApplicable = "" Then
                    Load_SMS_Policy()
                End If
                If g_SMSApplicable = "Y" Then
                    If g_isAllSMS = "Y" Then
                        Try
                            If ds.Tables(0).Rows(i).Item("TELEPHONE1").ToString.Trim <> "" Then
                                Dim SmsContent As String = ds.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim & " " & g_AllContent1 & " " & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm") & " " & g_AllContent2
                                sendSMS(ds.Tables(0).Rows(i).Item("TELEPHONE1").ToString.Trim, SmsContent)
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                End If
                'sms end
            End If
        Next

        If servername = "Access" Then
            If con1.State <> ConnectionState.Open Then
                con1.Open()
            End If
            cmd1 = New OleDbCommand("delete from Rawdata", con1)
            cmd1.ExecuteNonQuery()
            If con1.State <> ConnectionState.Closed Then
                con1.Close()
            End If
        Else
            If con.State <> ConnectionState.Open Then
                con.Open()
            End If
            cmd = New SqlCommand("delete from Rawdata", con)
            cmd.ExecuteNonQuery()
            If con.State <> ConnectionState.Closed Then
                con.Close()
            End If
        End If

        Dim paycodeArray = paycodelist.Distinct.ToArray
        If paycodeArray.Length > 0 Then
            'Load_Corporate_PolicySql()
            Dim mindate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Dim maxdate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Dim mindatetmp As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Do While mindate <= maxdate
                For i As Integer = 0 To paycodeArray.Length - 1
                    XtraMasterTest.LabelControlStatus.Text = "Removing Duplicate Punch for " & paycodeArray(i) & " " & mindate.ToString("yyyy-MM-dd")
                    Application.DoEvents()
                    Remove_Duplicate_Punches(mindate, paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                    mindate = mindate.AddDays(1)
                    If mindate > maxdate Then
                        Exit Do
                    End If
                Next
            Loop
            For i As Integer = 0 To paycodeArray.Length - 1
                XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & paycodeArray.Length
                Application.DoEvents()
                Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PAYCODE = '" & paycodeArray(i) & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSqltmp, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                        Process_AllRTC(mindatetmp.AddDays(-1), maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                    Else
                        Process_AllnonRTC(mindatetmp, maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                        If EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                            Process_AllnonRTCMulti(mindatetmp, maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                        End If
                    End If
                End If
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
            Next
            XtraMasterTest.LabelControlCount.Text = ""
            Application.DoEvents()
        End If
        AxFP_CLOCK1.EnableDevice(vnMachineNumber, 1)
        AxFP_CLOCK1.CloseCommPort()

        Return "Success"
    End Function

    Public Function funcGetGeneralLogData_TF01(vnMachineNumber As Integer, vpszIPAddress As String, vpszNetPort As Integer, vnTimeOut As Long, vnProtocolType As Long, vpszNetPassword As Integer, vnLicense As Long, vnReadMark As Integer, clearLog As Boolean, A_R As String, Purpose As String, IN_OUTDType As String, ID_NO As String, datafile As String, CreateEmpMaster As Boolean, AxFP_CLOCK1 As AxFP_CLOCKLib.AxFP_CLOCK) As String
        Dim Rs As DataSet = New DataSet
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds, ds1 As DataSet
        Dim vnCount As Integer
        Dim vnResultCode As Long ' Update 2008/12/18 by CCH
        Dim vdBeginDate As Date = Date.MinValue
        Dim vdEndDate As Date = Date.MinValue
        Dim vstrTmp As String = ""
        Dim vstrFileData As String = ""
        Dim bRet As Boolean
        AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
        bRet = AxFP_CLOCK1.SetIPAddress(vpszIPAddress, vpszNetPort, vpszNetPassword)
        If Not bRet Then
            Return "Connection fail"
            Exit Function

        Else
            'Log Download Code
            bRet = AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
            bRet = DisableDevice(vnMachineNumber, AxFP_CLOCK1)
            If bRet Then
                If vnReadMark = 1 Then
                    bRet = AxFP_CLOCK1.ReadGeneralLogData(vnMachineNumber)
                Else
                    bRet = AxFP_CLOCK1.ReadAllGLogData(vnMachineNumber)
                End If
                If Not bRet Then
                    Return "Unable To Download"
                    Exit Function
                End If
                Dim gLogInfo As New GeneralLogInfo()
                While (bRet)
                    bRet = AxFP_CLOCK1.GetGeneralLogDataWithSecond(vnMachineNumber, gLogInfo.dwTMachineNumber, gLogInfo.dwEnrollNumber, gLogInfo.dwEMachineNumber, gLogInfo.dwVerifyMode, gLogInfo.dwInout, gLogInfo.dwEvent, gLogInfo.dwYear, gLogInfo.dwMonth, gLogInfo.dwDay, gLogInfo.dwHour, gLogInfo.dwMinute, gLogInfo.dwSecond)
                    If bRet Then
                        Dim tmpstrEnrollNumber As String = gLogInfo.dwEnrollNumber
                        If IsNumeric(tmpstrEnrollNumber) = True Then
                            tmpstrEnrollNumber = Convert.ToInt64(tmpstrEnrollNumber).ToString("000000000000")
                        Else
                            tmpstrEnrollNumber = Trim(tmpstrEnrollNumber)
                        End If
                        Dim punchdate = gLogInfo.dwYear.ToString() & "-" + gLogInfo.dwMonth.ToString("00") & "-" & gLogInfo.dwDay.ToString("00") & " " & gLogInfo.dwHour.ToString("00") & ":" & gLogInfo.dwMinute.ToString("00") & ":" & gLogInfo.dwSecond.ToString("00")
                        If CreateEmpMaster = True Then
                            Dim paycodelistForEmpMaster As New List(Of String)() ' for emp master
                            paycodelistForEmpMaster.Add(tmpstrEnrollNumber & ";")
                            Dim payCodeArrEmpMaster() As String = paycodelistForEmpMaster.ToArray
                            Common.CreateEmployee(payCodeArrEmpMaster)
                        End If
                        funcGetGeneralLogDataZK(tmpstrEnrollNumber, "", gLogInfo.dwInout, punchdate, "", Purpose, IN_OUTDType, ID_NO, datafile)
                        vnCount = vnCount + 1

                    End If

                End While

            End If
        End If

        Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(xsSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(xsSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        'If vnResultCode <> RUN_SUCCESS Then
        '    'MsgBox(ReturnResultPrint(vnResultCode))
        'Else
        '    vnCount = 1
        XtraMasterTest.LabelControlStatus.Text = "Downloading from " & vpszIPAddress & "..."
        Application.DoEvents()

        'End If
        If clearLog = True Then
            bRet = AxFP_CLOCK1.EmptyGeneralLogData(vnMachineNumber)
        End If
        AxFP_CLOCK1.EnableDevice(vnMachineNumber, 1)
        Dim sSql As String = "select Rawdata.CARDNO, Rawdata.OFFICEPUNCH, TblEmployee.PAYCODE, TblEmployee.TELEPHONE1, TblEmployee.EMPNAME, Rawdata.ReasonCode, EmployeeGroup.Id  from Rawdata, TblEmployee, EmployeeGroup where Rawdata.CARDNO = TblEmployee.PRESENTCARDNO and TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId order by  Rawdata.OFFICEPUNCH"
        ds = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If

        Dim sSql1 As String
        Dim paycodelist As New List(Of String)()

        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            XtraMasterTest.LabelControlStatus.Text = "Validating Punch " & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & " " & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss")
            Application.DoEvents()
            paycodelist.Add(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim)
            sSql1 = "select count (MachineRawPunch.CARDNO) from MachineRawPunch, Rawdata where Rawdata.CARDNO = MachineRawPunch.CARDNO and (MachineRawPunch.CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and MachineRawPunch.OFFICEPUNCH = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "')"
            ds1 = New DataSet
            If servername = "Access" Then
                sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd HH:mm:ss') = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                adapA1 = New OleDbDataAdapter(sSql1, con1)
                adapA1.Fill(ds1)
            Else
                adap1 = New SqlDataAdapter(sSql1, con)
                adap1.Fill(ds1)
            End If
            'MsgBox(ds1.Tables(0).Rows(0).Item(0))
            Dim IN_OUT As String = "I"
            If ds1.Tables(0).Rows(0).Item(0).ToString = 0 Then
                If IN_OUTDType = "B" Then
                    Dim ds2 As DataSet = New DataSet
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd") & "'"
                    If servername = "Access" Then
                        Dim adapAT As OleDbDataAdapter = New OleDbDataAdapter(sSql1, con1)
                        adapAT.Fill(ds2)
                    Else
                        sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd") & "'"
                        Dim adapT As SqlDataAdapter = New SqlDataAdapter(sSql1, con)
                        adapT.Fill(ds2)
                    End If
                    If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                        IN_OUT = "I"
                    Else
                        If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                            IN_OUT = "I"
                        Else
                            IN_OUT = "O"
                        End If
                    End If
                ElseIf IN_OUTDType = "I" Then
                    IN_OUT = "I"
                ElseIf IN_OUTDType = "O" Then
                    IN_OUT = "O"
                ElseIf IN_OUTDType = "D" Then
                    IN_OUT = ds.Tables(0).Rows(i).Item("ReasonCode").ToString.Trim
                End If
                sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                sSql1 = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                If servername = "Access" Then
                    Try
                        If con1.State <> ConnectionState.Open Then
                            con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, con1)
                        cmd1.ExecuteNonQuery()
                        cmd1 = New OleDbCommand(sSql1, con1)
                        cmd1.ExecuteNonQuery()
                        If con1.State <> ConnectionState.Closed Then
                            con1.Close()
                        End If
                    Catch
                        If con1.State <> ConnectionState.Closed Then
                            con1.Close()
                        End If
                    End Try
                Else
                    Try
                        If con.State <> ConnectionState.Open Then
                            con.Open()
                        End If
                        '"insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & ds.Tables(0).Rows(i).Item("CARDNO") & "','" & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & "' ) "
                        cmd = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        cmd = New SqlCommand(sSql1, con)
                        cmd.ExecuteNonQuery()
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    Catch
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    End Try
                End If

                If Common.IsParallel = "Y" Then
                    Dim cardNo As String
                    If IsNumeric(ds.Tables(0).Rows(i).Item("CARDNO")) Then
                        cardNo = Convert.ToDouble(ds.Tables(0).Rows(i).Item("CARDNO"))
                    Else
                        cardNo = ds.Tables(0).Rows(i).Item("CARDNO")
                    End If
                    Common.parallelInsert(cardNo, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")), IN_OUT, "N", ds.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim, ID_NO)
                End If

                'sms
                If g_SMSApplicable = "" Then
                    Load_SMS_Policy()
                End If
                If g_SMSApplicable = "Y" Then
                    If g_isAllSMS = "Y" Then
                        Try
                            If ds.Tables(0).Rows(i).Item("TELEPHONE1").ToString.Trim <> "" Then
                                Dim SmsContent As String = ds.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim & " " & g_AllContent1 & " " & Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd HH:mm") & " " & g_AllContent2
                                sendSMS(ds.Tables(0).Rows(i).Item("TELEPHONE1").ToString.Trim, SmsContent)
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                End If
                'sms end
            End If
        Next

        If servername = "Access" Then
            If con1.State <> ConnectionState.Open Then
                con1.Open()
            End If
            cmd1 = New OleDbCommand("delete from Rawdata", con1)
            cmd1.ExecuteNonQuery()
            If con1.State <> ConnectionState.Closed Then
                con1.Close()
            End If
        Else
            If con.State <> ConnectionState.Open Then
                con.Open()
            End If
            cmd = New SqlCommand("delete from Rawdata", con)
            cmd.ExecuteNonQuery()
            If con.State <> ConnectionState.Closed Then
                con.Close()
            End If
        End If

        Dim paycodeArray = paycodelist.Distinct.ToArray
        If paycodeArray.Length > 0 Then
            'Load_Corporate_PolicySql()
            Dim mindate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Dim maxdate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Dim mindatetmp As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            Do While mindate <= maxdate
                For i As Integer = 0 To paycodeArray.Length - 1
                    XtraMasterTest.LabelControlStatus.Text = "Removing Duplicate Punch for " & paycodeArray(i) & " " & mindate.ToString("yyyy-MM-dd")
                    Application.DoEvents()
                    Remove_Duplicate_Punches(mindate, paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                    mindate = mindate.AddDays(1)
                    If mindate > maxdate Then
                        Exit Do
                    End If
                Next
            Loop
            For i As Integer = 0 To paycodeArray.Length - 1
                XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & paycodeArray.Length
                Application.DoEvents()
                Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PAYCODE = '" & paycodeArray(i) & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSqltmp, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                        Process_AllRTC(mindatetmp.AddDays(-1), maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                    Else
                        Process_AllnonRTC(mindatetmp, maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                        If EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                            Process_AllnonRTCMulti(mindatetmp, maxdate.Date, paycodeArray(i), paycodeArray(i), ds.Tables(0).Rows(0).Item("Id"))
                        End If
                    End If
                End If
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
            Next
            XtraMasterTest.LabelControlCount.Text = ""
            Application.DoEvents()
        End If
        AxFP_CLOCK1.EnableDevice(vnMachineNumber, 1)
        AxFP_CLOCK1.CloseCommPort()

        Return "Success"
    End Function

    Public Function funcGetGeneralLogDataZK(sdwEnrollNumber As String, idwVerifyMode As String, idwInOutMode As String, punchdate As String, idwWorkcode As String, Purpose As String, IN_OUT As String, ID_NO As String, datafile As String)
        Dim ds As DataSet = New DataSet
        Dim CARDNO As String = sdwEnrollNumber
        If IsNumeric(CARDNO) Then
            CARDNO = Convert.ToInt64(CARDNO).ToString("000000000000")
        End If
        Dim OFFICEPUNCH As String = Convert.ToDateTime(punchdate).ToString("yyyy-MM-dd HH:mm:ss")

        XtraMasterTest.LabelControlStatus.Text = "Downloading " & CARDNO & " " & Convert.ToDateTime(punchdate).ToString("yyyy-MM-dd HH:mm:ss")
        Application.DoEvents()

        'Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
        'If Common.servername = "Access" Then
        '    If Common.con1.State <> ConnectionState.Open Then
        '        Common.con1.Open()
        '    End If
        '    cmd1 = New OleDbCommand(xsSql, Common.con1)
        '    cmd1.ExecuteNonQuery()
        '    If Common.con1.State <> ConnectionState.Closed Then
        '        Common.con1.Close()
        '    End If
        'Else
        '    If Common.con.State <> ConnectionState.Open Then
        '        Common.con.Open()
        '    End If
        '    cmd = New SqlCommand(xsSql, Common.con)
        '    cmd.ExecuteNonQuery()
        '    If Common.con.State <> ConnectionState.Closed Then
        '        Common.con.Close()
        '    End If
        'End If

        If Purpose = "C" Then
            Dim sSqlX As String = "select * from tblTimeSlab "
            Dim adapX As SqlDataAdapter
            Dim adapAX As OleDbDataAdapter
            Dim dsX As DataSet = New DataSet
            If Common.servername = "Access" Then
                adapAX = New OleDbDataAdapter(sSqlX, Common.con1)
                adapAX.Fill(dsX)
            Else
                adapX = New SqlDataAdapter(sSqlX, Common.con)
                adapX.Fill(dsX)
            End If
            Dim shift As String
            Dim PURPOSECan As String = "N"
            If dsX.Tables(0).Rows.Count > 0 Then
                Dim BStart As DateTime
                Dim BEnd As DateTime
                For i As Integer = 0 To dsX.Tables(0).Rows.Count - 1
                    BStart = Convert.ToDateTime(Convert.ToDateTime(punchdate).ToString("yyyy-MM-dd ") & Convert.ToDateTime(dsX.Tables(0).Rows(i).Item("BStart").ToString.Trim).ToString("HH:mm:00"))
                    BEnd = Convert.ToDateTime(Convert.ToDateTime(punchdate).ToString("yyyy-MM-dd ") & Convert.ToDateTime(dsX.Tables(0).Rows(i).Item("BEnd").ToString.Trim).ToString("HH:mm:00"))
                    If Convert.ToDateTime(punchdate) >= BStart And Convert.ToDateTime(punchdate) <= BEnd Then
                        PURPOSECan = dsX.Tables(0).Rows(i).Item("MealType").ToString.Trim '"B"
                        shift = dsX.Tables(0).Rows(i).Item("shift").ToString.Trim
                        Exit For
                    End If
                Next
            End If
            Dim sSqlC As String = "insert into TBLCANTEEN(CARDNO, OFFICEPUNCH, ISMANUAL, SHIFT, PURPOSE, ISVEG, WorkCode) VALUES('" & CARDNO & "','" & OFFICEPUNCH & "','N', '" & shift & "', '" & PURPOSECan & "','Y','" & idwWorkcode & "')"
            If servername = "Access" Then
                If con1.State <> ConnectionState.Open Then
                    con1.Open()
                End If
                Try
                    cmd1 = New OleDbCommand(sSqlC, con1)
                    cmd1.ExecuteNonQuery()
                Catch
                    con1.Close()
                End Try
                con1.Close()
            Else
                If con.State <> ConnectionState.Open Then
                    con.Open()
                End If
                Try
                    cmd = New SqlCommand(sSqlC, con)
                    cmd.ExecuteNonQuery()
                Catch
                    con.Close()
                End Try
                con.Close()
            End If
            Exit Function
        End If

        'insert in text file
        Dim fs As FileStream = New FileStream(datafile, FileMode.OpenOrCreate, FileAccess.Write)
        Dim sw As StreamWriter = New StreamWriter(fs)
        'find the end of the underlying filestream
        sw.BaseStream.Seek(0, SeekOrigin.End)
        sw.WriteLine(CARDNO & " " & OFFICEPUNCH & " " & ID_NO)
        sw.Flush()
        sw.Close()
        'end insert in text file

        If servername = "Access" Then
            If con1.State <> ConnectionState.Open Then
                con1.Open()
            End If
        Else
            If con.State <> ConnectionState.Open Then
                con.Open()
            End If
        End If
        Dim sSql As String = "insert into Rawdata (CARDNO,OFFICEPUNCH,Error_Code,DOOR_TIME,PROCESS,Id_No) values('" & CARDNO & "','" & OFFICEPUNCH & "','00', '00','N','" & ID_NO & "' ) "
        Dim sSql1 As String
        ds = New DataSet
        If servername = "Access" Then
            Try
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
            Catch
            End Try
            sSql1 = "select Rawdata.CARDNO, Rawdata.OFFICEPUNCH, TblEmployee.PAYCODE, TblEmployee.TELEPHONE1, TblEmployee.EMPNAME from Rawdata, TblEmployee where Rawdata.CARDNO = TblEmployee.PRESENTCARDNO and Rawdata.CARDNO = '" & CARDNO & "'"
            adapA = New OleDbDataAdapter(sSql1, con1)
            adapA.Fill(ds)
        Else
            Try
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
            Catch ex As Exception
            End Try
            sSql1 = "select Rawdata.CARDNO, Rawdata.OFFICEPUNCH, TblEmployee.PAYCODE, TblEmployee.TELEPHONE1, TblEmployee.EMPNAME  from Rawdata, TblEmployee where Rawdata.CARDNO = TblEmployee.PRESENTCARDNO and Rawdata.CARDNO = '" & CARDNO & "'"
            adap = New SqlDataAdapter(sSql1, con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            If IN_OUT = "B" Then
                Dim ds2 As DataSet = New DataSet
                If servername = "Access" Then
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"
                    Dim adapAT As OleDbDataAdapter = New OleDbDataAdapter(sSql1, con1)
                    adapAT.Fill(ds2)
                Else
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"
                    Dim adapT As SqlDataAdapter = New SqlDataAdapter(sSql1, con)
                    adapT.Fill(ds2)
                End If
                If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                    IN_OUT = "I"
                Else
                    If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                        IN_OUT = "I"
                    Else
                        IN_OUT = "O"
                    End If
                End If
            ElseIf IN_OUT = "D" Then
                If idwInOutMode = "0" Then
                    IN_OUT = "I"
                ElseIf idwInOutMode = "1" Then
                    IN_OUT = "O"
                End If
            End If


            If idwVerifyMode = "0" Then
                idwVerifyMode = "PIN"
            ElseIf idwVerifyMode = "1" Then
                idwVerifyMode = "Fp"
            ElseIf idwVerifyMode = "2" Then
                idwVerifyMode = "Card"
            ElseIf idwVerifyMode = "3" Then
                idwVerifyMode = "PIN & PW"
            ElseIf idwVerifyMode = "4" Then
                idwVerifyMode = "PIN & FP"
            ElseIf idwVerifyMode = "5" Then
                idwVerifyMode = "FP & (PIN & PW)"
            ElseIf idwVerifyMode = "6" Then
                idwVerifyMode = "FP & Card"
            ElseIf idwVerifyMode = "7" Then
                idwVerifyMode = "(PIN & PW) & Card"
            ElseIf idwVerifyMode = "8" Then
                idwVerifyMode = "FP & PW & Card"
            ElseIf idwVerifyMode = "9" Then
                idwVerifyMode = "(PIN & PW) & FP"
            ElseIf idwVerifyMode = "101" Then
                idwVerifyMode = "Slave Device"
            ElseIf idwVerifyMode = "15" Then
                idwVerifyMode = "Face"
            End If

            sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[VerifyMode],[DownloadedTime]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','" & idwWorkcode & "','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "','" & idwVerifyMode & "','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "' ) "
            sSql1 = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[VerifyMode],[DownloadedTime]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','" & idwWorkcode & "','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "','" & idwVerifyMode & "','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "' ) "
            Try
                If servername = "Access" Then
                    cmd1 = New OleDbCommand(sSql, con1)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand(sSql1, con1)
                    cmd1.ExecuteNonQuery()
                Else
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(sSql1, con)
                    cmd.ExecuteNonQuery()
                End If
            Catch ex As Exception

            End Try


            If servername = "Access" Then
                If con1.State <> ConnectionState.Closed Then
                    con1.Close()
                End If
            Else
                If con.State <> ConnectionState.Closed Then
                    con.Close()
                End If
            End If

            If Common.IsParallel = "Y" Then
                Common.parallelInsert(sdwEnrollNumber, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, Convert.ToDateTime(punchdate), IN_OUT, "N", ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim, ID_NO)
            End If

            'sms
            If g_SMSApplicable = "" Then
                Load_SMS_Policy()
            End If
            If g_SMSApplicable = "Y" Then
                If g_isAllSMS = "Y" Then
                    Try
                        If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then
                            Dim SmsContent As String = ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim & " " & g_AllContent1 & " " & OFFICEPUNCH & " " & g_AllContent2
                            sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                        End If
                    Catch ex As Exception

                    End Try
                End If
            End If
            'sms end
        End If
    End Function
    Public Function funcGetGeneralLogDataBio2(ByVal recordList As List(Of Riss.Devices.Record), Purpose As String, ID_NO As String, datafile As String, IN_OUT As String)
        Dim cmd1 As OleDbCommand
        Dim cmd As SqlCommand
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim ds As DataSet = New DataSet

        Dim x As Integer = 0
        Dim startdate As DateTime
        Dim paycodelist As New List(Of String)()
        For Each record As Riss.Devices.Record In recordList
            If x = 0 Then
                startdate = record.Clock
            End If
            Dim CARDNO As String = record.DIN.ToString() ' sdwEnrollNumber
            If IsNumeric(CARDNO) Then
                CARDNO = Convert.ToInt64(CARDNO).ToString("000000000000")
            End If
            'Dim punchdate As String = record.Clock.ToString("yyyy-MM-dd HH:mm:ss")
            Dim OFFICEPUNCH As String = record.Clock.ToString("yyyy-MM-dd HH:mm:ss") 'Convert.ToDateTime(punchdate).ToString("yyyy-MM-dd HH:mm:ss")

            XtraMasterTest.LabelControlStatus.Text = "Downloading " & CARDNO & " " & record.Clock.ToString("yyyy-MM-dd HH:mm:ss") 'Convert.ToDateTime(punchdate).ToString("yyyy-MM-dd HH:mm:ss")
            Application.DoEvents()

            If Purpose = "C" Then
                Dim sSqlX As String = "select * from tblTimeSlab "
                Dim adapX As SqlDataAdapter
                Dim adapAX As OleDbDataAdapter
                Dim dsX As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapAX = New OleDbDataAdapter(sSqlX, Common.con1)
                    adapAX.Fill(dsX)
                Else
                    adapX = New SqlDataAdapter(sSqlX, Common.con)
                    adapX.Fill(dsX)
                End If
                Dim shift As String
                Dim PURPOSECan As String = "N"
                If dsX.Tables(0).Rows.Count > 0 Then
                    Dim BStart As DateTime
                    Dim BEnd As DateTime
                    For i As Integer = 0 To dsX.Tables(0).Rows.Count - 1
                        BStart = Convert.ToDateTime(record.Clock.ToString("yyyy-MM-dd ") & Convert.ToDateTime(dsX.Tables(0).Rows(i).Item("BStart").ToString.Trim).ToString("HH:mm:00"))
                        BEnd = Convert.ToDateTime(record.Clock.ToString("yyyy-MM-dd ") & Convert.ToDateTime(dsX.Tables(0).Rows(i).Item("BEnd").ToString.Trim).ToString("HH:mm:00"))
                        If record.Clock >= BStart And record.Clock <= BEnd Then
                            PURPOSECan = dsX.Tables(0).Rows(i).Item("MealType").ToString.Trim '"B"
                            shift = dsX.Tables(0).Rows(i).Item("shift").ToString.Trim
                            Exit For
                        End If
                    Next
                End If
                Dim sSqlC As String = "insert into TBLCANTEEN(CARDNO, OFFICEPUNCH, ISMANUAL, SHIFT, PURPOSE, ISVEG) VALUES('" & CARDNO & "','" & OFFICEPUNCH & "','N', '" & shift & "', '" & PURPOSECan & "','Y')"
                If servername = "Access" Then
                    If con1.State <> ConnectionState.Open Then
                        con1.Open()
                    End If
                    Try
                        cmd1 = New OleDbCommand(sSqlC, con1)
                        cmd1.ExecuteNonQuery()
                    Catch
                        con1.Close()
                    End Try
                    con1.Close()
                Else
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    Try
                        cmd = New SqlCommand(sSqlC, con)
                        cmd.ExecuteNonQuery()
                    Catch
                        con.Close()
                    End Try
                    con.Close()
                End If
                Exit Function
            End If

            paycodelist.Add(record.DIN.ToString())
            Dim sSql As String = "insert into Rawdata (CARDNO,OFFICEPUNCH,Error_Code,DOOR_TIME,PROCESS,Id_No) values('" & CARDNO & "','" & OFFICEPUNCH & "','00', '00','N','" & ID_NO & "' ) "
            If servername = "Access" Then
                If con1.State <> ConnectionState.Open Then
                    con1.Open()
                End If
                Try
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()


                    'insert in text file
                    Dim fs As FileStream = New FileStream(datafile, FileMode.OpenOrCreate, FileAccess.Write)
                    Dim sw As StreamWriter = New StreamWriter(fs)
                    'find the end of the underlying filestream
                    sw.BaseStream.Seek(0, SeekOrigin.End)
                    sw.WriteLine(CARDNO & " " & OFFICEPUNCH & " " & ID_NO)
                    sw.Flush()
                    sw.Close()
                    'end insert in text file

                    Dim sSql1 As String = "select Rawdata.CARDNO, Rawdata.OFFICEPUNCH, TblEmployee.PAYCODE, TblEmployee.TELEPHONE1, TblEmployee.EMPNAME from Rawdata, TblEmployee where Rawdata.CARDNO = TblEmployee.PRESENTCARDNO and Rawdata.CARDNO = '" & CARDNO & "'"
                    ds = New DataSet
                    adapA = New OleDbDataAdapter(sSql1, con1)
                    adapA.Fill(ds)

                    If IN_OUT = "B" Then
                        Dim ds2 As DataSet = New DataSet
                        sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"
                        Dim adapAT As OleDbDataAdapter = New OleDbDataAdapter(sSql1, con1)
                        adapAT.Fill(ds2)
                        If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                            IN_OUT = "I"
                        Else
                            If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                                IN_OUT = "I"
                            Else
                                IN_OUT = "O"
                            End If
                        End If
                    End If
                    cmd1 = New OleDbCommand("insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) ", con1)
                    cmd1.ExecuteNonQuery()
                    Try
                        cmd1 = New OleDbCommand("insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) ", con1)
                        cmd1.ExecuteNonQuery()
                    Catch ex As Exception
                    End Try                    
                    con1.Close()


                    If Common.IsParallel = "Y" Then
                        Common.parallelInsert(record.DIN, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, record.Clock, IN_OUT, "N", ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim, ID_NO)
                    End If

                    'sms
                    If g_SMSApplicable = "" Then
                        Load_SMS_Policy()
                    End If
                    If g_SMSApplicable = "Y" Then
                        If g_isAllSMS = "Y" Then
                            Try
                                If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then
                                    Dim SmsContent As String = ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim & " " & g_AllContent1 & " " & OFFICEPUNCH & " " & g_AllContent2
                                    sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                                End If
                            Catch ex As Exception

                            End Try
                        End If
                    End If
                    'sms end

                Catch
                    con1.Close()
                End Try
            Else
                If con.State <> ConnectionState.Open Then
                    con.Open()
                End If
                Try
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()

                    'insert in text file
                    Dim fs As FileStream = New FileStream(datafile, FileMode.OpenOrCreate, FileAccess.Write)
                    Dim sw As StreamWriter = New StreamWriter(fs)
                    'find the end of the underlying filestream
                    sw.BaseStream.Seek(0, SeekOrigin.End)
                    sw.WriteLine(CARDNO & " " & OFFICEPUNCH & " " & ID_NO)
                    sw.Flush()
                    sw.Close()
                    'end insert in text file

                    Dim sSql1 As String = "select Rawdata.CARDNO, Rawdata.OFFICEPUNCH, TblEmployee.PAYCODE, TblEmployee.TELEPHONE1, TblEmployee.EMPNAME  from Rawdata, TblEmployee where Rawdata.CARDNO = TblEmployee.PRESENTCARDNO and Rawdata.CARDNO = '" & CARDNO & "'"
                    ds = New DataSet
                    adap = New SqlDataAdapter(sSql1, con)
                    adap.Fill(ds)

                    If IN_OUT = "B" Then
                        sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"
                        Dim ds2 As DataSet = New DataSet
                        Dim adapT As SqlDataAdapter = New SqlDataAdapter(sSql1, con)
                        adapT.Fill(ds2)
                        If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                            IN_OUT = "I"
                        Else
                            If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                                IN_OUT = "I"
                            Else
                                IN_OUT = "O"
                            End If
                        End If
                    End If

                    cmd = New SqlCommand("insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) ", con)
                    cmd.ExecuteNonQuery()
                    Try
                        cmd = New SqlCommand("insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) ", con)
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception

                    End Try
                    con.Close()

                    If Common.IsParallel = "Y" Then
                        Common.parallelInsert(record.DIN, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, record.Clock, IN_OUT, "N", ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim, ID_NO)
                    End If
                    'sms
                    If g_SMSApplicable = "" Then
                        Load_SMS_Policy()
                    End If
                    If g_SMSApplicable = "Y" Then
                        If g_isAllSMS = "Y" Then
                            Try
                                If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then
                                    Dim SmsContent As String = ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim & " " & g_AllContent1 & " " & OFFICEPUNCH & " " & g_AllContent2
                                    sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                                End If
                            Catch ex As Exception

                            End Try
                        End If
                    End If
                    'sms end
                Catch ex As Exception
                    con.Close()
                End Try
            End If
            x = x + 1
        Next


        Dim paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
        ds = New DataSet
        For i As Integer = 0 To paycodeArray.Length - 1
            Dim PRESENTCARDNO As String
            If IsNumeric(paycodeArray(i)) Then
                PRESENTCARDNO = Convert.ToInt64(paycodeArray(i)).ToString("000000000000")
            Else
                PRESENTCARDNO = paycodeArray(i)
            End If
            Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, , EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "'  and TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId"
            ds = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSqltmp, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                Remove_Duplicate_Punches(startdate, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(x).Item("Id"))
                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                    Process_AllRTC(startdate.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(x).Item("Id"))
                Else
                    Process_AllnonRTC(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(x).Item("Id"))
                    If EmpGrpArr(ds.Tables(0).Rows(x).Item("Id")).SHIFTTYPE = "M" Then
                        Process_AllnonRTCMulti(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(x).Item("Id"))

                    End If
                End If
            End If
        Next
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand("delete from Rawdata", Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand("delete from Rawdata", Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
    End Function
    Public Shared Sub Load_Corporate_PolicySql()
        'Dim Cn As Class_Connection = New Class_Connection
        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = " Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tb" & _
        "lsetup )"
        If servername = "Access" Then
            sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
            adapAc = New OleDbDataAdapter(sSql, con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, con)
            adapS.Fill(Rs)
        End If
        'Rs = Cn.FillDataSet_SQL(sSql)
        If (Rs.Tables(0).Rows.Count > 0) Then
            'g_s_end = Rs.Tables(0).Rows(0)("s_end").ToString.Trim
            'g_o_end = Rs.Tables(0).Rows(0)("s_out").ToString.Trim
            'g_PermisEarlydep = Rs.Tables(0).Rows(0)("PermisEarlydep").ToString.Trim
            'g_PermisLateArr = Rs.Tables(0).Rows(0)("PermisLateArr").ToString.Trim
            'g_MaxHlfAbsDur = Rs.Tables(0).Rows(0)("Half").ToString.Trim
            'If servername = "Access" Then
            '    g_PreMarkingDur = Rs.Tables(0).Rows(0)("Time_1").ToString.Trim
            'Else
            '    g_PreMarkingDur = Rs.Tables(0).Rows(0)("Time").ToString.Trim
            'End If
            'If servername = "Access" Then
            '    g_SrtMarkingDur = Rs.Tables(0).Rows(0)("Short1").ToString.Trim
            'Else
            '    g_SrtMarkingDur = Rs.Tables(0).Rows(0)("Short").ToString.Trim
            'End If
            'g_isHalfDay = Rs.Tables(0).Rows(0)("isHalfDay").ToString.Trim
            'g_isShort = Rs.Tables(0).Rows(0)("isShort").ToString.Trim
            'g_Two = Rs.Tables(0).Rows(0)("Two").ToString.Trim
            'g_Wo_Include = Rs.Tables(0).Rows(0)("WOInclude").ToString.Trim
            'g_OwMinus = Rs.Tables(0).Rows(0)("OwMinus").ToString.Trim
            'g_isPresentOnWOPresent = Rs.Tables(0).Rows(0)("isPresentOnWOPresent").ToString.Trim
            'g_isPresentOnHLDPresent = Rs.Tables(0).Rows(0)("isPresentOnHLDPresent").ToString.Trim
            'g_AbsentOnWO = Rs.Tables(0).Rows(0)("isAutoAbsent").ToString.Trim
            'g_As_Low = Convert.ToInt32(Rs.Tables(0).Rows(0)("AUTOSHIFT_LOW").ToString.Trim)
            'g_As_Up = Convert.ToInt32(Rs.Tables(0).Rows(0)("AUTOSHIFT_UP").ToString.Trim)
            'g_AutoShift = Rs.Tables(0).Rows(0)("ISAUTOSHIFT").ToString.Trim
            'g_isOverTime = Rs.Tables(0).Rows(0)("isOtAll").ToString.Trim
            'g_isOverStay = Rs.Tables(0).Rows(0)("isOverStay").ToString.Trim
            'g_NightShiftFourPunch = Rs.Tables(0).Rows(0)("NightShiftFourPunch").ToString.Trim
            g_LinesPerPage = Rs.Tables(0).Rows(0)("LinesPerPage").ToString.Trim
            g_Meals_Rate = Rs.Tables(0).Rows(0)("meals_rate").ToString.Trim
            If (Rs.Tables(0).Rows(0)("SkipAfterDepartment").ToString.Trim = "Y") Then
                g_SkipAfterDept = True
            Else
                g_SkipAfterDept = False
            End If

            gLeaveFinancialYear = Rs.Tables(0).Rows(0)("LeaveFinancialYear").ToString.Trim
            'If (Rs.Tables(0).Rows(0)("ISOTOUTMINUSSHIFTENDTIME").ToString.Trim = "Y") Then
            '    g_OTFormulae = 1
            'ElseIf (Rs.Tables(0).Rows(0)("ISOTWRKGHRSMINUSSHIFTHRS").ToString.Trim = "Y") Then
            '    g_OTFormulae = 2
            'Else
            '    g_OTFormulae = 3
            'End If

            'If (g_OTFormulae = 3) Then
            '    g_OTLate = Rs.Tables(0).Rows(0)("OTLATECOMINGDUR").ToString.Trim
            '    g_OTChkEarly = Rs.Tables(0).Rows(0)("ISOTEARLYCOMING").ToString.Trim
            '    g_OTEarly = Rs.Tables(0).Rows(0)("OTEARLYDUR").ToString.Trim
            '    g_OTEnd = Rs.Tables(0).Rows(0)("OTRESTRICTENDDUR").ToString.Trim
            'End If

            'g_OtRound = Rs.Tables(0).Rows(0)("OTROUND").ToString.Trim
            'g_Wo_OT = Rs.Tables(0).Rows(0)("DEDUCTWOOT").ToString.Trim
            'g_Hld_OT = Rs.Tables(0).Rows(0)("DEDUCTHOLIDAYOT").ToString.Trim
            'g_DupCheckMin = Rs.Tables(0).Rows(0)("DuplicateCheckMin").ToString.Trim
            'g_isAutoShift = Rs.Tables(0).Rows(0)("isAutoShift").ToString.Trim
            'g_Day_Hour = Rs.Tables(0).Rows(0)("MAXWRKDURATION").ToString.Trim
            'g_isOutWork = Rs.Tables(0).Rows(0)("IsOutWork").ToString.Trim
            '' Missing Report Variables
            'g_TIME1 = Rs.Tables(0).Rows(0)("Time1").ToString.Trim

            'G_CHECKLATE = Rs.Tables(0).Rows(0)("CHECKLATE").ToString.Trim

            'g_CHECKEARLY = Rs.Tables(0).Rows(0)("CHECKEARLY").ToString.Trim

            'g_Smart = Rs.Tables(0).Rows(0)("smart").ToString.Trim

            AutoDwnDur = Rs.Tables(0).Rows(0).Item("AutoDwnDur").ToString.Trim
            BioPort = Rs.Tables(0).Rows(0).Item("BioPort").ToString.Trim
            ZKPort = Rs.Tables(0).Rows(0).Item("ZKPort").ToString.Trim
            TWIR102Port = Rs.Tables(0).Rows(0).Item("TWIR102Port").ToString.Trim
            TF01Port = Rs.Tables(0).Rows(0).Item("TF01Port").ToString.Trim
            If Common.servername = "Access" Then
                online = "N"
            Else
                online = Rs.Tables(0).Rows(0).Item("Online").ToString.Trim
            End If
            IsNepali = Rs.Tables(0).Rows(0).Item("IsNepali").ToString.Trim
            TimerDur = Rs.Tables(0).Rows(0).Item("TimerDur").ToString.Trim
            If TimerDur = "" Then
                TimerDur = 0
            End If
            If TimerDur <> 0 Then
                XtraMasterTest.TimerCloud.Interval = Convert.ToInt32(Common.TimerDur) * 60 * 1000
            End If


            MarkInactivDurDay = Rs.Tables(0).Rows(0).Item("MarkInactivDurDay").ToString.Trim

            IsIOCL = Rs.Tables(0).Rows(0).Item("IsIOCL").ToString.Trim
            If IsIOCL = "Y" Then
                IOCLInterval = Rs.Tables(0).Rows(0).Item("IOCLInterval").ToString.Trim
                XtraMasterTest.TimerIOCL.Interval = Common.IOCLInterval * 60000
            Else
                IOCLInterval = 0
            End If

            IOCLLink = Rs.Tables(0).Rows(0).Item("IOCLLink").ToString.Trim

            TWCloud = Rs.Tables(0).Rows(0).Item("TWCloud").ToString.Trim
            IsWDMS = Rs.Tables(0).Rows(0).Item("IsWDMS").ToString.Trim
            iDMS = Rs.Tables(0).Rows(0).Item("iDMS").ToString.Trim

            WDMSDBName = Rs.Tables(0).Rows(0).Item("WDMSDBName").ToString.Trim
            IsBioSecurity = Rs.Tables(0).Rows(0).Item("IsBioSecurity").ToString.Trim
            BioSecurityDBName = Rs.Tables(0).Rows(0).Item("BioSecurityDBName").ToString.Trim
            IsZKAccess = Rs.Tables(0).Rows(0).Item("IsZKAccess").ToString.Trim
            ZKAccessDBName = Rs.Tables(0).Rows(0).Item("ZKAccessDBName").ToString.Trim

            IsTWAccess = Rs.Tables(0).Rows(0).Item("IsTWAccess").ToString.Trim
            TWAccessDBName = Rs.Tables(0).Rows(0).Item("TWAccessDBName").ToString.Trim
            iDMSDB = Rs.Tables(0).Rows(0).Item("iDMSDB").ToString.Trim
            TWCloudDB = Rs.Tables(0).Rows(0).Item("TWCloudDB").ToString.Trim
            OnlineStartUp = Rs.Tables(0).Rows(0).Item("OnlineStartUp").ToString.Trim

        End If

        
    End Sub
    Public Shared Function GetNextBckTime()
        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String
        sSql = "select * from BackUpData"
        Rs = New DataSet
        If Common.servername = "Access" Then
            adapAc = New OleDbDataAdapter(sSql, Common.con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, Common.con)
            adapS.Fill(Rs)
        End If
        If Rs.Tables(0).Rows.Count > 0 Then
            If Rs.Tables(0).Rows(0).Item("IsAutoBackUp").ToString.Trim = "Y" Then
                IsAutoBackUp = True
            Else
                IsAutoBackUp = False
            End If
            AutoBackUpForDays = Rs.Tables(0).Rows(0).Item("AutoBackUpForDays").ToString.Trim
            AccessAutoBackUpPath = Rs.Tables(0).Rows(0).Item("AccessAutoBackUpPath").ToString.Trim
            If Rs.Tables(0).Rows(0).Item("DeleteAfterAutoBackUp").ToString.Trim = "Y" Then
                DeleteAfterAutoBackUp = True
            Else
                DeleteAfterAutoBackUp = False
            End If
            BackUpType = Rs.Tables(0).Rows(0).Item("BackUpType").ToString.Trim
            BackUpPath = Rs.Tables(0).Rows(0).Item("BackUpPath").ToString.Trim
            If IsAutoBackUp = True Then
                Dim x As DateTime = Convert.ToDateTime(Rs.Tables(0).Rows(0).Item("LastAutoBackUpDate").ToString.Trim).AddDays((Rs.Tables(0).Rows(0).Item("AutoBackUpTimerDays").ToString.Trim))
                Dim timeX As String = Rs.Tables(0).Rows(0).Item("AutoBackUpTimerTime").ToString.Trim
                NextAutoBackUpDateTime = x.ToString("yyyy-MM-dd") & " " & timeX
            End If
        Else
        IsAutoBackUp = False
        End If

    End Function
    Public Sub Process_AllnonRTC(ByVal mindate As DateTime, ByVal maxdate As DateTime, ByVal paycodefrom As String, ByVal paycodeto As String, ByVal EmpGrpId As Integer)
        ' getinfo.Load_Corporate_PolicySql();        
        'Load_Corporate_PolicySql()

        Dim mCtr As Integer = 0
        Dim wCond As String = Nothing
        Dim i As Integer = 0
        Dim OwDur As Integer = 0
        Dim gs As Integer = 0
        Dim tout As String = Nothing
        Dim mReason_OutWork As String = Nothing
        Dim rsOutWork As DataSet = New DataSet
        Dim Qy As DataSet = New DataSet
        Dim MATT_ACC As Integer = 0
        Dim rsTime As DataSet = New DataSet
        Dim rsPunch As DataSet = New DataSet
        Dim mDate1 As String = Nothing
        Dim mDate2 As String = Nothing
        Dim Day_Hour As Integer = 0
        Dim mP_Flag As String = Nothing
        Dim mReasonCode As String = Nothing
        Dim strFirstPunchReasonCode As String = Nothing
        Dim strLastPunchReasonCode As String = Nothing
        Dim Proc_Date As String = ""
        ' Proc_Date = Proc_Date.ToString("yyyy-MM-dd");
        Dim rsEmp As DataSet = New DataSet
        Dim HCOMPANYCODE As String = ""
        Dim HDEPARTMENTCODE As String = ""
        Dim mmin_date As DateTime = System.DateTime.MinValue
        Dim mmax_date As DateTime = System.DateTime.MinValue
        Dim mdate_2 As DateTime = System.DateTime.MinValue
        Dim Out_2 As DateTime = System.DateTime.MinValue
        'Dim g_FromPaycode As String = ""
        'Dim g_ToPaycode As String = ""
        'g_FromPaycode = paycodefrom
        'g_ToPaycode = paycodeto
        mmin_date = mindate
        mmax_date = maxdate
        'Dim mIn1 As String = ""
        'Dim mIn2 As String = ""
        'Dim mout1 As String = ""
        'Dim mout2 As String = ""
        mIn1 = ""
        mIn2 = ""
        mout1 = ""
        mout2 = ""
        Dim mIn1Mannual As String = ""
        Dim mIn2Mannual As String = ""
        Dim mOut1Mannual As String = ""
        Dim mOut2Mannual As String = ""
        Dim g_NightShiftFourPunch As String = ""
        Dim g_o_end As String = ""
        Dim g_s_end As String = ""
        Dim dateoffice As DateTime = System.DateTime.MinValue
        Dim DateOffice_add As String = ""
        'g_o_end = getinfo.g_o_end
        'g_s_end = getinfo.g_s_end

        If servername = "Access" Then
            If con1Access.State <> ConnectionState.Open Then
                con1Access.Open()
            End If
        Else
            If conSQL.State <> ConnectionState.Open Then
                conSQL.Open()
            End If
        End If

        'sSql = ("Select a.*,b.companycode,b.departmentcode from tblemployeeshiftmaster a,tblemployee b where a.paycode" & _
        '"=b.paycode and a.ISROUNDTHECLOCKWORK='N' AND A.PAYCODE>='" _
        '            + (g_FromPaycode + ("' AND A.PAYCODE<='" _
        '            + (g_ToPaycode + "'"))))
        sSql = "Select a.*,b.companycode,b.departmentcode, b.BRANCHCODE, b.Telephone1, b.EMPNAME from tblemployeeshiftmaster a,tblemployee b where a.paycode" & _
      "=b.paycode and a.ISROUNDTHECLOCKWORK='N' AND A.PAYCODE='" & paycodefrom & "'"
        '+ (g_FromPaycode + ("' AND A.PAYCODE<='" _
        '+ (g_ToPaycode + "'"))))    ' with branch code
        rsEmp = New DataSet 'nitin
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1Access)
            adapA.Fill(rsEmp)
        Else
            adap = New SqlDataAdapter(sSql, conSQL)
            adap.Fill(rsEmp)
        End If
        'rsEmp = Cn.FillDataSet(sSql)
        If (rsEmp.Tables(0).Rows.Count > 0) Then
            'System.Windows.Forms.Application.DoEvents();
            Dim iE As Integer = 0
            Do While (iE < rsEmp.Tables(0).Rows.Count)
                HCOMPANYCODE = rsEmp.Tables(0).Rows(iE)("companycode").ToString.Trim
                HDEPARTMENTCODE = rsEmp.Tables(0).Rows(iE)("departmentcode").ToString.Trim
                Load_HolidaySql(HCOMPANYCODE, HDEPARTMENTCODE, rsEmp.Tables(0).Rows(iE)("BRANCHCODE").ToString.Trim)
                Load_ShiftMasterSql()
                If Not String.IsNullOrEmpty(rsEmp.Tables(0).Rows(iE)("MaxDayMin").ToString.Trim) Then
                    Day_Hour = Convert.ToInt32(rsEmp.Tables(0).Rows(iE)("MaxDayMin").ToString.Trim)
                End If

                sSql = ("UPDATE MACHINERAWPUNCH SET P_DAY='N' WHERE PAYCODE='" _
                            + (rsEmp.Tables(0).Rows(iE)("PAYCODE").ToString.Trim.Trim + ("' AND OFFICEPUNCH>='" _
                            + (Convert.ToDateTime(mmin_date).AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00'"))))
                Dim sSql1 As String = ("delete from tblOutWorkRegister where paycode='" _
                                    + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim + ("' and DateOffice between '" _
                                    + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                                    + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59'"))))))
                Dim sSql2 As String = ("Update tbltimeregister Set in1=null,in2=null,out1=null,out2=null " + (" where paycode='" _
                                + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and DateOffice between '" _
                                + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                                + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59'")))))))
                Dim sSql3 As String = ("Select * from tbltimeregister where paycode='" _
                               + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and DateOffice between '" _
                               + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                               + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59' Order By dateoffice"))))))
                If servername = "Access" Then
                    'If con1.State <> ConnectionState.Open Then
                    '    con1.Open()
                    'End If
                    sSql = ("UPDATE MACHINERAWPUNCH SET P_DAY='N' WHERE PAYCODE='" _
                          + (rsEmp.Tables(0).Rows(iE)("PAYCODE").ToString.Trim.Trim + ("' AND FORMAT(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss')>='" _
                          + (Convert.ToDateTime(mmin_date).AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00'"))))
                    cmd1 = New OleDbCommand(sSql, con1Access)
                    cmd1.ExecuteNonQuery()
                    sSql1 = ("delete from tblOutWorkRegister where paycode='" _
                                   + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim + ("' and FORMAT(DateOffice,'yyyy-MM-dd HH:mm:ss') between '" _
                                   + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                                   + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59'"))))))
                    cmd1 = New OleDbCommand(sSql1, con1Access)
                    cmd1.ExecuteNonQuery()
                    sSql2 = ("Update tbltimeregister Set in1=null,in2=null,out1=null,out2=null " + (" where paycode='" _
                                + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and FORMAT(DateOffice,'yyyy-MM-dd HH:mm:ss') between '" _
                                + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                                + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59'")))))))

                    cmd1 = New OleDbCommand(sSql2, con1Access)
                    cmd1.ExecuteNonQuery()
                    'con1.Close()
                    rsTime = New DataSet 'nitin
                    sSql3 = ("Select * from tbltimeregister where paycode='" _
                              + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and FORMAT(DateOffice,'yyyy-MM-dd HH:mm:ss') between '" _
                              + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                              + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59' Order By dateoffice"))))))
                    adapA1 = New OleDbDataAdapter(sSql3, con1Access)
                    adapA1.Fill(rsTime)
                Else
                    'If conSQL.State <> ConnectionState.Open Then
                    '    conSQL.Open()
                    'End If
                    cmd = New SqlCommand(sSql, conSQL)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(sSql1, conSQL)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(sSql2, conSQL)
                    cmd.ExecuteNonQuery()
                    'con.Close()
                    rsTime = New DataSet 'nitin
                    adap1 = New SqlDataAdapter(sSql3, conSQL)
                    adap1.Fill(rsTime)
                End If
                'Cn.execute_NonQuery(sSql)
                'Cn.execute_NonQuery(("delete from tblOutWorkRegister where paycode='" _
                '                + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim + ("' and DateOffice between '" _
                '                + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                '                + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59'")))))))
                'sSql = ("Update tbltimeregister Set in1=null,in2=null,out1=null,out2=null " + (" where paycode='" _
                '            + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and DateOffice between '" _
                '            + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                '            + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59'")))))))
                'Cn.execute_NonQuery(sSql)

                'sSql = ("Select * from tbltimeregister where paycode='" _
                '            + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and DateOffice between '" _
                '            + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                '            + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59' Order By dateoffice"))))))
                'rsTime = Cn.FillDataSet(sSql)


                mReasonCode = ""
                strFirstPunchReasonCode = ""
                strLastPunchReasonCode = ""
                If (rsTime.Tables(0).Rows.Count > 0) Then
                    Dim iT As Integer = 0
                    Do While (iT < rsTime.Tables(0).Rows.Count)
                        XtraMasterTest.LabelControlStatus.Text = "Processing for Paycode " & paycodefrom & "  Date: " & Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("dd/MM/yyyy")
                        Application.DoEvents()

                        Proc_Date = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("yyyy-MM-dd")

                        '  par.toolStripStatusLabel1.Text = " Processing the punches of Date " + Convert.ToDateTime(rsTime.Tables[0].Rows[iT]["dateoffice"]).ToString("yyyy-MM-dd") + " of Paycode " + rsTime.Tables[0].Rows[iT]["paycode"].ToString() + "(Please Wait) ";
                        ' System.Windows.Forms.Application.DoEvents();
                        mIn1 = "Null"
                        mIn2 = "Null"
                        mout1 = "Null"
                        mout2 = "Null"
                        mShiftStartTime = "Null"
                        mShiftEndTime = "Null"
                        mLunchStartTime = "Null"
                        mLunchEndTime = "Null"
                        mIn1Mannual = "N"
                        mIn2Mannual = "N"
                        mOut1Mannual = "N"
                        mOut2Mannual = "N"
                        OwDur = 0

                        'For SMS
                        If rsTime.Tables(0).Rows(iT)("LateSMS").ToString.Trim = "" Then
                            mLateSMS = "N"
                        Else
                            mLateSMS = Trim(rsTime.Tables(0).Rows(iT)("LateSMS").ToString)
                        End If
                        If rsTime.Tables(0).Rows(iT)("InSMS").ToString.Trim = "" Then
                            mInSMS = "N"
                        Else
                            mInSMS = Trim(rsTime.Tables(0).Rows(iT)("InSMS").ToString)
                        End If
                        If rsTime.Tables(0).Rows(iT)("OutSMS").ToString.Trim = "" Then
                            mOutSMS = "N"
                        Else
                            mOutSMS = Trim(rsTime.Tables(0).Rows(iT)("OutSMS").ToString)
                        End If

                        mDate1 = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("yyyy-MM-dd")

                        'nitin for shift early and late
                        Dim shiftEarly As DateTime
                        Dim shiftLate As DateTime
                        sSql = "Select * from tblShiftmaster where shift = '" & rsTime.Tables(0).Rows(iT).Item("SHIFT").ToString.Trim & "'"
                        Dim dsTmp As DataSet = New DataSet
                        Dim adapTmp As SqlDataAdapter
                        Dim adapATmp As OleDbDataAdapter
                        If servername = "Access" Then
                            adapATmp = New OleDbDataAdapter(sSql, con1Access)
                            adapATmp.Fill(dsTmp)
                        Else
                            adapTmp = New SqlDataAdapter(sSql, conSQL)
                            adapTmp.Fill(dsTmp)
                        End If
                        If rsTime.Tables(0).Rows(iT).Item("SHIFT").ToString.Trim = "OFF" Or rsTime.Tables(0).Rows(iT).Item("SHIFT").ToString.Trim = "IGN" Or rsTime.Tables(0).Rows(iT).Item("SHIFT").ToString.Trim = "FLX" Then
                            'orinal
                            sSql = ("Select * from machinerawpunch where paycode='" _
                                        + (rsEmp.Tables(0).Rows(iE)("paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and convert(varchar(10),officepunch,127) ='" _
                                        + (mDate1 + "'  Order By OfficePunch"))))
                        Else
                            shiftEarly = (Convert.ToDateTime(Proc_Date & " " & Convert.ToDateTime(dsTmp.Tables(0).Rows(0).Item("STARTTIME").ToString.Trim).ToString("HH:mm:ss"))).AddMinutes(-dsTmp.Tables(0).Rows(0).Item("ShiftEarly").ToString.Trim)
                            shiftLate = (Convert.ToDateTime(Proc_Date & " " & Convert.ToDateTime(dsTmp.Tables(0).Rows(0).Item("ENDTIME").ToString.Trim).ToString("HH:mm:ss"))).AddMinutes(dsTmp.Tables(0).Rows(0).Item("ShiftLate").ToString.Trim)
                            'orinal
                            If dsTmp.Tables(0).Rows(0).Item("ShiftEarly").ToString.Trim = 0 And dsTmp.Tables(0).Rows(0).Item("ShiftLate").ToString.Trim = 0 Then
                                sSql = ("Select * from machinerawpunch where paycode='" _
                                            + (rsEmp.Tables(0).Rows(iE)("paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and convert(varchar(10),officepunch,127) ='" _
                                            + (mDate1 + "'  Order By OfficePunch"))))
                            Else
                                sSql = "Select * from machinerawpunch where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString.Trim & _
                                    "' and officepunch >='" & shiftEarly.ToString("yyyy-MM-dd HH:mm:ss") & "' and officepunch <='" & shiftLate.ToString("yyyy-MM-dd HH:mm:ss") & "' Order By OfficePunch"
                            End If
                        End If

                        'End nitin for shift early and late




                        rsPunch = New DataSet 'nitin
                        If servername = "Access" Then
                            If rsTime.Tables(0).Rows(iT).Item("SHIFT").ToString.Trim = "OFF" Or rsTime.Tables(0).Rows(iT).Item("SHIFT").ToString.Trim = "IGN" Or rsTime.Tables(0).Rows(iT).Item("SHIFT").ToString.Trim = "FLX" Then
                                sSql = ("Select * from machinerawpunch where paycode='" _
                                        + (rsEmp.Tables(0).Rows(iE)("paycode").ToString.Trim + ("' and FORMAT(officepunch,'yyyy-MM-dd') ='" _
                                        + (mDate1 + "'  Order By OfficePunch"))))
                            Else
                                'original
                                If dsTmp.Tables(0).Rows(0).Item("ShiftEarly").ToString.Trim = 0 And dsTmp.Tables(0).Rows(0).Item("ShiftLate").ToString.Trim = 0 Then
                                    sSql = ("Select * from machinerawpunch where paycode='" _
                                            + (rsEmp.Tables(0).Rows(iE)("paycode").ToString.Trim + ("' and FORMAT(officepunch,'yyyy-MM-dd') ='" _
                                            + (mDate1 + "'  Order By OfficePunch"))))
                                Else
                                    sSql = "Select * from machinerawpunch where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString.Trim & _
                                   "' and FORMAT(officepunch,'yyyy-MM-dd HH:mm:ss') >='" & shiftEarly.ToString("yyyy-MM-dd HH:mm:ss") & "' " & _
                                   "and FORMAT(officepunch,'yyyy-MM-dd HH:mm:ss') <='" & shiftLate.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                End If
                            End If


                            adapA2 = New OleDbDataAdapter(sSql, con1Access)
                            adapA2.Fill(rsPunch)
                        Else
                            adap2 = New SqlDataAdapter(sSql, conSQL)
                            adap2.Fill(rsPunch)
                        End If
                        'rsPunch = Cn.FillDataSet(sSql)
                        If (rsEmp.Tables(0).Rows(iE)("IsPunchAll").ToString.Trim = "N") Then
                            GoTo AsgIt
                        End If

                        mReasonCode = ""
                        strFirstPunchReasonCode = ""
                        strLastPunchReasonCode = ""
                        If ((rsPunch.Tables(0).Rows.Count > 2) _
                                    AndAlso (rsEmp.Tables(0).Rows(iE)("IsOutWork").ToString.Trim = "Y")) Then
                            Dim iP As Integer = 0
                            Do While (iP < rsPunch.Tables(0).Rows.Count)
                                mIn1 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                mIn1Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                            AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                    mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                    strFirstPunchReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                Else
                                    strFirstPunchReasonCode = ""
                                End If

                                iP = (Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1)
                                mout2 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                mOut2Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                            AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                    mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                    strLastPunchReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                Else
                                    strLastPunchReasonCode = ""
                                End If

                                tout = rsPunch.Tables(0).Rows(iP)("Officepunch").ToString
                                Dim rO As Integer = 0
                                sSql = "Select * from tblOutWorkRegister where paycode='" & rsTime.Tables(0).Rows(iT)("paycode").ToString.Trim & "' And DateOffice='" & mDate1 & "'"
                                rsOutWork = New DataSet 'nitin
                                If servername = "Access" Then
                                    sSql = "Select * from tblOutWorkRegister where paycode='" & rsTime.Tables(0).Rows(iT)("paycode").ToString.Trim & "' And FORMAT(DateOffice, 'yyyy-MM-dd')='" & mDate1 & "'"
                                    adapA3 = New OleDbDataAdapter(sSql, con1Access)
                                    adapA3.Fill(rsOutWork)
                                Else
                                    adap3 = New SqlDataAdapter(sSql, conSQL)
                                    adap3.Fill(rsOutWork)
                                End If
                                'rsOutWork = Cn.FillDataSet(sSql)

                                Dim rsOutWorkpaycode As String
                                Dim rsOutWorkdateOffice As String
                                Dim rsOutWorkIn As String
                                Dim rsOutWorkRIn As String
                                Dim rsOutWorkOut As String
                                Dim rsOutWorROut As String

                                If (rsOutWork.Tables(0).Rows.Count < 1) Then
                                    rsOutWork.Tables(0).Rows.Add(rsOutWork.Tables(0).NewRow)
                                    rsOutWork.Tables(0).Rows(rO)("paycode") = rsTime.Tables(0).Rows(iT)("paycode")
                                    rsOutWork.Tables(0).Rows(rO)("dateOffice") = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("yyyy-MM-dd HH:mm")

                                    rsOutWorkpaycode = rsTime.Tables(0).Rows(iT)("paycode")
                                    rsOutWorkdateOffice = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("yyyy-MM-dd HH:mm")
                                    Dim tmpstr As String = "Insert into tblOutWorkRegister (paycode,dateOffice) values " & _
                                 "( '" & rsOutWorkpaycode & "','" & rsOutWorkdateOffice & "')"
                                    If servername = "Access" Then
                                        cmd1 = New OleDbCommand(tmpstr, con1Access)
                                        cmd1.ExecuteNonQuery()
                                    Else
                                        'If con.State <> ConnectionState.Open Then
                                        '    con.Open()
                                        'End If
                                        cmd = New SqlCommand(tmpstr, conSQL)
                                        cmd.ExecuteNonQuery()
                                    End If

                                End If

                                iP = 0
                                iP = 1
                                mCtr = 1
                                OwDur = 0
                                mReason_OutWork = "N"

                                While (iP <> (Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1))
                                    If (tout = rsPunch.Tables(0).Rows(iP)("Officepunch").ToString) Then
                                        Exit While
                                    End If

                                    If (mCtr <= 10) Then
                                        rsOutWorkIn = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                        rsOutWork.Tables(0).Rows(rO)(("In" + mCtr.ToString.Trim(Microsoft.VisualBasic.ChrW(32)))) = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                        If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                                    AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)) <> "")) Then
                                            rsOutWork.Tables(0).Rows(rO)(("RIn" + mCtr.ToString.Trim(Microsoft.VisualBasic.ChrW(32)))) = rsPunch.Tables(0).Rows(iP)("ReasonCode")
                                            rsOutWorkRIn = rsPunch.Tables(0).Rows(iP)("ReasonCode")
                                            mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                            If (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0) Then
                                                mReason_OutWork = "Y"
                                            Else
                                                mReason_OutWork = "N"
                                            End If

                                        End If

                                    End If

                                    iP = (iP + 1)
                                    If (iP = (Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1)) Then
                                        Exit While
                                    End If
                                    If (tout = rsPunch.Tables(0).Rows(iP)("Officepunch").ToString) Then
                                        Exit While
                                    End If

                                    If (mCtr <= 10) Then
                                        rsOutWorkOut = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")

                                        rsOutWork.Tables(0).Rows(rO)(("Out" + mCtr.ToString.Trim(Microsoft.VisualBasic.ChrW(32)))) = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                        If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                                    AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)) <> "")) Then
                                            rsOutWork.Tables(0).Rows(rO)(("ROut" + mCtr.ToString.Trim(Microsoft.VisualBasic.ChrW(32)))) = rsPunch.Tables(0).Rows(iP)("ReasonCode")
                                            rsOutWorROut = rsPunch.Tables(0).Rows(iP)("ReasonCode")

                                            mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                            If (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0) Then
                                                mReason_OutWork = "Y"
                                            Else
                                                mReason_OutWork = "N"
                                            End If

                                        End If
                                        OwDur = (OwDur + Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(rsOutWork.Tables(0).Rows(rO)(("In" + mCtr.ToString.Trim(Microsoft.VisualBasic.ChrW(32))))), Convert.ToDateTime(rsOutWork.Tables(0).Rows(rO)(("Out" + mCtr.ToString.Trim(Microsoft.VisualBasic.ChrW(32))))))))
                                    End If

                                    Dim tmpstr As String = "Update tblOutWorkRegister set OUTWORKDURATION = '" & OwDur & "',Reason_OutWork = '" & mReason_OutWork & "', " & "Out" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkOut & "', " & "ROut" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorROut & "', " & "RIn" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkRIn & "', " & "In" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkIn & "' where paycode = '" & rsOutWorkpaycode & "' and dateOffice='" & rsOutWorkdateOffice & "'"
                                    If servername = "Access" Then
                                        tmpstr = "Update tblOutWorkRegister set OUTWORKDURATION = '" & OwDur & "',Reason_OutWork = '" & mReason_OutWork & "', " & "Out" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkOut & "', " & "ROut" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorROut & "', " & "RIn" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkRIn & "', " & "In" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkIn & "' where paycode = '" & rsOutWorkpaycode & "' and FORMAT(dateOffice,'yyyy-MM-dd HH:mm')='" & rsOutWorkdateOffice & "'"
                                        cmd1 = New OleDbCommand(tmpstr, con1Access)
                                        cmd1.ExecuteNonQuery()
                                    Else
                                        cmd = New SqlCommand(tmpstr, conSQL)
                                        cmd.ExecuteNonQuery()
                                    End If

                                    iP = (iP + 1)
                                    mCtr = (mCtr + 1)
                                    If (iP = (Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1)) Then
                                        Exit While
                                    End If
                                    If (tout = rsPunch.Tables(0).Rows(iP)("Officepunch").ToString) Then
                                        Exit While
                                    End If
                                    If (mCtr > 10) Then  'New as per ajitesh.. cannot be greater tha 10
                                        Exit While
                                    End If
                                End While

                                rsOutWork.Tables(0).Rows(rO)("OUTWORKDURATION") = OwDur
                                rsOutWork.Tables(0).Rows(rO)("Reason_OutWork") = mReason_OutWork
                                rsOutWork.AcceptChanges()
                                mOutWorkDuration = OwDur
                                GoTo AsgIt
                                iP = (iP + 1)
                            Loop
                        ElseIf (rsPunch.Tables(0).Rows.Count > 0) Then
                            'Dim iP As Integer = 0
                            'Do While (iP < rsPunch.Tables(0).Rows.Count)
                            For iP As Integer = 0 To rsPunch.Tables(0).Rows.Count - 1
                                If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                            And (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                    mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                End If

                                mIn1 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm")
                                mIn1Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                iP = (iP + 1)
                                If (iP = Convert.ToInt32(rsPunch.Tables(0).Rows.Count)) Then
                                    GoTo AsgIt
                                End If

                                mout1 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                mOut1Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                            AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                    mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                End If

                                iP = (iP + 1)
                                If ((iP = Convert.ToInt32(rsPunch.Tables(0).Rows.Count)) _
                                            OrElse (rsEmp.Tables(0).Rows(iE)("Two").ToString.Trim = "Y")) Then
                                    mout2 = mout1
                                    mOut2Mannual = mOut1Mannual
                                    mout1 = "Null"
                                    mOut1Mannual = ""
                                    GoTo nRTCasgA
                                End If

                                mIn2 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm")
                                mIn2Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                            And (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                    mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                End If

                                iP = (iP + 1)
                                If (iP = Convert.ToInt32(rsPunch.Tables(0).Rows.Count)) Then
                                    GoTo AsgIt
                                End If

                                mout2 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                mOut2Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                            AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                    mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                End If

nRTCasgA:
                                Exit For
                                iP = (iP + 1)
                            Next

                        End If

AsgIt:
                        If ((mout2 = "Null") _
                                    AndAlso ((mout1 <> "Null") _
                                    AndAlso (mIn2 = "Null"))) Then
                            mout2 = mout1
                            mout1 = "Null"
                        End If

                        getShiftInfo()
                        mShiftAttended = AutoShift2(rsTime, iT, rsEmp, iE, mIn1, mIn2, mout1, mout2, EmpGrpId)
                        Upd2(rsTime, iT, rsEmp, iE, mIn1, mIn2, mout1, mout2, mShiftAttended, EmpGrpId, OwDur)
                        If (mShiftStartTime <> "Null") Then
                            mShiftStartTime = ("'" _
                                        + (mShiftStartTime + "'"))
                        End If

                        If (mShiftEndTime <> "Null") Then
                            mShiftEndTime = ("'" _
                                        + (mShiftEndTime + "'"))
                        End If

                        If (mLunchStartTime <> "Null") Then
                            mLunchStartTime = ("'" _
                                        + (mLunchStartTime + "'"))
                        End If

                        If (mLunchEndTime <> "Null") Then
                            mLunchEndTime = ("'" _
                                        + (mLunchEndTime + "'"))
                        End If

                        If (mIn1 <> "Null") Then
                            mIn1 = ("'" _
                                        + (mIn1 + "'"))
                        End If

                        If (mout1 <> "Null") Then
                            mout1 = ("'" _
                                        + (mout1 + "'"))
                        End If

                        If (mIn2 <> "Null") Then
                            mIn2 = ("'" _
                                        + (mIn2 + "'"))
                        End If

                        If (mout2 <> "Null") Then
                            mout2 = ("'" _
                                        + (mout2 + "'"))
                        End If
                        'If EmpGrpArr(EmpGrpId).g_OwMinus = "Y" Then
                        '    If mHoursworked > OwDur Then
                        '        mHoursworked = mHoursworked - OwDur
                        '    End If
                        'End If

                        sSql = "Update tblTimeRegister Set ShiftStartTime=" & mShiftStartTime & ", ShiftEndTime=" & mShiftEndTime & ",LunchStartTime=" & mLunchStartTime & ", " & _
                                " LunchEndTime=" & mLunchEndTime & ", HoursWorked=" & mHoursworked & ",ExcLunchHours=" & mExcLunchHours & ",OtDuration=" & mOtDuration & ", " & _
                                " OsDuration=" & mOSDuration & ",OtAmount=" & motamount & ", EarlyArrival=" & mEarlyArrival & ",EarlyDeparture=" & mearlydeparture & ", " & _
                                " LateArrival=" & mLateArrival & ", LunchEarlyDeparture=" & mLunchEarlyDeparture & ", LunchLateArrival=" & mLunchLateArrival & ", " & _
                                " TotalLossHrs=" & mTotalLossHrs & ",Status='" & mStatus & "',ShiftAttended='" & mShiftAttended & "',In1=" & mIn1 & ",In2=" & mIn2 & ", " & _
                                " out1=" & mout1 & ",Out2=" & mout2 & ",In1Mannual='" & mIn1Mannual & "',In2Mannual='" & mIn2Mannual & "',Out1Mannual='" & mOut1Mannual & "', " & _
                                " Out2Mannual='" & mOut2Mannual & "',LeaveValue=" & mLeaveValue & ",PresentValue=" & mPresentValue & ",AbsentValue=" & mAbsentValue & ", " & _
                                " Holiday_Value=" & mHoliday_Value & ",Wo_Value=" & mWO_Value & ",OutWorkDuration=" & OwDur & " " & _
                                " where paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim(Microsoft.VisualBasic.ChrW(32)) & "' and dateoffice = '" & Proc_Date & "' "

                        'Cn.execute_NonQuery(sSql)
                        If servername = "Access" Then
                            'If con1.State <> ConnectionState.Open Then
                            '    con1.Open()
                            'End If
                            Try
                                sSql = "Update tblTimeRegister Set ShiftStartTime=" & mShiftStartTime & ", ShiftEndTime=" & mShiftEndTime & ",LunchStartTime=" & mLunchStartTime & ", " & _
                              " LunchEndTime=" & mLunchEndTime & ", HoursWorked=" & mHoursworked & ",ExcLunchHours=" & mExcLunchHours & ",OtDuration=" & mOtDuration & ", " & _
                              " OsDuration=" & mOSDuration & ",OtAmount=" & motamount & ", EarlyArrival=" & mEarlyArrival & ",EarlyDeparture=" & mearlydeparture & ", " & _
                              " LateArrival=" & mLateArrival & ", LunchEarlyDeparture=" & mLunchEarlyDeparture & ", LunchLateArrival=" & mLunchLateArrival & ", " & _
                              " TotalLossHrs=" & mTotalLossHrs & ",Status='" & mStatus & "',ShiftAttended='" & mShiftAttended & "',In1=" & mIn1 & ",In2=" & mIn2 & ", " & _
                              " out1=" & mout1 & ",Out2=" & mout2 & ",In1Mannual='" & mIn1Mannual & "',In2Mannual='" & mIn2Mannual & "',Out1Mannual='" & mOut1Mannual & "', " & _
                              " Out2Mannual='" & mOut2Mannual & "',LeaveValue=" & mLeaveValue & ",PresentValue=" & mPresentValue & ",AbsentValue=" & mAbsentValue & ", " & _
                              " Holiday_Value=" & mHoliday_Value & ",Wo_Value=" & mWO_Value & ",OutWorkDuration=" & OwDur & " " & _
                              " where paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim(Microsoft.VisualBasic.ChrW(32)) & "' and FORMAT(dateoffice,'yyyy-MM-dd') = '" & Proc_Date & "' "

                                cmd1 = New OleDbCommand(sSql, con1Access)
                                cmd1.ExecuteNonQuery()
                            Catch ex As Exception

                            End Try
                            
                            ' con1.Close()
                        Else
                            'If con.State <> ConnectionState.Open Then
                            '    con.Open()
                            'End If
                            cmd = New SqlCommand(sSql, conSQL)
                            cmd.ExecuteNonQuery()
                            'con.Close()
                        End If
                        'iT = (iT + 1)

                        'SMS logic
                        Dim shiftEndTime As Date
                        'Dim request As HttpWebRequest
                        'Dim response As HttpWebResponse = Nothing
                        Dim Proc_DateTmp As DateTime = Convert.ToDateTime(Proc_Date)
                        If g_SMSApplicable = "" Then
                            Load_SMS_Policy()
                        End If
                        If g_SMSApplicable = "Y" Then
                            If Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) <> "" And Proc_DateTmp.ToString("yyyy-MM-dd") = Now.ToString("yyyy-MM-dd") Then
                                'Late
                                If g_isLate = "Y" And mLateArrival > 0 Then
                                    If mLateSMS = "N" Then 'temp comment
                                        LateDur = Math.Truncate(mLateArrival / 60).ToString("00") & ":" & (mLateArrival Mod 60).ToString("00")
                                        'LateDur = Min2Hr(mLateArrival)
                                        LateSMSContent = rsEmp.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim & " " & g_LateContent1 & " " & LateDur & " " & g_LateContent2
                                        ''url = "http://alerts.3mdigital.co.in/api/web2sms.php?workingkey=" & G_SmsKey & "&sender=" & G_SenderID & "&to=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString) & "&message=" & LateSMSContent & ""
                                        'original() '
                                        'url = "http://sms.mobtexting.com/index.php/api?method=sms.normal&api_key=" & Trim(G_SmsKey) & "&to=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) & "&sender=" & Trim(G_SenderID) & "&message=" & Trim(LateSMSContent) & "&flash=0&unicode=1"
                                        'url = "http://sms.jbssolution.in/submitsms.jsp?user=TESCHOTI&key=68e0655580XX&mobile=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) & "&message=" & Trim(LateSMSContent) & "&senderid=TESCHT&accusage=1&unicode=1"
                                        'Content = frmMdiMain.Inet1.OpenURL(url)
                                        Try
                                            sendSMS(Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim), LateSMSContent)
                                            'request = DirectCast(WebRequest.Create(url), HttpWebRequest)
                                            'response = DirectCast(request.GetResponse(), HttpWebResponse)
                                            If servername = "Access" Then
                                                sSql = "Update tblTimeRegister Set LateSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and format(dateoffice,'yyyy-MM-dd') = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                                                cmd1 = New OleDbCommand(sSql, con1Access)
                                                cmd1.ExecuteNonQuery()
                                            Else
                                                sSql = "Update tblTimeRegister Set LateSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and dateoffice = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                                                cmd = New SqlCommand(sSql, conSQL)
                                                cmd.ExecuteNonQuery()
                                            End If
                                        Catch ex As Exception

                                        End Try
                                    End If 'temp comment
                                End If
                                'IN
                                mIn1 = Replace(mIn1, "'", "")
                                If rsTime.Tables(0).Rows(iT).Item("SHIFT").ToString.Trim <> "OFF" And rsTime.Tables(0).Rows(iT).Item("SHIFT").ToString.Trim <> "IGN" Or rsTime.Tables(0).Rows(iT).Item("SHIFT").ToString.Trim = "FLX" Then
                                    If g_isInSMS = "Y" And IsDate(mIn1) Then
                                        If mInSMS = "N" Then  'temp comment
                                            'InPunch = Convert.ToDateTime(mIn1).ToString("HH:mm")
                                            InPunch = Convert.ToDateTime(mIn1).ToString("dd/MM/yyyy HH:mm")
                                            InSMSContent = rsEmp.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim & " " & g_InContent1 & " " & InPunch & " " & g_InContent2
                                            'InSMSContent = g_OutContent1 & " " & InPunch & " " & g_OutContent2 'for anil's customer
                                            Try
                                                sendSMS(Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim), InSMSContent)
                                                If servername = "Access" Then
                                                    sSql = "Update tblTimeRegister Set inSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and format(dateoffice,'yyyy-MM-dd') = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                                                    cmd1 = New OleDbCommand(sSql, con1Access)
                                                    cmd1.ExecuteNonQuery()
                                                Else
                                                    sSql = "Update tblTimeRegister Set inSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and dateoffice = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                                                    cmd = New SqlCommand(sSql, conSQL)
                                                    cmd.ExecuteNonQuery()
                                                End If
                                            Catch ex As Exception

                                            End Try
                                            'Cn.Execute(sSql)
                                        End If  'temp comment
                                    End If
                                    'Out
                                    mout2 = Replace(mout2, "'", "")
                                    mShiftEndTime = Replace(mShiftEndTime, "'", "")
                                    If g_isOutSMS = "Y" And IsDate(mout2) Then
                                        If mOutSMS = "N" Then  'temp comment
                                            shiftEndTime = mShiftEndTime
                                            shiftEndTime = DateAdd("n", g_OutSMSAfter, shiftEndTime)
                                            If CDate(mout2) > CDate(shiftEndTime) Then
                                                'OutPunch = Convert.ToDateTime(mout2).ToString("HH:mm") ' 
                                                OutPunch = Convert.ToDateTime(mout2).ToString("dd/MM/yyyy HH:mm")
                                                OutSMSContent = rsEmp.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim & " " & g_OutContent1 & " " & OutPunch & " " & g_OutContent2
                                                'OutSMSContent = InSMSContent & " " & OutPunch & " " & InSMSContent 'for anil's customer
                                                Try
                                                    sendSMS(Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim), OutSMSContent)
                                                    If servername = "Access" Then
                                                        sSql = "Update tblTimeRegister Set OutSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and format(dateoffice,'yyyy-MM-dd') = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                                                        cmd1 = New OleDbCommand(sSql, con1Access)
                                                        cmd1.ExecuteNonQuery()
                                                    Else
                                                        sSql = "Update tblTimeRegister Set OutSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and dateoffice = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                                                        cmd = New SqlCommand(sSql, conSQL)
                                                        cmd.ExecuteNonQuery()
                                                    End If
                                                Catch ex As Exception

                                                End Try
                                                'Cn.Execute(sSql)
                                            End If
                                        End If  'temp comment
                                    End If
                                End If
                            End If
                        End If
                        'End SMS logic
                        iT = (iT + 1)
                    Loop
                End If
                iE = (iE + 1)
            Loop

        End If
        If servername = "Access" Then
            If con1Access.State <> ConnectionState.Closed Then
                con1Access.Close()
            End If
        Else
            If conSQL.State <> ConnectionState.Closed Then
                conSQL.Close()
            End If
        End If
        ''XtraMaster.LabelControl4.Text = ""
        ''Application.DoEvents()
        ' par.toolStripStatusLabel1.Text = "Timewatch Application";
    End Sub
    Public Sub Process_AllnonRTCMulti(ByVal mindate As DateTime, ByVal maxdate As DateTime, ByVal paycodefrom As String, ByVal paycodeto As String, ByVal EmpGrpId As Integer)
        ' getinfo.Load_Corporate_PolicySql();        
        'Load_Corporate_PolicySql()

        Dim mCtr As Integer = 0
        Dim wCond As String = Nothing
        Dim i As Integer = 0
        Dim OwDur As Integer = 0
        Dim gs As Integer = 0
        Dim tout As String = Nothing
        Dim mReason_OutWork As String = Nothing
        Dim rsOutWork As DataSet = New DataSet
        Dim Qy As DataSet = New DataSet
        Dim MATT_ACC As Integer = 0
        Dim rsTime As DataSet = New DataSet
        Dim rsPunch As DataSet = New DataSet
        Dim mDate1 As String = Nothing
        Dim mDate2 As String = Nothing
        Dim Day_Hour As Integer = 0
        Dim mP_Flag As String = Nothing
        Dim mReasonCode As String = Nothing
        Dim strFirstPunchReasonCode As String = Nothing
        Dim strLastPunchReasonCode As String = Nothing
        Dim Proc_Date As String = ""
        ' Proc_Date = Proc_Date.ToString("yyyy-MM-dd");
        Dim rsEmp As DataSet = New DataSet
        Dim HCOMPANYCODE As String = ""
        Dim HDEPARTMENTCODE As String = ""
        Dim mmin_date As DateTime = System.DateTime.MinValue
        Dim mmax_date As DateTime = System.DateTime.MinValue
        Dim mdate_2 As DateTime = System.DateTime.MinValue
        Dim Out_2 As DateTime = System.DateTime.MinValue
        'Dim g_FromPaycode As String = ""
        'Dim g_ToPaycode As String = ""
        'g_FromPaycode = paycodefrom
        'g_ToPaycode = paycodeto
        mmin_date = mindate
        mmax_date = maxdate
        'Dim mIn1M As String = ""
        'Dim mIn2M As String = ""
        'Dim mOUT1M As String = ""
        'Dim mOUT2M As String = ""
        mIn1M = ""
        mIn2M = ""
        mout1M = ""
        mout2M = ""
        Dim mIn1Mannual As String = ""
        Dim mIn2Mannual As String = ""
        Dim mOut1Mannual As String = ""
        Dim mOut2Mannual As String = ""
        Dim g_NightShiftFourPunch As String = ""
        Dim g_o_end As String = ""
        Dim g_s_end As String = ""
        Dim dateoffice As DateTime = System.DateTime.MinValue
        Dim DateOffice_add As String = ""
        'g_o_end = getinfo.g_o_end
        'g_s_end = getinfo.g_s_end

        If servername = "Access" Then
            If con1Access.State <> ConnectionState.Open Then
                con1Access.Open()
            End If
        Else
            If conSQL.State <> ConnectionState.Open Then
                conSQL.Open()
            End If
        End If

        'sSql = ("Select a.*,b.companycode,b.departmentcode from tblemployeeshiftmaster a,tblemployee b where a.paycode" & _
        '"=b.paycode and a.ISROUNDTHECLOCKWORK='N' AND A.PAYCODE>='" _
        '            + (g_FromPaycode + ("' AND A.PAYCODE<='" _
        '            + (g_ToPaycode + "'"))))
        sSql = "Select a.*,b.companycode,b.departmentcode, b.BRANCHCODE, b.Telephone1, b.EMPNAME from tblemployeeshiftmaster a,tblemployee b where a.paycode" & _
      "=b.paycode and a.ISROUNDTHECLOCKWORK='N' AND A.PAYCODE='" & paycodefrom & "'"
        '+ (g_FromPaycode + ("' AND A.PAYCODE<='" _
        '+ (g_ToPaycode + "'"))))    ' with branch code
        rsEmp = New DataSet 'nitin
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1Access)
            adapA.Fill(rsEmp)
        Else
            adap = New SqlDataAdapter(sSql, conSQL)
            adap.Fill(rsEmp)
        End If
        'rsEmp = Cn.FillDataSet(sSql)
        If (rsEmp.Tables(0).Rows.Count > 0) Then
            'System.Windows.Forms.Application.DoEvents();
            Dim iE As Integer = 0
            Do While (iE < rsEmp.Tables(0).Rows.Count)
                HCOMPANYCODE = rsEmp.Tables(0).Rows(iE)("companycode").ToString.Trim
                HDEPARTMENTCODE = rsEmp.Tables(0).Rows(iE)("departmentcode").ToString.Trim
                Load_HolidaySql(HCOMPANYCODE, HDEPARTMENTCODE, rsEmp.Tables(0).Rows(iE)("BRANCHCODE").ToString.Trim)
                Load_ShiftMasterSql()
                If Not String.IsNullOrEmpty(rsEmp.Tables(0).Rows(iE)("MaxDayMin").ToString.Trim) Then
                    Day_Hour = Convert.ToInt32(rsEmp.Tables(0).Rows(iE)("MaxDayMin").ToString.Trim)
                End If

                sSql = ("UPDATE MACHINERAWPUNCH SET P_DAY='N' WHERE PAYCODE='" _
                            + (rsEmp.Tables(0).Rows(iE)("PAYCODE").ToString.Trim.Trim + ("' AND OFFICEPUNCH>='" _
                            + (Convert.ToDateTime(mmin_date).AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00'"))))
                'Dim sSql1 As String = ("delete from tblOutWorkRegister where paycode='" _
                '                    + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim + ("' and DateOffice between '" _
                '                    + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                '                    + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59'"))))))
                Dim sSql2 As String = ("Update tbltimeregister Set min1=null,min2=null,mout1=null,mout2=null " + (" where paycode='" _
                                + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and DateOffice between '" _
                                + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                                + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59'")))))))
                Dim sSql3 As String = ("Select * from tbltimeregister where paycode='" _
                               + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and DateOffice between '" _
                               + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                               + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59' Order By dateoffice"))))))
                If servername = "Access" Then
                    'If con1.State <> ConnectionState.Open Then
                    '    con1.Open()
                    'End If
                    sSql = ("UPDATE MACHINERAWPUNCH SET P_DAY='N' WHERE PAYCODE='" _
                          + (rsEmp.Tables(0).Rows(iE)("PAYCODE").ToString.Trim.Trim + ("' AND FORMAT(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss')>='" _
                          + (Convert.ToDateTime(mmin_date).AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00'"))))
                    cmd1 = New OleDbCommand(sSql, con1Access)
                    cmd1.ExecuteNonQuery()
                    'sSql1 = ("delete from tblOutWorkRegister where paycode='" _
                    '               + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim + ("' and FORMAT(DateOffice,'yyyy-MM-dd HH:mm:ss') between '" _
                    '               + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                    '               + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59'"))))))
                    'cmd1 = New OleDbCommand(sSql1, con1Access)
                    'cmd1.ExecuteNonQuery()
                    sSql2 = ("Update tbltimeregister Set min1=null,min2=null,mout1=null,mout2=null " + (" where paycode='" _
                                + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and FORMAT(DateOffice,'yyyy-MM-dd HH:mm:ss') between '" _
                                + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                                + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59'")))))))

                    cmd1 = New OleDbCommand(sSql2, con1Access)
                    cmd1.ExecuteNonQuery()
                    'con1.Close()
                    rsTime = New DataSet 'nitin
                    sSql3 = ("Select * from tbltimeregister where paycode='" _
                              + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and FORMAT(DateOffice,'yyyy-MM-dd HH:mm:ss') between '" _
                              + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                              + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59' Order By dateoffice"))))))
                    adapA1 = New OleDbDataAdapter(sSql3, con1Access)
                    adapA1.Fill(rsTime)
                Else
                    'If conSQL.State <> ConnectionState.Open Then
                    '    conSQL.Open()
                    'End If
                    cmd = New SqlCommand(sSql, conSQL)
                    cmd.ExecuteNonQuery()
                    'cmd = New SqlCommand(sSql1, conSQL)
                    'cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(sSql2, conSQL)
                    cmd.ExecuteNonQuery()
                    'con.Close()
                    rsTime = New DataSet 'nitin
                    adap1 = New SqlDataAdapter(sSql3, conSQL)
                    adap1.Fill(rsTime)
                End If
                'Cn.execute_NonQuery(sSql)
                'Cn.execute_NonQuery(("delete from tblOutWorkRegister where paycode='" _
                '                + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim + ("' and DateOffice between '" _
                '                + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                '                + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59'")))))))
                'sSql = ("Update tbltimeregister Set in1=null,in2=null,out1=null,out2=null " + (" where paycode='" _
                '            + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and DateOffice between '" _
                '            + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                '            + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59'")))))))
                'Cn.execute_NonQuery(sSql)

                'sSql = ("Select * from tbltimeregister where paycode='" _
                '            + (rsEmp.Tables(0).Rows(iE)("Paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and DateOffice between '" _
                '            + (mmin_date.ToString("yyyy-MM-dd") + (" 00:00:00' and '" _
                '            + (mmax_date.ToString("yyyy-MM-dd") + " 23:59:59' Order By dateoffice"))))))
                'rsTime = Cn.FillDataSet(sSql)


                mReasonCode = ""
                strFirstPunchReasonCode = ""
                strLastPunchReasonCode = ""
                If (rsTime.Tables(0).Rows.Count > 0) Then
                    Dim iT As Integer = 0
                    Do While (iT < rsTime.Tables(0).Rows.Count)
                        XtraMasterTest.LabelControlStatus.Text = "Processing for Paycode " & paycodefrom & "  Date: " & Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("dd/MM/yyyy")
                        Application.DoEvents()

                        Proc_Date = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("yyyy-MM-dd")

                        '  par.toolStripStatusLabel1.Text = " Processing the punches of Date " + Convert.ToDateTime(rsTime.Tables[0].Rows[iT]["dateoffice"]).ToString("yyyy-MM-dd") + " of Paycode " + rsTime.Tables[0].Rows[iT]["paycode"].ToString() + "(Please Wait) ";
                        ' System.Windows.Forms.Application.DoEvents();
                        mIn1M = "Null"
                        mIn2M = "Null"
                        mout1M = "Null"
                        mout2M = "Null"
                        mShiftStartTime = "Null"
                        mShiftEndTime = "Null"
                        mLunchStartTime = "Null"
                        mLunchEndTime = "Null"
                        mIn1Mannual = "N"
                        mIn2Mannual = "N"
                        mOut1Mannual = "N"
                        mOut2Mannual = "N"


                        ''For SMS
                        'If rsTime.Tables(0).Rows(iT)("LateSMS").ToString.Trim = "" Then
                        '    mLateSMS = "N"
                        'Else
                        '    mLateSMS = Trim(rsTime.Tables(0).Rows(iT)("LateSMS").ToString)
                        'End If
                        'If rsTime.Tables(0).Rows(iT)("InSMS").ToString.Trim = "" Then
                        '    mInSMS = "N"
                        'Else
                        '    mInSMS = Trim(rsTime.Tables(0).Rows(iT)("InSMS").ToString)
                        'End If
                        'If rsTime.Tables(0).Rows(iT)("OutSMS").ToString.Trim = "" Then
                        '    mOutSMS = "N"
                        'Else
                        '    mOutSMS = Trim(rsTime.Tables(0).Rows(iT)("OutSMS").ToString)
                        'End If

                        mDate1 = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("yyyy-MM-dd")

                        'nitin for shift early and late
                        Dim shiftEarly As DateTime
                        Dim shiftLate As DateTime
                        sSql = "Select * from tblShiftmaster where shift = '" & rsTime.Tables(0).Rows(iT).Item("MSHIFT").ToString.Trim & "'"
                        Dim dsTmp As DataSet = New DataSet
                        Dim adapTmp As SqlDataAdapter
                        Dim adapATmp As OleDbDataAdapter
                        If servername = "Access" Then
                            adapATmp = New OleDbDataAdapter(sSql, con1Access)
                            adapATmp.Fill(dsTmp)
                        Else
                            adapTmp = New SqlDataAdapter(sSql, conSQL)
                            adapTmp.Fill(dsTmp)
                        End If
                        If rsTime.Tables(0).Rows(iT).Item("MSHIFT").ToString.Trim = "OFF" Or rsTime.Tables(0).Rows(iT).Item("MSHIFT").ToString.Trim = "IGN" Or rsTime.Tables(0).Rows(iT).Item("MSHIFT").ToString.Trim = "FLX" Then
                            'orinal
                            sSql = ("Select * from machinerawpunch where paycode='" _
                                        + (rsEmp.Tables(0).Rows(iE)("paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and convert(varchar(10),officepunch,127) ='" _
                                        + (mDate1 + "'  Order By OfficePunch"))))
                        Else
                            shiftEarly = (Convert.ToDateTime(Proc_Date & " " & Convert.ToDateTime(dsTmp.Tables(0).Rows(0).Item("STARTTIME").ToString.Trim).ToString("HH:mm:ss"))).AddMinutes(-dsTmp.Tables(0).Rows(0).Item("ShiftEarly").ToString.Trim)
                            shiftLate = (Convert.ToDateTime(Proc_Date & " " & Convert.ToDateTime(dsTmp.Tables(0).Rows(0).Item("ENDTIME").ToString.Trim).ToString("HH:mm:ss"))).AddMinutes(dsTmp.Tables(0).Rows(0).Item("ShiftLate").ToString.Trim)
                            'orinal
                            If dsTmp.Tables(0).Rows(0).Item("ShiftEarly").ToString.Trim = 0 And dsTmp.Tables(0).Rows(0).Item("ShiftLate").ToString.Trim = 0 Then
                                sSql = ("Select * from machinerawpunch where paycode='" _
                                            + (rsEmp.Tables(0).Rows(iE)("paycode").ToString.Trim.Trim(Microsoft.VisualBasic.ChrW(32)) + ("' and convert(varchar(10),officepunch,127) ='" _
                                            + (mDate1 + "'  Order By OfficePunch"))))
                            Else
                                sSql = "Select * from machinerawpunch where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString.Trim & _
                                    "' and officepunch >='" & shiftEarly.ToString("yyyy-MM-dd HH:mm:ss") & "' and officepunch <='" & shiftLate.ToString("yyyy-MM-dd HH:mm:ss") & "' Order By OfficePunch"
                            End If
                        End If

                        'End nitin for shift early and late




                        rsPunch = New DataSet 'nitin
                        If servername = "Access" Then
                            If rsTime.Tables(0).Rows(iT).Item("MSHIFT").ToString.Trim = "OFF" Or rsTime.Tables(0).Rows(iT).Item("MSHIFT").ToString.Trim = "IGN" Or rsTime.Tables(0).Rows(iT).Item("MSHIFT").ToString.Trim = "FLX" Then
                                sSql = ("Select * from machinerawpunch where paycode='" _
                                        + (rsEmp.Tables(0).Rows(iE)("paycode").ToString.Trim + ("' and FORMAT(officepunch,'yyyy-MM-dd') ='" _
                                        + (mDate1 + "'  Order By OfficePunch"))))
                            Else
                                'original
                                If dsTmp.Tables(0).Rows(0).Item("ShiftEarly").ToString.Trim = 0 And dsTmp.Tables(0).Rows(0).Item("ShiftLate").ToString.Trim = 0 Then
                                    sSql = ("Select * from machinerawpunch where paycode='" _
                                            + (rsEmp.Tables(0).Rows(iE)("paycode").ToString.Trim + ("' and FORMAT(officepunch,'yyyy-MM-dd') ='" _
                                            + (mDate1 + "'  Order By OfficePunch"))))
                                Else
                                    sSql = "Select * from machinerawpunch where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString.Trim & _
                                   "' and FORMAT(officepunch,'yyyy-MM-dd HH:mm:ss') >='" & shiftEarly.ToString("yyyy-MM-dd HH:mm:ss") & "' " & _
                                   "and FORMAT(officepunch,'yyyy-MM-dd HH:mm:ss') <='" & shiftLate.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                End If
                            End If


                            adapA2 = New OleDbDataAdapter(sSql, con1Access)
                            adapA2.Fill(rsPunch)
                        Else
                            adap2 = New SqlDataAdapter(sSql, conSQL)
                            adap2.Fill(rsPunch)
                        End If
                        ''rsPunch = Cn.FillDataSet(sSql)
                        'If (rsEmp.Tables(0).Rows(iE)("IsPunchAll").ToString.Trim = "N") Then   'not for multi punch
                        '    GoTo AsgIt
                        'End If

                        mReasonCode = ""
                        strFirstPunchReasonCode = ""
                        strLastPunchReasonCode = ""
                        'If ((rsPunch.Tables(0).Rows.Count > 2) _
                        '            AndAlso (rsEmp.Tables(0).Rows(iE)("IsOutWork").ToString.Trim = "Y")) Then 'not for multi punch
                        '    Dim iP As Integer = 0
                        '    Do While (iP < rsPunch.Tables(0).Rows.Count)
                        '        mIn1M = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                        '        mIn1Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                        '        If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                        '                    AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                        '            mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                        '            strFirstPunchReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                        '        Else
                        '            strFirstPunchReasonCode = ""
                        '        End If

                        '        iP = (Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1)
                        '        mOUT2M = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                        '        mOut2Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                        '        If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                        '                    AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                        '            mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                        '            strLastPunchReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                        '        Else
                        '            strLastPunchReasonCode = ""
                        '        End If

                        '        tout = rsPunch.Tables(0).Rows(iP)("Officepunch").ToString
                        '        Dim rO As Integer = 0
                        '        sSql = "Select * from tblOutWorkRegister where paycode='" & rsTime.Tables(0).Rows(iT)("paycode").ToString.Trim & "' And DateOffice='" & mDate1 & "'"
                        '        rsOutWork = New DataSet 'nitin
                        '        If servername = "Access" Then
                        '            sSql = "Select * from tblOutWorkRegister where paycode='" & rsTime.Tables(0).Rows(iT)("paycode").ToString.Trim & "' And FORMAT(DateOffice, 'yyyy-MM-dd')='" & mDate1 & "'"
                        '            adapA3 = New OleDbDataAdapter(sSql, con1Access)
                        '            adapA3.Fill(rsOutWork)
                        '        Else
                        '            adap3 = New SqlDataAdapter(sSql, conSQL)
                        '            adap3.Fill(rsOutWork)
                        '        End If
                        '        'rsOutWork = Cn.FillDataSet(sSql)

                        '        Dim rsOutWorkpaycode As String
                        '        Dim rsOutWorkdateOffice As String
                        '        Dim rsOutWorkIn As String
                        '        Dim rsOutWorkRIn As String
                        '        Dim rsOutWorkOut As String
                        '        Dim rsOutWorROut As String

                        '        If (rsOutWork.Tables(0).Rows.Count < 1) Then
                        '            rsOutWork.Tables(0).Rows.Add(rsOutWork.Tables(0).NewRow)
                        '            rsOutWork.Tables(0).Rows(rO)("paycode") = rsTime.Tables(0).Rows(iT)("paycode")
                        '            rsOutWork.Tables(0).Rows(rO)("dateOffice") = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("yyyy-MM-dd HH:mm")

                        '            rsOutWorkpaycode = rsTime.Tables(0).Rows(iT)("paycode")
                        '            rsOutWorkdateOffice = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("yyyy-MM-dd HH:mm")
                        '            Dim tmpstr As String = "Insert into tblOutWorkRegister (paycode,dateOffice) values " & _
                        '         "( '" & rsOutWorkpaycode & "','" & rsOutWorkdateOffice & "')"
                        '            If servername = "Access" Then
                        '                cmd1 = New OleDbCommand(tmpstr, con1Access)
                        '                cmd1.ExecuteNonQuery()
                        '            Else
                        '                'If con.State <> ConnectionState.Open Then
                        '                '    con.Open()
                        '                'End If
                        '                cmd = New SqlCommand(tmpstr, conSQL)
                        '                cmd.ExecuteNonQuery()
                        '            End If

                        '        End If

                        '        iP = 0
                        '        iP = 1
                        '        mCtr = 1
                        '        OwDur = 0
                        '        mReason_OutWork = "N"

                        '        While (iP <> (Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1))
                        '            If (tout = rsPunch.Tables(0).Rows(iP)("Officepunch").ToString) Then
                        '                Exit While
                        '            End If

                        '            If (mCtr <= 10) Then
                        '                rsOutWorkIn = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                        '                rsOutWork.Tables(0).Rows(rO)(("In" + mCtr.ToString.Trim(Microsoft.VisualBasic.ChrW(32)))) = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                        '                If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                        '                            AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)) <> "")) Then
                        '                    rsOutWork.Tables(0).Rows(rO)(("RIn" + mCtr.ToString.Trim(Microsoft.VisualBasic.ChrW(32)))) = rsPunch.Tables(0).Rows(iP)("ReasonCode")
                        '                    rsOutWorkRIn = rsPunch.Tables(0).Rows(iP)("ReasonCode")
                        '                    mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                        '                    If (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0) Then
                        '                        mReason_OutWork = "Y"
                        '                    Else
                        '                        mReason_OutWork = "N"
                        '                    End If

                        '                End If

                        '            End If

                        '            iP = (iP + 1)
                        '            If (iP = (Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1)) Then
                        '                Exit While
                        '            End If
                        '            If (tout = rsPunch.Tables(0).Rows(iP)("Officepunch").ToString) Then
                        '                Exit While
                        '            End If

                        '            If (mCtr <= 10) Then
                        '                rsOutWorkOut = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")

                        '                rsOutWork.Tables(0).Rows(rO)(("Out" + mCtr.ToString.Trim(Microsoft.VisualBasic.ChrW(32)))) = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                        '                If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                        '                            AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)) <> "")) Then
                        '                    rsOutWork.Tables(0).Rows(rO)(("ROut" + mCtr.ToString.Trim(Microsoft.VisualBasic.ChrW(32)))) = rsPunch.Tables(0).Rows(iP)("ReasonCode")
                        '                    rsOutWorROut = rsPunch.Tables(0).Rows(iP)("ReasonCode")

                        '                    mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                        '                    If (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0) Then
                        '                        mReason_OutWork = "Y"
                        '                    Else
                        '                        mReason_OutWork = "N"
                        '                    End If

                        '                End If
                        '                OwDur = (OwDur + Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(rsOutWork.Tables(0).Rows(rO)(("In" + mCtr.ToString.Trim(Microsoft.VisualBasic.ChrW(32))))), Convert.ToDateTime(rsOutWork.Tables(0).Rows(rO)(("Out" + mCtr.ToString.Trim(Microsoft.VisualBasic.ChrW(32))))))))
                        '            End If

                        '            Dim tmpstr As String = "Update tblOutWorkRegister set OUTWORKDURATION = '" & OwDur & "',Reason_OutWork = '" & mReason_OutWork & "', " & "Out" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkOut & "', " & "ROut" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorROut & "', " & "RIn" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkRIn & "', " & "In" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkIn & "' where paycode = '" & rsOutWorkpaycode & "' and dateOffice='" & rsOutWorkdateOffice & "'"
                        '            If servername = "Access" Then
                        '                tmpstr = "Update tblOutWorkRegister set OUTWORKDURATION = '" & OwDur & "',Reason_OutWork = '" & mReason_OutWork & "', " & "Out" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkOut & "', " & "ROut" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorROut & "', " & "RIn" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkRIn & "', " & "In" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkIn & "' where paycode = '" & rsOutWorkpaycode & "' and FORMAT(dateOffice,'yyyy-MM-dd HH:mm')='" & rsOutWorkdateOffice & "'"
                        '                cmd1 = New OleDbCommand(tmpstr, con1Access)
                        '                cmd1.ExecuteNonQuery()
                        '            Else
                        '                cmd = New SqlCommand(tmpstr, conSQL)
                        '                cmd.ExecuteNonQuery()
                        '            End If

                        '            iP = (iP + 1)
                        '            mCtr = (mCtr + 1)
                        '            If (iP = (Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1)) Then
                        '                Exit While
                        '            End If
                        '            If (tout = rsPunch.Tables(0).Rows(iP)("Officepunch").ToString) Then
                        '                Exit While
                        '            End If
                        '            If (mCtr > 10) Then  'New as per ajitesh.. cannot be greater tha 10
                        '                Exit While
                        '            End If
                        '        End While

                        '        rsOutWork.Tables(0).Rows(rO)("OUTWORKDURATION") = OwDur
                        '        rsOutWork.Tables(0).Rows(rO)("Reason_OutWork") = mReason_OutWork
                        '        rsOutWork.AcceptChanges()
                        '        mOutWorkDuration = OwDur
                        '        GoTo AsgIt
                        '        iP = (iP + 1)
                        '    Loop

                        'Else
                        If rsEmp.Tables(0).Rows(iE)("Two").ToString.Trim = "Y" Then
                            If (rsPunch.Tables(0).Rows.Count > 2) Then
                                'Dim iP As Integer = 0
                                'Do While (iP < rsPunch.Tables(0).Rows.Count)
                                For iP As Integer = 2 To rsPunch.Tables(0).Rows.Count - 1
                                    'If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                    '            And (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                    '    mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                    'End If

                                    mIn1M = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm")
                                    mIn1Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                    iP = (iP + 1)
                                    If (iP = Convert.ToInt32(rsPunch.Tables(0).Rows.Count)) Then
                                        GoTo AsgIt
                                    End If

                                    mout1M = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                    mOut1Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                    If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                                AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                        mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                    End If

                                    iP = (iP + 1)
                                    If ((iP = Convert.ToInt32(rsPunch.Tables(0).Rows.Count)) _
                                                OrElse (rsEmp.Tables(0).Rows(iE)("Two").ToString.Trim = "Y")) Then
                                        mout2M = mout1M
                                        mOut2Mannual = mOut1Mannual
                                        mout1M = "Null"
                                        mOut1Mannual = ""
                                        GoTo nRTCasgA
                                    End If

                                    mIn2M = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm")
                                    mIn2Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                    If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                                And (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                        mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                    End If

                                    iP = (iP + 1)
                                    If (iP = Convert.ToInt32(rsPunch.Tables(0).Rows.Count)) Then
                                        GoTo AsgIt
                                    End If

                                    mout2M = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                    mOut2Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                    If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                                AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                        mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                    End If

nRTCasgA:
                                    Exit For
                                    iP = (iP + 1)
                                Next

                            End If
                        ElseIf rsEmp.Tables(0).Rows(iE)("Two").ToString.Trim = "N" And rsEmp.Tables(0).Rows(iE)("ISOUTWORK").ToString.Trim = "N" Then
                            If (rsPunch.Tables(0).Rows.Count > 4) Then
                                'Dim iP As Integer = 0
                                'Do While (iP < rsPunch.Tables(0).Rows.Count)
                                For iP As Integer = 4 To rsPunch.Tables(0).Rows.Count - 1
                                    'If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                    '            And (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                    '    mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                    'End If

                                    mIn1M = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm")
                                    mIn1Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                    iP = (iP + 1)
                                    If (iP = Convert.ToInt32(rsPunch.Tables(0).Rows.Count)) Then
                                        GoTo AsgIt
                                    End If

                                    mout1M = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                    mOut1Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                    If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                                AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                        mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                    End If

                                    iP = (iP + 1)
                                    If ((iP = Convert.ToInt32(rsPunch.Tables(0).Rows.Count)) _
                                                OrElse (rsEmp.Tables(0).Rows(iE)("Two").ToString.Trim = "Y")) Then
                                        mout2M = mout1M
                                        mOut2Mannual = mOut1Mannual
                                        mout1M = "Null"
                                        mOut1Mannual = ""
                                        GoTo nRTCasgA1
                                    End If

                                    mIn2M = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm")
                                    mIn2Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                    If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                                And (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                        mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                    End If

                                    iP = (iP + 1)
                                    If (iP = Convert.ToInt32(rsPunch.Tables(0).Rows.Count)) Then
                                        GoTo AsgIt
                                    End If

                                    mout2M = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                    mOut2Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString
                                    If (Not String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString) _
                                                AndAlso (rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString.Trim(Microsoft.VisualBasic.ChrW(32)).Length <> 0)) Then
                                        mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString
                                    End If

nRTCasgA1:
                                    Exit For
                                    iP = (iP + 1)
                                Next

                            End If
                        End If
AsgIt:
                        If ((mout2M = "Null") _
                                    AndAlso ((mout1M <> "Null") _
                                    AndAlso (mIn2M = "Null"))) Then
                            mout2M = mout1M
                            mout1M = "Null"
                        End If

                        'getShiftInfo()
                        mShiftAttended = rsTime.Tables(0).Rows(iT).Item("MSHIFT").ToString.Trim 'AutoShift2(rsTime, iT, rsEmp, iE, mIn1M, mIn2M, mout1M, mout2M, EmpGrpId)
                        Upd2Multi(rsTime, iT, rsEmp, iE, mIn1M, mIn2M, mout1M, mout2M, mShiftAttended, EmpGrpId)
                        If (mShiftStartTime <> "Null") Then
                            mShiftStartTime = ("'" _
                                        + (mShiftStartTime + "'"))
                        End If

                        If (mShiftEndTime <> "Null") Then
                            mShiftEndTime = ("'" _
                                        + (mShiftEndTime + "'"))
                        End If

                        If (mLunchStartTime <> "Null") Then
                            mLunchStartTime = ("'" _
                                        + (mLunchStartTime + "'"))
                        End If

                        If (mLunchEndTime <> "Null") Then
                            mLunchEndTime = ("'" _
                                        + (mLunchEndTime + "'"))
                        End If

                        If (mIn1M <> "Null") Then
                            mIn1M = ("'" _
                                        + (mIn1M + "'"))
                        End If

                        If (mout1M <> "Null") Then
                            mout1M = ("'" _
                                        + (mout1M + "'"))
                        End If

                        If (mIn2M <> "Null") Then
                            mIn2M = ("'" _
                                        + (mIn2M + "'"))
                        End If

                        If (mout2M <> "Null") Then
                            mout2M = ("'" _
                                        + (mout2M + "'"))
                        End If
                        If EmpGrpArr(EmpGrpId).g_OwMinus = "Y" Then
                            If mHoursworked > OwDur Then
                                mHoursworked = mHoursworked - OwDur
                            End If
                        End If

                        sSql = "Update tblTimeRegister Set MShiftStartTime=" & mShiftStartTime & ", MShiftEndTime=" & mShiftEndTime & ",MLunchStartTime=" & mLunchStartTime & ", " & _
                                " MLunchEndTime=" & mLunchEndTime & ", MHoursWorked=" & mHoursworked & ",MExcLunchHours=" & mExcLunchHours & ",MOtDuration=" & mOtDuration & ", " & _
                                " MOsDuration=" & mOSDuration & ",MOtAmount=" & motamount & ", MEarlyArrival=" & mEarlyArrival & ",MEarlyDeparture=" & mearlydeparture & ", " & _
                                " MLateArrival=" & mLateArrival & ", MLunchEarlyDeparture=" & mLunchEarlyDeparture & ", MLunchLateArrival=" & mLunchLateArrival & ", " & _
                                " MTotalLossHrs=" & mTotalLossHrs & ",MStatus='" & mStatus & "',MShiftAttended='" & mShiftAttended & "',mIn1=" & mIn1M & ",mIn2=" & mIn2M & ", " & _
                                " mOUT1=" & mout1M & ",mOUT2=" & mout2M & ",MIn1Mannual='" & mIn1Mannual & "',MIn2Mannual='" & mIn2Mannual & "',MOut1Mannual='" & mOut1Mannual & "', " & _
                                " MOut2Mannual='" & mOut2Mannual & "',MPresentValue=" & mPresentValue & ",MAbsentValue=" & mAbsentValue & " " & _
                                " where paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim(Microsoft.VisualBasic.ChrW(32)) & "' and dateoffice = '" & Proc_Date & "' "

                        'Cn.execute_NonQuery(sSql)
                        If servername = "Access" Then
                            'If con1.State <> ConnectionState.Open Then
                            '    con1.Open()
                            'End If

                            sSql = "Update tblTimeRegister Set MShiftStartTime=" & mShiftStartTime & ", MShiftEndTime=" & mShiftEndTime & ",MLunchStartTime=" & mLunchStartTime & ", " & _
                              " MLunchEndTime=" & mLunchEndTime & ", MHoursWorked=" & mHoursworked & ",MExcLunchHours=" & mExcLunchHours & ",MOtDuration=" & mOtDuration & ", " & _
                              " MOsDuration=" & mOSDuration & ",MOtAmount=" & motamount & ", MEarlyArrival=" & mEarlyArrival & ",MEarlyDeparture=" & mearlydeparture & ", " & _
                              " MLateArrival=" & mLateArrival & ", MLunchEarlyDeparture=" & mLunchEarlyDeparture & ", MLunchLateArrival=" & mLunchLateArrival & ", " & _
                              " MTotalLossHrs=" & mTotalLossHrs & ",MStatus='" & mStatus & "',MShiftAttended='" & mShiftAttended & "',mIn1=" & mIn1M & ",mIn2=" & mIn2M & ", " & _
                              " mOUT1=" & mout1M & ",mOUT2=" & mout2M & ",MIn1Mannual='" & mIn1Mannual & "',MIn2Mannual='" & mIn2Mannual & "',MOut1Mannual='" & mOut1Mannual & "', " & _
                              " MOut2Mannual='" & mOut2Mannual & "',MPresentValue=" & mPresentValue & ",MAbsentValue=" & mAbsentValue & " " & _
                              " where paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim(Microsoft.VisualBasic.ChrW(32)) & "' and FORMAT(dateoffice,'yyyy-MM-dd') = '" & Proc_Date & "' "

                            cmd1 = New OleDbCommand(sSql, con1Access)
                            cmd1.ExecuteNonQuery()
                            ' con1.Close()
                        Else
                            'If con.State <> ConnectionState.Open Then
                            '    con.Open()
                            'End If
                            cmd = New SqlCommand(sSql, conSQL)
                            cmd.ExecuteNonQuery()
                            'con.Close()
                        End If
                        'iT = (iT + 1)

                        ''SMS logic
                        'Dim shiftEndTime As Date
                        ''Dim request As HttpWebRequest
                        ''Dim response As HttpWebResponse = Nothing
                        'Dim Proc_DateTmp As DateTime = Convert.ToDateTime(Proc_Date)
                        'If g_SMSApplicable = "" Then
                        '    Load_SMS_Policy()
                        'End If
                        'If g_SMSApplicable = "Y" Then
                        '    If Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) <> "" And Proc_DateTmp.ToString("yyyy-MM-dd") = Now.ToString("yyyy-MM-dd") Then
                        '        'Late
                        '        If g_isLate = "Y" And mLateArrival > 0 Then
                        '            If mLateSMS = "N" Then 'temp comment
                        '                LateDur = Math.Truncate(mLateArrival / 60).ToString("00") & ":" & (mLateArrival Mod 60).ToString("00")
                        '                'LateDur = Min2Hr(mLateArrival)
                        '                LateSMSContent = rsEmp.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim & " " & g_LateContent1 & " " & LateDur & " " & g_LateContent2
                        '                ''url = "http://alerts.3mdigital.co.in/api/web2sms.php?workingkey=" & G_SmsKey & "&sender=" & G_SenderID & "&to=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString) & "&message=" & LateSMSContent & ""
                        '                'original() '
                        '                'url = "http://sms.mobtexting.com/index.php/api?method=sms.normal&api_key=" & Trim(G_SmsKey) & "&to=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) & "&sender=" & Trim(G_SenderID) & "&message=" & Trim(LateSMSContent) & "&flash=0&unicode=1"
                        '                'url = "http://sms.jbssolution.in/submitsms.jsp?user=TESCHOTI&key=68e0655580XX&mobile=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) & "&message=" & Trim(LateSMSContent) & "&senderid=TESCHT&accusage=1&unicode=1"
                        '                'Content = frmMdiMain.Inet1.OpenURL(url)
                        '                Try
                        '                    sendSMS(Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim), LateSMSContent)
                        '                    'request = DirectCast(WebRequest.Create(url), HttpWebRequest)
                        '                    'response = DirectCast(request.GetResponse(), HttpWebResponse)
                        '                    If servername = "Access" Then
                        '                        sSql = "Update tblTimeRegister Set LateSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and format(dateoffice,'yyyy-MM-dd') = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                        '                        cmd1 = New OleDbCommand(sSql, con1Access)
                        '                        cmd1.ExecuteNonQuery()
                        '                    Else
                        '                        sSql = "Update tblTimeRegister Set LateSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and dateoffice = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                        '                        cmd = New SqlCommand(sSql, conSQL)
                        '                        cmd.ExecuteNonQuery()
                        '                    End If
                        '                Catch ex As Exception

                        '                End Try
                        '            End If 'temp comment
                        '        End If
                        '        'IN
                        '        mIn1M = Replace(mIn1M, "'", "")
                        '        If rsTime.Tables(0).Rows(iT).Item("SHIFT").ToString.Trim <> "OFF" And rsTime.Tables(0).Rows(iT).Item("SHIFT").ToString.Trim <> "IGN" Then
                        '            If g_isInSMS = "Y" And IsDate(mIn1M) Then
                        '                If mInSMS = "N" Then  'temp comment
                        '                    InPunch = Convert.ToDateTime(mIn1M).ToString("HH:mm")
                        '                    InSMSContent = rsEmp.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim & " " & g_InContent1 & " " & InPunch & " " & g_InContent2
                        '                    'InSMSContent = g_OutContent1 & " " & InPunch & " " & g_OutContent2 'for anil's customer
                        '                    Try
                        '                        sendSMS(Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim), InSMSContent)
                        '                        If servername = "Access" Then
                        '                            sSql = "Update tblTimeRegister Set inSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and format(dateoffice,'yyyy-MM-dd') = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                        '                            cmd1 = New OleDbCommand(sSql, con1Access)
                        '                            cmd1.ExecuteNonQuery()
                        '                        Else
                        '                            sSql = "Update tblTimeRegister Set inSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and dateoffice = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                        '                            cmd = New SqlCommand(sSql, conSQL)
                        '                            cmd.ExecuteNonQuery()
                        '                        End If
                        '                    Catch ex As Exception

                        '                    End Try
                        '                    'Cn.Execute(sSql)
                        '                End If  'temp comment
                        '            End If
                        '            'Out
                        '            mout2M = Replace(mout2M, "'", "")
                        '            mShiftEndTime = Replace(mShiftEndTime, "'", "")
                        '            If g_isOutSMS = "Y" And IsDate(mout2M) Then
                        '                If mOutSMS = "N" Then  'temp comment
                        '                    shiftEndTime = mShiftEndTime
                        '                    shiftEndTime = DateAdd("n", g_OutSMSAfter, shiftEndTime)
                        '                    If CDate(mout2M) > CDate(shiftEndTime) Then
                        '                        OutPunch = Convert.ToDateTime(mout2M).ToString("HH:mm") ' Format(mOUT2M, "HH:mm")
                        '                        OutSMSContent = rsEmp.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim & " " & g_OutContent1 & " " & OutPunch & " " & g_OutContent2
                        '                        'OutSMSContent = InSMSContent & " " & OutPunch & " " & InSMSContent 'for anil's customer
                        '                        Try
                        '                            sendSMS(Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim), OutSMSContent)
                        '                            If servername = "Access" Then
                        '                                sSql = "Update tblTimeRegister Set OutSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and format(dateoffice,'yyyy-MM-dd') = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                        '                                cmd1 = New OleDbCommand(sSql, con1Access)
                        '                                cmd1.ExecuteNonQuery()
                        '                            Else
                        '                                sSql = "Update tblTimeRegister Set OutSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and dateoffice = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                        '                                cmd = New SqlCommand(sSql, conSQL)
                        '                                cmd.ExecuteNonQuery()
                        '                            End If
                        '                        Catch ex As Exception

                        '                        End Try
                        '                        'Cn.Execute(sSql)
                        '                    End If
                        '                End If  'temp comment
                        '            End If
                        '        End If
                        '    End If
                        'End If
                        ''End SMS logic
                        iT = (iT + 1)
                    Loop
                End If
                iE = (iE + 1)
            Loop

        End If
        If servername = "Access" Then
            If con1Access.State <> ConnectionState.Closed Then
                con1Access.Close()
            End If
        Else
            If conSQL.State <> ConnectionState.Closed Then
                conSQL.Close()
            End If
        End If
        ''XtraMaster.LabelControl4.Text = ""
        ''Application.DoEvents()
        ' par.toolStripStatusLabel1.Text = "Timewatch Application";
    End Sub
    Public Sub Process_AllRTC(ByVal mindate As DateTime, ByVal maxdate As DateTime, ByVal paycodefrom As String, ByVal paycodeto As String, ByVal EmpGrpId As Integer)
        'XtraMaster.LabelControl4.Text = "Processing for Pycode" & paycodefrom & "  Date: " & mindate.ToString("dd/MM/yyyy")
        'Application.DoEvents()       
        'Load_Corporate_PolicySql()
        Dim mCtr As Integer = 0
        Dim OwDur As Integer = 0
        Dim tout As String = Nothing
        Dim mReason_OutWork As String = Nothing
        Dim rsOutWork As DataSet = New DataSet()
        Dim Qy As DataSet = New DataSet()
        Dim rsTime As DataSet = New DataSet()
        Dim rsPunch As DataSet = New DataSet()
        Dim mDate1 As String = Nothing
        Dim mDate2 As String = Nothing
        Dim Day_Hour As Integer = 0
        Dim mP_Flag As String = Nothing
        Dim mReasonCode As String = Nothing
        Dim strFirstPunchReasonCode As String = Nothing
        Dim strLastPunchReasonCode As String = Nothing
        Dim Proc_Date As String = ""
        Dim rsEmp As DataSet = New DataSet()
        Dim HCOMPANYCODE As String = ""
        Dim HDEPARTMENTCODE As String = ""
        Dim mmin_date As DateTime = System.DateTime.MinValue
        Dim mmax_date As DateTime = System.DateTime.MinValue
        Dim mdate_2 As DateTime = System.DateTime.MinValue
        Dim Out_2 As DateTime = System.DateTime.MinValue
        'Dim g_FromPaycode As String = ""
        'Dim g_ToPaycode As String = ""
        'Dim mIn1 As String = ""
        'Dim mIn2 As String = ""
        'Dim mout1 As String = ""
        'Dim mout2 As String = ""
        mIn1 = ""
        mIn2 = ""
        mout1 = ""
        mout2 = ""
        Dim mIn1Mannual As String = ""
        Dim mIn2Mannual As String = ""
        Dim mOut1Mannual As String = ""
        Dim mOut2Mannual As String = ""
        'Dim g_NightShiftFourPunch As String = ""
        'Dim g_o_end As String = ""
        'Dim g_s_end As String = ""
        Dim dateoffice As DateTime = System.DateTime.MinValue
        'g_FromPaycode = paycodefrom
        'g_ToPaycode = paycodeto
        mmin_date = mindate
        mmax_date = maxdate
        'g_o_end = getinfo.g_o_end
        'g_s_end = getinfo.g_s_end
        If servername = "Access" Then
            If con1Access.State <> ConnectionState.Open Then
                con1Access.Open()
            End If
        Else
            If conSQL.State <> ConnectionState.Open Then
                conSQL.Open()
            End If
        End If

        sSql = "Select a.*,b.companycode,b.departmentcode, b.BRANCHCODE, b.Telephone1, b.EMPNAME from tblemployeeshiftmaster a,tblemployee b where a.paycode=b.paycode and a.ISROUNDTHECLOCKWORK='Y' AND A.PAYCODE='" & paycodefrom & "'" ' AND A.PAYCODE<='" & g_ToPaycode & "'"
        rsEmp = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1Access)
            adapA.Fill(rsEmp)
        Else
            adap = New SqlDataAdapter(sSql, conSQL)
            adap.Fill(rsEmp)
        End If
        'rsEmp = Cn.FillDataSet(sSql)
        If rsEmp.Tables(0).Rows.Count > 0 Then
            For iE As Integer = 0 To rsEmp.Tables(0).Rows.Count - 1
                HCOMPANYCODE = rsEmp.Tables(0).Rows(iE)("companycode").ToString().Trim()
                HDEPARTMENTCODE = rsEmp.Tables(0).Rows(iE)("departmentcode").ToString().Trim()
                Load_HolidaySql(HCOMPANYCODE, HDEPARTMENTCODE, rsEmp.Tables(0).Rows(iE)("BRANCHCODE").ToString().Trim)
                Load_ShiftMasterSql()
                If Not (String.IsNullOrEmpty(rsEmp.Tables(0).Rows(iE)("MaxDayMin").ToString().Trim())) Then
                    Day_Hour = Convert.ToInt32(rsEmp.Tables(0).Rows(iE)("MaxDayMin").ToString().Trim())
                End If

                Dim sSql1, sSql2, sSql3 As String
                sSql = "UPDATE MACHINERAWPUNCH SET P_DAY='N' WHERE PAYCODE='" & rsEmp.Tables(0).Rows(iE)("PAYCODE").ToString().Trim().Trim() & "' AND OFFICEPUNCH>='" & (Convert.ToDateTime(mmin_date).AddDays(1)).ToString("yyyy-MM-dd") & " 00:00:00'"
                sSql1 = "delete from tblOutWorkRegister where paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim() & "' and DateOffice between '" & mmin_date.ToString("yyyy-MM-dd") & " 00:00:00' and '" & mmax_date.ToString("yyyy-MM-dd") & " 23:59:59'"
                sSql2 = "Update tbltimeregister Set in1=null,in2=null,out1=null,out2=null " & " where paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim(" "c) & "' and DateOffice between '" & mmin_date.ToString("yyyy-MM-dd") & " 00:00:00' and '" & mmax_date.ToString("yyyy-MM-dd") & " 23:59:59'"
                sSql3 = "Select * from tbltimeregister where paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim(" "c) & "' and DateOffice between '" & mmin_date.ToString("yyyy-MM-dd") & " 00:00:00' and '" & mmax_date.ToString("yyyy-MM-dd") & " 23:59:59' Order By dateoffice"
                If servername = "Access" Then
                    'If con1.State <> ConnectionState.Open Then
                    '    con1.Open()
                    'End If
                    sSql = "UPDATE MACHINERAWPUNCH SET P_DAY='N' WHERE PAYCODE='" & rsEmp.Tables(0).Rows(iE)("PAYCODE").ToString().Trim().Trim() & "' AND FORMAT(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss')>='" & (Convert.ToDateTime(mmin_date).AddDays(1)).ToString("yyyy-MM-dd") & " 00:00:00'"
                    sSql1 = "delete from tblOutWorkRegister where paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim() & "' and FORMAT(DateOffice,'yyyy-MM-dd HH:mm:ss') between '" & mmin_date.ToString("yyyy-MM-dd") & " 00:00:00' and '" & mmax_date.ToString("yyyy-MM-dd") & " 23:59:59'"
                    sSql2 = "Update tbltimeregister Set in1=null,in2=null,out1=null,out2=null " & " where paycode='" + rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim(" "c) & "' and FORMAT(DateOffice,'yyyy-MM-dd HH:mm:ss') between '" & mmin_date.ToString("yyyy-MM-dd") & " 00:00:00' and '" & mmax_date.ToString("yyyy-MM-dd") & " 23:59:59'"
                    sSql3 = "Select * from tbltimeregister where paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim(" "c) & "' and FORMAT(DateOffice,'yyyy-MM-dd HH:mm:ss') between '" & mmin_date.ToString("yyyy-MM-dd") & " 00:00:00' and '" & mmax_date.ToString("yyyy-MM-dd") & " 23:59:59' Order By dateoffice"

                    rsTime = New DataSet
                    cmd1 = New OleDbCommand(sSql, con1Access)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand(sSql1, con1Access)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand(sSql2, con1Access)
                    cmd1.ExecuteNonQuery()
                    'con1.Close()
                    adapA1 = New OleDbDataAdapter(sSql3, con1Access)
                    adapA1.Fill(rsTime)
                Else
                    'If con.State <> ConnectionState.Open Then
                    '    con.Open()
                    'End If
                    cmd = New SqlCommand(sSql, conSQL)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(sSql1, conSQL)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(sSql2, conSQL)
                    cmd.ExecuteNonQuery()
                    'con.Close()
                    adap1 = New SqlDataAdapter(sSql3, conSQL)
                    adap1.Fill(rsTime)
                End If

                'rsTime = Cn.FillDataSet(sSql)
                mReasonCode = ""
                strFirstPunchReasonCode = ""
                strLastPunchReasonCode = ""
                If rsTime.Tables(0).Rows.Count > 0 Then
                    For iT As Integer = 0 To rsTime.Tables(0).Rows.Count - 1
                        XtraMasterTest.LabelControlStatus.Text = "Processing for Paycode " & paycodefrom & "  Date: " & Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("dd/MM/yyyy")
                        Application.DoEvents()
                        Proc_Date = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("yyyy-MM-dd")
                        mIn1 = "Null"
                        mIn2 = "Null"
                        mout1 = "Null"
                        mout2 = "Null"
                        mShiftStartTime = "Null"
                        mShiftEndTime = "Null"
                        mLunchStartTime = "Null"
                        mLunchEndTime = "Null"
                        mIn1Mannual = "N"
                        mIn2Mannual = "N"
                        mOut1Mannual = "N"
                        mOut2Mannual = "N"
                        OwDur = 0
                        'For SMS
                        If rsTime.Tables(0).Rows(iT)("LateSMS").ToString.Trim = "" Then
                            mLateSMS = "N"
                        Else
                            mLateSMS = Trim(rsTime.Tables(0).Rows(iT)("LateSMS").ToString)
                        End If
                        If rsTime.Tables(0).Rows(iT)("InSMS").ToString.Trim = "" Then
                            mInSMS = "N"
                        Else
                            mInSMS = Trim(rsTime.Tables(0).Rows(iT)("InSMS").ToString)
                        End If
                        If rsTime.Tables(0).Rows(iT)("OutSMS").ToString.Trim = "" Then
                            mOutSMS = "N"
                        Else
                            mOutSMS = Trim(rsTime.Tables(0).Rows(iT)("OutSMS").ToString)
                        End If

                        mDate1 = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("yyyy-MM-dd")
                        mDate2 = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).AddDays(1).ToString("yyyy-MM-dd")
                        If rsEmp.Tables(0).Rows(iE)("IsOutWork").ToString().Trim() = "Y" Then
                            sSql = "Select * from machinerawpunch where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString().Trim().Trim(" "c) & "' and officepunch between '" & mDate1 & " 00:00:00' and '" & mDate2 & " " + Convert.ToDateTime(EmpGrpArr(EmpGrpId).g_o_end).ToString("HH:mm") & "' " & " and p_day<>'Y' Order By OfficePunch"
                        Else
                            sSql = "Select * from machinerawpunch where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString().Trim().Trim(" "c) & "' and officepunch between '" & mDate1 & " 00:00:00' and '" & mDate2 & " 23:59:00" & "' " & " and p_day<>'Y' Order By OfficePunch"
                        End If
                        rsPunch = New DataSet
                        If servername = "Access" Then
                            If rsEmp.Tables(0).Rows(iE)("IsOutWork").ToString().Trim() = "Y" Then
                                sSql = "Select * from machinerawpunch where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString().Trim().Trim(" "c) & "' and FORMAT(officepunch,'yyyy-MM-dd HH:mm:ss') between '" & mDate1 & " 00:00:00' and '" & mDate2 & " " + Convert.ToDateTime(EmpGrpArr(EmpGrpId).g_o_end).ToString("HH:mm") & "' " & " and p_day<>'Y' Order By OfficePunch"
                            Else
                                sSql = "Select * from machinerawpunch where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString().Trim().Trim(" "c) & "' and FORMAT(officepunch,'yyyy-MM-dd HH:mm:ss') between '" & mDate1 & " 00:00:00' and '" & mDate2 & " 23:59:00" & "' " & " and p_day<>'Y' Order By OfficePunch"
                            End If
                            adapA2 = New OleDbDataAdapter(sSql, con1Access)
                            adapA2.Fill(rsPunch)
                        Else
                            adap2 = New SqlDataAdapter(sSql, conSQL)
                            adap2.Fill(rsPunch)
                        End If
                        'rsPunch = Cn.FillDataSet(sSql)

                        If rsEmp.Tables(0).Rows(iE)("IsPunchAll").ToString().Trim() = "N" Then
                            GoTo AsgIt
                        End If

                        If rsPunch.Tables(0).Rows.Count > 0 AndAlso rsEmp.Tables(0).Rows(iE)("IsOutWork").ToString().Trim() = "Y" Then
                            mIn1 = Convert.ToDateTime(rsPunch.Tables(0).Rows(0)("Officepunch")).ToString("yyyy-MM-dd HH:mm:ss")
                            mDate2 = Convert.ToDateTime(mIn1).AddMinutes(Day_Hour).ToString("yyyy-MM-dd HH:mm")
                            If Convert.ToDateTime(mDate2) > Convert.ToDateTime(Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).AddDays(1).ToString("yyyy-MM-dd") & " " + Convert.ToDateTime(EmpGrpArr(EmpGrpId).g_o_end).ToString("HH:mm")) Then
                                mDate2 = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).AddDays(1).ToString("yyyy-MM-dd") & " " + Convert.ToDateTime(EmpGrpArr(EmpGrpId).g_o_end).ToString("HH:mm")
                            End If

                            sSql = "Select * from machinerawpunch where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString().Trim().Trim(" "c) & "' and officepunch between '" & mIn1 & "' And '" & mDate2 & "'" & " and p_day<>'Y' Order By OfficePunch"
                            rsPunch = New DataSet
                            If servername = "Access" Then
                                sSql = "Select * from machinerawpunch where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString().Trim().Trim(" "c) & "' and FORMAT(officepunch,'yyyy-MM-dd HH:mm:ss') between '" & mIn1 & "' And '" & mDate2 & "'" & " and p_day<>'Y' Order By OfficePunch"
                                adapA3 = New OleDbDataAdapter(sSql, con1Access)
                                adapA3.Fill(rsPunch)
                            Else
                                adap3 = New SqlDataAdapter(sSql, conSQL)
                                adap3.Fill(rsPunch)
                            End If
                            'rsPunch = Cn.FillDataSet(sSql)
                        End If

                        If rsPunch.Tables(0).Rows.Count > 2 AndAlso rsEmp.Tables(0).Rows(iE)("IsOutWork").ToString().Trim() = "Y" Then
                            For iP As Integer = 0 To rsPunch.Tables(0).Rows.Count - 1
                                mIn1 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm:ss")
                                mDate1 = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).AddDays(1).ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(EmpGrpArr(EmpGrpId).g_s_end).ToString("HH:mm")
                                If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")), Convert.ToDateTime(mDate1))) < 0 Then
                                    mIn1 = "Null"
                                    GoTo AsgIt
                                End If

                                mIn1Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString().Trim
                                If Not (String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString())) AndAlso rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString().Trim(" "c).Length <> 0 Then
                                    mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                    strFirstPunchReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                Else
                                    strFirstPunchReasonCode = ""
                                End If

                                If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Day, Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("Dateoffice")).ToString("yyyy-MM-dd"), Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd"))) > 0 Then
                                    sSql = "Update MachineRawPunch Set P_Day='Y' Where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString().Trim() & "' And OfficePunch = '" & mIn1 & "'"
                                    If servername = "Access" Then
                                        'If con1.State <> ConnectionState.Open Then
                                        '    con1.Open()
                                        'End If
                                        sSql = sSql.Replace("OfficePunch", "FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss')")
                                        cmd1 = New OleDbCommand(sSql, con1Access)
                                        cmd1.ExecuteNonQuery()
                                        'con1.Close()
                                    Else
                                        'If con.State <> ConnectionState.Open Then
                                        '    con.Open()
                                        'End If
                                        cmd = New SqlCommand(sSql, conSQL)
                                        cmd.ExecuteNonQuery()
                                        'con.Close()
                                    End If
                                    'Cn.execute_NonQuery(sSql)
                                End If

                                iP = Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1
                                mout2 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm:ss")
                                mOut2Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString()
                                If Not (String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString())) AndAlso rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString().Trim(" "c).Length <> 0 Then
                                    mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                    strLastPunchReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                Else
                                    strLastPunchReasonCode = ""
                                End If

                                If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Day, Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("Dateoffice")).ToString("yyyy-MM-dd"), Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd"))) > 0 Then
                                    sSql = "Update MachineRawPunch Set P_Day='Y' Where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString().Trim() & "' And OfficePunch = '" & mout2 & "'"
                                    If servername = "Access" Then
                                        'If con1.State <> ConnectionState.Open Then
                                        '    con1.Open()
                                        'End If
                                        sSql = "Update MachineRawPunch Set P_Day='Y' Where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString().Trim() & "' And FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss') = '" & mout2 & "'"
                                        cmd1 = New OleDbCommand(sSql, con1Access)
                                        cmd1.ExecuteNonQuery()
                                        'con1.Open()
                                    Else
                                        'If con.State <> ConnectionState.Open Then
                                        '    con.Open()
                                        'End If
                                        cmd = New SqlCommand(sSql, conSQL)
                                        cmd.ExecuteNonQuery()
                                        'con.Close()
                                    End If
                                    'Cn.execute_NonQuery(sSql)
                                End If

                                tout = rsPunch.Tables(0).Rows(iP)("Officepunch").ToString()
                                Dim rO As Integer = 0
                                sSql = "Select * from tblOutWorkRegister where paycode='" & rsTime.Tables(0).Rows(iT)("paycode").ToString().Trim().Trim(" "c) & "' And " & "DateOffice='" & mDate1 & "'"
                                Dim adap4 As SqlDataAdapter
                                Dim adapA4 As OleDbDataAdapter
                                rsOutWork = New DataSet
                                If servername = "Access" Then
                                    sSql = "Select * from tblOutWorkRegister where paycode='" & rsTime.Tables(0).Rows(iT)("paycode").ToString.Trim & "' And FORMAT(DateOffice, 'yyyy-MM-dd')='" & mDate1 & "'"
                                    adapA4 = New OleDbDataAdapter(sSql, con1Access)
                                    adapA4.Fill(rsOutWork)
                                Else
                                    adap4 = New SqlDataAdapter(sSql, conSQL)
                                    adap4.Fill(rsOutWork)
                                End If
                                'rsOutWork = Cn.FillDataSet(sSql)

                                Dim rsOutWorkpaycode As String
                                Dim rsOutWorkdateOffice As String
                                Dim rsOutWorkIn As String
                                Dim rsOutWorkRIn As String
                                Dim rsOutWorkOut As String
                                Dim rsOutWorROut As String

                                If rsOutWork.Tables(0).Rows.Count < 1 Then
                                    rsOutWork.Tables(0).Rows.Add(rsOutWork.Tables(0).NewRow())
                                    rsOutWork.Tables(0).Rows(rO)("paycode") = rsTime.Tables(0).Rows(iT)("paycode")
                                    rsOutWork.Tables(0).Rows(rO)("dateOffice") = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("yyyy-MM-dd HH:mm")

                                    rsOutWorkpaycode = rsTime.Tables(0).Rows(iT)("paycode")
                                    rsOutWorkdateOffice = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).ToString("yyyy-MM-dd HH:mm")

                                    Dim tmpstr As String = "Insert into tblOutWorkRegister (paycode,dateOffice) values " & _
                                 "( '" & rsOutWorkpaycode & "','" & rsOutWorkdateOffice & "')"
                                    If servername = "Access" Then
                                        cmd1 = New OleDbCommand(tmpstr, con1Access)
                                        cmd1.ExecuteNonQuery()
                                    Else
                                        cmd = New SqlCommand(tmpstr, conSQL)
                                        cmd.ExecuteNonQuery()
                                    End If

                                End If

                                iP = 0
                                iP = 1
                                mCtr = 1
                                OwDur = 0
                                mReason_OutWork = "N"

                                While iP <> Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1
                                    If tout = rsPunch.Tables(0).Rows(iP)("Officepunch").ToString() Then
                                        Exit While
                                    End If

                                    If mCtr <= 10 Then
                                        rsOutWorkIn = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                        rsOutWork.Tables(0).Rows(rO)("In" & mCtr.ToString().Trim(" "c)) = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                        If Not (String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString())) AndAlso rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString().Trim(" "c) <> "" Then
                                            rsOutWorkRIn = rsPunch.Tables(0).Rows(iP)("ReasonCode")
                                            rsOutWork.Tables(0).Rows(rO)("RIn" & mCtr.ToString().Trim(" "c)) = rsPunch.Tables(0).Rows(iP)("ReasonCode")
                                            mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                            If (rsPunch.Tables(0).Rows(iP)("ReasonCode")).ToString().Trim(" "c).Length <> 0 Then
                                                mReason_OutWork = "Y"
                                            Else
                                                mReason_OutWork = "N"
                                            End If
                                        End If
                                    End If

                                    If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Day, Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("Dateoffice")).ToString("yyyy-MM-dd"), Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd"))) > 0 Then
                                        sSql = "Update MachineRawPunch Set P_Day='Y' Where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString().Trim() & "' And OfficePunch = '" & Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                        If servername = "Access" Then
                                            'If con1.State <> ConnectionState.Open Then
                                            '    con1.Open()
                                            'End If
                                            sSql = sSql.Replace("OfficePunch", "FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss')")
                                            cmd1 = New OleDbCommand(sSql, con1Access)
                                            cmd1.ExecuteNonQuery()
                                            'con1.Close()
                                        Else
                                            'If con.State <> ConnectionState.Open Then
                                            '    con.Open()
                                            'End If
                                            cmd = New SqlCommand(sSql, conSQL)
                                            cmd.ExecuteNonQuery()
                                            'con.Close()
                                        End If
                                        'Cn.execute_NonQuery(sSql)
                                    End If

                                    iP = iP + 1
                                    If iP > Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1 Then
                                        Exit While
                                    End If
                                    If tout = rsPunch.Tables(0).Rows(iP)("Officepunch").ToString() Then
                                        Exit While
                                    End If

                                    If mCtr <= 10 Then
                                        rsOutWorkOut = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")

                                        rsOutWork.Tables(0).Rows(rO)("Out" & mCtr.ToString().Trim(" "c)) = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")).ToString("yyyy-MM-dd HH:mm")
                                        If Not (String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString())) AndAlso rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString().Trim(" "c) <> "" Then
                                            rsOutWorROut = rsPunch.Tables(0).Rows(iP)("ReasonCode")
                                            rsOutWork.Tables(0).Rows(rO)("ROut" & mCtr.ToString().Trim(" "c)) = rsPunch.Tables(0).Rows(iP)("ReasonCode")
                                            mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                            If (rsPunch.Tables(0).Rows(iP)("ReasonCode")).ToString().Trim(" "c).Length <> 0 Then
                                                mReason_OutWork = "Y"
                                            Else
                                                mReason_OutWork = "N"
                                            End If
                                        End If

                                        OwDur = OwDur + Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(rsOutWork.Tables(0).Rows(rO)("In" & mCtr.ToString().Trim(" "c))), Convert.ToDateTime(rsOutWork.Tables(0).Rows(rO)("Out" & mCtr.ToString().Trim(" "c)))))
                                    End If

                                    If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Day, Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("Dateoffice")).ToString("yyyy-MM-dd"), Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd"))) > 0 Then
                                        sSql = "Update MachineRawPunch Set P_Day='Y' Where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString().Trim() & "' And OfficePunch = '" & Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                        If servername = "Access" Then
                                            'If con1.State <> ConnectionState.Open Then
                                            '    con1.Open()
                                            'End If
                                            sSql = "Update MachineRawPunch Set P_Day='Y' Where paycode='" & rsEmp.Tables(0).Rows(iE)("paycode").ToString().Trim() & "' And FORMAT(OfficePunch,'yyyy-MM-dd HH:mm') = '" & Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm") & "'"
                                            cmd1 = New OleDbCommand(sSql, con1Access)
                                            cmd1.ExecuteNonQuery()
                                            'con1.Close()
                                        Else
                                            'If con.State <> ConnectionState.Open Then
                                            '    con.Open()
                                            'End If
                                            cmd = New SqlCommand(sSql, conSQL)
                                            cmd.ExecuteNonQuery()
                                            'con.Close()
                                        End If
                                        'Cn.execute_NonQuery(sSql)
                                    End If

                                    Dim tmpstr As String = "Update tblOutWorkRegister set OUTWORKDURATION = '" & OwDur & "',Reason_OutWork = '" & mReason_OutWork & "', " & "Out" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkOut & "', " & "ROut" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorROut & "', " & "RIn" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkRIn & "', " & "In" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkIn & "' where paycode = '" & rsOutWorkpaycode & "' and dateOffice='" & rsOutWorkdateOffice & "'"
                                    If servername = "Access" Then
                                        tmpstr = "Update tblOutWorkRegister set OUTWORKDURATION = '" & OwDur & "',Reason_OutWork = '" & mReason_OutWork & "', " & "Out" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkOut & "', " & "ROut" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorROut & "', " & "RIn" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkRIn & "', " & "In" & mCtr.ToString().Trim(" "c) & " = '" & rsOutWorkIn & "' where paycode = '" & rsOutWorkpaycode & "' and FORMAT(dateOffice,'yyyy-MM-dd HH:mm')='" & rsOutWorkdateOffice & "'"
                                        cmd1 = New OleDbCommand(tmpstr, con1Access)
                                        cmd1.ExecuteNonQuery()
                                    Else
                                        cmd = New SqlCommand(tmpstr, conSQL)
                                        cmd.ExecuteNonQuery()
                                    End If
                                    iP = iP + 1
                                    mCtr = mCtr + 1
                                    If iP > Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1 Then
                                        Exit While
                                    End If
                                    If (mCtr > 10) Then  'New as per ajitesh.. cannot be greater tha 10
                                        Exit While
                                    End If
                                End While

                                'rsOutWork.AcceptChanges()
                                GoTo AsgIt
                            Next
                        ElseIf rsEmp.Tables(0).Rows(iE)("IsOutWork").ToString().Trim() = "Y" AndAlso rsPunch.Tables(0).Rows.Count > 0 Then
                            For iP As Integer = 0 To rsPunch.Tables(0).Rows.Count - 1
                                While rsPunch.Tables(0).Rows(iP)("p_day").ToString() = "Y" AndAlso Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Day, Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("OfficePunch")), Convert.ToDateTime(mDate1))) = 0
                                    iP = iP + 1
                                    If iP = Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1 Then
                                        Exit While
                                    End If
                                End While

                                If iP > Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1 Then
                                    GoTo AsgIt
                                End If

                                mDate1 = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).AddDays(1).ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(EmpGrpArr(EmpGrpId).g_s_end).ToString("HH:mm")
                                If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")), Convert.ToDateTime(mDate1))) < 0 Then
                                    mIn1 = "Null"
                                    GoTo AsgIt
                                End If

                                If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")), Convert.ToDateTime(mDate1))) >= 0 Then
                                    mIn1 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm:ss")
                                    mIn1Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString.Trim
                                    If Not (String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString())) AndAlso rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString().Trim(" "c).Length <> 0 Then
                                        mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                        strFirstPunchReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                    Else
                                        mReasonCode = ""
                                        strFirstPunchReasonCode = ""
                                    End If

                                    If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Day, Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("Dateoffice")).ToString("yyyy-MM-dd"), Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd"))) > 0 Then
                                        sSql = "Update MachineRawPunch Set P_Day='Y' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And OfficePunch = '" & mIn1 & "'"
                                        mP_Flag = "Y"
                                    Else
                                        sSql = "Update MachineRawPunch Set P_Day='N' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And OfficePunch = '" & mIn1 & "'"
                                        mP_Flag = "N"
                                    End If

                                    If String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("P_Day").ToString()) Then
                                        If servername = "Access" Then
                                            'If con1.State <> ConnectionState.Open Then
                                            '    con1.Open()
                                            'End If
                                            sSql = sSql.Replace("OfficePunch", "FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss')")
                                            cmd1 = New OleDbCommand(sSql, con1Access)
                                            cmd1.ExecuteNonQuery()
                                            'con1.Close()
                                        Else
                                            'If con.State <> ConnectionState.Open Then
                                            '    con.Open()
                                            'End If
                                            cmd = New SqlCommand(sSql, conSQL)
                                            cmd.ExecuteNonQuery()
                                            'con.Close()
                                        End If
                                        'Cn.execute_NonQuery(sSql)
                                    Else
                                        If mP_Flag <> rsPunch.Tables(0).Rows(iP)("P_Day").ToString() Then
                                            If servername = "Access" Then
                                                'If con1.State <> ConnectionState.Open Then
                                                '    con1.Open()
                                                'End If
                                                sSql = sSql.Replace("OfficePunch", "FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss')")
                                                cmd1 = New OleDbCommand(sSql, con1Access)
                                                cmd1.ExecuteNonQuery()
                                                'con1.Close()
                                            Else
                                                'If con.State <> ConnectionState.Open Then
                                                '    con.Open()
                                                'End If
                                                cmd = New SqlCommand(sSql, conSQL)
                                                cmd.ExecuteNonQuery()
                                                'con.Close()
                                            End If
                                            'Cn.execute_NonQuery(sSql)
                                        End If
                                    End If
                                Else
                                    GoTo AsgIt
                                End If

                                iP = iP + 1
                                If iP > Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1 Then
                                    GoTo AsgIt
                                End If

                                mout1 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm:ss")
                                mOut1Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString.Trim
                                If Not (String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString())) And rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString().Trim(" "c).Length <> 0 Then
                                    mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                    strLastPunchReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                Else
                                    If mReasonCode.Trim(" "c).Length <= 0 Then
                                        mReasonCode = ""
                                        strLastPunchReasonCode = ""
                                    End If
                                End If

                                If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Day, Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("Dateoffice")).ToString("yyyy-MM-dd"), Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd"))) > 0 Then
                                    sSql = "Update MachineRawPunch Set P_Day='Y' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And OfficePunch = '" & mout1 & "'"
                                    mP_Flag = "Y"
                                Else
                                    sSql = "Update MachineRawPunch Set P_Day='N' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And OfficePunch = '" & mout1 & "'"
                                    mP_Flag = "N"
                                End If

                                If String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("P_Day").ToString()) Then
                                    If servername = "Access" Then
                                        'If con1.State <> ConnectionState.Open Then
                                        '    con1.Open()
                                        'End If
                                        sSql = sSql.Replace("OfficePunch", "FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss')")
                                        cmd1 = New OleDbCommand(sSql, con1Access)
                                        cmd1.ExecuteNonQuery()
                                        'con1.Close()
                                    Else
                                        'If con.State <> ConnectionState.Open Then
                                        '    con.Open()
                                        'End If                                        
                                        cmd = New SqlCommand(sSql, conSQL)
                                        cmd.ExecuteNonQuery()
                                        'con.Close()
                                    End If
                                    'Cn.execute_NonQuery(sSql)
                                Else
                                    If mP_Flag <> rsPunch.Tables(0).Rows(iP)("P_Day") Then
                                        If servername = "Access" Then
                                            'If con1.State <> ConnectionState.Open Then
                                            '    con1.Open()
                                            'End If
                                            sSql = sSql.Replace("OfficePunch", "FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss')")
                                            cmd1 = New OleDbCommand(sSql, con1Access)
                                            cmd1.ExecuteNonQuery()
                                            'con1.Close()
                                        Else
                                            'If con.State <> ConnectionState.Open Then
                                            '    con.Open()
                                            'End If
                                            cmd = New SqlCommand(sSql, conSQL)
                                            cmd.ExecuteNonQuery()
                                            'con.Close()
                                        End If
                                        'Cn.execute_NonQuery(sSql)
                                    End If
                                End If

                                GoTo AsgIt
                            Next
                        ElseIf rsPunch.Tables(0).Rows.Count > 0 Then
                            For iP As Integer = 0 To rsPunch.Tables(0).Rows.Count - 1
                                While rsPunch.Tables(0).Rows(iP)("p_day").ToString() = "Y" AndAlso Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Day, Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("OfficePunch")), Convert.ToDateTime(mDate1))) = 0
                                    iP = iP + 1
                                    If iP = Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1 Then
                                        Exit While
                                    End If
                                End While

                                ''NITIN  MANUPULATION(ONLY FOR 2 PUNCH)
                                'If (rsEmp.Tables(0).Rows(iE)("Two").ToString().Trim() = "Y") Then
                                '    If iP >= Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1 Then
                                '        GoTo AsgIt
                                '    End If
                                'End If
                                ''END NITIN  MANUPULATION

                                If iP > Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1 Then
                                    GoTo AsgIt
                                End If


                                mDate1 = Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("dateoffice")).AddDays(1).ToString("yyyy-MM-dd") '& " " & Convert.ToDateTime(EmpGrpArr(EmpGrpId).g_s_end).ToString("HH:mm")
                                If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")), Convert.ToDateTime(mDate1))) < 0 Then
                                    mIn1 = "Null"
                                    GoTo AsgIt
                                End If

                                If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("Officepunch")), Convert.ToDateTime(mDate1))) >= 0 Then
                                    mIn1 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm:ss")
                                    mIn1Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString.Trim
                                    If Not (String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString())) And rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString().Trim(" "c).Length <> 0 Then
                                        mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                        strFirstPunchReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                    Else
                                        mReasonCode = ""
                                        strFirstPunchReasonCode = ""
                                    End If

                                    If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Day, Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("Dateoffice")).ToString("yyyy-MM-dd"), Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd"))) > 0 Then
                                        sSql = "Update MachineRawPunch Set P_Day='Y' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And OfficePunch = '" & mIn1 & "'"
                                        mP_Flag = "Y"
                                    Else
                                        sSql = "Update MachineRawPunch Set P_Day='N' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And OfficePunch = '" & mIn1 & "'"
                                        mP_Flag = "N"
                                    End If

                                    If String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("P_Day").ToString()) Then
                                        If servername = "Access" Then
                                            'If con1.State <> ConnectionState.Open Then
                                            '    con1.Open()
                                            'End If
                                            sSql = sSql.Replace("OfficePunch", "FORMAT(OfficePunch,'yyyy-MM-dd HH:mm')")
                                            cmd1 = New OleDbCommand(sSql, con1Access)
                                            cmd1.ExecuteNonQuery()
                                            'con1.Close()
                                        Else
                                            'If con.State <> ConnectionState.Open Then
                                            '    con.Open()
                                            'End If
                                            cmd = New SqlCommand(sSql, conSQL)
                                            cmd.ExecuteNonQuery()
                                            'con.Close()
                                        End If
                                        'Cn.execute_NonQuery(sSql)
                                    Else
                                        If mP_Flag <> rsPunch.Tables(0).Rows(iP)("P_Day").ToString() Then
                                            If servername = "Access" Then
                                                'If con1.State <> ConnectionState.Open Then
                                                '    con1.Open()
                                                'End If
                                                sSql = sSql.Replace("OfficePunch", "FORMAT(OfficePunch,'yyyy-MM-dd HH:mm')")
                                                cmd1 = New OleDbCommand(sSql, con1Access)
                                                cmd1.ExecuteNonQuery()
                                                'con1.Close()
                                            Else
                                                'If con.State <> ConnectionState.Open Then
                                                '    con.Open()
                                                'End If
                                                cmd = New SqlCommand(sSql, conSQL)
                                                cmd.ExecuteNonQuery()
                                                'con.Close()
                                            End If
                                            'Cn.execute_NonQuery(sSql)
                                        End If
                                    End If
                                Else
                                    GoTo AsgIt
                                End If

                                mDate1 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("OfficePunch")).AddMinutes(Day_Hour).ToString()
                                iP = iP + 1
                                If iP > Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1 Then
                                    GoTo AsgIt
                                End If
                                'Try

                                If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("OfficePunch")), Convert.ToDateTime(mDate1))) >= 0 Then
                                    mout1 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm:ss")
                                    mOut1Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString.Trim
                                    If Not (String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString())) AndAlso rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString().Trim(" "c).Length <> 0 Then
                                        mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                        strLastPunchReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                    Else
                                        If mReasonCode.Trim(" "c).Length <= 0 Then
                                            mReasonCode = ""
                                            strLastPunchReasonCode = ""
                                        End If
                                    End If

                                    If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Day, Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("Dateoffice")).ToString("yyyy-MM-dd"), Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd"))) > 0 Then
                                        If servername = "Access" Then
                                            sSql = "Update MachineRawPunch Set P_Day='Y' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss') = '" & mout1 & "'"
                                        Else
                                            sSql = "Update MachineRawPunch Set P_Day='Y' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And OfficePunch = '" & mout1 & "'"
                                        End If
                                        mP_Flag = "Y"
                                    Else
                                        If servername = "Access" Then
                                            sSql = "Update MachineRawPunch Set P_Day='N' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss') = '" & mout1 & "'"
                                        Else
                                            sSql = "Update MachineRawPunch Set P_Day='N' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And OfficePunch = '" & mout1 & "'"
                                        End If
                                        mP_Flag = "N"
                                    End If

                                    If String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("P_Day").ToString()) Then
                                        If servername = "Access" Then
                                            'If con1.State <> ConnectionState.Open Then
                                            '    con1.Open()
                                            'End If
                                            cmd1 = New OleDbCommand(sSql, con1Access)
                                            cmd1.ExecuteNonQuery()
                                            'con1.Close()
                                        Else
                                            'If con.State <> ConnectionState.Open Then
                                            '    con.Open()
                                            'End If
                                            cmd = New SqlCommand(sSql, conSQL)
                                            cmd.ExecuteNonQuery()
                                            'con.Close()
                                        End If
                                        'Cn.execute_NonQuery(sSql)
                                    Else
                                        If mP_Flag <> rsPunch.Tables(0).Rows(iP)("P_Day").ToString() Then
                                            If servername = "Access" Then
                                                'If con1.State <> ConnectionState.Open Then
                                                '    con1.Open()
                                                'End If
                                                cmd1 = New OleDbCommand(sSql, con1Access)
                                                cmd1.ExecuteNonQuery()
                                                'con1.Close()
                                            Else
                                                'If con.State <> ConnectionState.Open Then
                                                '    con.Open()
                                                'End If
                                                cmd = New SqlCommand(sSql, conSQL)
                                                cmd.ExecuteNonQuery()
                                                'con.Close()
                                            End If
                                            'Cn.execute_NonQuery(sSql)
                                        End If
                                    End If
                                Else
                                    GoTo AsgIt
                                End If
                                'Catch ex As Exception
                                '    GoTo AsgIt
                                'End Try
                                If EmpGrpArr(EmpGrpId).g_NightShiftFourPunch = "N" Then
                                    GoTo AsgIt
                                End If

                                If (iP <> Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1) And Not (rsEmp.Tables(0).Rows(iE)("Two").ToString().Trim() = "Y") AndAlso rsEmp.Tables(0).Rows(iE)("InOnly").ToString().Trim() = "N" Then
                                    iP = iP + 1
                                    If iP > Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1 Then
                                        GoTo AsgIt
                                    End If

                                    If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Day, Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("DateOffice")).ToString("yyyy-MM-dd"), Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("OfficePunch")).ToString("yyyy-MM-dd"))) > 0 AndAlso EmpGrpArr(EmpGrpId).g_NightShiftFourPunch = "N" Then
                                        GoTo AsgIt
                                    End If

                                    If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("OfficePunch")), Convert.ToDateTime(mDate1))) >= 0 Then
                                        mIn2 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm:ss")
                                        mIn2Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString.Trim
                                        If Not (String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString())) AndAlso rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString().Trim(" "c).Length <> 0 Then
                                            If mReasonCode.Trim(" "c).Length <= 0 Then
                                                mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                            End If
                                        Else
                                            If mReasonCode.Trim(" "c).Length <= 0 Then
                                                mReasonCode = ""
                                            End If
                                        End If

                                        If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Day, Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("Dateoffice")).ToString("yyyy-MM-dd"), Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd"))) > 0 Then
                                            sSql = "Update MachineRawPunch Set P_Day='Y' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And OfficePunch = '" & mIn2 & "'"
                                            mP_Flag = "Y"
                                        Else
                                            sSql = "Update MachineRawPunch Set P_Day='N' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And OfficePunch = '" & mIn2 & "'"
                                            mP_Flag = "N"
                                        End If

                                        If String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("P_Day").ToString()) Then
                                            If servername = "Access" Then
                                                'If con1.State <> ConnectionState.Open Then
                                                '    con1.Open()
                                                'End If
                                                sSql = sSql.Replace("OfficePunch", "FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss')")
                                                cmd1 = New OleDbCommand(sSql, con1Access)
                                                cmd1.ExecuteNonQuery()
                                                'con1.Close()
                                            Else
                                                'If con.State <> ConnectionState.Open Then
                                                '    con.Open()
                                                'End If
                                                cmd = New SqlCommand(sSql, conSQL)
                                                cmd.ExecuteNonQuery()
                                                'con.Close()
                                            End If
                                            'Cn.execute_NonQuery(sSql)
                                        Else
                                            If mP_Flag <> rsPunch.Tables(0).Rows(iP)("P_Day").ToString() Then
                                                If servername = "Access" Then
                                                    'If con1.State <> ConnectionState.Open Then
                                                    '    con1.Open()
                                                    'End If
                                                    sSql = sSql.Replace("OfficePunch", "FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss')")
                                                    cmd1 = New OleDbCommand(sSql, con1Access)
                                                    cmd1.ExecuteNonQuery()
                                                    'con1.Close()
                                                Else
                                                    'If con.State <> ConnectionState.Open Then
                                                    '    con.Open()
                                                    'End If
                                                    cmd = New SqlCommand(sSql, conSQL)
                                                    cmd.ExecuteNonQuery()
                                                    'con.Close()
                                                End If
                                                'Cn.execute_NonQuery(sSql)
                                            End If
                                        End If
                                    Else
                                        GoTo AsgIt
                                    End If

                                    iP = iP + 1
                                    If iP > Convert.ToInt32(rsPunch.Tables(0).Rows.Count) - 1 Then
                                        GoTo AsgIt
                                    End If

                                    If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("OfficePunch")), Convert.ToDateTime(mDate1))) >= 0 Then
                                        mout2 = Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd HH:mm:ss")
                                        mOut2Mannual = rsPunch.Tables(0).Rows(iP)("ISMANUAL").ToString.Trim
                                        If Not (String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString())) AndAlso rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString().Trim(" "c).Length <> 0 Then
                                            If mReasonCode.Trim(" "c).Length <= 0 Then
                                                mReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                                strLastPunchReasonCode = rsPunch.Tables(0).Rows(iP)("ReasonCode").ToString()
                                            End If
                                        Else
                                            If mReasonCode.Trim(" "c).Length <= 0 Then
                                                mReasonCode = ""
                                                strLastPunchReasonCode = ""
                                            End If
                                        End If

                                        If Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Day, Convert.ToDateTime(rsTime.Tables(0).Rows(iT)("Dateoffice")).ToString("yyyy-MM-dd"), Convert.ToDateTime(rsPunch.Tables(0).Rows(iP)("officepunch")).ToString("yyyy-MM-dd"))) > 0 Then
                                            sSql = "Update MachineRawPunch Set P_Day='Y' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And OfficePunch = '" & mout2 & "'"
                                            mP_Flag = "Y"
                                        Else
                                            sSql = "Update MachineRawPunch Set P_Day='N' Where Paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim() & "' And OfficePunch = '" & mout2 & "'"
                                            mP_Flag = "N"
                                        End If

                                        If String.IsNullOrEmpty(rsPunch.Tables(0).Rows(iP)("P_Day").ToString()) Then
                                            If servername = "Access" Then
                                                'If con1.State <> ConnectionState.Open Then
                                                '    con1.Open()
                                                'End If
                                                sSql = sSql.Replace("OfficePunch", "FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss')")
                                                cmd1 = New OleDbCommand(sSql, con1Access)
                                                cmd1.ExecuteNonQuery()
                                                'con1.Close()
                                            Else
                                                'If con.State <> ConnectionState.Open Then
                                                '    con.Open()
                                                'End If
                                                cmd = New SqlCommand(sSql, conSQL)
                                                cmd.ExecuteNonQuery()
                                                'con.Close()
                                            End If
                                            'Cn.execute_NonQuery(sSql)
                                        Else
                                            If mP_Flag <> rsPunch.Tables(0).Rows(iP)("P_Day").ToString() Then
                                                If servername = "Access" Then
                                                    'If con1.State <> ConnectionState.Open Then
                                                    '    con1.Open()
                                                    'End If
                                                    sSql = sSql.Replace("OfficePunch", "FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss')")
                                                    cmd1 = New OleDbCommand(sSql, con1Access)
                                                    cmd1.ExecuteNonQuery()
                                                    'con1.Close()
                                                Else
                                                    'If con.State <> ConnectionState.Open Then
                                                    '    con.Open()
                                                    'End If
                                                    cmd = New SqlCommand(sSql, conSQL)
                                                    cmd.ExecuteNonQuery()
                                                    'con.Close()
                                                End If
                                                'Cn.execute_NonQuery(sSql)
                                            End If
                                        End If
                                        GoTo AsgIt
                                    End If
                                End If
                            Next
                        End If

AsgIt:
                        If mout2 = "Null" AndAlso mout1 <> "Null" AndAlso mIn2 = "Null" Then
                            mout2 = mout1
                            mout1 = "Null"
                        End If

                        getShiftInfo() ' added by nitin
                        mShiftAttended = AutoShift2(rsTime, iT, rsEmp, iE, mIn1, mIn2, mout1, mout2, EmpGrpId)
                        Upd2(rsTime, iT, rsEmp, iE, mIn1, mIn2, mout1, mout2, mShiftAttended, EmpGrpId, OwDur)





                        If mShiftStartTime <> "Null" Then
                            mShiftStartTime = "'" & mShiftStartTime & "'"
                        End If

                        If mShiftEndTime <> "Null" Then
                            mShiftEndTime = "'" & mShiftEndTime & "'"
                        End If

                        If mLunchStartTime <> "Null" Then
                            mLunchStartTime = "'" & mLunchStartTime & "'"
                        End If

                        If mLunchEndTime <> "Null" Then
                            mLunchEndTime = "'" & mLunchEndTime & "'"
                        End If

                        If mIn1 <> "Null" Then
                            mIn1 = "'" & mIn1 & "'"
                        End If

                        If mout1 <> "Null" Then
                            mout1 = "'" & mout1 & "'"
                        End If

                        If mIn2 <> "Null" Then
                            mIn2 = "'" & mIn2 & "'"
                        End If

                        If mout2 <> "Null" Then
                            mout2 = "'" & mout2 & "'"
                        End If
                        'If EmpGrpArr(EmpGrpId).g_OwMinus = "Y" Then
                        '    If mHoursworked > OwDur Then
                        '        mHoursworked = mHoursworked - OwDur
                        '    End If
                        'End If





                        'sSql = "Update tblTimeRegister Set ShiftStartTime=" & mShiftStartTime & ", ShiftEndTime=" + mShiftEndTime & ",LunchStartTime=" + mLunchStartTime & ", " & " LunchEndTime=" + mLunchEndTime & ", HoursWorked=" + mHoursworked & ",ExcLunchHours=" + mExcLunchHours & ",OtDuration=" + mOtDuration & ", " & " OsDuration=" + mOSDuration & ",OtAmount=" + motamount & ", EarlyArrival=" + mEarlyArrival & ",EarlyDeparture=" + mearlydeparture & ", " & " LateArrival=" + mLateArrival & ", LunchEarlyDeparture=" + mLunchEarlyDeparture & ", LunchLateArrival=" + mLunchLateArrival & ", " & " TotalLossHrs=" + mTotalLossHrs & ",Status='" + mStatus & "',ShiftAttended='" + mShiftAttended & "',In1=" & mIn1 & ",In2=" & mIn2 & ", " & " out1=" & mout1 & ",Out2=" & mout2 & ",In1Mannual='" & mIn1Mannual & "',In2Mannual='" & mIn2Mannual & "',Out1Mannual='" & mOut1Mannual & "', " & " Out2Mannual='" & mOut2Mannual & "',LeaveValue=" + mLeaveValue & ",PresentValue=" + mPresentValue & ",AbsentValue=" + mAbsentValue & ", " & " Holiday_Value=" + mHoliday_Value & ",Wo_Value=" + mWO_Value & ",OutWorkDuration=" + mOutWorkDuration & " " & " where paycode='" + rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim(" "c) & "' and dateoffice = '" & Proc_Date & "' "
                        sSql = "Update tblTimeRegister Set ShiftStartTime=" & mShiftStartTime & ", ShiftEndTime=" & mShiftEndTime & ",LunchStartTime=" & mLunchStartTime & ", " & _
                              " LunchEndTime=" & mLunchEndTime & ", HoursWorked=" & mHoursworked & ",ExcLunchHours=" & mExcLunchHours & ",OtDuration=" & mOtDuration & ", " & _
                              " OsDuration=" & mOSDuration & ",OtAmount=" & motamount & ", EarlyArrival=" & mEarlyArrival & ",EarlyDeparture=" & mearlydeparture & ", " & _
                              " LateArrival=" & mLateArrival & ", LunchEarlyDeparture=" & mLunchEarlyDeparture & ", LunchLateArrival=" & mLunchLateArrival & ", " & _
                              " TotalLossHrs=" & mTotalLossHrs & ",Status='" & mStatus & "',ShiftAttended='" & mShiftAttended & "',In1=" & mIn1 & ",In2=" & mIn2 & ", " & _
                              " out1=" & mout1 & ",Out2=" & mout2 & ",In1Mannual='" & mIn1Mannual & "',In2Mannual='" & mIn2Mannual & "',Out1Mannual='" & mOut1Mannual & "', " & _
                              " Out2Mannual='" & mOut2Mannual & "',LeaveValue=" & mLeaveValue & ",PresentValue=" & mPresentValue & ",AbsentValue=" & mAbsentValue & ", " & _
                              " Holiday_Value=" & mHoliday_Value & ",Wo_Value=" & mWO_Value & ",OutWorkDuration=" & OwDur & " " & _
                              " where paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim(Microsoft.VisualBasic.ChrW(32)) & "' and dateoffice = '" & Proc_Date & "' "

                        If servername = "Access" Then
                            'sSql = "Update tblTimeRegister Set ShiftStartTime=" & mShiftStartTime & ", ShiftEndTime=" + mShiftEndTime & ",LunchStartTime=" + mLunchStartTime & ", " & " LunchEndTime=" + mLunchEndTime & ", HoursWorked=" + mHoursworked & ",ExcLunchHours=" + mExcLunchHours & ",OtDuration=" + mOtDuration & ", " & " OsDuration=" + mOSDuration & ",OtAmount=" + motamount & ", EarlyArrival=" + mEarlyArrival & ",EarlyDeparture=" + mearlydeparture & ", " & " LateArrival=" + mLateArrival & ", LunchEarlyDeparture=" + mLunchEarlyDeparture & ", LunchLateArrival=" + mLunchLateArrival & ", " & " TotalLossHrs=" + mTotalLossHrs & ",Status='" + mStatus & "',ShiftAttended='" + mShiftAttended & "',In1=" & mIn1 & ",In2=" & mIn2 & ", " & " out1=" & mout1 & ",Out2=" & mout2 & ",In1Mannual='" & mIn1Mannual & "',In2Mannual='" & mIn2Mannual & "',Out1Mannual='" & mOut1Mannual & "', " & " Out2Mannual='" & mOut2Mannual & "',LeaveValue=" + mLeaveValue & ",PresentValue=" + mPresentValue & ",AbsentValue=" + mAbsentValue & ", " & " Holiday_Value=" + mHoliday_Value & ",Wo_Value=" + mWO_Value & ",OutWorkDuration=" + mOutWorkDuration & " " & " where paycode='" + rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim(" "c) & "' and FORMAT(dateoffice,'yyyy-MM-dd') = '" & Proc_Date & "' "
                            'If con1.State <> ConnectionState.Open Then
                            '    con1.Open()
                            'End If
                            sSql = "Update tblTimeRegister Set ShiftStartTime=" & mShiftStartTime & ", ShiftEndTime=" & mShiftEndTime & ",LunchStartTime=" & mLunchStartTime & ", " & _
                             " LunchEndTime=" & mLunchEndTime & ", HoursWorked=" & mHoursworked & ",ExcLunchHours=" & mExcLunchHours & ",OtDuration=" & mOtDuration & ", " & _
                             " OsDuration=" & mOSDuration & ",OtAmount=" & motamount & ", EarlyArrival=" & mEarlyArrival & ",EarlyDeparture=" & mearlydeparture & ", " & _
                             " LateArrival=" & mLateArrival & ", LunchEarlyDeparture=" & mLunchEarlyDeparture & ", LunchLateArrival=" & mLunchLateArrival & ", " & _
                             " TotalLossHrs=" & mTotalLossHrs & ",Status='" & mStatus & "',ShiftAttended='" & mShiftAttended & "',In1=" & mIn1 & ",In2=" & mIn2 & ", " & _
                             " out1=" & mout1 & ",Out2=" & mout2 & ",In1Mannual='" & mIn1Mannual & "',In2Mannual='" & mIn2Mannual & "',Out1Mannual='" & mOut1Mannual & "', " & _
                             " Out2Mannual='" & mOut2Mannual & "',LeaveValue=" & mLeaveValue & ",PresentValue=" & mPresentValue & ",AbsentValue=" & mAbsentValue & ", " & _
                             " Holiday_Value=" & mHoliday_Value & ",Wo_Value=" & mWO_Value & ",OutWorkDuration=" & OwDur & " " & _
                             " where paycode='" & rsEmp.Tables(0).Rows(iE)("Paycode").ToString().Trim().Trim(Microsoft.VisualBasic.ChrW(32)) & "' and FORMAT(dateoffice,'yyyy-MM-dd') = '" & Proc_Date & "' "

                            cmd1 = New OleDbCommand(sSql, con1Access)
                            cmd1.ExecuteNonQuery()
                            'con1.Close()
                        Else
                            'If conSQL.State <> ConnectionState.Open Then
                            '    conSQL.Open()
                            'End If
                            cmd = New SqlCommand(sSql, conSQL)
                            cmd.ExecuteNonQuery()
                            'con.Close()
                        End If
                        'Cn.execute_NonQuery(sSql)





                        'SMS logic
                        Dim shiftEndTime As Date
                        Dim request As HttpWebRequest
                        Dim response As HttpWebResponse = Nothing
                        Dim Proc_DateTmp As DateTime = Convert.ToDateTime(Proc_Date)
                        If g_SMSApplicable = "" Then
                            Load_SMS_Policy()
                        End If
                        If g_SMSApplicable = "Y" Then
                            If Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) <> "" And Proc_DateTmp.ToString("yyyy-MM-dd") = Now.ToString("yyyy-MM-dd") Then
                                'Late
                                If g_isLate = "Y" And mLateArrival > 0 Then
                                    If mLateSMS = "N" Then 'temp comment
                                        LateDur = Math.Truncate(mLateArrival / 60).ToString("00") & ":" & (mLateArrival Mod 60).ToString("00")
                                        'LateDur = Min2Hr(mLateArrival)
                                        LateSMSContent = rsEmp.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim & " " & g_LateContent1 & " " & LateDur & " " & g_LateContent2
                                        ' 'url = "http://alerts.3mdigital.co.in/api/web2sms.php?workingkey=" & G_SmsKey & "&sender=" & G_SenderID & "&to=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString) & "&message=" & LateSMSContent & ""
                                        'original'url = "http://sms.mobtexting.com/index.php/api?method=sms.normal&api_key=" & Trim(G_SmsKey) & "&to=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) & "&sender=" & Trim(G_SenderID) & "&message=" & Trim(LateSMSContent) & "&flash=0&unicode=1"
                                        'url = "http://sms.jbssolution.in/submitsms.jsp?user=TESCHOTI&key=68e0655580XX&mobile=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) & "&message=" & Trim(LateSMSContent) & "&senderid=TESCHT&accusage=1&unicode=1"
                                        'Content = frmMdiMain.Inet1.OpenURL(url)
                                        Try
                                            sendSMS(Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim), LateSMSContent)
                                            'request = DirectCast(WebRequest.Create(url), HttpWebRequest)
                                            'response = DirectCast(request.GetResponse(), HttpWebResponse)
                                            If servername = "Access" Then
                                                sSql = "Update tblTimeRegister Set LateSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and format(dateoffice,'yyyy-MM-dd') = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                                                cmd1 = New OleDbCommand(sSql, con1Access)
                                                cmd1.ExecuteNonQuery()
                                            Else
                                                sSql = "Update tblTimeRegister Set LateSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and dateoffice = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                                                cmd = New SqlCommand(sSql, conSQL)
                                                cmd.ExecuteNonQuery()
                                            End If
                                        Catch ex As Exception

                                        End Try
                                    End If 'temp comment
                                End If
                                'IN
                                mIn1 = Replace(mIn1, "'", "")
                                If g_isInSMS = "Y" And IsDate(mIn1) Then
                                    If mInSMS = "N" Then  'temp comment
                                        'InPunch = Format(mIn1, "HH:mm")
                                        'InPunch = Convert.ToDateTime(mIn1).ToString("HH:mm")
                                        InPunch = Convert.ToDateTime(mIn1).ToString("dd/MM/yyyy HH:mm")
                                        InSMSContent = rsEmp.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim & " " & g_InContent1 & " " & InPunch & " " & g_InContent2
                                        ''url = "http://alerts.3mdigital.co.in/api/web2sms.php?workingkey=" & G_SmsKey & "&sender=" & G_SenderID & "&to=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) & "&message=" & InSMSContent & ""
                                        'original' url = "http://sms.mobtexting.com/index.php/api?method=sms.normal&api_key=" & Trim(G_SmsKey) & "&to=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) & "&sender=" & Trim(G_SenderID) & "&message=" & Trim(InSMSContent) & "&flash=0&unicode=1"
                                        'url = "http://sms.jbssolution.in/submitsms.jsp?user=TESCHOTI&key=68e0655580XX&mobile=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) & "&message=" & Trim(InSMSContent) & "&senderid=TESCHT&accusage=1&unicode=1"

                                        'Content = frmMdiMain.Inet1.OpenURL(url)
                                        Try
                                            sendSMS(Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim), InSMSContent)
                                            'request = DirectCast(WebRequest.Create(url), HttpWebRequest)
                                            'response = DirectCast(request.GetResponse(), HttpWebResponse)
                                            If servername = "Access" Then
                                                sSql = "Update tblTimeRegister Set inSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and format(dateoffice,'yyyy-MM-dd') = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                                                cmd1 = New OleDbCommand(sSql, con1Access)
                                                cmd1.ExecuteNonQuery()
                                            Else
                                                sSql = "Update tblTimeRegister Set inSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and dateoffice = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                                                cmd = New SqlCommand(sSql, conSQL)
                                                cmd.ExecuteNonQuery()
                                            End If
                                        Catch ex As Exception

                                        End Try
                                        'Cn.Execute(sSql)
                                    End If  'temp comment
                                End If
                                'Out
                                mout2 = Replace(mout2, "'", "")
                                mShiftEndTime = Replace(mShiftEndTime, "'", "")
                                If g_isOutSMS = "Y" And IsDate(mout2) Then
                                    If mOutSMS = "N" Then  'temp comment
                                        shiftEndTime = mShiftEndTime
                                        shiftEndTime = DateAdd("n", g_OutSMSAfter, shiftEndTime)
                                        If CDate(mout2) > CDate(shiftEndTime) Then
                                            'OutPunch = Convert.ToDateTime(mout2).ToString("HH:mm")
                                            OutPunch = Convert.ToDateTime(mout2).ToString("dd/MM/yyyy HH:mm")
                                            OutSMSContent = rsEmp.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim & " " & g_OutContent1 & " " & OutPunch & " " & g_OutContent2
                                            ''url = "http://alerts.3mdigital.co.in/api/web2sms.php?workingkey=" & G_SmsKey & "&sender=" & G_SenderID & "&to=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) & "&message=" & OutSMSContent & ""
                                            'original 'url = "http://sms.mobtexting.com/index.php/api?method=sms.normal&api_key=" & Trim(G_SmsKey) & "&to=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) & "&sender=" & Trim(G_SenderID) & "&message=" & Trim(OutSMSContent) & "&flash=0&unicode=1"
                                            'url = "http://sms.jbssolution.in/submitsms.jsp?user=TESCHOTI&key=68e0655580XX&mobile=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) & "&message=" & Trim(OutSMSContent) & "&senderid=TESCHT&accusage=1&unicode=1"

                                            'Content = frmMdiMain.Inet1.OpenURL(url)
                                            Try
                                                sendSMS(Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim), OutSMSContent)
                                                'request = DirectCast(WebRequest.Create(url), HttpWebRequest)
                                                'response = DirectCast(request.GetResponse(), HttpWebResponse)
                                                If servername = "Access" Then
                                                    sSql = "Update tblTimeRegister Set OutSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and format(dateoffice,'yyyy-MM-dd') = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                                                    cmd1 = New OleDbCommand(sSql, con1Access)
                                                    cmd1.ExecuteNonQuery()
                                                Else
                                                    sSql = "Update tblTimeRegister Set OutSMS='Y' where paycode='" & Trim(rsEmp.Tables(0).Rows(0).Item("Paycode").ToString.Trim) & "' and dateoffice = '" & Proc_DateTmp.ToString("yyyy-MM-dd") & "' "
                                                    cmd = New SqlCommand(sSql, conSQL)
                                                    cmd.ExecuteNonQuery()
                                                End If
                                            Catch ex As Exception

                                            End Try
                                            'Cn.Execute(sSql)
                                        End If
                                    End If  'temp comment
                                End If
                            End If
                        End If
                        'End SMS logic
                    Next
                End If
            Next
        End If
        If servername = "Access" Then
            If con1Access.State <> ConnectionState.Closed Then
                con1Access.Close()
            End If
        Else
            If conSQL.State <> ConnectionState.Closed Then
                conSQL.Close()
            End If
        End If
        'XtraMaster.LabelControl4.Text = ""
        'Application.DoEvents()
    End Sub
    Public Sub Load_ShiftMasterSql()
        Try
            Dim i As Integer = 0
            Dim iCtr As Integer = 0
            Dim Rs As DataSet = New DataSet
            Dum.Shiftlist.Items.Clear()
            'Shiftlist.Clear()
            sSql = "select SHIFT,Convert(varchar(5),STARTTIME,108) STARTTIME,Convert(varchar(5),ENDTIME,108) ENDTIME,Conv" & _
            "ert(varchar(5),LUNCHTIME,108) LUNCHTIME,Convert(varchar(5),LUNCHENDTIME,108) LUNCHENDTIME,CAST(repla" & _
            "ce(str(LunchDeduction,2),' ','0') AS char(2)) AS LunchDeduction,SHIFTPOSITION,CAST(replace(str(OtSta" & _
            "rtAfter,3),' ','0') AS char(3)) AS OtStartAfter,CAST(replace(str(OtDeductAfter,4),' ','0') AS char(4" & _
            ")) as OtDeductAfter,CAST(replace(str(OtDeductHrs,3),' ','0') AS char(4)) as OtDeductHrs, ShiftEarly, ShiftLate from tblshiftmaster"
            If servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1)
                adapA.Fill(Rs)
            Else
                adap = New SqlDataAdapter(sSql, con)
                adap.Fill(Rs)
            End If
            'Rs = Cn.FillDataSet(sSql)
            Dum.Shiftlist.Items.Clear()
            'Shiftlist.Clear()
            If (Rs.Tables(0).Rows.Count > 0) Then
                Dim i2 As Integer = 0
                Do While (i2 < Rs.Tables(0).Rows.Count)
                    sSql = (Rs.Tables(0).Rows(i2)("Shift").ToString + (" " _
                                + (Rs.Tables(0).Rows(i2)("StartTime").ToString + (" " _
                                + (Rs.Tables(0).Rows(i2)("EndTime").ToString + (" " _
                                + (Rs.Tables(0).Rows(i2)("LunchTime").ToString + (" " _
                                + (Rs.Tables(0).Rows(i2)("LunchEndTime").ToString + (" " _
                                + (Rs.Tables(0).Rows(i2)("ShiftPosition").ToString.Substring(0, 1) + (" " + Rs.Tables(0).Rows(i2)("LunchDeduction").ToString))))))))))))
                    sSql = (sSql + (" " _
                                + (Rs.Tables(0).Rows(i2)("OtStartAfter").ToString + (" " + Rs.Tables(0).Rows(i)("OtDeductAfter").ToString))))
                    sSql = (sSql + (" " + Rs.Tables(0).Rows(i2)("OtDeductHrs").ToString))
                    Dum.Shiftlist.Items.Add(iCtr)
                    'Shiftlist.Add(iCtr)
                    Dum.dShiftlst.Items.Add(Rs.Tables(0).Rows(i)("Shift").ToString)
                    'dShiftlst.Add(Rs.Tables(0).Rows(i)("Shift").ToString)
                    i2 = (i2 + 1)
                Loop
            End If
        Catch
        End Try
    End Sub
    Public Sub Load_HolidaySql(ByVal HCOMPANYCODE As String, ByVal HDEPARTMENTCODE As String, ByVal BRANCHCODE As String)
        Try
            Dim i As Integer = 0
            Dim holiday As DateTime = System.DateTime.MinValue
            Dim Rs As DataSet = New DataSet
            Dum.HldList.Items.Clear()
            'alHld.Clear()
            sSql = ("Select convert(varchar(10),hdate,126) from Holiday WHERE COMPANYCODE='" _
                        & (HCOMPANYCODE.ToString & ("' AND DEPARTMENTCODE='" _
                        & (HDEPARTMENTCODE.ToString & "'")) & " AND BRANCHCODE = '" & BRANCHCODE & "'"))
            If servername = "Access" Then
                sSql = ("Select hdate from Holiday WHERE COMPANYCODE='" _
                        & (HCOMPANYCODE.ToString & ("' AND DEPARTMENTCODE='" _
                        & (HDEPARTMENTCODE.ToString & "'")) & " AND BRANCHCODE = '" & BRANCHCODE & "'"))
                adapA = New OleDbDataAdapter(sSql, con1)
                adapA.Fill(Rs)
            Else
                adap = New SqlDataAdapter(sSql, con)
                adap.Fill(Rs)
            End If
            'Rs = Cn.FillDataSet(sSql)
            Dum.HldList.Items.Clear()
            If (Rs.Tables(0).Rows.Count > 0) Then
                Dim i1 As Integer = 0
                Do While (i1 < Rs.Tables(0).Rows.Count)
                    'MsgBox(Rs.Tables(0).Rows(i1)(0).ToString)
                    'holiday = DateTime.ParseExact(Rs.Tables(0).Rows(i1)(0).ToString, "yyyy-MM-dd", CultureInfo.InvariantCulture)
                    holiday = Convert.ToDateTime(Rs.Tables(0).Rows(i1)(0).ToString).ToString("yyyy-MM-dd")
                    Dum.HldList.Items.Add(holiday.ToString("yyyy-MM-dd"))
                    alHld.Add(holiday.ToString("yyyy-MM-dd"))
                    i1 = (i1 + 1)
                Loop
            End If
        Catch ex As Exception
            'MsgBox(ex.Message.ToString)
        End Try
    End Sub
    Public Function AutoShift2(ByVal mTime As DataSet, ByVal timerow As Integer, ByVal rsEmp As DataSet, ByVal emprow As Integer, ByVal mIn1 As String, ByVal mIn2 As String, ByVal mout1 As String, ByVal mout2 As String, ByVal EmpGrpId As Integer) As String
        Dim tempAutoShift2 As String = ""
        Dim mShift As String = ""
        Dim mCtr As Integer = 0
        Dim mShiftTime As String = ""
        Dim mStartTime As String = ""
        Dim mEndTime As String = ""
        Dim mFlag As Boolean = False
        'Dim g_As_Low As Integer = getinfo.g_As_Low
        'Dim g_As_Up As Integer = getinfo.g_As_Up
        Dim nShift As String = ""
        Dim ftext As String = ""
        mShiftAttended = mTime.Tables(0).Rows(timerow)("Shiftattended").ToString.Trim
        ftext = getText(mShiftAttended)
        tempAutoShift2 = mTime.Tables(0).Rows(timerow)("Shiftattended").ToString.Trim
        Dum.HldList.Text = Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")
        If Dum.HldList.Items.Contains(Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")) _
                    OrElse (mTime.Tables(0).Rows(timerow)("Shiftattended").ToString.Trim.ToUpper = "OFF") Then
            Return tempAutoShift2
        End If
        'original
        'If ((Dum.HldList.SelectedIndex >= 0) _
        '            OrElse (mTime.Tables(0).Rows(timerow)("Shiftattended").ToString.Trim.ToUpper = "OFF")) Then
        '    Return tempAutoShift2
        'End If

        mShift = rsEmp.Tables(0).Rows(emprow)("Auth_Shifts").ToString.Trim
        If (String.IsNullOrEmpty(mShift.ToString) _
                    OrElse (Convert.ToString(mShift).Trim.Length = 0)) Then
            Return tempAutoShift2
        End If

        mCtr = 1
        mFlag = False
        'MsgBox((mShift.ToString.Substring((Convert.ToInt32(mCtr) - 1), 3))) 'nitin
        Dim tmpShift() As String = mShift.Split(",")  'nitin
        'Try
        'While (mShift.ToString.Substring((Convert.ToInt32(mCtr) - 1), 2) <> "") 'nitin
        For i As Integer = 0 To tmpShift.Length - 1 'nitin
            'nShift = mShift.ToString.Substring((Convert.ToInt32(mCtr) - 1), 2) 'nitin
            nShift = tmpShift(i).Trim 'nitin
            'Dum.Shiftlist.SelectedIndex = Dum.dShiftlst.SelectedIndex;
            ftext = getText(nShift.Trim)
            Dim tempArray() As String = ftext.Split(" ") ' Array Added By Nitin For Shift value
            If arrShift.Contains(nShift) Then
                'mShiftTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(4, 5))) By Nitin
                mShiftTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd") & (" " & tempArray(1).ToString))
                mStartTime = Convert.ToDateTime(mShiftTime).AddMinutes((0 - EmpGrpArr(EmpGrpId).g_As_Low)).ToString
                mEndTime = Convert.ToDateTime(mShiftTime).AddMinutes(EmpGrpArr(EmpGrpId).g_As_Up).ToString
                If SimulateIsDate.IsDate(mIn1) Then
                    If ((SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mStartTime), Convert.ToDateTime(mIn1)) >= 0) _
                                AndAlso (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mEndTime)) >= 0)) Then
                        'tempAutoShift2 = mShift.ToString.Substring((mCtr - 1), 3) Commented By Nitin
                        tempAutoShift2 = tempArray(0).ToString().Trim ' Added By nitin
                        mFlag = True
                        Exit For 'nitin
                        'Exit While 'nitin
                    End If
                End If
            End If
            mCtr = (mCtr + 4)
        Next  'nitin
        'End While 'nitin
        'Catch ex As Exception

        'End Try
        If (Not SimulateIsDate.IsDate(mIn1) _
                    And (Not SimulateIsDate.IsDate(mout1) _
                    And (Not SimulateIsDate.IsDate(mIn2) _
                    And Not SimulateIsDate.IsDate(mout2)))) Then
            tempAutoShift2 = mTime.Tables(0).Rows(timerow)("Shift").ToString
        End If

        If (mFlag = False) Then
            tempAutoShift2 = mTime.Tables(0).Rows(timerow)("Shift").ToString
        End If

        Return tempAutoShift2
    End Function
    Public Function getText(ByVal code As String) As String
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim Rs As DataSet = New DataSet
        sSql = ("select SHIFT,Convert(varchar(5),STARTTIME,108) STARTTIME,Convert(varchar(5),ENDTIME,108) ENDTIME,Convert(varchar(5),LUNCHTIME,108) LUNCHTIME" &
        ",Convert(varchar(5),LUNCHENDTIME,108) LUNCHENDTIME,CAST(replace(str(LunchDeduction,2),' ','0') AS char(2)) AS LunchDeduction" &
        ",SHIFTPOSITION,CAST(replace(str(OtStartAfter,4),' ','0') AS char(4)) AS OtStartAfter" &
        ",CAST(replace(str(OtDeductAfter,4),' ','0') AS char(4)) as OtDeductAfter" &
        ",CAST(replace(str(OtDeductHrs,3),' ','0') AS char(4)) as OtDeductHrs from tblshiftmaster where shift='" & (code.ToString & "' "))

        If servername = "Access" Then
            sSql = ("select SHIFT,FORMAT(STARTTIME,'HH:mm') as STARTTIME,FORMAT(ENDTIME,'HH:mm') as ENDTIME,FORMAT(LUNCHTIME,'HH:mm') as LUNCHTIME" &
       ",FORMAT(LUNCHENDTIME,'HH:mm') as LUNCHENDTIME,FORMAT(LunchDeduction,'00') AS LunchDeduction" &
       ",SHIFTPOSITION,FORMAT(OtStartAfter,'0000') AS OtStartAfter" &
       ",FORMAT(OtDeductAfter,'0000') as OtDeductAfter" &
       ",FORMAT(OtDeductHrs,'000') as OtDeductHrs from tblshiftmaster where shift='" & (code.ToString & "' "))
            adapA = New OleDbDataAdapter(sSql, con1Access)
            adapA.Fill(Rs)
        Else
            adap = New SqlDataAdapter(sSql, conSQL)
            adap.Fill(Rs)
        End If

        'Rs = Cn.FillDataSet(sSql)
        If (Rs.Tables(0).Rows.Count > 0) Then
            Dim i2 As Integer = 0
            For i2 = 0 To Rs.Tables(0).Rows.Count - 1
                'MsgBox("Rs.Tables(0).Rows(i2)(ShiftPosition).ToString = " & Rs.Tables(0).Rows(i2)("ShiftPosition").ToString)
                sSql = (Rs.Tables(0).Rows(i2)("Shift").ToString.Trim + (" " _
                            + (Rs.Tables(0).Rows(i2)("StartTime").ToString + (" " _
                            + (Rs.Tables(0).Rows(i2)("EndTime").ToString + (" " _
                            + (Rs.Tables(0).Rows(i2)("LunchTime").ToString + (" " _
                            + (Rs.Tables(0).Rows(i2)("LunchEndTime").ToString + (" " _
                            + (Rs.Tables(0).Rows(i2)("ShiftPosition").ToString.Substring(0, 1) + (" " + Rs.Tables(0).Rows(i2)("LunchDeduction").ToString))))))))))))
                sSql = (sSql + (" " _
                            + (Rs.Tables(0).Rows(i2)("OtStartAfter").ToString + (" " + Rs.Tables(0).Rows(i2)("OtDeductAfter").ToString))))
                sSql = (sSql + (" " + Rs.Tables(0).Rows(i2)("OtDeductHrs").ToString))
                i2 = (i2 + 1)
            Next

        End If

        Return sSql
    End Function
    Public Sub getShiftInfo()
        Try
            'Dim Cn As Class_Connection = New Class_Connection
            Dim Rs As DataSet = New DataSet
            Dim sSql As String = "Select shift from tblshiftmaster"
            If servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1)
                adapA.Fill(Rs)
            Else
                adap = New SqlDataAdapter(sSql, con)
                adap.Fill(Rs)
            End If
            'Rs = Cn.FillDataSet_SQL(sSql)
            If (Rs.Tables(0).Rows.Count > 0) Then
                Dim i As Integer = 0
                Do While (i < Rs.Tables(0).Rows.Count)
                    arrShift.Add(Rs.Tables(0).Rows(i)(0).ToString)
                    i = (i + 1)
                Loop
            End If
        Catch
        End Try
    End Sub
    Public Sub Upd2(ByVal mTime As DataSet, ByVal timerow As Integer, ByVal rsEmp As DataSet, ByVal emprow As Integer, ByVal mIn1tmp As String, ByVal mIn2tmp As String, ByVal mout1tmp As String, ByVal mout2tmp As String, ByVal shiftattended As String, ByVal EmpGrpId As Integer, ByVal OWHR As Integer)
        Dim mLate As Integer = 0
        Dim mEarly As Integer = 0
        Dim mLate1 As Integer = 0
        Dim mEarly1 As Integer = 0
        Dim mShiftTime As Integer = 0
        Dim AbsHours As Integer = 0
        Dim isHld As Boolean = False
        Dim OTinHrs As Double = 0
        Dim OTfactor As Double = 0
        Dim mLDed As Integer = 0
        Dim mLDur As Integer = 0
        Dim mLunchDur As Integer = 0
        Dim mOt As Integer = 0
        Dim mOtStartAfter As Integer = 0
        Dim mOtDeductAfter As Integer = 0
        Dim mOtDeductHrs As Integer = 0
        mStatus = "A"
        mHoursworked = 0
        mExcLunchHours = 0
        mOtDuration = 0
        mOSDuration = 0
        motamount = 0
        mEarlyArrival = 0
        mearlydeparture = 0
        mLateArrival = 0
        mLunchEarlyDeparture = 0
        mLunchLateArrival = 0
        mTotalLossHrs = 0
        mLeaveValue = 0
        mPresentValue = 0
        mAbsentValue = 0
        mHoliday_Value = 0
        mWO_Value = 0
        mOutWorkDuration = 0
        Dim mShiftAttended As String = ""
        mShiftAttended = shiftattended

        mIn1 = mIn1tmp
        mIn2 = mIn2tmp
        mout1 = mout1tmp
        mout2 = mout2tmp
        mOutWorkDuration = OWHR
        Dum.HldList.Text = Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")
        'if (Dum.HldList.SelectedIndex >= 0)
        If alHld.Contains(Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")) Then
            isHld = True
        Else
            isHld = False
        End If

        Dim ftext As String = ""
        Dum.dShiftlst.Text = mShiftAttended
        ftext = getText(mShiftAttended)
        If ((mShiftAttended <> "OFF") And (mShiftAttended <> "IGN") And (mShiftAttended <> "FLX")) Then
            'Dum.Shiftlist.SelectedIndex  = Dum.dShiftlst.SelectedIndex;
            'Dum.Shiftlist.Text = mShiftAttended;
            'Dum.Shiftlist.SelectedIndex  = Dum.dShiftlst.SelectedIndex;
            'if (Dum.Shiftlist.SelectedIndex < 0)


            Dim Rs As DataSet = New DataSet
            Dim sSql As String = "Select shift from tblshiftmaster"
            If servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Access)
                adapA.Fill(Rs)
            Else
                adap = New SqlDataAdapter(sSql, conSQL)
                adap.Fill(Rs)
            End If
            'Rs = Cn.FillDataSet_SQL(sSql)
            If (Rs.Tables(0).Rows.Count > 0) Then
                Dim i As Integer = 0
                Do While (i < Rs.Tables(0).Rows.Count)
                    arrShift.Add(Rs.Tables(0).Rows(i)(0).ToString.Trim)
                    i = (i + 1)
                Loop
            End If

            If Not arrShift.Contains(mShiftAttended) Then
                'System.Windows.Forms.MessageBox.Show(("Shift " _
                '                + (mShiftAttended + (" is not defined in shift master of paycode " _
                '                + (mTime.Tables(0).Rows(timerow)("paycode").ToString + (" on date " + Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")))))), "TimeWatch", System.Windows.Forms.MessageBoxButtons.OK)
                Return
            End If
            Dim tmp() As String = ftext.Split(" ")

            'mShiftStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(4, 5)))
            mShiftStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(1)))
            ''MsgBox(mShiftStartTime)
            'mShiftEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(10, 5)))
            mShiftEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(2)))
            If (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mShiftEndTime)) < 0) Then
                'mShiftEndTime = (Convert.ToDateTime(mShiftEndTime).AddDays(1).ToString("yyyy-MM-dd") & " " & mShiftEndTime.Substring(11, 5)) 'nitin
                mShiftEndTime = (Convert.ToDateTime(mShiftEndTime).AddDays(1).ToString("yyyy-MM-dd") & " " & tmp(2)) 'nitin
                'mShiftEndTime = (Convert.ToDateTime(mShiftEndTime).AddDays(1).ToString("yyyy-MM-dd") + mShiftEndTime.Substring(11, 6)) 'nitin
            End If

            'mLunchStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(16, 5)))
            mLunchStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(3)))
            If (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mLunchStartTime)) < 0) Then
                'mLunchStartTime = (Convert.ToDateTime(mLunchStartTime).AddDays(1).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(16, 5)))
                mLunchStartTime = (Convert.ToDateTime(mLunchStartTime).AddDays(1).ToString("yyyy-MM-dd") & (" " & tmp(3)))
            End If

            'mLunchEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(22, 5)))
            mLunchEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(4)))
            If (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mLunchEndTime)) < 0) Then
                'mLunchEndTime = (Convert.ToDateTime(mLunchEndTime).AddDays(1).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(22, 5)))
                mLunchEndTime = (Convert.ToDateTime(mLunchEndTime).AddDays(1).ToString("yyyy-MM-dd") & (" " & tmp(4)))
            End If

            mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mLunchStartTime), Convert.ToDateTime(mLunchEndTime)))
            mLunchDur = mLDur
            mLDed = Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(30, 3)))
            'MsgBox(mShiftStartTime)
            mShiftTime = (Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mShiftEndTime))) - mLDed)
        End If

        'MsgBox(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString.Trim & "  lenght: " & mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString.Length)
        Dim tmpL As Double
        If mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString.Length = 0 Then
            tmpL = 0
        Else
            tmpL = Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString)
        End If
        'If (Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString) > 0) Then   'main
        If (tmpL > 0) Then    'nitin
            If (Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")) = 0.25) Then
                mStatus = "Q_"
            ElseIf (Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")) = 0.5) Then
                mStatus = "H_"
            ElseIf (Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")) = 0.75) Then
                mStatus = "T_"
            Else
                mStatus = ""
            End If

            'mStatus = (mStatus.ToString.Trim + mTime.Tables(timerow).Rows(0)("LeaveCode").ToString) 'nitin
            mStatus = (mStatus.ToString.Trim + mTime.Tables(0).Rows(timerow)("LeaveCode").ToString)
            If SimulateIsDate.IsDate(mIn1) Then
                If ((mTime.Tables(0).Rows(timerow)("ShiftAttended").ToString.Trim <> "IGN") _
                            AndAlso ((mTime.Tables(0).Rows(timerow)("ShiftAttended").ToString <> "OFF") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("ShiftAttended").ToString <> "FLX") _
                            AndAlso Not isHld)) Then
                    mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftStartTime)))
                    If (mEarly < 0) Then
                        mEarly = 0
                    End If
                End If

                If SimulateIsDate.IsDate(mout2) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
                    If SimulateIsDate.IsDate("in2") Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                        If (mLDur < mLDed) Then
                            mLDur = mLDed
                        End If

                        mHoursworked = (mHoursworked - mLDur)
                    Else
                        mHoursworked = (mHoursworked - mLDed)
                    End If

                End If

                Select Case (Convert.ToString(mTime.Tables(0).Rows(timerow)("LeaveType")))
                    Case "P"
                        mPresentValue = 1
                    Case "L"
                        mLeaveValue = (0 + Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                        mPresentValue = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                    Case Else
                        mAbsentValue = (0 + Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                        mPresentValue = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                End Select

            ElseIf (Not String.IsNullOrEmpty(mTime.Tables(0).Rows(timerow)("LeaveType1").ToString.Trim) _
                        AndAlso Not String.IsNullOrEmpty(mTime.Tables(0).Rows(timerow)("LeaveType2").ToString.Trim)) Then
                mStatus = (mTime.Tables(0).Rows(timerow)("FirstHalfLeavecode").ToString.Trim + mTime.Tables(0).Rows(timerow)("SecondHalfLeavecode").ToString.Trim)
                If (((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString.Trim = "P") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString.Trim = "A")) _
                            OrElse ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString.Trim = "A") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "P"))) Then
                    mPresentValue = 0.5
                    mLeaveValue = 0
                    mAbsentValue = 0.5
                End If

                If (((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "L") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "P")) _
                            OrElse ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "P") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "L"))) Then
                    mPresentValue = 0.5
                    mLeaveValue = 0.5
                    mAbsentValue = 0
                End If

                If (((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "L") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "A")) _
                            OrElse ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "A") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "L"))) Then
                    mPresentValue = 0
                    mLeaveValue = 0.5
                    mAbsentValue = 0.5
                End If

                If ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "L") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "L")) Then
                    mPresentValue = 0
                    mLeaveValue = 1
                    mAbsentValue = 0
                End If

                If ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "A") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "A")) Then
                    mPresentValue = 0
                    mLeaveValue = 0
                    mAbsentValue = 1
                End If

                If ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "P") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "P")) Then
                    mPresentValue = 1
                    mLeaveValue = 0
                    mAbsentValue = 0
                End If

            Else
                If (mTime.Tables(0).Rows(timerow)("LeaveType").ToString = "P") Then
                    mPresentValue = Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString)
                ElseIf (mTime.Tables(0).Rows(timerow)("LeaveType").ToString = "L") Then
                    mLeaveValue = Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount"))
                ElseIf (mTime.Tables(0).Rows(timerow)("LeaveType").ToString.Trim = "A") Then
                    mAbsentValue = Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount"))
                End If

                If (mTime.Tables(0).Rows(timerow)("ShiftAttended").ToString = "OFF") Then
                    mWO_Value = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                ElseIf isHld Then
                    mHoliday_Value = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                ElseIf (ftext.ToString.Substring(28, 1) = "H") Then
                    mWO_Value = 0.5
                ElseIf (mTime.Tables(0).Rows(timerow)("LEAVETYPE").ToString.Trim <> "A") Then
                    mAbsentValue = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                End If

            End If

            GoTo Lastl
        End If

        If ((mShiftAttended = "OFF") _
                    AndAlso isHld) Then
            mHoliday_Value = 1
            mStatus = "WOH"
            If SimulateIsDate.IsDate(mIn1) Then
                mStatus = "PWH"
                If ((EmpGrpArr(EmpGrpId).g_isPresentOnWOPresent = "Y") _
                            OrElse (EmpGrpArr(EmpGrpId).g_isPresentOnHLDPresent = "Y")) Then
                    mPresentValue = 1
                    mHoliday_Value = 0
                End If

                If SimulateIsDate.IsDate(mout2) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
                    If SimulateIsDate.IsDate(mIn2) Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                        mHoursworked = (mHoursworked - mLDur)
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOS").ToString.Trim = "Y") Then
                        mOSDuration = mHoursworked
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString.Trim = "Y") Then
                        mOtDuration = mHoursworked
                    End If

                End If

            End If
            ''' 
            If EmpGrpArr(EmpGrpId).ISCOMPOFF = "Y" Then
                mOtDuration = 0
            End If

            GoTo Lastl
        End If

        If (mShiftAttended = "OFF") Then
            mWO_Value = 1
            mStatus = "WO"
            If SimulateIsDate.IsDate(mIn1) Then
                mStatus = "POW"
                If (EmpGrpArr(EmpGrpId).g_isPresentOnWOPresent = "Y") Then
                    mWO_Value = 0
                    mPresentValue = 1
                End If

                If SimulateIsDate.IsDate(mout2) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
                    If SimulateIsDate.IsDate(mIn2) Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                        mHoursworked = (mHoursworked - mLDur)
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOS").ToString.Trim = "Y") Then
                        mOSDuration = mHoursworked
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString.Trim = "Y") Then
                        mOtDuration = (mHoursworked - EmpGrpArr(EmpGrpId).g_Wo_OT)
                        If mOtDuration < 0 Then
                            mOtDuration = 0
                        End If
                    End If

                End If

            End If

            'if compoff alowwed then ==>mOtDuration=0
            If EmpGrpArr(EmpGrpId).ISCOMPOFF = "Y" Then
                mOtDuration = 0
            End If
            GoTo Lastl
        End If

        If alHld.Contains(Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")) Then
            mHoliday_Value = 1
            mStatus = "HLD"
            If SimulateIsDate.IsDate(mIn1) Then
                mStatus = "POH"
                If (EmpGrpArr(EmpGrpId).g_isPresentOnHLDPresent = "Y") Then
                    mHoliday_Value = 0
                    mPresentValue = 1
                End If

                If SimulateIsDate.IsDate(mout2) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
                    If SimulateIsDate.IsDate("in2") Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                        mHoursworked = (mHoursworked - mLDur)
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOS").ToString = "Y") Then
                        mOSDuration = mHoursworked
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString = "Y") Then
                        mOtDuration = (mHoursworked - EmpGrpArr(EmpGrpId).g_Hld_OT)
                    End If

                End If

            End If

            GoTo Lastl
        End If
        If (SimulateIsDate.IsDate(mIn1) _
                    AndAlso (rsEmp.Tables(0).Rows(emprow)("InOnly").ToString.Trim <> "N")) Then
            If Not ((rsEmp.Tables(0).Rows(emprow)("InOnly").ToString = "O") AndAlso SimulateIsDate.IsDate(mout2)) Then
                mout2 = mShiftEndTime
            End If
        End If
        If ((rsEmp.Tables(0).Rows(emprow)("IsPunchAll").ToString.Trim = "N") AndAlso Not SimulateIsDate.IsDate(mIn1)) Then
            mIn1 = mShiftStartTime
            mout2 = mShiftEndTime
        End If
        If ((rsEmp.Tables(0).Rows(emprow)("IsPunchAll").ToString = "N") AndAlso (SimulateIsDate.IsDate(mIn1) AndAlso Not SimulateIsDate.IsDate(mout2))) Then
            If (Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftEndTime))) > 0) Then
                mout2 = mShiftEndTime
            Else
                mout2 = mIn1
                mIn1 = mShiftStartTime
            End If
        End If

        If ((SimulateIsDate.IsDate(mIn1) _
                    And Not SimulateIsDate.IsDate(mout2)) _
                    Or (Not SimulateIsDate.IsDate(mIn1) _
                    And SimulateIsDate.IsDate(mout2))) Then
            mStatus = "MIS"
            mPresentValue = 1

            'mark half day if miss punch
            If rsEmp.Tables(0).Rows(emprow).Item("MARKMISSASHALFDAY").ToString.Trim = "Y" Then
                mStatus = "HLF"
                mPresentValue = 0.5
                mAbsentValue = 0.5
            ElseIf EmpGrpArr(EmpGrpId).g_MIS = True Then
                mStatus = "A"
                mPresentValue = 0
                mAbsentValue = 1
            End If

            If (ftext.ToString.Trim.Substring(28, 1) = "H") Then
                If (mLeaveValue < 0.5) Then
                    mPresentValue = (0.5 - mLeaveValue)
                End If

                mWO_Value = 0.5
            End If

            If (mShiftAttended <> "IGN") And (mShiftAttended <> "FLX") Then
                If SimulateIsDate.IsDate(mIn1) Then
                    mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftStartTime)))
                    If (mEarly > 0) Then
                        mEarlyArrival = mEarly
                    ElseIf (Math.Abs(mEarly) > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("PERMISLATEARRIVAL"))) Then
                        mLateArrival = Math.Abs(mEarly)
                    End If
                End If
            End If
            GoTo Lastl
        End If
        If ((ftext.ToString.Trim.Substring(28, 1) = "H") AndAlso (mLeaveValue = 0)) Then
            mWO_Value = 0.5
            If SimulateIsDate.IsDate(mIn1) Then
                mPresentValue = 0.5
            Else
                mAbsentValue = 0.5
            End If

        End If

        If ((ftext.ToString.Trim.Substring(28, 1) = "H") AndAlso (mLeaveValue > 0)) Then
            mWO_Value = 0.5
            If (SimulateIsDate.IsDate(mIn1) _
                        And (mLeaveValue < 0.5)) Then
                mPresentValue = (0.5 - mLeaveValue)
            ElseIf (mLeaveValue < 0.5) Then
                mAbsentValue = (0.5 - mLeaveValue)
            End If

        End If

        If (SimulateIsDate.IsDate(mout2) And SimulateIsDate.IsDate(mIn1)) Then
            mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
            If SimulateIsDate.IsDate(mIn2) Then
                mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                If (mLDur < mLDed) Then
                    mLDur = mLDed
                End If
                mHoursworked = (mHoursworked - mLDur)
            Else
                mHoursworked = (mHoursworked - mLDed)
                'mHoursworked = (mHoursworked - mLDur)
            End If
            If EmpGrpArr(EmpGrpId).g_OwMinus = "Y" Then
                If mHoursworked > mOutWorkDuration Then
                    mHoursworked = mHoursworked - mOutWorkDuration
                End If
            End If
            If (mShiftAttended <> "IGN") And (mShiftAttended <> "FLX") Then
                'nitin start
                Dim tmp As Integer
                If servername = "Access" Then
                    tmp = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time1"))
                Else
                    tmp = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))
                End If
                'nitin end
                'If (mHoursworked < Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))) Then
                If (mHoursworked < tmp) Then
                    mHoursworked = 0
                    mAbsentValue = 1
                    GoTo Lastl
                End If

                If (SimulateIsDate.IsDate(mIn2) And SimulateIsDate.IsDate(mLunchStartTime)) Then
                    mEarly1 = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mLunchStartTime)))
                    If (mEarly1 > 0) Then
                        mLunchEarlyDeparture = mEarly1
                    End If

                    mLate1 = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mLunchEndTime), Convert.ToDateTime(mIn2)))
                    If (mLate1 > 0) Then
                        mLunchLateArrival = mLate1
                    End If

                    If (mLDur > mLunchDur) Then
                        mExcLunchHours = (mLDur - mLunchDur)
                    End If
                End If

                If (rsEmp.Tables(0).Rows(emprow)("IsOS").ToString.Trim = "Y") Then
                    'mOSDuration = ((mStatus == "HLF") ? mHoursworked - 240 : mHoursworked - mShiftTime);
                    If (mStatus = "HLF") Then
                        mOSDuration = mHoursworked - 240
                    Else
                        mOSDuration = mHoursworked - mShiftTime
                    End If
                    '(mStatus = "HLF")
                    '(mHoursworked - mShiftTime)
                    If (mOSDuration < 0) Then
                        mOSDuration = 0
                    End If
                End If

                mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftStartTime)))
                If (mEarly > 0) Then
                    mEarlyArrival = mEarly
                ElseIf (Math.Abs(mEarly) > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("PERMISLATEARRIVAL"))) Then
                    mLateArrival = Math.Abs(mEarly)
                End If

                mLate = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2)))
                If ((mLate < 0) _
                            And (Math.Abs(mLate) > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("PERMISEARLYDEPRT")))) Then
                    mearlydeparture = Math.Abs(mLate)
                End If

                mStatus = "P"
                If (ftext.ToString.Trim.Substring(28, 1) = "H") Then
                    If (mLeaveValue < 0.5) Then
                        mPresentValue = (0.5 - mLeaveValue)
                    End If

                    mAbsentValue = 0
                Else
                    mPresentValue = 1
                End If

                AbsHours = (Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mShiftEndTime))) - mHoursworked)
                If Not String.IsNullOrEmpty(mLunchStartTime) Then
                    AbsHours = (AbsHours - Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mLunchStartTime), Convert.ToDateTime(mLunchEndTime))))
                End If

                If (AbsHours > 0) Then
                    'nitin start
                    Dim tmp1, tmpsort As Integer
                    If servername = "Access" Then
                        tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time1"))
                        tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short1"))
                    Else
                        tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))
                        tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))
                    End If
                    'nitin end
                    'If ((rsEmp.Tables(0).Rows(emprow)("IsHalfDay").ToString = "Y") _
                    '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
                    '            AndAlso (mHoursworked >= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))))) Then   'nitin
                    If ((rsEmp.Tables(0).Rows(emprow)("IsHalfDay").ToString = "Y") _
                           AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
                           AndAlso (mHoursworked >= tmp1))) Then    'nitin
                        mStatus = "HLF"
                        mPresentValue = 0.5
                        mAbsentValue = 0.5
                        mLateArrival = 0
                        mearlydeparture = 0
                        mTotalLossHrs = 0
                        'ElseIf ((rsEmp.Tables(0).Rows(emprow)("isHalfday").ToString = "Y") _
                        '            AndAlso ((rsEmp.Tables(0).Rows(emprow)("isShort").ToString = "Y") _
                        '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))) _
                        '            AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))))) Then  'nitin
                    ElseIf ((rsEmp.Tables(0).Rows(emprow)("isHalfday").ToString = "Y") _
                          AndAlso ((rsEmp.Tables(0).Rows(emprow)("isShort").ToString = "Y") _
                          AndAlso ((mHoursworked <= tmpsort) _
                          AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))))) Then   'nitin
                        mStatus = "SRT"
                        mPresentValue = 1
                    End If

                End If

                If (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString = "Y") Then
                    'Calculate OT

                    Select Case (EmpGrpArr(EmpGrpId).g_OTFormulae)
                        Case 1
                            'Out - End
                            mOt = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2)))
                        Case 2
                            ' Hours Worked - Shift Hours
                            mOt = (mHoursworked - mShiftTime)
                        Case 3
                            ' Early Arrival + Late Departure
                            mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftStartTime)))
                            If (mEarly <= EmpGrpArr(EmpGrpId).g_OTEarly) Then
                                mEarly = 0
                            End If

                            '                        If mEarly < 0 And Abs(mEarly) <= g_OTLate Then
                            '                            mEarly = 0
                            '                        End If
                            '                        If mEarly > 0 And g_OTChkEarly = "Y" And mEarly <= g_OTEarly Then
                            '                            mEarly = 0
                            '                        End If

                            If EmpGrpArr(EmpGrpId).g_OTChkEarly = "N" Then
                                mEarly = 0
                            End If
                            mLate = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2)))
                            If (mLate <= EmpGrpArr(EmpGrpId).g_OTLate) Then
                                mLate = 0
                            End If

                            If (Math.Abs(mLate) <= EmpGrpArr(EmpGrpId).g_OTEnd) Then
                                mLate = 0
                            End If

                            mOt = (mEarly _
                                        + (mLate - mExcLunchHours))
                    End Select
                    If IsCompliance = True Then
                        mOt = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2)))
                    End If
                    Dim OtTmp() As String = ftext.Split(" ")
                    mOtStartAfter = Convert.ToInt32(SimulateVal.Val(OtTmp(7))) 'Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(33, 3))) '=> 7 konsa?
                    mOtDeductAfter = Convert.ToInt32(SimulateVal.Val(OtTmp(8))) 'Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(35, 4)))  '=>8
                    Try
                        mOtDeductHrs = Convert.ToInt32(SimulateVal.Val(OtTmp(9))) 'Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(9, 3))) '=>?
                    Catch ex As Exception
                        mOtDeductHrs = 0
                    End Try

                    If (mOt < mOtStartAfter) Then
                        mOt = 0
                    ElseIf ((mOtDeductHrs = 0) _
                                AndAlso (mOt > mOtDeductAfter)) Then
                        mOt = mOtDeductAfter
                    ElseIf (mOt _
                                >= (mOtDeductAfter + mOtDeductHrs)) Then
                        mOt = (mOt - mOtDeductHrs)
                    End If

                    If ((g_OTMinus = "N") _
                                AndAlso (mOt < 0)) Then
                        mOt = 0
                    End If
                    mOtDuration = mOt
                End If
                GoTo Lastl
            Else
                mPresentValue = 1
                mStatus = "P"
            End If

        End If

        If ((mLeaveValue = 0) _
                    AndAlso ((mPresentValue = 0) _
                    AndAlso ((mHoliday_Value = 0) _
                    AndAlso (mWO_Value = 0)))) Then
            mAbsentValue = 1
        End If

        If mShiftAttended = "FLX" Then
            'nitin start
            Dim tmp1, tmpsort As Integer
            If servername = "Access" Then
                tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time1"))
                tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short1"))
            Else
                tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))
                tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))
            End If
            'nitin end
            'If ((rsEmp.Tables(0).Rows(emprow)("IsHalfDay").ToString = "Y") _
            '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
            '            AndAlso (mHoursworked >= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))))) Then   'nitin
            If mHoursworked < Convert.ToInt32(tmp1) Then
                mStatus = "A"
                mPresentValue = 0
                mAbsentValue = 1
                mLateArrival = 0
                mearlydeparture = 0
                mTotalLossHrs = 0
            ElseIf (((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
                   AndAlso (mHoursworked >= tmp1))) Then    'nitin
                mStatus = "HLF"
                mPresentValue = 0.5
                mAbsentValue = 0.5
                mLateArrival = 0
                mearlydeparture = 0
                mTotalLossHrs = 0
                'ElseIf ((rsEmp.Tables(0).Rows(emprow)("isHalfday").ToString = "Y") _
                '            AndAlso ((rsEmp.Tables(0).Rows(emprow)("isShort").ToString = "Y") _
                '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))) _
                '            AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))))) Then  'nitin
            ElseIf ((mHoursworked < tmpsort) _
                  AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))) Then   'nitin
                mStatus = "SRT"
                mPresentValue = 1
            ElseIf mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString) And
                    mHoursworked > tmp1 And mHoursworked > tmpsort Then
                mStatus = "P"
                mPresentValue = 1
                mAbsentValue = 0
                mLateArrival = 0
                mearlydeparture = 0
                mTotalLossHrs = 0
            End If
        End If
Lastl:
        If ((mStatus = "HLF") _
                    AndAlso (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString = "Y")) Then
            mOtDuration = (mHoursworked - 240)
            mOtDuration = 0
        End If
        If (EmpGrpArr(EmpGrpId).g_isOutWork = "Y") Then

        End If


        If (EmpGrpArr(EmpGrpId).g_OtRound = "Y") Then
            OTinHrs = Convert.ToDouble(HM.minutetohour1(mOtDuration.ToString.Trim)) 'Convert.ToDouble(HM.minutetohour(mOtDuration.ToString.Trim))
            OTfactor = (OTinHrs - CType(Math.Floor(OTinHrs), Integer))
            If (OTfactor < 0.15) Then
                OTinHrs = CType(Math.Floor(OTinHrs), Integer)
            ElseIf ((OTfactor >= 0.15) _
                        AndAlso (OTfactor < 0.45)) Then
                OTinHrs = (CType(Math.Floor(OTinHrs), Integer) + 0.3)
            ElseIf (OTfactor >= 0.45) Then
                OTinHrs = (CType(Math.Floor(OTinHrs), Integer) + 1)
            End If

            mOtDuration = HM.hour(OTinHrs.ToString.Trim)
        End If

        If (rsEmp.Tables(0).Rows(emprow)("istimelossallowed").ToString = "Y") Then
            mTotalLossHrs = (mLateArrival _
                        + (mearlydeparture _
                        + (mLunchLateArrival + mLunchEarlyDeparture)))
        Else
            mLateArrival = 0
            mearlydeparture = 0
            mTotalLossHrs = 0
            mLunchLateArrival = 0
            mLunchEarlyDeparture = 0
        End If

        If (SimulateVal.Val(rsEmp.Tables(0).Rows(emprow)("OTRate").ToString.Trim) = 0) Then
            motamount = 0
        Else
            motamount = (mOtDuration * (Convert.ToDouble(rsEmp.Tables(0).Rows(emprow)("OtRate")) / 60))
        End If


    End Sub
    Public Sub Upd2Compliance(ByVal mTime As DataSet, ByVal timerow As Integer, ByVal rsEmp As DataSet, ByVal emprow As Integer, ByVal mIn1tmp As String, ByVal mIn2tmp As String, ByVal mout1tmp As String, ByVal mout2tmp As String, ByVal shiftattended As String, ByVal EmpGrpId As Integer, ByRef mOtDuration As Integer)
        Dim mLate As Integer = 0
        Dim mEarly As Integer = 0
        Dim mLate1 As Integer = 0
        Dim mEarly1 As Integer = 0
        Dim mShiftTime As Integer = 0
        Dim AbsHours As Integer = 0
        Dim isHld As Boolean = False
        Dim OTinHrs As Double = 0
        Dim OTfactor As Double = 0
        Dim mLDed As Integer = 0
        Dim mLDur As Integer = 0
        Dim mLunchDur As Integer = 0
        Dim mOt As Integer = 0
        Dim mOtStartAfter As Integer = 0
        Dim mOtDeductAfter As Integer = 0
        Dim mOtDeductHrs As Integer = 0
        mStatus = "A"
        mHoursworked = 0
        mExcLunchHours = 0
        mOtDuration = 0
        mOSDuration = 0
        motamount = 0
        mEarlyArrival = 0
        mearlydeparture = 0
        mLateArrival = 0
        mLunchEarlyDeparture = 0
        mLunchLateArrival = 0
        mTotalLossHrs = 0
        mLeaveValue = 0
        mPresentValue = 0
        mAbsentValue = 0
        mHoliday_Value = 0
        mWO_Value = 0
        mOutWorkDuration = 0
        Dim mShiftAttended As String = ""
        mShiftAttended = shiftattended

        mIn1 = mIn1tmp
        mIn2 = mIn2tmp
        mout1 = mout1tmp
        mout2 = mout2tmp

        Dum.HldList.Text = Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")
        'if (Dum.HldList.SelectedIndex >= 0)
        If alHld.Contains(Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")) Then
            isHld = True
        Else
            isHld = False
        End If

        Dim ftext As String = ""
        Dum.dShiftlst.Text = mShiftAttended
        ftext = getText(mShiftAttended)
        If ((mShiftAttended <> "OFF") And (mShiftAttended <> "IGN") And (mShiftAttended <> "FLX")) Then
            'Dum.Shiftlist.SelectedIndex  = Dum.dShiftlst.SelectedIndex;
            'Dum.Shiftlist.Text = mShiftAttended;
            'Dum.Shiftlist.SelectedIndex  = Dum.dShiftlst.SelectedIndex;
            'if (Dum.Shiftlist.SelectedIndex < 0)


            Dim Rs As DataSet = New DataSet
            Dim sSql As String = "Select shift from tblshiftmaster"
            If servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Access)
                adapA.Fill(Rs)
            Else
                adap = New SqlDataAdapter(sSql, conSQL)
                adap.Fill(Rs)
            End If
            'Rs = Cn.FillDataSet_SQL(sSql)
            If (Rs.Tables(0).Rows.Count > 0) Then
                Dim i As Integer = 0
                Do While (i < Rs.Tables(0).Rows.Count)
                    arrShift.Add(Rs.Tables(0).Rows(i)(0).ToString.Trim)
                    i = (i + 1)
                Loop
            End If

            If Not arrShift.Contains(mShiftAttended) Then
                'System.Windows.Forms.MessageBox.Show(("Shift " _
                '                + (mShiftAttended + (" is not defined in shift master of paycode " _
                '                + (mTime.Tables(0).Rows(timerow)("paycode").ToString + (" on date " + Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")))))), "TimeWatch", System.Windows.Forms.MessageBoxButtons.OK)
                Return
            End If
            Dim tmp() As String = ftext.Split(" ")

            'mShiftStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(4, 5)))
            mShiftStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(1)))
            ''MsgBox(mShiftStartTime)
            'mShiftEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(10, 5)))
            mShiftEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(2)))
            If (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mShiftEndTime)) < 0) Then
                'mShiftEndTime = (Convert.ToDateTime(mShiftEndTime).AddDays(1).ToString("yyyy-MM-dd") & " " & mShiftEndTime.Substring(11, 5)) 'nitin
                mShiftEndTime = (Convert.ToDateTime(mShiftEndTime).AddDays(1).ToString("yyyy-MM-dd") & " " & tmp(2)) 'nitin
                'mShiftEndTime = (Convert.ToDateTime(mShiftEndTime).AddDays(1).ToString("yyyy-MM-dd") + mShiftEndTime.Substring(11, 6)) 'nitin
            End If

            'mLunchStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(16, 5)))
            mLunchStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(3)))
            If (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mLunchStartTime)) < 0) Then
                'mLunchStartTime = (Convert.ToDateTime(mLunchStartTime).AddDays(1).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(16, 5)))
                mLunchStartTime = (Convert.ToDateTime(mLunchStartTime).AddDays(1).ToString("yyyy-MM-dd") & (" " & tmp(3)))
            End If

            'mLunchEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(22, 5)))
            mLunchEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(4)))
            If (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mLunchEndTime)) < 0) Then
                'mLunchEndTime = (Convert.ToDateTime(mLunchEndTime).AddDays(1).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(22, 5)))
                mLunchEndTime = (Convert.ToDateTime(mLunchEndTime).AddDays(1).ToString("yyyy-MM-dd") & (" " & tmp(4)))
            End If

            mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mLunchStartTime), Convert.ToDateTime(mLunchEndTime)))
            mLunchDur = mLDur
            mLDed = Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(30, 3)))
            'MsgBox(mShiftStartTime)
            mShiftTime = (Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mShiftEndTime))) - mLDed)
        End If

        'MsgBox(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString.Trim & "  lenght: " & mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString.Length)
        Dim tmpL As Double
        If mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString.Length = 0 Then
            tmpL = 0
        Else
            tmpL = Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString)
        End If
        'If (Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString) > 0) Then   'main
        If (tmpL > 0) Then    'nitin
            If (Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")) = 0.25) Then
                mStatus = "Q_"
            ElseIf (Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")) = 0.5) Then
                mStatus = "H_"
            ElseIf (Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")) = 0.75) Then
                mStatus = "T_"
            Else
                mStatus = ""
            End If

            'mStatus = (mStatus.ToString.Trim + mTime.Tables(timerow).Rows(0)("LeaveCode").ToString) 'nitin
            mStatus = (mStatus.ToString.Trim + mTime.Tables(0).Rows(timerow)("LeaveCode").ToString)
            If SimulateIsDate.IsDate(mIn1) Then
                If ((mTime.Tables(0).Rows(timerow)("ShiftAttended").ToString.Trim <> "IGN") _
                            AndAlso ((mTime.Tables(0).Rows(timerow)("ShiftAttended").ToString <> "OFF") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("ShiftAttended").ToString <> "FLX") _
                            AndAlso Not isHld)) Then
                    mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftStartTime)))
                    If (mEarly < 0) Then
                        mEarly = 0
                    End If
                End If

                If SimulateIsDate.IsDate(mout2) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
                    If SimulateIsDate.IsDate("in2") Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                        If (mLDur < mLDed) Then
                            mLDur = mLDed
                        End If

                        mHoursworked = (mHoursworked - mLDur)
                    Else
                        mHoursworked = (mHoursworked - mLDed)
                    End If

                End If

                Select Case (Convert.ToString(mTime.Tables(0).Rows(timerow)("LeaveType")))
                    Case "P"
                        mPresentValue = 1
                    Case "L"
                        mLeaveValue = (0 + Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                        mPresentValue = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                    Case Else
                        mAbsentValue = (0 + Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                        mPresentValue = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                End Select

            ElseIf (Not String.IsNullOrEmpty(mTime.Tables(0).Rows(timerow)("LeaveType1").ToString.Trim) _
                        AndAlso Not String.IsNullOrEmpty(mTime.Tables(0).Rows(timerow)("LeaveType2").ToString.Trim)) Then
                mStatus = (mTime.Tables(0).Rows(timerow)("FirstHalfLeavecode").ToString.Trim + mTime.Tables(0).Rows(timerow)("SecondHalfLeavecode").ToString.Trim)
                If (((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString.Trim = "P") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString.Trim = "A")) _
                            OrElse ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString.Trim = "A") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "P"))) Then
                    mPresentValue = 0.5
                    mLeaveValue = 0
                    mAbsentValue = 0.5
                End If

                If (((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "L") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "P")) _
                            OrElse ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "P") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "L"))) Then
                    mPresentValue = 0.5
                    mLeaveValue = 0.5
                    mAbsentValue = 0
                End If

                If (((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "L") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "A")) _
                            OrElse ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "A") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "L"))) Then
                    mPresentValue = 0
                    mLeaveValue = 0.5
                    mAbsentValue = 0.5
                End If

                If ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "L") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "L")) Then
                    mPresentValue = 0
                    mLeaveValue = 1
                    mAbsentValue = 0
                End If

                If ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "A") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "A")) Then
                    mPresentValue = 0
                    mLeaveValue = 0
                    mAbsentValue = 1
                End If

                If ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "P") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "P")) Then
                    mPresentValue = 1
                    mLeaveValue = 0
                    mAbsentValue = 0
                End If

            Else
                If (mTime.Tables(0).Rows(timerow)("LeaveType").ToString = "P") Then
                    mPresentValue = Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString)
                ElseIf (mTime.Tables(0).Rows(timerow)("LeaveType").ToString = "L") Then
                    mLeaveValue = Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount"))
                ElseIf (mTime.Tables(0).Rows(timerow)("LeaveType").ToString.Trim = "A") Then
                    mAbsentValue = Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount"))
                End If

                If (mTime.Tables(0).Rows(timerow)("ShiftAttended").ToString = "OFF") Then
                    mWO_Value = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                ElseIf isHld Then
                    mHoliday_Value = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                ElseIf (ftext.ToString.Substring(28, 1) = "H") Then
                    mWO_Value = 0.5
                ElseIf (mTime.Tables(0).Rows(timerow)("LEAVETYPE").ToString.Trim <> "A") Then
                    mAbsentValue = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                End If

            End If

            GoTo Lastl
        End If

        If ((mShiftAttended = "OFF") _
                    AndAlso isHld) Then
            mHoliday_Value = 1
            mStatus = "WOH"
            If SimulateIsDate.IsDate(mIn1) Then
                mStatus = "PWH"
                If ((EmpGrpArr(EmpGrpId).g_isPresentOnWOPresent = "Y") _
                            OrElse (EmpGrpArr(EmpGrpId).g_isPresentOnHLDPresent = "Y")) Then
                    mPresentValue = 1
                    mHoliday_Value = 0
                End If

                If SimulateIsDate.IsDate(mout2) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
                    If SimulateIsDate.IsDate(mIn2) Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                        mHoursworked = (mHoursworked - mLDur)
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOS").ToString.Trim = "Y") Then
                        mOSDuration = mHoursworked
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString.Trim = "Y") Then
                        mOtDuration = mHoursworked
                    End If

                End If

            End If
            ''' 
            If EmpGrpArr(EmpGrpId).ISCOMPOFF = "Y" Then
                mOtDuration = 0
            End If

            GoTo Lastl
        End If

        If (mShiftAttended = "OFF") Then
            mWO_Value = 1
            mStatus = "WO"
            If SimulateIsDate.IsDate(mIn1) Then
                mStatus = "POW"
                If (EmpGrpArr(EmpGrpId).g_isPresentOnWOPresent = "Y") Then
                    mWO_Value = 0
                    mPresentValue = 1
                End If

                If SimulateIsDate.IsDate(mout2) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
                    If SimulateIsDate.IsDate(mIn2) Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                        mHoursworked = (mHoursworked - mLDur)
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOS").ToString.Trim = "Y") Then
                        mOSDuration = mHoursworked
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString.Trim = "Y") Then
                        mOtDuration = (mHoursworked - EmpGrpArr(EmpGrpId).g_Wo_OT)
                        If mOtDuration < 0 Then
                            mOtDuration = 0
                        End If
                    End If

                End If

            End If

            'if compoff alowwed then ==>mOtDuration=0
            If EmpGrpArr(EmpGrpId).ISCOMPOFF = "Y" Then
                mOtDuration = 0
            End If
            GoTo Lastl
        End If

        If alHld.Contains(Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")) Then
            mHoliday_Value = 1
            mStatus = "HLD"
            If SimulateIsDate.IsDate(mIn1) Then
                mStatus = "POH"
                If (EmpGrpArr(EmpGrpId).g_isPresentOnHLDPresent = "Y") Then
                    mHoliday_Value = 0
                    mPresentValue = 1
                End If

                If SimulateIsDate.IsDate(mout2) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
                    If SimulateIsDate.IsDate("in2") Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                        mHoursworked = (mHoursworked - mLDur)
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOS").ToString = "Y") Then
                        mOSDuration = mHoursworked
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString = "Y") Then
                        mOtDuration = (mHoursworked - EmpGrpArr(EmpGrpId).g_Hld_OT)
                    End If

                End If

            End If

            GoTo Lastl
        End If
        If (SimulateIsDate.IsDate(mIn1) _
                    AndAlso (rsEmp.Tables(0).Rows(emprow)("InOnly").ToString.Trim <> "N")) Then
            If Not ((rsEmp.Tables(0).Rows(emprow)("InOnly").ToString = "O") AndAlso SimulateIsDate.IsDate(mout2)) Then
                mout2 = mShiftEndTime
            End If
        End If
        If ((rsEmp.Tables(0).Rows(emprow)("IsPunchAll").ToString.Trim = "N") AndAlso Not SimulateIsDate.IsDate(mIn1)) Then
            mIn1 = mShiftStartTime
            mout2 = mShiftEndTime
        End If
        If ((rsEmp.Tables(0).Rows(emprow)("IsPunchAll").ToString = "N") AndAlso (SimulateIsDate.IsDate(mIn1) AndAlso Not SimulateIsDate.IsDate(mout2))) Then
            If (Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftEndTime))) > 0) Then
                mout2 = mShiftEndTime
            Else
                mout2 = mIn1
                mIn1 = mShiftStartTime
            End If
        End If

        If ((SimulateIsDate.IsDate(mIn1) _
                    And Not SimulateIsDate.IsDate(mout2)) _
                    Or (Not SimulateIsDate.IsDate(mIn1) _
                    And SimulateIsDate.IsDate(mout2))) Then
            mStatus = "MIS"
            mPresentValue = 1

            'mark half day if miss punch
            If rsEmp.Tables(0).Rows(emprow).Item("MARKMISSASHALFDAY").ToString.Trim = "Y" Then
                mStatus = "HLF"
                mPresentValue = 0.5
                mAbsentValue = 0.5
            ElseIf EmpGrpArr(EmpGrpId).g_MIS = True Then
                mStatus = "A"
                mPresentValue = 0
                mAbsentValue = 1
            End If

            If (ftext.ToString.Trim.Substring(28, 1) = "H") Then
                If (mLeaveValue < 0.5) Then
                    mPresentValue = (0.5 - mLeaveValue)
                End If

                mWO_Value = 0.5
            End If

            If (mShiftAttended <> "IGN") And (mShiftAttended <> "FLX") Then
                If SimulateIsDate.IsDate(mIn1) Then
                    mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftStartTime)))
                    If (mEarly > 0) Then
                        mEarlyArrival = mEarly
                    ElseIf (Math.Abs(mEarly) > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("PERMISLATEARRIVAL"))) Then
                        mLateArrival = Math.Abs(mEarly)
                    End If
                End If
            End If
            GoTo Lastl
        End If
        If ((ftext.ToString.Trim.Substring(28, 1) = "H") AndAlso (mLeaveValue = 0)) Then
            mWO_Value = 0.5
            If SimulateIsDate.IsDate(mIn1) Then
                mPresentValue = 0.5
            Else
                mAbsentValue = 0.5
            End If

        End If

        If ((ftext.ToString.Trim.Substring(28, 1) = "H") AndAlso (mLeaveValue > 0)) Then
            mWO_Value = 0.5
            If (SimulateIsDate.IsDate(mIn1) _
                        And (mLeaveValue < 0.5)) Then
                mPresentValue = (0.5 - mLeaveValue)
            ElseIf (mLeaveValue < 0.5) Then
                mAbsentValue = (0.5 - mLeaveValue)
            End If

        End If

        If (SimulateIsDate.IsDate(mout2) And SimulateIsDate.IsDate(mIn1)) Then
            mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
            If SimulateIsDate.IsDate(mIn2) Then
                mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                If (mLDur < mLDed) Then
                    mLDur = mLDed
                End If
                mHoursworked = (mHoursworked - mLDur)
            Else
                mHoursworked = (mHoursworked - mLDed)
                'mHoursworked = (mHoursworked - mLDur)
            End If
            If (mShiftAttended <> "IGN") And (mShiftAttended <> "FLX") Then
                'nitin start
                Dim tmp As Integer
                If servername = "Access" Then
                    tmp = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time1"))
                Else
                    tmp = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))
                End If
                'nitin end
                'If (mHoursworked < Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))) Then
                If (mHoursworked < tmp) Then
                    mHoursworked = 0
                    mAbsentValue = 1
                    GoTo Lastl
                End If

                If (SimulateIsDate.IsDate(mIn2) And SimulateIsDate.IsDate(mLunchStartTime)) Then
                    mEarly1 = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mLunchStartTime)))
                    If (mEarly1 > 0) Then
                        mLunchEarlyDeparture = mEarly1
                    End If

                    mLate1 = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mLunchEndTime), Convert.ToDateTime(mIn2)))
                    If (mLate1 > 0) Then
                        mLunchLateArrival = mLate1
                    End If

                    If (mLDur > mLunchDur) Then
                        mExcLunchHours = (mLDur - mLunchDur)
                    End If
                End If

                If (rsEmp.Tables(0).Rows(emprow)("IsOS").ToString.Trim = "Y") Then
                    'mOSDuration = ((mStatus == "HLF") ? mHoursworked - 240 : mHoursworked - mShiftTime);
                    If (mStatus = "HLF") Then
                        mOSDuration = mHoursworked - 240
                    Else
                        mOSDuration = mHoursworked - mShiftTime
                    End If
                    '(mStatus = "HLF")
                    '(mHoursworked - mShiftTime)
                    If (mOSDuration < 0) Then
                        mOSDuration = 0
                    End If
                End If

                mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftStartTime)))
                If (mEarly > 0) Then
                    mEarlyArrival = mEarly
                ElseIf (Math.Abs(mEarly) > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("PERMISLATEARRIVAL"))) Then
                    mLateArrival = Math.Abs(mEarly)
                End If

                mLate = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2)))
                If ((mLate < 0) _
                            And (Math.Abs(mLate) > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("PERMISEARLYDEPRT")))) Then
                    mearlydeparture = Math.Abs(mLate)
                End If

                mStatus = "P"
                If (ftext.ToString.Trim.Substring(28, 1) = "H") Then
                    If (mLeaveValue < 0.5) Then
                        mPresentValue = (0.5 - mLeaveValue)
                    End If

                    mAbsentValue = 0
                Else
                    mPresentValue = 1
                End If

                AbsHours = (Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mShiftEndTime))) - mHoursworked)
                If Not String.IsNullOrEmpty(mLunchStartTime) Then
                    AbsHours = (AbsHours - Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mLunchStartTime), Convert.ToDateTime(mLunchEndTime))))
                End If

                If (AbsHours > 0) Then
                    'nitin start
                    Dim tmp1, tmpsort As Integer
                    If servername = "Access" Then
                        tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time1"))
                        tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short1"))
                    Else
                        tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))
                        tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))
                    End If
                    'nitin end
                    'If ((rsEmp.Tables(0).Rows(emprow)("IsHalfDay").ToString = "Y") _
                    '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
                    '            AndAlso (mHoursworked >= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))))) Then   'nitin
                    If ((rsEmp.Tables(0).Rows(emprow)("IsHalfDay").ToString = "Y") _
                           AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
                           AndAlso (mHoursworked >= tmp1))) Then    'nitin
                        mStatus = "HLF"
                        mPresentValue = 0.5
                        mAbsentValue = 0.5
                        mLateArrival = 0
                        mearlydeparture = 0
                        mTotalLossHrs = 0
                        'ElseIf ((rsEmp.Tables(0).Rows(emprow)("isHalfday").ToString = "Y") _
                        '            AndAlso ((rsEmp.Tables(0).Rows(emprow)("isShort").ToString = "Y") _
                        '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))) _
                        '            AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))))) Then  'nitin
                    ElseIf ((rsEmp.Tables(0).Rows(emprow)("isHalfday").ToString = "Y") _
                          AndAlso ((rsEmp.Tables(0).Rows(emprow)("isShort").ToString = "Y") _
                          AndAlso ((mHoursworked <= tmpsort) _
                          AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))))) Then   'nitin
                        mStatus = "SRT"
                        mPresentValue = 1
                    End If

                End If

                If (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString = "Y") Then
                    'Calculate OT

                    Select Case (EmpGrpArr(EmpGrpId).g_OTFormulae)
                        Case 1
                            'Out - End
                            mOt = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2)))
                        Case 2
                            ' Hours Worked - Shift Hours
                            mOt = (mHoursworked - mShiftTime)
                        Case 3
                            ' Early Arrival + Late Departure
                            mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftStartTime)))
                            If (mEarly <= EmpGrpArr(EmpGrpId).g_OTEarly) Then
                                mEarly = 0
                            End If

                            '                        If mEarly < 0 And Abs(mEarly) <= g_OTLate Then
                            '                            mEarly = 0
                            '                        End If
                            '                        If mEarly > 0 And g_OTChkEarly = "Y" And mEarly <= g_OTEarly Then
                            '                            mEarly = 0
                            '                        End If
                            mLate = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2)))
                            If (mLate <= EmpGrpArr(EmpGrpId).g_OTLate) Then
                                mLate = 0
                            End If

                            If (Math.Abs(mLate) <= EmpGrpArr(EmpGrpId).g_OTEnd) Then
                                mLate = 0
                            End If

                            mOt = (mEarly _
                                        + (mLate - mExcLunchHours))
                    End Select
                    If IsCompliance = True Then
                        mOt = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2)))
                    End If
                    Dim OtTmp() As String = ftext.Split(" ")
                    mOtStartAfter = Convert.ToInt32(SimulateVal.Val(OtTmp(7))) 'Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(33, 3))) '=> 7 konsa?
                    mOtDeductAfter = Convert.ToInt32(SimulateVal.Val(OtTmp(8))) 'Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(35, 4)))  '=>8
                    Try
                        mOtDeductHrs = Convert.ToInt32(SimulateVal.Val(OtTmp(9))) 'Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(9, 3))) '=>?
                    Catch ex As Exception
                        mOtDeductHrs = 0
                    End Try

                    If (mOt < mOtStartAfter) Then
                        mOt = 0
                    ElseIf ((mOtDeductHrs = 0) _
                                AndAlso (mOt > mOtDeductAfter)) Then
                        mOt = mOtDeductAfter
                    ElseIf (mOt _
                                >= (mOtDeductAfter + mOtDeductHrs)) Then
                        mOt = (mOt - mOtDeductHrs)
                    End If

                    If ((g_OTMinus = "N") _
                                AndAlso (mOt < 0)) Then
                        mOt = 0
                    End If
                    mOtDuration = mOt
                End If
                GoTo Lastl
            Else
                mPresentValue = 1
                mStatus = "P"
            End If

        End If

        If ((mLeaveValue = 0) _
                    AndAlso ((mPresentValue = 0) _
                    AndAlso ((mHoliday_Value = 0) _
                    AndAlso (mWO_Value = 0)))) Then
            mAbsentValue = 1
        End If

        If mShiftAttended = "FLX" Then
            'nitin start
            Dim tmp1, tmpsort As Integer
            If servername = "Access" Then
                tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time1"))
                tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short1"))
            Else
                tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))
                tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))
            End If
            'nitin end
            'If ((rsEmp.Tables(0).Rows(emprow)("IsHalfDay").ToString = "Y") _
            '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
            '            AndAlso (mHoursworked >= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))))) Then   'nitin
            If mHoursworked < Convert.ToInt32(tmp1) Then
                mStatus = "A"
                mPresentValue = 0
                mAbsentValue = 1
                mLateArrival = 0
                mearlydeparture = 0
                mTotalLossHrs = 0
            ElseIf (((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
                   AndAlso (mHoursworked >= tmp1))) Then    'nitin
                mStatus = "HLF"
                mPresentValue = 0.5
                mAbsentValue = 0.5
                mLateArrival = 0
                mearlydeparture = 0
                mTotalLossHrs = 0
                'ElseIf ((rsEmp.Tables(0).Rows(emprow)("isHalfday").ToString = "Y") _
                '            AndAlso ((rsEmp.Tables(0).Rows(emprow)("isShort").ToString = "Y") _
                '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))) _
                '            AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))))) Then  'nitin
            ElseIf ((mHoursworked < tmpsort) _
                  AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))) Then   'nitin
                mStatus = "SRT"
                mPresentValue = 1
            ElseIf mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString) And
                    mHoursworked > tmp1 And mHoursworked > tmpsort Then
                mStatus = "P"
                mPresentValue = 1
                mAbsentValue = 0
                mLateArrival = 0
                mearlydeparture = 0
                mTotalLossHrs = 0
            End If
        End If
Lastl:
        If ((mStatus = "HLF") _
                    AndAlso (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString = "Y")) Then
            mOtDuration = (mHoursworked - 240)
            mOtDuration = 0
        End If

        If (EmpGrpArr(EmpGrpId).g_OtRound = "Y") Then
            OTinHrs = Convert.ToDouble(HM.minutetohour1(mOtDuration.ToString.Trim)) 'Convert.ToDouble(HM.minutetohour(mOtDuration.ToString.Trim))
            OTfactor = (OTinHrs - CType(Math.Floor(OTinHrs), Integer))
            If (OTfactor < 0.15) Then
                OTinHrs = CType(Math.Floor(OTinHrs), Integer)
            ElseIf ((OTfactor >= 0.15) _
                        AndAlso (OTfactor < 0.45)) Then
                OTinHrs = (CType(Math.Floor(OTinHrs), Integer) + 0.3)
            ElseIf (OTfactor >= 0.45) Then
                OTinHrs = (CType(Math.Floor(OTinHrs), Integer) + 1)
            End If

            mOtDuration = HM.hour(OTinHrs.ToString.Trim)
        End If

        If (rsEmp.Tables(0).Rows(emprow)("istimelossallowed").ToString = "Y") Then
            mTotalLossHrs = (mLateArrival _
                        + (mearlydeparture _
                        + (mLunchLateArrival + mLunchEarlyDeparture)))
        Else
            mLateArrival = 0
            mearlydeparture = 0
            mTotalLossHrs = 0
            mLunchLateArrival = 0
            mLunchEarlyDeparture = 0
        End If

        If (SimulateVal.Val(rsEmp.Tables(0).Rows(emprow)("OTRate").ToString.Trim) = 0) Then
            motamount = 0
        Else
            motamount = (mOtDuration * (Convert.ToDouble(rsEmp.Tables(0).Rows(emprow)("OtRate")) / 60))
        End If

        If mShiftStartTime <> "Null" Then
            mShiftStartTime = mShiftStartTime
        End If
        If mShiftEndTime <> "Null" Then
            mShiftEndTime = mShiftEndTime
        End If
        If mLunchStartTime <> "Null" Then
            mLunchStartTime = mLunchStartTime
        End If
        If mLunchEndTime <> "Null" Then
            mLunchEndTime = mLunchEndTime
        End If
        If mIn1 <> "Null" Then
            mIn1 = "'" & mIn1 & "'"
        End If
        If mout1 <> "Null" Then
            mout1 = "'" & mout1 & "'"
        End If
        If mIn2 <> "Null" Then
            mIn2 = "'" & mIn2 & "'"
        End If
        If mout2 <> "Null" Then
            mout2 = "'" & mout2 & "'"
        End If
        sSql = "Update tblTimeRegisterD Set leaveamount='" & mTime.Tables(0).Rows(0)("leaveamount") & "',leavecode='" & mTime.Tables(0).Rows(0)("Leavecode") & "',LEAVETYPE='" & mTime.Tables(0).Rows(0)("LEAVETYPE") & "', ShiftStartTime='" & mShiftStartTime & "'" _
            & ", ShiftEndTime='" & mShiftEndTime & "',LunchStartTime='" _
            & mLunchStartTime & "', LunchEndTime='" & mLunchEndTime _
            & "', HoursWorked=" & mHoursworked & ",ExcLunchHours=" _
            & mExcLunchHours & ",OtDuration=" & mOtDuration _
            & ", OsDuration=" & mOSDuration & ",OtAmount=" & motamount _
            & ", EarlyArrival=" & mEarlyArrival & ",EarlyDeparture=" _
            & mearlydeparture & ",LateArrival=" & mLateArrival _
            & ", LunchEarlyDeparture=" & mLunchEarlyDeparture _
            & ", LunchLateArrival=" & mLunchLateArrival & ",TotalLossHrs=" _
            & mTotalLossHrs & ",Status='" & mStatus & "',ShiftAttended='" _
            & mShiftAttended & "',In1=" & mIn1 & ",In2=" & mIn2 & ",out1=" _
            & mout1 & ",Out2=" & mout2 & ",In1Mannual='N',In2Mannual='N',Out1Mannual='N',Out2Mannual='N',LeaveValue=" & mLeaveValue _
            & ",PresentValue=" & mPresentValue & ",AbsentValue=" & mAbsentValue _
            & ",Holiday_Value=" & mHoliday_Value & ",Wo_Value=" & mWO_Value _
            & ",OutWorkDuration=" & mOutWorkDuration & " where paycode='" & Trim(rsEmp.Tables(0).Rows(emprow)("Paycode").ToString.Trim) _
            & "' and dateoffice = '" & Convert.ToDateTime(mTime.Tables(0).Rows(0)("dateoffice")).ToString("yyyy-MM-dd") & "'"
        'Cn.Execute sSql, 64
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            Dim cmd As SqlCommand
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        'rsTime.Close

    End Sub
    Public Sub Upd2Multi(ByVal mTime As DataSet, ByVal timerow As Integer, ByVal rsEmp As DataSet, ByVal emprow As Integer, ByVal mIn1tmp As String, ByVal mIn2tmp As String, ByVal mout1tmp As String, ByVal mout2tmp As String, ByVal shiftattended As String, ByVal EmpGrpId As Integer)
        Dim mLate As Integer = 0
        Dim mEarly As Integer = 0
        Dim mLate1 As Integer = 0
        Dim mEarly1 As Integer = 0
        Dim mShiftTime As Integer = 0
        Dim AbsHours As Integer = 0
        Dim isHld As Boolean = False
        Dim OTinHrs As Double = 0
        Dim OTfactor As Double = 0
        Dim mLDed As Integer = 0
        Dim mLDur As Integer = 0
        Dim mLunchDur As Integer = 0
        Dim mOt As Integer = 0
        Dim mOtStartAfter As Integer = 0
        Dim mOtDeductAfter As Integer = 0
        Dim mOtDeductHrs As Integer = 0
        mStatus = "A"
        mHoursworked = 0
        mExcLunchHours = 0
        mOtDuration = 0
        mOSDuration = 0
        motamount = 0
        mEarlyArrival = 0
        mearlydeparture = 0
        mLateArrival = 0
        mLunchEarlyDeparture = 0
        mLunchLateArrival = 0
        mTotalLossHrs = 0
        mLeaveValue = 0
        mPresentValue = 0
        mAbsentValue = 0
        mHoliday_Value = 0
        mWO_Value = 0
        mOutWorkDuration = 0
        Dim mShiftAttended As String = ""
        mShiftAttended = shiftattended

        mIn1M = mIn1tmp
        mIn2M = mIn2tmp
        mout1M = mout1tmp
        mout2M = mout2tmp

        Dum.HldList.Text = Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")
        'if (Dum.HldList.SelectedIndex >= 0)
        If alHld.Contains(Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")) Then
            isHld = True
        Else
            isHld = False
        End If

        Dim ftext As String = ""
        Dum.dShiftlst.Text = mShiftAttended
        ftext = getText(mShiftAttended)
        If ((mShiftAttended <> "OFF") And (mShiftAttended <> "IGN") And (mShiftAttended <> "FLX")) Then
            'Dum.Shiftlist.SelectedIndex  = Dum.dShiftlst.SelectedIndex;
            'Dum.Shiftlist.Text = mShiftAttended;
            'Dum.Shiftlist.SelectedIndex  = Dum.dShiftlst.SelectedIndex;
            'if (Dum.Shiftlist.SelectedIndex < 0)


            Dim Rs As DataSet = New DataSet
            Dim sSql As String = "Select shift from tblshiftmaster "
            If servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Access)
                adapA.Fill(Rs)
            Else
                adap = New SqlDataAdapter(sSql, conSQL)
                adap.Fill(Rs)
            End If
            'Rs = Cn.FillDataSet_SQL(sSql)
            If (Rs.Tables(0).Rows.Count > 0) Then
                Dim i As Integer = 0
                Do While (i < Rs.Tables(0).Rows.Count)
                    arrShift.Add(Rs.Tables(0).Rows(i)(0).ToString.Trim)
                    i = (i + 1)
                Loop
            End If

            If Not arrShift.Contains(mShiftAttended) Then
                'System.Windows.Forms.MessageBox.Show(("Shift " _
                '                + (mShiftAttended + (" is not defined in shift master of paycode " _
                '                + (mTime.Tables(0).Rows(timerow)("paycode").ToString + (" on date " + Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")))))), "TimeWatch", System.Windows.Forms.MessageBoxButtons.OK)
                Return
            End If
            Dim tmp() As String = ftext.Split(" ")

            'mShiftStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(4, 5)))
            mShiftStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(1)))
            ''MsgBox(mShiftStartTime)
            'mShiftEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(10, 5)))
            mShiftEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(2)))
            If (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mShiftEndTime)) < 0) Then
                'mShiftEndTime = (Convert.ToDateTime(mShiftEndTime).AddDays(1).ToString("yyyy-MM-dd") & " " & mShiftEndTime.Substring(11, 5)) 'nitin
                mShiftEndTime = (Convert.ToDateTime(mShiftEndTime).AddDays(1).ToString("yyyy-MM-dd") & " " & tmp(2)) 'nitin
                'mShiftEndTime = (Convert.ToDateTime(mShiftEndTime).AddDays(1).ToString("yyyy-MM-dd") + mShiftEndTime.Substring(11, 6)) 'nitin
            End If

            'mLunchStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(16, 5)))
            mLunchStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(3)))
            If (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mLunchStartTime)) < 0) Then
                'mLunchStartTime = (Convert.ToDateTime(mLunchStartTime).AddDays(1).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(16, 5)))
                mLunchStartTime = (Convert.ToDateTime(mLunchStartTime).AddDays(1).ToString("yyyy-MM-dd") & (" " & tmp(3)))
            End If

            'mLunchEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(22, 5)))
            mLunchEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(4)))
            If (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mLunchEndTime)) < 0) Then
                'mLunchEndTime = (Convert.ToDateTime(mLunchEndTime).AddDays(1).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(22, 5)))
                mLunchEndTime = (Convert.ToDateTime(mLunchEndTime).AddDays(1).ToString("yyyy-MM-dd") & (" " & tmp(4)))
            End If

            mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mLunchStartTime), Convert.ToDateTime(mLunchEndTime)))
            mLunchDur = mLDur
            mLDed = Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(30, 3)))
            'MsgBox(mShiftStartTime)
            mShiftTime = (Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mShiftEndTime))) - mLDed)
        End If

        'MsgBox(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString.Trim & "  lenght: " & mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString.Length)
        Dim tmpL As Double
        If mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString.Length = 0 Then
            tmpL = 0
        Else
            tmpL = Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString)
        End If
        'If (Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString) > 0) Then   'main
        If (tmpL > 0) Then    'nitin
            If (Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")) = 0.25) Then
                mStatus = "Q_"
            ElseIf (Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")) = 0.5) Then
                mStatus = "H_"
            ElseIf (Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")) = 0.75) Then
                mStatus = "T_"
            Else
                mStatus = ""
            End If

            'mStatus = (mStatus.ToString.Trim + mTime.Tables(timerow).Rows(0)("LeaveCode").ToString) 'nitin
            mStatus = (mStatus.ToString.Trim + mTime.Tables(0).Rows(timerow)("LeaveCode").ToString)
            If SimulateIsDate.IsDate(mIn1M) Then
                If ((mTime.Tables(0).Rows(timerow)("ShiftAttended").ToString.Trim <> "IGN") _
                            AndAlso ((mTime.Tables(0).Rows(timerow)("ShiftAttended").ToString <> "OFF") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("ShiftAttended").ToString <> "FLX") _
                            AndAlso Not isHld)) Then
                    mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1M), Convert.ToDateTime(mShiftStartTime)))
                    If (mEarly < 0) Then
                        mEarly = 0
                    End If
                End If

                If SimulateIsDate.IsDate(mout2M) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1M), Convert.ToDateTime(mout2M)))
                    If SimulateIsDate.IsDate("in2") Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1M), Convert.ToDateTime(mIn2M)))
                        If (mLDur < mLDed) Then
                            mLDur = mLDed
                        End If

                        mHoursworked = (mHoursworked - mLDur)
                    Else
                        mHoursworked = (mHoursworked - mLDed)
                    End If

                End If

                Select Case (Convert.ToString(mTime.Tables(0).Rows(timerow)("LeaveType")))
                    Case "P"
                        mPresentValue = 1
                    Case "L"
                        mLeaveValue = (0 + Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                        mPresentValue = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                    Case Else
                        mAbsentValue = (0 + Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                        mPresentValue = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                End Select

            ElseIf (Not String.IsNullOrEmpty(mTime.Tables(0).Rows(timerow)("LeaveType1").ToString.Trim) _
                        AndAlso Not String.IsNullOrEmpty(mTime.Tables(0).Rows(timerow)("LeaveType2").ToString.Trim)) Then
                mStatus = (mTime.Tables(0).Rows(timerow)("FirstHalfLeavecode").ToString.Trim + mTime.Tables(0).Rows(timerow)("SecondHalfLeavecode").ToString.Trim)
                If (((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString.Trim = "P") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString.Trim = "A")) _
                            OrElse ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString.Trim = "A") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "P"))) Then
                    mPresentValue = 0.5
                    mLeaveValue = 0
                    mAbsentValue = 0.5
                End If

                If (((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "L") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "P")) _
                            OrElse ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "P") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "L"))) Then
                    mPresentValue = 0.5
                    mLeaveValue = 0.5
                    mAbsentValue = 0
                End If

                If (((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "L") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "A")) _
                            OrElse ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "A") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "L"))) Then
                    mPresentValue = 0
                    mLeaveValue = 0.5
                    mAbsentValue = 0.5
                End If

                If ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "L") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "L")) Then
                    mPresentValue = 0
                    mLeaveValue = 1
                    mAbsentValue = 0
                End If

                If ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "A") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "A")) Then
                    mPresentValue = 0
                    mLeaveValue = 0
                    mAbsentValue = 1
                End If

                If ((mTime.Tables(0).Rows(timerow)("LeaveType1").ToString = "P") _
                            AndAlso (mTime.Tables(0).Rows(timerow)("LeaveType2").ToString = "P")) Then
                    mPresentValue = 1
                    mLeaveValue = 0
                    mAbsentValue = 0
                End If

            Else
                If (mTime.Tables(0).Rows(timerow)("LeaveType").ToString = "P") Then
                    mPresentValue = Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString)
                ElseIf (mTime.Tables(0).Rows(timerow)("LeaveType").ToString = "L") Then
                    mLeaveValue = Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount"))
                ElseIf (mTime.Tables(0).Rows(timerow)("LeaveType").ToString.Trim = "A") Then
                    mAbsentValue = Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount"))
                End If

                If (mTime.Tables(0).Rows(timerow)("ShiftAttended").ToString = "OFF") Then
                    mWO_Value = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                ElseIf isHld Then
                    mHoliday_Value = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                ElseIf (ftext.ToString.Substring(28, 1) = "H") Then
                    mWO_Value = 0.5
                ElseIf (mTime.Tables(0).Rows(timerow)("LEAVETYPE").ToString.Trim <> "A") Then
                    mAbsentValue = (1 - Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount")))
                End If

            End If

            GoTo Lastl
        End If

        If ((mShiftAttended = "OFF") _
                    AndAlso isHld) Then
            mHoliday_Value = 1
            mStatus = "WOH"
            If SimulateIsDate.IsDate(mIn1M) Then
                mStatus = "PWH"
                If ((EmpGrpArr(EmpGrpId).g_isPresentOnWOPresent = "Y") _
                            OrElse (EmpGrpArr(EmpGrpId).g_isPresentOnHLDPresent = "Y")) Then
                    mPresentValue = 1
                    mHoliday_Value = 0
                End If

                If SimulateIsDate.IsDate(mout2M) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1M), Convert.ToDateTime(mout2M)))
                    If SimulateIsDate.IsDate(mIn2M) Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1M), Convert.ToDateTime(mIn2M)))
                        mHoursworked = (mHoursworked - mLDur)
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOS").ToString.Trim = "Y") Then
                        mOSDuration = mHoursworked
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString.Trim = "Y") Then
                        mOtDuration = mHoursworked
                    End If

                End If

            End If
            '''
            If EmpGrpArr(EmpGrpId).ISCOMPOFF = "Y" Then
                mOtDuration = 0
            End If
            GoTo Lastl
        End If

        If (mShiftAttended = "OFF") Then
            mWO_Value = 1
            mStatus = "WO"
            If SimulateIsDate.IsDate(mIn1M) Then
                mStatus = "POW"
                If (EmpGrpArr(EmpGrpId).g_isPresentOnWOPresent = "Y") Then
                    mWO_Value = 0
                    mPresentValue = 1
                End If

                If SimulateIsDate.IsDate(mout2M) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1M), Convert.ToDateTime(mout2M)))
                    If SimulateIsDate.IsDate(mIn2M) Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1M), Convert.ToDateTime(mIn2M)))
                        mHoursworked = (mHoursworked - mLDur)
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOS").ToString.Trim = "Y") Then
                        mOSDuration = mHoursworked
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString.Trim = "Y") Then
                        mOtDuration = (mHoursworked - EmpGrpArr(EmpGrpId).g_Wo_OT)
                        If mOtDuration < 0 Then
                            mOtDuration = 0
                        End If
                    End If

                End If

            End If
            '''
            If EmpGrpArr(EmpGrpId).ISCOMPOFF = "Y" Then
                mOtDuration = 0
            End If
            GoTo Lastl
        End If

        If alHld.Contains(Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")) Then
            mHoliday_Value = 1
            mStatus = "HLD"
            If SimulateIsDate.IsDate(mIn1M) Then
                mStatus = "POH"
                If (EmpGrpArr(EmpGrpId).g_isPresentOnHLDPresent = "Y") Then
                    mHoliday_Value = 0
                    mPresentValue = 1
                End If

                If SimulateIsDate.IsDate(mout2M) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1M), Convert.ToDateTime(mout2M)))
                    If SimulateIsDate.IsDate("in2") Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1M), Convert.ToDateTime(mIn2M)))
                        mHoursworked = (mHoursworked - mLDur)
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOS").ToString = "Y") Then
                        mOSDuration = mHoursworked
                    End If

                    If (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString = "Y") Then
                        mOtDuration = (mHoursworked - EmpGrpArr(EmpGrpId).g_Hld_OT)
                    End If

                End If

            End If

            GoTo Lastl
        End If
        If (SimulateIsDate.IsDate(mIn1M) _
                    AndAlso (rsEmp.Tables(0).Rows(emprow)("InOnly").ToString.Trim <> "N")) Then
            If Not ((rsEmp.Tables(0).Rows(emprow)("InOnly").ToString = "O") AndAlso SimulateIsDate.IsDate(mout2M)) Then
                mout2M = mShiftEndTime
            End If
        End If
        If ((rsEmp.Tables(0).Rows(emprow)("IsPunchAll").ToString.Trim = "N") AndAlso Not SimulateIsDate.IsDate(mIn1M)) Then
            mIn1M = mShiftStartTime
            mout2M = mShiftEndTime
        End If
        If ((rsEmp.Tables(0).Rows(emprow)("IsPunchAll").ToString = "N") AndAlso (SimulateIsDate.IsDate(mIn1M) AndAlso Not SimulateIsDate.IsDate(mout2M))) Then
            If (Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1M), Convert.ToDateTime(mShiftEndTime))) > 0) Then
                mout2M = mShiftEndTime
            Else
                mout2M = mIn1M
                mIn1M = mShiftStartTime
            End If
        End If

        If ((SimulateIsDate.IsDate(mIn1M) _
                    And Not SimulateIsDate.IsDate(mout2M)) _
                    Or (Not SimulateIsDate.IsDate(mIn1M) _
                    And SimulateIsDate.IsDate(mout2M))) Then
            mStatus = "MIS"
            mPresentValue = 1

            'mark half day if miss punch
            If rsEmp.Tables(0).Rows(emprow).Item("MARKMISSASHALFDAY").ToString.Trim = "Y" Then
                mStatus = "HLF"
                mPresentValue = 0.5
                mAbsentValue = 0.5
            ElseIf EmpGrpArr(EmpGrpId).g_MIS = True Then
                mStatus = "A"
                mPresentValue = 0
                mAbsentValue = 1
            End If

            If (ftext.ToString.Trim.Substring(28, 1) = "H") Then
                If (mLeaveValue < 0.5) Then
                    mPresentValue = (0.5 - mLeaveValue)
                End If

                mWO_Value = 0.5
            End If

            If (mShiftAttended <> "IGN") And (mShiftAttended <> "FLX") Then
                If SimulateIsDate.IsDate(mIn1M) Then
                    mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1M), Convert.ToDateTime(mShiftStartTime)))
                    If (mEarly > 0) Then
                        mEarlyArrival = mEarly
                    ElseIf (Math.Abs(mEarly) > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("PERMISLATEARRIVAL"))) Then
                        mLateArrival = Math.Abs(mEarly)
                    End If
                End If
            End If
            GoTo Lastl
        End If
        If ((ftext.ToString.Trim.Substring(28, 1) = "H") AndAlso (mLeaveValue = 0)) Then
            mWO_Value = 0.5
            If SimulateIsDate.IsDate(mIn1M) Then
                mPresentValue = 0.5
            Else
                mAbsentValue = 0.5
            End If

        End If

        If ((ftext.ToString.Trim.Substring(28, 1) = "H") AndAlso (mLeaveValue > 0)) Then
            mWO_Value = 0.5
            If (SimulateIsDate.IsDate(mIn1M) _
                        And (mLeaveValue < 0.5)) Then
                mPresentValue = (0.5 - mLeaveValue)
            ElseIf (mLeaveValue < 0.5) Then
                mAbsentValue = (0.5 - mLeaveValue)
            End If

        End If

        If (SimulateIsDate.IsDate(mout2M) And SimulateIsDate.IsDate(mIn1M)) Then
            mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1M), Convert.ToDateTime(mout2M)))
            If SimulateIsDate.IsDate(mIn2M) Then
                mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1M), Convert.ToDateTime(mIn2M)))
                If (mLDur < mLDed) Then
                    mLDur = mLDed
                End If
                mHoursworked = (mHoursworked - mLDur)
            Else
                mHoursworked = (mHoursworked - mLDed)
                'mHoursworked = (mHoursworked - mLDur)
            End If
            If (mShiftAttended <> "IGN") And (mShiftAttended <> "FLX") Then
                'nitin start
                Dim tmp As Integer
                If servername = "Access" Then
                    tmp = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time1"))
                Else
                    tmp = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))
                End If
                'nitin end
                'If (mHoursworked < Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))) Then
                If (mHoursworked < tmp) Then
                    mHoursworked = 0
                    mAbsentValue = 1
                    GoTo Lastl
                End If

                If (SimulateIsDate.IsDate(mIn2M) And SimulateIsDate.IsDate(mLunchStartTime)) Then
                    mEarly1 = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1M), Convert.ToDateTime(mLunchStartTime)))
                    If (mEarly1 > 0) Then
                        mLunchEarlyDeparture = mEarly1
                    End If

                    mLate1 = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mLunchEndTime), Convert.ToDateTime(mIn2M)))
                    If (mLate1 > 0) Then
                        mLunchLateArrival = mLate1
                    End If

                    If (mLDur > mLunchDur) Then
                        mExcLunchHours = (mLDur - mLunchDur)
                    End If
                End If

                If (rsEmp.Tables(0).Rows(emprow)("IsOS").ToString.Trim = "Y") Then
                    'mOSDuration = ((mStatus == "HLF") ? mHoursworked - 240 : mHoursworked - mShiftTime);
                    If (mStatus = "HLF") Then
                        mOSDuration = mHoursworked - 240
                    Else
                        mOSDuration = mHoursworked - mShiftTime
                    End If
                    '(mStatus = "HLF")
                    '(mHoursworked - mShiftTime)
                    If (mOSDuration < 0) Then
                        mOSDuration = 0
                    End If
                End If

                mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1M), Convert.ToDateTime(mShiftStartTime)))
                If (mEarly > 0) Then
                    mEarlyArrival = mEarly
                ElseIf (Math.Abs(mEarly) > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("PERMISLATEARRIVAL"))) Then
                    mLateArrival = Math.Abs(mEarly)
                End If

                mLate = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2M)))
                If ((mLate < 0) _
                            And (Math.Abs(mLate) > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("PERMISEARLYDEPRT")))) Then
                    mearlydeparture = Math.Abs(mLate)
                End If

                mStatus = "P"
                If (ftext.ToString.Trim.Substring(28, 1) = "H") Then
                    If (mLeaveValue < 0.5) Then
                        mPresentValue = (0.5 - mLeaveValue)
                    End If

                    mAbsentValue = 0
                Else
                    mPresentValue = 1
                End If

                AbsHours = (Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mShiftEndTime))) - mHoursworked)
                If Not String.IsNullOrEmpty(mLunchStartTime) Then
                    AbsHours = (AbsHours - Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mLunchStartTime), Convert.ToDateTime(mLunchEndTime))))
                End If

                If (AbsHours > 0) Then
                    'nitin start
                    Dim tmp1, tmpsort As Integer
                    If servername = "Access" Then
                        tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time1"))
                        tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short1"))
                    Else
                        tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))
                        tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))
                    End If
                    'nitin end
                    'If ((rsEmp.Tables(0).Rows(emprow)("IsHalfDay").ToString = "Y") _
                    '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
                    '            AndAlso (mHoursworked >= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))))) Then   'nitin
                    If ((rsEmp.Tables(0).Rows(emprow)("IsHalfDay").ToString = "Y") _
                           AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
                           AndAlso (mHoursworked >= tmp1))) Then    'nitin
                        mStatus = "HLF"
                        mPresentValue = 0.5
                        mAbsentValue = 0.5
                        mLateArrival = 0
                        mearlydeparture = 0
                        mTotalLossHrs = 0
                        'ElseIf ((rsEmp.Tables(0).Rows(emprow)("isHalfday").ToString = "Y") _
                        '            AndAlso ((rsEmp.Tables(0).Rows(emprow)("isShort").ToString = "Y") _
                        '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))) _
                        '            AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))))) Then  'nitin
                    ElseIf ((rsEmp.Tables(0).Rows(emprow)("isHalfday").ToString = "Y") _
                          AndAlso ((rsEmp.Tables(0).Rows(emprow)("isShort").ToString = "Y") _
                          AndAlso ((mHoursworked <= tmpsort) _
                          AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))))) Then   'nitin
                        mStatus = "SRT"
                        mPresentValue = 1
                    End If

                End If

                If (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString = "Y") Then
                    'Calculate OT
                    Select Case (EmpGrpArr(EmpGrpId).g_OTFormulae)
                        Case 1
                            'Out - End
                            mOt = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2M)))
                        Case 2
                            ' Hours Worked - Shift Hours
                            mOt = (mHoursworked - mShiftTime)
                        Case 3
                            ' Early Arrival + Late Departure
                            mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1M), Convert.ToDateTime(mShiftStartTime)))
                            If (mEarly <= EmpGrpArr(EmpGrpId).g_OTEarly) Then
                                mEarly = 0
                            End If

                            '                        If mEarly < 0 And Abs(mEarly) <= g_OTLate Then
                            '                            mEarly = 0
                            '                        End If
                            '                        If mEarly > 0 And g_OTChkEarly = "Y" And mEarly <= g_OTEarly Then
                            '                            mEarly = 0
                            '                        End If
                            mLate = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2M)))
                            If (mLate <= EmpGrpArr(EmpGrpId).g_OTLate) Then
                                mLate = 0
                            End If

                            If (Math.Abs(mLate) <= EmpGrpArr(EmpGrpId).g_OTEnd) Then
                                mLate = 0
                            End If

                            mOt = (mEarly _
                                        + (mLate - mExcLunchHours))
                    End Select

                    mOtStartAfter = Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(33, 3)))
                    mOtDeductAfter = Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(37, 4)))
                    Try
                        mOtDeductHrs = Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(42, 3)))
                    Catch ex As Exception
                        mOtDeductHrs = 0
                    End Try

                    If (mOt < mOtStartAfter) Then
                        mOt = 0
                    ElseIf ((mOtDeductHrs = 0) _
                                AndAlso (mOt > mOtDeductAfter)) Then
                        mOt = mOtDeductAfter
                    ElseIf (mOt _
                                >= (mOtDeductAfter + mOtDeductHrs)) Then
                        mOt = (mOt - mOtDeductHrs)
                    End If

                    If ((g_OTMinus = "N") _
                                AndAlso (mOt < 0)) Then
                        mOt = 0
                    End If
                    mOtDuration = mOt
                End If
                GoTo Lastl
            Else
                mPresentValue = 1
                mStatus = "P"
            End If

        End If

        If ((mLeaveValue = 0) _
                    AndAlso ((mPresentValue = 0) _
                    AndAlso ((mHoliday_Value = 0) _
                    AndAlso (mWO_Value = 0)))) Then
            mAbsentValue = 1
        End If

        If mShiftAttended = "FLX" Then
            'nitin start
            Dim tmp1, tmpsort As Integer
            If servername = "Access" Then
                tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time1"))
                tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short1"))
            Else
                tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))
                tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))
            End If
            'nitin end
            'If ((rsEmp.Tables(0).Rows(emprow)("IsHalfDay").ToString = "Y") _
            '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
            '            AndAlso (mHoursworked >= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))))) Then   'nitin
            If mHoursworked < Convert.ToInt32(tmp1) Then
                mStatus = "A"
                mPresentValue = 0
                mAbsentValue = 1
                mLateArrival = 0
                mearlydeparture = 0
                mTotalLossHrs = 0
            ElseIf (((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
                   AndAlso (mHoursworked >= tmp1))) Then    'nitin
                mStatus = "HLF"
                mPresentValue = 0.5
                mAbsentValue = 0.5
                mLateArrival = 0
                mearlydeparture = 0
                mTotalLossHrs = 0
                'ElseIf ((rsEmp.Tables(0).Rows(emprow)("isHalfday").ToString = "Y") _
                '            AndAlso ((rsEmp.Tables(0).Rows(emprow)("isShort").ToString = "Y") _
                '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))) _
                '            AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))))) Then  'nitin
            ElseIf ((mHoursworked < tmpsort) _
                  AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))) Then   'nitin
                mStatus = "SRT"
                mPresentValue = 1
            ElseIf mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString) And
                    mHoursworked > tmp1 And mHoursworked > tmpsort Then
                mStatus = "P"
                mPresentValue = 1
                mAbsentValue = 0
                mLateArrival = 0
                mearlydeparture = 0
                mTotalLossHrs = 0
            End If
        End If
Lastl:
        If ((mStatus = "HLF") _
                    AndAlso (rsEmp.Tables(0).Rows(emprow)("IsOT").ToString = "Y")) Then
            mOtDuration = (mHoursworked - 240)
        End If

        If (EmpGrpArr(EmpGrpId).g_OtRound = "Y") Then
            OTinHrs = Convert.ToDouble(HM.minutetohour1(mOtDuration.ToString.Trim)) 'Convert.ToDouble(HM.minutetohour(mOtDuration.ToString.Trim))
            OTfactor = (OTinHrs - CType(Math.Floor(OTinHrs), Integer))
            If (OTfactor < 0.15) Then
                OTinHrs = CType(Math.Floor(OTinHrs), Integer)
            ElseIf ((OTfactor >= 0.15) _
                        AndAlso (OTfactor < 0.45)) Then
                OTinHrs = (CType(Math.Floor(OTinHrs), Integer) + 0.3)
            ElseIf (OTfactor >= 0.45) Then
                OTinHrs = (CType(Math.Floor(OTinHrs), Integer) + 1)
            End If

            mOtDuration = HM.hour(OTinHrs.ToString.Trim)
        End If

        If (rsEmp.Tables(0).Rows(emprow)("istimelossallowed").ToString = "Y") Then
            mTotalLossHrs = (mLateArrival _
                        + (mearlydeparture _
                        + (mLunchLateArrival + mLunchEarlyDeparture)))
        Else
            mLateArrival = 0
            mearlydeparture = 0
            mTotalLossHrs = 0
            mLunchLateArrival = 0
            mLunchEarlyDeparture = 0
        End If

        If (SimulateVal.Val(rsEmp.Tables(0).Rows(emprow)("OTRate").ToString.Trim) = 0) Then
            motamount = 0
        Else
            motamount = (mOtDuration * (Convert.ToDouble(rsEmp.Tables(0).Rows(emprow)("OtRate")) / 60))
        End If
    End Sub
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    Public Sub loadSmSSetting()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet

        Dim sSql As String = "select * from tblSMS"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            g_SMSApplicable = "Y"
            mInSMS = ds.Tables(0).Rows(0).Item("IsIn").ToString.Trim
        Else
            g_SMSApplicable = "N"
        End If
    End Sub
    Public Shared Sub Load_SMS_Policy()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = "select * from tblSMS"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(Rs)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(Rs)
        End If
        If Rs.Tables(0).Rows.Count > 0 Then
            G_SmsKey = Trim(Rs.Tables(0).Rows(0).Item("SMSKey").ToString)
            G_SenderID = Trim(Rs.Tables(0).Rows(0).Item("SenderID").ToString)

            g_isLate = Rs.Tables(0).Rows(0).Item("IsLate").ToString.Trim
            g_LateContent1 = Rs.Tables(0).Rows(0).Item("LateSMS1").ToString.Trim
            g_LateContent2 = Rs.Tables(0).Rows(0).Item("LateSMS2").ToString.Trim

            g_isInSMS = Rs.Tables(0).Rows(0).Item("IsIn").ToString.Trim
            g_InContent1 = Rs.Tables(0).Rows(0).Item("InSMS1").ToString.Trim
            g_InContent2 = Rs.Tables(0).Rows(0).Item("InSMS2").ToString.Trim

            g_isOutSMS = Rs.Tables(0).Rows(0).Item("IsOut").ToString.Trim
            g_OutContent1 = Rs.Tables(0).Rows(0).Item("OutSMS1").ToString.Trim
            g_OutContent2 = Rs.Tables(0).Rows(0).Item("OutSMS2").ToString.Trim
            g_OutSMSAfter = Rs.Tables(0).Rows(0).Item("OutSMSAfter").ToString.Trim

            g_isAbsentSMS = Rs.Tables(0).Rows(0).Item("IsAbsent").ToString.Trim
            g_AbsContent1 = Rs.Tables(0).Rows(0).Item("AbsentSMS1").ToString.Trim
            g_AbsContent2 = Rs.Tables(0).Rows(0).Item("AbsentSMS2").ToString.Trim

            g_isAllSMS = Rs.Tables(0).Rows(0).Item("IsAll").ToString.Trim
            g_AllContent1 = Rs.Tables(0).Rows(0).Item("AllSMS1").ToString.Trim
            g_AllContent2 = Rs.Tables(0).Rows(0).Item("AllSMS2").ToString.Trim


            g_AbsMsgFor = Rs.Tables(0).Rows(0).Item("AbsentSMSFor").ToString.Trim
            g_AbsMsgTime = Rs.Tables(0).Rows(0).Item("AbsentSMSTime").ToString.Trim
            g_AbsSMSAfter = Rs.Tables(0).Rows(0).Item("AbsentSMSAfter").ToString.Trim
            g_AbsName = Rs.Tables(0).Rows(0).Item("AbsentName").ToString.Trim
            g_AbsDate = Rs.Tables(0).Rows(0).Item("AbsentDate").ToString.Trim
            g_DeviceWiseInOut = Rs.Tables(0).Rows(0).Item("DeviceWiseInOut").ToString.Trim

            If g_isLate = "Y" Or g_isInSMS = "Y" Or g_isOutSMS = "Y" Or g_isAbsentSMS = "Y" Or g_isAllSMS = "Y" Or g_DeviceWiseInOut = "Y" Then
                g_SMSApplicable = "Y"
            Else
                g_SMSApplicable = "N"
            End If
        Else
            g_SMSApplicable = "N"
        End If
        Exit Sub
    End Sub
    Sub Remove_Duplicate_Punches(DupDate As DateTime, paycode As String, ByVal EmpGrpId As Integer)
        XtraMasterTest.LabelControlStatus.Text = "Removing Duplicate Punches for " & paycode & " of " & DupDate.ToString("dd/MM/yyyy")
        Application.DoEvents()
        'On Error GoTo ErrorGen
        'Load_Corporate_PolicySql()

        'Dim adapS As SqlDataAdapter
        'Dim adapAc As OleDbDataAdapter
        'Dim RsTmp As DataSet = New DataSet
        'Dim sSql As String = " Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tb" & _
        '"lsetup )"
        'If servername = "Access" Then
        '    sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
        '    adapAc = New OleDbDataAdapter(sSql, con1)
        '    adapAc.Fill(RsTmp)
        'Else
        '    adapS = New SqlDataAdapter(sSql, con)
        '    adapS.Fill(RsTmp)
        'End If
        'g_DupCheckMin = RsTmp.Tables(0).Rows(0).Item("DuplicateCheckMin").ToString.Trim
        Dim mEmpcd As String, mDate As Date
        Dim S_Dupli As Integer
        Dim Rs As DataSet = New DataSet
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        'ADODB.Recordset

        S_Dupli = Convert.ToInt16(EmpGrpArr(EmpGrpId).g_DupCheckMin) '5 ' We Have To Take This From The TblSetup.
        'MsgBox("S_Dupli " & S_Dupli.ToString)
        If servername = "Access" Then
            sSql = "Select * from Machinerawpunch where FORMAT(officepunch,'yyyy-MM-dd HH:mm:ss') between '" & DupDate.ToString("yyyy-MM-dd 00:00:00") & "' and '" & DupDate.ToString("yyyy-MM-dd 23:59:59") & "' and Paycode='" & paycode & "' Order By Cardno,officepunch"
            adapA = New OleDbDataAdapter(sSql, con1Access)
            adapA.Fill(Rs)
        Else
            sSql = "Select * from Machinerawpunch where officepunch between '" & DupDate.ToString("yyyy-MM-dd 00:00:00") & "' and '" & DupDate.ToString("yyyy-MM-dd 23:59:59") & "' and Paycode='" & paycode & "'  Order By Cardno,officepunch"
            adap = New SqlDataAdapter(sSql, conSQL)
            adap.Fill(Rs)
        End If
        'Rs = Cn.Execute(sSql)
        If Rs.Tables(0).Rows.Count > 0 Then
            'Rs.MoveFirst()
            If servername = "Access" Then
                If con1Access.State <> ConnectionState.Open Then
                    con1Access.Open()
                End If
            Else
                If conSQL.State <> ConnectionState.Open Then
                    conSQL.Open()
                End If
            End If
            For i As Integer = 0 To Rs.Tables(0).Rows.Count - 1
                'Do While Not Rs.EOF
                mEmpcd = Rs.Tables(0).Rows(i).Item("Cardno").ToString.Trim
                mDate = Rs.Tables(0).Rows(i).Item("officePunch").ToString.Trim
                i = i + 1
                'Rs.MoveNext()
                If i > Rs.Tables(0).Rows.Count - 1 Then
                    Exit For
                End If
                'If Rs.EOF Then
                '    Exit Do
                'End If
                Do While mEmpcd = Rs.Tables(0).Rows(i).Item("Cardno").ToString.Trim
                    'If DateDiff("n", mDate, Convert.ToDateTime(Rs.Tables(0).Rows(i).Item("OfficePunch").ToString.Trim)) <= S_Dupli Then
                    If Convert.ToDateTime(Rs.Tables(0).Rows(i).Item("OfficePunch").ToString.Trim).Subtract(mDate).TotalMinutes <= S_Dupli Then
                        If servername = "Access" Then
                            sSql = "Delete from MachineRawPunch Where Cardno='" & mEmpcd & "' And FORMAT(OfficePunch,'yyyy-MM-dd HH:mm:ss') = '" & Convert.ToDateTime(Rs.Tables(0).Rows(i).Item("OfficePunch").ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                            cmd1 = New OleDbCommand(sSql, con1Access)
                            cmd1.ExecuteNonQuery()
                        Else
                            'Dim x As String = Rs.Tables(0).Rows(i).Item("OfficePunch").ToString.Trim
                            'Dim XX As DateTime
                            'DateTime.TryParse(Rs.Tables(0).Rows(i).Item("OfficePunch").ToString.Trim, XX)
                            sSql = "Delete from MachineRawPunch Where Cardno='" & mEmpcd & "' And OfficePunch = '" & Convert.ToDateTime(Rs.Tables(0).Rows(i).Item("OfficePunch").ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                            cmd = New SqlCommand(sSql, conSQL)
                            cmd.ExecuteNonQuery()
                        End If
                        'Cn.Execute(sSql)
                    Else
                        mDate = Convert.ToDateTime(Rs.Tables(0).Rows(i).Item("OfficePunch").ToString.Trim) 'Rs("OfficePunch")
                    End If
                    i = i + 1
                    If i > Rs.Tables(0).Rows.Count - 1 Then
                        Exit Do
                    End If
                    'Rs.MoveNext()
                    'If Rs.EOF Then
                    '    Exit Do
                    'End If
                Loop
            Next
            If servername = "Access" Then
                If con1Access.State <> ConnectionState.Closed Then
                    con1Access.Close()
                End If
            Else
                If conSQL.State <> ConnectionState.Closed Then
                    conSQL.Close()
                End If
            End If
            'Loop
            'Rs.Close()

        Else
            'Rs.Close()
        End If
    End Sub
    Public Function Upd(ByVal mTime As DataSet, ByVal EmpGpId As Integer) As String
        Dim mLate As Integer = 0
        Dim mEarly As Integer = 0
        Dim mLate1 As Integer = 0
        Dim mEarly1 As Integer = 0
        Dim mShiftTime As Integer = 0
        Dim AbsHours As Integer = 0
        Dim isHld As Boolean = False
        Dim OTinHrs As Double = 0
        Dim OTfactor As Double = 0
        Dim mLDed As Integer = 0
        Dim mLDur As Integer = 0
        Dim mLunchDur As Integer = 0
        Dim mOt As Integer = 0
        Dim mOtStartAfter As Integer = 0
        Dim mOtDeductAfter As Integer = 0
        Dim mOtDeductHrs As Integer = 0
        mStatus = "A"
        mHoursworked = 0
        mExcLunchHours = 0
        mOtDuration = 0
        mOSDuration = 0
        motamount = 0
        mEarlyArrival = 0
        mearlydeparture = 0
        mLateArrival = 0
        mLunchEarlyDeparture = 0
        mLunchLateArrival = 0
        mTotalLossHrs = 0
        mLeaveValue = 0
        mPresentValue = 0
        mAbsentValue = 0
        mHoliday_Value = 0
        mWO_Value = 0
        mOutWorkDuration = 0
        Dim mShiftAttended As String = ""
        mShiftAttended = mTime.Tables(0).Rows(0)("SHIFT").ToString.Trim ' shiftattended
        Dim rsEmp As DataSet

        '     sSql = "Select a.*,b.companycode,b.departmentcode, b.BRANCHCODE from tblemployeeshiftmaster a,tblemployee b where a.paycode" & _
        '"=b.paycode and a.ISROUNDTHECLOCKWORK='N' AND A.PAYCODE='" & mTime.Tables(0).Rows(0)("PAYCODE").ToString.Trim & "'    " ' with branch code"
        sSql = "Select a.*,b.companycode,b.departmentcode, b.BRANCHCODE from tblemployeeshiftmaster a,tblemployee b where a.paycode" & _
  "=b.paycode AND A.PAYCODE='" & mTime.Tables(0).Rows(0)("PAYCODE").ToString.Trim & "'    " ' with branch code"
        rsEmp = New DataSet 'nitin
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(rsEmp)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(rsEmp)
        End If
        If mTime.Tables(0).Rows(0)("IN1").ToString.Trim = "" Then : mIn1 = "" : Else : mIn1 = Convert.ToDateTime(mTime.Tables(0).Rows(0)("IN1")).ToString("yyyy-MM-dd HH:mm") : End If
        If mTime.Tables(0).Rows(0)("IN2").ToString.Trim = "" Then : mIn2 = "" : Else : mIn2 = Convert.ToDateTime(mTime.Tables(0).Rows(0)("IN2")).ToString("yyyy-MM-dd HH:mm") : End If
        If mTime.Tables(0).Rows(0)("out1").ToString.Trim = "" Then : mout1 = "" : Else : mout1 = Convert.ToDateTime(mTime.Tables(0).Rows(0)("out1")).ToString("yyyy-MM-dd HH:mm") : End If
        If mTime.Tables(0).Rows(0)("out2").ToString.Trim = "" Then : mout2 = "" : Else : mout2 = Convert.ToDateTime(mTime.Tables(0).Rows(0)("out2")).ToString("yyyy-MM-dd HH:mm") : End If

        'mIn1 = Convert.ToDateTime(mTime.Tables(0).Rows(0)("IN1")).ToString("yyyy-MM-dd HH:mm") 'mIn1tmp
        'mIn2 = Convert.ToDateTime(mTime.Tables(0).Rows(0)("IN2")).ToString("yyyy-MM-dd HH:mm") 'mIn2tmp
        'mout1 = Convert.ToDateTime(mTime.Tables(0).Rows(0)("out1")).ToString("yyyy-MM-dd HH:mm") 'mout1tmp
        'mout2 = Convert.ToDateTime(mTime.Tables(0).Rows(0)("out2")).ToString("yyyy-MM-dd HH:mm") 'mout2tmp

        Dum.HldList.Text = Convert.ToDateTime(mTime.Tables(0).Rows(0)("DateOffice")).ToString("yyyy-MM-dd")
        'if (Dum.HldList.SelectedIndex >= 0)
        If alHld.Contains(Convert.ToDateTime(mTime.Tables(0).Rows(0)("DateOffice")).ToString("yyyy-MM-dd")) Then
            isHld = True
        Else
            isHld = False
        End If
        Dim ftext As String = ""
        Dum.dShiftlst.Text = mShiftAttended
        ftext = getText(mShiftAttended)
        If ((mShiftAttended <> "OFF") And (mShiftAttended <> "IGN") And (mShiftAttended <> "FLX")) Then
            'Dum.Shiftlist.SelectedIndex  = Dum.dShiftlst.SelectedIndex;
            'Dum.Shiftlist.Text = mShiftAttended;
            'Dum.Shiftlist.SelectedIndex  = Dum.dShiftlst.SelectedIndex;
            'if (Dum.Shiftlist.SelectedIndex < 0)

            Dim Rs As DataSet = New DataSet
            Dim sSql As String = "Select shift from tblshiftmaster"
            If servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1)
                adapA.Fill(Rs)
            Else
                adap = New SqlDataAdapter(sSql, con)
                adap.Fill(Rs)
            End If
            'Rs = Cn.FillDataSet_SQL(sSql)
            If (Rs.Tables(0).Rows.Count > 0) Then
                Dim i As Integer = 0
                Do While (i < Rs.Tables(0).Rows.Count)
                    arrShift.Add(Rs.Tables(0).Rows(i)(0).ToString.Trim)
                    i = (i + 1)
                Loop
            End If

            If Not arrShift.Contains(mShiftAttended) Then
                'System.Windows.Forms.MessageBox.Show(("Shift " _
                '                + (mShiftAttended + (" is not defined in shift master of paycode " _
                '                + (mTime.Tables(0).Rows(timerow)("paycode").ToString + (" on date " + Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("DateOffice")).ToString("yyyy-MM-dd")))))), "TimeWatch", System.Windows.Forms.MessageBoxButtons.OK)
                'Return
            End If
            Dim tmp() As String = ftext.Split(" ")

            'mShiftStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(4, 5)))
            mShiftStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(0).Item("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(1)))
            ''MsgBox(mShiftStartTime)
            'mShiftEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(10, 5)))
            mShiftEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(0).Item("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(2)))
            If (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mShiftEndTime)) < 0) Then
                'mShiftEndTime = (Convert.ToDateTime(mShiftEndTime).AddDays(1).ToString("yyyy-MM-dd") & " " & mShiftEndTime.Substring(11, 5)) 'nitin
                mShiftEndTime = (Convert.ToDateTime(mShiftEndTime).AddDays(1).ToString("yyyy-MM-dd") & " " & tmp(2)) 'nitin
                'mShiftEndTime = (Convert.ToDateTime(mShiftEndTime).AddDays(1).ToString("yyyy-MM-dd") + mShiftEndTime.Substring(11, 6)) 'nitin
            End If

            'mLunchStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(16, 5)))
            mLunchStartTime = (Convert.ToDateTime(mTime.Tables(0).Rows(0).Item("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(3)))
            If (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mLunchStartTime)) < 0) Then
                'mLunchStartTime = (Convert.ToDateTime(mLunchStartTime).AddDays(1).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(16, 5)))
                mLunchStartTime = (Convert.ToDateTime(mLunchStartTime).AddDays(1).ToString("yyyy-MM-dd") & (" " & tmp(3)))
            End If

            'mLunchEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(timerow)("dateoffice")).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(22, 5)))
            mLunchEndTime = (Convert.ToDateTime(mTime.Tables(0).Rows(0).Item("dateoffice")).ToString("yyyy-MM-dd") & (" " & tmp(4)))
            If (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mLunchEndTime)) < 0) Then
                'mLunchEndTime = (Convert.ToDateTime(mLunchEndTime).AddDays(1).ToString("yyyy-MM-dd") + (" " + ftext.ToString.Substring(22, 5)))
                mLunchEndTime = (Convert.ToDateTime(mLunchEndTime).AddDays(1).ToString("yyyy-MM-dd") & (" " & tmp(4)))
            End If

            mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mLunchStartTime), Convert.ToDateTime(mLunchEndTime)))
            mLunchDur = mLDur
            mLDed = Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(30, 3)))
            'MsgBox(mShiftStartTime)
            mShiftTime = (Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mShiftEndTime))) - mLDed)
        End If

        'MsgBox(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString.Trim & "  lenght: " & mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString.Length)
        Dim tmpL As Double
        If mTime.Tables(0).Rows(0).Item("LeaveAmount").ToString.Length = 0 Then
            tmpL = 0
        Else
            tmpL = Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount").ToString)
        End If
        'If (Convert.ToDouble(mTime.Tables(0).Rows(timerow)("LeaveAmount").ToString) > 0) Then   'main
        If (tmpL > 0) Then    'nitin
            If (Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount")) = 0.25) Then
                mStatus = "Q_"
            ElseIf (Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount")) = 0.5) Then
                mStatus = "H_"
            ElseIf (Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount")) = 0.75) Then
                mStatus = "T_"
            Else
                mStatus = ""
            End If

            'mStatus = (mStatus.ToString.Trim + mTime.Tables(timerow).Rows(0)("LeaveCode").ToString) 'nitin
            mStatus = (mStatus.ToString.Trim + mTime.Tables(0).Rows(0).Item("LeaveCode").ToString)
            If SimulateIsDate.IsDate(mIn1) Then
                If ((mTime.Tables(0).Rows(0).Item("ShiftAttended").ToString.Trim <> "IGN") _
                            AndAlso ((mTime.Tables(0).Rows(0).Item("ShiftAttended").ToString <> "OFF") _
                            AndAlso (mTime.Tables(0).Rows(0).Item("ShiftAttended").ToString <> "FLX") _
                            AndAlso Not isHld)) Then
                    mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftStartTime)))
                    If (mEarly < 0) Then
                        mEarly = 0
                    End If
                End If

                If SimulateIsDate.IsDate(mout2) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
                    If SimulateIsDate.IsDate("in2") Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                        If (mLDur < mLDed) Then
                            mLDur = mLDed
                        End If

                        mHoursworked = (mHoursworked - mLDur)
                    Else
                        mHoursworked = (mHoursworked - mLDed)
                    End If

                End If

                Select Case (Convert.ToString(mTime.Tables(0).Rows(0).Item("LeaveType")))
                    Case "P"
                        mPresentValue = 1
                    Case "L"
                        mLeaveValue = (0 + Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount")))
                        mPresentValue = (1 - Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount")))
                    Case Else
                        mAbsentValue = (0 + Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount")))
                        mPresentValue = (1 - Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount")))
                End Select

            ElseIf (Not String.IsNullOrEmpty(mTime.Tables(0).Rows(0).Item("LeaveType1").ToString.Trim) _
                        AndAlso Not String.IsNullOrEmpty(mTime.Tables(0).Rows(0).Item("LeaveType2").ToString.Trim)) Then
                mStatus = (mTime.Tables(0).Rows(0).Item("FirstHalfLeavecode").ToString.Trim + mTime.Tables(0).Rows(0).Item("SecondHalfLeavecode").ToString.Trim)
                If (((mTime.Tables(0).Rows(0).Item("LeaveType1").ToString.Trim = "P") _
                            AndAlso (mTime.Tables(0).Rows(0).Item("LeaveType2").ToString.Trim = "A")) _
                            OrElse ((mTime.Tables(0).Rows(0).Item("LeaveType1").ToString.Trim = "A") _
                            AndAlso (mTime.Tables(0).Rows(0).Item("LeaveType2").ToString = "P"))) Then
                    mPresentValue = 0.5
                    mLeaveValue = 0
                    mAbsentValue = 0.5
                End If

                If (((mTime.Tables(0).Rows(0).Item("LeaveType1").ToString = "L") _
                            AndAlso (mTime.Tables(0).Rows(0).Item("LeaveType2").ToString = "P")) _
                            OrElse ((mTime.Tables(0).Rows(0).Item("LeaveType1").ToString = "P") _
                            AndAlso (mTime.Tables(0).Rows(0).Item("LeaveType2").ToString = "L"))) Then
                    mPresentValue = 0.5
                    mLeaveValue = 0.5
                    mAbsentValue = 0
                End If

                If (((mTime.Tables(0).Rows(0).Item("LeaveType1").ToString = "L") _
                            AndAlso (mTime.Tables(0).Rows(0).Item("LeaveType2").ToString = "A")) _
                            OrElse ((mTime.Tables(0).Rows(0).Item("LeaveType1").ToString = "A") _
                            AndAlso (mTime.Tables(0).Rows(0).Item("LeaveType2").ToString = "L"))) Then
                    mPresentValue = 0
                    mLeaveValue = 0.5
                    mAbsentValue = 0.5
                End If

                If ((mTime.Tables(0).Rows(0).Item("LeaveType1").ToString = "L") _
                            AndAlso (mTime.Tables(0).Rows(0).Item("LeaveType2").ToString = "L")) Then
                    mPresentValue = 0
                    mLeaveValue = 1
                    mAbsentValue = 0
                End If

                If ((mTime.Tables(0).Rows(0).Item("LeaveType1").ToString = "A") _
                            AndAlso (mTime.Tables(0).Rows(0).Item("LeaveType2").ToString = "A")) Then
                    mPresentValue = 0
                    mLeaveValue = 0
                    mAbsentValue = 1
                End If

                If ((mTime.Tables(0).Rows(0).Item("LeaveType1").ToString = "P") _
                            AndAlso (mTime.Tables(0).Rows(0).Item("LeaveType2").ToString = "P")) Then
                    mPresentValue = 1
                    mLeaveValue = 0
                    mAbsentValue = 0
                End If

            Else
                If (mTime.Tables(0).Rows(0).Item("LeaveType").ToString = "P") Then
                    mPresentValue = Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount").ToString)
                ElseIf (mTime.Tables(0).Rows(0).Item("LeaveType").ToString = "L") Then
                    mLeaveValue = Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount"))
                ElseIf (mTime.Tables(0).Rows(0).Item("LeaveType").ToString.Trim = "A") Then
                    mAbsentValue = Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount"))
                End If

                If (mTime.Tables(0).Rows(0).Item("ShiftAttended").ToString = "OFF") Then
                    mWO_Value = (1 - Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount")))
                ElseIf isHld Then
                    mHoliday_Value = (1 - Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount")))
                ElseIf (ftext.ToString.Substring(28, 1) = "H") Then
                    mWO_Value = 0.5
                ElseIf (mTime.Tables(0).Rows(0).Item("LEAVETYPE").ToString.Trim <> "A") Then
                    mAbsentValue = (1 - Convert.ToDouble(mTime.Tables(0).Rows(0).Item("LeaveAmount")))
                End If
            End If
            GoTo Lastl
        End If

        If ((mShiftAttended = "OFF") _
                    AndAlso isHld) Then
            mHoliday_Value = 1
            mStatus = "WOH"
            If SimulateIsDate.IsDate(mIn1) Then
                mStatus = "PWH"
                If ((EmpGrpArr(EmpGpId).g_isPresentOnWOPresent = "Y") _
                            OrElse (EmpGrpArr(EmpGpId).g_isPresentOnHLDPresent = "Y")) Then
                    mPresentValue = 1
                    mHoliday_Value = 0
                End If

                If SimulateIsDate.IsDate(mout2) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
                    If SimulateIsDate.IsDate(mIn2) Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                        mHoursworked = (mHoursworked - mLDur)
                    End If

                    If (rsEmp.Tables(0).Rows(0).Item("IsOS").ToString.Trim = "Y") Then
                        mOSDuration = mHoursworked
                    End If

                    If (rsEmp.Tables(0).Rows(0).Item("IsOT").ToString.Trim = "Y") Then
                        mOtDuration = mHoursworked
                    End If
                End If
            End If
            GoTo Lastl
        End If

        If (mShiftAttended = "OFF") Then
            mWO_Value = 1
            mStatus = "WO"
            If SimulateIsDate.IsDate(mIn1) Then
                mStatus = "POW"
                If (EmpGrpArr(EmpGpId).g_isPresentOnWOPresent = "Y") Then
                    mWO_Value = 0
                    mPresentValue = 1
                End If

                If SimulateIsDate.IsDate(mout2) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
                    If SimulateIsDate.IsDate(mIn2) Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                        mHoursworked = (mHoursworked - mLDur)
                    End If

                    If (rsEmp.Tables(0).Rows(0).Item("IsOS").ToString.Trim = "Y") Then
                        mOSDuration = mHoursworked
                    End If

                    If (rsEmp.Tables(0).Rows(0).Item("IsOT").ToString.Trim = "Y") Then
                        mOtDuration = (mHoursworked - EmpGrpArr(EmpGpId).g_Wo_OT)
                        If mOtDuration < 0 Then
                            mOtDuration = 0
                        End If
                    End If
                End If
            End If
            '''
            If EmpGrpArr(EmpGpId).ISCOMPOFF = "Y" Then
                mOtDuration = 0
            End If
            GoTo Lastl
        End If

        If alHld.Contains(Convert.ToDateTime(mTime.Tables(0).Rows(0).Item("DateOffice")).ToString("yyyy-MM-dd")) Then
            mHoliday_Value = 1
            mStatus = "HLD"
            If SimulateIsDate.IsDate(mIn1) Then
                mStatus = "POH"
                If (EmpGrpArr(EmpGpId).g_isPresentOnHLDPresent = "Y") Then
                    mHoliday_Value = 0
                    mPresentValue = 1
                End If
                If SimulateIsDate.IsDate(mout2) Then
                    mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
                    If SimulateIsDate.IsDate("in2") Then
                        mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                        mHoursworked = (mHoursworked - mLDur)
                    End If

                    If (rsEmp.Tables(0).Rows(0).Item("IsOS").ToString = "Y") Then
                        mOSDuration = mHoursworked
                    End If
                    If (rsEmp.Tables(0).Rows(0).Item("IsOT").ToString = "Y") Then
                        mOtDuration = (mHoursworked - EmpGrpArr(EmpGpId).g_Hld_OT)
                    End If
                End If
            End If
            '''
            '
            If EmpGrpArr(EmpGpId).ISCOMPOFF = "Y" Then
                mOtDuration = 0
            End If
            GoTo Lastl
        End If
        If (SimulateIsDate.IsDate(mIn1) _
                    AndAlso (rsEmp.Tables(0).Rows(0).Item("InOnly").ToString.Trim <> "N")) Then
            If Not ((rsEmp.Tables(0).Rows(0).Item("InOnly").ToString = "O") AndAlso SimulateIsDate.IsDate(mout2)) Then
                mout2 = mShiftEndTime
            End If
        End If
        If ((rsEmp.Tables(0).Rows(0).Item("IsPunchAll").ToString.Trim = "N") AndAlso Not SimulateIsDate.IsDate(mIn1)) Then
            mIn1 = mShiftStartTime
            mout2 = mShiftEndTime
        End If
        If ((rsEmp.Tables(0).Rows(0).Item("IsPunchAll").ToString = "N") AndAlso (SimulateIsDate.IsDate(mIn1) AndAlso Not SimulateIsDate.IsDate(mout2))) Then
            If (Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftEndTime))) > 0) Then
                mout2 = mShiftEndTime
            Else
                mout2 = mIn1
                mIn1 = mShiftStartTime
            End If
        End If

        If ((SimulateIsDate.IsDate(mIn1) _
                    And Not SimulateIsDate.IsDate(mout2)) _
                    Or (Not SimulateIsDate.IsDate(mIn1) _
                    And SimulateIsDate.IsDate(mout2))) Then
            mStatus = "MIS"
            mPresentValue = 1
            If (ftext.ToString.Trim.Substring(28, 1) = "H") Then
                If (mLeaveValue < 0.5) Then
                    mPresentValue = (0.5 - mLeaveValue)
                End If

                mWO_Value = 0.5
            End If

            If (mShiftAttended <> "IGN") And (mShiftAttended <> "FLX") Then
                If SimulateIsDate.IsDate(mIn1) Then
                    mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftStartTime)))
                    If (mEarly > 0) Then
                        mEarlyArrival = mEarly
                    ElseIf (Math.Abs(mEarly) > Convert.ToInt32(rsEmp.Tables(0).Rows(0).Item("PERMISLATEARRIVAL"))) Then
                        mLateArrival = Math.Abs(mEarly)
                    End If
                End If
            End If
            GoTo Lastl
        End If
        If ((ftext.ToString.Trim.Substring(28, 1) = "H") AndAlso (mLeaveValue = 0)) Then
            mWO_Value = 0.5
            If SimulateIsDate.IsDate(mIn1) Then
                mPresentValue = 0.5
            Else
                mAbsentValue = 0.5
            End If
        End If
        If ((ftext.ToString.Trim.Substring(28, 1) = "H") AndAlso (mLeaveValue > 0)) Then
            mWO_Value = 0.5
            If (SimulateIsDate.IsDate(mIn1) _
                        And (mLeaveValue < 0.5)) Then
                mPresentValue = (0.5 - mLeaveValue)
            ElseIf (mLeaveValue < 0.5) Then
                mAbsentValue = (0.5 - mLeaveValue)
            End If
        End If

        If (SimulateIsDate.IsDate(mout2) And SimulateIsDate.IsDate(mIn1)) Then
            mHoursworked = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mout2)))
            If SimulateIsDate.IsDate(mIn2) Then
                mLDur = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mIn2)))
                If (mLDur < mLDed) Then
                    mLDur = mLDed
                End If
                mHoursworked = (mHoursworked - mLDur)
            Else
                mHoursworked = (mHoursworked - mLDed)
                'mHoursworked = (mHoursworked - mLDur)
            End If
            If (mShiftAttended <> "IGN" And mShiftAttended <> "FLX") Then
                'nitin start
                Dim tmp As Integer
                If servername = "Access" Then
                    tmp = Convert.ToInt32(rsEmp.Tables(0).Rows(0).Item("Time1"))
                Else
                    tmp = Convert.ToInt32(rsEmp.Tables(0).Rows(0).Item("Time"))
                End If
                'nitin end
                'If (mHoursworked < Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))) Then
                If (mHoursworked < tmp) Then
                    mHoursworked = 0
                    mAbsentValue = 1
                    GoTo Lastl
                End If

                If (SimulateIsDate.IsDate(mIn2) And SimulateIsDate.IsDate(mLunchStartTime)) Then
                    mEarly1 = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mout1), Convert.ToDateTime(mLunchStartTime)))
                    If (mEarly1 > 0) Then
                        mLunchEarlyDeparture = mEarly1
                    End If

                    mLate1 = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mLunchEndTime), Convert.ToDateTime(mIn2)))
                    If (mLate1 > 0) Then
                        mLunchLateArrival = mLate1
                    End If

                    If (mLDur > mLunchDur) Then
                        mExcLunchHours = (mLDur - mLunchDur)
                    End If
                End If

                If (rsEmp.Tables(0).Rows(0).Item("IsOS").ToString.Trim = "Y") Then
                    'mOSDuration = ((mStatus == "HLF") ? mHoursworked - 240 : mHoursworked - mShiftTime);
                    If (mStatus = "HLF") Then
                        mOSDuration = mHoursworked - 240
                    Else
                        mOSDuration = mHoursworked - mShiftTime
                    End If
                    '(mStatus = "HLF")
                    '(mHoursworked - mShiftTime)
                    If (mOSDuration < 0) Then
                        mOSDuration = 0
                    End If
                End If

                mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftStartTime)))
                If (mEarly > 0) Then
                    mEarlyArrival = mEarly
                ElseIf (Math.Abs(mEarly) > Convert.ToInt32(rsEmp.Tables(0).Rows(0).Item("PERMISLATEARRIVAL"))) Then
                    mLateArrival = Math.Abs(mEarly)
                End If

                mLate = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2)))
                If ((mLate < 0) _
                            And (Math.Abs(mLate) > Convert.ToInt32(rsEmp.Tables(0).Rows(0).Item("PERMISEARLYDEPRT")))) Then
                    mearlydeparture = Math.Abs(mLate)
                End If

                mStatus = "P"
                If (ftext.ToString.Trim.Substring(28, 1) = "H") Then
                    If (mLeaveValue < 0.5) Then
                        mPresentValue = (0.5 - mLeaveValue)
                    End If

                    mAbsentValue = 0
                Else
                    mPresentValue = 1
                End If

                AbsHours = (Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftStartTime), Convert.ToDateTime(mShiftEndTime))) - mHoursworked)
                If Not String.IsNullOrEmpty(mLunchStartTime) Then
                    AbsHours = (AbsHours - Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mLunchStartTime), Convert.ToDateTime(mLunchEndTime))))
                End If

                If (AbsHours > 0) Then
                    'nitin start
                    Dim tmp1, tmpsort As Integer
                    If servername = "Access" Then
                        tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(0).Item("Time1"))
                        tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(0).Item("Short1"))
                    Else
                        tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(0).Item("Time"))
                        tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(0).Item("Short"))
                    End If
                    'nitin end
                    'If ((rsEmp.Tables(0).Rows(emprow)("IsHalfDay").ToString = "Y") _
                    '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
                    '            AndAlso (mHoursworked >= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))))) Then   'nitin
                    If ((rsEmp.Tables(0).Rows(0).Item("IsHalfDay").ToString = "Y") _
                           AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(0).Item("Half").ToString)) _
                           AndAlso (mHoursworked >= tmp1))) Then    'nitin
                        mStatus = "HLF"
                        mPresentValue = 0.5
                        mAbsentValue = 0.5
                        mLateArrival = 0
                        mearlydeparture = 0
                        mTotalLossHrs = 0
                        'ElseIf ((rsEmp.Tables(0).Rows(emprow)("isHalfday").ToString = "Y") _
                        '            AndAlso ((rsEmp.Tables(0).Rows(emprow)("isShort").ToString = "Y") _
                        '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))) _
                        '            AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))))) Then  'nitin
                    ElseIf ((rsEmp.Tables(0).Rows(0).Item("isHalfday").ToString = "Y") _
                          AndAlso ((rsEmp.Tables(0).Rows(0).Item("isShort").ToString = "Y") _
                          AndAlso ((mHoursworked <= tmpsort) _
                          AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(0).Item("Half")))))) Then   'nitin
                        mStatus = "SRT"
                        mPresentValue = 1
                    End If
                End If
                If (rsEmp.Tables(0).Rows(0).Item("IsOT").ToString = "Y") Then
                    'Calculate OT
                    Select Case (EmpGrpArr(EmpGpId).g_OTFormulae)
                        Case 1
                            'Out - End
                            mOt = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2)))
                        Case 2
                            ' Hours Worked - Shift Hours
                            mOt = (mHoursworked - mShiftTime)
                        Case 3
                            ' Early Arrival + Late Departure
                            mEarly = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mIn1), Convert.ToDateTime(mShiftStartTime)))
                            If (mEarly <= EmpGrpArr(EmpGpId).g_OTEarly) Then
                                mEarly = 0
                            End If
                            mLate = Convert.ToInt32(SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, Convert.ToDateTime(mShiftEndTime), Convert.ToDateTime(mout2)))
                            If (mLate <= EmpGrpArr(EmpGpId).g_OTLate) Then
                                mLate = 0
                            End If

                            If (Math.Abs(mLate) <= EmpGrpArr(EmpGpId).g_OTEnd) Then
                                mLate = 0
                            End If

                            mOt = (mEarly + (mLate - mExcLunchHours))
                    End Select

                    mOtStartAfter = Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(33, 3)))
                    mOtDeductAfter = Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(37, 4)))
                    mOtDeductHrs = Convert.ToInt32(SimulateVal.Val(ftext.ToString.Substring(42, 3)))
                    If (mOt < mOtStartAfter) Then
                        mOt = 0
                    ElseIf ((mOtDeductHrs = 0) _
                                AndAlso (mOt > mOtDeductAfter)) Then
                        mOt = mOtDeductAfter
                    ElseIf (mOt _
                                >= (mOtDeductAfter + mOtDeductHrs)) Then
                        mOt = (mOt - mOtDeductHrs)
                    End If

                    If ((g_OTMinus = "N") _
                                AndAlso (mOt < 0)) Then
                        mOt = 0
                    End If
                    mOtDuration = mOt
                End If
                GoTo Lastl
            Else
                mPresentValue = 1
                mStatus = "P"
            End If

        End If

        If ((mLeaveValue = 0) _
                    AndAlso ((mPresentValue = 0) _
                    AndAlso ((mHoliday_Value = 0) _
                    AndAlso (mWO_Value = 0)))) Then
            mAbsentValue = 1
        End If

        If mShiftAttended = "FLX" Then
            'nitin start
            Dim tmp1, tmpsort As Integer
            If servername = "Access" Then
                tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(0)("Time1"))
                tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(0)("Short1"))
            Else
                tmp1 = Convert.ToInt32(rsEmp.Tables(0).Rows(0)("Time"))
                tmpsort = Convert.ToInt32(rsEmp.Tables(0).Rows(0)("Short"))
            End If
            'nitin end
            'If ((rsEmp.Tables(0).Rows(emprow)("IsHalfDay").ToString = "Y") _
            '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half").ToString)) _
            '            AndAlso (mHoursworked >= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Time"))))) Then   'nitin
            If mHoursworked < Convert.ToInt32(tmp1) Then
                mStatus = "A"
                mPresentValue = 0
                mAbsentValue = 1
                mLateArrival = 0
                mearlydeparture = 0
                mTotalLossHrs = 0
            ElseIf (((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(0)("Half").ToString)) _
                   AndAlso (mHoursworked >= tmp1))) Then    'nitin
                mStatus = "HLF"
                mPresentValue = 0.5
                mAbsentValue = 0.5
                mLateArrival = 0
                mearlydeparture = 0
                mTotalLossHrs = 0
                'ElseIf ((rsEmp.Tables(0).Rows(emprow)("isHalfday").ToString = "Y") _
                '            AndAlso ((rsEmp.Tables(0).Rows(emprow)("isShort").ToString = "Y") _
                '            AndAlso ((mHoursworked <= Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Short"))) _
                '            AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(emprow)("Half")))))) Then  'nitin
            ElseIf ((mHoursworked < tmpsort) _
                  AndAlso (mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(0)("Half")))) Then   'nitin
                mStatus = "SRT"
                mPresentValue = 1
            ElseIf mHoursworked > Convert.ToInt32(rsEmp.Tables(0).Rows(0)("Half").ToString) And _
                    mHoursworked > tmp1 And mHoursworked > tmpsort Then
                mStatus = "P"
                mPresentValue = 1
                mAbsentValue = 0
                mLateArrival = 0
                mearlydeparture = 0
                mTotalLossHrs = 0
            End If
        End If
Lastl:
        If ((mStatus = "HLF") _
                    AndAlso (rsEmp.Tables(0).Rows(0).Item("IsOT").ToString = "Y")) Then
            mOtDuration = (mHoursworked - 240)
            mOtDuration = 0
        End If

        If (EmpGrpArr(EmpGpId).g_OtRound = "Y") Then
            OTinHrs = Convert.ToDouble(HM.minutetohour1(mOtDuration.ToString.Trim))
            OTfactor = (OTinHrs - CType(Math.Floor(OTinHrs), Integer))
            If (OTfactor < 0.15) Then
                OTinHrs = CType(Math.Floor(OTinHrs), Integer)
            ElseIf ((OTfactor >= 0.15) _
                        AndAlso (OTfactor < 0.45)) Then
                OTinHrs = (CType(Math.Floor(OTinHrs), Integer) + 0.3)
            ElseIf (OTfactor >= 0.45) Then
                OTinHrs = (CType(Math.Floor(OTinHrs), Integer) + 1)
            End If

            mOtDuration = HM.hour(OTinHrs.ToString.Trim)
        End If

        If (rsEmp.Tables(0).Rows(0).Item("istimelossallowed").ToString = "Y") Then
            mTotalLossHrs = (mLateArrival _
                        + (mearlydeparture _
                        + (mLunchLateArrival + mLunchEarlyDeparture)))
        Else
            mLateArrival = 0
            mearlydeparture = 0
            mTotalLossHrs = 0
            mLunchLateArrival = 0
            mLunchEarlyDeparture = 0
        End If

        If (SimulateVal.Val(rsEmp.Tables(0).Rows(0).Item("OTRate").ToString.Trim) = 0) Then
            motamount = 0
        Else
            motamount = (mOtDuration _
                        * (Convert.ToInt32(rsEmp.Tables(0).Rows(0).Item("OtRate")) / 60))
        End If

        sSql = "Update tblTimeRegister Set status ='" & mStatus & "', Absentvalue='" & mAbsentValue & "', presentvalue='" & mPresentValue & "', leavevalue='" & mLeaveValue & "', WO_value='" & mWO_Value & "', holiday_value='" & mHoliday_Value & "'" & _
                                               " Where Paycode='" & mTime.Tables(0).Rows(0)("Paycode").ToString.Trim & "' and DateOffice ='" & Convert.ToDateTime(mTime.Tables(0).Rows(0)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
        If Common.servername = "Access" Then
            sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If

        Return mStatus
    End Function

    'All 3rd party integrations
    Public Sub Convertdat_Cloud()
        Dim mFile, mLine, mPath
        Dim mDate As DateTime
        Dim mCnt As Integer
        Dim Rs As DataSet = New DataSet 'ADODB.Recordset
        Dim rsTmp As DataSet = New DataSet '
        Dim RsDev As DataSet = New DataSet '
        Dim aFileNum As Integer
        Dim sFileName As String
        Dim mCARDNO As String
        Dim mDevId As String
        Dim mDate1 As DateTime
        Dim Temperature As String
        Dim rsCardRepl As DataSet = New DataSet '
        Dim bCardRepl As Boolean
        Dim mrescd As String
        Dim TempCardNo As String
        ' Dim i As Integer
        Dim mReaderNo As String
        Dim inout As String
        Dim mReaders As String
        Dim mStatus As String
        Dim iscalled As Boolean

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand
        Try
            Dim connectionString_TWCloud As String = Common.ConnectionString.Replace(Common.DB, Common.TWCloudDB)
            Dim TWCloud_con As SqlConnection = New SqlConnection(connectionString_TWCloud)

            'If iscalled = False Then
            sSql = "SELECT PUNCH_DATE_TWCloud FROM Min_max_Date "
            rsTmp = New DataSet
            If servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1)
                adapA.Fill(rsTmp)
            Else
                adap = New SqlDataAdapter(sSql, con)
                adap.Fill(rsTmp)
            End If
            'rsTmp = Cn.Execute(sSql)
            If rsTmp.Tables(0).Rows.Count > 0 Then
                If rsTmp.Tables(0).Rows(0).Item(0).ToString.Trim <> "" Then
                    mDate = Convert.ToDateTime(rsTmp.Tables(0).Rows(0).Item(0).ToString.Trim) '.ToString("yyyy-MM-dd HH:mm:ss")
                Else
                    mDate = Now().ToString("yyyy-MM-01 00:00:00")
                End If
            Else
                mDate = Now().ToString("yyyy-MM-01 00:00:00")
            End If

            sSql = "select * from tbl_realtime_glog where io_time>='" & mDate.ToString("yyyy-MM-dd HH:mm:ss") & "' order by [user_id],io_time"
            Rs = New DataSet
            If servername = "Access" Then
                'sSql = "select * from tbl_realtime_glog where Format(io_time, 'yyyy-MM-dd HH:mm:ss')>='" & mDate.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                'adapA = New OleDbDataAdapter(sSql, con1)
                'adapA.Fill(Rs)
            Else
                adap = New SqlDataAdapter(sSql, TWCloud_con)
                adap.Fill(Rs)
            End If
            'Rs = Cn_C.Execute(sSql)
            If Rs.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To Rs.Tables(0).Rows.Count - 1 ' Do While Not Rs.EOF
                    XtraMasterTest.LabelControlStatus.Text = "Inserting data from TW Cloud " & Rs.Tables(0).Rows(i).Item("user_id").ToString.Trim & "  DateTime: " & Rs.Tables(0).Rows(i).Item("io_time").ToString.Trim
                    Application.DoEvents()
                    mStatus = ""
                    sSql = "select * from tblmachine where Location= '" & Rs.Tables(0).Rows(i).Item("device_id").ToString.Trim & "'"
                    RsDev = New DataSet
                    If servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, con1)
                        adapA.Fill(RsDev)
                    Else
                        adap = New SqlDataAdapter(sSql, con)
                        adap.Fill(RsDev)
                    End If
                    'RsDev = Cn.Execute(sSql)
                    If RsDev.Tables(0).Rows.Count > 0 Then
                        mDevId = Convert.ToDouble(RsDev.Tables(0).Rows(0).Item("ID_NO").ToString.Trim).ToString("00")
                    Else
                        mDevId = "00"
                    End If

                    mCARDNO = Convert.ToDouble(Rs.Tables(0).Rows(i).Item("user_id").ToString.Trim).ToString("000000000000")
                    mDate1 = Convert.ToDateTime(Rs.Tables(0).Rows(i).Item("io_time").ToString.Trim) '.ToString("yyyy-MM-dd HH:mm:ss")
                    Temperature = Rs.Tables(0).Rows(i).Item("io_mode").ToString.Trim
                    '  Dim Serial As String = Rs.Tables(0).Rows(i).Item("io_mode").ToString.Trim

#Region "IDTECH"  'Manipal
                    Try 'IDTECH
                        '' Json
                        'Try
                        '    Dim CardType As String = "", CardSRNNO As String = "", PunchType As String = "", Category As String = "", RefNo As String = "", OrgID As String = "", PresentCardNo As String = ""
                        '    Dim MachineID As Int64 = 1

                        '    Dim FinalData As JObject = New JObject
                        '    Dim httpWebRequest = CType(WebRequest.Create("https://in.nexuba.com/S/E2Pro_ASL_Inbound.php"), HttpWebRequest)
                        '    httpWebRequest.ContentType = "application/json"
                        '    httpWebRequest.Method = "POST"
                        '    ServicePointManager.Expect100Continue = True
                        '    ServicePointManager.SecurityProtocol = CType(3072, SecurityProtocolType)
                        '    Dim jsons As String
                        '    Dim PunchDate As DateTime = Convert.ToDateTime(mDate1.ToString())
                        '    Dim jStrong = "{""type"": 1,""data"":{""org_id"":""1046"",""badge_id"":""" & mCARDNO & """,""device_id"":""" & Rs.Tables(0).Rows(i).Item("device_id").ToString.Trim & """,""swipetime"":""" & PunchDate.ToString("yyyy-MM-dd HH:mm:ss") & """,""security token"":""xyz13aso""}}"
                        '    jsons = JsonConvert.ToString(jStrong)
                        '    Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
                        '        ''EnrollNumber = EnrollNumber.Trim
                        '        'FinalData.Add("type", "1")
                        '        'FinalData.Add("org_id", "1046")
                        '        'FinalData.Add("badge_id", EnrollNumber.Trim)
                        '        'FinalData.Add("device_id", deviceSerialNumber)
                        '        'FinalData.Add("swipetime", PunchDate.ToString("yyyy-MM-dd HH:mm:ss"))
                        '        'FinalData.Add("security token", "xyz13aso")
                        '        ' jsons = FinalData.ToString(Formatting.None)
                        '        streamWriter.Write(jStrong)
                        '        streamWriter.Flush()
                        '        streamWriter.Close()
                        '    End Using
                        '    Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
                        '    Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                        '        Dim result = streamReader.ReadToEnd()
                        '        Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
                        '        Dim sw As StreamWriter = New StreamWriter(fs)
                        '        ' Dim sw As StreamWriter = New StreamWriter(fs)
                        '        sw.BaseStream.Seek(0, SeekOrigin.End)
                        '        sw.WriteLine(vbCrLf & jStrong & ", " & result & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                        '        sw.Flush()
                        '        sw.Close()
                        '    End Using
                        'Catch ex As Exception
                        '    'TraceService(ex.Message, "JSON")
                        '    Dim fs As FileStream = New FileStream("JsonResponseError.txt", FileMode.OpenOrCreate, FileAccess.Write)
                        '    Dim sw As StreamWriter = New StreamWriter(fs)
                        '    ' Dim sw As StreamWriter = New StreamWriter(fs)
                        '    sw.BaseStream.Seek(0, SeekOrigin.End)
                        '    sw.WriteLine(vbCrLf & ex.Message & ", JSON::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                        '    sw.Flush()
                        '    sw.Close()
                        'End Try
                        ''end jSON
                    Catch ex As Exception

                    End Try
#End Region


                    'If Trim(UCase(mStatus)) = "ENTRY" Then
                    '    mStatus = "I"
                    'ElseIf Trim(UCase(mStatus)) = "EXIT" Then
                    '    mStatus = "O"
                    'End If

                    'sSql = "select paycode from tblEmployee where PRESENTCARDNO='" & mCARDNO & "' and active='Y' and dateofjoin <='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & mCARDNO & "' and TblEmployee.dateofjoin <='" & mDate1.ToString("yyyy-MM-dd") & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                    Dim ds As DataSet = New DataSet
                    If servername = "Access" Then
                        ''sSql = "select paycode from tblEmployee where PRESENTCARDNO='" & mCARDNO & "'and active='Y' and FORMAT(dateofjoin,'yyyy-MM-dd HH:mm:ss') <='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        'sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.Id  from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & mCARDNO & "' and FORMAT(TblEmployee.dateofjoin,'yyyy-MM-dd') <='" & mDate1.ToString("yyyy-MM-dd") & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                        'adapA = New OleDbDataAdapter(sSql, con1)
                        'adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSql, con)
                        adap.Fill(ds)
                    End If

                    'sSql = "Insert into RAWDATA(Cardno,OfficePunch,id_NO,REASONCODE,Inout,Error_Code,DOOR_TIME,PROCESS) VALUES ('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','" & mDevId & "','','','00','00','N' )"

                    If ds.Tables(0).Rows.Count > 0 Then
                        Try
                            sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[Temperature ]) values('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & mDevId & "','','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "','" & Temperature & "' ) "
                            Dim sSql1 As String = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[Temperature]) values('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & mDevId & "','','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "','" & Temperature & "' ) "
                            If servername = "Access" Then
                                'If con1.State <> ConnectionState.Open Then
                                '    con1.Open()
                                'End If
                                'cmd1 = New OleDbCommand(sSql, con1)
                                'cmd1.ExecuteNonQuery()
                                'Try
                                '    cmd1 = New OleDbCommand(sSql1, con1)
                                '    cmd1.ExecuteNonQuery()
                                'Catch ex As Exception

                                'End Try
                                'If con1.State <> ConnectionState.Closed Then
                                '    con1.Close()
                                'End If
                        Else
                            If con.State <> ConnectionState.Open Then
                                con.Open()
                            End If
                            cmd = New SqlCommand(sSql, con)
                            cmd.ExecuteNonQuery()
                            Try
                                cmd = New SqlCommand(sSql1, con)
                                cmd.ExecuteNonQuery()
                                Catch ex As Exception
                                    'MsgBox(ex.Message & " 11777")
                                End Try
                            If con.State <> ConnectionState.Closed Then
                                con.Close()
                            End If
                        End If
                            'process
                            'Cn.Execute (sSql), i



                            If ds.Tables(0).Rows.Count > 0 Then
                            Remove_Duplicate_Punches(mDate1, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                            If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                Process_AllRTC(mDate1.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                            Else
                                Process_AllnonRTC(mDate1, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                If EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                                    Process_AllnonRTCMulti(mDate1, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                End If
                            End If
                            End If
                        Catch ex As Exception
                            'MsgBox(ex.Message & " 11799")
                        End Try
                    End If                    
                Next 'Loop
        If rsTmp.Tables(0).Rows.Count = 0 Then
            sSql = "Insert Into Min_max_Date (PUNCH_DATE_TWCloud) values('" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        Else
            sSql = "Update Min_max_Date set PUNCH_DATE_TWCloud ='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
        End If
        If servername = "Access" Then
            If con1.State <> ConnectionState.Open Then
                con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, con1)
            cmd1.ExecuteNonQuery()
            If con1.State <> ConnectionState.Closed Then
                con1.Close()
            End If
        Else
            If con.State <> ConnectionState.Open Then
                con.Open()
            End If
            cmd = New SqlCommand(sSql, con)
            cmd.ExecuteNonQuery()
            If con.State <> ConnectionState.Closed Then
                con.Close()
            End If
        End If
            End If
        Catch ex As Exception
            'MsgBox(ex.Message & " 11828")
        End Try
    End Sub
    Public Delegate Sub invokeDelegate()
    Public Sub Convertdat_WDMS()
        Dim mFile, mLine, mPath
        Dim mDate As DateTime
        Dim mCnt As Integer
        Dim Rs As DataSet = New DataSet 'ADODB.Recordset
        Dim rsTmp As DataSet = New DataSet '
        Dim RsDev As DataSet = New DataSet '
        Dim aFileNum As Integer
        Dim sFileName As String
        Dim mCARDNO As String
        Dim mDevId As String
        Dim mDate1 As DateTime
        Dim rsCardRepl As DataSet = New DataSet '
        Dim bCardRepl As Boolean
        Dim mrescd As String
        Dim TempCardNo As String
        ' Dim i As Integer
        Dim mReaderNo As String
        Dim inout As String
        Dim mReaders As String
        Dim mStatus As String
        Dim iscalled As Boolean

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand
        Dim IsWDMS As Boolean = True
        Try
            'Dim connectionString_WDMS = "Data Source=" & Common.servername & ";Initial Catalog=wdms6;User Id=sa;Password=Passw0rd!;"
            Dim connectionString_WDMS As String = Common.ConnectionString.Replace(Common.DB, Common.WDMSDBName)
            Dim WDMS_con As SqlConnection = New SqlConnection(connectionString_WDMS)
            sSql = "SELECT PUNCH_DATE_WDMS FROM Min_max_Date "
            rsTmp = New DataSet
            If servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1)
                adapA.Fill(rsTmp)
            Else
                adap = New SqlDataAdapter(sSql, con)
                adap.Fill(rsTmp)
            End If
            'rsTmp = Cn.Execute(sSql)
            If rsTmp.Tables(0).Rows.Count > 0 Then
                If rsTmp.Tables(0).Rows(0).Item(0).ToString.Trim <> "" Then
                    mDate = Convert.ToDateTime(rsTmp.Tables(0).Rows(0).Item(0).ToString.Trim) '.ToString("yyyy-MM-dd HH:mm:ss")
                Else
                    mDate = Now().ToString("yyyy-MM-01 00:00:00")
                End If
            Else
                mDate = Now().ToString("yyyy-MM-01 00:00:00")
            End If


            sSql = "Select * From sysobjects Where name ='iclock_transaction'"
            adap = New SqlDataAdapter(sSql, WDMS_con)
            adap.Fill(Rs)

            If Rs.Tables(0).Rows.Count = 0 Then
                IsWDMS = True
                sSql = "select * from transaction_ where [time]>='" & mDate.ToString("yyyy-MM-01 00:00:00") & "' order by Employee_ID, [time]"
            Else
                IsWDMS = False
                sSql = "select * from iclock_transaction where punch_time>='" & mDate.ToString("yyyy-MM-01 00:00:00") & "' order by emp_code, punch_time"
            End If


            Rs = New DataSet
            If servername = "Access" Then
                Return
                'sSql = "select * from tbl_realtime_glog where Format(io_time, 'yyyy-MM-dd HH:mm:ss')>='" & mDate.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                'adapA = New OleDbDataAdapter(sSql, con1)
                'adapA.Fill(Rs)
            Else
                Try
                    adap = New SqlDataAdapter(sSql, WDMS_con)
                    adap.Fill(Rs)
                Catch ex As Exception

                End Try

            End If
            'MsgBox("Count " & Rs.Tables(0).Rows.Count)

            'Rs = Cn_C.Execute(sSql)
            If Rs.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To Rs.Tables(0).Rows.Count - 1 ' Do While Not Rs.EOF
                    If IsWDMS Then
                        XtraMasterTest.LabelControlStatus.Text = "Inserting data from WDMS " & Rs.Tables(0).Rows(i).Item("Employee_ID").ToString.Trim & "  DateTime: " & Rs.Tables(0).Rows(i).Item("Time").ToString.Trim
                        Application.DoEvents()
                    Else
                        XtraMasterTest.LabelControlStatus.Text = "Inserting data from WDMS " & Rs.Tables(0).Rows(i).Item("emp_code").ToString.Trim & "  DateTime: " & Rs.Tables(0).Rows(i).Item("punch_time").ToString.Trim
                        Application.DoEvents()
                    End If
                    'XtraMasterTest.LabelControlStatus.Text = "Inserting data from WDMS " & Rs.Tables(0).Rows(i).Item("Employee_ID").ToString.Trim & "  DateTime: " & Rs.Tables(0).Rows(i).Item("Time").ToString.Trim

                    mStatus = ""
                    'sSql = "select * from tblmachine where Location= '" & Rs.Tables(0).Rows(i).Item("device_id").ToString.Trim & "'"
                    If IsWDMS Then


                        sSql = "select PIN as 'CardNo' from employee where ID='" & Rs.Tables(0).Rows(i).Item("Employee_ID").ToString.Trim & "'"
                        RsDev = New DataSet

                        If servername = "Access" Then
                            adapA = New OleDbDataAdapter(sSql, con1)
                            adapA.Fill(RsDev)
                        Else
                            adap = New SqlDataAdapter(sSql, WDMS_con)
                            adap.Fill(RsDev)
                        End If
                        'If RsZK.RecordCount > 0 Then
                        '    mCARDNO = Trim(Format(RsZK("CardNo"), "00000000"))
                        'End If
                        'PunchDate = Format(Rs!Time, "dd/mm/yyyy HH:MM:SS")
                        If Rs.Tables(0).Rows(i).Item("iclock_id").ToString.Trim = "1" Then
                            inout = "I"
                        Else
                            inout = "O"
                        End If

                        'RsDev = Cn.Execute(sSql)
                        If RsDev.Tables(0).Rows.Count > 0 Then
                            'mDevId = Convert.ToDouble(RsDev.Tables(0).Rows(0).Item("ID_NO").ToString.Trim).ToString("00")
                            mCARDNO = RsDev.Tables(0).Rows(0).Item("CardNo").ToString.Trim
                            If IsNumeric(mCARDNO) Then
                                mCARDNO = Convert.ToDouble(mCARDNO).ToString("000000000000")
                            End If
                            'Else
                            mDevId = "01"
                            mDate1 = Convert.ToDateTime(Rs.Tables(0).Rows(i).Item("Time").ToString.Trim)
                        Else
                            Continue For
                        End If
                    Else
                        mCARDNO = RsDev.Tables(0).Rows(0).Item("emp_code").ToString.Trim
                        mDate1 = Convert.ToDateTime(Rs.Tables(0).Rows(i).Item("punch_time").ToString.Trim)
                        mDevId = "01"

                    End If

                    'mCARDNO = Convert.ToDouble(Rs.Tables(0).Rows(i).Item("Employee_ID").ToString.Trim).ToString("000000000000")
                    '.ToString("yyyy-MM-dd HH:mm:ss")
                    'sSql = "select paycode from tblEmployee where PRESENTCARDNO='" & mCARDNO & "' and active='Y' and dateofjoin <='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & mCARDNO & "' and TblEmployee.dateofjoin <='" & mDate1.ToString("yyyy-MM-dd") & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                    Dim ds As DataSet = New DataSet
                    If servername = "Access" Then
                        'sSql = "select paycode from tblEmployee where PRESENTCARDNO='" & mCARDNO & "'and active='Y' and FORMAT(dateofjoin,'yyyy-MM-dd HH:mm:ss') <='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.Id  from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & mCARDNO & "' and FORMAT(TblEmployee.dateofjoin,'yyyy-MM-dd') <='" & mDate1.ToString("yyyy-MM-dd") & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                        adapA = New OleDbDataAdapter(sSql, con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSql, con)
                        adap.Fill(ds)
                    End If

                    'sSql = "Insert into RAWDATA(Cardno,OfficePunch,id_NO,REASONCODE,Inout,Error_Code,DOOR_TIME,PROCESS) VALUES ('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','" & mDevId & "','','','00','00','N' )"
                    Try
                        If ds.Tables(0).Rows.Count > 0 Then

                            sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & mDevId & "','" & inout & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) "
                            Dim sSql1 As String = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & mDevId & "','" & inout & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) "
                            If servername = "Access" Then
                                If con1.State <> ConnectionState.Open Then
                                    con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, con1)
                                cmd1.ExecuteNonQuery()
                                Try
                                    cmd1 = New OleDbCommand(sSql1, con1)
                                    cmd1.ExecuteNonQuery()
                                Catch ex As Exception

                                End Try
                                If con1.State <> ConnectionState.Closed Then
                                    con1.Close()
                                End If
                            Else
                                If con.State <> ConnectionState.Open Then
                                    con.Open()
                                End If
                                cmd = New SqlCommand(sSql, con)
                                cmd.ExecuteNonQuery()
                                Try
                                    cmd = New SqlCommand(sSql1, con)
                                    cmd.ExecuteNonQuery()
                                Catch ex As Exception
                                End Try
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            End If
                            'process
                            'Cn.Execute (sSql), i



                            If ds.Tables(0).Rows.Count > 0 Then
                                Remove_Duplicate_Punches(mDate1, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                    Process_AllRTC(mDate1.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                Else
                                    Process_AllnonRTC(mDate1, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    If EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                                        Process_AllnonRTCMulti(mDate1, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    End If
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        'MsgBox(ex.Message & " 12155 ")
                    End Try
                Next 'Loop

                If rsTmp.Tables(0).Rows.Count = 0 Then
                    sSql = "Insert Into Min_max_Date (PUNCH_DATE_WDMS) values('" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "')"
                Else
                    sSql = "Update Min_max_Date set PUNCH_DATE_WDMS ='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                End If
                If servername = "Access" Then
                    If con1.State <> ConnectionState.Open Then
                        con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, con1)
                    cmd1.ExecuteNonQuery()
                    If con1.State <> ConnectionState.Closed Then
                        con1.Close()
                    End If
                Else
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End If

            End If
        Catch ex As Exception
            'MsgBox(ex.Message & " 12186 ")
        End Try
    End Sub
    Public Sub Convertdat_iDMS()
        Dim mFile, mLine, mPath
        'Dim mDate As String
        Dim mCnt As Integer
        Dim Rs As DataSet = New DataSet 'ADODB.Recordset
        'Dim rsTmp As DataSet = New DataSet '
        Dim RsDev As DataSet = New DataSet '
        Dim aFileNum As Integer
        Dim sFileName As String
        Dim mCARDNO As String
        Dim mDevId As String
        Dim mDate1 As DateTime
        Dim rsCardRepl As DataSet = New DataSet '
        Dim bCardRepl As Boolean
        Dim mrescd As String
        Dim TempCardNo As String
        ' Dim i As Integer
        Dim mReaderNo As String
        Dim inout As String
        Dim mReaders As String
        Dim mStatus As String
        Dim iscalled As Boolean

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand
        Try

            Dim connectionString_iDMS = Common.ConnectionString.Replace(Common.DB, Common.iDMSDB) ' "Data Source=" & Common.servername & ";Initial Catalog=TimeWatch;User Id=sa;Password=Passw0rd!;"
            'Dim connectionString_iDMS = "Data Source=" & Common.servername & ";Initial Catalog=AppDB;User Id=sa;Password=sss;"
            Dim coniDMS As SqlConnection = New SqlConnection(connectionString_iDMS)
iASCheck:   Try
                sSql = "select * from UserAttendance where (iASCheck is NULL or iASCheck<>'Y')  order by UserID, AttDateTime"
                Rs = New DataSet
                adap = New SqlDataAdapter(sSql, coniDMS)
                adap.Fill(Rs)
            Catch ex As Exception
                'If ex.Message.ToString = "Invalid column name 'iASCheck'." Then
                sSql = "ALTER TABLE UserAttendance ADD iASCheck char(1)"
                If coniDMS.State <> ConnectionState.Open Then
                    coniDMS.Open()
                End If
                cmd = New SqlCommand(sSql, coniDMS)
                cmd.ExecuteNonQuery()
                If coniDMS.State <> ConnectionState.Closed Then
                    coniDMS.Close()
                End If
                GoTo iASCheck
                'End If
            End Try
            'MsgBox("Count " & Rs.Tables(0).Rows.Count)

            Dim RowIdIdmsLst As New List(Of String)()
            If Rs.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To Rs.Tables(0).Rows.Count - 1 ' Do While Not Rs.EOF
                    XtraMasterTest.LabelControlCount.Text = "Inserting data " & i + 1 & " of " & Rs.Tables(0).Rows.Count
                    XtraMasterTest.LabelControlStatus.Text = "Inserting data from iDMS " & Rs.Tables(0).Rows(i).Item("UserID").ToString.Trim & "  DateTime: " & Rs.Tables(0).Rows(i).Item("AttDateTime").ToString.Trim
                    Application.DoEvents()
                    mStatus = ""
                    'sSql = "select * from tblmachine where Location= '" & Rs.Tables(0).Rows(i).Item("device_id").ToString.Trim & "'"
                    mCARDNO = Rs.Tables(0).Rows(i).Item("USERID").ToString.Trim
                    If IsNumeric(mCARDNO) Then
                        mCARDNO = Convert.ToDouble(mCARDNO).ToString("000000000000")
                    End If
                    sSql = "select ID_NO from tblmachine where LOCATION='" & Rs.Tables(0).Rows(i).Item("DeviceID").ToString.Trim & "'"
                    RsDev = New DataSet
                    If servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, con1)
                        adapA.Fill(RsDev)
                    Else
                        adap = New SqlDataAdapter(sSql, coniDMS)
                        adap.Fill(RsDev)
                    End If


                    'RsDev = Cn.Execute(sSql)
                    If RsDev.Tables(0).Rows.Count > 0 Then
                        mDevId = Convert.ToDouble(RsDev.Tables(0).Rows(0).Item("ID_NO").ToString.Trim).ToString("00")
                    Else
                        mDevId = "01"
                    End If

                    'mCARDNO = Convert.ToDouble(Rs.Tables(0).Rows(i).Item("Employee_ID").ToString.Trim).ToString("000000000000")
                    mDate1 = Convert.ToDateTime(Rs.Tables(0).Rows(i).Item("AttDateTime").ToString.Trim) '.ToString("yyyy-MM-dd HH:mm:ss")
                    'sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & mCARDNO & "' and TblEmployee.dateofjoin <='" & mDate1.ToString("yyyy-MM-dd") & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                    sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & mCARDNO & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                    Dim ds As DataSet = New DataSet
                    If servername = "Access" Then
                        'sSql = "select paycode from tblEmployee where PRESENTCARDNO='" & mCARDNO & "'and active='Y' and FORMAT(dateofjoin,'yyyy-MM-dd HH:mm:ss') <='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.Id  from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & mCARDNO & "' and FORMAT(TblEmployee.dateofjoin,'yyyy-MM-dd') <='" & mDate1.ToString("yyyy-MM-dd") & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                        adapA = New OleDbDataAdapter(sSql, con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSql, con)
                        adap.Fill(ds)
                    End If

                    'sSql = "Insert into RAWDATA(Cardno,OfficePunch,id_NO,REASONCODE,Inout,Error_Code,DOOR_TIME,PROCESS) VALUES ('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','" & mDevId & "','','','00','00','N' )"
                    Try
                        If ds.Tables(0).Rows.Count > 0 Then
                            RowIdIdmsLst.Add(Rs.Tables(0).Rows(i).Item("RowID").ToString.Trim)
                            sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & mDevId & "','" & inout & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) "
                            Dim sSql1 As String = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & mDevId & "','" & inout & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) "
                            If servername = "Access" Then
                                If con1.State <> ConnectionState.Open Then
                                    con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, con1)
                                cmd1.ExecuteNonQuery()
                                Try
                                    cmd1 = New OleDbCommand(sSql1, con1)
                                    cmd1.ExecuteNonQuery()
                                Catch ex As Exception

                                End Try
                                If con1.State <> ConnectionState.Closed Then
                                    con1.Close()
                                End If
                            Else
                                If con.State <> ConnectionState.Open Then
                                    con.Open()
                                End If
                                cmd = New SqlCommand(sSql, con)
                                cmd.ExecuteNonQuery()
                                Try
                                    cmd = New SqlCommand(sSql1, con)
                                    cmd.ExecuteNonQuery()
                                Catch ex As Exception
                                End Try
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            End If
                            'process
                            'Cn.Execute (sSql), i
                            If ds.Tables(0).Rows.Count > 0 Then
                                Remove_Duplicate_Punches(mDate1, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                    Process_AllRTC(mDate1.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                Else
                                    Process_AllnonRTC(mDate1, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    If EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                                        Process_AllnonRTCMulti(mDate1, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    End If
                                End If
                            End If

                        End If
                    Catch ex As Exception
                        'MsgBox(ex.Message & "line 15598")
                    End Try
                Next 'Loop

                sSql = "update UserAttendance set iASCheck ='Y' where RowID IN ('" & String.Join("', '", RowIdIdmsLst.Distinct.ToArray()) & "')"
                If coniDMS.State <> ConnectionState.Open Then
                    coniDMS.Open()
                End If
                cmd = New SqlCommand(sSql, coniDMS)
                cmd.ExecuteNonQuery()
                If coniDMS.State <> ConnectionState.Closed Then
                    coniDMS.Close()
                End If
            End If
            XtraMasterTest.LabelControlCount.Text = ""
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
        Catch ex As Exception
            'MsgBox(ex.Message & "line 15641")
        End Try
    End Sub
    Public Sub Convertdat_BioSecurity()
        Dim mFile, mLine, mPath
        Dim mDate As DateTime
        Dim mCnt As Integer
        Dim Rs As DataSet = New DataSet 'ADODB.Recordset
        Dim rsTmp As DataSet = New DataSet '
        Dim RsDev As DataSet = New DataSet '
        Dim aFileNum As Integer
        Dim sFileName As String
        Dim mCARDNO As String
        Dim mDevId As String
        Dim mDate1 As DateTime
        Dim rsCardRepl As DataSet = New DataSet '
        Dim bCardRepl As Boolean
        Dim mrescd As String
        Dim TempCardNo As String
        ' Dim i As Integer
        Dim mReaderNo As String
        Dim inout As String
        Dim mReaders As String
        Dim mStatus As String
        Dim iscalled As Boolean

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand
        Try
            'Dim connectionString_WDMS = "Data Source=" & Common.servername & ";Initial Catalog=wdms6;User Id=sa;Password=Passw0rd!;"
            Dim connectionString_WDMS = Common.ConnectionString.Replace(Common.DB, Common.BioSecurityDBName)
            Dim WDMS_con As SqlConnection = New SqlConnection(connectionString_WDMS)
            sSql = "SELECT PUNCH_DATE_BioSecurity FROM Min_max_Date "
            rsTmp = New DataSet
            If servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1)
                adapA.Fill(rsTmp)
            Else
                adap = New SqlDataAdapter(sSql, con)
                adap.Fill(rsTmp)
            End If
            'rsTmp = Cn.Execute(sSql)
            If rsTmp.Tables(0).Rows.Count > 0 Then
                If rsTmp.Tables(0).Rows(0).Item(0).ToString.Trim <> "" Then
                    mDate = Convert.ToDateTime(rsTmp.Tables(0).Rows(0).Item(0).ToString.Trim) '.ToString("yyyy-MM-dd HH:mm:ss")
                Else
                    mDate = Now().ToString("yyyy-MM-01 00:00:00")
                End If
            Else
                mDate = Now().ToString("yyyy-MM-01 00:00:00")
            End If

            sSql = "select * from acc_transaction where [event_time]>='" & mDate.ToString("yyyy-MM-01 00:00:00") & "' and pin!='' order by pin,[event_time]"
            Rs = New DataSet
            If servername = "Access" Then
                Return
                'sSql = "select * from tbl_realtime_glog where Format(io_time, 'yyyy-MM-dd HH:mm:ss')>='" & mDate.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                'adapA = New OleDbDataAdapter(sSql, con1)
                'adapA.Fill(Rs)
            Else
                adap = New SqlDataAdapter(sSql, WDMS_con)
                adap.Fill(Rs)
            End If
            'MsgBox("Count " & Rs.Tables(0).Rows.Count)

            'Rs = Cn_C.Execute(sSql)
            If Rs.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To Rs.Tables(0).Rows.Count - 1 ' Do While Not Rs.EOF
                    XtraMasterTest.LabelControlStatus.Text = "Inserting data from BioSecurity " & Rs.Tables(0).Rows(i).Item("pin").ToString.Trim & "  DateTime: " & Rs.Tables(0).Rows(i).Item("event_time").ToString.Trim
                    Application.DoEvents()
                    mStatus = ""
                    mCARDNO = Rs.Tables(0).Rows(i).Item("pin").ToString.Trim
                    If IsNumeric(mCARDNO) Then
                        mCARDNO = Convert.ToDouble(mCARDNO).ToString("000000000000")
                    End If
                    mDate1 = Convert.ToDateTime(Rs.Tables(0).Rows(i).Item("event_time").ToString.Trim) '.ToString("yyyy-MM-dd HH:mm:ss")
                    mDevId = "BS"
                    sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & mCARDNO & "' and TblEmployee.dateofjoin <='" & mDate1.ToString("yyyy-MM-dd") & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                    Dim ds As DataSet = New DataSet
                    If servername = "Access" Then
                        ''sSql = "select paycode from tblEmployee where PRESENTCARDNO='" & mCARDNO & "'and active='Y' and FORMAT(dateofjoin,'yyyy-MM-dd HH:mm:ss') <='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        'sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.Id  from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & mCARDNO & "' and FORMAT(TblEmployee.dateofjoin,'yyyy-MM-dd') <='" & mDate1.ToString("yyyy-MM-dd") & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                        'adapA = New OleDbDataAdapter(sSql, con1)
                        'adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSql, con)
                        adap.Fill(ds)
                    End If
                    Try
                        If ds.Tables(0).Rows.Count > 0 Then
                            sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & mDevId & "','" & inout & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) "
                            Dim sSql1 As String = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & mDevId & "','" & inout & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) "
                            If servername = "Access" Then
                                If con1.State <> ConnectionState.Open Then
                                    con1.Open()
                                End If                               
                                Try
                                    cmd1 = New OleDbCommand(sSql, con1)
                                    cmd1.ExecuteNonQuery()
                                    cmd1 = New OleDbCommand(sSql1, con1)
                                    cmd1.ExecuteNonQuery()
                                Catch ex As Exception

                                End Try
                                If con1.State <> ConnectionState.Closed Then
                                    con1.Close()
                                End If
                            Else
                                If con.State <> ConnectionState.Open Then
                                    con.Open()
                                End If
                               
                                Try
                                    cmd = New SqlCommand(sSql, con)
                                    cmd.ExecuteNonQuery()
                                    cmd = New SqlCommand(sSql1, con)
                                    cmd.ExecuteNonQuery()
                                Catch ex As Exception
                                    'MsgBox(ex.Message & " 12553")
                                End Try
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            End If
                            'process
                            'Cn.Execute (sSql), i

                            If ds.Tables(0).Rows.Count > 0 Then
                                Remove_Duplicate_Punches(mDate1, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                    Process_AllRTC(mDate1.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                Else
                                    Process_AllnonRTC(mDate1, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    If EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                                        Process_AllnonRTCMulti(mDate1, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    End If
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        'MsgBox(ex.Message & " 12596")
                    End Try                  
                Next 'Loop

                If rsTmp.Tables(0).Rows.Count = 0 Then
                    sSql = "Insert Into Min_max_Date (PUNCH_DATE_BioSecurity) values('" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "')"
                Else
                    sSql = "Update Min_max_Date set PUNCH_DATE_BioSecurity ='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                End If
                If servername = "Access" Then
                    If con1.State <> ConnectionState.Open Then
                        con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, con1)
                    cmd1.ExecuteNonQuery()
                    If con1.State <> ConnectionState.Closed Then
                        con1.Close()
                    End If
                Else
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    cmd = New SqlCommand(sSql, con)
                    cmd.ExecuteNonQuery()
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub Convertdat_ZKAccess()
        Dim mFile, mLine, mPath
        Dim mDate As DateTime
        Dim mCnt As Integer
        Dim Rs As DataSet = New DataSet 'ADODB.Recordset
        Dim rsTmp As DataSet = New DataSet '
        Dim RsDev As DataSet = New DataSet '
        Dim aFileNum As Integer
        Dim sFileName As String
        Dim mCARDNO As String
        Dim mDevId As String
        Dim mDate1 As DateTime
        Dim rsCardRepl As DataSet = New DataSet '
        Dim bCardRepl As Boolean
        Dim mrescd As String
        Dim TempCardNo As String
        ' Dim i As Integer
        Dim mReaderNo As String
        Dim inout As String
        Dim mReaders As String
        Dim mStatus As String
        Dim iscalled As Boolean

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand

        'Dim connectionString_ZKAccess = "Data Source=PL-HO-DT013\SQLEXPRESS;Initial Catalog=accessnew;User Id=access;Password=access;"
        ''Dim connectionString_ZKAccess = "Data Source=DESKTOP-PG6NDV8;Initial Catalog=access;User Id=sa;Password=sss;"
        Dim connectionString_ZKAccess = Common.ConnectionString.Replace(Common.DB, Common.ZKAccessDBName)
        Dim ZKAccess_con As SqlConnection = New SqlConnection(connectionString_ZKAccess)
        'MsgBox(connectionString_ZKAccess)
        sSql = "SELECT PUNCH_DATE_ZKAccess FROM Min_max_Date "
        rsTmp = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(rsTmp)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(rsTmp)
        End If
        'rsTmp = Cn.Execute(sSql)
        If rsTmp.Tables(0).Rows.Count > 0 Then
            If rsTmp.Tables(0).Rows(0).Item(0).ToString.Trim <> "" Then
                mDate = Convert.ToDateTime(rsTmp.Tables(0).Rows(0).Item(0).ToString.Trim) '.ToString("yyyy-MM-dd HH:mm:ss")
            Else
                mDate = Now().ToString("yyyy-MM-01 00:00:00")
            End If
        Else
            mDate = Now().ToString("yyyy-MM-01 00:00:00")
        End If

        'for emp name DOJ and DOB from ZKAccess DB
        'select PIN, [time], case 
        'when lastname is null then  (Name ) 
        'when Name is null then (lastname)
        'when Name is null and lastname is null then null
        'else (Name + ' ' + lastname ) end as name, BIRTHDAY, HIREDDAY, userid  from acc_monitor_log left outer join userinfo on userinfo.Badgenumber=acc_monitor_log.pin 
        'where Time>='2018-10-15 10:42:39'  and pin <> '' and pin <> '0' order by pin,[Time]

        sSql = "select * from acc_monitor_log where Time>='" & mDate.ToString("yyyy-MM-dd HH:mm:ss") & "'  and pin <> '' and pin <> '0' order by pin,[Time]"
        Rs = New DataSet
        If servername = "Access" Then
            Return
            'sSql = "select * from acc_monitor_log where (Time, 'yyyy-MM-dd HH:mm:ss')>='" & mDate.ToString("yyyy-MM-dd HH:mm:ss") & "' and pin <> ''"
            'adapA = New OleDbDataAdapter(sSql, con1)
            'adapA.Fill(Rs)
        Else
            adap = New SqlDataAdapter(sSql, ZKAccess_con)
            adap.Fill(Rs)
        End If
        'MsgBox("Count " & Rs.Tables(0).Rows.Count)

        'Rs = Cn_C.Execute(sSql)
        If Rs.Tables(0).Rows.Count > 0 Then

            For i As Integer = 0 To Rs.Tables(0).Rows.Count - 1 ' Do While Not Rs.EOF
                XtraMasterTest.LabelControlStatus.Text = "Inserting data from ZKAccess data " & Rs.Tables(0).Rows(i).Item("PIN").ToString.Trim & "  DateTime: " & Rs.Tables(0).Rows(i).Item("Time").ToString.Trim
                Application.DoEvents()
                'mStatus = ""
                ''sSql = "select * from tblmachine where Location= '" & Rs.Tables(0).Rows(i).Item("device_id").ToString.Trim & "'"

                'sSql = "select PIN as 'CardNo' from employee where ID='" & Rs.Tables(0).Rows(i).Item("Employee_ID").ToString.Trim & "'"
                'RsDev = New DataSet
                'If servername = "Access" Then
                '    adapA = New OleDbDataAdapter(sSql, con1)
                '    adapA.Fill(RsDev)
                'Else
                '    adap = New SqlDataAdapter(sSql, connectionString_WDMS)
                '    adap.Fill(RsDev)
                'End If
                'If RsZK.RecordCount > 0 Then
                '    mCARDNO = Trim(Format(RsZK("CardNo"), "00000000"))
                'End If
                'PunchDate = Format(Rs!Time, "dd/mm/yyyy HH:MM:SS")
                'If Rs.Tables(0).Rows(i).Item("iclock_id").ToString.Trim = "1" Then
                '    inout = "I"
                'Else
                '    inout = "O"
                'End If

                'RsDev = Cn.Execute(sSql)
                ' If RsDev.Tables(0).Rows.Count > 0 Then
                'mDevId = Convert.ToDouble(RsDev.Tables(0).Rows(0).Item("ID_NO").ToString.Trim).ToString("00")

                mCARDNO = Rs.Tables(0).Rows(i).Item("PIN").ToString.Trim
                If mCARDNO.Trim = "" Then
                    Continue For
                End If
                If IsNumeric(mCARDNO) Then
                    mCARDNO = Convert.ToDouble(mCARDNO).ToString("000000000000")
                End If
                'Else
                mDevId = "1"
                ' Else
                ' Continue For
                ' End If

                'mCARDNO = Convert.ToDouble(Rs.Tables(0).Rows(i).Item("Employee_ID").ToString.Trim).ToString("000000000000")
                mDate1 = Convert.ToDateTime(Rs.Tables(0).Rows(i).Item("Time").ToString.Trim) '.ToString("yyyy-MM-dd HH:mm:ss")
                'sSql = "select paycode from tblEmployee where PRESENTCARDNO='" & mCARDNO & "' and active='Y' and dateofjoin <='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & mCARDNO & "' and TblEmployee.dateofjoin <='" & mDate1.ToString("yyyy-MM-dd") & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                Dim ds As DataSet = New DataSet
                If servername = "Access" Then
                    'sSql = "select paycode from tblEmployee where PRESENTCARDNO='" & mCARDNO & "'and active='Y' and FORMAT(dateofjoin,'yyyy-MM-dd HH:mm:ss') <='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.Id  from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & mCARDNO & "' and FORMAT(TblEmployee.dateofjoin,'yyyy-MM-dd') <='" & mDate1.ToString("yyyy-MM-dd") & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                    adapA = New OleDbDataAdapter(sSql, con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, con)
                    adap.Fill(ds)
                End If

                'sSql = "Insert into RAWDATA(Cardno,OfficePunch,id_NO,REASONCODE,Inout,Error_Code,DOOR_TIME,PROCESS) VALUES ('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','" & mDevId & "','','','00','00','N' )"
                Try


                    If ds.Tables(0).Rows.Count > 0 Then
                        sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & mDevId & "','" & inout & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) "
                        Dim sSql1 As String = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & mDevId & "','" & inout & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) "
                        If servername = "Access" Then
                            If con1.State <> ConnectionState.Open Then
                                con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, con1)
                            cmd1.ExecuteNonQuery()
                            Try
                                cmd1 = New OleDbCommand(sSql1, con1)
                                cmd1.ExecuteNonQuery()
                            Catch ex As Exception

                            End Try
                            If con1.State <> ConnectionState.Closed Then
                                con1.Close()
                            End If
                        Else
                            If con.State <> ConnectionState.Open Then
                                con.Open()
                            End If
                            cmd = New SqlCommand(sSql, con)
                            cmd.ExecuteNonQuery()
                            Try
                                cmd = New SqlCommand(sSql1, con)
                                cmd.ExecuteNonQuery()
                            Catch ex As Exception
                            End Try
                            If con.State <> ConnectionState.Closed Then
                                con.Close()
                            End If
                        End If
                        'process
                        'Cn.Execute (sSql), i



                        If ds.Tables(0).Rows.Count > 0 Then
                            Remove_Duplicate_Punches(mDate1, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                            If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                Process_AllRTC(mDate1.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                            Else
                                Process_AllnonRTC(mDate1, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                If EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                                    Process_AllnonRTCMulti(mDate1, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                End If
                            End If
                        End If
                    End If
                Catch ex As Exception
                    'MsgBox("line 14618 " & ex.Message)
                End Try
            Next 'Loop
            If rsTmp.Tables(0).Rows.Count = 0 Then
                sSql = "Insert Into Min_max_Date (PUNCH_DATE_ZKAccess) values('" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "')"
            Else
                sSql = "Update Min_max_Date set PUNCH_DATE_ZKAccess ='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            End If
            If servername = "Access" Then
                If con1.State <> ConnectionState.Open Then
                    con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, con1)
                cmd1.ExecuteNonQuery()
                If con1.State <> ConnectionState.Closed Then
                    con1.Close()
                End If
            Else
                If con.State <> ConnectionState.Open Then
                    con.Open()
                End If
                cmd = New SqlCommand(sSql, con)
                cmd.ExecuteNonQuery()
                If con.State <> ConnectionState.Closed Then
                    con.Close()
                End If
            End If
        End If
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try
    End Sub
    Public Sub Convertdat_TWAccess()
        Dim mFile, mLine, mPath
        Dim mDate As DateTime
        Dim mCnt As Integer
        Dim Rs As DataSet = New DataSet 'ADODB.Recordset
        Dim rsTmp As DataSet = New DataSet '
        Dim RsDev As DataSet = New DataSet '
        Dim aFileNum As Integer
        Dim sFileName As String
        Dim mCARDNO As String
        Dim mDevId As String
        Dim mDate1 As DateTime
        Dim rsCardRepl As DataSet = New DataSet '
        Dim bCardRepl As Boolean
        Dim mrescd As String
        Dim TempCardNo As String
        ' Dim i As Integer
        Dim mReaderNo As String
        Dim inout As String
        Dim mReaders As String
        Dim mStatus As String
        Dim iscalled As Boolean

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand

         Dim connectionString_TWAccess = Common.ConnectionString.Replace(Common.DB, Common.TWAccessDBName)
        Dim TWAccess_con As SqlConnection = New SqlConnection(connectionString_TWAccess)

        'MsgBox(connectionString_ZKAccess)
        sSql = "SELECT PUNCH_DATE_TWAccess FROM Min_max_Date "
        rsTmp = New DataSet
        If servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(rsTmp)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(rsTmp)
        End If
        'rsTmp = Cn.Execute(sSql)
        If rsTmp.Tables(0).Rows.Count > 0 Then
            If rsTmp.Tables(0).Rows(0).Item(0).ToString.Trim <> "" Then
                mDate = Convert.ToDateTime(rsTmp.Tables(0).Rows(0).Item(0).ToString.Trim) '.ToString("yyyy-MM-dd HH:mm:ss")
            Else
                mDate = Now().ToString("yyyy-MM-01 00:00:00")
            End If
        Else
            mDate = Now().ToString("yyyy-MM-01 00:00:00")
        End If

        sSql = "select PCode 'UserNo',cast(DataTime as datetime) 'PunchTime' from cardrecord where personnelID<>'0' and  cast(datatime as datetime)>='" & mDate.ToString("yyyy-MM-dd HH:mm:ss") & "'order by personnelID,datatime "
        Rs = New DataSet
        If servername = "Access" Then
            Return
            'sSql = "select * from acc_monitor_log where (Time, 'yyyy-MM-dd HH:mm:ss')>='" & mDate.ToString("yyyy-MM-dd HH:mm:ss") & "' and pin <> ''"
            'adapA = New OleDbDataAdapter(sSql, con1)
            'adapA.Fill(Rs)
        Else
            adap = New SqlDataAdapter(sSql, TWAccess_con)
            adap.Fill(Rs)
        End If
        'MsgBox("Count " & Rs.Tables(0).Rows.Count)

        'Rs = Cn_C.Execute(sSql)
        If Rs.Tables(0).Rows.Count > 0 Then

            For i As Integer = 0 To Rs.Tables(0).Rows.Count - 1 ' Do While Not Rs.EOF
                XtraMasterTest.LabelControlStatus.Text = "Inserting data from TW Access " & Rs.Tables(0).Rows(i).Item("UserNo").ToString.Trim & "  DateTime: " & Rs.Tables(0).Rows(i).Item("PunchTime").ToString.Trim
                Application.DoEvents()
               
                mCARDNO = Rs.Tables(0).Rows(i).Item("UserNo").ToString.Trim
                If mCARDNO.Trim = "" Then
                    Continue For
                End If
                If IsNumeric(mCARDNO) Then
                    mCARDNO = Convert.ToDouble(mCARDNO).ToString("000000000000")
                End If
                'Else
                mDevId = "1"
                
                mDate1 = Convert.ToDateTime(Rs.Tables(0).Rows(i).Item("PunchTime").ToString.Trim) '.ToString("yyyy-MM-dd HH:mm:ss")
                mDate1 = mDate1.AddDays(-2)
                'sSql = "select paycode from tblEmployee where PRESENTCARDNO='" & mCARDNO & "' and active='Y' and dateofjoin <='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & mCARDNO & "' and TblEmployee.dateofjoin <='" & mDate1.ToString("yyyy-MM-dd") & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                Dim ds As DataSet = New DataSet
                If servername = "Access" Then
                    'sSql = "select paycode from tblEmployee where PRESENTCARDNO='" & mCARDNO & "'and active='Y' and FORMAT(dateofjoin,'yyyy-MM-dd HH:mm:ss') <='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.Id  from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & mCARDNO & "' and FORMAT(TblEmployee.dateofjoin,'yyyy-MM-dd') <='" & mDate1.ToString("yyyy-MM-dd") & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                    adapA = New OleDbDataAdapter(sSql, con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, con)
                    adap.Fill(ds)
                End If
                Try
                    If ds.Tables(0).Rows.Count > 0 Then
                        sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & mDevId & "','" & inout & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) "
                        Dim sSql1 As String = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & mCARDNO & "','" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & mDevId & "','" & inout & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) "
                        If servername = "Access" Then
                            If con1.State <> ConnectionState.Open Then
                                con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, con1)
                            cmd1.ExecuteNonQuery()
                            Try
                                cmd1 = New OleDbCommand(sSql1, con1)
                                cmd1.ExecuteNonQuery()
                            Catch ex As Exception

                            End Try
                            If con1.State <> ConnectionState.Closed Then
                                con1.Close()
                            End If
                        Else
                            If con.State <> ConnectionState.Open Then
                                con.Open()
                            End If
                            cmd = New SqlCommand(sSql, con)
                            cmd.ExecuteNonQuery()
                            Try
                                cmd = New SqlCommand(sSql1, con)
                                cmd.ExecuteNonQuery()
                            Catch ex As Exception
                            End Try
                            If con.State <> ConnectionState.Closed Then
                                con.Close()
                            End If
                        End If
                        'process
                        'Cn.Execute (sSql), i

                        If ds.Tables(0).Rows.Count > 0 Then
                            Remove_Duplicate_Punches(mDate1, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                            If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                Process_AllRTC(mDate1.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                            Else
                                Process_AllnonRTC(mDate1, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                If EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                                    Process_AllnonRTCMulti(mDate1, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                End If
                            End If
                        End If
                    End If
                Catch ex As Exception
                    'MsgBox("line 14618 " & ex.Message)
                End Try
            Next 'Loop
            If rsTmp.Tables(0).Rows.Count = 0 Then
                sSql = "Insert Into Min_max_Date (PUNCH_DATE_TWAccess) values('" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "')"
            Else
                sSql = "Update Min_max_Date set PUNCH_DATE_TWAccess ='" & mDate1.ToString("yyyy-MM-dd HH:mm:ss") & "'"
            End If
            If servername = "Access" Then
                If con1.State <> ConnectionState.Open Then
                    con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, con1)
                cmd1.ExecuteNonQuery()
                If con1.State <> ConnectionState.Closed Then
                    con1.Close()
                End If
            Else
                If con.State <> ConnectionState.Open Then
                    con.Open()
                End If
                cmd = New SqlCommand(sSql, con)
                cmd.ExecuteNonQuery()
                If con.State <> ConnectionState.Closed Then
                    con.Close()
                End If
            End If
        End If
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try
    End Sub
    'End All 3rd party integrations

    'for bio employee validity
    Structure ExtCmd_USERDOORINFO
        Public CmdHeader As ExtCmdStructHeader
        Public UserID As UInt32
        Public Reserved As Byte
        <System.Runtime.InteropServices.MarshalAs(UnmanagedType.ByValArray, SizeConst:=7)>
        Public WeekPassTime As Byte()
        Public StartYear As Short
        Public StartMonth As Byte
        Public StartDay As Byte
        Public EndYear As Short
        Public EndMonth As Byte
        Public EndDay As Byte
        Public StartHour As Byte
        Public Startmin As Short
        Public Endhour As Byte
        Public Endmin As Byte
        Public Sub Init(ByVal asCmdCode As String, ByVal anUserID As UInt32)
            'for bio employee validity
            Dim VER_USERDOORINFO_V1 As Integer = 2
            Dim SIZE_USERDOORINFO_V1 As Integer = 20
            'End for bio employee validity
            CmdHeader = New ExtCmdStructHeader()
            CmdHeader.Init(asCmdCode, VER_USERDOORINFO_V1, SIZE_USERDOORINFO_V1)
            WeekPassTime = New Byte(6) {}
            UserID = anUserID
            Reserved = 0
            StartYear = 0
            StartMonth = 0
            StartDay = 0
            EndYear = 0
            EndMonth = 0
            EndDay = 0
        End Sub
    End Structure
    Structure ExtCmdStructHeader
        <System.Runtime.InteropServices.MarshalAs(UnmanagedType.ByValArray, SizeConst:=56)>
        Public bytCmdCode As Byte()
        Public StructVer As Int32
        Public StructSize As Int32

        Public Sub Init(ByVal asCmdCode As String, ByVal aStructVer As Integer, ByVal aStructSize As Integer)
            bytCmdCode = New Byte(56 - 1) {}
            Array.Clear(bytCmdCode, 0, bytCmdCode.Length)
            Dim bytAnsi As Byte() = System.Text.Encoding.GetEncoding("utf-8").GetBytes(asCmdCode)
            Dim nBytesToCopy As Integer = bytAnsi.Length
            If nBytesToCopy > bytCmdCode.Length - 1 Then nBytesToCopy = bytCmdCode.Length - 1
            Array.Copy(bytAnsi, bytCmdCode, nBytesToCopy)
            StructVer = aStructVer
            StructSize = aStructSize
        End Sub
    End Structure
    'End for bio employee validity
    Public Shared Sub CheckNewColumnSMS()
        Dim sSql As String = "select IsAll, AllSMS1, AllSMS2 from tblSMS"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As New DataSet
        Try
            If servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            sSql = "ALTER TABLE tblSMS ADD IsAll char(1), AllSMS1 varchar(500), AllSMS2 varchar(500)"
            Dim sSql2 As String = "ALTER TABLE tblSMS ALTER COLUMN SMSKey VARCHAR(500)"
            Dim sSql1 As String = "Update tblSMS set IsAll='N', AllSMS1='', AllSMS2=''"
            If servername = "Access" Then
                If con1.State <> ConnectionState.Open Then
                    con1.Open()
                End If
                sSql = "ALTER TABLE tblSMS ADD IsAll char(1), AllSMS1 longtext, AllSMS2 longtext"
                Dim cmd1 As OleDbCommand = New OleDbCommand(sSql, con1)
                cmd1.ExecuteNonQuery()

                cmd1 = New OleDbCommand(sSql1, con1)
                cmd1.ExecuteNonQuery()

                sSql2 = "ALTER TABLE tblSMS ALTER COLUMN SMSKey longtext"
                cmd1 = New OleDbCommand(sSql2, con1)
                cmd1.ExecuteNonQuery()
                If con1.State <> ConnectionState.Closed Then
                    con1.Close()
                End If
            Else
                If con.State <> ConnectionState.Open Then
                    con.Open()
                End If
                Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                cmd.ExecuteNonQuery()

                cmd = New SqlCommand(sSql1, con)
                cmd.ExecuteNonQuery()

                cmd = New SqlCommand(sSql2, con)
                cmd.ExecuteNonQuery()

                If con.State <> ConnectionState.Closed Then
                    con.Close()
                End If
            End If
        End Try



        sSql = "select DeviceWiseInOut from tblSMS"
        ds = New DataSet
        Try
            If servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            sSql = "ALTER TABLE tblSMS ADD DeviceWiseInOut char(1)"
            Dim sSql1 As String = "Update tblSMS set DeviceWiseInOut='N'"
            If servername = "Access" Then
                If con1.State <> ConnectionState.Open Then
                    con1.Open()
                End If
                sSql = "ALTER TABLE tblSMS ADD DeviceWiseInOut char(1)"
                Dim cmd1 As OleDbCommand = New OleDbCommand(sSql, con1)
                cmd1.ExecuteNonQuery()

                cmd1 = New OleDbCommand(sSql1, con1)
                cmd1.ExecuteNonQuery()
                If con1.State <> ConnectionState.Closed Then
                    con1.Close()
                End If
            Else
                If con.State <> ConnectionState.Open Then
                    con.Open()
                End If
                Dim cmd As SqlCommand = New SqlCommand(sSql, con)
                cmd.ExecuteNonQuery()

                cmd = New SqlCommand(sSql1, con)
                cmd.ExecuteNonQuery()
                If con.State <> ConnectionState.Closed Then
                    con.Close()
                End If
            End If
        End Try

    End Sub
    Public Sub sendSMS(ByVal phone As String, ByVal message As String)
        If g_SMSApplicable = "" Then
            Load_SMS_Policy()
        End If
        'url = "http://sms.mobtexting.com/index.php/api?method=sms.normal&api_key=" & Trim(G_SmsKey) & "&to=" & Trim(rsEmp.Tables(0).Rows(0).Item("Telephone1").ToString.Trim) & "&sender=" & Trim(G_SenderID) & "&message=" & Trim(LateSMSContent) & "&flash=0&unicode=1"
        Dim url1 As String = G_SmsKey.Replace("[MOBILENO]", phone.Trim).Replace("[MESSAGE]", message.Trim)
        'MessageBox.Show(url1)
        Dim sendSMSThread As Thread = New Thread(Sub() Me.sendSMSThreadFun(url1))
        sendSMSThread.Start()
        sendSMSThread.IsBackground = True
    End Sub
    Public Sub sendSMSThreadFun(ByVal url1 As String)
        Dim request As HttpWebRequest
        'Dim response As HttpWebResponse = Nothing       
        Try
            Dim WebRequestObject As HttpWebRequest = CType(HttpWebRequest.Create(url1), HttpWebRequest)
            WebRequestObject.Timeout = Timeout.Infinite
            WebRequestObject.KeepAlive = True
            'WebRequestObject.Timeout = 5000
            'WebRequestObject.ReadWriteTimeout = 5000
            Dim response As HttpWebResponse
            response = DirectCast(WebRequestObject.GetResponse(), HttpWebResponse)
            response.Close()
            'request = DirectCast(WebRequest.Create(url1), HttpWebRequest)
            'response = DirectCast(request.GetResponse(), HttpWebResponse)
        Catch ex As Exception
        End Try
    End Sub
    Public Sub SendAbsentSMS()
        Dim rsCalender As DataSet = New DataSet
        Dim rsTime As DataSet = New DataSet
        Dim AbsentContent As String
        Dim EmpName As String
        Dim url As String
        Dim Content As String
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim LDt As DateTime
        LDt = Now.AddDays(-1).ToString("yyyy-MM-dd") 'Format(DateAdd("d", -1, Now()), "dd/MM/yyyy")
        Dim sSql As String
        If servername = "Access" Then
            sSql = "Select tbltimeregister.paycode,tblemployee.empname,tblemployee.telephone1 from tbltimeregister, tblemployee where tbltimeregister.paycode=tblemployee.paycode and FORMAT(dateoffice,'yyyy-MM-dd')='" & LDt.ToString("yyyy-MM-dd") & "' and in1 is null and trim(Telephone1)<>'' and tbltimeregister.AbsentSMS<>'Y'  and tblemployee.ACTIVE='Y'"
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(rsTime)
        Else
            sSql = "Select tbltimeregister.paycode,tblemployee.empname,tblemployee.telephone1 from tbltimeregister tbltimeregister join tblemployee tblemployee on tbltimeregister.paycode=tblemployee.paycode where dateoffice='" & LDt.ToString("yyyy-MM-dd") & "' and in1 is null and Telephone1 is not null and ltrim(rtrim(Telephone1))!='' and tbltimeregister.AbsentSMS<>'Y'  and tblemployee.ACTIVE='Y'"
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(rsTime)
        End If
        'rsTime = Cn.Execute(sSql)
        If rsTime.Tables(0).Rows.Count > 0 Then
            'Do While Not rsTime.EOF
            For i As Integer = 0 To rsTime.Tables(0).Rows.Count - 1
                If Trim(rsTime.Tables(0).Rows(i).Item("Telephone1").ToString) <> "" Then
                    EmpName = Trim(rsTime.Tables(0).Rows(i).Item("EmpName").ToString)
                    AbsentContent = g_AbsContent1 + " " + EmpName + " " + g_AbsContent2 + " " + Format(LDt, "dd/MM/yyyy")
                    ''url = "http://alerts.3mdigital.co.in/api/web2sms.php?workingkey=" & G_SmsKey & "&sender=" & G_SenderID & "&to=" & Trim(rsTime("Telephone1")) & "&message=" & AbsentContent & ""
                    'url = "http://sms.mobtexting.com/index.php/api?method=sms.normal&api_key=" & Trim(G_SmsKey) & "&to=" & Trim(rsTime("Telephone1")) & "&sender=" & Trim(G_SenderID) & "&message=" & Trim(AbsentContent) & "&flash=0&unicode=1"
                    'Content = FrmMonitorZK.Inet1.OpenURL(url)
                    sendSMS(Trim(rsTime.Tables(0).Rows(i).Item("Telephone1").ToString), AbsentContent)
                    If servername = "Access" Then
                        sSql = "Update tblTimeregister Set AbsentSms='Y' where paycode='" & Trim(rsTime.Tables(0).Rows(i).Item("Paycode").ToString) & "' and format(dateoffice,'yyyy-MM-dd') = '" & LDt.ToString("yyyy-MM-dd") & "' "
                        If con1.State <> ConnectionState.Open Then
                            con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, con1)
                        cmd1.ExecuteNonQuery()
                        If con1.State <> ConnectionState.Closed Then
                            con1.Close()
                        End If
                    Else
                        sSql = "Update tblTimeregister Set AbsentSms='Y' where paycode='" & Trim(rsTime.Tables(0).Rows(i).Item("Paycode").ToString) & "' and dateoffice = '" & LDt.ToString("yyyy-MM-dd") & "' "
                        If con.State <> ConnectionState.Open Then
                            con.Open()
                        End If
                        cmd = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    End If
                    'Cn.Execute(sSql)
                    'rsTime.MoveNext()
                End If
            Next
            'Loop
        End If
        'If servername = "Access" Then
        '    sSql = "Update tblAbsentSMS Set AbsentSms='Y' where format(dateoffice,'yyyy-MM-dd') = '" & Now().ToString("yyyy-MM-dd") & "' "
        '    If con1.State <> ConnectionState.Open Then
        '        con1.Open()
        '    End If
        '    cmd1 = New OleDbCommand(sSql, con1)
        '    cmd1.ExecuteNonQuery()
        '    If con1.State <> ConnectionState.Closed Then
        '        con1.Close()
        '    End If
        'Else
        '    sSql = "Update tblAbsentSMS Set AbsentSms='Y' where dateoffice = '" & Now().ToString("yyyy-MM-dd") & "' "
        '    If con.State <> ConnectionState.Open Then
        '        con.Open()
        '    End If
        '    cmd = New SqlCommand(sSql, con)
        '    cmd.ExecuteNonQuery()
        '    If con.State <> ConnectionState.Closed Then
        '        con.Close()
        '    End If
        'End If
    End Sub
    Public Sub SendAbsentMS_CurrentDt()
        Dim rsCalender As DataSet = New DataSet
        Dim rsTime As DataSet = New DataSet
        Dim shiftStartTime As Date
        Dim AbsentContent As String
        Dim EmpName As String, IsSMSSent As String
        Dim url As String
        Dim Content As String
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim sSql As String
        If servername = "Access" Then
            'sSql = "SELECT tbltimeregister.paycode,tblemployee.empname,tblemployee.telephone1,tbltimeregister.shift,tblshiftmaster.STARTTIME as ShiftStartTime,tbltimeregister.AbsentSMS from tblshiftmaster inner join (tbltimeregister " & _
            '        " inner join tblemployee on tblemployee.paycode=tbltimeregister.paycode) on tblshiftmaster.shift=tbltimeregister.shift   where Format(dateoffice,'yyyy-mm-dd')='" & Now.ToString("yyyy-MM-dd") & "' and  trim(telephone1) <>'' and isnull(in1) "
            sSql = "SELECT tbltimeregister.paycode,tblemployee.empname,tblemployee.telephone1,tbltimeregister.shift,tblshiftmaster.STARTTIME as ShiftStartTime,tbltimeregister.AbsentSMS from tblshiftmaster, tbltimeregister, tblemployee " & _
                   " where tblemployee.paycode=tbltimeregister.paycode and tblshiftmaster.shift=tbltimeregister.shift and Format(dateoffice,'yyyy-MM-dd')='" & Now.ToString("yyyy-MM-dd") & "' and  trim(telephone1) <>'' and isnull(in1) and tblemployee.ACTIVE='Y'" 'and tbltimeregister.AbsentSMS <>'Y'
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(rsTime)
        Else
            sSql = "Select tbltimeregister.paycode,convert(varchar(10),getdate(),126)+' ' +convert(varchar(5),tblshiftmaster.starttime,108) 'shiftstarttime', " & _
                    " tblemployee.empname,tblemployee.telephone1,tbltimeregister.absentsms from tbltimeregister tbltimeregister join tblemployee tblemployee on tbltimeregister.paycode=tblemployee.paycode " & _
                    " join tblshiftmaster on tbltimeregister.shift=tblshiftmaster.shift where dateoffice='" & Now.ToString("yyyy-MM-dd") & "' and in1 is null and Telephone1 is not null and ltrim(rtrim(Telephone1))!='' and 2 is not null  and tblemployee.ACTIVE='Y'-- and tbltimeregister.AbsentSMS <>'Y'"
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(rsTime)
        End If
        'rsTime = Cn.Execute(sSql)
        If rsTime.Tables(0).Rows.Count > 0 Then
            'Do While Not rsTime.EOF
            For i As Integer = 0 To rsTime.Tables(0).Rows.Count - 1
                If Trim(rsTime.Tables(0).Rows(i).Item("Telephone1").ToString) <> "" Then
                    If Trim(rsTime.Tables(0).Rows(i).Item("AbsentSMS").ToString) = "" Then
                        IsSMSSent = "N"
                    Else
                        IsSMSSent = Trim(rsTime.Tables(0).Rows(i).Item("AbsentSMS").ToString)
                    End If

                    If IsSMSSent = "N" Then
                        EmpName = Trim(rsTime.Tables(0).Rows(i).Item("EmpName").ToString)
                        shiftStartTime = rsTime.Tables(0).Rows(i).Item("ShiftStartTime").ToString
                        shiftStartTime = DateAdd("n", g_AbsSMSAfter, shiftStartTime)
                        If Format(shiftStartTime, "HH:mm") < Format(Now(), "HH:mm") Then
                            If g_AbsName = "Y" Then
                                AbsentContent = g_AbsContent1 & " " & EmpName & " " & g_AbsContent2
                            ElseIf g_AbsDate = "Y" Then
                                AbsentContent = g_AbsContent1 & " " & g_AbsContent2 & " " & Now.ToString("dd-MM-yyyy")
                            ElseIf g_AbsDate = "Y" And g_AbsName = "Y" Then
                                AbsentContent = g_AbsContent1 & " " & EmpName & " " & g_AbsContent2 & " " & Now.ToString("dd-MM-yyyy")
                            Else
                                AbsentContent = g_AbsContent1 & " " & g_AbsContent2
                            End If

                            ''url = "http://alerts.3mdigital.co.in/api/web2sms.php?workingkey=" & G_SmsKey & "&sender=" & G_SenderID & "&to=" & Trim(rsTime("Telephone1")) & "&message=" & AbsentContent & ""
                            'url = "http://sms.mobtexting.com/index.php/api?method=sms.normal&api_key=" & Trim(G_SmsKey) & "&to=" & Trim(rsTime("Telephone1")) & "&sender=" & Trim(G_SenderID) & "&message=" & Trim(AbsentContent) & "&flash=0&unicode=1"
                            'Content = FrmMonitorZK.Inet1.OpenURL(url)
                            sendSMS(Trim(rsTime.Tables(0).Rows(i).Item("Telephone1").ToString), AbsentContent)
                            If servername = "Access" Then
                                sSql = "Update tblTimeregister Set AbsentSms='Y' where paycode='" & Trim(rsTime.Tables(0).Rows(i).Item("Paycode").ToString) & "' and Format(dateoffice,'yyyy-MM-dd') = '" & Now.ToString("yyyy-MM-dd") & "' "
                                If con1.State <> ConnectionState.Open Then
                                    con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, con1)
                                cmd1.ExecuteNonQuery()
                                If con1.State <> ConnectionState.Closed Then
                                    con1.Close()
                                End If
                            Else
                                sSql = "Update tblTimeregister Set AbsentSms='Y' where paycode='" & Trim(rsTime.Tables(0).Rows(i).Item("Paycode").ToString) & "' and dateoffice = '" & Now.ToString("yyyy-MM-dd") & "' "
                                If con.State <> ConnectionState.Open Then
                                    con.Open()
                                End If
                                cmd = New SqlCommand(sSql, con)
                                cmd.ExecuteNonQuery()
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            End If
                            'Cn.Execute(sSql)
                        End If
                    End If
                    'rsTime.MoveNext(
                End If
            Next
            'Loop
        End If
    End Sub
    Public Shared Function JSONCallHexa(empid As String, punchtime As DateTime, machineid As String)

        'Dim httpWebRequest = CType(WebRequest.Create("http://10.10.17.24/patientfirst/Registry"), HttpWebRequest)
        Dim httpWebRequest = DirectCast(WebRequest.Create("http://10.10.17.24/patientfirst/Registry"), HttpWebRequest)
        httpWebRequest.ContentType = "application/json"
        httpWebRequest.Method = "POST"
        httpWebRequest.Headers("Authorization") = "twl=GDF3#4C&G*$FDHN@GHJ" 'only for HEXA, Authorization is used only if there any Authorization give by API developer in API header.
        'httpWebRequest.Headers("Content-Type") = "application/json"

        'httpWebRequest.UseDefaultCredentials = True
        'httpWebRequest.PreAuthenticate = True
        'httpWebRequest.Credentials = CredentialCache.DefaultCredentials

        Dim FinalData As JObject = New JObject
        'Dim streamWriter As StreamWriter = New StreamWriter(httpWebRequest.GetRequestStream)
        Dim jsons As String
        Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
            FinalData.Add("action", "deliverybio")
            FinalData.Add("empid", empid)
            FinalData.Add("time", punchtime.ToString("yyyyMMdd HH:mm"))
            FinalData.Add("machineid", machineid)
            jsons = FinalData.ToString(Formatting.None)
            'MessageBox.Show(jsons)
            streamWriter.Write(jsons)
            streamWriter.Flush()
            streamWriter.Close()
        End Using
        Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
        Using streamReader = New StreamReader(httpResponse.GetResponseStream())
            'Dim streamReader = New StreamReader(httpResponse.GetResponseStream)
            Dim result = streamReader.ReadToEnd()
            Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            sw.BaseStream.Seek(0, SeekOrigin.End)
            sw.WriteLine(vbCrLf & jsons & ", " & result & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
            sw.Flush()
            sw.Close()
        End Using
    End Function
    Public Class IOCLDATA
        Public Site As String
        Public EMP As String
        Public [Date] As String
        Public IO As String
        Public RdrNum As String
        Public RdrName As String
    End Class
    Public Shared Function JSONCallIOCLStuct(IOCLFinal() As IOCLDATA) As Boolean
        Try
            'Dim httpWebRequest As WebRequest = WebRequest.Create("http://uat.indianoil.co.in/acsdatapush/webresources/acs/retrieve")  'UAT
            'Dim httpWebRequest As WebRequest = WebRequest.Create("https://spandan.indianoil.co.in/acsdatapush/webresources/acs/retrieve")
            'Dim httpWebRequest As WebRequest = WebRequest.Create("https://webapp.indianoil.co.in/acsdatapush/webresources/acs/retrieve") 'Live
            Dim httpWebRequest As WebRequest = WebRequest.Create(IOCLLink) 'Live
            httpWebRequest.ContentType = "application/json"
            httpWebRequest.Method = "POST"
            'httpWebRequest.Headers("Authorization") = "@csu$erp@$$" 'only for HEXA, Authorization is used only if there any Authorization give by API developer in API header.
            httpWebRequest.Headers("Authorization") = "Basic " & Convert.ToBase64String(Encoding.Default.GetBytes("acsuser:@csu$erp@$$"))

            'Dim jsonData As String = JsonConvert.SerializeObject(IOCLFinal)
            'Dim FinalData As JObject = New JObject
            Dim jsons As String = JsonConvert.SerializeObject(IOCLFinal)
            Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
                'FinalData.Add("Site", LocCode)
                'FinalData.Add("EMP", empid)
                'FinalData.Add("Date", punchtime.ToString("yyyy-MM-dd HH:mm:ss"))
                'FinalData.Add("IO", IO)
                'FinalData.Add("RdrNum", DeviceId)
                'FinalData.Add("RdrName", LocName)
                'jsons = FinalData.ToString(Formatting.None)
                'jsons = "[" & jsons & "]"
                'MessageBox.Show(jsons)
                streamWriter.Write(jsons)
                streamWriter.Flush()
                streamWriter.Close()
            End Using
            Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
            Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                'Dim streamReader = New StreamReader(httpResponse.GetResponseStream)
                Dim result = streamReader.ReadToEnd()
                Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.BaseStream.Seek(0, SeekOrigin.End)
                sw.WriteLine(vbCrLf & jsons & ", " & result & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                sw.Flush()
                sw.Close()
            End Using
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Shared Function JSONCallIOCL(empid As String, punchtime As DateTime, machineid As String, LocCode As String, LocName As String, IO As String, DeviceId As String)
        'Dim httpWebRequest As WebRequest = WebRequest.Create("http://uat.indianoil.co.in/acsdatapush/webresources/acs/retrieve")  'UAT
        'Dim httpWebRequest As WebRequest = WebRequest.Create("https://spandan.indianoil.co.in/acsdatapush/webresources/acs/retrieve")
        Dim httpWebRequest As WebRequest = WebRequest.Create("https://webapp.indianoil.co.in/acsdatapush/webresources/acs/retrieve") 'Live
        httpWebRequest.ContentType = "application/json"
        httpWebRequest.Method = "POST"
        'httpWebRequest.Headers("Authorization") = "@csu$erp@$$" 'only for HEXA, Authorization is used only if there any Authorization give by API developer in API header.
        httpWebRequest.Headers("Authorization") = "Basic " & Convert.ToBase64String(Encoding.Default.GetBytes("acsuser:@csu$erp@$$"))
        'httpWebRequest.UseDefaultCredentials = True
        'httpWebRequest.PreAuthenticate = True
        'httpWebRequest.Credentials = CredentialCache.DefaultCredentials
        If IO = "I" Then
            IO = 1
        ElseIf IO = "O" Then
            IO = 2
        End If
        Dim FinalData As JObject = New JObject
        'Dim streamWriter As StreamWriter = New StreamWriter(httpWebRequest.GetRequestStream)
        Dim jsons As String
        Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
            FinalData.Add("Site", LocCode)
            FinalData.Add("EMP", empid)
            FinalData.Add("Date", punchtime.ToString("yyyy-MM-dd HH:mm:ss"))
            FinalData.Add("IO", IO)
            FinalData.Add("RdrNum", DeviceId)
            FinalData.Add("RdrName", LocName)
            jsons = FinalData.ToString(Formatting.None)
            jsons = "[" & jsons & "]"
            'MessageBox.Show(jsons)
            streamWriter.Write(jsons)
            streamWriter.Flush()
            streamWriter.Close()
        End Using
        Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
        Using streamReader = New StreamReader(httpResponse.GetResponseStream())
            'Dim streamReader = New StreamReader(httpResponse.GetResponseStream)
            Dim result = streamReader.ReadToEnd()
            Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            sw.BaseStream.Seek(0, SeekOrigin.End)
            sw.WriteLine(vbCrLf & jsons & ", " & result & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
            sw.Flush()
            sw.Close()
        End Using
    End Function
    Public Shared Function HexToByte(ByVal hexString As String) As Byte()
        hexString = hexString.Replace(" ", "")
        Dim length As Integer = hexString.Length
        Dim upperBound As Integer = length \ 2
        If length Mod 2 = 0 Then
            upperBound -= 1
        Else
            hexString = "0" & hexString
        End If
        Dim bytes(upperBound) As Byte
        For i As Integer = 0 To upperBound
            'MsgBox(hexString.Substring(i * 2, 2))
            bytes(i) = Convert.ToByte(hexString.Substring(i * 2, 2), 16)
        Next
        Return bytes
    End Function
    Public Shared Function backUpTxtDat(ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal fileFormat As String, ByVal filePath As String, ByVal delete As Boolean)
        Dim sSql As String ' = "select * from MachineRawPunch"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            sSql = "Select M.*, E.EMPNAME from Machinerawpunch M, TblEmployee E where FORMAT(officepunch,'yyyy-MM-dd HH:mm:ss') between '" & startDate.ToString("yyyy-MM-dd 00:00:00") & "' and '" & endDate.ToString("yyyy-MM-dd 23:59:59") & "' and e.PRESENTCARDNO=m.CARDNO Order By officepunch"
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            sSql = "Select M.*, E.EMPNAME from Machinerawpunch M, TblEmployee E where officepunch between '" & startDate.ToString("yyyy-MM-dd 00:00:00") & "' and '" & endDate.ToString("yyyy-MM-dd 23:59:59") & "'  and e.PRESENTCARDNO=m.CARDNO Order By officepunch"
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            If fileFormat = "TEXT" Then
                Dim mstrFile_Name As String = filePath & "\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
                Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
                objWriter.WriteLine("No" & vbTab & "Mchn" & vbTab & "EnNo" & vbTab & "Name" & vbTab & "Mode" & vbTab & "IOMd" & vbTab & "DateTime")
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim No As String = (i + 1).ToString("000000")
                    Dim Mchn As String = ds.Tables(0).Rows(i).Item("MC_NO").ToString.Trim
                    If Mchn = "" Then
                        Mchn = "0"
                    End If
                    Dim EnNo As String '= ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim
                    If IsNumeric(ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim) Then
                        EnNo = Convert.ToDouble(ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim).ToString("0000000000")
                    Else
                        EnNo = ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim.PadLeft(10)
                    End If
                    Dim Name As String = ds.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim.PadRight(32)
                    Dim Mode As String = "1"
                    Dim IOMd As String = "1"
                    Dim OfficeDateTime As String = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString.Trim).ToString("yyyy/MM/dd HH:mm:ss")
                    objWriter.WriteLine(No & vbTab & Mchn & vbTab & EnNo & vbTab & Name & vbTab & Mode & vbTab & IOMd & vbTab & OfficeDateTime)
                Next
                objWriter.Close()
            ElseIf fileFormat = "DAT" Then
                Dim mstrFile_Name As String = filePath & "\" & Now.ToString("yyyyMMddHHmmss") & ".dat"
                Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim EnNo As String '= ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim
                    If IsNumeric(ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim) Then
                        EnNo = Convert.ToDouble(ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim).ToString.PadLeft(9)
                    Else
                        EnNo = ds.Tables(0).Rows(i).Item("CARDNO").ToString.Trim.PadLeft(9)
                    End If
                    Dim OfficeDateTime As String = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss")
                    objWriter.WriteLine(EnNo & vbTab & OfficeDateTime & vbTab & "1" & vbTab & "0" & vbTab & "1" & vbTab & "0")
                Next
                objWriter.Close()
            End If
            If delete = True Then
                If Common.servername = "" Then
                    Dim cmd As OleDbCommand
                    sSql = "delete from MachineRawPunch where FORMAT(officepunch,'yyyy-MM-dd HH:mm:ss') between '" & startDate.ToString("yyyy-MM-dd 00:00:00") & "' and '" & endDate.ToString("yyyy-MM-dd 23:59:59") & "'"
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd = New OleDbCommand(sSql, Common.con1)
                    cmd.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    Dim cmd As SqlCommand
                    sSql = "delete from MachineRawPunch where officepunch between '" & startDate.ToString("yyyy-MM-dd 00:00:00") & "' and '" & endDate.ToString("yyyy-MM-dd 23:59:59") & "'"
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If


            End If
        End If
    End Function
    Public Function initClientEF45(ByVal ipAddress As String) As Client
        Dim serialNumber As String = ""
        Dim checkSerialEnabled As Boolean = False
        Dim _client As Client = Nothing
        _client = New Client(ipAddress, serialNumber, checkSerialEnabled)
        ' Already generated client is unmanaged (Don't need to delete client before generating again)
        'AddHandler _client.CaptureCompleted, AddressOf Me._client_CaptureCompleted
        'AddHandler _client.CaptureTimedOut, AddressOf Me._client_CaptureTimedOut
        'AddHandler _client.CaptureError, AddressOf Me._client_CaptureError
        'AddHandler _client.CardReadCompleted, AddressOf Me._client_CardReadCompleted
        'AddHandler _client.CardReadTimedOut, AddressOf Me._client_CardReadTimedOut
        'AddHandler _client.CardReadError, AddressOf Me._client_CardReadError
        'AddHandler _client.UploadCompleted, AddressOf Me._client_UploadCompleted
        'AddHandler _client.UploadError, AddressOf Me._client_UploadError
        'AddHandler _client.ProgressStatus, AddressOf Me._client_ProgressStatus
        'AddHandler _client.MatchLog, AddressOf Me._client_MatchLog
        'AddHandler _client.MatchData, AddressOf Me._client_MatchData
        'AddHandler _client.DoorStatus, AddressOf Me._client_DoorStatus
        'AddHandler _client.PreviewImage, AddressOf Me._client_PreviewImage
        Return _client

    End Function
    Public Shared Function AmtAsText(mAmount As Double) As String
        Dim outarray(99) As String, RetString As String
        Dim intpart As Long, frpart As Integer

        outarray(1) = "One"
        outarray(2) = "Two"
        outarray(3) = "Three"
        outarray(4) = "Four"
        outarray(5) = "Five"
        outarray(6) = "Six"
        outarray(7) = "Seven"
        outarray(8) = "Eight"
        outarray(9) = "Nine"
        outarray(10) = "Ten"
        outarray(11) = "Eleven"
        outarray(12) = "Twelve"
        outarray(13) = "Thirteen"
        outarray(14) = "Fourteen"
        outarray(15) = "Fifteen"
        outarray(16) = "Sixteen"
        outarray(17) = "Seventeen"
        outarray(18) = "Eighteen"
        outarray(19) = "Nineteen"
        outarray(20) = "Twenty"
        outarray(21) = "Twenty One"
        outarray(22) = "Twenty Two"
        outarray(23) = "Twenty Three"
        outarray(24) = "Twenty Four"
        outarray(25) = "Twenty Five"
        outarray(26) = "Twenty Six"
        outarray(27) = "Twenty Seven"
        outarray(28) = "Twenty Eight"
        outarray(29) = "Twenty Nine"
        outarray(30) = "Thirty"
        outarray(31) = "Thirty One"
        outarray(32) = "Thirty Two"
        outarray(33) = "Thirty Three"
        outarray(34) = "Thirty Four"
        outarray(35) = "Thirty Five"
        outarray(36) = "Thirty Six"
        outarray(37) = "Thirty Seven"
        outarray(38) = "Thirty Eight"
        outarray(39) = "Thirty Nine"
        outarray(40) = "Fourty"
        outarray(41) = "Forty One"
        outarray(42) = "Forty Two"
        outarray(43) = "Forty Three"
        outarray(44) = "Forty Four"
        outarray(45) = "Forty Five"
        outarray(46) = "Forty Six"
        outarray(47) = "Forty Seven"
        outarray(48) = "Forty Eight"
        outarray(49) = "Forty Nine"
        outarray(50) = "Fifty"
        outarray(51) = "Fifty One"
        outarray(52) = "Fifty Two"
        outarray(53) = "Fifty Three"
        outarray(54) = "Fifty Four"
        outarray(55) = "Fifty Five"
        outarray(56) = "Fifty Six"
        outarray(57) = "Fifty Seven"
        outarray(58) = "Fifty Eight"
        outarray(59) = "Fifty Nine"
        outarray(60) = "Sixty"
        outarray(61) = "Sixty One"
        outarray(62) = "Sixty Two"
        outarray(63) = "Sixty Three"
        outarray(64) = "Sixty Four"
        outarray(65) = "Sixty Five"
        outarray(66) = "Sixty Six"
        outarray(67) = "Sixty Seven"
        outarray(68) = "Sixty Eight"
        outarray(69) = "Sixty Nine"
        outarray(70) = "Seventy"
        outarray(71) = "Seventy One"
        outarray(72) = "Seventy Two"
        outarray(73) = "Seventy Three"
        outarray(74) = "Seventy Four"
        outarray(75) = "Seventy Five"
        outarray(76) = "Seventy Six"
        outarray(77) = "Seventy Seven"
        outarray(78) = "Seventy Eight"
        outarray(79) = "Seventy Nine"
        outarray(80) = "Eighty"
        outarray(81) = "Eighty One"
        outarray(82) = "Eighty Two"
        outarray(83) = "Eighty Three"
        outarray(84) = "Eighty Four"
        outarray(85) = "Eighty Five"
        outarray(86) = "Eighty Six"
        outarray(87) = "Eighty Seven"
        outarray(88) = "Eighty Eight"
        outarray(89) = "Eighty Nine"
        outarray(90) = "Ninty"
        outarray(91) = "Ninty One"
        outarray(92) = "Ninty Two"
        outarray(93) = "Ninty Three"
        outarray(94) = "Ninty Four"
        outarray(95) = "Ninty Five"
        outarray(96) = "Ninty Six"
        outarray(97) = "Ninty Seven"
        outarray(98) = "Ninty Eight"
        outarray(99) = "Ninty Nine"

        intpart = Int(mAmount)
        frpart = (mAmount - intpart) * 100
        Select Case intpart
            Case Is < 100
                RetString = outarray(Str(intpart))
            Case Is < 1000
                RetString = outarray(Mid(Trim(Str(intpart)), 1, 1)) & " Hundred " & outarray(Mid(Trim(Str(intpart)), 2))
            Case Is < 10000
                RetString = outarray(Mid(Trim(Str(intpart)), 1, 1)) & " Thousand " & IIf((Mid(Trim(Str(intpart)), 2, 1)) <> 0, outarray(Mid(Trim(Str(intpart)), 2, 1)) & " Hundred ", "") & outarray(Mid(Trim(Str(intpart)), 3))
            Case Is < 100000
                RetString = outarray(Mid(Trim(Str(intpart)), 1, 2)) & " Thousand " & IIf((Mid(Trim(Str(intpart)), 3, 1)) <> 0, outarray(Mid(Trim(Str(intpart)), 3, 1)) & " Hundred ", "") & outarray(Mid(Trim(Str(intpart)), 4))
            Case Is < 1000000
                RetString = outarray(Mid(Trim(Str(intpart)), 1, 1)) & " Lac " & IIf((Mid(Trim(Str(intpart)), 2, 2)) <> 0, outarray(Mid(Trim(Str(intpart)), 2, 2)) & " Thousand ", "") & IIf((Mid(Trim(Str(intpart)), 4, 1)) <> 0, outarray(Mid(Trim(Str(intpart)), 4, 1)) & " Hundred ", "") & outarray(Mid(Trim(Str(intpart)), 5))
            Case Is < 10000000
                RetString = outarray(Mid(Trim(Str(intpart)), 1, 2)) & " Lac " & IIf((Mid(Trim(Str(intpart)), 3, 2)) <> 0, outarray(Mid(Trim(Str(intpart)), 3, 2)) & " Thousand ", "") & IIf((Mid(Trim(Str(intpart)), 5, 1)) <> 0, outarray(Mid(Trim(Str(intpart)), 5, 1)) & " Hundred ", "") & outarray(Mid(Trim(Str(intpart)), 6))
        End Select
        If frpart > 0 Then
            RetString = RetString & " And Paise " & outarray(Str(frpart))
        End If
        AmtAsText = RetString & " Only."
    End Function
    Public Shared Function LogPost(LogText As String)
        Try
            Dim conS As SqlConnection
            Dim conA As OleDbConnection
            Dim sSql As String = "insert into ApplicationLog (LogText, LogTime, LogedInUser) values ('" & LogText & "','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','" & Common.USER_R & "')"
            If Common.servername = "Access" Then
                conA = New OleDbConnection(Common.ConnectionString)
                Try
                    If conA.State <> ConnectionState.Open Then
                        conA.Open()
                    End If
                    Dim cmdA As OleDbCommand = New OleDbCommand(sSql, conA)
                    cmdA.ExecuteNonQuery()
                    If conA.State <> ConnectionState.Closed Then
                        conA.Close()
                    End If
                Catch ex As Exception
                    If conA.State <> ConnectionState.Closed Then
                        conA.Close()
                    End If
                End Try
            Else
                conS = New SqlConnection(Common.ConnectionString)
                Try
                    If conS.State <> ConnectionState.Open Then
                        conS.Open()
                    End If
                    Dim cmdS As SqlCommand = New SqlCommand(sSql, conS)
                    cmdS.ExecuteNonQuery()
                    If conS.State <> ConnectionState.Closed Then
                        conS.Close()
                    End If
                Catch ex As Exception
                    If conS.State <> ConnectionState.Closed Then
                        conS.Close()
                    End If
                End Try
            End If
        Catch ex As Exception

        End Try
    End Function

    
End Class
Class SimulateDateDiff
    Enum DateInterval
        Day
        DayOfYear
        Hour
        Minute
        Month
        Quarter
        Second
        Weekday
        WeekOfYear
        Year
    End Enum

    Friend Shared Function DateDiff(ByVal intervalType As DateInterval, ByVal dateOne As Date, ByVal dateTwo As Date) As Long
        Select Case (intervalType)
            Case DateInterval.Day, DateInterval.DayOfYear
                Dim spanForDays As System.TimeSpan = (dateTwo - dateOne)
                Return CType(spanForDays.TotalDays, Long)
            Case DateInterval.Hour
                Dim spanForHours As System.TimeSpan = (dateTwo - dateOne)
                Return CType(spanForHours.TotalHours, Long)
            Case DateInterval.Minute
                Dim spanForMinutes As System.TimeSpan = (dateTwo - dateOne)
                Return CType(spanForMinutes.TotalMinutes, Long)
            Case DateInterval.Month
                Return (((dateTwo.Year - dateOne.Year) _
                            * 12) _
                            + (dateTwo.Month - dateOne.Month))
            Case DateInterval.Quarter
                Dim dateOneQuarter As Long = CType(System.Math.Ceiling((dateOne.Month / 3)), Long)
                Dim dateTwoQuarter As Long = CType(System.Math.Ceiling((dateTwo.Month / 3)), Long)
                Return ((4 _
                            * (dateTwo.Year - dateOne.Year)) _
                            + (dateTwoQuarter - dateOneQuarter))
            Case DateInterval.Second
                Dim spanForSeconds As System.TimeSpan = (dateTwo - dateOne)
                Return CType(spanForSeconds.TotalSeconds, Long)
            Case DateInterval.Weekday
                Dim spanForWeekdays As System.TimeSpan = (dateTwo - dateOne)
                Return CType((spanForWeekdays.TotalDays / 7), Long)
            Case DateInterval.WeekOfYear
                Dim dateOneModified As Date = dateOne
                Dim dateTwoModified As Date = dateTwo

                While (dateTwoModified.DayOfWeek <> System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                    dateTwoModified = dateTwoModified.AddDays(-1)

                End While


                While (dateOneModified.DayOfWeek <> System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                    dateOneModified = dateOneModified.AddDays(-1)

                End While

                Dim spanForWeekOfYear As System.TimeSpan = (dateTwoModified - dateOneModified)
                Return CType((spanForWeekOfYear.TotalDays / 7), Long)
            Case DateInterval.Year
                Return (dateTwo.Year - dateOne.Year)
            Case Else
                Return 0
        End Select

    End Function
End Class
Class SimulateIsDate

    Friend Shared Function IsDate(ByVal expression As Object) As Boolean
        If (expression Is Nothing) Then
            Return False
        End If

        Dim testDate As Date
        Return System.DateTime.TryParse(expression.ToString, testDate)
    End Function
End Class
Class SimulateVal
    Friend Overloads Shared Function Val(ByVal expression As String) As Double
        If (expression Is Nothing) Then
            Return 0
        End If

        'try the entire string, then progressively smaller
        'substrings to simulate the behavior of VB's 'Val',
        'which ignores trailing characters after a recognizable value:
        Dim size As Integer = expression.Length
        Do While (size > 0)
            Dim testDouble As Double
            If Double.TryParse(expression.Substring(0, size), testDouble) Then
                Return testDouble
            End If

            size = (size - 1)
        Loop

        'no value is recognized, so return 0:
        Return 0
    End Function
    Friend Overloads Shared Function Val(ByVal expression As Object) As Double
        If (expression Is Nothing) Then
            Return 0
        End If

        Dim testDouble As Double
        If Double.TryParse(expression.ToString, testDouble) Then
            Return testDouble
        End If

        'VB's 'Val' function returns -1 for 'true':
        Dim testBool As Boolean
        If Boolean.TryParse(expression.ToString, testBool) Then
            If testBool = True Then
                Return -1
            Else
                Return 0
            End If
            'Return -1
        End If

        'VB's 'Val' function returns the day of the month for dates:
        Dim testDate As Date
        If System.DateTime.TryParse(expression.ToString, testDate) Then
            Return testDate.Day
        End If

        'no value is recognized, so return 0:
        Return 0
    End Function
    Friend Overloads Shared Function Val(ByVal expression As Char) As Integer
        Dim testInt As Integer
        If Integer.TryParse(expression.ToString, testInt) Then
            Return testInt
        Else
            Return 0
        End If

    End Function
End Class
Public Class hourToMinute

    Public Function hour(ByVal time As String) As Integer
        ' Dim values() As String = time.Split(Microsoft.VisualBasic.ChrW(58))
        Dim values() As String = time.Split(".")
        Dim h As Integer = Convert.ToInt32(values(0))
        Dim m As Integer
        Try
            'm = Convert.ToInt32(values(1)) 'old
            'new
            If values(1).Length = 1 Then
                m = Convert.ToInt32(values(1)) & "0"
            Else
                m = Convert.ToInt32(values(1))
            End If
            'new end
        Catch ex As Exception
            m = 0
        End Try

        Return ((h * 60) + m)
    End Function

    Public Function minutetohour(ByVal min As String) As String
        Dim latehours As Double = Convert.ToInt32(Math.Truncate(Convert.ToDouble(min) / 60))
        Dim latemin As Double = Convert.ToInt32(Math.Truncate(Convert.ToDouble(min) Mod 60))
        Dim totallate As String = (latehours.ToString + (":" + latemin.ToString))
        Return totallate
    End Function

    Public Function minutetohour1(ByVal min As String) As String
        Dim latehours As Double = Convert.ToInt32(Math.Truncate(Convert.ToDouble(min) / 60))
        Dim latemin As Double = Convert.ToInt32(Math.Truncate(Convert.ToDouble(min) Mod 60))
        Dim totallate As String = (latehours.ToString + ("." + latemin.ToString("00")))
        Return totallate
    End Function

    Public Function leaveAccural(ByVal time As String) As String
        Dim t() As String = time.ToString.Split(Microsoft.VisualBasic.ChrW(46))
        Dim f As String = Nothing
        Dim length As Integer = Convert.ToInt32(t(0).Length)
        If (length = 1) Then
            f = ("00" _
                        + (time.ToString + "00"))
        ElseIf (length = 2) Then
            f = ("0" _
                        + (time.ToString + "00"))
        ElseIf (length = 3) Then
            f = (time.ToString + "00")
        End If

        Return f
    End Function

    'Separate alternate offdays by comma
    Public Function ReverseNumber(ByVal number As Long) As String
        Dim digit As Long = (CType(number, Long) Mod 10)
        Dim rest As Long = (CType(number, Long) / 10)
        Dim o As Long = digit
        Dim a As String = Nothing

        While (rest > 0)
            digit = (rest Mod 10)
10:
            a = (a + ("," + digit.ToString))

        End While

        Return a.Substring(1)
    End Function

    'Upper case first latter
    Public Function UppercaseFirst(ByVal s As String) As String
        Dim a() As Char = s.ToCharArray
        a(0) = Char.ToUpper(a(0))
        Return New String(a)
    End Function

    Public Sub New()
        MyBase.New()
        '
        ' TODO: Add constructor logic here
        '
    End Sub
End Class
'for e.error text for grid validation
Public Class MyLocalizer
    Inherits DevExpress.XtraGrid.Localization.GridLocalizer
    Public Overrides Function GetLocalizedString(ByVal id As DevExpress.XtraGrid.Localization.GridStringId) As String
        If (id = DevExpress.XtraGrid.Localization.GridStringId.ColumnViewExceptionMessage) Then
            Return " Do you want to correct value?</size>"
        End If

        Return MyBase.GetLocalizedString(id)
    End Function
End Class


