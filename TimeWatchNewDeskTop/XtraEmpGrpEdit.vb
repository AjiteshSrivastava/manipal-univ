﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb

Public Class XtraEmpGrpEdit
    Dim GrpId As String
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info
    'Dim servername As String
    'Dim ConnectionString As String
    Dim ulf As UserLookAndFeel
    'Dim con As SqlConnection
    'Dim con1 As OleDbConnection
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim adap, adap1, adap2 As SqlDataAdapter
    Dim adapA, adapA1 As OleDbDataAdapter
    Dim ds, ds1, ds2 As DataSet
    'Dim x As String = ""
    Dim shiftlst As New List(Of String)()

    Public Sub New()
        InitializeComponent()
        'Dim fs As FileStream = New FileStream("db.txt", FileMode.Open, FileAccess.Read)
        'Dim sr As StreamReader = New StreamReader(fs)
        'Dim str As String
        'Dim str1() As String
        'Do While sr.Peek <> -1
        '    str = sr.ReadLine
        '    str1 = str.Split(",")
        '    servername = str1(0)
        'Loop
        'sr.Close()
        'fs.Close()

        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            'con1 = New OleDbConnection(ConnectionString)
            Me.TblShiftMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblShiftMaster1)
            GridControl1.DataSource = SSSDBDataSet.tblShiftMaster1
            GridControl2.DataSource = SSSDBDataSet.tblShiftMaster1
            GridLookUpEditDefaltShift.Properties.DataSource = SSSDBDataSet.tblShiftMaster1
            GridLookUpEdit2.Properties.DataSource = SSSDBDataSet.tblShiftMaster1
            GridLookUpEdit3.Properties.DataSource = SSSDBDataSet.tblShiftMaster1
            GridLookUpEdit4.Properties.DataSource = SSSDBDataSet.tblShiftMaster1
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            'con = New SqlConnection(ConnectionString)
            TblShiftMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblShiftMasterTableAdapter.Fill(Me.SSSDBDataSet.tblShiftMaster)
            GridControl1.DataSource = SSSDBDataSet.tblShiftMaster
            GridControl2.DataSource = SSSDBDataSet.tblShiftMaster
            GridLookUpEditDefaltShift.Properties.DataSource = SSSDBDataSet.tblShiftMaster
            GridLookUpEdit2.Properties.DataSource = SSSDBDataSet.tblShiftMaster
            GridLookUpEdit3.Properties.DataSource = SSSDBDataSet.tblShiftMaster
            GridLookUpEdit4.Properties.DataSource = SSSDBDataSet.tblShiftMaster
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 9))
        Common.SetGridFont(GridView2, New Font("Tahoma", 9))
    End Sub
    Private Sub XtraEmpGrpEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'x = ""
        shiftlst.Clear()
        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            'con1 = New OleDbConnection(ConnectionString)
            Me.TblShiftMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblShiftMaster1)
            GridControl1.DataSource = SSSDBDataSet.tblShiftMaster1
            GridControl2.DataSource = SSSDBDataSet.tblShiftMaster1
            GridLookUpEditDefaltShift.Properties.DataSource = SSSDBDataSet.tblShiftMaster1
            GridLookUpEditMultiShif.Properties.DataSource = SSSDBDataSet.tblShiftMaster1
            GridLookUpEdit2.Properties.DataSource = SSSDBDataSet.tblShiftMaster1
            GridLookUpEdit3.Properties.DataSource = SSSDBDataSet.tblShiftMaster1
            GridLookUpEdit4.Properties.DataSource = SSSDBDataSet.tblShiftMaster1
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            'con = New SqlConnection(ConnectionString)
            TblShiftMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblShiftMasterTableAdapter.Fill(Me.SSSDBDataSet.tblShiftMaster)
            GridControl1.DataSource = SSSDBDataSet.tblShiftMaster
            GridControl2.DataSource = SSSDBDataSet.tblShiftMaster
            GridLookUpEditDefaltShift.Properties.DataSource = SSSDBDataSet.tblShiftMaster
            GridLookUpEditMultiShif.Properties.DataSource = SSSDBDataSet.tblShiftMaster
            GridLookUpEdit2.Properties.DataSource = SSSDBDataSet.tblShiftMaster
            GridLookUpEdit3.Properties.DataSource = SSSDBDataSet.tblShiftMaster
            GridLookUpEdit4.Properties.DataSource = SSSDBDataSet.tblShiftMaster
        End If

        LabelControlETOP.Text = "End Time for Out punch(Next Date)" & vbCrLf & "for RTC Employee with Multiple Punch"
        LabelControlMarkWO.Text = "Mark WO as Absent when No of" & vbCrLf & "Present<No of Present for WO"

        Dim xe As XtraEmployeeGroup = New XtraEmployeeGroup
        GrpId = XtraEmployeeGroup.GpId
        'MsgBox(GrpId)

        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraEmpGrpEdit).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")
        'TabNavigationPage1.PageText = Common.res_man.GetString("timeoffice", Common.cul)
        'TabNavigationPage2.PageText = Common.res_man.GetString("shiftwoff", Common.cul)

        TabPane1.SelectedPage = TabNavigationPage1
        If GrpId.Length = 0 Then
            SetDefaultValue()
        Else
            SetFormValue()
        End If


        If CheckEdit2.Checked = True Then 'single punch only
            GroupControl3.Enabled = True
        Else
            GroupControl3.Enabled = False
        End If

        If ComboBoxShiftType.SelectedItem = "Rotational" Then  'shift type
            PopupContainerEdit1.Enabled = True
            GridLookUpEdit3.Enabled = True
            GridLookUpEdit4.Enabled = True
            SimpleButton3.Enabled = True
            SimpleButton4.Enabled = True

            TextEditRotation.Enabled = True
            TextEdit10.Enabled = True
            TextEdit11.Enabled = True
        ElseIf ComboBoxShiftType.SelectedItem = "Ignore" Or ComboBoxShiftType.SelectedItem = "Flexi" Then
            PopupContainerEdit1.Enabled = False
            GridLookUpEdit3.Enabled = False
            GridLookUpEdit4.Enabled = False
            SimpleButton3.Enabled = False
            SimpleButton4.Enabled = False

            TextEditRotation.Enabled = False
            GridLookUpEditDefaltShift.Enabled = False
            TextEdit10.Enabled = False
            TextEdit11.Enabled = False
        Else
            PopupContainerEdit1.Enabled = False
            GridLookUpEdit3.Enabled = False
            GridLookUpEdit4.Enabled = False
            SimpleButton3.Enabled = False
            SimpleButton4.Enabled = False

            TextEditRotation.Enabled = False
            GridLookUpEditDefaltShift.Enabled = True
            TextEdit10.Enabled = False
            TextEdit11.Enabled = False
        End If

        If ToggleSwitch5.IsOn = True Then 'overtime
            TextEdit9.Enabled = True
        Else
            TextEdit9.Enabled = False
        End If

        If ToggleSwitchAutoShift.IsOn = True Then ' auto shift
            PopupContainerEdit2.Enabled = True
        Else
            PopupContainerEdit2.Enabled = False
        End If

        If ComboBoxEdit3.SelectedItem = "NONE" Then  'second week off
            ComboBoxEdit4.Enabled = False
            GridLookUpEdit2.Enabled = False
            GroupControl7.Enabled = False
        Else
            ComboBoxEdit4.Enabled = True
            GridLookUpEdit2.Enabled = True
            GroupControl7.Enabled = True
        End If

        If ComboBoxEdit4.SelectedItem = "Half" Then
            GridLookUpEdit2.Enabled = True
        Else
            GridLookUpEdit2.Enabled = False
        End If

        If ComboBoxEdit4.EditValue = "Half" Then
            GridLookUpEdit2.Enabled = True
        ElseIf ComboBoxEdit4.EditValue = "Full" Then
            GridLookUpEdit2.Enabled = False
        End If
    End Sub
    Private Sub CheckEdit2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdit2.CheckedChanged

        If CheckEdit2.Checked = True Then
            GroupControl3.Enabled = True
        Else
            GroupControl3.Enabled = False
        End If
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonCancel.Click
        Me.Close()
    End Sub
    Private Sub PopupContainerEdit1_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit1.QueryPopUp
        Dim val As Object = PopupContainerEdit1.EditValue
        If (val Is Nothing) Then
            GridView1.ClearSelection()
        Else
            'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
                Dim rowHandle As Integer = GridView1.LocateByValue("SHIFT", text)
                GridView1.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        Dim selectedRows() As Integer = GridView1.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            'Dim f As XtraEmpGroupEdit = CType(GridView1.GetRow(selectionRow), XtraEmpGroupEdit)
            'Dim f As GridView1.SSSDBDataSetTableAdapters.row = CType(GridView1.GetRow(selectionRow), GridRow)
            Dim a As System.Data.DataRowView = GridView1.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("SHIFT"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEdit2_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit2.QueryPopUp
        Dim val As Object = PopupContainerEdit2.EditValue
        If (val Is Nothing) Then
            GridView2.ClearSelection()
        Else
            'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                Else
                    text = text.Trim
                End If
                Dim rowHandle As Integer = GridView2.LocateByValue("SHIFT", text)
                GridView2.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub PopupContainerEdit2_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit2.QueryResultValue
        Dim selectedRows() As Integer = GridView2.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            'Dim f As XtraEmpGroupEdit = CType(GridView1.GetRow(selectionRow), XtraEmpGroupEdit)
            'Dim f As GridView1.SSSDBDataSetTableAdapters.row = CType(GridView1.GetRow(selectionRow), GridRow)
            Dim a As System.Data.DataRowView = GridView2.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("SHIFT"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub ComboBoxShiftType_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxShiftType.SelectedIndexChanged
        If ComboBoxShiftType.SelectedItem = "Rotational" Then
            PopupContainerEdit1.Enabled = True
            GridLookUpEdit3.Enabled = True
            GridLookUpEdit4.Enabled = True
            SimpleButton3.Enabled = True
            SimpleButton4.Enabled = True

            TextEditRotation.Enabled = True
            TextEdit10.Enabled = True
            TextEdit11.Enabled = True
            ToggleSwitchAutoShift.IsOn = False
            ToggleSwitchAutoShift.Enabled = False

            GridLookUpEdit3.Visible = True
            SimpleButton3.Visible = True
            TextEditRotation.Visible = True
            GridLookUpEdit4.Visible = True
            SimpleButton4.Visible = True
            GridLookUpEditMultiShif.Visible = False
            LabelControl18.Text = "Shifts for rotation"
            LabelControl53.Visible = False
            ToggleAllowCmpOffMlt.Visible = False
        ElseIf ComboBoxShiftType.SelectedItem = "Ignore" Or ComboBoxShiftType.SelectedItem = "Flexi" Then
            PopupContainerEdit1.Enabled = False
            GridLookUpEdit3.Enabled = False
            GridLookUpEdit4.Enabled = False
            SimpleButton3.Enabled = False
            SimpleButton4.Enabled = False

            TextEditRotation.Enabled = False
            GridLookUpEditDefaltShift.Enabled = False
            TextEdit10.Enabled = False
            TextEdit11.Enabled = False
            ToggleSwitchAutoShift.IsOn = False
            ToggleSwitchAutoShift.Enabled = False
            GridLookUpEditMultiShif.Visible = False
            LabelControl18.Text = "Shifts for rotation"
            LabelControl53.Visible = False
            LabelControl18.Visible = False
            ToggleAllowCmpOffMlt.Visible = False
        ElseIf ComboBoxShiftType.SelectedItem = "MultiShift" Then
            GridLookUpEditDefaltShift.Enabled = True
            GridLookUpEdit3.Visible = False
            SimpleButton3.Visible = False
            TextEditRotation.Visible = False
            GridLookUpEdit4.Visible = False
            SimpleButton4.Visible = False

            GridLookUpEditMultiShif.Visible = True
            LabelControl18.Text = "Second Shift"
            LabelControl53.Visible = True
            ToggleAllowCmpOffMlt.Visible = True
            ToggleSwitchAutoShift.IsOn = False
            ToggleSwitchAutoShift.Enabled = False
        Else
            PopupContainerEdit1.Enabled = False
            GridLookUpEdit3.Enabled = False
            GridLookUpEdit4.Enabled = False
            SimpleButton3.Enabled = False

            SimpleButton4.Enabled = False
            TextEditRotation.Enabled = False
            GridLookUpEditDefaltShift.Enabled = True
            TextEdit10.Enabled = False
            TextEdit11.Enabled = False
            ToggleSwitchAutoShift.Enabled = True

            GridLookUpEdit3.Visible = True
            SimpleButton3.Visible = True
            TextEditRotation.Visible = True
            GridLookUpEdit4.Visible = True
            SimpleButton4.Visible = True
            GridLookUpEditMultiShif.Visible = False
            LabelControl18.Text = "Shifts for rotation"
            LabelControl53.Visible = False
            ToggleAllowCmpOffMlt.Visible = False
            ToggleAllowCmpOffMlt.IsOn = False
        End If
    End Sub
    Private Sub ToggleSwitch5_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitch5.Toggled
        If ToggleSwitch5.IsOn = True Then
            TextEdit9.Enabled = True
        Else
            TextEdit9.Enabled = False
        End If
    End Sub
    Private Sub ToggleSwitchAutoShift_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitchAutoShift.Toggled
        If ToggleSwitchAutoShift.IsOn = True Then
            PopupContainerEdit2.Enabled = True
        Else
            PopupContainerEdit2.Enabled = False
        End If
    End Sub
    Private Sub ComboBoxEdit3_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit3.SelectedIndexChanged
        If ComboBoxEdit3.SelectedItem = "NONE" Then
            ComboBoxEdit4.Enabled = False
            GridLookUpEdit2.Enabled = False
            GroupControl7.Enabled = False
        Else
            If ComboBoxEdit3.SelectedItem = ComboBoxEdit2.SelectedItem Then
                XtraMessageBox.Show(ulf, "<size=10>First week off and second week off cannot be same</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboBoxEdit3.SelectedItem = "NONE"
                ComboBoxEdit4.Enabled = False
                GridLookUpEdit2.Enabled = False
                GroupControl7.Enabled = False
                CheckEdit8.Text = "I "
                CheckEdit9.Text = "II "
                CheckEdit10.Text = "III "
                CheckEdit11.Text = "IV "
                CheckEdit12.Text = "V "
            Else
                ComboBoxEdit4.Enabled = True
                GridLookUpEdit2.Enabled = True
                GroupControl7.Enabled = True
                CheckEdit8.Text = "I " & ComboBoxEdit3.SelectedItem
                CheckEdit9.Text = "II " & ComboBoxEdit3.SelectedItem
                CheckEdit10.Text = "III " & ComboBoxEdit3.SelectedItem
                CheckEdit11.Text = "IV " & ComboBoxEdit3.SelectedItem
                CheckEdit12.Text = "V " & ComboBoxEdit3.SelectedItem

                If ComboBoxEdit4.EditValue = "Half" Then
                    GridLookUpEdit2.Enabled = True
                ElseIf ComboBoxEdit4.EditValue = "Full" Then
                    GridLookUpEdit2.Enabled = False
                End If

            End If
        End If
    End Sub
    Private Sub ComboBoxEdit4_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit4.SelectedIndexChanged
        If ComboBoxEdit4.SelectedItem = "Half" Then
            GridLookUpEdit2.Enabled = True
        Else
            GridLookUpEdit2.Enabled = False
        End If
    End Sub
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        Dim GroupId As String = TextEdit1.Text.Trim
        Dim GroupName As String = TextEdit2.Text.Trim
        If GroupId = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Group Id cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextEdit1.Select()
            Exit Sub
        ElseIf GroupName = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Group Name cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextEdit2.Select()
            Exit Sub
        End If

        If ComboBoxShiftType.SelectedItem = "MultiShift" Then
            If GridLookUpEditMultiShif.EditValue.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Second Shift cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                GridLookUpEditMultiShif.Select()
                Exit Sub
            ElseIf GridLookUpEditDefaltShift.EditValue.Trim = GridLookUpEditMultiShif.EditValue.Trim Then
                XtraMessageBox.Show(ulf, "<size=10>Default Shift and Second Shift cannot be same</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                GridLookUpEditMultiShif.Select()
                Exit Sub
            End If
        End If
        Dim late() As String = TextEdit3.Text.Trim.Split(":")
        Dim PERMISLATEARRIVAL As Integer = late(0) * 60 + late(1)

        Dim early() As String = TextEdit4.Text.Trim.Split(":")
        Dim PERMISEARLYDEPRT As Integer = early(0) * 60 + early(1)

        Dim maxwork() As String = TextEdit5.Text.Trim.Split(":")
        Dim MAXDAYMIN As Double = maxwork(0) * 60 + maxwork(1)

        Dim ISROUNDTHECLOCKWORK As Char = ""
        If ToggleSwitch1.IsOn = True Then
            ISROUNDTHECLOCKWORK = "Y"
        Else
            ISROUNDTHECLOCKWORK = "N"
        End If

        Dim ISTIMELOSSALLOWED As Char = ""
        If ToggleSwitch2.IsOn = True Then
            ISTIMELOSSALLOWED = "Y"
        Else
            ISTIMELOSSALLOWED = "N"
        End If

        Dim ISHALFDAY As Char = ""
        If ToggleSwitch3.IsOn = True Then
            ISHALFDAY = "Y"
        Else
            ISHALFDAY = "N"
        End If

        Dim ISSHORT As Char = ""
        If ToggleSwitch4.IsOn = True Then
            ISSHORT = "Y"
        Else
            ISSHORT = "N"
        End If

        Dim MARKMISSASHALFDAY As Char = ""
        If ToggleSwitchMarkHlf.IsOn = True Then
            MARKMISSASHALFDAY = "Y"
        Else
            MARKMISSASHALFDAY = "N"
        End If

        Dim preMarkDur() As String = TextEdit6.Text.Trim.Split(":")
        Dim TIME As Integer = preMarkDur(0) * 60 + preMarkDur(1)  'present marking duration

        Dim maxWorkHalfD() As String = TextEdit7.Text.Trim.Split(":")
        Dim HALF As Integer = maxWorkHalfD(0) * 60 + maxWorkHalfD(1)  'Maximum Working Hours for half day

        Dim maxWorkShortD() As String = TextEdit8.Text.Trim.Split(":")
        Dim SHORTT As Integer = maxWorkShortD(0) * 60 + maxWorkShortD(1)  'Maximum Working Hours for Short day

        Dim ISPUNCHALL As Char = ""
        Dim INONLY As Char = ""
        Dim TWO As Char = ""
        Dim ISOUTWORK As Char = ""
        If CheckEdit1.Checked = True Then
            ISPUNCHALL = "N"
            INONLY = "N"
            TWO = "N"
            ISOUTWORK = "N"
        ElseIf CheckEdit2.Checked = True Then
            If CheckEdit6.Checked = True Then
                ISPUNCHALL = "Y"
                INONLY = "Y"
            ElseIf CheckEdit7.Checked = True Then
                ISPUNCHALL = "Y"
                INONLY = "O"
            End If
            TWO = "N"
            ISOUTWORK = "N"
        ElseIf CheckEdit3.Checked = True Then
            ISPUNCHALL = "Y"
            INONLY = "N"
            TWO = "Y"
            ISOUTWORK = "N"
        ElseIf CheckEdit4.Checked = True Then
            ISPUNCHALL = "Y"
            INONLY = "N"
            TWO = "N"
            ISOUTWORK = "N"
        ElseIf CheckEdit5.Checked = True Then
            ISPUNCHALL = "Y"
            INONLY = "N"
            TWO = "N"
            ISOUTWORK = "Y"
        End If

        Dim ISOT As Char = ""
        If ToggleSwitch5.IsOn = True Then
            ISOT = "Y"
        Else
            ISOT = "N"
        End If

        Dim OTRATE As String = TextEdit9.Text.Trim

        Dim ISOS As Char = ""
        If ToggleSwitch6.IsOn = True Then
            ISOS = "Y"
        Else
            ISOS = "N"
        End If

        'columns only made for reference
        Dim PunchRequiredInDay As Integer = 0
        If CheckEdit1.Checked = True Then
            PunchRequiredInDay = 1
        ElseIf CheckEdit2.Checked = True Then
            PunchRequiredInDay = 2
        ElseIf CheckEdit3.Checked = True Then
            PunchRequiredInDay = 3
        ElseIf CheckEdit4.Checked = True Then
            PunchRequiredInDay = 4
        ElseIf CheckEdit5.Checked = True Then
            PunchRequiredInDay = 5
        End If

        Dim SinglePunchOnly As Integer = 0
        If CheckEdit6.Checked = True Then
            SinglePunchOnly = 1
        ElseIf CheckEdit7.Checked = True Then
            SinglePunchOnly = 2
        End If
        'end columns only made for reference

        'Shift
        Dim SHIFTTYPE As Char = ""
        If ComboBoxShiftType.SelectedItem = "Fixed" Then
            SHIFTTYPE = "F"
        ElseIf ComboBoxShiftType.SelectedItem = "Rotational" Then
            SHIFTTYPE = "R"
        ElseIf ComboBoxShiftType.SelectedItem = "Ignore" Then
            SHIFTTYPE = "I"
        ElseIf ComboBoxShiftType.SelectedItem = "Flexi" Then
            SHIFTTYPE = "L"
        ElseIf ComboBoxShiftType.SelectedItem = "MultiShift" Then
            SHIFTTYPE = "M"
        End If

        Dim SHIFT As String = GridLookUpEditDefaltShift.EditValue.trim
        If SHIFT = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Shift cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            GridLookUpEditDefaltShift.Select()
            Exit Sub
        End If

        Dim SHIFTPATTERN As String = TextEditRotation.Text.Trim 'PopupContainerEdit1.EditValue
        Dim CDAYS As Double = 0
        If SHIFTTYPE = "R" And SHIFTPATTERN = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Select Shifts for rotation</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            PopupContainerEdit1.Select()
            Exit Sub
        End If
        If SHIFTTYPE = "R" And TextEdit11.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Shift Change After How Many Days cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextEdit11.Select()
            Exit Sub
        ElseIf SHIFTTYPE = "R" And TextEdit11.Text <> "" Then
            CDAYS = Convert.ToDouble(TextEdit11.Text)
        End If

        Dim SHIFTREMAINDAYS As Integer = 0
        If TextEdit10.Text.Trim = "" Then
            SHIFTREMAINDAYS = 0
        Else
            SHIFTREMAINDAYS = Convert.ToInt32(TextEdit10.Text)
        End If

        Dim ISAUTOSHIFT As Char = ""
        If ToggleSwitchAutoShift.IsOn = True Then
            ISAUTOSHIFT = "Y"
        Else
            ISAUTOSHIFT = "N"
        End If

        'logic to check default shift present in shifts for rotation
        If SHIFTTYPE = "R" Then
            Dim sftpattrn() As String = SHIFTPATTERN.Trim.Split(",")
            Dim count As Integer = 0
            For i As Integer = 0 To sftpattrn.Length - 1
                If sftpattrn(i).Trim = SHIFT Then
                    count = count + 1
                End If
            Next
            If count = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Default shift must be selected in Shifts for rotation</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                PopupContainerEdit1.Select()
                Exit Sub
            End If
        End If
        'end logic to check default shift present in shifts for rotation

        Dim AUTH_SHIFTS As String = PopupContainerEdit2.EditValue
        If ToggleSwitchAutoShift.IsOn = True And AUTH_SHIFTS = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Select Add Auto shifts</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            PopupContainerEdit2.Select()
            Exit Sub
        End If
        If ToggleSwitchAutoShift.IsOn = False Then
            AUTH_SHIFTS = ""
        End If
        'End shift

        'Week off
        Dim FIRSTOFFDAY As String = ComboBoxEdit2.EditValue.Substring(0, 3)
        Dim SECONDOFFDAY As String = ComboBoxEdit3.EditValue.Substring(0, 3)
        Dim SECONDOFFTYPE As Char = ""
        If ComboBoxEdit4.SelectedItem = "Full" Then
            SECONDOFFTYPE = "F"
        ElseIf ComboBoxEdit4.SelectedItem = "Half" Then
            SECONDOFFTYPE = "H"
        End If
        Dim HALFDAYSHIFT As String = GridLookUpEdit2.EditValue.trim
        If SECONDOFFTYPE = "H" And HALFDAYSHIFT = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Select Half Day Shift</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            GridLookUpEdit2.Select()
            Exit Sub
        End If
        Dim ALTERNATE_OFF_DAYS As String = ""
        If CheckEdit8.Checked = True Then
            ALTERNATE_OFF_DAYS = ALTERNATE_OFF_DAYS & "1"
        End If
        If CheckEdit9.Checked = True Then
            ALTERNATE_OFF_DAYS = ALTERNATE_OFF_DAYS & "2"
        End If
        If CheckEdit10.Checked = True Then
            ALTERNATE_OFF_DAYS = ALTERNATE_OFF_DAYS & "3"
        End If
        If CheckEdit11.Checked = True Then
            ALTERNATE_OFF_DAYS = ALTERNATE_OFF_DAYS & "4"
        End If
        If CheckEdit12.Checked = True Then
            ALTERNATE_OFF_DAYS = ALTERNATE_OFF_DAYS & "5"
        End If
        'End Week off


        'policies
        'Dim SETUPID As String = TxtSetRegNo.Text.Trim
        'Dim PERMISLATEARR As String = TxtPermisLateArr.Text.Trim
        'Dim PERMISEARLYDEP As String = TxtPermisEarlyDpt.Text.Trim
        Dim DUPLICATECHECKMIN As String = TxtDuplicateCheck.Text.Trim

        Dim S_END As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd ") & TxtInTimeForINPunch.Text.Trim & ":00")
        Dim S_OUT As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd ") & TxtEndTimeForOutPunch.Text.Trim & ":00")

        Dim ISOTOUTMINUSSHIFTENDTIME As String
        Dim ISOTWRKGHRSMINUSSHIFTHRS As String
        Dim ISOTEARLYCOMEPLUSLATEDEP As String
        If CheckEditOT1.Checked = True Then
            ISOTOUTMINUSSHIFTENDTIME = "Y"
            ISOTWRKGHRSMINUSSHIFTHRS = "N"
            ISOTEARLYCOMEPLUSLATEDEP = "N"
        ElseIf CheckEditOT2.Checked = True Then
            ISOTOUTMINUSSHIFTENDTIME = "N"
            ISOTWRKGHRSMINUSSHIFTHRS = "Y"
            ISOTEARLYCOMEPLUSLATEDEP = "N"
        ElseIf CheckEditOT3.Checked = True Then
            ISOTOUTMINUSSHIFTENDTIME = "N"
            ISOTWRKGHRSMINUSSHIFTHRS = "N"
            ISOTEARLYCOMEPLUSLATEDEP = "Y"
        End If

        Dim ISOTEARLYCOMING As String
        If ToggleOTisAllowedInEarlyComing.IsOn = True Then
            ISOTEARLYCOMING = "Y"
        Else
            ISOTEARLYCOMING = "N"
        End If

        'Dim StartRealTime As String
        'If ToggleRealTime.IsOn = True Then           
        '    StartRealTime = "Y"
        'Else
        '    StartRealTime = "N"
        'End If

        Dim OTEARLYDUR As String = TxtOtEarlyDur.Text.Trim
        Dim OTLATECOMINGDUR As String = TxtOtLateDur.Text.Trim
        Dim OTRESTRICTENDDUR As String = TxtOtRestrictDur.Text.Trim
        'MsgBox(TxtOtEarlyDur.Text.Trim)
        Dim OTEARLYDEPARTUREDUR As String = TxtOtEarlyDur.Text.Trim

        Dim DEDUCTWOOT As String = TxtDeductOTinWO.Text.Trim
        Dim DEDUCTHOLIDAYOT As String = TxtDeductOTinHLD.Text.Trim
        Dim ISPRESENTONWOPRESENT As String
        If ToggleIsPresentOnWeekOff.IsOn = True Then
            ISPRESENTONWOPRESENT = "Y"
        Else
            ISPRESENTONWOPRESENT = "N"
        End If
        Dim ISPRESENTONHLDPRESENT As String
        If ToggleIsPResentOnHLD.IsOn = True Then
            ISPRESENTONHLDPRESENT = "Y"
        Else
            ISPRESENTONHLDPRESENT = "N"
        End If
        Dim ISAUTOABSENT As String
        If ToggleIsAutoAbsentAllowed.IsOn = True Then
            ISAUTOABSENT = "Y"
        Else
            ISAUTOABSENT = "N"
        End If
        Dim AUTOSHIFT_LOW As String = TxtPermisEarlyMinAutoShift.Text.Trim
        Dim AUTOSHIFT_UP As String = TxtPermisLateMinAutoShift.Text.Trim
        'Dim ISAUTOSHIFT As String
        'If ToggleAutoShiftAllowed.IsOn = True Then
        '    ISAUTOSHIFT = "Y"
        'Else
        '    ISAUTOSHIFT = "N"
        'End If       
        Dim WOINCLUDE As String
        If ToggleWeekOffIncludeInDUtyRoster.IsOn = True Then
            WOINCLUDE = "Y"
        Else
            WOINCLUDE = "N"
        End If
        'Dim IsOutWork As String
        'If ToggleOutWorkAllowed.IsOn = True Then
        '    IsOutWork = "Y"
        'Else
        '    IsOutWork = "N"
        'End If
        Dim NightShiftFourPunch As String
        If ToggleFourPunchNight.IsOn = True Then
            NightShiftFourPunch = "Y"
        Else
            NightShiftFourPunch = "N"
        End If
        Dim LinesPerPage As String = "58"  'hardcoded       
        Dim OTROUND As String
        If ToggleRoundOverTime.IsOn = True Then
            OTROUND = "Y"
        Else
            OTROUND = "N"
        End If
        Dim PREWO As String = TxtNoOffpresentOnweekOff.Text.Trim
        Dim ISAWA As String
        If ToggleMarkAWAasAAA.IsOn = True Then
            ISAWA = "Y"
        Else
            ISAWA = "N"
        End If
        Dim ISPREWO As String
        If ToggleMarkWOasAbsent.IsOn = True Then
            ISPREWO = "Y"
        Else
            ISPREWO = "N"
        End If
        'Dim LeaveFinancialYear As String
        'If ToggleLeaveAsPerFinancialYear.IsOn = True Then
        '    LeaveFinancialYear = "Y"
        'Else
        '    LeaveFinancialYear = "N"
        'End If
        'Dim Online As String
        'If ToggleOnlineEvents.IsOn = True Then
        '    Online = "Y"
        'Else
        '    Online = "N"
        'End If
        'Dim AutoDownload As String
        'If ToggleDownloadAtStartUp.IsOn = True Then
        '    AutoDownload = "Y"
        'Else
        '    AutoDownload = "N"
        'End If
        Dim MIS As String
        If ToggleMarkMisAsAbsent.IsOn = True Then
            MIS = "Y"
        Else
            MIS = "N"
        End If
        Dim OwMinus As String
        If ToggleOutWorkMins.IsOn = True Then
            OwMinus = "Y"
        Else
            OwMinus = "N"
        End If
        ''Dim LateVerification As String
        'Dim SMSPassword As String = TxtPwd.Text.Trim
        'Dim SMSUserID As String = TxtUserId.Text.Trim
        'Dim SMSMessage As String = TxtMsg.Text.Trim
        'Dim BioPort As String = TextEditBioPort.Text.Trim
        'Dim ZKPort As String = TextEditZKPort.Text.Trim
        'Dim TWIR102Port As String = TextTWIR102Port.Text.Trim
        'Dim Canteen As String
        'Dim Visitor As String
        'If ToggleSwitchCanteen.IsOn = True Then
        '    Canteen = "Y"
        'Else
        '    Canteen = "N"
        'End If
        'If ToggleSwitchVisitor.IsOn = True Then
        '    Visitor = "Y"
        'Else
        '    Visitor = "N"
        'End If
        'Dim IsNepali As String = "N"
        'If ToggleSwitchIsNepali.IsOn = True Then
        '    IsNepali = "Y"
        'Else
        '    IsNepali = "N"
        'End If
        'Dim TimerDur As String = TextEditTimerDur.Text.Trim
        'If TextEditTimerDur.Text.Trim = "" Then
        '    TimerDur = 10
        'End If
        'Dim AutoDwnDur As String = TextEditAutoDwnDur.Text.Trim
        'If AutoDwnDur = "" Then
        '    AutoDwnDur = "0"
        'End If
        'end polices
        Dim MISCOMPOFF As String
        If ToggleAllowCmpOffMlt.IsOn Then
            MISCOMPOFF = "Y"
        Else
            MISCOMPOFF = "N"
        End If

        Dim ISCOMPOFF As String
        If ToggleCmpPOWPOH.IsOn Then
            ISCOMPOFF = "Y"
        Else
            ISCOMPOFF = "N"
        End If
        Dim MSHIFT As String = GridLookUpEditMultiShif.EditValue.trim

        Dim ISAW As String = ToggleAWasAA.EditValue
        Dim ISWA As String = ToggleWAasAA.EditValue
        Dim ISAWP As String = ToggleAWPasAAP.EditValue
        Dim ISPWA As String = TogglePWAasPAA.EditValue

        If GrpId = "" Then
            'insert
            If Common.servername = "Access" Then

                adapA = New OleDbDataAdapter("select GroupId from EmployeeGroup where GroupId = '" & GroupId & "' ", Common.con1)
                ds1 = New DataSet
                adapA.Fill(ds1)
                If ds1.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Dupplicate Group Id</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit1.Select()
                    Exit Sub
                End If

                adapA = New OleDbDataAdapter("select GroupId from EmployeeGroup where GroupName = '" & GroupName & "' ", Common.con1)
                ds1 = New DataSet
                adapA.Fill(ds1)
                If ds1.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Dupplicate Group Name</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit2.Select()
                    Exit Sub
                End If
                Dim sSql1 As String = "INSERT INTO EmployeeGroup (GroupId,GroupName,PERMISLATEARRIVAL,PERMISEARLYDEPRT,MAXDAYMIN,ISROUNDTHECLOCKWORK,ISTIMELOSSALLOWED,ISHALFDAY,ISSHORT,TIME1,HALF,SHORT1,ISPUNCHALL,INONLY,TWO,ISOUTWORK,ISOT,OTRATE,ISOS,SHIFTTYPE,ISAUTOSHIFT,SHIFTPATTERN,CDAYS,SHIFTREMAINDAYS,AUTH_SHIFTS,FIRSTOFFDAY,SECONDOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,ALTERNATE_OFF_DAYS,PunchRequiredInDay,SinglePunchOnly,LastModifiedBy,LastModifiedDate,SHIFT, MARKMISSASHALFDAY, " & _
                       "DUPLICATECHECKMIN,S_END,S_OUT,ISOTOUTMINUSSHIFTENDTIME,ISOTWRKGHRSMINUSSHIFTHRS,ISOTEARLYCOMEPLUSLATEDEP,ISOTEARLYCOMING,OTLATECOMINGDUR,OTRESTRICTENDDUR,OTEARLYDEPARTUREDUR,DEDUCTWOOT,DEDUCTHOLIDAYOT,ISPRESENTONWOPRESENT,ISPRESENTONHLDPRESENT,ISAUTOABSENT,AUTOSHIFT_LOW,AUTOSHIFT_UP,WOINCLUDE,NightShiftFourPunch,OTROUND,PREWO,ISAWA,ISPREWO,MIS,OwMinus,OTEARLYDUR, MISCOMPOFF, ISCOMPOFF, MSHIFT,ISAW,ISWA,ISAWP,ISPWA" & _
                       ") values('" & GroupId & "','" & GroupName & "','" & PERMISLATEARRIVAL & "','" & PERMISEARLYDEPRT & "','" & MAXDAYMIN & "','" & ISROUNDTHECLOCKWORK & "','" & ISTIMELOSSALLOWED & "','" & ISHALFDAY & "','" & ISSHORT & "','" & TIME & "','" & HALF & "','" & SHORTT & "','" & ISPUNCHALL & "','" & INONLY & "','" & TWO & "','" & ISOUTWORK & "','" & ISOT & "','" & OTRATE & "','" & ISOS & "','" & SHIFTTYPE & "','" & ISAUTOSHIFT & "','" & SHIFTPATTERN & "','" & CDAYS & "', " & _
                       "'" & SHIFTREMAINDAYS & "','" & AUTH_SHIFTS & "','" & FIRSTOFFDAY & "','" & SECONDOFFDAY & "','" & SECONDOFFTYPE & "','" & HALFDAYSHIFT & "','" & ALTERNATE_OFF_DAYS & "','" & PunchRequiredInDay & "','" & SinglePunchOnly & "','admin','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','" & SHIFT & "','" & MARKMISSASHALFDAY & "', " & _
                       "'" & DUPLICATECHECKMIN & "','" & S_END.ToString("yyyy-MM-dd HH:mm:ss") & "','" & S_OUT.ToString("yyyy-MM-dd HH:mm:ss") & "','" & ISOTOUTMINUSSHIFTENDTIME & "','" & ISOTWRKGHRSMINUSSHIFTHRS & "','" & ISOTEARLYCOMEPLUSLATEDEP & "','" & ISOTEARLYCOMING & "','" & OTLATECOMINGDUR & "','" & OTRESTRICTENDDUR & "','" & OTEARLYDEPARTUREDUR & "', " & _
                       "'" & DEDUCTWOOT & "','" & DEDUCTHOLIDAYOT & "','" & ISPRESENTONWOPRESENT & "','" & ISPRESENTONHLDPRESENT & "','" & ISAUTOABSENT & "','" & AUTOSHIFT_LOW & "','" & AUTOSHIFT_UP & "','" & WOINCLUDE & "','" & NightShiftFourPunch & "','" & OTROUND & "','" & PREWO & "','" & ISAWA & "', " & _
                       "'" & ISPREWO & "','" & MIS & "','" & OwMinus & "','" & OTEARLYDUR & "','" & MISCOMPOFF & "','" & ISCOMPOFF & "','" & MSHIFT & "','" & ISAW & "','" & ISWA & "','" & ISAWP & "','" & ISPWA & "')"

                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                'cmd1 = New OleDbCommand("INSERT INTO EmployeeGroup (GroupId,GroupName,PERMISLATEARRIVAL,PERMISEARLYDEPRT,MAXDAYMIN,ISROUNDTHECLOCKWORK,ISTIMELOSSALLOWED,ISHALFDAY,ISSHORT,TIME1,HALF,SHORT1,ISPUNCHALL,INONLY,TWO,ISOUTWORK,ISOT,OTRATE,ISOS,SHIFTTYPE,ISAUTOSHIFT,SHIFTPATTERN,CDAYS,SHIFTREMAINDAYS,AUTH_SHIFTS,FIRSTOFFDAY,SECONDOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,ALTERNATE_OFF_DAYS,PunchRequiredInDay,SinglePunchOnly,LastModifiedBy,LastModifiedDate,SHIFT, MARKMISSASHALFDAY) VALUES ('" & GroupId & "','" & GroupName & "','" & PERMISLATEARRIVAL & "','" & PERMISEARLYDEPRT & "','" & MAXDAYMIN & "','" & ISROUNDTHECLOCKWORK & "','" & ISTIMELOSSALLOWED & "','" & ISHALFDAY & "','" & ISSHORT & "','" & TIME & "','" & HALF & "','" & SHORTT & "','" & ISPUNCHALL & "','" & INONLY & "','" & TWO & "','" & ISOUTWORK & "','" & ISOT & "','" & OTRATE & "','" & ISOS & "','" & SHIFTTYPE & "','" & ISAUTOSHIFT & "','" & SHIFTPATTERN & "','" & CDAYS & "','" & SHIFTREMAINDAYS & "','" & AUTH_SHIFTS & "','" & FIRSTOFFDAY & "','" & SECONDOFFDAY & "','" & SECONDOFFTYPE & "','" & HALFDAYSHIFT & "','" & ALTERNATE_OFF_DAYS & "','" & PunchRequiredInDay & "','" & SinglePunchOnly & "','admin','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','" & SHIFT & "','" & MARKMISSASHALFDAY & "')", Common.con1)
                cmd1 = New OleDbCommand(sSql1, Common.con1)
                cmd1.ExecuteNonQuery()
                Common.con1.Close()
            Else
                adap = New SqlDataAdapter("select GroupId from EmployeeGroup where GroupId = '" & GroupId & "' ", Common.con)
                ds = New DataSet
                adap.Fill(ds)
                If ds.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Dupplicate Group Id</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit1.Select()
                    Exit Sub
                End If

                adap = New SqlDataAdapter("select GroupId from EmployeeGroup where GroupName = '" & GroupName & "' ", Common.con)
                ds = New DataSet
                adap.Fill(ds)
                If ds.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Dupplicate Group Name</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit2.Select()
                    Exit Sub
                End If



                'cmd = New SqlCommand("insert into EmployeeGroup (GroupId,GroupName,PERMISLATEARRIVAL,PERMISEARLYDEPRT,MAXDAYMIN,ISROUNDTHECLOCKWORK,ISTIMELOSSALLOWED,ISHALFDAY,ISSHORT,TIME,HALF,SHORT,ISPUNCHALL,INONLY,TWO,ISOUTWORK,ISOT,OTRATE,ISOS) values('" & GroupId & "','" & GroupName & "','" & PERMISLATEARRIVAL & "','" & PERMISEARLYDEPRT & "','" & MAXDAYMIN & "','" & ISROUNDTHECLOCKWORK & "','" & ISTIMELOSSALLOWED & "','" & ISHALFDAY & "','" & ISSHORT & "','" & TIME & "','" & HALF & "','" & SHORTT & "','" & ISPUNCHALL & "','" & INONLY & "','" & TWO & "','" & ISOUTWORK & "','" & ISOT & "','" & OTRATE & "','" & ISOS & "')", con)
                Dim sSql1 As String = "insert into EmployeeGroup (GroupId,GroupName,PERMISLATEARRIVAL,PERMISEARLYDEPRT,MAXDAYMIN,ISROUNDTHECLOCKWORK,ISTIMELOSSALLOWED,ISHALFDAY,ISSHORT,TIME,HALF,SHORT,ISPUNCHALL,INONLY,TWO,ISOUTWORK,ISOT,OTRATE,ISOS,SHIFTTYPE,ISAUTOSHIFT,SHIFTPATTERN,CDAYS,SHIFTREMAINDAYS,AUTH_SHIFTS,FIRSTOFFDAY,SECONDOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,ALTERNATE_OFF_DAYS,PunchRequiredInDay,SinglePunchOnly,LastModifiedBy,LastModifiedDate,SHIFT, MARKMISSASHALFDAY, " & _
                        "DUPLICATECHECKMIN,S_END,S_OUT,ISOTOUTMINUSSHIFTENDTIME,ISOTWRKGHRSMINUSSHIFTHRS,ISOTEARLYCOMEPLUSLATEDEP,ISOTEARLYCOMING,OTLATECOMINGDUR,OTRESTRICTENDDUR,OTEARLYDEPARTUREDUR,DEDUCTWOOT,DEDUCTHOLIDAYOT,ISPRESENTONWOPRESENT,ISPRESENTONHLDPRESENT,ISAUTOABSENT,AUTOSHIFT_LOW,AUTOSHIFT_UP,WOINCLUDE,NightShiftFourPunch,OTROUND,PREWO,ISAWA,ISPREWO,MIS,OwMinus,OTEARLYDUR,MISCOMPOFF,ISCOMPOFF,MSHIFT,ISAW,ISWA,ISAWP,ISPWA" & _
                        ") values('" & GroupId & "','" & GroupName & "','" & PERMISLATEARRIVAL & "','" & PERMISEARLYDEPRT & "','" & MAXDAYMIN & "','" & ISROUNDTHECLOCKWORK & "','" & ISTIMELOSSALLOWED & "','" & ISHALFDAY & "','" & ISSHORT & "','" & TIME & "','" & HALF & "','" & SHORTT & "','" & ISPUNCHALL & "','" & INONLY & "','" & TWO & "','" & ISOUTWORK & "','" & ISOT & "','" & OTRATE & "','" & ISOS & "','" & SHIFTTYPE & "','" & ISAUTOSHIFT & "','" & SHIFTPATTERN & "','" & CDAYS & "', " & _
                        "'" & SHIFTREMAINDAYS & "','" & AUTH_SHIFTS & "','" & FIRSTOFFDAY & "','" & SECONDOFFDAY & "','" & SECONDOFFTYPE & "','" & HALFDAYSHIFT & "','" & ALTERNATE_OFF_DAYS & "','" & PunchRequiredInDay & "','" & SinglePunchOnly & "','admin','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','" & SHIFT & "','" & MARKMISSASHALFDAY & "', " & _
                        "'" & DUPLICATECHECKMIN & "','" & S_END.ToString("yyyy-MM-dd HH:mm:ss") & "','" & S_OUT.ToString("yyyy-MM-dd HH:mm:ss") & "','" & ISOTOUTMINUSSHIFTENDTIME & "','" & ISOTWRKGHRSMINUSSHIFTHRS & "','" & ISOTEARLYCOMEPLUSLATEDEP & "','" & ISOTEARLYCOMING & "','" & OTLATECOMINGDUR & "','" & OTRESTRICTENDDUR & "','" & OTEARLYDEPARTUREDUR & "', " & _
                        "'" & DEDUCTWOOT & "','" & DEDUCTHOLIDAYOT & "','" & ISPRESENTONWOPRESENT & "','" & ISPRESENTONHLDPRESENT & "','" & ISAUTOABSENT & "','" & AUTOSHIFT_LOW & "','" & AUTOSHIFT_UP & "','" & WOINCLUDE & "','" & NightShiftFourPunch & "','" & OTROUND & "','" & PREWO & "','" & ISAWA & "', " & _
                        "'" & ISPREWO & "','" & MIS & "','" & OwMinus & "','" & OTEARLYDUR & "','" & MISCOMPOFF & "','" & ISCOMPOFF & "','" & MSHIFT & "','" & ISAW & "','" & ISWA & "','" & ISAWP & "','" & ISPWA & "')"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
                Common.con.Close()

            End If
            Dim sSql As String = "insert into EmployeeGroupLeaveLedger (GroupId, L01_ADD, L02_ADD, L03_ADD, L04_ADD, L05_ADD, L06_ADD, L07_ADD, L08_ADD, L09_ADD, L10_ADD, L11_ADD, L12_ADD, L13_ADD, L14_ADD, L15_ADD, L16_ADD, L17_ADD, L18_ADD, L19_ADD, L20_ADD, ACC_FLAG, L01_CASH, L02_CASH, L03_CASH, L04_CASH, L05_CASH, L06_CASH, L07_CASH, L08_CASH, L09_CASH, L10_CASH, L11_CASH, L12_CASH, L13_CASH, L14_CASH, L15_CASH, L16_CASH, L17_CASH, L18_CASH, L19_CASH, L20_CASH, LYEAR) VALUES ('" & GroupId & "', " & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & ",'N', " & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0 & ",'" & Now.Year & "') "
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

            Common.LogPost("Employee Group Add; Group Id='" & GroupId)
            Common.SetEmpGrpId()
            Common.LoadGroupStruct()
            Me.Close()
        Else
            'update
            If XtraMessageBox.Show(ulf, "<size=10>All employees beloging to this group will be affected." & vbCrLf & "Are you sure to update this group?</size>", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                If Common.servername = "Access" Then
                    Dim sSql As String = "Update EmployeeGroup set GroupName = '" & GroupName & "', ISROUNDTHECLOCKWORK = '" & ISROUNDTHECLOCKWORK & "', PERMISLATEARRIVAL = '" & PERMISLATEARRIVAL & "',  PERMISEARLYDEPRT= '" & PERMISEARLYDEPRT & "',  MAXDAYMIN = '" & MAXDAYMIN & "',   ISTIMELOSSALLOWED = '" & ISTIMELOSSALLOWED & "',  ISHALFDAY = '" & ISHALFDAY & "',  ISSHORT = '" & ISSHORT & "',  TIME1 = '" & TIME & "',  HALF = '" & HALF & "',  SHORT1 = '" & SHORTT & "',  ISPUNCHALL = '" & ISPUNCHALL & "',  INONLY = '" & INONLY & "',  TWO = '" & TWO & "',  ISOUTWORK = '" & ISOUTWORK & "',  ISOT = '" & ISOT & "',  OTRATE = '" & OTRATE & "',  ISOS = '" & ISOS & "',  SHIFTTYPE = '" & SHIFTTYPE & "',  ISAUTOSHIFT = '" & ISAUTOSHIFT & "',  SHIFTPATTERN = '" & SHIFTPATTERN & "',  CDAYS = '" & CDAYS & "',  SHIFTREMAINDAYS = '" & SHIFTREMAINDAYS & "',  AUTH_SHIFTS = '" & AUTH_SHIFTS & "',  FIRSTOFFDAY = '" & FIRSTOFFDAY & "',  SECONDOFFDAY = '" & SECONDOFFDAY & "',  SECONDOFFTYPE = '" & SECONDOFFTYPE & "',  HALFDAYSHIFT = '" & HALFDAYSHIFT & "',  ALTERNATE_OFF_DAYS = '" & ALTERNATE_OFF_DAYS & "',  PunchRequiredInDay = '" & PunchRequiredInDay & "',  SinglePunchOnly = '" & SinglePunchOnly & "',  LastModifiedBy = 'admin',  LastModifiedDate = '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "',  SHIFT = '" & SHIFT & "', MARKMISSASHALFDAY='" & MARKMISSASHALFDAY & "', " & _
                    " DUPLICATECHECKMIN='" & DUPLICATECHECKMIN & "'," & _
                    " S_END='" & S_END.ToString("yyyy-MM-dd HH:mm:ss") & "'," & _
                    " S_OUT='" & S_OUT.ToString("yyyy-MM-dd HH:mm:ss") & "'," & _
                    " ISOTOUTMINUSSHIFTENDTIME='" & ISOTOUTMINUSSHIFTENDTIME & "'," & _
                    " ISOTWRKGHRSMINUSSHIFTHRS='" & ISOTWRKGHRSMINUSSHIFTHRS & "'," & _
                    " ISOTEARLYCOMEPLUSLATEDEP='" & ISOTEARLYCOMEPLUSLATEDEP & "'," & _
                    " ISOTEARLYCOMING='" & ISOTEARLYCOMING & "'," & _
                    " OTLATECOMINGDUR='" & OTLATECOMINGDUR & "'," & _
                    " OTRESTRICTENDDUR='" & OTRESTRICTENDDUR & "'," & _
                    " OTEARLYDEPARTUREDUR='" & OTEARLYDEPARTUREDUR & "'," & _
                    " DEDUCTWOOT='" & DEDUCTWOOT & "'," & _
                    " DEDUCTHOLIDAYOT='" & DEDUCTHOLIDAYOT & "'," & _
                    " ISPRESENTONWOPRESENT='" & ISPRESENTONWOPRESENT & "'," & _
                    " ISPRESENTONHLDPRESENT='" & ISPRESENTONHLDPRESENT & "'," & _
                    " ISAUTOABSENT='" & ISAUTOABSENT & "'," & _
                    " AUTOSHIFT_LOW='" & AUTOSHIFT_LOW & "'," & _
                    " AUTOSHIFT_UP='" & AUTOSHIFT_UP & "'," & _
                    " WOINCLUDE='" & WOINCLUDE & "'," & _
                    " NightShiftFourPunch='" & NightShiftFourPunch & "'," & _
                    " OTROUND='" & OTROUND & "'," & _
                    " PREWO='" & PREWO & "'," & _
                    " ISAWA='" & ISAWA & "'," & _
                    " ISPREWO='" & ISPREWO & "'," & _
                    " MIS='" & MIS & "'," & _
                    " OwMinus='" & OwMinus & "'," & _
                    " OTEARLYDUR='" & OTEARLYDUR & "', " & _
                    " MISCOMPOFF='" & MISCOMPOFF & "', " & _
                    " ISCOMPOFF = '" & ISCOMPOFF & "', " & _
                    " MSHIFT='" & MSHIFT & "', " & _
                    " ISAW='" & ISAW & "', " & _
                    " ISWA='" & ISWA & "', " & _
                    " ISAWP='" & ISAWP & "', " & _
                    " ISPWA='" & ISPWA & "' " & _
                    " where GroupId = '" & GrpId & "'"

                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    'cmd1 = New OleDbCommand("Update EmployeeGroup set GroupName = '" & GroupName & "', ISROUNDTHECLOCKWORK = '" & ISROUNDTHECLOCKWORK & "', PERMISLATEARRIVAL = '" & PERMISLATEARRIVAL & "',  PERMISEARLYDEPRT= '" & PERMISEARLYDEPRT & "',  MAXDAYMIN = '" & MAXDAYMIN & "',   ISTIMELOSSALLOWED = '" & ISTIMELOSSALLOWED & "',  ISHALFDAY = '" & ISHALFDAY & "',  ISSHORT = '" & ISSHORT & "',  TIME1 = '" & TIME & "',  HALF = '" & HALF & "',  SHORT1 = '" & SHORTT & "',  ISPUNCHALL = '" & ISPUNCHALL & "',  INONLY = '" & INONLY & "',  TWO = '" & TWO & "',  ISOUTWORK = '" & ISOUTWORK & "',  ISOT = '" & ISOT & "',  OTRATE = '" & OTRATE & "',  ISOS = '" & ISOS & "',  SHIFTTYPE = '" & SHIFTTYPE & "',  ISAUTOSHIFT = '" & ISAUTOSHIFT & "',  SHIFTPATTERN = '" & SHIFTPATTERN & "',  CDAYS = '" & CDAYS & "',  SHIFTREMAINDAYS = '" & SHIFTREMAINDAYS & "',  AUTH_SHIFTS = '" & AUTH_SHIFTS & "',  FIRSTOFFDAY = '" & FIRSTOFFDAY & "',  SECONDOFFDAY = '" & SECONDOFFDAY & "',  SECONDOFFTYPE = '" & SECONDOFFTYPE & "',  HALFDAYSHIFT = '" & HALFDAYSHIFT & "',  ALTERNATE_OFF_DAYS = '" & ALTERNATE_OFF_DAYS & "',  PunchRequiredInDay = '" & PunchRequiredInDay & "',  SinglePunchOnly = '" & SinglePunchOnly & "',  LastModifiedBy = 'admin',  LastModifiedDate = '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "',  SHIFT = '" & SHIFT & "', MARKMISSASHALFDAY='" & MARKMISSASHALFDAY & "' where GroupId = '" & GrpId & "'", Common.con1)
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()

                    adapA1 = New OleDbDataAdapter("select PAYCODE from TblEmployee where EmployeeGroupId = '" & GrpId & "' ", Common.con1)
                    ds2 = New DataSet
                    adapA1.Fill(ds2)
                    If ds2.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To ds2.Tables(0).Rows.Count - 1
                            'If Common.con1.State <> ConnectionState.Open Then
                            '    Common.con1.Open()
                            'End If
                            cmd1 = New OleDbCommand("Update tblEmployeeShiftMaster set MSHIFT='" & MSHIFT & "', ISROUNDTHECLOCKWORK = '" & ISROUNDTHECLOCKWORK & "', PERMISLATEARRIVAL = " & PERMISLATEARRIVAL & ",  PERMISEARLYDEPRT= " & PERMISEARLYDEPRT & ",  MAXDAYMIN = " & MAXDAYMIN & ", ISTIMELOSSALLOWED = '" & ISTIMELOSSALLOWED & "',  ISHALFDAY = '" & ISHALFDAY & "',  ISSHORT = '" & ISSHORT & "',  TIME1 = " & TIME & ",  HALF = " & HALF & ",  SHORT1 = " & SHORTT & ",  ISPUNCHALL = '" & ISPUNCHALL & "',  INONLY = '" & INONLY & "',  TWO = '" & TWO & "',  ISOUTWORK = '" & ISOUTWORK & "',  ISOT = '" & ISOT & "',  OTRATE = '" & OTRATE & "',  ISOS = '" & ISOS & "',  SHIFTTYPE = '" & SHIFTTYPE & "',  ISAUTOSHIFT = '" & ISAUTOSHIFT & "',  SHIFTPATTERN = '" & SHIFTPATTERN & "',  CDAYS = " & CDAYS & ",  SHIFTREMAINDAYS = " & SHIFTREMAINDAYS & ",  AUTH_SHIFTS = '" & AUTH_SHIFTS & "',  FIRSTOFFDAY = '" & FIRSTOFFDAY & "',  SECONDOFFDAY = '" & SECONDOFFDAY & "',  SECONDOFFTYPE = '" & SECONDOFFTYPE & "',  HALFDAYSHIFT = '" & HALFDAYSHIFT & "',  ALTERNATE_OFF_DAYS = '" & ALTERNATE_OFF_DAYS & "', SHIFT = '" & SHIFT & "', MARKMISSASHALFDAY='" & MARKMISSASHALFDAY & "' where PAYCODE = '" & ds2.Tables(0).Rows(i).Item("PAYCODE").ToString & "'", Common.con1)
                            cmd1.ExecuteNonQuery()
                            'Common.con1.Close()
                        Next
                    End If
                    Common.con1.Close()
                Else
                    Dim sSql As String = "Update EmployeeGroup set GroupName = '" & GroupName & "', ISROUNDTHECLOCKWORK = '" & ISROUNDTHECLOCKWORK & "', PERMISLATEARRIVAL = '" & PERMISLATEARRIVAL & "',  PERMISEARLYDEPRT= '" & PERMISEARLYDEPRT & "',  MAXDAYMIN = '" & MAXDAYMIN & "',   ISTIMELOSSALLOWED = '" & ISTIMELOSSALLOWED & "',  ISHALFDAY = '" & ISHALFDAY & "',  ISSHORT = '" & ISSHORT & "',  TIME = '" & TIME & "',  HALF = '" & HALF & "',  SHORT = '" & SHORTT & "',  ISPUNCHALL = '" & ISPUNCHALL & "',  INONLY = '" & INONLY & "',  TWO = '" & TWO & "',  ISOUTWORK = '" & ISOUTWORK & "',  ISOT = '" & ISOT & "',  OTRATE = '" & OTRATE & "',  ISOS = '" & ISOS & "',  SHIFTTYPE = '" & SHIFTTYPE & "',  ISAUTOSHIFT = '" & ISAUTOSHIFT & "',  SHIFTPATTERN = '" & SHIFTPATTERN & "',  CDAYS = '" & CDAYS & "',  SHIFTREMAINDAYS = '" & SHIFTREMAINDAYS & "',  AUTH_SHIFTS = '" & AUTH_SHIFTS & "',  FIRSTOFFDAY = '" & FIRSTOFFDAY & "',  SECONDOFFDAY = '" & SECONDOFFDAY & "',  SECONDOFFTYPE = '" & SECONDOFFTYPE & "',  HALFDAYSHIFT = '" & HALFDAYSHIFT & "',  ALTERNATE_OFF_DAYS = '" & ALTERNATE_OFF_DAYS & "',  PunchRequiredInDay = '" & PunchRequiredInDay & "',  SinglePunchOnly = '" & SinglePunchOnly & "',  LastModifiedBy = 'admin',  LastModifiedDate = '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "',  SHIFT = '" & SHIFT & "', MARKMISSASHALFDAY='" & MARKMISSASHALFDAY & "', " & _
                   " DUPLICATECHECKMIN='" & DUPLICATECHECKMIN & "'," & _
                   " S_END='" & S_END.ToString("yyyy-MM-dd HH:mm:ss") & "'," & _
                   " S_OUT='" & S_OUT.ToString("yyyy-MM-dd HH:mm:ss") & "'," & _
                   " ISOTOUTMINUSSHIFTENDTIME='" & ISOTOUTMINUSSHIFTENDTIME & "'," & _
                   " ISOTWRKGHRSMINUSSHIFTHRS='" & ISOTWRKGHRSMINUSSHIFTHRS & "'," & _
                   " ISOTEARLYCOMEPLUSLATEDEP='" & ISOTEARLYCOMEPLUSLATEDEP & "'," & _
                   " ISOTEARLYCOMING='" & ISOTEARLYCOMING & "'," & _
                   " OTLATECOMINGDUR='" & OTLATECOMINGDUR & "'," & _
                   " OTRESTRICTENDDUR='" & OTRESTRICTENDDUR & "'," & _
                   " OTEARLYDEPARTUREDUR='" & OTEARLYDEPARTUREDUR & "'," & _
                   " DEDUCTWOOT='" & DEDUCTWOOT & "'," & _
                   " DEDUCTHOLIDAYOT='" & DEDUCTHOLIDAYOT & "'," & _
                   " ISPRESENTONWOPRESENT='" & ISPRESENTONWOPRESENT & "'," & _
                   " ISPRESENTONHLDPRESENT='" & ISPRESENTONHLDPRESENT & "'," & _
                   " ISAUTOABSENT='" & ISAUTOABSENT & "'," & _
                   " AUTOSHIFT_LOW='" & AUTOSHIFT_LOW & "'," & _
                   " AUTOSHIFT_UP='" & AUTOSHIFT_UP & "'," & _
                   " WOINCLUDE='" & WOINCLUDE & "'," & _
                   " NightShiftFourPunch='" & NightShiftFourPunch & "'," & _
                   " OTROUND='" & OTROUND & "'," & _
                   " PREWO='" & PREWO & "'," & _
                   " ISAWA='" & ISAWA & "'," & _
                   " ISPREWO='" & ISPREWO & "'," & _
                   " MIS='" & MIS & "'," & _
                   " OwMinus='" & OwMinus & "'," & _
                   " OTEARLYDUR='" & OTEARLYDUR & "', " & _
                    " MISCOMPOFF='" & MISCOMPOFF & "', " & _
                    " ISCOMPOFF = '" & ISCOMPOFF & "', " & _
                    " MSHIFT = '" & MSHIFT & "', " & _
                    " ISAW='" & ISAW & "', " & _
                    " ISWA='" & ISWA & "', " & _
                    " ISAWP='" & ISAWP & "', " & _
                    " ISPWA='" & ISPWA & "' " & _
                   " where GroupId = '" & GrpId & "'"
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    'cmd = New SqlCommand("Update EmployeeGroup set GroupName = '" & GroupName & "', ISROUNDTHECLOCKWORK = '" & ISROUNDTHECLOCKWORK & "', PERMISLATEARRIVAL = '" & PERMISLATEARRIVAL & "',  PERMISEARLYDEPRT= '" & PERMISEARLYDEPRT & "',  MAXDAYMIN = '" & MAXDAYMIN & "',   ISTIMELOSSALLOWED = '" & ISTIMELOSSALLOWED & "',  ISHALFDAY = '" & ISHALFDAY & "',  ISSHORT = '" & ISSHORT & "',  TIME = '" & TIME & "',  HALF = '" & HALF & "',  SHORT = '" & SHORTT & "',  ISPUNCHALL = '" & ISPUNCHALL & "',  INONLY = '" & INONLY & "',  TWO = '" & TWO & "',  ISOUTWORK = '" & ISOUTWORK & "',  ISOT = '" & ISOT & "',  OTRATE = '" & OTRATE & "',  ISOS = '" & ISOS & "',  SHIFTTYPE = '" & SHIFTTYPE & "',  ISAUTOSHIFT = '" & ISAUTOSHIFT & "',  SHIFTPATTERN = '" & SHIFTPATTERN & "',  CDAYS = '" & CDAYS & "',  SHIFTREMAINDAYS = '" & SHIFTREMAINDAYS & "',  AUTH_SHIFTS = '" & AUTH_SHIFTS & "',  FIRSTOFFDAY = '" & FIRSTOFFDAY & "',  SECONDOFFDAY = '" & SECONDOFFDAY & "',  SECONDOFFTYPE = '" & SECONDOFFTYPE & "',  HALFDAYSHIFT = '" & HALFDAYSHIFT & "',  ALTERNATE_OFF_DAYS = '" & ALTERNATE_OFF_DAYS & "',  PunchRequiredInDay = '" & PunchRequiredInDay & "',  SinglePunchOnly = '" & SinglePunchOnly & "',  LastModifiedBy = 'admin',  LastModifiedDate = '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "',  SHIFT = '" & SHIFT & "', MARKMISSASHALFDAY='" & MARKMISSASHALFDAY & "' where GroupId = '" & GrpId & "'", Common.con)
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    'Common.con.Close()

                    adap2 = New SqlDataAdapter("select PAYCODE from TblEmployee where EmployeeGroupId = '" & GrpId & "' ", Common.con)
                    ds2 = New DataSet
                    adap2.Fill(ds2)
                    If ds2.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To ds2.Tables(0).Rows.Count - 1
                            'If Common.con.State <> ConnectionState.Open Then
                            '    Common.con.Open()
                            'End If
                            cmd = New SqlCommand("Update tblEmployeeShiftMaster set MSHIFT='" & MSHIFT & "', ISROUNDTHECLOCKWORK = '" & ISROUNDTHECLOCKWORK & "', PERMISLATEARRIVAL = '" & PERMISLATEARRIVAL & "',  PERMISEARLYDEPRT= '" & PERMISEARLYDEPRT & "',  MAXDAYMIN = '" & MAXDAYMIN & "',   ISTIMELOSSALLOWED = '" & ISTIMELOSSALLOWED & "',  ISHALFDAY = '" & ISHALFDAY & "',  ISSHORT = '" & ISSHORT & "',  TIME = '" & TIME & "',  HALF = '" & HALF & "',  SHORT = '" & SHORTT & "',  ISPUNCHALL = '" & ISPUNCHALL & "',  INONLY = '" & INONLY & "',  TWO = '" & TWO & "',  ISOUTWORK = '" & ISOUTWORK & "',  ISOT = '" & ISOT & "',  OTRATE = '" & OTRATE & "',  ISOS = '" & ISOS & "',  SHIFTTYPE = '" & SHIFTTYPE & "',  ISAUTOSHIFT = '" & ISAUTOSHIFT & "',  SHIFTPATTERN = '" & SHIFTPATTERN & "',  CDAYS = '" & CDAYS & "',  SHIFTREMAINDAYS = '" & SHIFTREMAINDAYS & "',  AUTH_SHIFTS = '" & AUTH_SHIFTS & "',  FIRSTOFFDAY = '" & FIRSTOFFDAY & "',  SECONDOFFDAY = '" & SECONDOFFDAY & "',  SECONDOFFTYPE = '" & SECONDOFFTYPE & "',  HALFDAYSHIFT = '" & HALFDAYSHIFT & "',  ALTERNATE_OFF_DAYS = '" & ALTERNATE_OFF_DAYS & "', SHIFT = '" & SHIFT & "', MARKMISSASHALFDAY='" & MARKMISSASHALFDAY & "' where PAYCODE = '" & ds2.Tables(0).Rows(i).Item("PAYCODE").ToString & "'", Common.con)
                            cmd.ExecuteNonQuery()
                            'Common.con.Close()
                        Next
                    End If
                    Common.con.Close()
                End If
            End If

            Common.LogPost("Employee Group Update; Group Id='" & GroupId)
            Common.SetEmpGrpId()
            Common.LoadGroupStruct()
            Me.Close()
        End If
    End Sub
    Private Sub SetFormValue()
        ds1 = New DataSet
        If Common.servername = "Access" Then
            Dim a As OleDbDataAdapter = New OleDbDataAdapter("select * from EmployeeGroup where GroupId = '" & GrpId & "' ", Common.con1)
            a.Fill(ds1)
        Else
            adap1 = New SqlDataAdapter("select * from EmployeeGroup where GroupId = '" & GrpId & "' ", Common.con)
            adap1.Fill(ds1)
        End If


        'Time Office Policy
        TextEdit1.Enabled = False
        TextEdit1.Text = ds1.Tables(0).Rows(0).Item("GroupId").ToString.Trim
        TextEdit2.Text = ds1.Tables(0).Rows(0).Item("GroupName").ToString.Trim
        TextEdit3.Text = (ds1.Tables(0).Rows(0).Item("PERMISLATEARRIVAL").ToString.Trim \ 60) & ":" & ds1.Tables(0).Rows(0).Item("PERMISLATEARRIVAL").ToString.Trim Mod 60
        TextEdit4.Text = (ds1.Tables(0).Rows(0).Item("PERMISEARLYDEPRT").ToString.Trim \ 60) & ":" & ds1.Tables(0).Rows(0).Item("PERMISEARLYDEPRT").ToString.Trim Mod 60
        TextEdit5.Text = (ds1.Tables(0).Rows(0).Item("MAXDAYMIN").ToString.Trim \ 60) & ":" & ds1.Tables(0).Rows(0).Item("MAXDAYMIN").ToString.Trim Mod 60
        If ds1.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString.Trim() = "Y" Then
            ToggleSwitch1.IsOn = True
        ElseIf ds1.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString.Trim() = "N" Then
            ToggleSwitch1.IsOn = False
        End If

        If ds1.Tables(0).Rows(0).Item("ISTIMELOSSALLOWED").ToString.Trim() = "Y" Then
            ToggleSwitch2.IsOn = True
        ElseIf ds1.Tables(0).Rows(0).Item("ISTIMELOSSALLOWED").ToString.Trim() = "N" Then
            ToggleSwitch2.IsOn = False
        End If

        If ds1.Tables(0).Rows(0).Item("ISHALFDAY").ToString.Trim() = "Y" Then
            ToggleSwitch3.IsOn = True
        ElseIf ds1.Tables(0).Rows(0).Item("ISHALFDAY").ToString.Trim() = "N" Then
            ToggleSwitch3.IsOn = False
        End If

        If ds1.Tables(0).Rows(0).Item("ISSHORT").ToString.Trim() = "Y" Then
            ToggleSwitch4.IsOn = True
        ElseIf ds1.Tables(0).Rows(0).Item("ISSHORT").ToString.Trim() = "N" Then
            ToggleSwitch4.IsOn = False
        End If

        If Common.servername = "Access" Then
            TextEdit6.Text = (ds1.Tables(0).Rows(0).Item("TIME1").ToString.Trim \ 60) & ":" & ds1.Tables(0).Rows(0).Item("TIME1").ToString.Trim Mod 60
            TextEdit8.Text = (ds1.Tables(0).Rows(0).Item("SHORT1").ToString.Trim \ 60) & ":" & ds1.Tables(0).Rows(0).Item("SHORT1").ToString.Trim Mod 60
        Else
            TextEdit6.Text = (ds1.Tables(0).Rows(0).Item("TIME").ToString.Trim \ 60) & ":" & ds1.Tables(0).Rows(0).Item("TIME").ToString.Trim Mod 60
            TextEdit8.Text = (ds1.Tables(0).Rows(0).Item("SHORT").ToString.Trim \ 60) & ":" & ds1.Tables(0).Rows(0).Item("SHORT").ToString.Trim Mod 60
        End If
        TextEdit7.Text = (ds1.Tables(0).Rows(0).Item("HALF").ToString.Trim \ 60) & ":" & ds1.Tables(0).Rows(0).Item("HALF").ToString.Trim Mod 60

        If ds1.Tables(0).Rows(0).Item("PunchRequiredInDay").ToString.Trim = 1 Then
            CheckEdit1.Checked = True
        ElseIf ds1.Tables(0).Rows(0).Item("PunchRequiredInDay").ToString.Trim = 2 Then
            CheckEdit2.Checked = True
        ElseIf ds1.Tables(0).Rows(0).Item("PunchRequiredInDay").ToString.Trim = 3 Then
            CheckEdit3.Checked = True
        ElseIf ds1.Tables(0).Rows(0).Item("PunchRequiredInDay").ToString.Trim = 4 Then
            CheckEdit4.Checked = True
        ElseIf ds1.Tables(0).Rows(0).Item("PunchRequiredInDay").ToString.Trim = 5 Then
            CheckEdit5.Checked = True
        End If

        If ds1.Tables(0).Rows(0).Item("SinglePunchOnly").ToString.Trim = 1 Then
            CheckEdit6.Checked = True
        ElseIf ds1.Tables(0).Rows(0).Item("SinglePunchOnly").ToString.Trim = 2 Then
            CheckEdit7.Checked = True
        End If

        If ds1.Tables(0).Rows(0).Item("ISOT").ToString.Trim = "Y" Then
            ToggleSwitch5.IsOn = True
        ElseIf ds1.Tables(0).Rows(0).Item("ISOT").ToString.Trim = "N" Then
            ToggleSwitch5.IsOn = False
        End If

        TextEdit9.Text = ds1.Tables(0).Rows(0).Item("OTRATE").ToString.Trim

        If ds1.Tables(0).Rows(0).Item("ISOS").ToString.Trim = "Y" Then
            ToggleSwitch6.IsOn = True
        ElseIf ds1.Tables(0).Rows(0).Item("ISOS").ToString.Trim = "N" Then
            ToggleSwitch6.IsOn = False
        End If

        If ds1.Tables(0).Rows(0).Item("MARKMISSASHALFDAY").ToString.Trim = "Y" Then
            ToggleSwitchMarkHlf.IsOn = True
        ElseIf ds1.Tables(0).Rows(0).Item("MARKMISSASHALFDAY").ToString.Trim = "N" Then
            ToggleSwitchMarkHlf.IsOn = False
        End If
        'end Time Office Policy

        'Shift
        If ds1.Tables(0).Rows(0).Item("SHIFTTYPE").ToString.Trim = "F" Then
            ComboBoxShiftType.SelectedItem = "Fixed"
        ElseIf ds1.Tables(0).Rows(0).Item("SHIFTTYPE").ToString.Trim = "R" Then
            ComboBoxShiftType.SelectedItem = "Rotational"
        ElseIf ds1.Tables(0).Rows(0).Item("SHIFTTYPE").ToString.Trim = "I" Then
            ComboBoxShiftType.SelectedItem = "Ignore"
        ElseIf ds1.Tables(0).Rows(0).Item("SHIFTTYPE").ToString.Trim = "L" Then
            ComboBoxShiftType.SelectedItem = "Flexi"
        ElseIf ds1.Tables(0).Rows(0).Item("SHIFTTYPE").ToString.Trim = "M" Then
            GridLookUpEditMultiShif.EditValue = ds1.Tables(0).Rows(0).Item("MSHIFT").ToString
            ComboBoxShiftType.SelectedItem = "MultiShift"
            LabelControl53.Visible = True
            ToggleAllowCmpOffMlt.Visible = True
            If ds1.Tables(0).Rows(0).Item("MISCOMPOFF").ToString.Trim = "Y" Then
                ToggleAllowCmpOffMlt.IsOn = True
            Else
                ToggleAllowCmpOffMlt.IsOn = False
            End If
        Else
            LabelControl53.Visible = False
            ToggleAllowCmpOffMlt.Visible = False
            ToggleAllowCmpOffMlt.IsOn = False
        End If

        If ds1.Tables(0).Rows(0).Item("ISCOMPOFF").ToString.Trim = "Y" Then
            ToggleCmpPOWPOH.IsOn = True
        Else
            ToggleCmpPOWPOH.IsOn = False
        End If

        GridLookUpEditDefaltShift.EditValue = ds1.Tables(0).Rows(0).Item("SHIFT").ToString
        PopupContainerEdit1.EditValue = ds1.Tables(0).Rows(0).Item("SHIFTPATTERN").ToString.Trim
        TextEditRotation.Text = ds1.Tables(0).Rows(0).Item("SHIFTPATTERN").ToString.Trim
        PopupContainerEdit2.EditValue = ds1.Tables(0).Rows(0).Item("AUTH_SHIFTS").ToString.Trim
        If ds1.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString.Trim = "Y" Then
            ToggleSwitchAutoShift.IsOn = True
        ElseIf ds1.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString.Trim = "N" Then
            ToggleSwitchAutoShift.IsOn = False
        End If
        TextEdit10.Text = ds1.Tables(0).Rows(0).Item("SHIFTREMAINDAYS").ToString.Trim
        TextEdit11.Text = ds1.Tables(0).Rows(0).Item("CDAYS").ToString.Trim


        If ds1.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString.Trim = "NON" Then
            ComboBoxEdit2.SelectedItem = "NONE"
        ElseIf ds1.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString.Trim = "SUN" Then
            ComboBoxEdit2.SelectedItem = "SUNDAY"
        ElseIf ds1.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString.Trim = "MON" Then
            ComboBoxEdit2.SelectedItem = "MONDAY"
        ElseIf ds1.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString.Trim = "TUE" Then
            ComboBoxEdit2.SelectedItem = "TUESDAY"
        ElseIf ds1.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString.Trim = "WED" Then
            ComboBoxEdit2.SelectedItem = "WEDNESDAY"
        ElseIf ds1.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString.Trim = "THU" Then
            ComboBoxEdit2.SelectedItem = "THURSDAY"
        ElseIf ds1.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString.Trim = "FRI" Then
            ComboBoxEdit2.SelectedItem = "FRIDAY"
        ElseIf ds1.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString.Trim = "SAT" Then
            ComboBoxEdit2.SelectedItem = "SATURDAY"
        End If



        If ds1.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString.Trim = "NON" Then
            ComboBoxEdit3.SelectedItem = "NONE"
        ElseIf ds1.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString.Trim = "SUN" Then
            ComboBoxEdit3.SelectedItem = "SUNDAY"
        ElseIf ds1.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString.Trim = "MON" Then
            ComboBoxEdit3.SelectedItem = "MONDAY"
        ElseIf ds1.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString.Trim = "TUE" Then
            ComboBoxEdit3.SelectedItem = "TUESDAY"
        ElseIf ds1.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString.Trim = "WED" Then
            ComboBoxEdit3.SelectedItem = "WEDNESDAY"
        ElseIf ds1.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString.Trim = "THU" Then
            ComboBoxEdit3.SelectedItem = "THURSDAY"
        ElseIf ds1.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString.Trim = "FRI" Then
            ComboBoxEdit3.SelectedItem = "FRIDAY"
        ElseIf ds1.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString.Trim = "SAT" Then
            ComboBoxEdit3.SelectedItem = "SATURDAY"
        End If

        If ds1.Tables(0).Rows(0).Item("SECONDOFFTYPE").ToString.Trim = "F" Then
            ComboBoxEdit4.SelectedItem = "Full"
        ElseIf ds1.Tables(0).Rows(0).Item("SECONDOFFTYPE").ToString.Trim = "H" Then
            ComboBoxEdit4.SelectedItem = "Half"
        End If

        GridLookUpEdit2.EditValue = ds1.Tables(0).Rows(0).Item("HALFDAYSHIFT").ToString

        Dim secweekoff() As Char = ds1.Tables(0).Rows(0).Item("ALTERNATE_OFF_DAYS").ToString
        For i As Integer = 0 To secweekoff.Length - 1
            If secweekoff(i) = "1" Then
                CheckEdit8.Checked = True
            End If
            If secweekoff(i) = "2" Then
                CheckEdit9.Checked = True
            End If
            If secweekoff(i) = "3" Then
                CheckEdit10.Checked = True
            End If
            If secweekoff(i) = "4" Then
                CheckEdit11.Checked = True
            End If
            If secweekoff(i) = "5" Then
                CheckEdit12.Checked = True
            End If
        Next
        'End Shift


        ToggleAWasAA.EditValue = ds1.Tables(0).Rows(0).Item("ISAW").ToString
        ToggleWAasAA.EditValue = ds1.Tables(0).Rows(0).Item("ISWA").ToString
        ToggleAWPasAAP.EditValue = ds1.Tables(0).Rows(0).Item("ISAWP").ToString
        TogglePWAasPAA.EditValue = ds1.Tables(0).Rows(0).Item("ISPWA").ToString

        setPolicy()
    End Sub
    Private Sub setPolicy()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        'Dim sSql As String = "select * from tblSetup where SETUPID= (select MAX(SETUPID) from tblSetup)"
        Dim sSql As String = "select * from EmployeeGroup where GroupId = '" & GrpId & "' "
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        'TxtSetRegNo.Text = (Convert.ToInt32(ds.Tables(0).Rows(0).Item("SETUPID").ToString) + 1)
        If CheckEditOT3.Checked = True Then
            LabelControl42.Enabled = True
            ToggleOTisAllowedInEarlyComing.Enabled = True
            GroupControl11.Enabled = True
        Else
            LabelControl42.Enabled = False
            ToggleOTisAllowedInEarlyComing.Enabled = False
            GroupControl11.Enabled = False
        End If
        'TxtPermisLateArr.Text = ds.Tables(0).Rows(0).Item("PERMISLATEARR").ToString.Trim
        'TxtPermisEarlyDpt.Text = ds.Tables(0).Rows(0).Item("PERMISEARLYDEP").ToString.Trim
        'TxtPermisEarlyDpt.Text = ds.Tables(0).Rows(0).Item("PERMISEARLYDEP").ToString.Trim
        TxtDuplicateCheck.Text = ds.Tables(0).Rows(0).Item("DUPLICATECHECKMIN").ToString.Trim
        TxtInTimeForINPunch.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("S_END").ToString.Trim).ToString("HH:mm")
        TxtEndTimeForOutPunch.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("S_OUT").ToString.Trim).ToString("HH:mm")
        If ds.Tables(0).Rows(0).Item("ISOTOUTMINUSSHIFTENDTIME").ToString.Trim = "Y" Then
            CheckEditOT1.Checked = True
            CheckEditOT2.Checked = False
            CheckEditOT3.Checked = False
        ElseIf ds.Tables(0).Rows(0).Item("ISOTWRKGHRSMINUSSHIFTHRS").ToString.Trim = "Y" Then
            CheckEditOT1.Checked = False
            CheckEditOT2.Checked = True
            CheckEditOT3.Checked = False
        ElseIf ds.Tables(0).Rows(0).Item("ISOTEARLYCOMEPLUSLATEDEP").ToString.Trim = "Y" Then
            CheckEditOT1.Checked = False
            CheckEditOT2.Checked = False
            CheckEditOT3.Checked = True
        End If
        If ds.Tables(0).Rows(0).Item("ISOTEARLYCOMING").ToString.Trim = "Y" Then
            ToggleOTisAllowedInEarlyComing.IsOn = True
        Else
            ToggleOTisAllowedInEarlyComing.IsOn = False
        End If
        TxtOtLateDur.Text = ds.Tables(0).Rows(0).Item("OTLATECOMINGDUR").ToString.Trim
        TxtOtRestrictDur.Text = ds.Tables(0).Rows(0).Item("OTRESTRICTENDDUR").ToString.Trim

        TxtOtEarlyDur.Text = ds.Tables(0).Rows(0).Item("OTEARLYDUR").ToString.Trim
        TxtDeductOTinWO.Text = ds.Tables(0).Rows(0).Item("DEDUCTWOOT").ToString.Trim
        TxtDeductOTinHLD.Text = ds.Tables(0).Rows(0).Item("DEDUCTHOLIDAYOT").ToString.Trim
        If ds.Tables(0).Rows(0).Item("ISPRESENTONWOPRESENT").ToString.Trim = "Y" Then
            ToggleIsPresentOnWeekOff.IsOn = True
        Else
            ToggleIsPresentOnWeekOff.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("ISPRESENTONHLDPRESENT").ToString.Trim = "Y" Then
            ToggleIsPResentOnHLD.IsOn = True
        Else
            ToggleIsPResentOnHLD.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("ISAUTOABSENT").ToString.Trim = "Y" Then
            ToggleIsAutoAbsentAllowed.IsOn = True
        Else
            ToggleIsAutoAbsentAllowed.IsOn = False
        End If
        TxtPermisEarlyMinAutoShift.Text = ds.Tables(0).Rows(0).Item("AUTOSHIFT_LOW").ToString.Trim()
        TxtPermisLateMinAutoShift.Text = ds.Tables(0).Rows(0).Item("AUTOSHIFT_UP").ToString.Trim()

        If ds.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString.Trim = "Y" Then
            ToggleAutoShiftAllowed.IsOn = True
        Else
            ToggleAutoShiftAllowed.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("WOINCLUDE").ToString.Trim = "Y" Then
            ToggleWeekOffIncludeInDUtyRoster.IsOn = True
        Else
            ToggleWeekOffIncludeInDUtyRoster.IsOn = False
        End If

        'If ds.Tables(0).Rows(0).Item("IsOutWork").ToString.Trim = "Y" Then
        '    ToggleOutWorkAllowed.IsOn = True
        'Else
        '    ToggleOutWorkAllowed.IsOn = False
        'End If

        Dim NightShiftFourPunch As String
        If ToggleFourPunchNight.IsOn = True Then
            NightShiftFourPunch = "Y"
        Else
            NightShiftFourPunch = "N"
        End If

        If ds.Tables(0).Rows(0).Item("OTROUND").ToString.Trim = "Y" Then
            ToggleRoundOverTime.IsOn = True
        Else
            ToggleRoundOverTime.IsOn = False
        End If

        TxtNoOffpresentOnweekOff.Text = ds.Tables(0).Rows(0).Item("PREWO").ToString.Trim
        If ds.Tables(0).Rows(0).Item("ISAWA").ToString.Trim = "Y" Then
            ToggleMarkAWAasAAA.IsOn = True
        Else
            ToggleMarkAWAasAAA.IsOn = False
        End If


        If ds.Tables(0).Rows(0).Item("ISPREWO").ToString.Trim = "Y" Then
            ToggleMarkWOasAbsent.IsOn = True
        Else
            ToggleMarkWOasAbsent.IsOn = False
        End If

        'If ds.Tables(0).Rows(0).Item("LeaveFinancialYear").ToString.Trim = "Y" Then
        '    ToggleLeaveAsPerFinancialYear.IsOn = True
        'Else
        '    ToggleLeaveAsPerFinancialYear.IsOn = False
        'End If

        'If ds.Tables(0).Rows(0).Item("Online").ToString.Trim = "Y" Then
        '    ToggleOnlineEvents.IsOn = True
        'Else
        '    ToggleOnlineEvents.IsOn = False
        'End If

        'If ds.Tables(0).Rows(0).Item("AutoDownload").ToString.Trim = "Y" Then
        '    ToggleDownloadAtStartUp.IsOn = True
        'Else
        '    ToggleDownloadAtStartUp.IsOn = False
        'End If

        If ds.Tables(0).Rows(0).Item("MIS").ToString.Trim = "Y" Then
            ToggleMarkMisAsAbsent.IsOn = True
        Else
            ToggleMarkMisAsAbsent.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("OwMinus").ToString.Trim = "Y" Then
            ToggleOutWorkMins.IsOn = True
        Else
            ToggleOutWorkMins.IsOn = False
        End If

        ''CreateEmpPunchDwnld
        'If ds.Tables(0).Rows(0).Item("StartRealTime").ToString.Trim = "Y" Then
        '    ToggleRealTime.IsOn = True
        'Else
        '    ToggleRealTime.IsOn = False
        'End If

        ''Dim LateVerification As String
        'TxtPwd.Text = ds.Tables(0).Rows(0).Item("SMSPassword").ToString.Trim
        'TxtUserId.Text = ds.Tables(0).Rows(0).Item("SMSUserID").ToString.Trim
        'TxtMsg.Text = ds.Tables(0).Rows(0).Item("SMSMessage").ToString.Trim
        'TextEditBioPort.Text = ds.Tables(0).Rows(0).Item("BioPort").ToString.Trim
        'TextEditZKPort.Text = ds.Tables(0).Rows(0).Item("ZKPort").ToString.Trim
        'TextTWIR102Port.Text = ds.Tables(0).Rows(0).Item("TWIR102Port").ToString.Trim
        'TextEditTimerDur.Text = ds.Tables(0).Rows(0).Item("TimerDur").ToString.Trim
        'If ds.Tables(0).Rows(0).Item("Canteen").ToString.Trim = "Y" Then
        '    ToggleSwitchCanteen.IsOn = True
        'ElseIf ds.Tables(0).Rows(0).Item("Canteen").ToString.Trim = "N" Then
        '    ToggleSwitchCanteen.IsOn = False
        'End If
        'If ds.Tables(0).Rows(0).Item("Visitor").ToString.Trim = "Y" Then
        '    ToggleSwitchVisitor.IsOn = True
        'ElseIf ds.Tables(0).Rows(0).Item("Visitor").ToString.Trim = "N" Then
        '    ToggleSwitchVisitor.IsOn = False
        'End If

        'If ds.Tables(0).Rows(0).Item("IsNepali").ToString.Trim = "Y" Then
        '    ToggleSwitchIsNepali.IsOn = True
        'ElseIf ds.Tables(0).Rows(0).Item("IsNepali").ToString.Trim = "N" Then
        '    ToggleSwitchIsNepali.IsOn = False
        'End If
        'TextEditAutoDwnDur.Text = ds.Tables(0).Rows(0).Item("AutoDwnDur").ToString.Trim

    End Sub

    Private Sub SetDefaultValue()
        TextEdit1.Enabled = True
        TextEdit1.Text = ""
        TextEdit2.Text = ""
        TextEditRotation.Text = ""
        TextEdit3.Text = "00:10"
        TextEdit4.Text = "00:10"
        TextEdit5.Text = "23:59"
        ToggleSwitch1.IsOn = False
        ToggleSwitch2.IsOn = True
        ToggleSwitch3.IsOn = False
        ToggleSwitch4.IsOn = False
        TextEdit6.Text = "04:00"
        TextEdit7.Text = "00:00"
        TextEdit8.Text = "00:00"
        CheckEdit4.Checked = True
        CheckEdit7.Checked = True
        ToggleSwitch5.IsOn = False
        TextEdit9.Text = "000.00"
        ToggleSwitch6.IsOn = False

        ComboBoxShiftType.SelectedItem = "Fixed"
        GridLookUpEditDefaltShift.EditValue = GridLookUpEditDefaltShift.Properties.GetKeyValue(0)
        PopupContainerEdit1.EditValue = ""
        ToggleSwitchAutoShift.IsOn = False
        PopupContainerEdit2.EditValue = ""
        TextEdit10.Text = ""
        TextEdit11.Text = ""
        ComboBoxEdit2.SelectedItem = "SUNDAY"
        ComboBoxEdit3.SelectedItem = "NONE"
        ComboBoxEdit4.SelectedItem = "Full"
        GridLookUpEdit2.EditValue = ""
        CheckEdit8.Checked = False
        CheckEdit9.Checked = False
        CheckEdit10.Checked = False
        CheckEdit11.Checked = False
        CheckEdit12.Checked = False
        ToggleSwitchMarkHlf.IsOn = False


        GridLookUpEdit3.Visible = True
        SimpleButton3.Visible = True
        TextEditRotation.Visible = True
        GridLookUpEdit4.Visible = True
        SimpleButton4.Visible = True
        GridLookUpEditMultiShif.Visible = False
        GridLookUpEditMultiShif.EditValue = ""

        LabelControl18.Text = "Shifts for rotation"
        LabelControl53.Visible = False
        ToggleAllowCmpOffMlt.Visible = False
        ToggleAllowCmpOffMlt.IsOn = False
        ToggleCmpPOWPOH.IsOn = False
        ToggleAWasAA.IsOn = False
        ToggleWAasAA.IsOn = False
        ToggleAWPasAAP.IsOn = False
        TogglePWAasPAA.IsOn = False
        SetDefaultPilicy()
    End Sub
    Private Sub SetDefaultPilicy()
        'Dim adap As SqlDataAdapter
        'Dim adapA As OleDbDataAdapter
        'Dim ds As DataSet = New DataSet
        ''Dim sSql As String = "select * from tblSetup where SETUPID= (select MAX(SETUPID) from tblSetup)"
        'Dim sSql As String = "Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblSetup )"
        'If Common.servername = "Access" Then
        '    sSql = "Select * from tblSetUp where setupid =(Select MAX(val(Setupid)) from tblSetup )"
        '    adapA = New OleDbDataAdapter(sSql, Common.con1)
        '    adapA.Fill(ds)
        'Else
        '    adap = New SqlDataAdapter(sSql, Common.con)
        '    adap.Fill(ds)
        'End If
        CheckEditOT2.Checked = True
        If CheckEditOT3.Checked = True Then
            LabelControl42.Enabled = True
            ToggleOTisAllowedInEarlyComing.Enabled = True
            GroupControl11.Enabled = True
        Else
            LabelControl42.Enabled = False
            ToggleOTisAllowedInEarlyComing.Enabled = False
            GroupControl11.Enabled = False
        End If
        'TxtPermisLateArr.Text = ds.Tables(0).Rows(0).Item("PERMISLATEARR").ToString.Trim
        'TxtPermisEarlyDpt.Text = ds.Tables(0).Rows(0).Item("PERMISEARLYDEP").ToString.Trim
        'TxtPermisEarlyDpt.Text = ds.Tables(0).Rows(0).Item("PERMISEARLYDEP").ToString.Trim
        TxtDuplicateCheck.Text = "5" ' ds.Tables(0).Rows(0).Item("DUPLICATECHECKMIN").ToString.Trim
        TxtInTimeForINPunch.Text = "05:00" 'Convert.ToDateTime(ds.Tables(0).Rows(0).Item("S_END").ToString.Trim).ToString("HH:mm")
        TxtEndTimeForOutPunch.Text = "05:00" 'Convert.ToDateTime(ds.Tables(0).Rows(0).Item("S_OUT").ToString.Trim).ToString("HH:mm")

        'If ds.Tables(0).Rows(0).Item("ISOTOUTMINUSSHIFTENDTIME").ToString.Trim = "Y" Then
        '    CheckEditOT1.Checked = True
        '    CheckEditOT2.Checked = False
        '    CheckEditOT3.Checked = False
        'ElseIf ds.Tables(0).Rows(0).Item("ISOTWRKGHRSMINUSSHIFTHRS").ToString.Trim = "Y" Then
        '    CheckEditOT1.Checked = False
        '    CheckEditOT2.Checked = True
        '    CheckEditOT3.Checked = False
        'ElseIf ds.Tables(0).Rows(0).Item("ISOTEARLYCOMEPLUSLATEDEP").ToString.Trim = "Y" Then
        '    CheckEditOT1.Checked = False
        '    CheckEditOT2.Checked = False
        '    CheckEditOT3.Checked = True
        'End If
        'If ds.Tables(0).Rows(0).Item("ISOTEARLYCOMING").ToString.Trim = "Y" Then
        '    ToggleOTisAllowedInEarlyComing.IsOn = True
        'Else
        ToggleOTisAllowedInEarlyComing.IsOn = False
        'End If
        TxtOtLateDur.Text = "0" 'ds.Tables(0).Rows(0).Item("OTLATECOMINGDUR").ToString.Trim
        TxtOtRestrictDur.Text = "0" 'ds.Tables(0).Rows(0).Item("OTRESTRICTENDDUR").ToString.Trim

        TxtOtEarlyDur.Text = "0" 'ds.Tables(0).Rows(0).Item("OTEARLYDUR").ToString.Trim
        TxtDeductOTinWO.Text = "0" 'ds.Tables(0).Rows(0).Item("DEDUCTWOOT").ToString.Trim
        TxtDeductOTinHLD.Text = "0" ' ds.Tables(0).Rows(0).Item("DEDUCTHOLIDAYOT").ToString.Trim
        'If ds.Tables(0).Rows(0).Item("ISPRESENTONWOPRESENT").ToString.Trim = "Y" Then
        '    ToggleIsPresentOnWeekOff.IsOn = True
        'Else
        ToggleIsPresentOnWeekOff.IsOn = False
        'End If

        'If ds.Tables(0).Rows(0).Item("ISPRESENTONHLDPRESENT").ToString.Trim = "Y" Then
        '    ToggleIsPResentOnHLD.IsOn = True
        'Else
        ToggleIsPResentOnHLD.IsOn = False
        'End If

        'If ds.Tables(0).Rows(0).Item("ISAUTOABSENT").ToString.Trim = "Y" Then
        '    ToggleIsAutoAbsentAllowed.IsOn = True
        'Else
        ToggleIsAutoAbsentAllowed.IsOn = False
        'End If
        TxtPermisEarlyMinAutoShift.Text = "240" ' ds.Tables(0).Rows(0).Item("AUTOSHIFT_LOW").ToString.Trim()
        TxtPermisLateMinAutoShift.Text = "240" 'ds.Tables(0).Rows(0).Item("AUTOSHIFT_UP").ToString.Trim()

        'If ds.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString.Trim = "Y" Then
        '    ToggleAutoShiftAllowed.IsOn = True
        'Else
        ToggleAutoShiftAllowed.IsOn = False
        'End If

        'If ds.Tables(0).Rows(0).Item("WOINCLUDE").ToString.Trim = "Y" Then
        '    ToggleWeekOffIncludeInDUtyRoster.IsOn = True
        'Else
        ToggleWeekOffIncludeInDUtyRoster.IsOn = False
        'End If

        'If ds.Tables(0).Rows(0).Item("IsOutWork").ToString.Trim = "Y" Then
        '    ToggleOutWorkAllowed.IsOn = True
        'Else
        'ToggleOutWorkAllowed.IsOn = False
        'End If
        ToggleFourPunchNight.IsOn = False
        'Dim NightShiftFourPunch As String
        'If ToggleFourPunchNight.IsOn = True Then
        '    NightShiftFourPunch = "Y"
        'Else
        '    NightShiftFourPunch = "N"
        'End If
        Dim LinesPerPage As String = "58"  'hardcoded




        'If ds.Tables(0).Rows(0).Item("OTROUND").ToString.Trim = "Y" Then
        '    ToggleRoundOverTime.IsOn = True
        'Else
        ToggleRoundOverTime.IsOn = False
        'End If

        TxtNoOffpresentOnweekOff.Text = "3" 'ds.Tables(0).Rows(0).Item("PREWO").ToString.Trim
        'If ds.Tables(0).Rows(0).Item("ISAWA").ToString.Trim = "Y" Then
        '    ToggleMarkAWAasAAA.IsOn = True
        'Else
        ToggleMarkAWAasAAA.IsOn = False
        'End If


        'If ds.Tables(0).Rows(0).Item("ISPREWO").ToString.Trim = "Y" Then
        '    ToggleMarkWOasAbsent.IsOn = True
        'Else
        ToggleMarkWOasAbsent.IsOn = False
        'End If

        'If ds.Tables(0).Rows(0).Item("LeaveFinancialYear").ToString.Trim = "Y" Then
        '    ToggleLeaveAsPerFinancialYear.IsOn = True
        'Else
        '    ToggleLeaveAsPerFinancialYear.IsOn = False
        'End If

        'If ds.Tables(0).Rows(0).Item("Online").ToString.Trim = "Y" Then
        '    ToggleOnlineEvents.IsOn = True
        'Else
        '    ToggleOnlineEvents.IsOn = False
        'End If

        'If ds.Tables(0).Rows(0).Item("AutoDownload").ToString.Trim = "Y" Then
        '    ToggleDownloadAtStartUp.IsOn = True
        'Else
        '    ToggleDownloadAtStartUp.IsOn = False
        'End If

        'If ds.Tables(0).Rows(0).Item("MIS").ToString.Trim = "Y" Then
        '    ToggleMarkMisAsAbsent.IsOn = True
        'Else
        ToggleMarkMisAsAbsent.IsOn = False
        'End If

        'If ds.Tables(0).Rows(0).Item("OwMinus").ToString.Trim = "Y" Then
        '    ToggleOutWorkMins.IsOn = True
        'Else
        ToggleOutWorkMins.IsOn = False
        'End If

        ''CreateEmpPunchDwnld
        'If ds.Tables(0).Rows(0).Item("StartRealTime").ToString.Trim = "Y" Then
        '    ToggleRealTime.IsOn = True
        'Else
        '    ToggleRealTime.IsOn = False
        'End If

    End Sub
    Private Sub TextEdit1_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextEdit1.TextChanged
        TextEdit1.Text = TextEdit1.Text.ToUpper
    End Sub
    Private Sub GridLookUpEdit1_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEditDefaltShift.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value.ToString.PadRight(3)), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("SHIFT").ToString & (" - " + Convert.ToDateTime(row("STARTTIME").ToString).ToString("HH:mm")) & (" - " + Convert.ToDateTime(row("ENDTIME").ToString).ToString("HH:mm")))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    Private Sub GridLookUpEdit2_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEdit2.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("SHIFT").ToString & (" - " + Convert.ToDateTime(row("STARTTIME").ToString).ToString("HH:mm")) & (" - " + Convert.ToDateTime(row("ENDTIME").ToString).ToString("HH:mm")))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    Private Sub GridView1_SelectionChanged(sender As System.Object, e As DevExpress.Data.SelectionChangedEventArgs) Handles GridView1.SelectionChanged
        If GrpId.Length = 0 Then
            TextEditRotation.Text = ""
        Else
            'Dim x As String() = TextEdit1.Text.Split(",")
            'Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            'MsgBox(GridView1.GetSelectedRows.Length)
            'Dim result As Object() = New Object(x.Length) {}
            'For i As Integer = 0 To x.Length - 1
            '    Dim rowHandle As Integer = selectedRows(i)
            '    If Not GridView1.IsGroupRow(rowHandle) Then
            '        result(i) = GridView1.GetRowCellValue(rowHandle, "SHIFT")
            '        shiftlst.Add(result(i))
            '        MsgBox(result(i))
            '    End If
            'Next

            'Dim tmp() As String = shiftlst.ToArray
            'Dim count As Integer = 0
            'For i As Integer = 0 To tmp.Length - 1
            '    If result(i).ToString = tmp(i).Trim Then
            '        count = count + 1
            '        If count > 1 Then
            '            shiftlst.Remove(tmp(i).Trim)
            '            shiftlst.Remove(tmp(i).Trim)
            '            'shiftlst.RemoveAt(i)
            '        End If
            '    End If
            'Next
            'Dim y() As String = shiftlst.ToArray
            'For i As Integer = 0 To y.Length - 1
            '    TextEdit12.Text = TextEdit12.Text & ", " & y(i)
            'Next
            'TextEdit12.Text = TextEdit12.Text.TrimStart(",")
        End If
        Dim view As GridView = CType(sender, GridView)
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim CellId As String = row("SHIFT").ToString.Trim
        'Dim x As String = ""
        'x = x & "," & CellId
        shiftlst.Add(CellId)
        Dim tmp() As String = shiftlst.ToArray
        'For i As Integer = 0 To tmp.Length - 1
        '    shiftlst.Add(tmp(i).Trim)
        'Next
        Dim count As Integer = 0
        For i As Integer = 0 To tmp.Length - 1
            If CellId = tmp(i).Trim Then
                count = count + 1
                If count > 1 Then
                    shiftlst.Remove(tmp(i).Trim)
                    shiftlst.Remove(tmp(i).Trim)
                    'shiftlst.RemoveAt(i)
                End If
            End If
        Next
        Dim y() As String = shiftlst.ToArray
        For i As Integer = 0 To y.Length - 1
            TextEditRotation.Text = TextEditRotation.Text & ", " & y(i)
        Next
        TextEditRotation.Text = TextEditRotation.Text.TrimStart(",")
        'GetSelectedValues(view, "SHIFT")
    End Sub
    Private Function GetSelectedValues(ByVal view As DevExpress.XtraGrid.Views.Grid.GridView, ByVal fieldName As String) As Object()
        'TextEdit12.Text = ""
        Dim selectedRows As Integer() = view.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        'For i As Integer = 0 To selectedRows.Length - 1
        Dim rowHandle As Integer = selectedRows(0)
        If Not GridView1.IsGroupRow(rowHandle) Then
            result(0) = view.GetRowCellValue(rowHandle, fieldName)
            TextEditRotation.Text = TextEditRotation.Text & "," & result(0).ToString
        Else
            result(0) = -1
        End If
        'Next
        MsgBox(result(0).ToString())
        Return result
    End Function
    Private Sub TextEdit12_Click(sender As System.Object, e As System.EventArgs) Handles TextEditRotation.Click
        PopupContainerEdit1.ShowPopup()
        'GridView1.SetFocusedValue(-1)
        'GridView1.ClearSelection()
    End Sub
    Private Sub SimpleButton3_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton3.Click
        If GridLookUpEdit3.EditValue <> "" Then
            Dim shiftlst As New List(Of String)()
            Dim tmp() As String = TextEditRotation.Text.Split(",")
            If tmp.Length > 2 Then
                XtraMessageBox.Show(ulf, "<size=10>Cannot enter more than 3 shifts</size>", "<size=9>Information</size>")
                Exit Sub
            End If
            For i As Integer = 0 To tmp.Length - 1
                If tmp(i) = "" Then
                    Continue For
                End If
                shiftlst.Add(tmp(i).Trim)
            Next
            For i As Integer = 0 To tmp.Length - 1
                If tmp(i).Trim <> GridLookUpEdit3.EditValue.ToString.Trim Then
                    shiftlst.Add(GridLookUpEdit3.EditValue.ToString)
                    Exit For
                    'TextEdit12.Text = TextEdit12.Text & ", " & GridLookUpEdit3.EditValue.ToString
                End If
            Next
            Dim x() As String = shiftlst.ToArray
            TextEditRotation.Text = ""
            For i As Integer = 0 To x.Length - 1
                TextEditRotation.Text = TextEditRotation.Text & "," & x(i)
            Next
            TextEditRotation.Text = TextEditRotation.Text.TrimStart(",")
            TextEditRotation.Text = TextEditRotation.Text.Trim

        End If
    End Sub
    Private Sub SimpleButton4_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton4.Click
        If TextEditRotation.Text <> "" Then
            Dim tmp() As String = TextEditRotation.Text.Split(",")
            TextEditRotation.Text = ""
            Dim shiftlst As New List(Of String)()
            For i As Integer = 0 To tmp.Length - 1
                shiftlst.Add(tmp(i).Trim)
            Next
            For i As Integer = 0 To tmp.Length - 1
                If tmp(i).Trim = GridLookUpEdit4.EditValue.ToString.Trim Then
                    shiftlst.Remove(GridLookUpEdit4.EditValue.ToString.Trim)
                End If
            Next
            Dim x() As String = shiftlst.ToArray
            For i As Integer = 0 To x.Length - 1
                TextEditRotation.Text = TextEditRotation.Text & ", " & x(i)
            Next
            TextEditRotation.Text = TextEditRotation.Text.TrimStart(",")
        End If
    End Sub
    Private Sub CheckEditOT3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditOT3.CheckedChanged
        If CheckEditOT3.Checked = True Then
            LabelControl42.Enabled = True
            ToggleOTisAllowedInEarlyComing.Enabled = True
            GroupControl11.Enabled = True
        Else
            LabelControl42.Enabled = False
            ToggleOTisAllowedInEarlyComing.Enabled = False
            GroupControl11.Enabled = False
        End If
    End Sub
    Private Sub ToggleMarkMisAsAbsent_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleMarkMisAsAbsent.Toggled
        If ToggleMarkMisAsAbsent.IsOn = True Then
            ToggleSwitchMarkHlf.IsOn = False
        End If
    End Sub
    Private Sub ToggleSwitchMarkHlf_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitchMarkHlf.Toggled
        If ToggleSwitchMarkHlf.IsOn = True Then
            ToggleMarkMisAsAbsent.IsOn = False
        End If
    End Sub
    Private Sub GridLookUpEditMultiShif_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEditMultiShif.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value.ToString.PadRight(3)), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("SHIFT").ToString & (" - " + Convert.ToDateTime(row("STARTTIME").ToString).ToString("HH:mm")) & (" - " + Convert.ToDateTime(row("ENDTIME").ToString).ToString("HH:mm")))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    Private Sub ToggleMarkAWAasAAA_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleMarkAWAasAAA.Toggled
        If ToggleMarkAWAasAAA.IsOn = True Then
            ToggleAWasAA.IsOn = False
            ToggleWAasAA.IsOn = False
            ToggleAWasAA.Enabled = False
            ToggleWAasAA.Enabled = False
        Else
            ToggleAWasAA.Enabled = True
            ToggleWAasAA.Enabled = True
        End If
    End Sub
End Class