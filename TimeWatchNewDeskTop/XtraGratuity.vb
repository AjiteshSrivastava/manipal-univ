﻿Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Net.NetworkInformation
Imports System.Text.RegularExpressions

Public Class XtraGratuity
    Dim ulf As UserLookAndFeel
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        Me.Cursor = Cursors.WaitCursor
        Dim PP As XtraPayrollProcess = New XtraPayrollProcess
        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand
        Dim rsProcess As DataSet = New DataSet
        Dim rsGratuity As DataSet = New DataSet
        Dim sWhichFormula As String ' * 355
        Dim sPaycode As String
        Dim decpos As Integer
        Dim iPFAMT As Double, iFPFAMT As Double, iOtAmt As Double, iHRAAMT As Double, iESIAMT As Double
        Dim iDedAmt1 As Double, iDedAmt2 As Double, iDedAmt3 As Double, iDedAmt4 As Double, iDedAmt5 As Double, iDedAmt6 As Double, iDedAmt7 As Double, iDedAmt8 As Double, iDedAmt9 As Double, iDedAmt10 As Double
        Dim iErnAmt1 As Double, iErnAmt2 As Double, iErnAmt3 As Double, iErnAmt4 As Double, iErnAmt5 As Double, iErnAmt6 As Double, iErnAmt7 As Double, iErnAmt8 As Double, iErnAmt9 As Double, iErnAmt10 As Double
        Dim iSalary As Double, iNetSal As Double, iGrossSal As Double
        Dim BASIC As Double, DA As Double, CONV As Double, MED As Double, PF As Double, FPF As Double, OTRate As Double, TDS As Double, PROF_TAX As Double
        Dim HRA As Double, ESI As Double, Fine As Double, Advance As Double
        Dim PRE As Double, ABS1 As Double, HLD As Double, LATE As Double
        Dim EARLY As Double, OT As Double, CL As Double, SL As Double, PL_EL As Double
        Dim OTHER_LV As Double, LEAVE As Double, TDAYS As Double, T_LATE As Double, T_EARLY As Double
        Dim D_1TOD_10 As Double, E_1TOE_10 As Double, OT_RATE As Double, MON_DAY As Double
        Dim ided1 As Double, ided2 As Double, ided3 As Double, ided4 As Double, ided5 As Double, ided6 As Double, ided7 As Double, ided8 As Double, ided9 As Double, ided10 As Double
        Dim iern1 As Double, iern2 As Double, iern3 As Double, iern4 As Double, iern5 As Double, iern6 As Double, iern7 As Double, iern8 As Double, iern9 As Double, iern10 As Double
        Dim iRESULT As Double
        Dim iWOValue As Integer
        Dim rsPaysetup As DataSet = New DataSet
        Dim MYEAR As Double
        Dim conv_amt = 0, medical_amt = 0, tds_amt = 0, prof_tax_amt = 0, amt_on_pf = 0, amt_on_esi = 0, iVPFAMT = 0, iEPFAMT = 0
        Dim sSql As String = "select a.*,b.* from pay_master a, tblemployee b where a.paycode=b.paycode and a.VEmployeeType='O'"
        'rsProcess = Cn.Execute(sSql)
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsProcess)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsProcess)
        End If

        sSql = "select * from pay_setup"
        'rsPaysetup = Cn.Execute(sSql)
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsPaysetup)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsPaysetup)
        End If
        'Do While Not rsProcess.EOF
        For i As Integer = 0 To rsProcess.Tables(0).Rows.Count - 1
            

            iPFAMT = 0 : iFPFAMT = 0 : iOtAmt = 0 : iHRAAMT = 0 : Advance = 0 : Fine = 0 : iDedAmt1 = 0 : iDedAmt2 = 0 : iDedAmt3 = 0 : iDedAmt4 = 0 : iDedAmt5 = 0 : iDedAmt6 = 0 : iDedAmt7 = 0 : iDedAmt8 = 0 : iDedAmt9 = 0 : iDedAmt10 = 0
            iErnAmt1 = 0 : iErnAmt2 = 0 : iErnAmt3 = 0 : iErnAmt4 = 0 : iErnAmt5 = 0 : iErnAmt6 = 0 : iErnAmt7 = 0 : iErnAmt8 = 0 : iErnAmt9 = 0 : iErnAmt10 = 0 : iESIAMT = 0
            conv_amt = 0 : medical_amt = 0 : tds_amt = 0 : prof_tax_amt = 0 : amt_on_pf = 0 : amt_on_esi = 0 : iVPFAMT = 0 : iEPFAMT = 0
            sPaycode = rsProcess.Tables(0).Rows(i)("PAYCODE").ToString.Trim : BASIC = rsProcess.Tables(0).Rows(i)("VBASIC")
            DA = IIf((rsProcess.Tables(0).Rows(i)("VDA_RATE").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VDA_RATE"))
            HRA = IIf((rsProcess.Tables(0).Rows(i)("VHRA_RATE").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VHRA_RATE"))
            MED = IIf((rsProcess.Tables(0).Rows(i)("Vmed_rate").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("Vmed_rate"))
            CONV = IIf((rsProcess.Tables(0).Rows(i)("Vconv_rate").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("Vconv_rate"))
            ided1 = IIf((rsProcess.Tables(0).Rows(i)("VD_1").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VD_1"))
            ided2 = IIf((rsProcess.Tables(0).Rows(i)("VD_2").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VD_2"))
            ided3 = IIf((rsProcess.Tables(0).Rows(i)("VD_3").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VD_3"))
            ided4 = IIf((rsProcess.Tables(0).Rows(i)("VD_4").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VD_4"))
            ided5 = IIf((rsProcess.Tables(0).Rows(i)("VD_5").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VD_5"))
            ided6 = IIf((rsProcess.Tables(0).Rows(i)("VD_6").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VD_6"))
            ided7 = IIf((rsProcess.Tables(0).Rows(i)("VD_7").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VD_7"))
            ided8 = IIf((rsProcess.Tables(0).Rows(i)("VD_8").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VD_8"))
            ided9 = IIf((rsProcess.Tables(0).Rows(i)("VD_9").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VD_9"))
            ided10 = IIf((rsProcess.Tables(0).Rows(i)("VD_10").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VD_10"))

            iern1 = IIf((rsProcess.Tables(0).Rows(i)("VI_1").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VI_1"))
            iern2 = IIf((rsProcess.Tables(0).Rows(i)("VI_2").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VI_2"))
            iern3 = IIf((rsProcess.Tables(0).Rows(i)("VI_3").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VI_3"))
            iern4 = IIf((rsProcess.Tables(0).Rows(i)("VI_4").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VI_4"))
            iern5 = IIf((rsProcess.Tables(0).Rows(i)("VI_5").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VI_5"))
            iern6 = IIf((rsProcess.Tables(0).Rows(i)("VI_6").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VI_6"))
            iern7 = IIf((rsProcess.Tables(0).Rows(i)("VI_7").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VI_7"))
            iern8 = IIf((rsProcess.Tables(0).Rows(i)("VI_8").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VI_8"))
            iern9 = IIf((rsProcess.Tables(0).Rows(i)("VI_9").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VI_9"))
            iern10 = IIf((rsProcess.Tables(0).Rows(i)("VI_10").ToString.Trim = ""), 0, rsProcess.Tables(0).Rows(i)("VI_10"))

            OT_RATE = rsProcess.Tables(0).Rows(i)("VOT_RATE")

            If rsProcess.Tables(0).Rows(i)("GRADUTY_ALLOWED") = "Y" Then
                If (rsProcess.Tables(0).Rows(i)("dateofjoin").ToString.Trim = "") Then
                    MsgBox("Date of Joining not feeded in Employee Maser, Cannot Process Gratuity of :" & rsProcess.Tables(0).Rows(i)("empname"), vbInformation + vbExclamation, "TimeWatch Message")
                Else
                    amt_on_pf = 0
                    sWhichFormula = rsPaysetup.Tables(0).Rows(0)("GRADUTY_FORMULA")
                    If (rsProcess.Tables(0).Rows(i)("Leavingdate").ToString.Trim = "") Then
                        MYEAR = Format((DateDiff("d", rsProcess.Tables(0).Rows(i)("dateofjoin"), dtpProcessDate.DateTime) + 1) / 365, "00.00")
                    Else
                        MYEAR = Format((DateDiff("d", rsProcess.Tables(0).Rows(i)("dateofjoin"), rsProcess.Tables(0).Rows(i)("Leavingdate")) + 1) / 365, "00.00")
                    End If
                    If MYEAR >= rsPaysetup.Tables(0).Rows(0)("graduty_yrlimit") Then
                        MYEAR = Math.Round(MYEAR, 0)
                        If InStr(1, UCase(sWhichFormula), " YEAR ") <> 0 Then
                            sWhichFormula = Replace(sWhichFormula, " YEAR ", Format(MYEAR, "00.00"))
                        End If
                        amt_on_pf = Math.Round(PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED), 0)
                        sSql = "Select Paycode From PAY_GRATUITY" & " Where Paycode='" & Trim(sPaycode) & "'"
                        'rsGratuity = Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                            adapA.Fill(rsGratuity)
                        Else
                            adap = New SqlDataAdapter(sSql, Common.con)
                            adap.Fill(rsGratuity)
                        End If

                        If rsGratuity.Tables(0).Rows.Count = 0 Then
                            sSql = "Insert Into PAY_GRATUITY(PAYCODE) values('" & Trim(sPaycode) & "')"
                            If Common.servername = "Access" Then
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(sSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If

                        End If
                        sSql = "Update PAY_GRATUITY Set ASONDATE='" & dtpProcessDate.DateTime.ToString("yyyy-MM-dd") & "',GRATUITY_AMT=" & Format(amt_on_pf, "00000000.00") & ",PERIOD=" & MYEAR & " Where Paycode='" & Trim(sPaycode) & "'"
                        'Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                    End If
                End If
            End If
        Next

        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Processed Successfully</size>", "Success")
        Me.Close()
    End Sub

    Private Sub XtraCompanyInfo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        'MAC =  getMacAddress()
        dtpProcessDate.EditValue = Now

    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
End Class