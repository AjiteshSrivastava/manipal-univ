﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraEmployeeGroup
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraEmployeeGroup))
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colGroupId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGroupName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTTYPE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTPATTERN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTREMAINDAYS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLASTSHIFTPERFORMED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISAUTOSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAUTH_SHIFTS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFIRSTOFFDAY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSECONDOFFTYPE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHALFDAYSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSECONDOFFDAY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colALTERNATE_OFF_DAYS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colINONLY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISPUNCHALL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISTIMELOSSALLOWED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCDAYS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISROUNDTHECLOCKWORK = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISOT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTRATE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPERMISLATEARRIVAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPERMISEARLYDEPRT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMAXDAYMIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISOS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHORT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHALF = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISHALFDAY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISSHORT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTWO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemPopupContainerEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit()
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.EmployeeGroupBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.EmployeeGroupTableAdapter = New iAS.SSSDBDataSetTableAdapters.EmployeeGroupTableAdapter()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.EmployeeGroup1TableAdapter = New iAS.SSSDBDataSetTableAdapters.EmployeeGroup1TableAdapter()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemPopupContainerEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmployeeGroupBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemPopupContainerEdit1, Me.RepositoryItemComboBox1})
        Me.GridControl1.Size = New System.Drawing.Size(1036, 568)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GridView1.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Blue
        Me.GridView1.Appearance.TopNewRow.Options.UseFont = True
        Me.GridView1.Appearance.TopNewRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colGroupId, Me.colGroupName, Me.colBranch, Me.colSHIFT, Me.colSHIFTTYPE, Me.colSHIFTPATTERN, Me.colSHIFTREMAINDAYS, Me.colLASTSHIFTPERFORMED, Me.colISAUTOSHIFT, Me.colAUTH_SHIFTS, Me.colFIRSTOFFDAY, Me.colSECONDOFFTYPE, Me.colHALFDAYSHIFT, Me.colSECONDOFFDAY, Me.colALTERNATE_OFF_DAYS, Me.colINONLY, Me.colISPUNCHALL, Me.colISTIMELOSSALLOWED, Me.colCDAYS, Me.colISROUNDTHECLOCKWORK, Me.colISOT, Me.colOTRATE, Me.colPERMISLATEARRIVAL, Me.colPERMISEARLYDEPRT, Me.colMAXDAYMIN, Me.colISOS, Me.colTIME, Me.colSHORT, Me.colHALF, Me.colISHALFDAY, Me.colISSHORT, Me.colTWO, Me.colLastModifiedBy, Me.colLastModifiedDate})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.NewItemRowText = "Click here to add new Employee Group"
        Me.GridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colGroupId, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colGroupId
        '
        Me.colGroupId.FieldName = "GroupId"
        Me.colGroupId.Name = "colGroupId"
        Me.colGroupId.Visible = True
        Me.colGroupId.VisibleIndex = 0
        '
        'colGroupName
        '
        Me.colGroupName.FieldName = "GroupName"
        Me.colGroupName.Name = "colGroupName"
        Me.colGroupName.Visible = True
        Me.colGroupName.VisibleIndex = 1
        '
        'colBranch
        '
        Me.colBranch.FieldName = "Branch"
        Me.colBranch.Name = "colBranch"
        '
        'colSHIFT
        '
        Me.colSHIFT.FieldName = "SHIFT"
        Me.colSHIFT.Name = "colSHIFT"
        Me.colSHIFT.Visible = True
        Me.colSHIFT.VisibleIndex = 2
        '
        'colSHIFTTYPE
        '
        Me.colSHIFTTYPE.FieldName = "SHIFTTYPE"
        Me.colSHIFTTYPE.Name = "colSHIFTTYPE"
        Me.colSHIFTTYPE.Visible = True
        Me.colSHIFTTYPE.VisibleIndex = 3
        '
        'colSHIFTPATTERN
        '
        Me.colSHIFTPATTERN.FieldName = "SHIFTPATTERN"
        Me.colSHIFTPATTERN.Name = "colSHIFTPATTERN"
        Me.colSHIFTPATTERN.Visible = True
        Me.colSHIFTPATTERN.VisibleIndex = 4
        '
        'colSHIFTREMAINDAYS
        '
        Me.colSHIFTREMAINDAYS.FieldName = "SHIFTREMAINDAYS"
        Me.colSHIFTREMAINDAYS.Name = "colSHIFTREMAINDAYS"
        Me.colSHIFTREMAINDAYS.Visible = True
        Me.colSHIFTREMAINDAYS.VisibleIndex = 5
        '
        'colLASTSHIFTPERFORMED
        '
        Me.colLASTSHIFTPERFORMED.FieldName = "LASTSHIFTPERFORMED"
        Me.colLASTSHIFTPERFORMED.Name = "colLASTSHIFTPERFORMED"
        Me.colLASTSHIFTPERFORMED.Visible = True
        Me.colLASTSHIFTPERFORMED.VisibleIndex = 6
        '
        'colISAUTOSHIFT
        '
        Me.colISAUTOSHIFT.FieldName = "ISAUTOSHIFT"
        Me.colISAUTOSHIFT.Name = "colISAUTOSHIFT"
        Me.colISAUTOSHIFT.Visible = True
        Me.colISAUTOSHIFT.VisibleIndex = 7
        '
        'colAUTH_SHIFTS
        '
        Me.colAUTH_SHIFTS.FieldName = "AUTH_SHIFTS"
        Me.colAUTH_SHIFTS.Name = "colAUTH_SHIFTS"
        Me.colAUTH_SHIFTS.Visible = True
        Me.colAUTH_SHIFTS.VisibleIndex = 8
        '
        'colFIRSTOFFDAY
        '
        Me.colFIRSTOFFDAY.FieldName = "FIRSTOFFDAY"
        Me.colFIRSTOFFDAY.Name = "colFIRSTOFFDAY"
        Me.colFIRSTOFFDAY.Visible = True
        Me.colFIRSTOFFDAY.VisibleIndex = 9
        '
        'colSECONDOFFTYPE
        '
        Me.colSECONDOFFTYPE.FieldName = "SECONDOFFTYPE"
        Me.colSECONDOFFTYPE.Name = "colSECONDOFFTYPE"
        Me.colSECONDOFFTYPE.Visible = True
        Me.colSECONDOFFTYPE.VisibleIndex = 10
        '
        'colHALFDAYSHIFT
        '
        Me.colHALFDAYSHIFT.FieldName = "HALFDAYSHIFT"
        Me.colHALFDAYSHIFT.Name = "colHALFDAYSHIFT"
        Me.colHALFDAYSHIFT.Visible = True
        Me.colHALFDAYSHIFT.VisibleIndex = 11
        '
        'colSECONDOFFDAY
        '
        Me.colSECONDOFFDAY.FieldName = "SECONDOFFDAY"
        Me.colSECONDOFFDAY.Name = "colSECONDOFFDAY"
        Me.colSECONDOFFDAY.Visible = True
        Me.colSECONDOFFDAY.VisibleIndex = 12
        '
        'colALTERNATE_OFF_DAYS
        '
        Me.colALTERNATE_OFF_DAYS.FieldName = "ALTERNATE_OFF_DAYS"
        Me.colALTERNATE_OFF_DAYS.Name = "colALTERNATE_OFF_DAYS"
        Me.colALTERNATE_OFF_DAYS.Visible = True
        Me.colALTERNATE_OFF_DAYS.VisibleIndex = 13
        '
        'colINONLY
        '
        Me.colINONLY.FieldName = "INONLY"
        Me.colINONLY.Name = "colINONLY"
        Me.colINONLY.Visible = True
        Me.colINONLY.VisibleIndex = 14
        '
        'colISPUNCHALL
        '
        Me.colISPUNCHALL.FieldName = "ISPUNCHALL"
        Me.colISPUNCHALL.Name = "colISPUNCHALL"
        Me.colISPUNCHALL.Visible = True
        Me.colISPUNCHALL.VisibleIndex = 15
        '
        'colISTIMELOSSALLOWED
        '
        Me.colISTIMELOSSALLOWED.FieldName = "ISTIMELOSSALLOWED"
        Me.colISTIMELOSSALLOWED.Name = "colISTIMELOSSALLOWED"
        Me.colISTIMELOSSALLOWED.Visible = True
        Me.colISTIMELOSSALLOWED.VisibleIndex = 16
        '
        'colCDAYS
        '
        Me.colCDAYS.FieldName = "CDAYS"
        Me.colCDAYS.Name = "colCDAYS"
        Me.colCDAYS.Visible = True
        Me.colCDAYS.VisibleIndex = 17
        '
        'colISROUNDTHECLOCKWORK
        '
        Me.colISROUNDTHECLOCKWORK.FieldName = "ISROUNDTHECLOCKWORK"
        Me.colISROUNDTHECLOCKWORK.Name = "colISROUNDTHECLOCKWORK"
        Me.colISROUNDTHECLOCKWORK.Visible = True
        Me.colISROUNDTHECLOCKWORK.VisibleIndex = 18
        '
        'colISOT
        '
        Me.colISOT.FieldName = "ISOT"
        Me.colISOT.Name = "colISOT"
        Me.colISOT.Visible = True
        Me.colISOT.VisibleIndex = 19
        '
        'colOTRATE
        '
        Me.colOTRATE.FieldName = "OTRATE"
        Me.colOTRATE.Name = "colOTRATE"
        Me.colOTRATE.Visible = True
        Me.colOTRATE.VisibleIndex = 20
        '
        'colPERMISLATEARRIVAL
        '
        Me.colPERMISLATEARRIVAL.FieldName = "PERMISLATEARRIVAL"
        Me.colPERMISLATEARRIVAL.Name = "colPERMISLATEARRIVAL"
        Me.colPERMISLATEARRIVAL.Visible = True
        Me.colPERMISLATEARRIVAL.VisibleIndex = 21
        '
        'colPERMISEARLYDEPRT
        '
        Me.colPERMISEARLYDEPRT.FieldName = "PERMISEARLYDEPRT"
        Me.colPERMISEARLYDEPRT.Name = "colPERMISEARLYDEPRT"
        Me.colPERMISEARLYDEPRT.Visible = True
        Me.colPERMISEARLYDEPRT.VisibleIndex = 22
        '
        'colMAXDAYMIN
        '
        Me.colMAXDAYMIN.FieldName = "MAXDAYMIN"
        Me.colMAXDAYMIN.Name = "colMAXDAYMIN"
        Me.colMAXDAYMIN.Visible = True
        Me.colMAXDAYMIN.VisibleIndex = 23
        '
        'colISOS
        '
        Me.colISOS.FieldName = "ISOS"
        Me.colISOS.Name = "colISOS"
        Me.colISOS.Visible = True
        Me.colISOS.VisibleIndex = 24
        '
        'colTIME
        '
        Me.colTIME.FieldName = "TIME"
        Me.colTIME.Name = "colTIME"
        Me.colTIME.Visible = True
        Me.colTIME.VisibleIndex = 25
        '
        'colSHORT
        '
        Me.colSHORT.FieldName = "SHORT"
        Me.colSHORT.Name = "colSHORT"
        Me.colSHORT.Visible = True
        Me.colSHORT.VisibleIndex = 26
        '
        'colHALF
        '
        Me.colHALF.FieldName = "HALF"
        Me.colHALF.Name = "colHALF"
        Me.colHALF.Visible = True
        Me.colHALF.VisibleIndex = 27
        '
        'colISHALFDAY
        '
        Me.colISHALFDAY.FieldName = "ISHALFDAY"
        Me.colISHALFDAY.Name = "colISHALFDAY"
        Me.colISHALFDAY.Visible = True
        Me.colISHALFDAY.VisibleIndex = 28
        '
        'colISSHORT
        '
        Me.colISSHORT.FieldName = "ISSHORT"
        Me.colISSHORT.Name = "colISSHORT"
        Me.colISSHORT.Visible = True
        Me.colISSHORT.VisibleIndex = 29
        '
        'colTWO
        '
        Me.colTWO.FieldName = "TWO"
        Me.colTWO.Name = "colTWO"
        Me.colTWO.Visible = True
        Me.colTWO.VisibleIndex = 30
        '
        'colLastModifiedBy
        '
        Me.colLastModifiedBy.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy.Name = "colLastModifiedBy"
        '
        'colLastModifiedDate
        '
        Me.colLastModifiedDate.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate.Name = "colLastModifiedDate"
        '
        'RepositoryItemPopupContainerEdit1
        '
        Me.RepositoryItemPopupContainerEdit1.AutoHeight = False
        Me.RepositoryItemPopupContainerEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemPopupContainerEdit1.Name = "RepositoryItemPopupContainerEdit1"
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"Fixed", "Rotational", "Ignore"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'EmployeeGroupBindingSource
        '
        Me.EmployeeGroupBindingSource.DataMember = "EmployeeGroup"
        Me.EmployeeGroupBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'EmployeeGroupTableAdapter
        '
        Me.EmployeeGroupTableAdapter.ClearBeforeFill = True
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 10
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 2
        '
        'EmployeeGroup1TableAdapter
        '
        Me.EmployeeGroup1TableAdapter.ClearBeforeFill = True
        '
        'XtraEmployeeGroup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraEmployeeGroup"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemPopupContainerEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmployeeGroupBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents EmployeeGroupBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EmployeeGroupTableAdapter As iAS.SSSDBDataSetTableAdapters.EmployeeGroupTableAdapter
    Friend WithEvents RepositoryItemPopupContainerEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents colGroupId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGroupName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTTYPE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTPATTERN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTREMAINDAYS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLASTSHIFTPERFORMED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISAUTOSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAUTH_SHIFTS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFIRSTOFFDAY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSECONDOFFTYPE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHALFDAYSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSECONDOFFDAY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colALTERNATE_OFF_DAYS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINONLY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISPUNCHALL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISTIMELOSSALLOWED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCDAYS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISROUNDTHECLOCKWORK As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISOT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTRATE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPERMISLATEARRIVAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPERMISEARLYDEPRT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMAXDAYMIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISOS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHORT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHALF As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISHALFDAY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISSHORT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTWO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents EmployeeGroup1TableAdapter As iAS.SSSDBDataSetTableAdapters.EmployeeGroup1TableAdapter

End Class
