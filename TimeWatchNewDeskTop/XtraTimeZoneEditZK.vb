﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Public Class XtraTimeZoneEditZK
    Dim ulf As UserLookAndFeel
    Dim h As IntPtr = IntPtr.Zero
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim cn As Common = New Common
    Dim InNewRecord As Boolean = False
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub btnTZCreate_Click(sender As System.Object, e As System.EventArgs) Handles btnTZCreate.Click
        Dim lpszIPAddress As String
        Me.Cursor = Cursors.WaitCursor
        'Dim sSql As String = ""
        'Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        'Dim result As Object() = New Object(selectedRows.Length - 1) {}
        'Dim LstMachineId As String

        'If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
        'sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
        'Dim bConn As Boolean = False
        'vnMachineNumber = LstMachineId
        'vnLicense = 1261 '1789 '
        'lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
        'vpszIPAddress = Trim(lpszIPAddress)
        'vpszNetPort = CLng("5005")
        'vpszNetPassword = CLng("0")
        'vnTimeOut = CLng("5000")
        'vnProtocolType = 0 'PROTOCOL_TCPIP
        ''bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
        'If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
        '    vRet = FK_ConnectUSB(vnMachineNumber, vnLicense)
        'Else
        '    vpszIPAddress = Trim(lpszIPAddress)
        '    vpszNetPort = CLng("5005")
        '    vpszNetPassword = CLng("0")
        '    vnTimeOut = CLng("5000")
        '    vnProtocolType = 0
        '    vRet = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
        '    nCommHandleIndex = vRet
        'End If
        'If vRet > 0 Then
        '    bConn = True
        'Else
        '    bConn = False
        'End If
        'If bConn Then
        '    Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
        '    Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
        '    For x As Integer = 0 To selectedRowsEmp.Length - 1
        '        'LstEmployeesTarget.ListIndex = e
        '        Dim rowHandleEmp As Integer = selectedRowsEmp(x)
        '        If Not GridView2.IsGroupRow(rowHandleEmp) Then
        '            Dim EnrollNumber As String = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim)
        '            Dim FingerNumber As Long = 0 'GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
        '            Dim Uname As String = GridView2.GetRowCellValue(rowHandleEmp, "EMPNAME").ToString.Trim
        '            XtraMaster.LabelControl4.Text = "Uploading Template " & EnrollNumber
        '            Application.DoEvents()

        '            Dim vStrEnrollNumber As String = Trim(EnrollNumber)
        '            Dim vEnrollNumber As Long = Convert.ToUInt64(vStrEnrollNumber)
        '            If FK_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
        '                vRet = FK_SetUserName_StringID(vRet, vStrEnrollNumber, Trim(Uname))
        '            Else
        '                vRet = FK_SetUserName(vRet, vEnrollNumber, Trim(Uname))
        '            End If

        '            'finger validity
        '            vRet = FK_EnableDevice(nCommHandleIndex, 0)
        '            If (vRet <> CType(RUN_SUCCESS, Integer)) Then
        '                Continue For
        '            End If
        '            Dim vSize As Integer = (20 + 264)
        '            Dim bytExtCmd_USERDOORINFO() As Byte = New Byte((vSize) - 1) {}
        '            Dim vExtCmd_USERDOORINFO As Common.ExtCmd_USERDOORINFO = New Common.ExtCmd_USERDOORINFO
        '            vStrEnrollNumber = vEnrollNumber.ToString.Trim
        '            'vEnrollNumber = FKAttendDLL.GetInt(vStrEnrollNumber)
        '            vExtCmd_USERDOORINFO.Init("ECMD_SetUserDoorInfo", vEnrollNumber)
        '            vExtCmd_USERDOORINFO.StartYear = CType(DateEditStart.DateTime.ToString("yyyy"), Short)
        '            vExtCmd_USERDOORINFO.StartMonth = CType(DateEditStart.DateTime.ToString("MM"), Byte)
        '            vExtCmd_USERDOORINFO.StartDay = CType(DateEditStart.DateTime.ToString("dd"), Byte)
        '            vExtCmd_USERDOORINFO.EndYear = CType(DateEditEnd.DateTime.ToString("yyyy"), Short)
        '            vExtCmd_USERDOORINFO.EndMonth = CType(DateEditEnd.DateTime.ToString("MM"), Byte)
        '            vExtCmd_USERDOORINFO.EndDay = CType(DateEditEnd.DateTime.ToString("dd"), Byte)
        '            ConvertStructureToByteArray(vExtCmd_USERDOORINFO, bytExtCmd_USERDOORINFO)
        '            vRet = FK_ExtCommand(nCommHandleIndex, bytExtCmd_USERDOORINFO)
        '            vRet = FK_EnableDevice(nCommHandleIndex, 1)
        '            'End finger validity
        '            'cmdSetAllEnrollData_bio_All(vnMachineNumber, EnrollNumber, FingerNumber, Uname)
        '        End If
        '    Next x
        'Else
        '    'MsgBox("Device No: " & LstMachineId & " Not connected..")
        '    XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
        'End If
        ''FK623Attend.Disconnect
        'FK_DisConnect(nCommHandleIndex)
        'ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Then
        Dim sName As String = ""
        Dim sPassword As String = ""
        Dim sTmpData As String = ""
        Dim sEnabled As String = ""
        Dim bEnabled As Boolean = False

        lpszIPAddress = XtraTimeZoneZK.TZIP
        Dim vpszIPAddress As String = Trim(lpszIPAddress)
        'For ZK
        Dim bIsConnected = False
        Dim iMachineNumber As Integer = 1 'result(i)
        Dim idwErrorCode As Integer
        Dim com As Common = New Common
        'Dim axCZKEM1 As New zkemkeeper.CZKEM
        'bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
        'If bIsConnected = True Then
        '    Dim sTimeZone As String = TxtSunStrtTime.Text.Trim.Replace(":", "") & TxtSunEndTime.Text.Trim.Replace(":", "") &
        '        TxtMonStrtTime.Text.Trim.Replace(":", "") & TxtMonEndTime.Text.Trim.Replace(":", "") &
        '        TxtTueStrtTime.Text.Trim.Replace(":", "") & TxtTueEndTime.Text.Trim.Replace(":", "") &
        '        TxtWedStrtTime.Text.Trim.Replace(":", "") & TxtWedEndTime.Text.Trim.Replace(":", "") &
        '        TxtThuStrtTime.Text.Trim.Replace(":", "") & TxtThuEndTime.Text.Trim.Replace(":", "") &
        '        TxtFriStrtTime.Text.Trim.Replace(":", "") & TxtFriEndTime.Text.Trim.Replace(":", "") &
        '        TxtSatStrtTime.Text.Trim.Replace(":", "") & TxtSatEndTime3.Text.Trim.Replace(":", "")
        '    Dim vRet As Boolean = axCZKEM1.SetTZInfo(iMachineNumber, TextEditTZCre.Text.Trim, sTimeZone)
        '    'axCZKEM1.EnableDevice(iMachineNumber, True)
        '    Common.LogPost("Device TimeZone Set; Device Id='" & vpszIPAddress)
        'Else
        '    axCZKEM1.GetLastError(idwErrorCode)
        'End If

        ' For ZK End
        'For ZK Controller 
        ''Dim ConnectT = "protocol=TCP,ipaddress=" & lpszIPAddress.ToString.Trim & ",port=4370,timeout=2000,passwd="

        ''If IntPtr.Zero = h Then
        ''    h = ZKController.Connect(ConnectT)
        ''End If
        Dim Data As String = ""
        Dim SunTime1 As String = Hex(TxtSunStrtTime.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtSunEndTime.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim MonTime1 As String = Hex(TxtMonStrtTime.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtMonEndTime.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim TueTime1 As String = Hex(TxtTueStrtTime.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtTueEndTime.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim WedTime1 As String = Hex(TxtWedStrtTime.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtWedEndTime.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim ThuTime1 As String = Hex(TxtThuStrtTime.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtThuEndTime.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim FriTime1 As String = Hex(TxtFriStrtTime.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtFriEndTime.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim SatTime1 As String = Hex(TxtSatStrtTime.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtSatEndTime.Text.Trim.Replace(":", "")).PadLeft(4, "0")

        Dim SunTime2 As String = Hex(TxtSunStrtTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtSunEndTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim MonTime2 As String = Hex(TxtMonStrtTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtMonEndTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim TueTime2 As String = Hex(TxtTueStrtTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtTueEndTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim WedTime2 As String = Hex(TxtWedStrtTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtWedEndTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim ThuTime2 As String = Hex(TxtThuStrtTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtThuEndTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim FriTime2 As String = Hex(TxtFriStrtTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtFriEndTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim SatTime2 As String = Hex(TxtSatStrtTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtSatEndTime2.Text.Trim.Replace(":", "")).PadLeft(4, "0")

        Dim SunTime3 As String = Hex(TxtSunStrtTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtSunEndTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim MonTime3 As String = Hex(TxtMonStrtTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtMonEndTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim TueTime3 As String = Hex(TxtTueStrtTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtTueEndTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim WedTime3 As String = Hex(TxtWedStrtTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtWedEndTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim ThuTime3 As String = Hex(TxtThuStrtTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtThuEndTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim FriTime3 As String = Hex(TxtFriStrtTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtFriEndTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0")
        Dim SatTime3 As String = Hex(TxtSatStrtTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0") & Hex(TxtSatEndTime3.Text.Trim.Replace(":", "")).PadLeft(4, "0")

        Dim conSQL As SqlConnection = New SqlConnection(Common.ConnectionString)
        Dim sSql As String
        Dim cmd As New SqlCommand
        If Common.servername = "Access" Then

        Else
            Try


                If InNewRecord Then
                    If TextEditTZCre.Text.Trim = "" Then
                        XtraMessageBox.Show(ulf, "<size=10>Input TimeZone ID</size>", "<size=9>Error</size>")
                        Me.Cursor = Cursors.Default
                        TextEditTZCre.Select()

                        Exit Sub
                    End If


                    Dim adap As SqlDataAdapter
                    Dim adapA As OleDbDataAdapter
                    Dim ds As DataSet = New DataSet
                    sSql = "select TimeZoneID from ZKAccessTimeZone where TimeZoneID = '" & TextEditTZCre.Text.Trim & "'"
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(ds)
                    End If
                    If ds.Tables(0).Rows.Count > 0 Then
                        XtraMessageBox.Show(ulf, "<size=10>Duplicate TimeZone ID</size>", "<size=9>Error</size>")
                        Me.Cursor = Cursors.Default
                        TextEditTZCre.Select()

                        Exit Sub
                    End If

                    sSql = " Insert Into ZKAccessTimeZone (TimeZoneID,SunIn1,SunIn2,SunIn3,SunOut1,SunOut2,SunOut3,MonIn1,MonIn2,MonIn3,MonOut1,MonOut2,MonOut3,TueIn1, " &
                           " TueIn2,TueIn3,Tueout1,Tueout2,Tueout3,WedIn1,WedIn2,WedIn3,WedOut1,WedOut2,WedOut3,ThuIn1,ThuIn2,ThuIn3,ThuOut1 ,ThuOut2 ,ThuOut3,FriIn1,FriIn2,FriIn3,FriOut1,FriOut2,  " &
                           " FriOut3,Satin1,Satin2,Satin3,SatOut1,SatOut2,SatOut3) Values ('" & TextEditTZCre.Text & "','" & TxtSunStrtTime.Text & "','" & TxtSunStrtTime2.Text & "','" & TxtSunStrtTime3.Text & "','" & TxtSunEndTime.Text & "','" & TxtSunEndTime3.Text & "','" & TxtSunEndTime3.Text & "' " &
                           " ,'" & TxtMonStrtTime.Text & "','" & TxtMonStrtTime2.Text & "','" & TxtMonStrtTime3.Text & "','" & TxtMonEndTime.Text & "','" & TxtMonEndTime2.Text & "','" & TxtMonEndTime3.Text & "'" &
                           " ,'" & TxtTueStrtTime.Text & "','" & TxtTueStrtTime2.Text & "','" & TxtTueStrtTime3.Text & "','" & TxtTueEndTime.Text & "','" & TxtTueEndTime2.Text & "','" & TxtTueEndTime3.Text & "'" &
                           " ,'" & TxtWedStrtTime.Text & "','" & TxtWedStrtTime2.Text & "','" & TxtWedStrtTime3.Text & "','" & TxtWedEndTime.Text & "','" & TxtWedEndTime2.Text & "','" & TxtWedEndTime3.Text & "'" &
                            " ,'" & TxtThuStrtTime.Text & "','" & TxtThuStrtTime2.Text & "','" & TxtThuStrtTime3.Text & "','" & TxtThuEndTime.Text & "','" & TxtThuEndTime2.Text & "','" & TxtThuEndTime3.Text & "'" &
                            " ,'" & TxtFriStrtTime.Text & "','" & TxtFriStrtTime2.Text & "','" & TxtFriStrtTime3.Text & "','" & TxtFriEndTime.Text & "','" & TxtFriEndTime2.Text & "','" & TxtFriEndTime3.Text & "'" &
                            " ,'" & TxtSatStrtTime.Text & "','" & TxtSatStrtTime2.Text & "','" & TxtSatStrtTime3.Text & "','" & TxtSatEndTime.Text & "','" & TxtSatEndTime2.Text & "','" & TxtSatEndTime3.Text & "')"
                Else
                    sSql = "update ZKAccessTimeZone set SunIn1='" & TxtSunStrtTime.Text & "',SunIn2='" & TxtSunStrtTime2.Text & "',SunIn3='" & TxtSunStrtTime3.Text & "',SunOut1='" & TxtSunEndTime.Text & "',SunOut2='" & TxtSunEndTime2.Text & "',SunOut3='" & TxtSunEndTime3.Text & "',MonIn1='" & TxtMonStrtTime.Text & "',MonIn2='" & TxtMonStrtTime2.Text & "',MonIn3='" & TxtMonStrtTime3.Text & "',MonOut1='" & TxtMonEndTime.Text & "',MonOut2='" & TxtMonEndTime2.Text & "',MonOut3='" & TxtMonEndTime3.Text & "',TueIn1='" & TxtTueStrtTime.Text & "', " &
                           " TueIn2='" & TxtTueStrtTime2.Text & "',TueIn3='" & TxtTueStrtTime3.Text & "',Tueout1='" & TxtTueEndTime.Text & "',Tueout2='" & TxtTueEndTime2.Text & "',Tueout3='" & TxtTueEndTime3.Text & "',WedIn1='" & TxtWedStrtTime.Text & "',WedIn2='" & TxtWedStrtTime2.Text & "',WedIn3='" & TxtWedStrtTime3.Text & "',WedOut1='" & TxtWedEndTime.Text & "',WedOut2='" & TxtWedEndTime2.Text & "',WedOut3='" & TxtWedEndTime3.Text & "',ThuIn1='" & TxtThuStrtTime.Text & "',ThuIn2='" & TxtThuStrtTime2.Text & "',ThuIn3='" & TxtThuStrtTime3.Text & "',ThuOut1='" & TxtThuEndTime.Text & "' ,ThuOut2='" & TxtThuEndTime2.Text & "' ,ThuOut3='" & TxtThuEndTime3.Text & "',FriIn1='" & TxtFriStrtTime.Text & "',FriIn2='" & TxtFriStrtTime2.Text & "',FriIn3='" & TxtFriStrtTime3.Text & "',FriOut1='" & TxtFriEndTime.Text & "',FriOut2='" & TxtFriEndTime2.Text & "',  " &
                           " FriOut3='" & TxtFriEndTime3.Text & "',Satin1='" & TxtSatStrtTime.Text & "',Satin2='" & TxtSatStrtTime2.Text & "',Satin3='" & TxtSatStrtTime3.Text & "',SatOut1='" & TxtSatEndTime3.Text & "',SatOut2='" & TxtSatEndTime2.Text & "',SatOut3 ='" & TxtSatEndTime3.Text & "' where  TimeZoneID='" & TextEditTZCre.Text & "'"
                End If
                If conSQL.State <> ConnectionState.Open Then
                    conSQL.Open()
                End If

                cmd = New SqlCommand(sSql, conSQL)
                cmd.ExecuteNonQuery()

                If conSQL.State <> ConnectionState.Closed Then
                    conSQL.Close()
                End If
            Catch ex As Exception
                If conSQL.State <> ConnectionState.Closed Then
                    conSQL.Close()
                End If
            End Try
        End If

        ''Dim ret As Integer = 0
        ''If h <> IntPtr.Zero Then
        ''    Dim DeleteData As String = ""

        ''    Data = "TimezoneId = 3" & vbTab & "SunTime1 = " & CInt("&H" & SunTime1) & " " & vbTab & "SunTime2 = " & CInt("&H" & SunTime1) & " " & vbTab & "SunTime3 = " & CInt("&H" & SunTime1) & " " & vbTab & "MonTime1 = " & CInt("&H" & MonTime1) & " " & vbTab & "MonTime2 = " & CInt("&H" & MonTime1) & " " & vbTab & "MonTime3 = " & CInt("&H" & MonTime1) & " " & vbTab & "TueTime1 = " & CInt("&H" & TueTime1) & " " & vbTab & "TueTime2 = " & CInt("&H" & TueTime1) & " " & vbTab & "TueTime3 = " & CInt("&H" & TueTime1) & " " & vbTab & "WedTime1 = " & CInt("&H" & WedTime1) & " " & vbTab & "WedTime2 = " & CInt("&H" & WedTime1) & " " & vbTab & "WedTime3 = " & CInt("&H" & WedTime1) & " " & vbTab & "ThuTime1 = " & CInt("&H" & ThuTime1) & " " & vbTab & "ThuTime2 = " & CInt("&H" & ThuTime1) & " " & vbTab & "ThuTime3 = " & CInt("&H" & ThuTime1) & " " & vbTab & "FriTime1 = " & CInt("&H" & FriTime1) & " " & vbTab & "FriTime2 = " & CInt("&H" & FriTime1) & " " & vbTab & "FriTime3 = " & CInt("&H" & FriTime1) & " " & vbTab & "SatTime1 = " & CInt("&H" & SatTime1) & " " & vbTab & "SatTime2 = " & CInt("&H" & SatTime1) & " " & vbTab & "SatTime3 = " & CInt("&H" & SatTime1) & " " & vbTab & "Hol1Time1 = " & CInt("&H" & SunTime1) & " " & vbTab & "Hol1Time2 = " & CInt("&H" & SunTime1) & " " & vbTab & "Hol1Time3 = " & CInt("&H" & SunTime1) & " " & vbTab & "Hol2Time1 = " & CInt("&H" & SunTime1) & " " & vbTab & "Hol2Time2=" & CInt("&H" & SunTime1) & " " & vbTab & "Hol2Time3=" & CInt("&H" & SunTime1) & " " & vbTab & "Hol3Time1=" & CInt("&H" & SunTime1) & " " & vbTab & "Hol3Time2=" & CInt("&H" & SunTime1) & " " & vbTab & "Hol3Time3=" & CInt("&H" & SunTime1) & " "
        ''    Data = Data.Replace(" ", "")
        ''    ret = ZKController.SetDeviceData(h, "timezone", Data, "")
        ''End If

        'For ZK Controller End



        Me.Cursor = Cursors.Default
        Me.Close()
    End Sub
    Public Function getTimeZKAcces(value As String) As String
        value = Convert.ToInt64(value).ToString("00000000")
        Dim HexVal As String = Hex(value).PadLeft(8, "0")
        Dim HexStart As String = HexVal.Substring(0, 4)
        Dim HexEnd As String = HexVal.Substring(4, HexStart.Length)
        Dim sTime As String = CInt("&H" & HexStart)
        Dim eTime As String = CInt("&H" & HexEnd)
        Dim startTime As String = sTime.ToString.PadLeft(4, "0")
        Dim EndTime As String = eTime.ToString.PadLeft(4, "0")

        startTime = startTime.Substring(0, 2) & ":" & startTime.Substring(2, 2)
        EndTime = EndTime.Substring(0, 2) & ":" & EndTime.Substring(2, 2)


        Return startTime & "," & EndTime
    End Function
    Private Sub XtraTimeZoneEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        TextEditTZCre.Text = XtraTimeZoneZK.TZID
        If XtraTimeZoneZK.TZID = "" Then
            InNewRecord = True
            TextEditTZCre.Properties.ReadOnly = False
        Else
            InNewRecord = False
            TextEditTZCre.Properties.ReadOnly = True
        End If

        If TextEditTZCre.Text.ToString.Trim <> "" Then
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim sSql As String
            sSql = "select * from ZKAccessTimeZone where TimeZoneID = '" & TextEditTZCre.Text.Trim & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                'Salb One
                TxtSunStrtTime.Text = ds.Tables(0).Rows(0)("SunIn1").ToString.Trim
                TxtSunEndTime.Text = ds.Tables(0).Rows(0)("SunOut1").ToString.Trim

                TxtMonStrtTime.Text = ds.Tables(0).Rows(0)("MonIn1").ToString.Trim
                TxtMonEndTime.Text = ds.Tables(0).Rows(0)("MonOut1").ToString.Trim

                TxtTueStrtTime.Text = ds.Tables(0).Rows(0)("TueIn1").ToString.Trim
                TxtTueEndTime.Text = ds.Tables(0).Rows(0)("Tueout1").ToString.Trim

                TxtWedStrtTime.Text = ds.Tables(0).Rows(0)("WedIn1").ToString.Trim
                TxtWedEndTime.Text = ds.Tables(0).Rows(0)("WedOut1").ToString.Trim

                TxtThuStrtTime.Text = ds.Tables(0).Rows(0)("ThuIn1").ToString.Trim
                TxtThuEndTime.Text = ds.Tables(0).Rows(0)("ThuOut1").ToString.Trim

                TxtFriStrtTime.Text = ds.Tables(0).Rows(0)("FriIn1").ToString.Trim
                TxtFriEndTime.Text = ds.Tables(0).Rows(0)("FriOut1").ToString.Trim

                TxtSatStrtTime.Text = ds.Tables(0).Rows(0)("Satin1").ToString.Trim
                TxtSatEndTime.Text = ds.Tables(0).Rows(0)("SatOut1").ToString.Trim

                'Salb Two
                TxtSunStrtTime2.Text = ds.Tables(0).Rows(0)("SunIn2").ToString.Trim
                TxtSunEndTime2.Text = ds.Tables(0).Rows(0)("SunOut2").ToString.Trim

                TxtMonStrtTime2.Text = ds.Tables(0).Rows(0)("MonIn2").ToString.Trim
                TxtMonEndTime2.Text = ds.Tables(0).Rows(0)("MonOut2").ToString.Trim

                TxtTueStrtTime2.Text = ds.Tables(0).Rows(0)("TueIn2").ToString.Trim
                TxtTueEndTime2.Text = ds.Tables(0).Rows(0)("Tueout2").ToString.Trim

                TxtWedStrtTime2.Text = ds.Tables(0).Rows(0)("WedIn2").ToString.Trim
                TxtWedEndTime2.Text = ds.Tables(0).Rows(0)("WedOut2").ToString.Trim

                TxtThuStrtTime2.Text = ds.Tables(0).Rows(0)("ThuIn2").ToString.Trim
                TxtThuEndTime2.Text = ds.Tables(0).Rows(0)("ThuOut2").ToString.Trim

                TxtFriStrtTime2.Text = ds.Tables(0).Rows(0)("FriIn2").ToString.Trim
                TxtFriEndTime2.Text = ds.Tables(0).Rows(0)("FriOut2").ToString.Trim

                TxtSatStrtTime2.Text = ds.Tables(0).Rows(0)("Satin2").ToString.Trim
                TxtSatEndTime2.Text = ds.Tables(0).Rows(0)("SatOut2").ToString.Trim



                'Salb Three
                TxtSunStrtTime3.Text = ds.Tables(0).Rows(0)("SunIn3").ToString.Trim
                TxtSunEndTime3.Text = ds.Tables(0).Rows(0)("SunOut3").ToString.Trim

                TxtMonStrtTime3.Text = ds.Tables(0).Rows(0)("MonIn3").ToString.Trim
                TxtMonEndTime3.Text = ds.Tables(0).Rows(0)("MonOut3").ToString.Trim

                TxtTueStrtTime3.Text = ds.Tables(0).Rows(0)("TueIn3").ToString.Trim
                TxtTueEndTime3.Text = ds.Tables(0).Rows(0)("Tueout3").ToString.Trim

                TxtWedStrtTime3.Text = ds.Tables(0).Rows(0)("WedIn3").ToString.Trim
                TxtWedEndTime3.Text = ds.Tables(0).Rows(0)("WedOut3").ToString.Trim

                TxtThuStrtTime3.Text = ds.Tables(0).Rows(0)("ThuIn3").ToString.Trim
                TxtThuEndTime3.Text = ds.Tables(0).Rows(0)("ThuOut3").ToString.Trim

                TxtFriStrtTime3.Text = ds.Tables(0).Rows(0)("FriIn3").ToString.Trim
                TxtFriEndTime3.Text = ds.Tables(0).Rows(0)("FriOut3").ToString.Trim

                TxtSatStrtTime3.Text = ds.Tables(0).Rows(0)("Satin3").ToString.Trim
                TxtSatEndTime3.Text = ds.Tables(0).Rows(0)("SatOut3").ToString.Trim
            End If




            'Dim TZarray() As String = XtraTimeZoneZK.TZTime.Split(" ")
            'TxtSunStrtTime.Text = TZarray(0)
            'TxtSunEndTime.Text = TZarray(1)

            'TxtMonStrtTime.Text = TZarray(2)
            'TxtMonEndTime.Text = TZarray(3)

            'TxtTueStrtTime.Text = TZarray(4)
            'TxtTueEndTime.Text = TZarray(5)

            'TxtWedStrtTime.Text = TZarray(6)
            'TxtWedEndTime.Text = TZarray(7)

            'TxtThuStrtTime.Text = TZarray(8)
            'TxtThuEndTime.Text = TZarray(9)

            'TxtFriStrtTime.Text = TZarray(10)
            'TxtFriEndTime.Text = TZarray(11)

            'TxtSatStrtTime.Text = TZarray(12)
            'TxtSatEndTime.Text = TZarray(13)
        Else
            TxtSunStrtTime.Text = "00:00"
            TxtSunEndTime.Text = "23:59"

            TxtMonStrtTime.Text = "00:00"
            TxtMonEndTime.Text = "23:59"

            TxtTueStrtTime.Text = "00:00"
            TxtTueEndTime.Text = "23:59"

            TxtWedStrtTime.Text = "00:00"
            TxtWedEndTime.Text = "23:59"

            TxtThuStrtTime.Text = "00:00"
            TxtThuEndTime.Text = "23:59"

            TxtFriStrtTime.Text = "00:00"
            TxtFriEndTime.Text = "23:59"

            TxtSatStrtTime.Text = "00:00"
            TxtSatEndTime.Text = "23:59"


            TxtSunStrtTime2.Text = "00:00"
            TxtSunEndTime2.Text = "23:59"

            TxtMonStrtTime2.Text = "00:00"
            TxtMonEndTime2.Text = "23:59"

            TxtTueStrtTime2.Text = "00:00"
            TxtTueEndTime2.Text = "23:59"

            TxtWedStrtTime2.Text = "00:00"
            TxtWedEndTime2.Text = "23:59"

            TxtThuStrtTime2.Text = "00:00"
            TxtThuEndTime2.Text = "23:59"

            TxtFriStrtTime2.Text = "00:00"
            TxtFriEndTime2.Text = "23:59"

            TxtSatStrtTime2.Text = "00:00"
            TxtSatEndTime2.Text = "23:59"


            TxtSunStrtTime3.Text = "00:00"
            TxtSunEndTime3.Text = "23:59"

            TxtMonStrtTime3.Text = "00:00"
            TxtMonEndTime3.Text = "23:59"

            TxtTueStrtTime3.Text = "00:00"
            TxtTueEndTime3.Text = "23:59"

            TxtWedStrtTime3.Text = "00:00"
            TxtWedEndTime3.Text = "23:59"

            TxtThuStrtTime3.Text = "00:00"
            TxtThuEndTime3.Text = "23:59"

            TxtFriStrtTime3.Text = "00:00"
            TxtFriEndTime3.Text = "23:59"

            TxtSatStrtTime3.Text = "00:00"
            TxtSatEndTime3.Text = "23:59"

        End If

    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
End Class