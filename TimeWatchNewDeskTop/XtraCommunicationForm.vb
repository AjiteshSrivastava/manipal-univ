﻿Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports System.Text
Imports DevExpress.XtraSplashScreen
Imports DevExpress.XtraEditors
Imports DevExpress.LookAndFeel
Imports Riss.Devices 'bio2+
Imports iAS.HSeriesSampleCSharp
Imports System.Runtime.InteropServices
Imports CMITech.UMXClient
Imports System.Drawing.Imaging

Public Class XtraCommunicationForm
    Dim ConnectionString As String
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim DC As New DateConverter()
    Dim h As IntPtr = IntPtr.Zero
    Public Sub New()
        InitializeComponent()
        'MsgBox("")
        'If Common.servername = "Access" Then
        '    'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        'Else
        '    'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
        '    TblMachineTableAdapter1.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
        '    Me.TblMachineTableAdapter1.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        'End If
        GridControl1.DataSource = Common.MachineNonAdmin
        Common.SetGridFont(GridView1, New Font("Tahoma", 9))
    End Sub
    Private Sub XtraCommunicationForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Me.Width = Common.NavWidth 'Me.Parent.Width
        'Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = SplitContainerControl1.Parent.Width  'Common.splitforMasterMenuWidth '
        'SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100 
        'MsgBox(SplitContainerControl1.Width)
        'MsgBox(Common.SplitterPosition)
        'MsgBox(SplitContainerControl1.SplitterPosition)

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraDevice).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")
        LabelControl1.Text = Common.res_man.GetString("selectdevice", Common.cul)
        LabelControl2.Text = Common.res_man.GetString("download_from_all", Common.cul)
        LabelControl3.Text = Common.res_man.GetString("delete_from_device", Common.cul)
        ToggleSwitch1.Properties.OffText = Common.res_man.GetString("off", Common.cul)
        ToggleSwitch1.Properties.OnText = Common.res_man.GetString("onn", Common.cul)
        ToggleSwitch2.Properties.OffText = Common.res_man.GetString("off", Common.cul)
        ToggleSwitch2.Properties.OnText = Common.res_man.GetString("onn", Common.cul)
        'CheckEdit1.Text = Common.res_man.GetString("oldlog", Common.cul)
        'CheckEdit2.Text = Common.res_man.GetString("newlog", Common.cul)
        SimpleButtonRead.Text = Common.res_man.GetString("readdate", Common.cul)
        SimpleButtonSet.Text = Common.res_man.GetString("setdate", Common.cul)
        SimpleButtonDownload.Text = Common.res_man.GetString("downloadlogs", Common.cul)
        colID_NO.Caption = Common.res_man.GetString("controller_id", Common.cul)
        colLOCATION.Caption = Common.res_man.GetString("device_ip", Common.cul)
        colbranch.Caption = Common.res_man.GetString("location", Common.cul)


        ''this is if device modified in device grid then immidiate update in device list of download device select 
        'If Common.servername = "Access" Then
        '    'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        'Else
        '    'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
        '    TblMachineTableAdapter1.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
        '    Me.TblMachineTableAdapter1.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        'End If
        GridControl1.DataSource = Common.MachineNonAdmin
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        PopupContainerEdit1.EditValue = ""
        TextEdit1.Text = ""
        CheckEdit2.Checked = True
        ToggleSwitch2.IsOn = False
        ToggleEmp.IsOn = False
        CheckEditTimePerioad.Checked = False
        DateEditFrmDate.Enabled = False
        DateEditToDate.Enabled = False
        LabelControl4.Enabled = False
        LabelControl5.Enabled = False
        DateEditFrmDate.DateTime = Now.AddDays(-1)
        DateEditToDate.DateTime = Now

        If CheckEditTimePerioad.Checked = True Then
            ComboNYearStr.Enabled = True
            ComboNMonthStr.Enabled = True
            ComboNDateStr.Enabled = True
            ComboNYearEnd.Enabled = True
            ComboNMonthEnd.Enabled = True
            ComboNDateEnd.Enabled = True
            LabelControl4.Enabled = True
            LabelControl5.Enabled = True
        Else
            ComboNYearStr.Enabled = False
            ComboNMonthStr.Enabled = False
            ComboNDateStr.Enabled = False
            ComboNYearEnd.Enabled = False
            ComboNMonthEnd.Enabled = False
            ComboNDateEnd.Enabled = False
            LabelControl4.Enabled = False
            LabelControl5.Enabled = False
        End If

        If Common.IsNepali = "Y" Then
            DateEditFrmDate.Visible = False
            DateEditToDate.Visible = False
            ComboNYearStr.Visible = True
            ComboNMonthStr.Visible = True
            ComboNDateStr.Visible = True
            ComboNYearEnd.Visible = True
            ComboNMonthEnd.Visible = True
            ComboNDateEnd.Visible = True

            'Dim doj As DateTime = DC.ToBS(New Date(DateEditFrmDate.DateTime.Year, DateEditFrmDate.DateTime.Month, DateEditFrmDate.DateTime.Day))
            Dim doj As String = DC.ToBS(New Date(DateEditFrmDate.DateTime.Year, DateEditFrmDate.DateTime.Month, DateEditFrmDate.DateTime.Day))
            Dim dojTmp() As String = doj.Split("-")
            ComboNYearStr.EditValue = dojTmp(0)
            ComboNMonthStr.SelectedIndex = dojTmp(1) - 1
            ComboNDateStr.EditValue = dojTmp(2)

            doj = DC.ToBS(New Date(DateEditToDate.DateTime.Year, DateEditToDate.DateTime.Month, DateEditToDate.DateTime.Day))
            dojTmp = doj.Split("-")
            ComboNYearEnd.EditValue = dojTmp(0)
            ComboNMonthEnd.SelectedIndex = dojTmp(1) - 1
            ComboNDateEnd.EditValue = dojTmp(2)
        Else
            DateEditFrmDate.Visible = True
            DateEditToDate.Visible = True
            ComboNYearStr.Visible = False
            ComboNMonthStr.Visible = False
            ComboNDateStr.Visible = False
            ComboNYearEnd.Visible = False
            ComboNMonthEnd.Visible = False
            ComboNDateEnd.Visible = False
        End If
        GridView1.ClearSelection()
    End Sub
    Private Sub PopupContainerEdit1_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit1.QueryPopUp
        Dim val As Object = PopupContainerEdit1.EditValue
        If (val Is Nothing) Then
            GridView1.ClearSelection()
        Else
            'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
                Dim rowHandle As Integer = GridView1.LocateByValue("ID_NO", text)
                GridView1.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        Dim selectedRows() As Integer = GridView1.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridView1.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("ID_NO"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub SimpleButton4_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton4.Click
        'Dim comclass As Common = New Common
        'comclass.Load_Corporate_PolicySql()
        'Me.Cursor = Cursors.WaitCursor

        'comclass.Process_AllnonRTC(DateEdit3.DateTime, Now.Date, "000000000000", "zzzzzzzzzzzz")
        'comclass.Process_AllRTC(DateEdit3.DateTime, Now.Date, "000000000000", "zzzzzzzzzzzz")
        'Me.Cursor = Cursors.Default
        Me.Close()
    End Sub
    Private Sub SimpleButtonRead_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonRead.Click
        If PopupContainerEdit1.EditValue.ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select device</size>", "Failed")
            PopupContainerEdit1.Select()
            Exit Sub
        End If
        Dim a As DateTime
        Dim c As Common = New Common()
        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R As Object
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        Me.Cursor = Cursors.WaitCursor
        IP = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("LOCATION"))
        Dim commkey As Integer '= Convert.ToInt32(GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("commkey")))
        ID_NO = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("ID_NO"))
        DeviceType = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("DeviceType"))
        A_R = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("A_R"))
        Common.LogPost("Device Get DateTime; Device ID: " & ID_NO)
        If DeviceType = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
            vnMachineNumber = ID_NO.ToString  '1
            vpszIPAddress = IP.ToString '"192.168.0.111"
            vpszNetPort = 5005
            vpszNetPassword = 0
            vnTimeOut = 5000
            vnProtocolType = PROTOCOL_TCPIP
            vnLicense = 1261
            Dim gnCommHandleIndex As Long
            If A_R = "S" Then
                gnCommHandleIndex = FK_ConnectUSB(vnMachineNumber, vnLicense)
            Else
                gnCommHandleIndex = c.ConnectToDevice(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
            End If
            If gnCommHandleIndex > 0 Then
                FK_GetDeviceTime(gnCommHandleIndex, a)
                If Common.IsNepali = "Y" Then
                    'Dim doj As DateTime = DC.ToBS(New Date(a.Year, a.Month, a.Day))
                    Dim doj As String = DC.ToBS(New Date(a.Year, a.Month, a.Day))
                    Dim dojTmp() As String = doj.Split("-")
                    TextEdit1.Text = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0) & " " & a.ToString("HH:mm:ss")
                Else
                    TextEdit1.Text = a.ToString("yyyy-MM-dd HH:mm:ss")
                End If
                FK_DisConnect(gnCommHandleIndex)
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        ElseIf DeviceType = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
            commkey = Convert.ToInt32(GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("commkey")))
            Dim axCZKEM1tmp As New zkemkeeper.CZKEM
            Dim comZK As CommonZK = New CommonZK
            axCZKEM1tmp.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
            Dim result As Boolean = comZK.connectZK(IP, 4370, axCZKEM1tmp)
            If result = True Then
                Dim iMachineNumber As Integer = 1
                Dim idwErrorCode As Integer

                Dim idwYear As Integer
                Dim idwMonth As Integer
                Dim idwDay As Integer
                Dim idwHour As Integer
                Dim idwMinute As Integer
                Dim idwSecond As Integer

                If axCZKEM1tmp.GetDeviceTime(iMachineNumber, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond) = True Then
                    axCZKEM1tmp.RefreshData(iMachineNumber) 'the data in the device should be refreshed
                    If Common.IsNepali = "Y" Then
                        'Dim doj As DateTime = DC.ToBS(New Date(idwYear.ToString(), idwMonth.ToString(), idwDay.ToString()))
                        Dim doj As String = DC.ToBS(New Date(idwYear.ToString(), idwMonth.ToString(), idwDay.ToString()))
                        Dim dojTmp() As String = doj.Split("-")
                        TextEdit1.Text = dojTmp(2) & "-" & Common.NepaliMonth(dojTmp(1) - 1) & "-" & dojTmp(0) & " " & idwHour.ToString() & ":" & idwMinute.ToString() & ":" & idwSecond.ToString()
                    Else
                        TextEdit1.Text = idwYear.ToString("0000") & "-" & idwMonth.ToString("00") & "-" & idwDay.ToString("00") & " " & idwHour.ToString("00") & ":" & idwMinute.ToString("00") & ":" & idwSecond.ToString("00")
                    End If
                Else
                    axCZKEM1tmp.GetLastError(idwErrorCode)
                    XtraMessageBox.Show(ulf, "<size=10>Operation failed,ErrorCode=" & idwErrorCode.ToString() & "</size>", "Failed")
                End If
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            comZK.DisconnectZK(axCZKEM1tmp)
        ElseIf DeviceType = "Bio2+" Then
            commkey = Convert.ToInt32(GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("commkey")))
            Dim deviceG2 As Riss.Devices.Device
            Dim deviceConnectionG2 As Riss.Devices.DeviceConnection
            deviceG2 = New Riss.Devices.Device()
            deviceG2.DN = 1 'CInt(nud_DN.Value)
            deviceG2.Password = commkey ' nud_Pwd.Value.ToString()
            deviceG2.Model = "ZDC2911"
            deviceG2.ConnectionModel = 5
            If A_R = "S" Then
                deviceG2.CommunicationType = CommunicationType.Usb
            Else
                deviceG2.IpAddress = IP
                deviceG2.IpPort = 5500 'CInt(nud_Port.Value)
                deviceG2.CommunicationType = CommunicationType.Tcp
            End If
            deviceConnectionG2 = DeviceConnection.CreateConnection(deviceG2)
            If deviceConnectionG2.Open() > 0 Then
                Try
                    Dim extraProperty As New Object()
                    Dim extraData As New Object()
                    Dim result As Boolean = deviceConnectionG2.GetProperty(DeviceProperty.DeviceTime, extraProperty, deviceG2, extraData)
                    If result Then
                        Dim dt As DateTime = DirectCast(extraData, DateTime)
                        TextEdit1.Text = dt.ToString("yyyy-MM-dd HH:mm:ss")
                    Else
                        deviceConnectionG2.Close()
                        XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                        Me.Cursor = Cursors.Default
                        Exit Sub
                    End If
                Catch ex As Exception
                    deviceConnectionG2.Close()
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                    'MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.[Error])
                End Try
                deviceConnectionG2.Close()
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        ElseIf DeviceType = "Bio1Eco" Then
            Dim adap, adap1 As SqlDataAdapter
            Dim adapA, adapA1 As OleDbDataAdapter
            Dim ds, ds1 As DataSet
            commkey = Convert.ToInt32(GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("commkey")))
            Dim mOpenFlag As Boolean
            Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

            Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
            Dim equNo As UInt32 = Convert.ToUInt32(ID_NO)
            Dim gEquNo As UInt32 = equNo
            If A_R = "T" Then
                Dim netCfg As SyNetCfg
                Dim pswd As UInt32 = 0
                Dim tmpPswd As String = commkey
                If (tmpPswd = "0") Then
                    tmpPswd = "FFFFFFFF"
                End If
                Try
                    pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                    ret = CType(SyLastError.slePasswordError, Int32)
                End Try
                If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                    netCfg.mIsTCP = 1
                    netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                    netCfg.mIPAddr = New Byte((4) - 1) {}
                    Dim sArray() As String = IP.Split(Microsoft.VisualBasic.ChrW(46))
                    Dim j As Byte = 0
                    For Each i As String In sArray
                        Try
                            netCfg.mIPAddr(j) = Convert.ToByte(i.Trim)
                        Catch ex As System.Exception
                            netCfg.mIPAddr(j) = 255
                        End Try
                        j = (j + 1)
                        If j > 3 Then
                            Exit For
                        End If

                    Next
                End If

                Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                Try
                    Marshal.StructureToPtr(netCfg, pnt, False)
                    '        If optWifiDevice.Checked Then
                    'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                    '        Else
                    ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                    '        End If
                    '                'TODO: Warning!!!, inline IF is not supported ?
                    '                chkTranceIO.Checked()
                    '0:
                    '                '
                Finally
                    Marshal.FreeHGlobal(pnt)
                End Try
            ElseIf A_R = "S" Then
                Dim equtype As String = "Finger Module USB Device"
                Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
            End If
            If (ret = CType(SyLastError.sleSuss, Int32)) Then
                ret = SyFunctions.CmdTestConn2Device

                Dim y As UInt16 = 0
                Dim M As Byte = 0, d = 0, h = 0, m1 = 0, s = 0
                ret = SyFunctions.CmdGetTime(y, M, d, h, m1, s)
                If (ret = CType(SyLastError.sleSuss, Int32)) Then
                    If Common.IsNepali = "Y" Then
                        'Dim doj As DateTime = DC.ToBS(New Date(y.ToString(), M.ToString(), d.ToString()))
                        Dim Vstart As String = DC.ToBS(New Date(y.ToString(), M.ToString(), d.ToString()))
                        Dim dojTmp() As String = Vstart.Split("-")
                        TextEdit1.Text = dojTmp(2) & "-" & Common.NepaliMonth(dojTmp(1) - 1) & "-" & dojTmp(0) & " " & h.ToString() & ":" & m1.ToString() & ":" & s.ToString()

                        'TextEdit1.Text = doj.Day & "-" & Common.NepaliMonth(doj.Month - 1) & "-" & doj.Year & " " & h.ToString() & ":" & m1.ToString() & ":" & s.ToString()
                    Else
                        TextEdit1.Text = y.ToString("0000") & "-" & M.ToString("00") & "-" & d.ToString("00") & " " & h.ToString("00") & ":" & m1.ToString("00") & ":" & s.ToString("00")
                    End If
                Else
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If

                SyFunctions.CloseCom()
                ' close com 

                SyFunctions.CmdUnLockDevice()
                SyFunctions.CloseCom()
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        ElseIf DeviceType = "F9" Then
            vnMachineNumber = ID_NO.ToString  '1
            vpszIPAddress = IP.ToString '"192.168.0.111"
            vpszNetPort = 5005
            vpszNetPassword = 0
            vnTimeOut = 5000
            vnProtocolType = PROTOCOL_TCPIP
            vnLicense = 7881
            Dim gnCommHandleIndex As Long
            Dim F9 As mdlPublic_f9 = New mdlPublic_f9
            If A_R = "S" Then
                gnCommHandleIndex = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
            Else
                gnCommHandleIndex = F9.FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
            End If
            If gnCommHandleIndex > 0 Then
                F9.FK_GetDeviceTime(gnCommHandleIndex, a)
                If Common.IsNepali = "Y" Then
                    'Dim doj As DateTime = DC.ToBS(New Date(a.Year, a.Month, a.Day))
                    Dim doj As String = DC.ToBS(New Date(a.Year, a.Month, a.Day))
                    Dim dojTmp() As String = doj.Split("-")
                    TextEdit1.Text = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0) & " " & a.ToString("HH:mm:ss")
                Else
                    TextEdit1.Text = a.ToString("yyyy-MM-dd HH:mm:ss")
                End If
                F9.FK_DisConnect(gnCommHandleIndex)
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        ElseIf DeviceType = "ATF686n" Then
            vpszIPAddress = IP.ToString
            vpszNetPort = CLng("5005")
            vpszNetPassword = CLng("0")
            vnTimeOut = CLng("5000")
            vnProtocolType = 0
            vnLicense = 7881
            Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
            'If A_R = "S" Then
            '    vnResult = F9.FK_ConnectUSB(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vnLicense)
            'Else
            Dim vnResult As Integer = atf686n.ConnectNet(vpszIPAddress)
            Dim vdwDate As DateTime = DateTime.Now
            Dim strDataTime As String
            Dim vnResultCode As Integer


            vnResultCode = atf686n.ST_EnableDevice(vnResult, 0)
            If vnResultCode <> CInt(enumErrorCode.RETURN_SUCCESS) Then
                Me.Cursor = Cursors.Default
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
            Else
                vnResultCode = atf686n.ST_GetDeviceTime(vnResult, vdwDate)
                If vnResultCode = CInt(enumErrorCode.RETURN_SUCCESS) Then
                    strDataTime = String.Format("{0:yyyy-MM-dd HH:mm:ss}", vdwDate)
                    TextEdit1.Text = vdwDate.ToString("yyyy-MM-dd HH:mm:ss")
                Else
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    TextEdit1.Text = ""
                End If
            End If
            atf686n.ST_EnableDevice(vnResult, 1)
            atf686n.ST_DisConnect(vnResult)
        ElseIf DeviceType = "TF-01" Then
            vpszIPAddress = IP.ToString
            vpszNetPort = CLng("5005")
            vpszNetPassword = CLng("0")
            vnTimeOut = CLng("5000")
            vnProtocolType = 0
            vnLicense = 7881
            Dim bRet As Boolean
            vnMachineNumber = ID_NO.ToString  '1
            vpszIPAddress = IP.ToString '"192.168.0.111"
            vpszNetPort = 5005
            vpszNetPassword = 0
            vnTimeOut = 5000
            vnProtocolType = PROTOCOL_TCPIP
            vnLicense = 7881
            Dim nPort As Integer = vpszNetPort
            Dim nPassword As Integer = vpszNetPassword
            Dim strIP As String = vpszIPAddress.ToString()
            Me.AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
            bRet = AxFP_CLOCK1.SetIPAddress(strIP, nPort, nPassword)
            If bRet Then
                bRet = AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
                bRet = DisableDevice(vnMachineNumber)
                Dim tTimeInfo As TimeInfo = New TimeInfo(2) 'test value 
                bRet = AxFP_CLOCK1.EnableDevice(vnMachineNumber, 0)
                If bRet Then
                    AxFP_CLOCK1.GetDeviceTime(vnMachineNumber, tTimeInfo.nYear, tTimeInfo.nMonth, tTimeInfo.nDay, tTimeInfo.nHour, tTimeInfo.nMinute, tTimeInfo.nDayofWeek)
                    If tTimeInfo.nDayofWeek = 0 Then
                        tTimeInfo.nDayofWeek = 7
                    End If
                    Dim dateTime As Date = New DateTime(tTimeInfo.nYear, tTimeInfo.nMonth, tTimeInfo.nDay, tTimeInfo.nHour, tTimeInfo.nMinute, 0)
                    Dim enUS As CultureInfo = New CultureInfo("en-US")
                    'labelInfo.Text = dateTime.ToString("R", enUS);
                    TextEdit1.Text = dateTime.ToString("f", enUS)
                    AxFP_CLOCK1.EnableDevice(vnMachineNumber, 1)
                End If


            Else
                Me.Cursor = Cursors.Default
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")

            End If

        End If
        Me.Cursor = Cursors.Default
    End Sub
    Public Structure TimeInfo
        Public nYear As Integer
        Public nMonth As Integer
        Public nDay As Integer
        Public nHour As Integer
        Public nMinute As Integer
        Public nDayofWeek As Integer

        Public Sub New(ByVal init As Integer)
            nYear = CSharpImpl.__Assign(nMonth, CSharpImpl.__Assign(nDay, CSharpImpl.__Assign(nHour, CSharpImpl.__Assign(nMinute, CSharpImpl.__Assign(nDayofWeek, init)))))
        End Sub

        Private Class CSharpImpl
            <Obsolete("Please refactor calling code to use normal Visual Basic assignment")>
            Shared Function __Assign(Of T)(ByRef target As T, value As T) As T
                target = value
                Return value
            End Function
        End Class
    End Structure

    Private Function DisableDevice(vnMachineNumber) As Boolean

        Dim bRet As Boolean = AxFP_CLOCK1.EnableDevice(vnMachineNumber, 0)
        If bRet Then
            'labelInfo.Text = "Disable Device Success!"

            Return True
        Else
            'labelInfo.Text = "No Device..."
            Return False
        End If
    End Function
    Private Sub SimpleButtonSet_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSet.Click
        If PopupContainerEdit1.EditValue.ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select device</size>", "Failed")
            PopupContainerEdit1.Select()
            Exit Sub
        End If
        Dim a As DateTime
        Dim c As Common = New Common()
        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer
        Me.Cursor = Cursors.WaitCursor

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R As Object
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()

        IP = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("LOCATION"))
        ID_NO = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("ID_NO"))
        DeviceType = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("DeviceType"))
        A_R = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("A_R"))
        Dim commkey As Integer '= Convert.ToInt32(GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("commkey")))
        Common.LogPost("Device Set DateTime; Device ID: " & ID_NO)
        If DeviceType = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
            vnMachineNumber = ID_NO.ToString  '1
            vpszIPAddress = IP.ToString '"192.168.0.111"
            vpszNetPort = 5005
            vpszNetPassword = 0
            vnTimeOut = 5000
            vnProtocolType = PROTOCOL_TCPIP
            vnLicense = 1261

            Dim gnCommHandleIndex As Long
            If A_R = "S" Then
                gnCommHandleIndex = FK_ConnectUSB(vnMachineNumber, vnLicense)
            Else
                gnCommHandleIndex = c.ConnectToDevice(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
            End If
            If gnCommHandleIndex > 0 Then
                FK_SetDeviceTime(gnCommHandleIndex, Now)
                TextEdit1.Text = "Set Date time success"
                FK_DisConnect(gnCommHandleIndex)
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        ElseIf DeviceType = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
            commkey = Convert.ToInt32(GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("commkey")))
            Dim axCZKEM1tmp As New zkemkeeper.CZKEM
            Dim comZK As CommonZK = New CommonZK
            axCZKEM1tmp.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
            Dim result As Boolean = comZK.connectZK(IP, 4370, axCZKEM1tmp)
            If result = True Then
                Dim iMachineNumber As Integer = 1
                If axCZKEM1tmp.SetDeviceTime(iMachineNumber) = False Then
                    XtraMessageBox.Show(ulf, "<size=10>Set Date failed!</size>", "Failed")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If

                Dim idwErrorCode As Integer

                Dim idwYear As Integer
                Dim idwMonth As Integer
                Dim idwDay As Integer
                Dim idwHour As Integer
                Dim idwMinute As Integer
                Dim idwSecond As Integer

                If axCZKEM1tmp.GetDeviceTime(iMachineNumber, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond) = True Then
                    axCZKEM1tmp.RefreshData(iMachineNumber) 'the data in the device should be refreshed
                    TextEdit1.Text = idwYear.ToString("0000") + "-" + idwMonth.ToString("00") + "-" + idwDay.ToString("00") + " " + idwHour.ToString("00") + ":" + idwMinute.ToString("00") + ":" + idwSecond.ToString("00")
                Else
                    axCZKEM1tmp.GetLastError(idwErrorCode)
                    XtraMessageBox.Show(ulf, "<size=10>Operation failed,ErrorCode=" & idwErrorCode.ToString() & "</size>", "Error")
                End If
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            comZK.DisconnectZK(axCZKEM1tmp)
        ElseIf DeviceType = "Bio2+" Then
            commkey = Convert.ToInt32(GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("commkey")))
            Dim deviceG2 As Riss.Devices.Device
            Dim deviceConnectionG2 As Riss.Devices.DeviceConnection
            deviceG2 = New Riss.Devices.Device()
            deviceG2.DN = 1 'CInt(nud_DN.Value)
            deviceG2.Password = commkey ' nud_Pwd.Value.ToString()
            deviceG2.Model = "ZDC2911"
            deviceG2.ConnectionModel = 5
            If A_R = "S" Then
                deviceG2.CommunicationType = CommunicationType.Usb
            Else
                deviceG2.IpAddress = IP
                deviceG2.IpPort = 5500 'CInt(nud_Port.Value)
                deviceG2.CommunicationType = CommunicationType.Tcp
            End If
            deviceConnectionG2 = DeviceConnection.CreateConnection(deviceG2)
            If deviceConnectionG2.Open() > 0 Then
                Try
                    Dim extraProperty As New Object()
                    Dim extraData As New Object()
                    Dim result As Boolean = deviceConnectionG2.SetProperty(DeviceProperty.DeviceTime, extraProperty, deviceG2, extraData)
                    If result Then
                        result = deviceConnectionG2.GetProperty(DeviceProperty.DeviceTime, extraProperty, deviceG2, extraData)
                        Dim dt As DateTime = DirectCast(extraData, DateTime)
                        TextEdit1.Text = dt.ToString("yyyy-MM-dd HH:mm:ss")
                    Else
                        deviceConnectionG2.Close()
                        XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                        Me.Cursor = Cursors.Default
                        Exit Sub
                    End If
                Catch ex As Exception
                    deviceConnectionG2.Close()
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End Try
                deviceConnectionG2.Close()
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        ElseIf DeviceType = "Bio1Eco" Then
            Dim adap, adap1 As SqlDataAdapter
            Dim adapA, adapA1 As OleDbDataAdapter
            Dim ds, ds1 As DataSet
            commkey = Convert.ToInt32(GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("commkey")))
            Dim mOpenFlag As Boolean
            Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

            Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
            Dim equNo As UInt32 = Convert.ToUInt32(ID_NO)
            Dim gEquNo As UInt32 = equNo
            If A_R = "T" Then
                Dim netCfg As SyNetCfg
                Dim pswd As UInt32 = 0
                Dim tmpPswd As String = commkey
                If (tmpPswd = "0") Then
                    tmpPswd = "FFFFFFFF"
                End If
                Try
                    pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                    ret = CType(SyLastError.slePasswordError, Int32)
                End Try
                If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                    netCfg.mIsTCP = 1
                    netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                    netCfg.mIPAddr = New Byte((4) - 1) {}
                    Dim sArray() As String = IP.Split(Microsoft.VisualBasic.ChrW(46))
                    Dim j As Byte = 0
                    For Each i As String In sArray
                        Try
                            netCfg.mIPAddr(j) = Convert.ToByte(i.Trim)
                        Catch ex As System.Exception
                            netCfg.mIPAddr(j) = 255
                        End Try
                        j = (j + 1)
                        If j > 3 Then
                            Exit For
                        End If

                    Next
                End If

                Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                Try
                    Marshal.StructureToPtr(netCfg, pnt, False)
                    '        If optWifiDevice.Checked Then
                    'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                    '        Else
                    ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                    '        End If
                    '                'TODO: Warning!!!, inline IF is not supported ?
                    '                chkTranceIO.Checked()
                    '0:
                    '                '
                Finally
                    Marshal.FreeHGlobal(pnt)
                End Try
            ElseIf A_R = "S" Then
                Dim equtype As String = "Finger Module USB Device"
                Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
            End If
            If (ret = CType(SyLastError.sleSuss, Int32)) Then
                ret = SyFunctions.CmdTestConn2Device

                Dim dt As Date = DateTime.Now
                Dim y As System.UInt16
                Dim M As Byte, d, h, m1, s, w
                y = CType(dt.Year, System.UInt16)
                M = CType(dt.Month, Byte)
                d = CType(dt.Day, Byte)
                h = CType(dt.Hour, Byte)
                m1 = CType(dt.Minute, Byte)
                s = CType(dt.Second, Byte)
                w = CType(dt.DayOfWeek, Byte)
                ret = SyFunctions.CmdSetDeviceTime(y, M, d, w, h, m1, s)

                If Common.IsNepali = "Y" Then
                    'Dim doj As DateTime = DC.ToBS(New Date(dt.Year.ToString(), dt.Month.ToString(), dt.Day.ToString()))
                    Dim Vstart As String = DC.ToBS(New Date(dt.Year.ToString(), dt.Month.ToString(), dt.Day.ToString()))
                    Dim dojTmp() As String = Vstart.Split("-")
                    TextEdit1.Text = dojTmp(2) & "-" & Common.NepaliMonth(dojTmp(1) - 1) & "-" & dojTmp(0) & " " & h.ToString() & ":" & m1.ToString() & ":" & s.ToString()

                    'Dim doj As DateTime = DC.ToBS(New Date(dt.Year.ToString(), dt.Month.ToString(), dt.Day.ToString()))
                    'TextEdit1.Text = doj.Day & "-" & Common.NepaliMonth(doj.Month - 1) & "-" & doj.Year & " " & dt.Hour.ToString() & ":" & dt.Minute.ToString() & ":" & dt.Second.ToString()
                Else
                    TextEdit1.Text = dt.Year.ToString("0000") & "-" & dt.Month.ToString("00") & "-" & dt.Day.ToString("00") & " " & dt.Hour.ToString("00") & ":" & dt.Minute.ToString("00") & ":" & dt.Second.ToString("00")
                End If

                SyFunctions.CloseCom()
                SyFunctions.CmdUnLockDevice()
                SyFunctions.CloseCom()
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        ElseIf DeviceType = "EF45" Then
            Dim _client As Client = Nothing
            Dim connectOk As Boolean = False
            Try
                _client = cn.initClientEF45(IP)
                connectOk = _client.StealConnect()
                If connectOk Then
                    Dim SetDate As String = Now.Month.ToString("00") & "" & Now.Day.ToString("00") & "" & Now.Hour.ToString("00") & "" & Now.Minute.ToString("00") & "" & Now.Year.ToString("0000") & "." & Now.Second.ToString("00")
                    _client.SetDeviceTime(SetDate)
                    _client.Disconnect()
                Else
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End Try
        ElseIf DeviceType = "F9" Then
            vnMachineNumber = ID_NO.ToString  '1
            vpszIPAddress = IP.ToString '"192.168.0.111"
            vpszNetPort = 5005
            vpszNetPassword = 0
            vnTimeOut = 5000
            vnProtocolType = PROTOCOL_TCPIP
            vnLicense = 7881
            Dim F9 As mdlPublic_f9 = New mdlPublic_f9
            Dim gnCommHandleIndex As Long
            If A_R = "S" Then
                gnCommHandleIndex = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
            Else
                gnCommHandleIndex = F9.FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
            End If
            If gnCommHandleIndex > 0 Then
                F9.FK_SetDeviceTime(gnCommHandleIndex, Now)
                TextEdit1.Text = "Set Date time success"
                FK_DisConnect(gnCommHandleIndex)
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        ElseIf DeviceType = "ATF686n" Then
            vpszIPAddress = IP.ToString
            vpszNetPort = CLng("5005")
            vpszNetPassword = CLng("0")
            vnTimeOut = CLng("5000")
            vnProtocolType = 0
            vnLicense = 7881
            Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
            'If A_R = "S" Then
            '    vnResult = F9.FK_ConnectUSB(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vnLicense)
            'Else
            Dim vnResult As Integer = atf686n.ConnectNet(vpszIPAddress)
            Dim vdwDate As DateTime = DateTime.Now
            Dim strDataTime As String
            Dim vnResultCode As Integer


            vnResultCode = atf686n.ST_EnableDevice(vnResult, 0)
            If vnResultCode <> CInt(enumErrorCode.RETURN_SUCCESS) Then
                Me.Cursor = Cursors.Default
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
            Else
                vnResultCode = atf686n.ST_GetDeviceTime(vnResult, vdwDate)
                If vnResultCode = CInt(enumErrorCode.RETURN_SUCCESS) Then
                    vdwDate = Now.ToString("yyyy-MM-dd HH:mm:ss")
                    vnResultCode = atf686n.ST_SetDeviceTime(vnResult, vdwDate)
                Else
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    TextEdit1.Text = ""
                End If
            End If
            atf686n.ST_EnableDevice(vnResult, 1)
            atf686n.ST_DisConnect(vnResult)
        ElseIf DeviceType = "TF-01" Then
            vpszIPAddress = IP.ToString
            vpszNetPort = CLng("5005")
            vpszNetPassword = CLng("0")
            vnTimeOut = CLng("5000")
            vnProtocolType = 0
            vnLicense = 7881
            Dim bRet As Boolean
            vnMachineNumber = ID_NO.ToString  '1
            vpszIPAddress = IP.ToString '"192.168.0.111"
            vpszNetPort = 5005
            vpszNetPassword = 0
            vnTimeOut = 5000
            vnProtocolType = PROTOCOL_TCPIP
            vnLicense = 7881
            Dim nPort As Integer = vpszNetPort
            Dim nPassword As Integer = vpszNetPassword
            Dim strIP As String = vpszIPAddress.ToString()
            Me.AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
            bRet = AxFP_CLOCK1.SetIPAddress(strIP, nPort, nPassword)
            If bRet Then
                bRet = AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
                bRet = DisableDevice(vnMachineNumber)
                Dim tTimeInfo As TimeInfo = New TimeInfo(2) 'test value 
                bRet = AxFP_CLOCK1.EnableDevice(vnMachineNumber, 0)
                If bRet Then
                    AxFP_CLOCK1.SetDeviceTime(vnMachineNumber)
                    TextEdit1.Text = "Success"
                    AxFP_CLOCK1.EnableDevice(vnMachineNumber, 1)
                End If

            Else
                Me.Cursor = Cursors.Default
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")

            End If
        End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub SimpleButtonDownload_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDownload.Click
        If PopupContainerEdit1.EditValue.ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select device</size>", "Failed")
            PopupContainerEdit1.Select()
            Exit Sub
        End If
        If ToggleEmp.IsOn = True Then
            If XtraMessageBox.Show(ulf, "<size=10>You have selected to Create Employee Master. It will take time to Download logs " & vbCrLf & "Are you sure to Proceed</size>", "Confirmation", _
                               MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Exit Sub
            End If
        End If
        Dim datafile As String = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer

        If CheckEdit1.Checked = True Then   'All logs
            vnReadMark = 0
        ElseIf CheckEdit2.Checked Then   'new logs
            vnReadMark = 1
        End If
        Dim clearLog As Boolean
        If ToggleSwitch2.IsOn = True Then
            clearLog = True
        Else
            clearLog = False
        End If

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R, Purpose As Object
        Dim commkey As Integer
        Dim IN_OUT As String
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
        For Each rowHandle As Integer In RowHandles
            IP = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("LOCATION"))
            IN_OUT = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("IN_OUT"))
            XtraMasterTest.LabelControlStatus.Text = "Connecting " & IP & "..."
            Application.DoEvents()

            If vnReadMark = 1 Then
                Common.LogPost("Device New Log Download; Device ID: " & ID_NO)
            ElseIf vnReadMark = 0 Then
                Common.LogPost("Device All Log Download; Device ID: " & ID_NO)
            End If
            If ToggleSwitch2.IsOn = True Then
                Common.LogPost("Device Clear Logs; Device ID: " & ID_NO)
            End If
            ID_NO = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("ID_NO"))
            DeviceType = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("DeviceType"))
            A_R = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("A_R"))
            Purpose = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("Purpose"))
            If DeviceType.ToString.Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 1261
                Dim CreateEmpMaster As Boolean = False
                If ToggleEmp.IsOn = True Then
                    CreateEmpMaster = True
                End If
                Dim result As String = cn.funcGetGeneralLogData(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, clearLog, A_R, Purpose, IN_OUT, ID_NO, datafile, CreateEmpMaster)
                'MsgBox("Result " & result)
                If result <> "Success" Then
                    If result.Contains("Invalid Serial number") Then
                        SplashScreenManager.CloseForm(False)
                        XtraMessageBox.Show(ulf, "<size=10>" & result & "</size>", "<size=9>Error</size>")
                        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
                        Continue For
                    Else
                        failIP.Add(IP.ToString)
                    End If
                End If
            ElseIf DeviceType.ToString.Trim = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                Dim adap, adap1 As SqlDataAdapter
                Dim adapA, adapA1 As OleDbDataAdapter
                Dim ds, ds1 As DataSet
                Dim sdwEnrollNumber As String = ""
                Dim idwVerifyMode As Integer
                Dim idwInOutMode As Integer
                Dim idwYear As Integer
                Dim idwMonth As Integer
                Dim idwDay As Integer
                Dim idwHour As Integer
                Dim idwMinute As Integer
                Dim idwSecond As Integer
                Dim idwWorkcode As Integer
                commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                Dim comZK As CommonZK = New CommonZK
                Dim bIsConnected = False
                Dim iMachineNumber As Integer
                Dim idwErrorCode As Integer
                Dim com As Common = New Common
                Dim axCZKEM1 As New zkemkeeper.CZKEM
                axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                bIsConnected = axCZKEM1.Connect_Net(IP, 4370)
                If bIsConnected = True Then
                    iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                    axCZKEM1.RegEvent(iMachineNumber, 65535) 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                    axCZKEM1.EnableDevice(iMachineNumber, False) 'disable the device
                    Dim McSrno As String = ""
                    Dim vRet As Boolean = axCZKEM1.GetSerialNumber(iMachineNumber, McSrno)
                    If license.DeviceSerialNo.Contains(McSrno.Trim) Then
                    Else
                        SplashScreenManager.CloseForm(False)
                        axCZKEM1.EnableDevice(iMachineNumber, True)
                        axCZKEM1.Disconnect()
                        XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & McSrno & "</size>", "<size=9>Error</size>")
                        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
                        Continue For
                    End If
                    Dim LogResult As Boolean
                    If CheckEditTimePerioad.Checked = True Then
                        If Common.IsNepali = "Y" Then
                            Try
                                'DateEditFrmDate.DateTime = DC.ToAD(New Date(ComboNYearStr.EditValue, ComboNMonthStr.SelectedIndex + 1, ComboNDateStr.EditValue)) & " 00:00:00"
                                DateEditFrmDate.DateTime = DC.ToAD(ComboNYearStr.EditValue & "-" & ComboNMonthStr.SelectedIndex + 1 & "-" & ComboNDateStr.EditValue) & " 00:00:00"
                            Catch ex As Exception
                                XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                DateEditFrmDate.Select()
                                Exit Sub
                            End Try
                            Try
                                'DateEditToDate.DateTime = DC.ToAD(New Date(ComboNYearEnd.EditValue, ComboNMonthEnd.SelectedIndex + 1, ComboNDateEnd.EditValue)) & " 23:59:59"
                                DateEditToDate.DateTime = DC.ToAD(ComboNYearEnd.EditValue & "-" & ComboNMonthEnd.SelectedIndex + 1 & "-" & ComboNDateEnd.EditValue) & " 23:59:59"
                            Catch ex As Exception
                                XtraMessageBox.Show(ulf, "<size=10>Invalid To Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                DateEditToDate.Select()
                                Exit Sub
                            End Try
                        End If
                        LogResult = axCZKEM1.ReadTimeGLogData(iMachineNumber, DateEditFrmDate.DateTime.ToString("yyyy-MM-dd HH:mm:ss"), DateEditToDate.DateTime.ToString("yyyy-MM-dd HH:mm:ss"))
                    ElseIf CheckEdit2.Checked And DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then 'new logs and K60 to ignore logs after lastdownlaoded 
                        LogResult = axCZKEM1.ReadGeneralLogData(iMachineNumber)
                    ElseIf CheckEdit1.Checked = True Then   'all logs
                        LogResult = axCZKEM1.ReadGeneralLogData(iMachineNumber)
                    ElseIf CheckEdit2.Checked Then   ' new logs
                        LogResult = axCZKEM1.ReadNewGLogData(iMachineNumber)
                    End If
                    If LogResult Then 'read all the attendance records to the memory
                        XtraMasterTest.LabelControlStatus.Text = "Downloading from " & IP & "..."
                        Application.DoEvents()
                        'get records from the memory
                        Dim x As Integer = 0
                        Dim startdate As DateTime
                        Dim paycodelist As New List(Of String)()
                        Dim paycodelistForEmpMaster As New List(Of String)() ' for emp master  
                        'Dim count As Integer = 0                       
                        While axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)
                            'XtraMasterTest.LabelControlCount.Text = "Downloading " & x + 1 & " of " & count
                            'Application.DoEvents()                           
                            paycodelist.Add(sdwEnrollNumber)
                            paycodelistForEmpMaster.Add(sdwEnrollNumber & ";")
                            If ToggleEmp.IsOn = True Then
                                Dim payCodeArrEmpMaster() As String = paycodelistForEmpMaster.ToArray
                                Common.CreateEmployee(payCodeArrEmpMaster)
                            End If
                            Dim punchdate = idwYear.ToString() & "-" + idwMonth.ToString("00") & "-" & idwDay.ToString("00") & " " & idwHour.ToString("00") & ":" & idwMinute.ToString("00") & ":" & idwSecond.ToString("00")
                            If x = 0 Then
                                startdate = punchdate
                            End If
                            If CheckEdit2.Checked And DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                                Try
                                    If Convert.ToDateTime(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("LastDownloaded"))).ToString("yyyy-MM-dd HH:mm:ss") >
                                    Convert.ToDateTime(punchdate).ToString("yyyy-MM-dd HH:mm:ss") Then
                                        x = x + 1
                                        Continue While
                                    End If
                                Catch ex As Exception
                                End Try
                            End If


                            com.funcGetGeneralLogDataZK(sdwEnrollNumber, idwVerifyMode, idwInOutMode, punchdate, idwWorkcode, Purpose, IN_OUT, ID_NO, datafile)
                            x = x + 1
                        End While

                        Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(xsSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(xsSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If

                        Dim paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
                        ds = New DataSet
                        For i As Integer = 0 To paycodeArray.Length - 1
                            XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & paycodeArray.Length
                            Application.DoEvents()
                            Dim PRESENTCARDNO As String
                            If IsNumeric(paycodeArray(i)) Then
                                PRESENTCARDNO = Convert.ToInt64(paycodeArray(i)).ToString("000000000000")
                            Else
                                PRESENTCARDNO = paycodeArray(i)
                            End If
                            Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                            '"select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "'"
                            ds = New DataSet
                            If Common.servername = "Access" Then
                                adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                                adapA.Fill(ds)
                            Else
                                adap = New SqlDataAdapter(sSqltmp, Common.con)
                                adap.Fill(ds)
                            End If
                            If ds.Tables(0).Rows.Count > 0 Then
                                cn.Remove_Duplicate_Punches(startdate, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                    com.Process_AllRTC(startdate.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                Else
                                    com.Process_AllnonRTC(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    If Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                                        com.Process_AllnonRTCMulti(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    End If
                                End If
                            End If
                        Next
                        XtraMasterTest.LabelControlCount.Text = ""
                        Application.DoEvents()
                    Else
                        Cursor = Cursors.Default
                        axCZKEM1.GetLastError(idwErrorCode)
                        'If idwErrorCode <> 0 Then
                        '    failIP.Add(IP.ToString)
                        'Else
                        '    'MsgBox("No data from terminal returns!", MsgBoxStyle.Exclamation, "Error")
                        'End If
                    End If
                    axCZKEM1.EnableDevice(iMachineNumber, True)
                    If clearLog = True Then
                        axCZKEM1.ClearGLog(iMachineNumber)
                    End If
                    axCZKEM1.Disconnect()
                Else
                    axCZKEM1.GetLastError(idwErrorCode)
                    failIP.Add(IP.ToString)
                    Continue For
                End If

                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand("delete from Rawdata", Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand("delete from Rawdata", Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                XtraMasterTest.LabelControlCount.Text = ""
                Application.DoEvents()
            ElseIf DeviceType = "Bio2+" Then
                commkey = Convert.ToInt32(GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("commkey")))
                Dim deviceG2 As Riss.Devices.Device
                Dim deviceConnectionG2 As Riss.Devices.DeviceConnection
                deviceG2 = New Riss.Devices.Device()
                deviceG2.DN = 1 'CInt(nud_DN.Value)
                deviceG2.Password = commkey ' nud_Pwd.Value.ToString()
                deviceG2.Model = "ZDC2911"
                deviceG2.ConnectionModel = 5
                If A_R = "S" Then
                    deviceG2.CommunicationType = CommunicationType.Usb
                Else
                    deviceG2.IpAddress = IP
                    deviceG2.IpPort = 5500 'CInt(nud_Port.Value)
                    deviceG2.CommunicationType = CommunicationType.Tcp
                End If
                deviceConnectionG2 = DeviceConnection.CreateConnection(deviceG2)
                If deviceConnectionG2.Open() > 0 Then
                    Try
                        Dim extraProperty As New Object()
                        Dim extraData As New Object()
                        extraData = ConvertObject.DeviceBusy

                        Try
                            Dim dtList As List(Of DateTime) = GetDateTimeListBio2()  'fill be used only for all logs
                            Dim result As Boolean = deviceConnectionG2.SetProperty(DeviceProperty.Enable, extraProperty, deviceG2, extraData)
                            If CheckEdit2.Checked = True Then
                                extraProperty = True
                            Else
                                extraProperty = False
                            End If
                            extraData = dtList
                            result = deviceConnectionG2.GetProperty(DeviceProperty.AttRecordsCount, extraProperty, deviceG2, extraData)
                            If False = result Then
                                'MessageBox.Show("Get New Glog Fail", "Prompt", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                'Return
                                failIP.Add(IP.ToString)
                                Continue For
                            End If

                            Dim recordCount As Integer = CInt(extraData)
                            If 0 = recordCount Then
                                'lvw_GLogList.Items.Clear()
                                deviceConnectionG2.Close()
                                Continue For
                                '  Return
                            End If

                            Dim boolList As New List(Of Boolean)()
                            If CheckEdit2.Checked = True Then
                                boolList.Add(True)
                            Else
                                boolList.Add(False)
                            End If

                            'boolList.Add(chk_NewFlag.Checked)
                            boolList.Add(False)
                            extraProperty = boolList
                            extraData = dtList
                            result = deviceConnectionG2.GetProperty(DeviceProperty.AttRecords, extraProperty, deviceG2, extraData)
                            If result Then
                                Dim recordList As List(Of Record) = DirectCast(extraData, List(Of Record))
                                'AddRecordToListView(recordList)
                                cn.funcGetGeneralLogDataBio2(recordList, Purpose, ID_NO, datafile, IN_OUT)
                            Else
                                'MessageBox.Show("Get New Glog Fail", "Prompt", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                failIP.Add(IP.ToString)
                                'Continue For
                            End If
                        Catch ex As Exception
                            deviceConnectionG2.Close()
                            failIP.Add(IP.ToString)
                            'Continue For
                        Finally
                            extraData = ConvertObject.DeviceIdle
                            deviceConnectionG2.SetProperty(DeviceProperty.Enable, extraProperty, deviceG2, extraData)
                        End Try
                    Catch ex As Exception
                        deviceConnectionG2.Close()
                        failIP.Add(IP.ToString)
                        'Continue For
                    End Try
                    deviceConnectionG2.Close()
                Else
                    failIP.Add(IP.ToString)
                    'Continue For
                End If

            ElseIf DeviceType = "Bio1Eco" Then
                Dim adap, adap1 As SqlDataAdapter
                Dim adapA, adapA1 As OleDbDataAdapter
                Dim ds, ds1 As DataSet
                commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                Dim mOpenFlag As Boolean
                Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

                Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
                Dim equNo As UInt32 = Convert.ToUInt32(ID_NO)
                Dim gEquNo As UInt32 = equNo
                If A_R = "T" Then
                    Dim netCfg As SyNetCfg
                    Dim pswd As UInt32 = 0
                    Dim tmpPswd As String = commkey
                    If (tmpPswd = "0") Then
                        tmpPswd = "FFFFFFFF"
                    End If
                    Try
                        pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                    Catch ex As Exception
                        XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                        ret = CType(SyLastError.slePasswordError, Int32)
                    End Try
                    If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                        netCfg.mIsTCP = 1
                        netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                        netCfg.mIPAddr = New Byte((4) - 1) {}
                        Dim sArray() As String = IP.Split(Microsoft.VisualBasic.ChrW(46))
                        Dim j As Byte = 0
                        For Each i As String In sArray
                            Try
                                netCfg.mIPAddr(j) = Convert.ToByte(i.Trim)
                            Catch ex As System.Exception
                                netCfg.mIPAddr(j) = 255
                            End Try
                            j = (j + 1)
                            If j > 3 Then
                                Exit For
                            End If

                        Next
                    End If

                    Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                    Try
                        Marshal.StructureToPtr(netCfg, pnt, False)
                        '        If optWifiDevice.Checked Then
                        'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                        '        Else
                        ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                        '        End If
                        '                'TODO: Warning!!!, inline IF is not supported ?
                        '                chkTranceIO.Checked()
                        '0:
                        '                '
                    Finally
                        Marshal.FreeHGlobal(pnt)
                    End Try
                ElseIf A_R = "S" Then
                    Dim equtype As String = "Finger Module USB Device"
                    Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                    ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
                End If
                If (ret = CType(SyLastError.sleSuss, Int32)) Then
                    ret = SyFunctions.CmdTestConn2Device
                    '
                    If (ret = CType(SyLastError.sleSuss, Int32)) Then
                        ret = 66
                        Dim totalCnt As Integer = 0, unreadPos = 0
                        Dim i As Integer, remain = 0
                        Dim read As Byte = 0, dwMonth, dwDay, dwHour, dwMinute, dwSecond
                        Dim kind As Byte = 0, hasCooperated = 0, hasSetup = 0, hasOpenDoor = 0, hasAlarm = 0
                        Dim dwYear As System.UInt16 = 0
                        Dim dwEnrollID As Integer, dwLogType
                        Dim logType As String = ""
                        dwEnrollID = 0
                        dwLogType = 0
                        dwYear = 0
                        dwMonth = 0
                        dwDay = 0
                        dwHour = 0
                        dwMinute = 0
                        dwSecond = 0
                        SyFunctions.CmdLockDevice()
                        If CheckEdit1.Checked = True Then   'all logs
                            ret = SyFunctions.CmdGetCountOfValidDailyEntryRecord(totalCnt, unreadPos)
                            unreadPos = 0
                        ElseIf CheckEdit2.Checked = True Then   ' new logs
                            ret = SyFunctions.CmdGetCountOfValidDailyEntryRecord(totalCnt, unreadPos)
                        End If

                        Dim index As Integer = 0
                        If (ret = CType(SyLastError.sleSuss, Int32)) Then
                            Dim records As StringBuilder = New StringBuilder
                            i = unreadPos
                            remain = (totalCnt - unreadPos)
                            Dim x As Integer = 0
                            Dim startdate As DateTime
                            Dim paycodelist As New List(Of String)()

                            While (i < totalCnt)
                                If (remain > 64) Then
                                    read = 64
                                Else
                                    read = CType(remain, Byte)
                                End If

                                Dim dera() As SyLogRecord = New SyLogRecord((read) - 1) {}
                                ret = SyFunctions.CmdGetValidDailyEntryRecord(dera, i, read)
                                If (ret = CType(SyLastError.sleSuss, Int32)) Then
                                    For j As Byte = 0 To read - 1 ' Do While (j < read)
                                        Try


                                            If (dera(j).wUserID = 0) Or (dera(j).wUserID > 65534) Then
                                                Continue For
                                            End If

                                            dwEnrollID = dera(j).wUserID
                                            'Dim paycodelist As New List(Of String)()
                                            Dim paycodelistForEmpMaster As New List(Of String)() ' for emp master  
                                            'paycodelist.Add(sdwEnrollNumber)
                                            paycodelistForEmpMaster.Add(dwEnrollID & ";")
                                            If ToggleEmp.IsOn = True Then
                                                Dim payCodeArrEmpMaster() As String = paycodelistForEmpMaster.ToArray
                                                Common.CreateEmployee(payCodeArrEmpMaster)
                                            End If

                                            SyFunctions.DecodeSyDailyEntryType(dera(j).cLogType, kind, hasCooperated, hasSetup, hasOpenDoor, hasAlarm)
                                            SyFunctions.DecodeSyDailyEntryTime(dera(j).sRecordTime, dwSecond, dwMinute, dwHour, dwDay, dwMonth, dwYear)
                                            Dim dt As DateTime = New DateTime(dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond)
                                            dwLogType = (kind Mod 8)

                                            paycodelist.Add(dwEnrollID)
                                            Dim punchdate = dt 'idwYear.ToString() & "-" + idwMonth.ToString("00") & "-" & idwDay.ToString("00") & " " & idwHour.ToString("00") & ":" & idwMinute.ToString("00") & ":" & idwSecond.ToString("00")
                                            If x = 0 Then
                                                startdate = punchdate
                                            End If
                                            cn.funcGetGeneralLogDataZK(dwEnrollID, logType, "", punchdate, "", Purpose, IN_OUT, ID_NO, datafile)
                                            x = x + 1

                                            'Select Case (dwLogType)
                                            '    Case 1
                                            '        'finger
                                            '        logType = "Fingerprint"
                                            '    Case 4
                                            '        'finger
                                            '        logType = "Fingerprint + Password"
                                            '    Case 5
                                            '        'finger
                                            '        logType = "Fingerprint + Card"
                                            '    Case 6
                                            '        'card
                                            '        logType = "Password + Card"
                                            '    Case 7
                                            '        'finger
                                            '        logType = "Fingerprint + Card + Password"
                                            '    Case 2
                                            '        'password
                                            '        logType = "Password"
                                            '    Case 3
                                            '        'card
                                            '        logType = "Card"
                                            '    Case Else
                                            '        logType = ""
                                            'End Select

                                            'row = dtLog.NewRow
                                            'row("no") = (index + 1).ToString
                                            'row("MachineNo") = Program.gEquNo.ToString
                                            'row("EnrollNo") = dwEnrollID
                                            'row("DateTime") = dt.ToString
                                            'row("LogType") = logType
                                            'dtLog.Rows.Add(row)
                                            index = (index + 1)
                                        Catch ex As Exception
                                            index = (index + 1)
                                        End Try
                                    Next 'Loop
                                    'end for  block record
                                End If
                                'end if
                                i = (i + read)
                            End While
                            'while
                            Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
                            If Common.servername = "Access" Then
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(xsSql, Common.con1)
                                cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(xsSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                            Dim paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
                            ds = New DataSet
                            For i = 0 To paycodeArray.Length - 1
                                Dim PRESENTCARDNO As String
                                If IsNumeric(paycodeArray(i)) Then
                                    PRESENTCARDNO = Convert.ToInt64(paycodeArray(i)).ToString("000000000000")
                                Else
                                    PRESENTCARDNO = paycodeArray(i)
                                End If
                                Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "'  and TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId "
                                ds = New DataSet
                                If Common.servername = "Access" Then
                                    adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                                    adapA.Fill(ds)
                                Else
                                    adap = New SqlDataAdapter(sSqltmp, Common.con)
                                    adap.Fill(ds)
                                End If
                                If ds.Tables(0).Rows.Count > 0 Then
                                    cn.Remove_Duplicate_Punches(startdate, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                        cn.Process_AllRTC(startdate.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    Else
                                        cn.Process_AllnonRTC(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                        If Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                                            cn.Process_AllnonRTCMulti(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    End If
                    SyFunctions.CmdUnLockDevice()
                    SyFunctions.CloseCom()
                Else
                    failIP.Add(IP.ToString)
                    'Continue For
                End If

            ElseIf DeviceType = "EF45" Then
                XtraMasterTest.LabelControlStatus.Text = "Downloading from " & IP & "..."
                Application.DoEvents()

                Dim EF45LogsImages As String = System.Environment.CurrentDirectory & "\EF45_LogsImages"
                If (Not System.IO.Directory.Exists(EF45LogsImages)) Then
                    System.IO.Directory.CreateDirectory(EF45LogsImages)
                End If

                Dim _client As Client = Nothing
                Dim connectOk As Boolean = False
                Try
                    _client = cn.initClientEF45(IP)
                    connectOk = _client.StealConnect '_client.Connect()                   
                    If connectOk Then
                        Dim com As Common = New Common
                        Try
                            Dim logs As List(Of CMITech.UMXClient.Entities.Log) = _client.GetLogs
                            If (Not (logs) Is Nothing) Then
                                Dim x As Integer = 0
                                Dim startdate As DateTime
                                Dim paycodelist As New List(Of String)()
                                Dim paycodelistForEmpMaster As New List(Of String)() ' for emp master  
                                For Each log As CMITech.UMXClient.Entities.Log In logs
                                    'dataGridViewLog.Rows.Add(log.LogUID, log.EventType, log.Info, log.AdditionalData, log.SubjectUID)
                                    If log.EventType = "Recognition" Then  ' for logs
                                        If log.SubjectUID.Trim <> "" Then
                                            MsgBox("SubjectUID " & log.SubjectUID.Trim)
                                            paycodelist.Add(log.SubjectUID)
                                            paycodelistForEmpMaster.Add(log.SubjectUID & ";")
                                            If ToggleEmp.IsOn = True Then
                                                Dim payCodeArrEmpMaster() As String = paycodelistForEmpMaster.ToArray
                                                Common.CreateEmployee(payCodeArrEmpMaster)
                                            End If
                                            Dim punchdate = log.Timestamp.ToString("yyyy-MM-dd HH:mm:ss") ' idwYear.ToString() & "-" & idwMonth.ToString("00") & "-" & idwDay.ToString("00") & " " & idwHour.ToString("00") & ":" & idwMinute.ToString("00") & ":" & idwSecond.ToString("00")
                                            If x = 0 Then
                                                startdate = punchdate
                                            End If
                                            com.funcGetGeneralLogDataZK(log.SubjectUID, log.Info, "", punchdate, "", Purpose, IN_OUT, ID_NO, datafile)
                                            Try
                                                Dim bm As Bitmap = log.MatchedFaceImage
                                                ' Save the picture as a bitmap, JPEG, and GIF. 
                                                Dim oBitmap As Bitmap
                                                oBitmap = New Bitmap(log.MatchedFaceImage)
                                                Dim oGraphic As Graphics
                                                ' Here create a new bitmap object of the same height and width of the image.
                                                Dim bmpNew As Bitmap = New Bitmap(oBitmap.Width, oBitmap.Height)
                                                oGraphic = Graphics.FromImage(bmpNew)
                                                oGraphic.DrawImage(oBitmap, New Rectangle(0, 0,
                                            bmpNew.Width, bmpNew.Height), 0, 0, oBitmap.Width,
                                            oBitmap.Height, GraphicsUnit.Pixel)
                                                ' Release the lock on the image file. Of course,
                                                ' image from the image file is existing in Graphics object
                                                oBitmap.Dispose()
                                                oBitmap = bmpNew
                                                oGraphic.Dispose()
                                                Dim imageName As String = My.Application.Info.DirectoryPath & "\EF45_LogsImages\" & log.SubjectUID & "_" & log.Timestamp.ToString("yyyyMMddHHmmss") & ".jpg"
                                                oBitmap.Save(imageName, ImageFormat.Jpeg)
                                                oBitmap.Dispose()
                                            Catch ex As Exception

                                            End Try
                                            x = x + 1
                                        End If
                                    End If
                                Next
                                _client.Disconnect()
                                Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
                                If Common.servername = "Access" Then
                                    If Common.con1.State <> ConnectionState.Open Then
                                        Common.con1.Open()
                                    End If
                                    cmd1 = New OleDbCommand(xsSql, Common.con1)
                                    cmd1.ExecuteNonQuery()
                                    If Common.con1.State <> ConnectionState.Closed Then
                                        Common.con1.Close()
                                    End If
                                Else
                                    If Common.con.State <> ConnectionState.Open Then
                                        Common.con.Open()
                                    End If
                                    cmd = New SqlCommand(xsSql, Common.con)
                                    cmd.ExecuteNonQuery()
                                    If Common.con.State <> ConnectionState.Closed Then
                                        Common.con.Close()
                                    End If
                                End If
                                Dim paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
                                Dim ds As DataSet = New DataSet
                                Dim adapA As OleDbDataAdapter
                                Dim adap As SqlDataAdapter
                                For i As Integer = 0 To paycodeArray.Length - 1
                                    Dim PRESENTCARDNO As String
                                    If IsNumeric(paycodeArray(i)) Then
                                        PRESENTCARDNO = Convert.ToInt64(paycodeArray(i)).ToString("000000000000")
                                    Else
                                        PRESENTCARDNO = paycodeArray(i)
                                    End If
                                    'Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "'"
                                    Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                                    ds = New DataSet
                                    If Common.servername = "Access" Then
                                        adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                                        adapA.Fill(ds)
                                    Else
                                        adap = New SqlDataAdapter(sSqltmp, Common.con)
                                        adap.Fill(ds)
                                    End If
                                    If ds.Tables(0).Rows.Count > 0 Then
                                        cn.Remove_Duplicate_Punches(startdate, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                        If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                            com.Process_AllRTC(startdate.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                        Else
                                            com.Process_AllnonRTC(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                        End If
                                    End If
                                Next
                            Else
                                'MessageBox.Show("[Error] " & DateTime.Now & " Fail to get logs")
                                'm_logMessages.AddMessage(LogMessages.Icon.Error, DateTime.Now, "Fail to get logs")                    
                            End If
                        Catch ex As Exception
                            _client.Disconnect()
                            MsgBox(ex.Message.Trim & " inner")
                            failIP.Add(IP.ToString)
                            Continue For
                        End Try
                    Else
                        failIP.Add(IP.ToString)
                        Continue For
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message.Trim & " outer")
                    failIP.Add(IP.ToString)
                    Continue For
                End Try
            ElseIf DeviceType.ToString.Trim = "F9" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881
                Dim CreateEmpMaster As Boolean = False
                If ToggleEmp.IsOn = True Then
                    CreateEmpMaster = True
                End If
                Dim result As String = cn.funcGetGeneralLogData_f9(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, clearLog, A_R, Purpose, IN_OUT, ID_NO, datafile, CreateEmpMaster)
                'MsgBox("Result " & result)
                If result <> "Success" Then
                    failIP.Add(IP.ToString)
                End If
            ElseIf DeviceType.ToString.Trim = "ATF686n" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881
                Dim CreateEmpMaster As Boolean = False
                If ToggleEmp.IsOn = True Then
                    CreateEmpMaster = True
                End If
                Dim result As String = cn.funcGetGeneralLogData_ATF686n(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, clearLog, A_R, Purpose, IN_OUT, ID_NO, datafile, CreateEmpMaster)
                'MsgBox("Result " & result)
                If result <> "Success" Then
                    failIP.Add(IP.ToString)
                End If
            ElseIf DeviceType.ToString.Trim = "TF-01" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881
                Dim CreateEmpMaster As Boolean = False
                If ToggleEmp.IsOn = True Then
                    CreateEmpMaster = True
                End If
                Dim result As String = cn.funcGetGeneralLogData_TF01(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, clearLog, A_R, Purpose, IN_OUT, ID_NO, datafile, CreateEmpMaster, AxFP_CLOCK1)
                'MsgBox("Result " & result)
                If result <> "Success" Then
                    failIP.Add(IP.ToString)
                End If
            ElseIf DeviceType.ToString.Trim = "ZK Controller" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                Dim ret As Integer = 0
                Dim str As String = "Cardno" & vbTab & "Pin" & vbTab & "Verified" & vbTab & "DoorID" & vbTab & "EventType" & vbTab & "InOutState" & vbTab & "Time_second"
                Dim BUFFERSIZE As Integer = 1 * 1024 * 1024
                Dim buffer(BUFFERSIZE - 1) As Byte
                Dim options As String = "NewRecord"
                Dim devdatfilter As String = ""
                Dim Logdata As String = ""
                Dim com As Common = New Common

                'funcGetGeneralLogDataZK
                Dim CreateEmpMaster As Boolean = False
                If ToggleEmp.IsOn = True Then
                    CreateEmpMaster = True
                End If
                Dim ConnectT = "protocol=TCP,ipaddress=" & vpszIPAddress.ToString.Trim & ",port=4370,timeout=2000,passwd="
                If IntPtr.Zero = h Then
                    h = ZKController.Connect(ConnectT)
                End If
                If h <> IntPtr.Zero Then
                    'CheckEdit1""
                    ret = ZKController.GetDeviceData(h, buffer(0), BUFFERSIZE, "transaction", str, devdatfilter, options)
                    If ret > 0 Then
                        Logdata = Encoding.Default.GetString(buffer)
                        Dim AttendanceData() As String = Logdata.Split(vbNewLine)
                        For m As Integer = 1 To AttendanceData.Length - 1
                            Try

                                If AttendanceData(m).Trim(vbNullChar).Trim(vbLf) = "" Then
                                    Continue For
                                End If
                                Dim TZArrRowData() As String = AttendanceData(m).Split(",")
                                Dim t As String '= TimeSpan.FromMilliseconds(TZArrRowData(6))
                                Dim startDate As New DateTime(1999, 8, 13)
                                Dim targetDate As DateTime
                                Dim iSpan As TimeSpan = TimeSpan.FromSeconds(TZArrRowData(6))
                                targetDate = startDate.AddDays(iSpan.Days)
                                t = iSpan.Hours.ToString.PadLeft(2, "0"c) & ":" & iSpan.Minutes.ToString.PadLeft(2, "0"c) & ":" & iSpan.Seconds.ToString.PadLeft(2, "0"c)
                                Dim PDate As DateTime = Convert.ToDateTime(targetDate & " " & t)
                                If TZArrRowData(1).Trim() = "0" Or TZArrRowData(4).Trim() <> 0 Then
                                    Continue For
                                End If
                                com.funcGetGeneralLogDataZK(TZArrRowData(1), 0, TZArrRowData(5), PDate, 0, Purpose, IN_OUT, ID_NO, datafile)
                            Catch ex As Exception
                                Continue For
                            End Try

                        Next


                    End If
                End If




                Dim result As String = cn.funcGetGeneralLogData_TF01(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, clearLog, A_R, Purpose, IN_OUT, ID_NO, datafile, CreateEmpMaster, AxFP_CLOCK1)
                    'MsgBox("Result " & result)
                    If result <> "Success" Then
                        failIP.Add(IP.ToString)
                    End If
                End If
        Next
        XtraMasterTest.LabelControlCount.Text = ""
        Application.DoEvents()
        If ToggleEmp.IsOn = True Then
            Common.loadEmp()
        End If
        '"Connection fail" ' write logic to show connection fail devices
        SplashScreenManager.CloseForm(False)
        Dim failedIpArr() As String = failIP.ToArray
        Dim failIpStr As String = ""
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        If failedIpArr.Length > 0 Then
            For i As Integer = 0 To failedIpArr.Length - 1
                failIpStr = failIpStr & "," & failedIpArr(i)
            Next
            failIpStr = failIpStr.TrimStart(",")
            XtraMessageBox.Show(ulf, "<size=10>" & failIpStr & " failed to download data</size>", "Failed")
        Else
            XtraMessageBox.Show(ulf, "<size=10>Download data success</size>", "Success")
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub
    Private Function GetDateTimeListBio2() As List(Of DateTime)

        If Common.IsNepali = "Y" Then
            Try
                'DateEditFrmDate.DateTime = DC.ToAD(New Date(ComboNYearStr.EditValue, ComboNMonthStr.SelectedIndex + 1, ComboNDateStr.EditValue)) & " 00:00:00"
                DateEditFrmDate.DateTime = DC.ToAD(ComboNYearStr.EditValue & "-" & ComboNMonthStr.SelectedIndex + 1 & "-" & ComboNDateStr.EditValue) & " 00:00:00"
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                DateEditFrmDate.Select()
                Exit Function
            End Try
            Try
                'DateEditToDate.DateTime = DC.ToAD(New Date(ComboNYearEnd.EditValue, ComboNMonthEnd.SelectedIndex + 1, ComboNDateEnd.EditValue)) & " 23:59:59"
                DateEditToDate.DateTime = DC.ToAD(ComboNYearEnd.EditValue & "-" & ComboNMonthEnd.SelectedIndex + 1 & "-" & ComboNDateEnd.EditValue) & " 23:59:59"
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid To Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                DateEditToDate.Select()
                Exit Function
            End Try
        End If

        Dim dtList As New List(Of DateTime)()
        dtList.Add(DateEditFrmDate.DateTime)
        dtList.Add(DateEditToDate.DateTime)
        Return dtList
    End Function
    Private Sub CheckEditTimePerioad_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditTimePerioad.CheckedChanged
        If Common.IsNepali = "Y" Then
            If CheckEditTimePerioad.Checked = True Then
                ComboNYearStr.Enabled = True
                ComboNMonthStr.Enabled = True
                ComboNDateStr.Enabled = True
                ComboNYearEnd.Enabled = True
                ComboNMonthEnd.Enabled = True
                ComboNDateEnd.Enabled = True
                LabelControl4.Enabled = True
                LabelControl5.Enabled = True
            Else
                ComboNYearStr.Enabled = False
                ComboNMonthStr.Enabled = False
                ComboNDateStr.Enabled = False
                ComboNYearEnd.Enabled = False
                ComboNMonthEnd.Enabled = False
                ComboNDateEnd.Enabled = False
                LabelControl4.Enabled = False
                LabelControl5.Enabled = False
            End If
        Else
            If CheckEditTimePerioad.Checked = True Then
                DateEditFrmDate.Enabled = True
                DateEditToDate.Enabled = True
                LabelControl4.Enabled = True
                LabelControl5.Enabled = True
            Else
                DateEditFrmDate.Enabled = False
                DateEditToDate.Enabled = False
                LabelControl4.Enabled = False
                LabelControl5.Enabled = False
            End If
        End If
    End Sub
End Class