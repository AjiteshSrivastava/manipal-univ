﻿Imports DevExpress.XtraGrid.Views.Grid
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Base
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraSplashScreen
Imports DevExpress.Utils
Imports Riss.Devices 'bio2+
Imports iAS.HSeriesSampleCSharp
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Drawing.Imaging
Imports CMITech.UMXClient

Public Class XtraDevice
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info

    'Dim servername As String
    'Dim con As SqlConnection
    'Dim con1 As OleDbConnection
    'Dim ConnectionString As String
    Dim adap1, adap As SqlDataAdapter
    Dim ds As DataSet
    Dim ulf As UserLookAndFeel
    Public Shared DeviceID As String
    Dim h As IntPtr = IntPtr.Zero

    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim buttons As Dictionary(Of Integer, Repository.RepositoryItemButtonEdit) = New Dictionary(Of Integer, Repository.RepositoryItemButtonEdit)

    Public Sub New()
        InitializeComponent()
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
            Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine1
            RepositoryItemLookUpEdit1.DataSource = Tblbranch1TableAdapter1
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
            Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine
            RepositoryItemLookUpEdit1.DataSource = TblbranchTableAdapter
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub FillByToolStripButton_Click(sender As System.Object, e As System.EventArgs)
        Try
            Me.TblMachineTableAdapter.FillBy(Me.SSSDBDataSet.tblMachine)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub XtraDevice_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'GridView1.Appearance.TopNewRow.ForeColor = Color.Blue
        'GridView1.OptionsNavigation.UseOfficePageNavigation = True
        'NavigationPage1.Width = NavigationPage1.Parent.Width
        'Common.NavHeight = NavigationPage1.Height
        'Common.NavWidth = NavigationPage1.Width

        Me.Width = Me.Parent.Width
        Me.Height = Me.Parent.Height
        'SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100
        'MsgBox(SplitContainerControl1.SplitterPosition)
        'not to do in all pages
        Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        Common.SplitterPosition = SplitContainerControl1.SplitterPosition
        'MsgBox(Common.SplitterPosition)

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraDevice).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")
        Me.Text = Common.res_man.GetString("home_title", Common.cul)
        'NavigationPage1.Caption = Common.res_man.GetString("device_tl", Common.cul)
        'NavigationPage2.Caption = Common.res_man.GetString("communication_tl", Common.cul)

        GridView1.Columns.Item(0).Caption = Common.res_man.GetString("controller_id", Common.cul)
        GridView1.Columns.Item(1).Caption = Common.res_man.GetString("protocol", Common.cul)
        GridView1.Columns.Item(2).Caption = Common.res_man.GetString("inout_device_type", Common.cul)
        'GridView1.Columns.Item(3).Caption = Common.res_man.GetString("device_type", Common.cul)
        GridView1.Columns.Item(4).Caption = Common.res_man.GetString("device_ip", Common.cul)
        GridView1.Columns.Item(5).Caption = Common.res_man.GetString("location", Common.cul)
        GridView1.Columns.Item(6).Caption = Common.res_man.GetString("com_key", Common.cul)
        GridView1.Columns.Item(7).Caption = "Serial No" 'Common.res_man.GetString("macadd", Common.cul)

        'If Common.servername = "Access" Then
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
        '    RepositoryItemLookUpEdit1.DataSource = Tblbranch1TableAdapter1
        'Else
        '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        '    Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
        '    RepositoryItemLookUpEdit1.DataSource = TblbranchTableAdapter
        'End If
        GridControl1.DataSource = Common.MachineNonAdmin
        RepositoryItemLookUpEdit1.DataSource = Common.LocationNonAdmin
        If Common.DeviceDelete <> "Y" Then
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = False
        Else
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = True
        End If
        If Common.LogMgmt <> "Y" Then
            BarButtonNewLogs.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            BarButtonOldLogs.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        Else
            BarButtonNewLogs.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            BarButtonOldLogs.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        End If
    End Sub

    'Private Sub GridView1_InitNewRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
    '    Dim view As GridView = CType(sender, GridView)
    '    view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
    '    view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")

    '    Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
    '    If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
    '        view.SetRowCellValue(e.RowHandle, "A_R", "TCP/IP")
    '        view.SetRowCellValue(e.RowHandle, "IN_OUT", "IN")
    '        view.SetRowCellValue(e.RowHandle, "DeviceType", "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872")
    '    End If
    'End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        'Me.TblMachineTableAdapter.Delete(Me.SSSDBDataSet.tblMachine.ID_NOColumn)
        Me.TblMachineTableAdapter.Update(Me.SSSDBDataSet.tblMachine)
        Me.TblMachine1TableAdapter1.Update(Me.SSSDBDataSet.tblMachine1)
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        Me.TblMachineTableAdapter.Update(Me.SSSDBDataSet.tblMachine)
        Me.TblMachine1TableAdapter1.Update(Me.SSSDBDataSet.tblMachine1)
    End Sub
    'Private Sub GridView1_ValidateRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles GridView1.ValidateRow
    '    Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
    '    Dim CellProtocol As String = row(1).ToString.Trim
    '    If CellProtocol = "TCP/IP" Then
    '        row(1) = "T"
    '    ElseIf CellProtocol = "USB" Then
    '        row(1) = "S"
    '        row(4) = "1"
    '    ElseIf CellProtocol = "" Then
    '        e.Valid = False
    '        e.ErrorText = "<size=10>" & Common.res_man.GetString("protocol", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
    '    Else
    '        e.Valid = False
    '        e.ErrorText = "<size=10>" & Common.res_man.GetString("protocol", Common.cul) & " " & Common.res_man.GetString("is_invalid", Common.cul) & ","
    '    End If
    '    'MsgBox(CellProtocol)

    '    Dim CellInOutType = row(2).ToString.Trim
    '    If CellInOutType = "IN" Then
    '        row(2) = "I"
    '    ElseIf CellInOutType = "OUT" Then
    '        row(2) = "O"
    '    ElseIf CellInOutType = "" Then
    '        e.Valid = False
    '        e.ErrorText = "<size=10>" & Common.res_man.GetString("inout_device_type", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
    '    Else
    '        e.Valid = False
    '        e.ErrorText = "<size=10>" & Common.res_man.GetString("inout_device_type", Common.cul) & " " & Common.res_man.GetString("is_invalid", Common.cul) & ","
    '    End If

    '    Dim ipaddress As Net.IPAddress
    '    If Not Net.IPAddress.TryParse(row(4).ToString, ipaddress) Then
    '        e.Valid = False
    '        e.ErrorText = "<size=10>" & Common.res_man.GetString("device_ip", Common.cul) & " " & Common.res_man.GetString("is_invalid", Common.cul) & ","
    '    End If
    '    'MsgBox(row(3).ToString)
    '    If row(3).ToString = "" Then
    '        e.Valid = False
    '        e.ErrorText = "<size=10>" & Common.res_man.GetString("device_type", Common.cul) & " " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
    '    ElseIf DeviceListComboBox1.Items.Contains(row(3).ToString) = False Then
    '        e.Valid = False
    '        e.ErrorText = "<size=10>" & Common.res_man.GetString("device_type", Common.cul) & " " & Common.res_man.GetString("is_invalid", Common.cul) & ","
    '    End If
    '    If row(0).ToString.Trim.Length = 1 Then
    '        row(0) = "0" & row(0).ToString.Trim
    '    End If
    '    Dim view As GridView = CType(sender, GridView)
    '    view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
    '    view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    'End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            If Common.DeviceAdd <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        Else
            If Common.DeviceModi <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        End If

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            DeviceID = row("ID_NO").ToString.Trim
        Catch ex As Exception
            DeviceID = ""
        End Try
        e.Allow = False

        XtraDeviceEdit.ShowDialog()
        Common.loadDevice()
        GridControl1.DataSource = Common.MachineNonAdmin
        For rowHandle As Integer = 0 To GridView1.RowCount - 1
            GridView1.SetRowCellValue(rowHandle, "Status", "")
        Next

        'If Common.servername = "Access" Then   'to refresh the grid
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        'Else
        '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        'End If

        'Try
        '    'Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        '    Dim CellProtocol As String = row(1).ToString.Trim

        '    If CellProtocol = "T" Then
        '        row(1) = "TCP/IP"
        '    ElseIf CellProtocol = "S" Then
        '        row(1) = "USB"
        '    End If

        '    'MsgBox(CellProtocol)
        '    Dim CellInOutType = row(2).ToString.Trim
        '    If CellInOutType = "I" Then
        '        row(2) = "IN"
        '    ElseIf CellInOutType = "O" Then
        '        row(2) = "OUT"
        '    End If
        'Catch
        'End Try
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Try
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "A_R" Then
                Dim coll As String = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "A_R")
                If coll = "T" Then
                    e.DisplayText = "TCP/IP"
                ElseIf coll = "S" Then
                    e.DisplayText = "USB"
                End If
            End If

            If e.Column.FieldName = "IN_OUT" Then
                Dim coll As String = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "IN_OUT")
                If coll = "I" Then
                    e.DisplayText = "IN"
                ElseIf coll = "O" Then
                    e.DisplayText = "OUT"
                ElseIf coll = "B" Then
                    e.DisplayText = "IN/OUT"
                Else
                    e.DisplayText = "DEVICE DIRECTION"
                End If
            End If

            If e.Column.FieldName = "Purpose" Then
                Dim coll As String = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Purpose")
                If coll = "C" Then
                    e.DisplayText = "Canteen"
                ElseIf coll = "A" Then
                    e.DisplayText = "Attendance"
                End If
            End If

            If Common.IsNepali = "Y" Then
                If e.Column.FieldName = "LastDownloaded" Then
                    Dim coll As DateTime = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "LastDownloaded")
                    Dim DC As New DateConverter()
                    Dim Vstart As String = DC.ToBS(New Date(coll.Year, coll.Month, coll.Day))
                    Dim dojTmp() As String = Vstart.Split("-")
                    e.DisplayText = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    'Private Sub NavigationPane1_SelectedPageIndexChanged(sender As System.Object, e As System.EventArgs)
    '    If NavigationPane1.SelectedPageIndex = 1 Then
    '        NavigationPage2.Controls.Clear()
    '        Dim form As UserControl = New XtraCommunication
    '        form.Dock = DockStyle.Fill
    '        NavigationPage2.Controls.Add(form)
    '        form.Show()
    '    End If
    'End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then

            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Dim sSql As String
                Dim cmd As SqlCommand
                Dim cmd1 As OleDbCommand
                e.Handled = True
                Dim selectedRows As Integer() = GridView1.GetSelectedRows()
                Dim result As Object() = New Object(selectedRows.Length - 1) {}
                For i = 0 To selectedRows.Length - 1
                    Dim rowHandle As Integer = selectedRows(i)
                    If Not GridView1.IsGroupRow(rowHandle) Then
                        Dim ID_NO As String = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                        sSql = "DELETE from tblMachine where ID_NO ='" & ID_NO & "'"
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                        Common.LogPost("Device Delete; Device ID: " & ID_NO)
                    End If
                Next
                Common.loadDevice()
                GridControl1.DataSource = Common.MachineNonAdmin
                For rowHandle As Integer = 0 To GridView1.RowCount - 1
                    GridView1.SetRowCellValue(rowHandle, "Status", "")
                Next
                'If Common.servername = "Access" Then
                '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
                '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
                'Else
                '    TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
                '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
                '    GridControl1.DataSource = SSSDBDataSet.tblMachine
                'End If
                XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))

            End If
        End If
    End Sub
    Private Sub GridView1_EditFormPrepared(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs) Handles GridView1.EditFormPrepared
        For Each control As Control In e.Panel.Controls
            For Each button As Control In control.Controls
                If (button.Text = "Update") Then
                    button.Text = Common.res_man.GetString("save", Common.cul)
                End If
                If (button.Text = "Cancel") Then
                    button.Text = Common.res_man.GetString("cancel", Common.cul)
                End If
            Next
        Next
    End Sub
    Private Sub BarButtonItem3_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem3.ItemClick
        XtraCommunicationForm.ShowDialog()
        Me.Cursor = Cursors.WaitCursor
        Common.loadDevice()
        GridControl1.DataSource = Common.MachineNonAdmin
        Application.DoEvents()
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub GridView1_ShowingPopupEditForm(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.ShowingPopupEditFormEventArgs) Handles GridView1.ShowingPopupEditForm
        For Each control As Control In e.EditForm.Controls
            Common.SetFont(control, 9)
        Next control
        e.EditForm.StartPosition = FormStartPosition.CenterParent
    End Sub
    Private Sub BarButtonItem4_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem4.ItemClick
        XtraFringerDataMgmt.ShowDialog()
    End Sub
    Private Sub BarButtonItem5_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        XtraRealTimePunches.Show()
    End Sub
    Private Sub BarButtonNewLogs_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonNewLogs.ItemClick
        downloadLogs("N")
        Common.loadDevice()
        GridControl1.DataSource = Common.MachineNonAdmin
        Application.DoEvents()
    End Sub
    Private Sub BarButtonOldLogs_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonOldLogs.ItemClick
        downloadLogs("A")
    End Sub
    Private Sub downloadLogs(ByVal type As Char)
        Dim datafile As String = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
        If GridView1.SelectedRowsCount = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select device</size>", "Failed")
            Exit Sub
        End If

        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer

        Dim cmd1 As OleDbCommand
        Dim cmd As SqlCommand

        If type = "A" Then   'All logs
            vnReadMark = 0
        ElseIf type = "N" Then   'new logs
            vnReadMark = 1
        End If
        Dim clearLog As Boolean = False

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R, Purpose As Object
        Dim commkey As Integer
        Dim IN_OUT As String
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
        For Each rowHandle As Integer In RowHandles
            IP = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("LOCATION"))
            IN_OUT = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("IN_OUT"))
            XtraMasterTest.LabelControlStatus.Text = "Connecting " & IP & "..."
            Application.DoEvents()

            ID_NO = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("ID_NO"))
            DeviceType = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("DeviceType"))
            A_R = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("A_R"))
            Purpose = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("Purpose"))
            If DeviceType.ToString.Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 1261

                Dim result As String = cn.funcGetGeneralLogData(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, clearLog, A_R, Purpose, IN_OUT, ID_NO, datafile, False)
                'MsgBox("Result " & result)
                If result <> "Success" Then
                    If result.Contains("Invalid Serial number") Then
                        SplashScreenManager.CloseForm(False)
                        XtraMessageBox.Show(ulf, "<size=10>" & result & "</size>", "<size=9>Error</size>")
                        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
                        Continue For
                    Else
                        failIP.Add(IP.ToString)
                    End If
                End If
            ElseIf DeviceType.ToString.Trim = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                Dim adap, adap1 As SqlDataAdapter
                Dim adapA, adapA1 As OleDbDataAdapter
                Dim ds, ds1 As DataSet
                Dim sdwEnrollNumber As String = ""
                Dim idwVerifyMode As Integer
                Dim idwInOutMode As Integer
                Dim idwYear As Integer
                Dim idwMonth As Integer
                Dim idwDay As Integer
                Dim idwHour As Integer
                Dim idwMinute As Integer
                Dim idwSecond As Integer
                Dim idwWorkcode As Integer
                commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                Dim comZK As CommonZK = New CommonZK
                Dim bIsConnected = False
                Dim iMachineNumber As Integer
                Dim idwErrorCode As Integer
                Dim com As Common = New Common
                Dim axCZKEM1 As New zkemkeeper.CZKEM
                axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                bIsConnected = axCZKEM1.Connect_Net(IP, 4370)
                If bIsConnected = True Then
                    iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                    axCZKEM1.RegEvent(iMachineNumber, 65535) 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                    axCZKEM1.EnableDevice(iMachineNumber, False) 'disable the device
                    Dim McSrno As String = ""
                    Dim vRet As Boolean = axCZKEM1.GetSerialNumber(iMachineNumber, McSrno)
                    'If McSrno.Substring(0, 3) = "AF6" Or McSrno.Substring(0, 3) = "AEH" Or McSrno.Substring(0, 3) = "AJV" Or McSrno.Substring(0, 3) = "AOO" Or McSrno.Substring(0, 3) = "719" Or McSrno.Substring(0, 3) = "ALM" Or McSrno.Substring(0, 3) = "701" Or McSrno.Substring(0, 3) = "BYR" Or McSrno.Substring(0, 3) = "OIN" Or McSrno.Substring(0, 3) = "AF6" Or McSrno.Substring(0, 3) = "BYX" Or McSrno.Substring(0, 3) = "AIJ" Or McSrno.Substring(0, 3) = "A6G" Or McSrno.Substring(0, 3) = "AEH" Or McSrno.Substring(0, 3) = "CCG" Or McSrno.Substring(0, 3) = "BJV" _
                    '    Or Mid(Trim(McSrno), 1, 1) = "A" Or Mid(Trim(McSrno), 1, 3) = "OIN" Or Mid(Trim(McSrno), 1, 5) = "FPCTA" Or Mid(Trim(McSrno), 1, 4) = "0000" Or Mid(Trim(McSrno), 1, 4) = "1808" Or Mid(Trim(McSrno), 1, 4) = "ATPL" Or Mid(Trim(McSrno), 1, 4) = "TIPL" Or Mid(Trim(McSrno), 1, 4) = "ZXJK" Or Mid(Trim(McSrno), 1, 8) = "10122013" Or Mid(Trim(McSrno), 1, 8) = "76140122" Or Mid(Trim(McSrno), 1, 8) = "17614012" Or Mid(Trim(McSrno), 1, 8) = "17214012" Or Mid(Trim(McSrno), 1, 8) = "27022014" Or Mid(Trim(McSrno), 1, 8) = "24042014" Or Mid(Trim(McSrno), 1, 8) = "25042014" _
                    '    Or Mid(Trim(McSrno), 1, 8) = "26042014" Or Mid(Trim(McSrno), 1, 7) = "SN:0000" Or Mid(Trim(McSrno), 1, 4) = "BOCK" Or McSrno.Substring(0, 1) = "B" Or McSrno.Substring(0, 4) = "2455" Or McSrno.Substring(0, 4) = "CDSL" _
                    '     Or McSrno = "6583151100400" Then
                    If license.DeviceSerialNo.Contains(McSrno.Trim) Then
                    Else
                        SplashScreenManager.CloseForm(False)
                        axCZKEM1.EnableDevice(iMachineNumber, True)
                        axCZKEM1.Disconnect()
                        XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & McSrno & "</size>", "<size=9>Error</size>")
                        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
                        Continue For
                    End If
                    Dim LogResult As Boolean
                    If type = "A" Then   'all logs
                        LogResult = axCZKEM1.ReadGeneralLogData(iMachineNumber)
                    ElseIf type = "N" Then   ' new logs
                        LogResult = axCZKEM1.ReadNewGLogData(iMachineNumber)
                    End If
                    If LogResult Then 'read all the attendance records to the memory
                        XtraMasterTest.LabelControlStatus.Text = "Downloading from " & IP & "..."
                        Application.DoEvents()
                        'get records from the memory
                        Dim x As Integer = 0
                        Dim startdate As DateTime
                        Dim paycodelist As New List(Of String)()
                        While axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)
                            paycodelist.Add(sdwEnrollNumber)
                            Dim punchdate = idwYear.ToString() & "-" + idwMonth.ToString("00") & "-" & idwDay.ToString("00") & " " & idwHour.ToString("00") & ":" & idwMinute.ToString("00") & ":" & idwSecond.ToString("00")
                            If x = 0 Then
                                startdate = punchdate
                            End If
                            com.funcGetGeneralLogDataZK(sdwEnrollNumber, idwVerifyMode, idwInOutMode, punchdate, idwWorkcode, Purpose, IN_OUT, ID_NO, datafile)
                            x = x + 1
                        End While
                        Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(xsSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(xsSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                        Dim paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
                        ds = New DataSet
                        For i As Integer = 0 To paycodeArray.Length - 1
                            Dim PRESENTCARDNO As String
                            If IsNumeric(paycodeArray(i)) Then
                                PRESENTCARDNO = Convert.ToInt64(paycodeArray(i)).ToString("000000000000")
                            Else
                                PRESENTCARDNO = paycodeArray(i)
                            End If
                            Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "'  and TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId "
                            ds = New DataSet
                            If Common.servername = "Access" Then
                                adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                                adapA.Fill(ds)
                            Else
                                adap = New SqlDataAdapter(sSqltmp, Common.con)
                                adap.Fill(ds)
                            End If
                            If ds.Tables(0).Rows.Count > 0 Then
                                cn.Remove_Duplicate_Punches(startdate, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                    com.Process_AllRTC(startdate.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                Else
                                    com.Process_AllnonRTC(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    If Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                                        com.Process_AllnonRTCMulti(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    End If
                                End If
                            End If
                        Next
                    Else
                        Cursor = Cursors.Default
                        axCZKEM1.GetLastError(idwErrorCode)
                        If idwErrorCode <> 0 Then
                            failIP.Add(IP.ToString)
                        Else
                            'MsgBox("No data from terminal returns!", MsgBoxStyle.Exclamation, "Error")
                        End If
                    End If
                    axCZKEM1.EnableDevice(iMachineNumber, True)
                    If clearLog = True Then
                        axCZKEM1.ClearGLog(iMachineNumber)
                    End If
                Else
                    axCZKEM1.GetLastError(idwErrorCode)
                    failIP.Add(IP.ToString)
                    Continue For
                End If
                axCZKEM1.Disconnect()
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand("delete from Rawdata", Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand("delete from Rawdata", Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            ElseIf DeviceType = "Bio1Eco" Then
                Dim adap, adap1 As SqlDataAdapter
                Dim adapA, adapA1 As OleDbDataAdapter
                Dim ds, ds1 As DataSet
                commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                Dim mOpenFlag As Boolean
                Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

                Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
                Dim equNo As UInt32 = Convert.ToUInt32(ID_NO)
                Dim gEquNo As UInt32 = equNo
                If A_R = "T" Then
                    Dim netCfg As SyNetCfg
                    Dim pswd As UInt32 = 0
                    Dim tmpPswd As String = commkey
                    If (tmpPswd = "0") Then
                        tmpPswd = "FFFFFFFF"
                    End If
                    Try
                        pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                    Catch ex As Exception
                        XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                        ret = CType(SyLastError.slePasswordError, Int32)
                    End Try
                    If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                        netCfg.mIsTCP = 1
                        netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                        netCfg.mIPAddr = New Byte((4) - 1) {}
                        Dim sArray() As String = IP.Split(Microsoft.VisualBasic.ChrW(46))
                        Dim j As Byte = 0
                        For Each i As String In sArray
                            Try
                                netCfg.mIPAddr(j) = Convert.ToByte(i.Trim)
                            Catch ex As System.Exception
                                netCfg.mIPAddr(j) = 255
                            End Try
                            j = (j + 1)
                            If j > 3 Then
                                Exit For
                            End If

                        Next
                    End If

                    Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                    Try
                        Marshal.StructureToPtr(netCfg, pnt, False)
                        '        If optWifiDevice.Checked Then
                        'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                        '        Else
                        ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                        '        End If
                        '                'TODO: Warning!!!, inline IF is not supported ?
                        '                chkTranceIO.Checked()
                        '0:
                        '                '
                    Finally
                        Marshal.FreeHGlobal(pnt)
                    End Try
                ElseIf A_R = "S" Then
                    Dim equtype As String = "Finger Module USB Device"
                    Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                    ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
                End If
                If (ret = CType(SyLastError.sleSuss, Int32)) Then
                    ret = SyFunctions.CmdTestConn2Device
                    '
                    If (ret = CType(SyLastError.sleSuss, Int32)) Then
                        ret = 66
                        Dim totalCnt As Integer = 0, unreadPos = 0
                        Dim i As Integer, remain = 0
                        Dim read As Byte = 0, dwMonth, dwDay, dwHour, dwMinute, dwSecond
                        Dim kind As Byte = 0, hasCooperated = 0, hasSetup = 0, hasOpenDoor = 0, hasAlarm = 0
                        Dim dwYear As System.UInt16 = 0
                        Dim dwEnrollID As Integer, dwLogType
                        Dim logType As String = ""
                        dwEnrollID = 0
                        dwLogType = 0
                        dwYear = 0
                        dwMonth = 0
                        dwDay = 0
                        dwHour = 0
                        dwMinute = 0
                        dwSecond = 0
                        SyFunctions.CmdLockDevice()
                        If type = "A" Then   'all logs
                            ret = SyFunctions.CmdGetCountOfValidDailyEntryRecord(totalCnt, unreadPos)
                            unreadPos = 0
                        ElseIf type = "N" Then   ' new logs
                            ret = SyFunctions.CmdGetCountOfValidDailyEntryRecord(totalCnt, unreadPos)
                        End If

                        Dim index As Integer = 0
                        If (ret = CType(SyLastError.sleSuss, Int32)) Then
                            Dim records As StringBuilder = New StringBuilder
                            i = unreadPos
                            remain = (totalCnt - unreadPos)
                            Dim x As Integer = 0
                            Dim startdate As DateTime
                            Dim paycodelist As New List(Of String)()

                            While (i < totalCnt)
                                If (remain > 64) Then
                                    read = 64
                                Else
                                    read = CType(remain, Byte)
                                End If

                                Dim dera() As SyLogRecord = New SyLogRecord((read) - 1) {}
                                ret = SyFunctions.CmdGetValidDailyEntryRecord(dera, i, read)
                                If (ret = CType(SyLastError.sleSuss, Int32)) Then
                                    For j As Byte = 0 To read - 1 ' Do While (j < read)
                                        If (dera(j).wUserID = 0) Or (dera(j).wUserID > 65534) Then
                                            Continue For
                                        End If

                                        dwEnrollID = dera(j).wUserID
                                        SyFunctions.DecodeSyDailyEntryType(dera(j).cLogType, kind, hasCooperated, hasSetup, hasOpenDoor, hasAlarm)
                                        SyFunctions.DecodeSyDailyEntryTime(dera(j).sRecordTime, dwSecond, dwMinute, dwHour, dwDay, dwMonth, dwYear)
                                        Dim dt As DateTime = New DateTime(dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond)
                                        dwLogType = (kind Mod 8)

                                        paycodelist.Add(dwEnrollID)
                                        Dim punchdate = dt 'idwYear.ToString() & "-" + idwMonth.ToString("00") & "-" & idwDay.ToString("00") & " " & idwHour.ToString("00") & ":" & idwMinute.ToString("00") & ":" & idwSecond.ToString("00")
                                        If x = 0 Then
                                            startdate = punchdate
                                        End If
                                        cn.funcGetGeneralLogDataZK(dwEnrollID, logType, "", punchdate, "", Purpose, IN_OUT, ID_NO, datafile)
                                        x = x + 1

                                        'Select Case (dwLogType)
                                        '    Case 1
                                        '        'finger
                                        '        logType = "Fingerprint"
                                        '    Case 4
                                        '        'finger
                                        '        logType = "Fingerprint + Password"
                                        '    Case 5
                                        '        'finger
                                        '        logType = "Fingerprint + Card"
                                        '    Case 6
                                        '        'card
                                        '        logType = "Password + Card"
                                        '    Case 7
                                        '        'finger
                                        '        logType = "Fingerprint + Card + Password"
                                        '    Case 2
                                        '        'password
                                        '        logType = "Password"
                                        '    Case 3
                                        '        'card
                                        '        logType = "Card"
                                        '    Case Else
                                        '        logType = ""
                                        'End Select

                                        'row = dtLog.NewRow
                                        'row("no") = (index + 1).ToString
                                        'row("MachineNo") = Program.gEquNo.ToString
                                        'row("EnrollNo") = dwEnrollID
                                        'row("DateTime") = dt.ToString
                                        'row("LogType") = logType
                                        'dtLog.Rows.Add(row)
                                        index = (index + 1)

                                    Next 'Loop
                                    'end for  block record
                                End If
                                'end if
                                i = (i + read)
                            End While
                            'while
                            Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
                            If Common.servername = "Access" Then
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(xsSql, Common.con1)
                                cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(xsSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                            Dim paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
                            ds = New DataSet
                            For i = 0 To paycodeArray.Length - 1
                                Dim PRESENTCARDNO As String
                                If IsNumeric(paycodeArray(i)) Then
                                    PRESENTCARDNO = Convert.ToInt64(paycodeArray(i)).ToString("000000000000")
                                Else
                                    PRESENTCARDNO = paycodeArray(i)
                                End If
                                Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "'  and TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId "
                                ds = New DataSet
                                If Common.servername = "Access" Then
                                    adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                                    adapA.Fill(ds)
                                Else
                                    adap = New SqlDataAdapter(sSqltmp, Common.con)
                                    adap.Fill(ds)
                                End If
                                If ds.Tables(0).Rows.Count > 0 Then
                                    cn.Remove_Duplicate_Punches(startdate, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                        cn.Process_AllRTC(startdate.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    Else
                                        cn.Process_AllnonRTC(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                        If Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                                            cn.Process_AllnonRTCMulti(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    End If
                    SyFunctions.CmdUnLockDevice()
                    SyFunctions.CloseCom()
                Else
                    failIP.Add(IP.ToString)
                    Continue For
                End If
            ElseIf DeviceType = "EF45" Then
                XtraMasterTest.LabelControlStatus.Text = "Downloading from " & IP & "..."
                Application.DoEvents()
                Dim adapA As OleDbDataAdapter
                Dim EF45LogsImages As String = System.Environment.CurrentDirectory & "\EF45_LogsImages"
                If (Not System.IO.Directory.Exists(EF45LogsImages)) Then
                    System.IO.Directory.CreateDirectory(EF45LogsImages)
                End If

                Dim _client As Client = Nothing
                Dim connectOk As Boolean = False
                Try
                    _client = cn.initClientEF45(IP)
                    connectOk = _client.StealConnect '_client.Connect()
                    If connectOk Then
                        Dim com As Common = New Common
                        Dim logs As List(Of CMITech.UMXClient.Entities.Log) = _client.GetLogs
                        If (Not (logs) Is Nothing) Then
                            Dim x As Integer = 0
                            Dim startdate As DateTime
                            Dim paycodelist As New List(Of String)()

                            For Each log As CMITech.UMXClient.Entities.Log In logs
                                'dataGridViewLog.Rows.Add(log.LogUID, log.EventType, log.Info, log.AdditionalData, log.SubjectUID)
                                If log.EventType = "Recognition" Then  ' for logs
                                    If log.SubjectUID.Trim <> "" Then
                                        paycodelist.Add(log.SubjectUID)
                                        Dim punchdate = log.Timestamp.ToString("yyyy-MM-dd HH:mm:ss") ' idwYear.ToString() & "-" & idwMonth.ToString("00") & "-" & idwDay.ToString("00") & " " & idwHour.ToString("00") & ":" & idwMinute.ToString("00") & ":" & idwSecond.ToString("00")
                                        If x = 0 Then
                                            startdate = punchdate
                                        End If
                                        com.funcGetGeneralLogDataZK(log.SubjectUID, log.Info, "", punchdate, "", Purpose, IN_OUT, ID_NO, datafile)
                                        Try
                                            Dim bm As Bitmap = log.MatchedFaceImage
                                            ' Save the picture as a bitmap, JPEG, and GIF. 
                                            Dim oBitmap As Bitmap
                                            oBitmap = New Bitmap(log.MatchedFaceImage)
                                            Dim oGraphic As Graphics
                                            ' Here create a new bitmap object of the same height and width of the image.
                                            Dim bmpNew As Bitmap = New Bitmap(oBitmap.Width, oBitmap.Height)
                                            oGraphic = Graphics.FromImage(bmpNew)
                                            oGraphic.DrawImage(oBitmap, New Rectangle(0, 0,
                                            bmpNew.Width, bmpNew.Height), 0, 0, oBitmap.Width,
                                            oBitmap.Height, GraphicsUnit.Pixel)
                                            ' Release the lock on the image file. Of course,
                                            ' image from the image file is existing in Graphics object
                                            oBitmap.Dispose()
                                            oBitmap = bmpNew
                                            oGraphic.Dispose()
                                            Dim imageName As String = My.Application.Info.DirectoryPath & "\EF45_LogsImages\" & log.SubjectUID & "_" & log.Timestamp.ToString("yyyyMMddHHmmss") & ".jpg"
                                            oBitmap.Save(imageName, ImageFormat.Jpeg)
                                            oBitmap.Dispose()
                                        Catch ex As Exception

                                        End Try
                                        x = x + 1
                                    End If
                                End If
                            Next
                            _client.Disconnect()
                            Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
                            If Common.servername = "Access" Then
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(xsSql, Common.con1)
                                cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(xsSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                            Dim paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
                            ds = New DataSet
                            For i As Integer = 0 To paycodeArray.Length - 1
                                Dim PRESENTCARDNO As String
                                If IsNumeric(paycodeArray(i)) Then
                                    PRESENTCARDNO = Convert.ToInt64(paycodeArray(i)).ToString("000000000000")
                                Else
                                    PRESENTCARDNO = paycodeArray(i)
                                End If
                                'Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "'"
                                Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                                ds = New DataSet
                                If Common.servername = "Access" Then
                                    adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                                    adapA.Fill(ds)
                                Else
                                    adap = New SqlDataAdapter(sSqltmp, Common.con)
                                    adap.Fill(ds)
                                End If
                                If ds.Tables(0).Rows.Count > 0 Then
                                    cn.Remove_Duplicate_Punches(startdate, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                        com.Process_AllRTC(startdate.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    Else
                                        com.Process_AllnonRTC(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    End If
                                End If
                            Next
                        Else
                            'MessageBox.Show("[Error] " & DateTime.Now & " Fail to get logs")
                            'm_logMessages.AddMessage(LogMessages.Icon.Error, DateTime.Now, "Fail to get logs")
                        End If
                    Else
                        failIP.Add(IP.ToString)
                        Continue For
                    End If
                Catch ex As Exception
                    failIP.Add(IP.ToString)
                    Continue For
                End Try
            ElseIf DeviceType.ToString.Trim = "F9" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881

                Dim result As String = cn.funcGetGeneralLogData_f9(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, clearLog, A_R, Purpose, IN_OUT, ID_NO, datafile, False)
                'MsgBox("Result " & result)
                If result <> "Success" Then
                    failIP.Add(IP.ToString)
                End If
            ElseIf DeviceType.ToString.Trim = "ATF686n" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881
                Dim CreateEmpMaster As Boolean = False
                Dim result As String = cn.funcGetGeneralLogData_ATF686n(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, clearLog, A_R, Purpose, IN_OUT, ID_NO, datafile, CreateEmpMaster)
                'MsgBox("Result " & result)
                If result <> "Success" Then
                    failIP.Add(IP.ToString)
                End If
            ElseIf DeviceType.ToString.Trim = "TF-01" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881

                Dim result As String = cn.funcGetGeneralLogData_TF01(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, clearLog, A_R, Purpose, IN_OUT, ID_NO, datafile, False, AxFP_CLOCK1)
                'MsgBox("Result " & result)


                If result <> "Success" Then
                    failIP.Add(IP.ToString)
                End If
            End If
            If type = "N" Then
                Common.LogPost("Device New Log Download; Device ID: " & ID_NO)
            ElseIf type = "A" Then
                Common.LogPost("Device All Log Download; Device ID: " & ID_NO)
            End If

        Next
        '"Connection fail" ' write logic to show connection fail devices
        SplashScreenManager.CloseForm(False)
        Dim failedIpArr() As String = failIP.ToArray
        Dim failIpStr As String = ""
        If failedIpArr.Length > 0 Then
            If failedIpArr.Length = 1 Then
                failIpStr = failedIpArr(0)
            Else
                For i As Integer = 0 To failedIpArr.Length - 1
                    failIpStr = failIpStr & ", " & failedIpArr(i)
                Next
            End If
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
            XtraMessageBox.Show(ulf, "<size=10>" & failIpStr.TrimStart(",") & " failed to download data</size>", "Failed")
        Else
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
            Common.loadDevice()
            GridControl1.DataSource = Common.MachineNonAdmin
            Application.DoEvents()
            XtraMessageBox.Show(ulf, "<size=10>Download data success</size>", "Success")
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub
    Private Sub GridControl1_Load(sender As System.Object, e As System.EventArgs) Handles GridControl1.Load
        For rowHandle As Integer = 0 To GridView1.RowCount - 1
            GridView1.SetRowCellValue(rowHandle, "Status", "")
        Next

    End Sub
    Private Sub BarButtonItem6_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        'If GridView1.SelectedRowsCount = 0 Then
        '    XtraMessageBox.Show(ulf, "<size=10>Please select device</size>", "Failed")
        '    Exit Sub
        'End If
        Me.Cursor = Cursors.WaitCursor
        For i As Integer = 0 To GridView1.RowCount - 1
            GridView1.SetRowCellValue(i, "Status", "")
        Next
        Application.DoEvents()
        Me.Cursor = Cursors.WaitCursor

        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer

        Dim cmd1 As OleDbCommand
        Dim cmd As SqlCommand
        Dim sSql As String

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R, Purpose As Object
        Dim commkey As Integer
        Dim IN_OUT As String
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        'DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
        For rowHandle As Integer = 0 To GridView1.RowCount - 1
            IP = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("LOCATION"))
            IN_OUT = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("IN_OUT"))
            XtraMasterTest.LabelControlStatus.Text = "Connecting " & IP & "..."
            Application.DoEvents()
            Me.Cursor = Cursors.WaitCursor
            ID_NO = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("ID_NO"))
            DeviceType = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("DeviceType"))
            A_R = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("A_R"))
            Purpose = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("Purpose"))
            If DeviceType.ToString.Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 1261
                Dim gnCommHandleIndex As Long
                If A_R = "S" Then
                    gnCommHandleIndex = FK_ConnectUSB(vnMachineNumber, vnLicense)
                Else
                    gnCommHandleIndex = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                End If
                If gnCommHandleIndex > 0 Then
                    GridView1.SetRowCellValue(rowHandle, "Status", "Online")
                    Application.DoEvents()
                    Me.Cursor = Cursors.WaitCursor
                    'GridView1.SetRowCellValue(rowHandle, "LastModifiedBy", "Online")
                    Dim UserCount As String = "0"
                    Dim FPCount As String = "0"
                    Dim FaceCount As String = "0"
                    Dim AttLogCount As String = "0"
                    Dim AdminCount As String = "0"

                    Dim vnStatusIndex As Integer
                    Dim vnValue As Integer
                    Dim vnResultCode As Integer
                    vnResultCode = FK_EnableDevice(gnCommHandleIndex, 0)

                    vnStatusIndex = GET_USERS
                    vnResultCode = FK_GetDeviceStatus(gnCommHandleIndex, vnStatusIndex, vnValue)
                    If vnResultCode = RUN_SUCCESS Then
                        UserCount = vnValue
                    End If

                    vnStatusIndex = GET_FPS
                    vnResultCode = FK_GetDeviceStatus(gnCommHandleIndex, vnStatusIndex, vnValue)
                    If vnResultCode = RUN_SUCCESS Then
                        FPCount = vnValue
                    End If

                    vnStatusIndex = GET_FACES
                    vnResultCode = FK_GetDeviceStatus(gnCommHandleIndex, vnStatusIndex, vnValue)
                    If vnResultCode = RUN_SUCCESS Then
                        FaceCount = vnValue
                    End If

                    vnStatusIndex = GET_AGLOGS
                    vnResultCode = FK_GetDeviceStatus(gnCommHandleIndex, vnStatusIndex, vnValue)
                    If vnResultCode = RUN_SUCCESS Then
                        AttLogCount = vnValue
                    End If

                    vnStatusIndex = GET_MANAGERS
                    vnResultCode = FK_GetDeviceStatus(gnCommHandleIndex, vnStatusIndex, vnValue)
                    If vnResultCode = RUN_SUCCESS Then
                        AdminCount = vnValue
                    End If
                    vnResultCode = FK_EnableDevice(gnCommHandleIndex, 1)
                    sSql = "UPDATE tblMachine set  [Status] ='Online',  [UserCount]  = '" & UserCount & "' ,[FPCount]='" & FPCount & "',[FaceCount]='" & FaceCount & "',[AttLogCount]='" & AttLogCount & "',[AdminCount]='" & AdminCount & "' where ID_NO='" & ID_NO & "'"
                    FK_DisConnect(gnCommHandleIndex)
                Else
                    GridView1.SetRowCellValue(rowHandle, "Status", "Offline")
                    Application.DoEvents()
                    Me.Cursor = Cursors.WaitCursor
                    sSql = "UPDATE tblMachine set  [Status] ='Offline' where ID_NO='" & ID_NO & "'"
                End If
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                'Common.loadDevice()
                'GridControl1.DataSource = Common.MachineNonAdmin
                'Application.DoEvents()
                'MsgBox("Result " & result)               
            ElseIf DeviceType.ToString.Trim = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                Dim adap, adap1 As SqlDataAdapter
                Dim adapA, adapA1 As OleDbDataAdapter
                Dim ds, ds1 As DataSet
                Dim sdwEnrollNumber As String = ""
                Dim idwVerifyMode As Integer
                Dim idwInOutMode As Integer
                Dim idwYear As Integer
                Dim idwMonth As Integer
                Dim idwDay As Integer
                Dim idwHour As Integer
                Dim idwMinute As Integer
                Dim idwSecond As Integer
                Dim idwWorkcode As Integer
                commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                Dim comZK As CommonZK = New CommonZK
                Dim bIsConnected = False
                Dim iMachineNumber As Integer
                Dim idwErrorCode As Integer
                Dim com As Common = New Common
                Dim axCZKEM1 As New zkemkeeper.CZKEM
                axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                bIsConnected = axCZKEM1.Connect_Net(IP, 4370)
                If bIsConnected = True Then
                    GridView1.SetRowCellValue(rowHandle, "Status", "Online")
                    Application.DoEvents()
                    Me.Cursor = Cursors.WaitCursor
                    Dim adminCnt = 0
                    Dim userCount = 0
                    Dim fpCnt = 0
                    Dim recordCnt = 0
                    Dim pwdCnt = 0
                    Dim oplogCnt = 0
                    Dim faceCnt = 0
                    Dim GetMachineNumber = 1
                    axCZKEM1.EnableDevice(GetMachineNumber, False)
                    'disable the device
                    axCZKEM1.GetDeviceStatus(GetMachineNumber, 2, userCount)
                    axCZKEM1.GetDeviceStatus(GetMachineNumber, 1, adminCnt)
                    axCZKEM1.GetDeviceStatus(GetMachineNumber, 3, fpCnt)
                    axCZKEM1.GetDeviceStatus(GetMachineNumber, 4, pwdCnt)
                    axCZKEM1.GetDeviceStatus(GetMachineNumber, 5, oplogCnt)
                    axCZKEM1.GetDeviceStatus(GetMachineNumber, 6, recordCnt)
                    axCZKEM1.GetDeviceStatus(GetMachineNumber, 21, faceCnt)
                    axCZKEM1.Disconnect()

                    sSql = "UPDATE tblMachine set  [Status] ='Online',  [UserCount]  = '" & userCount & "' ,[FPCount]='" & fpCnt & "',[FaceCount]='" & faceCnt & "',[AttLogCount]='" & recordCnt & "',[AdminCount]='" & adminCnt & "' where ID_NO='" & ID_NO & "'"
                    'Common.loadDevice()
                    'GridControl1.DataSource = Common.MachineNonAdmin
                    'Application.DoEvents()

                Else
                    GridView1.SetRowCellValue(rowHandle, "Status", "Offline")
                    Application.DoEvents()
                    Me.Cursor = Cursors.WaitCursor
                    sSql = "UPDATE tblMachine set  [Status] ='Offline' where ID_NO='" & ID_NO & "'"
                    axCZKEM1.GetLastError(idwErrorCode)
                    failIP.Add(IP.ToString)
                End If
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            ElseIf DeviceType = "Bio2+" Then
                commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                Dim deviceG2 As Riss.Devices.Device
                Dim deviceConnectionG2 As Riss.Devices.DeviceConnection
                deviceG2 = New Riss.Devices.Device()
                deviceG2.DN = 1 'CInt(nud_DN.Value)
                deviceG2.Password = commkey ' TextEditComm.Text.Trim ' nud_Pwd.Value.ToString()
                deviceG2.Model = "ZDC2911"
                deviceG2.ConnectionModel = 5
                If A_R = "S" Then
                    deviceG2.CommunicationType = CommunicationType.Usb
                Else
                    deviceG2.IpAddress = IP ' TextEdit2.Text.Trim()
                    deviceG2.IpPort = 5500 'CInt(nud_Port.Value)
                    deviceG2.CommunicationType = CommunicationType.Tcp
                End If
                deviceConnectionG2 = DeviceConnection.CreateConnection(deviceG2)
                If deviceConnectionG2.Open() > 0 Then
                    GridView1.SetRowCellValue(rowHandle, "Status", "Online")
                    Application.DoEvents()
                    Me.Cursor = Cursors.WaitCursor
                    sSql = "UPDATE tblMachine set  [Status] ='Online' where ID_NO='" & ID_NO & "'"
                Else
                    GridView1.SetRowCellValue(rowHandle, "Status", "Offline")
                    Application.DoEvents()
                    Me.Cursor = Cursors.WaitCursor
                    sSql = "UPDATE tblMachine set  [Status] ='Offline' where ID_NO='" & ID_NO & "'"
                End If
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If

            ElseIf DeviceType = "Bio1Eco" Then
                commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                Dim mOpenFlag As Boolean
                Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

                Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
                Dim equNo As UInt32 = Convert.ToUInt32(ID_NO)
                Dim gEquNo As UInt32 = equNo
                If A_R = "T" Then
                    Dim netCfg As SyNetCfg
                    Dim pswd As UInt32 = 0
                    Dim tmpPswd As String = commkey
                    If (tmpPswd = "0") Then
                        tmpPswd = "FFFFFFFF"
                    End If
                    Try
                        pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                    Catch ex As Exception
                        XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                        ret = CType(SyLastError.slePasswordError, Int32)
                    End Try
                    If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                        netCfg.mIsTCP = 1
                        netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                        netCfg.mIPAddr = New Byte((4) - 1) {}
                        Dim sArray() As String = IP.Split(Microsoft.VisualBasic.ChrW(46))
                        Dim j As Byte = 0
                        For Each i As String In sArray
                            Try
                                netCfg.mIPAddr(j) = Convert.ToByte(i.Trim)
                            Catch ex As System.Exception
                                netCfg.mIPAddr(j) = 255
                            End Try
                            j = (j + 1)
                            If j > 3 Then
                                Exit For
                            End If

                        Next
                    End If

                    Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                    Try
                        Marshal.StructureToPtr(netCfg, pnt, False)
                        '        If optWifiDevice.Checked Then
                        'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                        '        Else
                        ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                        '        End If
                        '                'TODO: Warning!!!, inline IF is not supported ?
                        '                chkTranceIO.Checked()
                        '0:
                        '                '
                    Finally
                        Marshal.FreeHGlobal(pnt)
                    End Try
                ElseIf A_R = "S" Then
                    Dim equtype As String = "Finger Module USB Device"
                    Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                    ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
                End If
                If (ret = CType(SyLastError.sleSuss, Int32)) Then
                    GridView1.SetRowCellValue(rowHandle, "Status", "Online")
                    Application.DoEvents()
                    ret = SyFunctions.CmdTestConn2Device
                    '
                    If (ret = CType(SyLastError.sleSuss, Int32)) Then
                        mOpenFlag = True
                        mMK8001Device = False
                        Dim si As SySystemInfo = New SySystemInfo
                        si.cSerialNum = New Byte((20) - 1) {}
                        si.cManuName = New Byte((24) - 1) {}
                        si.cDevName = New Byte((24) - 1) {}
                        si.cAlgVer = New Byte((16) - 1) {}
                        si.cFirmwareVer = New Byte((24) - 1) {}
                        Dim rret As Integer = 66
                        Try
                            rret = SyFunctions.CmdGetSystemInfo(si)
                        Catch ex As Exception

                        End Try

                        If (rret = CType(SyLastError.sleSuss, Int32)) Then
                            Dim sFirmwareVer As String = System.Text.Encoding.Default.GetString(si.cFirmwareVer)
                            If sFirmwareVer.Contains("MK8001/8002") Then
                                mMK8001Device = True
                            Else
                                mMK8001Device = False
                            End If
                        End If
                        Dim gbk As Encoding = Encoding.GetEncoding(20936)

                        GridView1.SetRowCellValue(rowHandle, "Status", "Online")
                        Application.DoEvents()
                        Me.Cursor = Cursors.WaitCursor
                        Dim adminCnt = 0
                        Dim userCount = 0
                        Dim fpCnt = 0
                        Dim recordCnt = 0
                        Dim pwdCnt = 0
                        Dim oplogCnt = 0
                        Dim faceCnt = 0
                        Dim GetMachineNumber = 1

                        adminCnt = String.Format("{0, 6:D}", si.wAdminLogCnt)
                        userCount = String.Format("{0, 6:D}", si.wUserCnt)
                        fpCnt = String.Format("{0, 6:D}", si.wFpCnt)
                        recordCnt = String.Format("{0, 6:D}", si.wAttLogCnt)
                        pwdCnt = String.Format("{0, 6:D}", si.wPwsCnt)
                        oplogCnt = String.Format("{0, 6:D}", si.wLogNum)
                        sSql = "UPDATE tblMachine set  [Status] ='Online',  [UserCount]  = '" & userCount & "' ,[FPCount]='" & fpCnt & "',[FaceCount]='" & faceCnt & "',[AttLogCount]='" & recordCnt & "',[AdminCount]='" & adminCnt & "' where ID_NO='" & ID_NO & "'"
                        'TextSrNo.Text = Encoding.Default.GetString(Encoding.Convert(gbk, Encoding.Default, si.cSerialNum, 0, 13))
                    End If
                    SyFunctions.CloseCom()
                Else
                    GridView1.SetRowCellValue(rowHandle, "Status", "Offline")
                    Application.DoEvents()
                    Me.Cursor = Cursors.WaitCursor
                    sSql = "UPDATE tblMachine set  [Status] ='Offline' where ID_NO='" & ID_NO & "'"
                    failIP.Add(IP.ToString)
                End If
                Try
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                Catch ex As Exception

                End Try
                'If (ret <> CType(SyLastError.sleSuss, Int32)) Then
                '    XtraMessageBox.Show(ulf, "<size=10>" & (util.ErrorPrint(ret)) & "</size>", "Failed")
                'End If
            ElseIf DeviceType.ToString.Trim = "F9" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881
                Dim gnCommHandleIndex As Long
                Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                If A_R = "S" Then
                    gnCommHandleIndex = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
                Else
                    gnCommHandleIndex = F9.FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                End If
                If gnCommHandleIndex > 0 Then
                    GridView1.SetRowCellValue(rowHandle, "Status", "Online")
                    Application.DoEvents()
                    Me.Cursor = Cursors.WaitCursor
                    'GridView1.SetRowCellValue(rowHandle, "LastModifiedBy", "Online")
                    Dim UserCount As String = "0"
                    Dim FPCount As String = "0"
                    Dim FaceCount As String = "0"
                    Dim AttLogCount As String = "0"
                    Dim AdminCount As String = "0"

                    Dim vnStatusIndex As Integer
                    Dim vnValue As Integer
                    Dim vnResultCode As Integer
                    vnResultCode = F9.FK_EnableDevice(gnCommHandleIndex, 0)

                    vnStatusIndex = GET_USERS
                    vnResultCode = F9.FK_GetDeviceStatus(gnCommHandleIndex, vnStatusIndex, vnValue)
                    If vnResultCode = RUN_SUCCESS Then
                        UserCount = vnValue
                    End If

                    vnStatusIndex = GET_FPS
                    vnResultCode = F9.FK_GetDeviceStatus(gnCommHandleIndex, vnStatusIndex, vnValue)
                    If vnResultCode = RUN_SUCCESS Then
                        FPCount = vnValue
                    End If

                    vnStatusIndex = GET_FACES
                    vnResultCode = F9.FK_GetDeviceStatus(gnCommHandleIndex, vnStatusIndex, vnValue)
                    If vnResultCode = RUN_SUCCESS Then
                        FaceCount = vnValue
                    End If

                    vnStatusIndex = GET_AGLOGS
                    vnResultCode = F9.FK_GetDeviceStatus(gnCommHandleIndex, vnStatusIndex, vnValue)
                    If vnResultCode = RUN_SUCCESS Then
                        AttLogCount = vnValue
                    End If

                    vnStatusIndex = GET_MANAGERS
                    vnResultCode = F9.FK_GetDeviceStatus(gnCommHandleIndex, vnStatusIndex, vnValue)
                    If vnResultCode = RUN_SUCCESS Then
                        AdminCount = vnValue
                    End If
                    vnResultCode = F9.FK_EnableDevice(gnCommHandleIndex, 1)
                    sSql = "UPDATE tblMachine set  [Status] ='Online',  [UserCount]  = '" & UserCount & "' ,[FPCount]='" & FPCount & "',[FaceCount]='" & FaceCount & "',[AttLogCount]='" & AttLogCount & "',[AdminCount]='" & AdminCount & "' where ID_NO='" & ID_NO & "'"
                    FK_DisConnect(gnCommHandleIndex)
                Else
                    GridView1.SetRowCellValue(rowHandle, "Status", "Offline")
                    Application.DoEvents()
                    Me.Cursor = Cursors.WaitCursor
                    sSql = "UPDATE tblMachine set  [Status] ='Offline' where ID_NO='" & ID_NO & "'"
                End If
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ATF686n" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString
                vpszNetPort = CLng("5005")
                vpszNetPassword = CLng("0")
                vnTimeOut = CLng("5000")
                vnProtocolType = 0
                vnLicense = 7881
                Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
                'If A_R = "S" Then
                '    vnResult = F9.FK_ConnectUSB(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vnLicense)
                'Else
                Dim vnResult As Integer = atf686n.ConnectNet(vpszIPAddress)
                If vnResult > 0 Then
                    GridView1.SetRowCellValue(rowHandle, "Status", "Online")
                    Application.DoEvents()
                End If

                atf686n.ST_EnableDevice(vnResult, 1)
                atf686n.ST_DisConnect(vnResult)

            ElseIf DeviceType.ToString.Trim = "TF-01" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881
                Dim gnCommHandleIndex As Long
                Dim bRet As Boolean
                ' Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                'Dim AxFP_CLOCK1 As AxFP_CLOCKLib.AxFP_CLOCK = New AxFP_CLOCKLib.AxFP_CLOCK

                AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
                bRet = AxFP_CLOCK1.SetIPAddress(vpszIPAddress, vpszNetPort, vpszNetPassword)
                If bRet Then
                    GridView1.SetRowCellValue(rowHandle, "Status", "Online")
                    Application.DoEvents()
                    Me.Cursor = Cursors.WaitCursor
                    'GridView1.SetRowCellValue(rowHandle, "LastModifiedBy", "Online")
                    Dim UserCount As String = "0"
                    Dim FPCount As String = "0"
                    Dim FaceCount As String = "0"
                    Dim AttLogCount As String = "0"
                    Dim AdminCount As String = "0"

                    Dim vnStatusIndex As Integer
                    Dim vnValue As Integer
                    Dim vnResultCode As Integer
                    vnResultCode = AxFP_CLOCK1.EnableDevice(vnMachineNumber, 0)
                    vnStatusIndex = GET_USERS
                    bRet = AxFP_CLOCK1.GetDeviceStatus(vnMachineNumber, vnStatusIndex, vnValue)
                    If bRet Then
                        UserCount = vnValue
                    End If

                    vnStatusIndex = GET_FPS
                    bRet = AxFP_CLOCK1.GetDeviceStatus(vnMachineNumber, vnStatusIndex, vnValue)
                    If bRet Then
                        FPCount = vnValue
                    End If

                    vnStatusIndex = GET_FACES
                    bRet = AxFP_CLOCK1.GetDeviceStatus(vnMachineNumber, vnStatusIndex, vnValue)
                    If bRet Then
                        FaceCount = vnValue
                    End If

                    vnStatusIndex = GET_AGLOGS
                    bRet = AxFP_CLOCK1.GetDeviceStatus(vnMachineNumber, vnStatusIndex, vnValue)
                    If bRet Then
                        AttLogCount = vnValue
                    End If

                    vnStatusIndex = GET_MANAGERS
                    bRet = AxFP_CLOCK1.GetDeviceStatus(vnMachineNumber, vnStatusIndex, vnValue)
                    If bRet Then
                        AdminCount = vnValue
                    End If
                    'vnResultCode = AxFP_CLOCK1.GetDeviceStatus(vnMachineNumber, 1)
                    sSql = "UPDATE tblMachine set  [Status] ='Online',  [UserCount]  = '" & UserCount & "' ,[FPCount]='" & FPCount & "',[FaceCount]='" & FaceCount & "',[AttLogCount]='" & AttLogCount & "',[AdminCount]='" & AdminCount & "' where ID_NO='" & ID_NO & "'"
                    AxFP_CLOCK1.EnableDevice(vnMachineNumber, 1)
                    AxFP_CLOCK1.CloseCommPort()
                Else
                    GridView1.SetRowCellValue(rowHandle, "Status", "Offline")
                    Application.DoEvents()
                    Me.Cursor = Cursors.WaitCursor
                    sSql = "UPDATE tblMachine set  [Status] ='Offline' where ID_NO='" & ID_NO & "'"
                End If
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK Controller" Then
                Dim ConnectT = "protocol=TCP,ipaddress=" & IP.ToString.Trim & ",port=4370,timeout=2000,passwd="
                Dim ret As Integer = 0
                Dim UserCount As String = "0"
                Dim AttLogCount As String = "0"
                Dim options As String = ""
                Dim tablenamestr As String = ""
                Dim count(19) As String
                Dim devdatfilter As String = ""
                If IntPtr.Zero = h Then
                    h = ZKController.Connect(ConnectT)
                End If
                If h <> IntPtr.Zero Then
                    UserCount = ZKController.GetDeviceDataCount(h, "user", devdatfilter, options)
                    If UserCount > 0 Then
                        UserCount = UserCount
                    End If
                    AttLogCount = ZKController.GetDeviceDataCount(h, "transaction", devdatfilter, options)
                    If AttLogCount > 0 Then
                        AttLogCount = AttLogCount
                    End If
                    sSql = "UPDATE tblMachine set  [Status] ='Online',  [UserCount]  = '" & UserCount & "' ,[AttLogCount]='" & AttLogCount & "' where ID_NO='" & ID_NO & "'"
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                Else
                    sSql = "UPDATE tblMachine set  [Status] ='Offline' where ID_NO='" & ID_NO & "'"
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                End If

            End If
            Common.LogPost("Device Status Check; Device ID: " & ID_NO)
        Next
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        Me.Cursor = Cursors.WaitCursor
        Common.loadDevice()
        GridControl1.DataSource = Common.MachineNonAdmin
        Application.DoEvents()
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub GridView1_RowCellStyle(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles GridView1.RowCellStyle
        Dim view As GridView = TryCast(sender, GridView)
        Dim _mark As Boolean = CBool(view.GetRowCellValue(e.RowHandle, "Online"))
        If e.Column.FieldName = "Status" Then
            If IsNothing(e.CellValue) = False Then
                If e.CellValue.ToString = "Online" Then
                    e.Appearance.ForeColor = Color.Green 'If(_mark, Color.LightGreen, Color.LightSalmon)
                    e.Appearance.TextOptions.HAlignment = If(_mark, HorzAlignment.Center, HorzAlignment.Near)
                ElseIf e.CellValue.ToString = "Offline" Then
                    e.Appearance.ForeColor = Color.Red 'If(_mark, Color.LightGreen, Color.LightSalmon)
                    e.Appearance.TextOptions.HAlignment = If(_mark, HorzAlignment.Center, HorzAlignment.Near)
                End If
            End If
        End If
        If e.Column.FieldName = "LastDownloaded" Then
            e.Appearance.ForeColor = Color.Blue 'If(_mark, Color.LightGreen, Color.LightSalmon)
            e.Appearance.TextOptions.HAlignment = If(_mark, HorzAlignment.Center, HorzAlignment.Near)
        End If
    End Sub
    Private Sub btnDetails_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles btnDetails.ButtonClick
        MsgBox("btnDetails_ButtonClick")
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim CellId As String = row("ID_No").ToString.Trim
        MsgBox(CellId)
    End Sub

    Private Sub BarButtonItem7_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem7.ItemClick
        Try
            DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
            Dim ret As Integer = 0, i As Integer = 0, j As Integer = 0, k As Integer = 0
            Dim buffer((64 * 1024) - 1) As Byte
            Dim str As String = ""
            Dim filed() As String = Nothing
            Dim tmp() As String = Nothing
            Dim udp As String = "UDP"
            Dim adr As String = "255.255.255.255"
            Dim DevieID As String
            Dim DeviceIP As String
            Dim SerialNo As String
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            ret = ZKController.SearchDevice(udp, adr, buffer(0))
            str = Encoding.Default.GetString(buffer)
            'str = str.Replace(vbNewLine, vbTab)

            'Try
            '    'insert in text file
            '    Dim datafile As String = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
            '    Dim fs As FileStream = New FileStream(datafile, FileMode.OpenOrCreate, FileAccess.Write)
            '    Dim sw As StreamWriter = New StreamWriter(fs)
            '    'find the end of the underlying filestream
            '    sw.BaseStream.Seek(0, SeekOrigin.End)
            '    sw.WriteLine(str)
            '    sw.Flush()
            '    sw.Close()
            '    'end insert in text file
            'Catch ex As Exception

            'End Try

            '            str = "MAC=00:17:61:CA:9E:A5,IP=192.168.1.201,NetMask=255.255.255.0,GATEIPAddress=192.168.1.254,SN=AJNV201160172,Device=C3-200,Ver=AC Ver 4.1.9-4882-01 Oct 13 2016
            'MAC=00:17:61:CA:9E:72,IP=172.17.109.2,NetMask=255.255.255.0,GATEIPAddress=172.17.109.254,SN=AJNV201160121,Device=C3-200,Ver=AC Ver 4.1.9-4882-01 Oct 13 2016
            'MAC=00:17:61:CA:9E:7E,IP=172.17.109.1,NetMask=255.255.255.0,GATEIPAddress=172.17.109.254,SN=AJNV201160133,Device=C3-200,Ver=AC Ver 4.1.9-4882-01 Oct 13 2016
            'MAC=00:17:61:CA:9E:82,IP=172.17.109.8,NetMask=255.255.255.0,GATEIPAddress=172.17.109.254,SN=AJNV201160137,Device=C3-200,Ver=AC Ver 4.1.9-4882-01 Oct 13 2016
            'MAC=00:17:61:CA:9E:8D,IP=172.17.109.4,NetMask=255.255.255.0,GATEIPAddress=172.17.109.254,SN=AJNV201160148,Device=C3-200,Ver=AC Ver 4.1.9-4882-01 Oct 13 2016
            'MAC=00:17:61:CA:9E:B3,IP=172.17.109.7,NetMask=255.255.255.0,GATEIPAddress=172.17.109.254,SN=AJNV201160186,Device=C3-200,Ver=AC Ver 4.1.9-4882-01 Oct 13 2016
            'MAC=00:17:61:CA:9E:77,IP=172.17.109.3,NetMask=255.255.255.0,GATEIPAddress=172.17.109.254,SN=AJNV201160126,Device=C3-200,Ver=AC Ver 4.1.9-4882-01 Oct 13 2016
            'MAC=00:17:61:CA:9E:81,IP=172.17.109.5,NetMask=255.255.255.0,GATEIPAddress=172.17.109.254,SN=AJNV201160136,Device=C3-200,Ver=AC Ver 4.1.9-4882-01 Oct 13 2016
            'MAC=00:17:61:CA:9E:87,IP=172.17.109.6,NetMask=255.255.255.0,GATEIPAddress=172.17.109.254,SN=AJNV201160142,Device=C3-200,Ver=AC Ver 4.1.9-4882-01 Oct 13 2016

            '"
            tmp = str.Split(vbLf)
            Try
                For m As Integer = 0 To tmp.Length - 1
                    If tmp(m).Trim(vbNullChar).Trim(vbLf) = "" Then
                        Continue For
                    End If
                    Dim TZArrRowData() As String = tmp(m).Split(",")
                    DeviceIP = TZArrRowData(1).Trim(vbNullChar).Trim(vbLf).Substring(3, TZArrRowData(1).Trim.Length - 3)
                    SerialNo = TZArrRowData(4).Trim(vbNullChar).Trim(vbLf).Substring(3, TZArrRowData(4).Trim.Length - 3)

                    Dim cquery As String = "select max(ISNULL(ID_NO,0))+1 from tblMachine"
                    ds = New DataSet()
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(cquery, Common.con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(cquery, Common.con)
                        adap.Fill(ds)
                    End If
                    If ds.Tables(0).Rows(0)(0).ToString.Trim = "" Or IsDBNull(ds.Tables(0).Rows(0)(0).ToString.Trim) Then
                        DevieID = "1"
                    Else
                        DevieID = ds.Tables(0).Rows(0)(0).ToString.Trim
                    End If
                    cquery = "select * from tblMachine where MAC_ADDRESS='" & SerialNo & "'"
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(cquery, Common.con1)
                        adapA.Fill(ds)
                    Else
                        ds = New DataSet()
                        adap = New SqlDataAdapter(cquery, Common.con)
                        adap.Fill(ds)
                    End If
                    If ds.Tables(0).Rows.Count = 0 Then

                        Dim sSql As String = "INSERT INTO tblMachine (ID_NO, A_R, IN_OUT, DeviceType,LOCATION, branch,MAC_ADDRESS, LastModifiedBy, LastModifiedDate, Purpose, commkey, LEDIP ) VALUES('" & Convert.ToInt16(DevieID) & "', 'T', 'I', 'ZK Controller','" & DeviceIP & "', '', '" & SerialNo & "', 'admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','A',0,'')"
                        Common.LogPost("Device Insert; Device ID: " & DevieID)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            'MsgBox(sSql)
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Close()
                            End If
                        End If
                    Else
                        Continue For
                    End If

                Next
            Catch ex As Exception

            End Try




            SplashScreenManager.CloseForm(False)
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
            Common.loadDevice()
            GridControl1.DataSource = Common.MachineNonAdmin
            RepositoryItemLookUpEdit1.DataSource = Common.LocationNonAdmin

        Catch ex As Exception
            SplashScreenManager.CloseForm(False)
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
        End Try
    End Sub

    Private Sub btnDetails_Click(sender As System.Object, e As System.EventArgs) Handles btnDetails.Click
        MsgBox("btnDetails_Click")
    End Sub
End Class
