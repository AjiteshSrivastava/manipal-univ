﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base
Imports iAS.HSeriesSampleCSharp
Imports System.Runtime.InteropServices
Imports CMITech.UMXClient

Public Class XtraEmployeeExperiedValidity
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand

    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView2, New Font("Tahoma", 10))
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraEmployeeCopyToDevice_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LabelControl1.Text = "User's validity expiring today " & Now.ToString("dd MMMM yyyy")

        Dim EmpValidity As New DataTable("TBLEmployee")
        Dim gridselet As String ' = "select * from TBLEmployee order by PRESENTCARDNO"
        If Common.servername = "Access" Then
            gridselet = "select * from TBLEmployee where Format(ValidityEndDate,'yyyy-MM-dd HH:mm:ss')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' order by PRESENTCARDNO"
            Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
            dataAdapter.Fill(EmpValidity)
        Else
            gridselet = "select * from TBLEmployee where ValidityEndDate='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' order by PRESENTCARDNO"
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
            dataAdapter.Fill(EmpValidity)
        End If
        If EmpValidity.Rows.Count = 0 Then
            Me.Close()
        End If

        GridControl2.DataSource = EmpValidity
        GridView2.ClearSelection()
    End Sub
    Private Sub GridView2_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView2.CustomColumnDisplayText
        Try
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "FingerNumber" Then
                If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim >= 0 And _
                    view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim < 10 Then
                    e.DisplayText = "FP-" & view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim + 1
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 12 Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 50 Then
                    e.DisplayText = "FACE"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 11 Then
                    e.DisplayText = "CARD"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 10 Then
                    e.DisplayText = "PASSWORD"
                End If
                e.DisplayText = (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim \ 60).ToString("00") & ":" & (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim Mod 60).ToString("00")
            End If
        Catch
        End Try
        Try
            Dim adapA As OleDbDataAdapter
            Dim adap As SqlDataAdapter
            Dim ds As DataSet
            Dim sSql As String
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "UserName" Then
                sSql = "SELECT EMPNAME from TblEmployee where PRESENTCARDNO = '" & view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EnrollNumber").ToString().Trim & "'"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    e.DisplayText = ds.Tables(0).Rows(0).Item(0).ToString
                Else
                    e.DisplayText = ""
                End If
            End If

            If e.Column.Caption = "Device Type" Then
                If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Template").ToString().Trim = "" Then
                    e.DisplayText = "Bio"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Template_Tw").ToString().Trim = "" Then
                    e.DisplayText = "ZK"
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
End Class