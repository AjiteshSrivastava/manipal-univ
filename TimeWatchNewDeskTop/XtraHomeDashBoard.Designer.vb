﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraHomeDashBoard
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TileItemElement1 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement2 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraHomeDashBoard))
        Dim Series1 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel1 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView1 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim Series2 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel2 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView2 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim Series3 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel3 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView3 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim Series4 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel4 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView4 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim Series5 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel5 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView5 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim Series6 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim PieSeriesLabel6 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView6 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Me.TileItem1 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem2 = New DevExpress.XtraEditors.TileItem()
        Me.TimerRefreshDashBoard = New System.Windows.Forms.Timer(Me.components)
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelTrial = New DevExpress.XtraEditors.LabelControl()
        Me.LabelActivate = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit()
        Me.SidePanelPie = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelPie3 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelPie32 = New DevExpress.XtraEditors.SidePanel()
        Me.ChartControlWO = New DevExpress.XtraCharts.ChartControl()
        Me.SidePanelWO = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelPie31 = New DevExpress.XtraEditors.SidePanel()
        Me.ChartControlLeave = New DevExpress.XtraCharts.ChartControl()
        Me.SidePanelLeave = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelPie2 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelPie22 = New DevExpress.XtraEditors.SidePanel()
        Me.ChartControlLate = New DevExpress.XtraCharts.ChartControl()
        Me.SidePanelLate = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelPie21 = New DevExpress.XtraEditors.SidePanel()
        Me.ChartControlAbs = New DevExpress.XtraCharts.ChartControl()
        Me.SidePanelAbs = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelPie1 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelPie12 = New DevExpress.XtraEditors.SidePanel()
        Me.ChartControlPresent = New DevExpress.XtraCharts.ChartControl()
        Me.SidePanelPresent = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelPie11 = New DevExpress.XtraEditors.SidePanel()
        Me.ChartControlTotal = New DevExpress.XtraCharts.ChartControl()
        Me.SidePanelTotal = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelShortCut = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelImg3 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelImg32 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelImg33 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelImg2 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelImg22 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelImg21 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelImg1 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelImg11 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanel1.SuspendLayout()
        Me.SidePanel2.SuspendLayout()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanelPie.SuspendLayout()
        Me.SidePanelPie3.SuspendLayout()
        Me.SidePanelPie32.SuspendLayout()
        CType(Me.ChartControlWO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanelPie31.SuspendLayout()
        CType(Me.ChartControlLeave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanelPie2.SuspendLayout()
        Me.SidePanelPie22.SuspendLayout()
        CType(Me.ChartControlLate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanelPie21.SuspendLayout()
        CType(Me.ChartControlAbs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanelPie1.SuspendLayout()
        Me.SidePanelPie12.SuspendLayout()
        CType(Me.ChartControlPresent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanelPie11.SuspendLayout()
        CType(Me.ChartControlTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanelShortCut.SuspendLayout()
        Me.SidePanelImg3.SuspendLayout()
        Me.SidePanelImg2.SuspendLayout()
        Me.SidePanelImg1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TileItem1
        '
        TileItemElement1.Text = "TileItem1"
        Me.TileItem1.Elements.Add(TileItemElement1)
        Me.TileItem1.Id = 4
        Me.TileItem1.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem1.Name = "TileItem1"
        '
        'TileItem2
        '
        TileItemElement2.Text = "TileItem2"
        Me.TileItem2.Elements.Add(TileItemElement2)
        Me.TileItem2.Id = 5
        Me.TileItem2.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem2.Name = "TileItem2"
        '
        'TimerRefreshDashBoard
        '
        Me.TimerRefreshDashBoard.Enabled = True
        Me.TimerRefreshDashBoard.Interval = 60000
        '
        'SidePanel1
        '
        Me.SidePanel1.AllowResize = False
        Me.SidePanel1.Appearance.BackColor = System.Drawing.Color.White
        Me.SidePanel1.Appearance.BorderColor = System.Drawing.Color.White
        Me.SidePanel1.Appearance.Options.UseBackColor = True
        Me.SidePanel1.Appearance.Options.UseBorderColor = True
        Me.SidePanel1.Controls.Add(Me.LabelTrial)
        Me.SidePanel1.Controls.Add(Me.LabelActivate)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel1.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1432, 40)
        Me.SidePanel1.TabIndex = 7
        Me.SidePanel1.Text = "SidePanel1"
        '
        'LabelTrial
        '
        Me.LabelTrial.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelTrial.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelTrial.Appearance.Options.UseFont = True
        Me.LabelTrial.Appearance.Options.UseForeColor = True
        Me.LabelTrial.Dock = System.Windows.Forms.DockStyle.Right
        Me.LabelTrial.Location = New System.Drawing.Point(1268, 0)
        Me.LabelTrial.Name = "LabelTrial"
        Me.LabelTrial.Size = New System.Drawing.Size(98, 19)
        Me.LabelTrial.TabIndex = 12
        Me.LabelTrial.Text = "LabelControl1"
        '
        'LabelActivate
        '
        Me.LabelActivate.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Underline)
        Me.LabelActivate.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelActivate.Appearance.Options.UseFont = True
        Me.LabelActivate.Appearance.Options.UseForeColor = True
        Me.LabelActivate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LabelActivate.Dock = System.Windows.Forms.DockStyle.Right
        Me.LabelActivate.LineColor = System.Drawing.Color.Red
        Me.LabelActivate.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom
        Me.LabelActivate.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal
        Me.LabelActivate.LineVisible = True
        Me.LabelActivate.Location = New System.Drawing.Point(1366, 0)
        Me.LabelActivate.Name = "LabelActivate"
        Me.LabelActivate.Size = New System.Drawing.Size(66, 22)
        Me.LabelActivate.TabIndex = 11
        Me.LabelActivate.Text = " Activate "
        '
        'SidePanel2
        '
        Me.SidePanel2.AllowResize = False
        Me.SidePanel2.Appearance.BackColor = System.Drawing.Color.White
        Me.SidePanel2.Appearance.BorderColor = System.Drawing.Color.White
        Me.SidePanel2.Appearance.Options.UseBackColor = True
        Me.SidePanel2.Appearance.Options.UseBorderColor = True
        Me.SidePanel2.Controls.Add(Me.PictureEdit1)
        Me.SidePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel2.Location = New System.Drawing.Point(0, 760)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(1432, 40)
        Me.SidePanel2.TabIndex = 0
        Me.SidePanel2.Text = "SidePanel2"
        '
        'PictureEdit1
        '
        Me.PictureEdit1.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureEdit1.EditValue = CType(resources.GetObject("PictureEdit1.EditValue"), Object)
        Me.PictureEdit1.Location = New System.Drawing.Point(0, 1)
        Me.PictureEdit1.Name = "PictureEdit1"
        Me.PictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.PictureEdit1.Size = New System.Drawing.Size(1432, 39)
        Me.PictureEdit1.TabIndex = 0
        '
        'SidePanelPie
        '
        Me.SidePanelPie.AllowResize = False
        Me.SidePanelPie.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelPie.Appearance.Options.UseBorderColor = True
        Me.SidePanelPie.Controls.Add(Me.SidePanelPie3)
        Me.SidePanelPie.Controls.Add(Me.SidePanelPie2)
        Me.SidePanelPie.Controls.Add(Me.SidePanelPie1)
        Me.SidePanelPie.Dock = System.Windows.Forms.DockStyle.Left
        Me.SidePanelPie.Location = New System.Drawing.Point(0, 40)
        Me.SidePanelPie.Name = "SidePanelPie"
        Me.SidePanelPie.Size = New System.Drawing.Size(566, 720)
        Me.SidePanelPie.TabIndex = 8
        Me.SidePanelPie.Text = "SidePanel1"
        '
        'SidePanelPie3
        '
        Me.SidePanelPie3.AllowResize = False
        Me.SidePanelPie3.Appearance.BackColor = System.Drawing.Color.White
        Me.SidePanelPie3.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelPie3.Appearance.Options.UseBackColor = True
        Me.SidePanelPie3.Appearance.Options.UseBorderColor = True
        Me.SidePanelPie3.Controls.Add(Me.SidePanelPie32)
        Me.SidePanelPie3.Controls.Add(Me.SidePanelPie31)
        Me.SidePanelPie3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanelPie3.Location = New System.Drawing.Point(0, 346)
        Me.SidePanelPie3.Name = "SidePanelPie3"
        Me.SidePanelPie3.Size = New System.Drawing.Size(565, 374)
        Me.SidePanelPie3.TabIndex = 3
        Me.SidePanelPie3.Text = "SidePanel1"
        '
        'SidePanelPie32
        '
        Me.SidePanelPie32.AllowResize = False
        Me.SidePanelPie32.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelPie32.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelPie32.Appearance.Options.UseBackColor = True
        Me.SidePanelPie32.Appearance.Options.UseBorderColor = True
        Me.SidePanelPie32.Controls.Add(Me.ChartControlWO)
        Me.SidePanelPie32.Controls.Add(Me.SidePanelWO)
        Me.SidePanelPie32.Cursor = System.Windows.Forms.Cursors.Default
        Me.SidePanelPie32.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanelPie32.Location = New System.Drawing.Point(304, 0)
        Me.SidePanelPie32.Name = "SidePanelPie32"
        Me.SidePanelPie32.Size = New System.Drawing.Size(261, 374)
        Me.SidePanelPie32.TabIndex = 2
        '
        'ChartControlWO
        '
        Me.ChartControlWO.BorderOptions.Color = System.Drawing.Color.Transparent
        Me.ChartControlWO.DataBindings = Nothing
        Me.ChartControlWO.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ChartControlWO.Legend.Name = "Default Legend"
        Me.ChartControlWO.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[False]
        Me.ChartControlWO.Location = New System.Drawing.Point(0, 0)
        Me.ChartControlWO.LookAndFeel.SkinName = "iMaginary"
        Me.ChartControlWO.LookAndFeel.UseDefaultLookAndFeel = False
        Me.ChartControlWO.Name = "ChartControlWO"
        PieSeriesLabel1.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.Inside
        PieSeriesLabel1.TextPattern = "{A}"
        Series1.Label = PieSeriesLabel1
        Series1.LegendName = "Default Legend"
        Series1.Name = "Series1"
        Series1.View = PieSeriesView1
        Me.ChartControlWO.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series1}
        Me.ChartControlWO.Size = New System.Drawing.Size(261, 340)
        Me.ChartControlWO.TabIndex = 6
        '
        'SidePanelWO
        '
        Me.SidePanelWO.AllowResize = False
        Me.SidePanelWO.Appearance.BackColor = System.Drawing.Color.White
        Me.SidePanelWO.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelWO.Appearance.Options.UseBackColor = True
        Me.SidePanelWO.Appearance.Options.UseBorderColor = True
        Me.SidePanelWO.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanelWO.Location = New System.Drawing.Point(0, 340)
        Me.SidePanelWO.Name = "SidePanelWO"
        Me.SidePanelWO.Size = New System.Drawing.Size(261, 34)
        Me.SidePanelWO.TabIndex = 1
        '
        'SidePanelPie31
        '
        Me.SidePanelPie31.AllowResize = False
        Me.SidePanelPie31.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelPie31.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelPie31.Appearance.Options.UseBackColor = True
        Me.SidePanelPie31.Appearance.Options.UseBorderColor = True
        Me.SidePanelPie31.Controls.Add(Me.ChartControlLeave)
        Me.SidePanelPie31.Controls.Add(Me.SidePanelLeave)
        Me.SidePanelPie31.Cursor = System.Windows.Forms.Cursors.Default
        Me.SidePanelPie31.Dock = System.Windows.Forms.DockStyle.Left
        Me.SidePanelPie31.Location = New System.Drawing.Point(0, 0)
        Me.SidePanelPie31.Name = "SidePanelPie31"
        Me.SidePanelPie31.Size = New System.Drawing.Size(304, 374)
        Me.SidePanelPie31.TabIndex = 0
        '
        'ChartControlLeave
        '
        Me.ChartControlLeave.BorderOptions.Color = System.Drawing.Color.Transparent
        Me.ChartControlLeave.DataBindings = Nothing
        Me.ChartControlLeave.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ChartControlLeave.Legend.Name = "Default Legend"
        Me.ChartControlLeave.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[False]
        Me.ChartControlLeave.Location = New System.Drawing.Point(0, 0)
        Me.ChartControlLeave.LookAndFeel.SkinName = "iMaginary"
        Me.ChartControlLeave.LookAndFeel.UseDefaultLookAndFeel = False
        Me.ChartControlLeave.Name = "ChartControlLeave"
        PieSeriesLabel2.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.Inside
        PieSeriesLabel2.TextPattern = "{A}"
        Series2.Label = PieSeriesLabel2
        Series2.LegendName = "Default Legend"
        Series2.Name = "Series1"
        Series2.View = PieSeriesView2
        Me.ChartControlLeave.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series2}
        Me.ChartControlLeave.Size = New System.Drawing.Size(303, 340)
        Me.ChartControlLeave.TabIndex = 6
        '
        'SidePanelLeave
        '
        Me.SidePanelLeave.AllowResize = False
        Me.SidePanelLeave.Appearance.BackColor = System.Drawing.Color.White
        Me.SidePanelLeave.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelLeave.Appearance.Options.UseBackColor = True
        Me.SidePanelLeave.Appearance.Options.UseBorderColor = True
        Me.SidePanelLeave.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanelLeave.Location = New System.Drawing.Point(0, 340)
        Me.SidePanelLeave.Name = "SidePanelLeave"
        Me.SidePanelLeave.Size = New System.Drawing.Size(303, 34)
        Me.SidePanelLeave.TabIndex = 1
        '
        'SidePanelPie2
        '
        Me.SidePanelPie2.AllowResize = False
        Me.SidePanelPie2.Appearance.BackColor = System.Drawing.Color.White
        Me.SidePanelPie2.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelPie2.Appearance.Options.UseBackColor = True
        Me.SidePanelPie2.Appearance.Options.UseBorderColor = True
        Me.SidePanelPie2.Controls.Add(Me.SidePanelPie22)
        Me.SidePanelPie2.Controls.Add(Me.SidePanelPie21)
        Me.SidePanelPie2.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanelPie2.Location = New System.Drawing.Point(0, 173)
        Me.SidePanelPie2.Name = "SidePanelPie2"
        Me.SidePanelPie2.Size = New System.Drawing.Size(565, 173)
        Me.SidePanelPie2.TabIndex = 2
        Me.SidePanelPie2.Text = "SidePanel1"
        '
        'SidePanelPie22
        '
        Me.SidePanelPie22.AllowResize = False
        Me.SidePanelPie22.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelPie22.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelPie22.Appearance.Options.UseBackColor = True
        Me.SidePanelPie22.Appearance.Options.UseBorderColor = True
        Me.SidePanelPie22.Controls.Add(Me.ChartControlLate)
        Me.SidePanelPie22.Controls.Add(Me.SidePanelLate)
        Me.SidePanelPie22.Cursor = System.Windows.Forms.Cursors.Default
        Me.SidePanelPie22.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanelPie22.Location = New System.Drawing.Point(304, 0)
        Me.SidePanelPie22.Name = "SidePanelPie22"
        Me.SidePanelPie22.Size = New System.Drawing.Size(261, 172)
        Me.SidePanelPie22.TabIndex = 2
        '
        'ChartControlLate
        '
        Me.ChartControlLate.BorderOptions.Color = System.Drawing.Color.Transparent
        Me.ChartControlLate.DataBindings = Nothing
        Me.ChartControlLate.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ChartControlLate.Legend.Name = "Default Legend"
        Me.ChartControlLate.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[False]
        Me.ChartControlLate.Location = New System.Drawing.Point(0, 0)
        Me.ChartControlLate.LookAndFeel.SkinName = "iMaginary"
        Me.ChartControlLate.LookAndFeel.UseDefaultLookAndFeel = False
        Me.ChartControlLate.Name = "ChartControlLate"
        PieSeriesLabel3.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.Inside
        PieSeriesLabel3.TextPattern = "{A}"
        Series3.Label = PieSeriesLabel3
        Series3.LegendName = "Default Legend"
        Series3.Name = "Series1"
        Series3.View = PieSeriesView3
        Me.ChartControlLate.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series3}
        Me.ChartControlLate.Size = New System.Drawing.Size(261, 138)
        Me.ChartControlLate.TabIndex = 6
        '
        'SidePanelLate
        '
        Me.SidePanelLate.AllowResize = False
        Me.SidePanelLate.Appearance.BackColor = System.Drawing.Color.White
        Me.SidePanelLate.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelLate.Appearance.Options.UseBackColor = True
        Me.SidePanelLate.Appearance.Options.UseBorderColor = True
        Me.SidePanelLate.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanelLate.Location = New System.Drawing.Point(0, 138)
        Me.SidePanelLate.Name = "SidePanelLate"
        Me.SidePanelLate.Size = New System.Drawing.Size(261, 34)
        Me.SidePanelLate.TabIndex = 1
        '
        'SidePanelPie21
        '
        Me.SidePanelPie21.AllowResize = False
        Me.SidePanelPie21.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelPie21.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelPie21.Appearance.Options.UseBackColor = True
        Me.SidePanelPie21.Appearance.Options.UseBorderColor = True
        Me.SidePanelPie21.Controls.Add(Me.ChartControlAbs)
        Me.SidePanelPie21.Controls.Add(Me.SidePanelAbs)
        Me.SidePanelPie21.Cursor = System.Windows.Forms.Cursors.Default
        Me.SidePanelPie21.Dock = System.Windows.Forms.DockStyle.Left
        Me.SidePanelPie21.Location = New System.Drawing.Point(0, 0)
        Me.SidePanelPie21.Name = "SidePanelPie21"
        Me.SidePanelPie21.Size = New System.Drawing.Size(304, 172)
        Me.SidePanelPie21.TabIndex = 0
        '
        'ChartControlAbs
        '
        Me.ChartControlAbs.BorderOptions.Color = System.Drawing.Color.Transparent
        Me.ChartControlAbs.DataBindings = Nothing
        Me.ChartControlAbs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ChartControlAbs.Legend.Name = "Default Legend"
        Me.ChartControlAbs.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[False]
        Me.ChartControlAbs.Location = New System.Drawing.Point(0, 0)
        Me.ChartControlAbs.LookAndFeel.SkinName = "iMaginary"
        Me.ChartControlAbs.LookAndFeel.UseDefaultLookAndFeel = False
        Me.ChartControlAbs.Name = "ChartControlAbs"
        PieSeriesLabel4.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.Inside
        PieSeriesLabel4.TextPattern = "{A}"
        Series4.Label = PieSeriesLabel4
        Series4.LegendName = "Default Legend"
        Series4.Name = "Series1"
        Series4.View = PieSeriesView4
        Me.ChartControlAbs.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series4}
        Me.ChartControlAbs.Size = New System.Drawing.Size(303, 138)
        Me.ChartControlAbs.TabIndex = 6
        '
        'SidePanelAbs
        '
        Me.SidePanelAbs.AllowResize = False
        Me.SidePanelAbs.Appearance.BackColor = System.Drawing.Color.White
        Me.SidePanelAbs.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelAbs.Appearance.Options.UseBackColor = True
        Me.SidePanelAbs.Appearance.Options.UseBorderColor = True
        Me.SidePanelAbs.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanelAbs.Location = New System.Drawing.Point(0, 138)
        Me.SidePanelAbs.Name = "SidePanelAbs"
        Me.SidePanelAbs.Size = New System.Drawing.Size(303, 34)
        Me.SidePanelAbs.TabIndex = 1
        '
        'SidePanelPie1
        '
        Me.SidePanelPie1.AllowResize = False
        Me.SidePanelPie1.Appearance.BackColor = System.Drawing.Color.White
        Me.SidePanelPie1.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelPie1.Appearance.Options.UseBackColor = True
        Me.SidePanelPie1.Appearance.Options.UseBorderColor = True
        Me.SidePanelPie1.Controls.Add(Me.SidePanelPie12)
        Me.SidePanelPie1.Controls.Add(Me.SidePanelPie11)
        Me.SidePanelPie1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanelPie1.Location = New System.Drawing.Point(0, 0)
        Me.SidePanelPie1.Name = "SidePanelPie1"
        Me.SidePanelPie1.Size = New System.Drawing.Size(565, 173)
        Me.SidePanelPie1.TabIndex = 1
        Me.SidePanelPie1.Text = "SidePanel1"
        '
        'SidePanelPie12
        '
        Me.SidePanelPie12.AllowResize = False
        Me.SidePanelPie12.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelPie12.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelPie12.Appearance.Options.UseBackColor = True
        Me.SidePanelPie12.Appearance.Options.UseBorderColor = True
        Me.SidePanelPie12.Controls.Add(Me.ChartControlPresent)
        Me.SidePanelPie12.Controls.Add(Me.SidePanelPresent)
        Me.SidePanelPie12.Cursor = System.Windows.Forms.Cursors.Default
        Me.SidePanelPie12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanelPie12.Location = New System.Drawing.Point(304, 0)
        Me.SidePanelPie12.Name = "SidePanelPie12"
        Me.SidePanelPie12.Size = New System.Drawing.Size(261, 172)
        Me.SidePanelPie12.TabIndex = 1
        '
        'ChartControlPresent
        '
        Me.ChartControlPresent.BorderOptions.Color = System.Drawing.Color.Transparent
        Me.ChartControlPresent.DataBindings = Nothing
        Me.ChartControlPresent.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ChartControlPresent.Legend.Name = "Default Legend"
        Me.ChartControlPresent.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[False]
        Me.ChartControlPresent.Location = New System.Drawing.Point(0, 0)
        Me.ChartControlPresent.LookAndFeel.SkinName = "iMaginary"
        Me.ChartControlPresent.LookAndFeel.UseDefaultLookAndFeel = False
        Me.ChartControlPresent.Name = "ChartControlPresent"
        PieSeriesLabel5.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.Inside
        PieSeriesLabel5.TextPattern = "{A}"
        Series5.Label = PieSeriesLabel5
        Series5.LegendName = "Default Legend"
        Series5.Name = "Series1"
        Series5.View = PieSeriesView5
        Me.ChartControlPresent.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series5}
        Me.ChartControlPresent.Size = New System.Drawing.Size(261, 138)
        Me.ChartControlPresent.TabIndex = 5
        '
        'SidePanelPresent
        '
        Me.SidePanelPresent.AllowResize = False
        Me.SidePanelPresent.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelPresent.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelPresent.Appearance.Options.UseBackColor = True
        Me.SidePanelPresent.Appearance.Options.UseBorderColor = True
        Me.SidePanelPresent.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanelPresent.Location = New System.Drawing.Point(0, 138)
        Me.SidePanelPresent.Name = "SidePanelPresent"
        Me.SidePanelPresent.Size = New System.Drawing.Size(261, 34)
        Me.SidePanelPresent.TabIndex = 1
        '
        'SidePanelPie11
        '
        Me.SidePanelPie11.AllowResize = False
        Me.SidePanelPie11.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelPie11.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelPie11.Appearance.Options.UseBackColor = True
        Me.SidePanelPie11.Appearance.Options.UseBorderColor = True
        Me.SidePanelPie11.Controls.Add(Me.ChartControlTotal)
        Me.SidePanelPie11.Controls.Add(Me.SidePanelTotal)
        Me.SidePanelPie11.Cursor = System.Windows.Forms.Cursors.Default
        Me.SidePanelPie11.Dock = System.Windows.Forms.DockStyle.Left
        Me.SidePanelPie11.Location = New System.Drawing.Point(0, 0)
        Me.SidePanelPie11.Name = "SidePanelPie11"
        Me.SidePanelPie11.Size = New System.Drawing.Size(304, 172)
        Me.SidePanelPie11.TabIndex = 0
        '
        'ChartControlTotal
        '
        Me.ChartControlTotal.BorderOptions.Color = System.Drawing.Color.Transparent
        Me.ChartControlTotal.DataBindings = Nothing
        Me.ChartControlTotal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ChartControlTotal.Legend.Name = "Default Legend"
        Me.ChartControlTotal.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[False]
        Me.ChartControlTotal.Location = New System.Drawing.Point(0, 0)
        Me.ChartControlTotal.LookAndFeel.SkinName = "iMaginary"
        Me.ChartControlTotal.LookAndFeel.UseDefaultLookAndFeel = False
        Me.ChartControlTotal.Name = "ChartControlTotal"
        PieSeriesLabel6.BackColor = System.Drawing.Color.Transparent
        PieSeriesLabel6.Border.Color = System.Drawing.Color.Transparent
        PieSeriesLabel6.Border.Visibility = DevExpress.Utils.DefaultBoolean.[False]
        PieSeriesLabel6.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.Inside
        PieSeriesLabel6.TextColor = System.Drawing.Color.Transparent
        PieSeriesLabel6.TextPattern = "{A}"
        Series6.Label = PieSeriesLabel6
        Series6.LegendName = "Default Legend"
        Series6.Name = "Series1"
        PieSeriesView6.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid
        Series6.View = PieSeriesView6
        Me.ChartControlTotal.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series6}
        Me.ChartControlTotal.Size = New System.Drawing.Size(303, 138)
        Me.ChartControlTotal.TabIndex = 4
        '
        'SidePanelTotal
        '
        Me.SidePanelTotal.AllowResize = False
        Me.SidePanelTotal.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelTotal.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelTotal.Appearance.Options.UseBackColor = True
        Me.SidePanelTotal.Appearance.Options.UseBorderColor = True
        Me.SidePanelTotal.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanelTotal.Location = New System.Drawing.Point(0, 138)
        Me.SidePanelTotal.Name = "SidePanelTotal"
        Me.SidePanelTotal.Size = New System.Drawing.Size(303, 34)
        Me.SidePanelTotal.TabIndex = 0
        '
        'SidePanelShortCut
        '
        Me.SidePanelShortCut.AllowResize = False
        Me.SidePanelShortCut.Appearance.BackColor = System.Drawing.Color.White
        Me.SidePanelShortCut.Appearance.Options.UseBackColor = True
        Me.SidePanelShortCut.BackgroundImage = CType(resources.GetObject("SidePanelShortCut.BackgroundImage"), System.Drawing.Image)
        Me.SidePanelShortCut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.SidePanelShortCut.Controls.Add(Me.SidePanelImg3)
        Me.SidePanelShortCut.Controls.Add(Me.SidePanelImg2)
        Me.SidePanelShortCut.Controls.Add(Me.SidePanelImg1)
        Me.SidePanelShortCut.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanelShortCut.Location = New System.Drawing.Point(566, 40)
        Me.SidePanelShortCut.Name = "SidePanelShortCut"
        Me.SidePanelShortCut.Size = New System.Drawing.Size(866, 720)
        Me.SidePanelShortCut.TabIndex = 9
        Me.SidePanelShortCut.Text = "SidePanel1"
        '
        'SidePanelImg3
        '
        Me.SidePanelImg3.AllowResize = False
        Me.SidePanelImg3.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelImg3.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelImg3.Appearance.Options.UseBackColor = True
        Me.SidePanelImg3.Appearance.Options.UseBorderColor = True
        Me.SidePanelImg3.Controls.Add(Me.SidePanelImg32)
        Me.SidePanelImg3.Controls.Add(Me.SidePanelImg33)
        Me.SidePanelImg3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanelImg3.Location = New System.Drawing.Point(0, 346)
        Me.SidePanelImg3.Name = "SidePanelImg3"
        Me.SidePanelImg3.Size = New System.Drawing.Size(866, 374)
        Me.SidePanelImg3.TabIndex = 2
        Me.SidePanelImg3.Text = "SidePanel1"
        '
        'SidePanelImg32
        '
        Me.SidePanelImg32.AllowResize = False
        Me.SidePanelImg32.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelImg32.Appearance.Options.UseBorderColor = True
        Me.SidePanelImg32.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SidePanelImg32.Dock = System.Windows.Forms.DockStyle.Right
        Me.SidePanelImg32.Location = New System.Drawing.Point(332, 0)
        Me.SidePanelImg32.Name = "SidePanelImg32"
        Me.SidePanelImg32.Size = New System.Drawing.Size(267, 374)
        Me.SidePanelImg32.TabIndex = 1
        '
        'SidePanelImg33
        '
        Me.SidePanelImg33.AllowResize = False
        Me.SidePanelImg33.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelImg33.Appearance.Options.UseBorderColor = True
        Me.SidePanelImg33.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SidePanelImg33.Dock = System.Windows.Forms.DockStyle.Right
        Me.SidePanelImg33.Location = New System.Drawing.Point(599, 0)
        Me.SidePanelImg33.Name = "SidePanelImg33"
        Me.SidePanelImg33.Size = New System.Drawing.Size(267, 374)
        Me.SidePanelImg33.TabIndex = 0
        '
        'SidePanelImg2
        '
        Me.SidePanelImg2.AllowResize = False
        Me.SidePanelImg2.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelImg2.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelImg2.Appearance.Options.UseBackColor = True
        Me.SidePanelImg2.Appearance.Options.UseBorderColor = True
        Me.SidePanelImg2.Controls.Add(Me.SidePanelImg22)
        Me.SidePanelImg2.Controls.Add(Me.SidePanelImg21)
        Me.SidePanelImg2.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanelImg2.Location = New System.Drawing.Point(0, 173)
        Me.SidePanelImg2.Name = "SidePanelImg2"
        Me.SidePanelImg2.Size = New System.Drawing.Size(866, 173)
        Me.SidePanelImg2.TabIndex = 1
        Me.SidePanelImg2.Text = "SidePanel1"
        '
        'SidePanelImg22
        '
        Me.SidePanelImg22.AllowResize = False
        Me.SidePanelImg22.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelImg22.Appearance.Options.UseBorderColor = True
        Me.SidePanelImg22.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SidePanelImg22.Dock = System.Windows.Forms.DockStyle.Left
        Me.SidePanelImg22.Location = New System.Drawing.Point(281, 0)
        Me.SidePanelImg22.Name = "SidePanelImg22"
        Me.SidePanelImg22.Size = New System.Drawing.Size(281, 172)
        Me.SidePanelImg22.TabIndex = 1
        '
        'SidePanelImg21
        '
        Me.SidePanelImg21.AllowResize = False
        Me.SidePanelImg21.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelImg21.Appearance.Options.UseBorderColor = True
        Me.SidePanelImg21.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SidePanelImg21.Dock = System.Windows.Forms.DockStyle.Left
        Me.SidePanelImg21.Location = New System.Drawing.Point(0, 0)
        Me.SidePanelImg21.Name = "SidePanelImg21"
        Me.SidePanelImg21.Size = New System.Drawing.Size(281, 172)
        Me.SidePanelImg21.TabIndex = 0
        '
        'SidePanelImg1
        '
        Me.SidePanelImg1.AllowResize = False
        Me.SidePanelImg1.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelImg1.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelImg1.Appearance.Options.UseBackColor = True
        Me.SidePanelImg1.Appearance.Options.UseBorderColor = True
        Me.SidePanelImg1.Controls.Add(Me.SidePanelImg11)
        Me.SidePanelImg1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanelImg1.Location = New System.Drawing.Point(0, 0)
        Me.SidePanelImg1.Name = "SidePanelImg1"
        Me.SidePanelImg1.Size = New System.Drawing.Size(866, 173)
        Me.SidePanelImg1.TabIndex = 0
        Me.SidePanelImg1.Text = "SidePanel1"
        '
        'SidePanelImg11
        '
        Me.SidePanelImg11.AllowResize = False
        Me.SidePanelImg11.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelImg11.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.SidePanelImg11.Appearance.Options.UseBackColor = True
        Me.SidePanelImg11.Appearance.Options.UseBorderColor = True
        Me.SidePanelImg11.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SidePanelImg11.Dock = System.Windows.Forms.DockStyle.Left
        Me.SidePanelImg11.Location = New System.Drawing.Point(0, 0)
        Me.SidePanelImg11.Name = "SidePanelImg11"
        Me.SidePanelImg11.Padding = New System.Windows.Forms.Padding(100)
        Me.SidePanelImg11.Size = New System.Drawing.Size(304, 172)
        Me.SidePanelImg11.TabIndex = 0
        '
        'XtraHomeDashBoard
        '
        Me.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SidePanelShortCut)
        Me.Controls.Add(Me.SidePanelPie)
        Me.Controls.Add(Me.SidePanel2)
        Me.Controls.Add(Me.SidePanel1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraHomeDashBoard"
        Me.Size = New System.Drawing.Size(1432, 800)
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        Me.SidePanel2.ResumeLayout(False)
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanelPie.ResumeLayout(False)
        Me.SidePanelPie3.ResumeLayout(False)
        Me.SidePanelPie32.ResumeLayout(False)
        CType(PieSeriesLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartControlWO, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanelPie31.ResumeLayout(False)
        CType(PieSeriesLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartControlLeave, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanelPie2.ResumeLayout(False)
        Me.SidePanelPie22.ResumeLayout(False)
        CType(PieSeriesLabel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartControlLate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanelPie21.ResumeLayout(False)
        CType(PieSeriesLabel4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartControlAbs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanelPie1.ResumeLayout(False)
        Me.SidePanelPie12.ResumeLayout(False)
        CType(PieSeriesLabel5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartControlPresent, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanelPie11.ResumeLayout(False)
        CType(PieSeriesLabel6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartControlTotal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanelShortCut.ResumeLayout(False)
        Me.SidePanelImg3.ResumeLayout(False)
        Me.SidePanelImg2.ResumeLayout(False)
        Me.SidePanelImg1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TileItem1 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem2 As DevExpress.XtraEditors.TileItem
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelPie As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelPie3 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelPie32 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents ChartControlWO As DevExpress.XtraCharts.ChartControl
    Friend WithEvents SidePanelWO As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelPie31 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents ChartControlLeave As DevExpress.XtraCharts.ChartControl
    Friend WithEvents SidePanelLeave As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelPie2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelPie22 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents ChartControlLate As DevExpress.XtraCharts.ChartControl
    Friend WithEvents SidePanelLate As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelPie21 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents ChartControlAbs As DevExpress.XtraCharts.ChartControl
    Friend WithEvents SidePanelAbs As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelPie1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelPie12 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents ChartControlPresent As DevExpress.XtraCharts.ChartControl
    Friend WithEvents SidePanelPresent As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelPie11 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents ChartControlTotal As DevExpress.XtraCharts.ChartControl
    Friend WithEvents SidePanelTotal As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelShortCut As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelImg3 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelImg32 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelImg33 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelImg2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelImg22 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelImg21 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelImg1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelImg11 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
    Public WithEvents TimerRefreshDashBoard As System.Windows.Forms.Timer
    Friend WithEvents LabelActivate As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelTrial As DevExpress.XtraEditors.LabelControl

End Class
