﻿Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Data.SqlClient
Imports System.IO
Imports System.Resources
Imports System.Globalization

Public Class XtraDBBackUpSetting
    Dim connection As SqlConnection
    Dim ConnectionString As String
    Dim Con1 As OleDbConnection
    Dim ulf As UserLookAndFeel

    Private Sub XtraDBBackUpSetting_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100
        'Common.res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraCompany).Assembly)
        'Common.cul = CultureInfo.CreateSpecificCulture("en")
        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraDB).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")

        PanelControl1.Width = PanelControl1.Parent.Width * 3 / 4
        TextBKPath.Width = TextBKPath.Parent.Width * 3 / 4
        TextAccessBKLocation.Width = TextBKPath.Width 'TextAccessBKLocation.Width * 3 / 4
        DateEdit2.DateTime = Now.AddDays(-30)
        DateEdit3.DateTime = Now
        If Common.IsNepali = "Y" Then
            ComboBoxEditNepaliYear.Visible = True
            ComboBoxEditNEpaliMonth.Visible = True
            ComboBoxEditNepaliDate.Visible = True
            DateEdit2.Visible = False
            DateEdit3.Visible = False
            ComboBoxEditNepaliYearDOB.Visible = True
            ComboBoxEditNEpaliMonthDOB.Visible = True
            ComboBoxEditNepaliDateDOB.Visible = True

            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(DateEdit2.DateTime.Year, DateEdit2.DateTime.Month, DateEdit2.DateTime.Day))            
            Dim dojTmp() As String = doj.Split("-")
            ComboBoxEditNepaliYear.EditValue = dojTmp(0)
            ComboBoxEditNEpaliMonth.SelectedIndex = dojTmp(1) - 1
            ComboBoxEditNepaliDate.EditValue = dojTmp(2)


            doj = DC.ToBS(New Date(DateEdit3.DateTime.Year, DateEdit3.DateTime.Month, DateEdit3.DateTime.Day))          
            dojTmp = doj.Split("-")
            ComboBoxEditNepaliYearDOB.EditValue = dojTmp(0)
            ComboBoxEditNEpaliMonthDOB.SelectedIndex = dojTmp(1) - 1
            ComboBoxEditNepaliDateDOB.EditValue = dojTmp(2)

        Else
            ComboBoxEditNepaliYear.Visible = False
            ComboBoxEditNEpaliMonth.Visible = False
            ComboBoxEditNepaliDate.Visible = False
            DateEdit2.Visible = True
            DateEdit3.Visible = True
            ComboBoxEditNepaliYearDOB.Visible = False
            ComboBoxEditNEpaliMonthDOB.Visible = False
            ComboBoxEditNepaliDateDOB.Visible = False
        End If

        If Common.servername = "Access" Then
            LabelControl9.Visible = True
            TextAccessBKLocation.Visible = True
            BtnAccessDB.Visible = True
        Else
            LabelControl9.Visible = False
            TextAccessBKLocation.Visible = False
            BtnAccessDB.Visible = False
        End If
        Dim BackUpType As String
        Dim BackUpPath As String
        Dim IsAutoBackUp As String
        Dim DeleteAfterAutoBackUp As String
        Dim AutoBackUpTimerDays As String
        Dim AutoBackUpTimerTime As String
        Dim AutoBackUpForDays As String
        Dim AccessAutoBackUpPath As String
        Dim LastAutoBackUpDate As DateTime
        Dim LastBackUpDate As DateTime

        Dim sSql As String = "select * from BackUpData"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            ComboBKType.EditValue = ds.Tables(0).Rows(0).Item("BackUpType").ToString.Trim
            TextBKPath.Text = ds.Tables(0).Rows(0).Item("BackUpPath").ToString.Trim
            If ds.Tables(0).Rows(0).Item("DeleteAfterAutoBackUp").ToString.Trim = "Y" Then
                ToggleBKDelete.IsOn = True
            Else
                ToggleBKDelete.IsOn = False
            End If

            If ds.Tables(0).Rows(0).Item("IsAutoBackUp").ToString.Trim = "Y" Then
                ToggleBKAuto.IsOn = True
            Else
                ToggleBKAuto.IsOn = False
            End If
            TextBKTimerDays.Text = ds.Tables(0).Rows(0).Item("AutoBackUpTimerDays").ToString.Trim
            TxtAutoBackUpTimerTime.Text = ds.Tables(0).Rows(0).Item("AutoBackUpTimerTime").ToString.Trim
            TextBKFor.Text = ds.Tables(0).Rows(0).Item("AutoBackUpForDays").ToString.Trim
            TextAccessBKLocation.Text = ds.Tables(0).Rows(0).Item("AccessAutoBackUpPath").ToString.Trim
        Else
            ComboBKType.SelectedIndex = 0
            TextBKPath.Text = ""
            ToggleBKDelete.IsOn = False
            ToggleBKAuto.IsOn = False
            TextBKTimerDays.Text = ""
            TxtAutoBackUpTimerTime.Text = "10:00"
            TextBKFor.Text = ""
            TextAccessBKLocation.Text = ""

        End If
    End Sub
    Private Sub TextBKPath_Click(sender As System.Object, e As System.EventArgs) Handles TextBKPath.Click
        Dim folderBrowserDialog As FolderBrowserDialog = New FolderBrowserDialog
        folderBrowserDialog.ShowDialog()
        If folderBrowserDialog.SelectedPath.ToString.Trim <> "" Then
            TextBKPath.Text = folderBrowserDialog.SelectedPath
        End If
    End Sub
    Private Sub TextAccessBKLocation_Click(sender As System.Object, e As System.EventArgs) Handles TextAccessBKLocation.Click
        Dim folderBrowserDialog As FolderBrowserDialog = New FolderBrowserDialog
        folderBrowserDialog.ShowDialog()
        If folderBrowserDialog.SelectedPath.ToString.Trim <> "" Then
            TextAccessBKLocation.Text = folderBrowserDialog.SelectedPath
        End If
    End Sub
    Private Sub SimpleSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleSave.Click
        If TextBKPath.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>BackUp File Location Cannot be Empty</size>", "<size=9>Error</size>")
            TextBKPath.Select()
            Exit Sub
        End If
        If ToggleBKAuto.IsOn = True Then
            If TextBKTimerDays.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Auto BackUp In Days Cannot be Empty</size>", "<size=9>Error</size>")
                TextBKTimerDays.Select()
                Exit Sub
            End If

            If TextBKFor.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Auto BackUp For Days Cannot be Empty</size>", "<size=9>Error</size>")
                TextBKFor.Select()
                Exit Sub
            End If

        End If
        Me.Cursor = Cursors.WaitCursor
        Dim BackUpType As String = ComboBKType.EditValue.ToString.Trim
        Dim BackUpPath As String = TextBKPath.Text.Trim
        Dim IsAutoBackUp As String
        If ToggleBKAuto.IsOn = True Then
            IsAutoBackUp = "Y"
        Else
            IsAutoBackUp = "N"
        End If
        Dim DeleteAfterAutoBackUp As String
        If ToggleBKDelete.IsOn = True Then
            DeleteAfterAutoBackUp = "Y"
            Common.IsAutoBackUp = True
        Else
            DeleteAfterAutoBackUp = "N"
            Common.IsAutoBackUp = False
        End If
        Dim AutoBackUpTimerDays As String = TextBKTimerDays.Text.Trim
        Dim AutoBackUpTimerTime As String = TxtAutoBackUpTimerTime.Text.Trim
        Dim AutoBackUpForDays As String = TextBKFor.Text.Trim
        Dim AccessAutoBackUpPath As String = TextAccessBKLocation.Text.Trim
        'Dim LastAutoBackUpDate As DateTime
        'Dim LastBackUpDate As DateTime
        Dim sSql As String = "select * from BackUpData"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        Dim LastAutoBackUpDateTime As String = Now.ToString("yyyy-MM-dd HH:mm:ss")      
        If ds.Tables(0).Rows.Count > 0 Then
            'update 
            sSql = "update BackUpData set BackUpType='" & BackUpType & "', BackUpPath='" & BackUpPath & "', IsAutoBackUp='" & IsAutoBackUp & "' , " & _
                "DeleteAfterAutoBackUp ='" & DeleteAfterAutoBackUp & "', AutoBackUpTimerDays='" & AutoBackUpTimerDays & "', AutoBackUpTimerTime='" & AutoBackUpTimerTime & "', " & _
                "AutoBackUpForDays='" & AutoBackUpForDays & "', AccessAutoBackUpPath='" & AccessAutoBackUpPath & "', LastAutoBackUpDate='" & LastAutoBackUpDateTime & "'"
        Else
            sSql = "insert into BackUpData (BackUpType,BackUpPath,IsAutoBackUp,DeleteAfterAutoBackUp,AutoBackUpTimerDays,AutoBackUpTimerTime,AutoBackUpForDays,AccessAutoBackUpPath, LastAutoBackUpDate) " & _
                "values ('" & BackUpType & "','" & BackUpPath & "','" & IsAutoBackUp & "','" & DeleteAfterAutoBackUp & "','" & AutoBackUpTimerDays & "','" & AutoBackUpTimerTime & "','" & AutoBackUpForDays & "','" & AccessAutoBackUpPath & "','" & LastAutoBackUpDateTime & "')"
        End If
        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand
        Try
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            Common.GetNextBckTime()
            Common.LogPost("Log Backup Setting Save")
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>Success</size>")
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.Trim & "</size>", "<size=9>Error</size>")
        End Try
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            If ComboBoxEditNepaliYear.EditValue.ToString.Trim <> "" And ComboBoxEditNEpaliMonth.EditValue.ToString.Trim <> "" And ComboBoxEditNepaliDate.EditValue.ToString.Trim <> "" Then
                Try
                    'DateEdit2.DateTime = DC.ToAD(New Date(ComboBoxEditNepaliYear.EditValue, ComboBoxEditNEpaliMonth.SelectedIndex + 1, ComboBoxEditNepaliDate.EditValue))
                    DateEdit2.DateTime = DC.ToAD(ComboBoxEditNepaliYear.EditValue & "-" & ComboBoxEditNEpaliMonth.SelectedIndex + 1 & "-" & ComboBoxEditNepaliDate.EditValue)
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Start Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ComboBoxEditNepaliDate.Select()
                    Exit Sub
                End Try
            End If
            If ComboBoxEditNepaliYearDOB.EditValue.ToString.Trim <> "" And ComboBoxEditNEpaliMonthDOB.EditValue.ToString.Trim <> "" And ComboBoxEditNepaliDateDOB.EditValue.ToString.Trim <> "" Then
                Try
                    'DateEdit3.DateTime = DC.ToAD(New Date(ComboBoxEditNepaliYearDOB.EditValue, ComboBoxEditNEpaliMonthDOB.SelectedIndex + 1, ComboBoxEditNepaliDateDOB.EditValue))
                    DateEdit3.DateTime = DC.ToAD(ComboBoxEditNepaliYearDOB.EditValue & "-" & ComboBoxEditNEpaliMonthDOB.SelectedIndex + 1 & "-" & ComboBoxEditNepaliDateDOB.EditValue)
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Invalid End Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ComboBoxEditNepaliDateDOB.Select()
                    Exit Sub
                End Try
            End If
        End If
        Me.Cursor = Cursors.WaitCursor
        Dim delete As Boolean
        If ToggleBKDelete.IsOn = True Then
            delete = True
        Else
            delete = False
        End If

        Dim sSql As String = "select * from BackUpData"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            'update 
            sSql = "update BackUpData set LastBackUpDate='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
        Else
            sSql = "insert into BackUpData (LastBackUpDate) " & _
                "values ('" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        End If
        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand
        Try
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            'XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.Trim & "</size>", "<size=9>Error</size>")
        End Try
        Common.LogPost("Manual Log Backup")
        Me.Cursor = Cursors.Default
        Common.backUpTxtDat(DateEdit2.DateTime, DateEdit3.DateTime, ComboBKType.EditValue.ToString.Trim, TextBKPath.Text.Trim, delete)
        XtraMessageBox.Show(ulf, "<size=10>BackUp Successful</size>", "<size=9>Success</size>")
    End Sub
    Private Sub BtnAccessDB_Click(sender As System.Object, e As System.EventArgs) Handles BtnAccessDB.Click
        Try
            Dim sourceFile As String = My.Application.Info.DirectoryPath & "\TimeWatch.mdb"
            Dim destibation As String = TextAccessBKLocation.Text.Trim & "\TimeWatch.mdb"
            My.Computer.FileSystem.CopyFile(sourceFile, destibation, Microsoft.VisualBasic.FileIO.UIOption.AllDialogs, Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)
            Common.LogPost("Access Database Backup")
            XtraMessageBox.Show(ulf, "<size=10>BackUp Successful</size>", "<size=9>Success</size>")
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.Trim & "</size>", "<size=9>Error</size>")
        End Try
        
    End Sub
End Class
