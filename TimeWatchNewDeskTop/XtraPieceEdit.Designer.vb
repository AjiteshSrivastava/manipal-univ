﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraPieceEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPCode = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPDes = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPRate = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.TextPCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPDes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPRate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(18, 25)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(108, 14)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Piece Type Code: *"
        '
        'TextPCode
        '
        Me.TextPCode.Location = New System.Drawing.Point(131, 22)
        Me.TextPCode.Name = "TextPCode"
        Me.TextPCode.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPCode.Properties.Appearance.Options.UseFont = True
        Me.TextPCode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextPCode.Properties.Mask.EditMask = "[A-Z0-9]*"
        Me.TextPCode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextPCode.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextPCode.Properties.MaxLength = 3
        Me.TextPCode.Size = New System.Drawing.Size(230, 20)
        Me.TextPCode.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(18, 51)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(75, 14)
        Me.LabelControl1.TabIndex = 6
        Me.LabelControl1.Text = "Description: *"
        '
        'TextPDes
        '
        Me.TextPDes.Location = New System.Drawing.Point(131, 48)
        Me.TextPDes.Name = "TextPDes"
        Me.TextPDes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPDes.Properties.Appearance.Options.UseFont = True
        Me.TextPDes.Properties.Mask.EditMask = "[a-zA-Z0-9]*"
        Me.TextPDes.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextPDes.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextPDes.Properties.MaxLength = 35
        Me.TextPDes.Size = New System.Drawing.Size(230, 20)
        Me.TextPDes.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(18, 77)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(29, 14)
        Me.LabelControl3.TabIndex = 8
        Me.LabelControl3.Text = "Rate:"
        '
        'TextPRate
        '
        Me.TextPRate.Location = New System.Drawing.Point(131, 74)
        Me.TextPRate.Name = "TextPRate"
        Me.TextPRate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPRate.Properties.Appearance.Options.UseFont = True
        Me.TextPRate.Properties.Mask.EditMask = "###.##"
        Me.TextPRate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextPRate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextPRate.Properties.MaxLength = 6
        Me.TextPRate.Size = New System.Drawing.Size(85, 20)
        Me.TextPRate.TabIndex = 3
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(286, 100)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 5
        Me.SimpleButton2.Text = "Cancel"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(198, 100)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 4
        Me.SimpleButton1.Text = "Save"
        '
        'XtraPieceEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 140)
        Me.ControlBox = False
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.TextPRate)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.TextPDes)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.TextPCode)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraPieceEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Piece"
        CType(Me.TextPCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPDes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPRate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPDes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPRate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
End Class
