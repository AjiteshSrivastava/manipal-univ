﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base
Imports iAS.HSeriesSampleCSharp
Imports System.Runtime.InteropServices
Imports CMITech.UMXClient

Public Class XtraEmployeeCopyToDevice
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim h As IntPtr = IntPtr.Zero
    Dim DC As New DateConverter()

    Const PWD_DATA_SIZE = 40
    Const FP_DATA_SIZE = 1680
    Const FACE_DATA_SIZE = 20080
    Const VEIN_DATA_SIZE = 3080
    Private mbytEnrollData(FACE_DATA_SIZE - 1) As Byte
    Private mbytPhotoImage() As Byte

    Private mnCommHandleIndex As Long
    Private mbGetState As Boolean
    Private mlngPasswordData As Long

    Dim vpszIPAddress As String
    Dim vpszNetPort As Long
    Dim vpszNetPassword As Long
    Dim vnTimeOut As Long
    Dim vnProtocolType As Long
    Dim strDateTime As String
    Dim vnResult As Long
    Dim vnLicense As Long
    '//=============== Backup Number Constant ===============//
    'Public Const BACKUP_FP_0 = 0                  ' Finger 0
    'Public Const BACKUP_FP_1 = 1                  ' Finger 1
    'Public Const BACKUP_FP_2 = 2                  ' Finger 2
    'Public Const BACKUP_FP_3 = 3                  ' Finger 3
    'Public Const BACKUP_FP_4 = 4                  ' Finger 4
    'Public Const BACKUP_FP_5 = 5                  ' Finger 5
    'Public Const BACKUP_FP_6 = 6                  ' Finger 6
    'Public Const BACKUP_FP_7 = 7                  ' Finger 7
    'Public Const BACKUP_FP_8 = 8                  ' Finger 8
    'Public Const BACKUP_FP_9 = 9                  ' Finger 9
    'Public Const BACKUP_PSW = 10                  ' Password
    Public Const BACKUP_CARD = 11                 ' Card
    Public Const BACKUP_FACE = 12                 ' Face
    Public Const BACKUP_VEIN_0 = 20               ' Vein 0

    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
        Common.SetGridFont(GridView2, New Font("Tahoma", 10))
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraEmployeeCopyToDevice_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'If Common.servername = "Access" Then
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        '    GridControl2.DataSource = SSSDBDataSet.TblEmployee1
        'Else
        '    TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        '    GridControl2.DataSource = SSSDBDataSet.TblEmployee
        'End If
        GridControl1.DataSource = Common.MachineNonAdmin
        GridControl2.DataSource = Common.EmpNonAdmin

        DateEditStart.DateTime = "2018-01-01 00:00:00"
        DateEditEnd.DateTime = "2029-12-31 23:59:59"
        If Common.IsNepali = "Y" Then
            DateEditStart.Visible = False
            DateEditEnd.Visible = False
            ComboNepaliVStartDate.Visible = True
            ComboNepaliVStartMonth.Visible = True
            ComboNepaliVStartYear.Visible = True

            ComboNepaliVEndDate.Visible = True
            ComboNepaliVEndMonth.Visible = True
            ComboNepaliVEndYear.Visible = True

            Dim Vstart As String = DC.ToBS(New Date(DateEditStart.DateTime.Year, DateEditStart.DateTime.Month, DateEditStart.DateTime.Day))
            Dim dojTmp() As String = Vstart.Split("-")
            ComboNepaliVStartYear.EditValue = dojTmp(0)
            ComboNepaliVStartMonth.SelectedIndex = dojTmp(1) - 1
            ComboNepaliVStartDate.EditValue = dojTmp(2)


            Dim VEnd As String = DC.ToBS(New Date(DateEditEnd.DateTime.Year, DateEditEnd.DateTime.Month, DateEditEnd.DateTime.Day))           
            dojTmp = VEnd.Split("-")
            ComboNepaliVEndYear.EditValue = dojTmp(0)
            ComboNepaliVEndMonth.SelectedIndex = dojTmp(1) - 1
            ComboNepaliVEndDate.EditValue = dojTmp(2)


            ComboNepaliVStartDate.Enabled = False
            ComboNepaliVStartMonth.Enabled = False
            ComboNepaliVStartYear.Enabled = False

            ComboNepaliVEndDate.Enabled = False
            ComboNepaliVEndMonth.Enabled = False
            ComboNepaliVEndYear.Enabled = False

        Else
            DateEditStart.Visible = True
            DateEditEnd.Visible = True
            ComboNepaliVStartDate.Visible = False
            ComboNepaliVStartMonth.Visible = False
            ComboNepaliVStartYear.Visible = False

            ComboNepaliVEndDate.Visible = False
            ComboNepaliVEndMonth.Visible = False
            ComboNepaliVEndYear.Visible = False
        End If
        'CheckCreate.Checked = True
        'CheckCreate.Enabled = False
        CheckEdit1.Checked = False
        CheckFrmDt.Checked = True
        GridView1.ClearSelection()
        GridView2.ClearSelection()
        CheckEditCopyCard.Checked = False
    End Sub
    Private Sub GridView2_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView2.CustomColumnDisplayText
        Try
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "FingerNumber" Then
                If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim >= 0 And _
                    view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim < 10 Then
                    e.DisplayText = "FP-" & view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim + 1
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 12 Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 50 Then
                    e.DisplayText = "FACE"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 11 Then
                    e.DisplayText = "CARD"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 10 Then
                    e.DisplayText = "PASSWORD"
                End If
                e.DisplayText = (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim \ 60).ToString("00") & ":" & (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim Mod 60).ToString("00")
            End If
        Catch
        End Try
        Try
            Dim adapA As OleDbDataAdapter
            Dim adap As SqlDataAdapter
            Dim ds As DataSet
            Dim sSql As String
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "UserName" Then
                sSql = "SELECT EMPNAME from TblEmployee where PRESENTCARDNO = '" & view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EnrollNumber").ToString().Trim & "'"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    e.DisplayText = ds.Tables(0).Rows(0).Item(0).ToString
                Else
                    e.DisplayText = ""
                End If
            End If

            If e.Column.Caption = "Device Type" Then
                If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Template").ToString().Trim = "" Then
                    e.DisplayText = "Bio"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Template_Tw").ToString().Trim = "" Then
                    e.DisplayText = "ZK"
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
    Private Sub btnUploadFinger_Click(sender As System.Object, e As System.EventArgs) Handles btnUploadFinger.Click
        Dim m As Integer
        'Dim e As Integer
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim mKey As String
        Dim vnMachineNumber As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long
        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim strDateTime As String
        Dim vRet As Long
        Dim nCommHandleIndex As Long
        Dim vPrivilege As Long
        Dim failIP As New List(Of String)()
        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If GridView2.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Employee</size>", "<size=9>Error</size>")
            Exit Sub
        End If

        If Common.IsNepali = "Y" Then
            Try
                'DateEditStart.DateTime = DC.ToAD(New Date(ComboNepaliVStartYear.EditValue, ComboNepaliVStartMonth.SelectedIndex + 1, ComboNepaliVStartDate.EditValue))
                DateEditStart.DateTime = DC.ToAD(ComboNepaliVStartYear.EditValue & "-" & ComboNepaliVStartMonth.SelectedIndex + 1 & "-" & ComboNepaliVStartDate.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Validity Start Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliVStartDate.Select()
                Exit Sub
            End Try
            Try
                'DateEditEnd.DateTime = DC.ToAD(New Date(ComboNepaliVEndYear.EditValue, ComboNepaliVEndMonth.SelectedIndex + 1, ComboNepaliVEndDate.EditValue))
                DateEditEnd.DateTime = DC.ToAD(ComboNepaliVEndYear.EditValue & "-" & ComboNepaliVEndMonth.SelectedIndex + 1 & "-" & ComboNepaliVEndDate.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Validity End Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliVEndDate.Select()
                Exit Sub
            End Try
        End If
        Dim sSql As String = ""
        Me.Cursor = Cursors.WaitCursor
        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        Dim commkey As Integer
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then


                LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                vpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim
                Common.LogPost("Employee Export to Device; Device Id: " & LstMachineId)
                If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                    sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    Dim bConn As Boolean = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)
                    vpszNetPort = CLng("5005")
                    vpszNetPassword = CLng("0")
                    vnTimeOut = CLng("5000")
                    XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    Application.DoEvents()
                    vnProtocolType = 0 'PROTOCOL_TCPIP
                    'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        vRet = FK_ConnectUSB(vnMachineNumber, vnLicense)
                    Else
                        vpszIPAddress = Trim(lpszIPAddress)
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vRet = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        nCommHandleIndex = vRet
                    End If
                    If vRet > 0 Then
                        bConn = True
                    Else
                        bConn = False
                    End If
                    If bConn Then
                        Dim isStringId As Boolean
                        If FK_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
                            isStringId = True
                        Else
                            isStringId = False
                        End If
                        vRet = FK_EnableDevice(nCommHandleIndex, 0)
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Integer = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim EnrollNumber As String = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim)
                                Dim FingerNumber As Long = 0 'GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
                                Dim Uname As String = GridView2.GetRowCellValue(rowHandleEmp, "EMPNAME").ToString.Trim

                                XtraMasterTest.LabelControlStatus.Text = "Uploading User " & EnrollNumber
                                Application.DoEvents()

                                Dim vStrEnrollNumber As String = Trim(EnrollNumber)
                                Dim vEnrollNumber As Long = Convert.ToUInt64(vStrEnrollNumber)


                                If GridView2.GetRowCellValue(rowHandleEmp, "MachineCard").ToString.Trim <> "" And CheckEditCopyCard.Checked Then
                                    Dim vbytEnrollData(FACE_DATA_SIZE - 1) As Byte
                                    vbytEnrollData = System.Text.Encoding.UTF8.GetBytes("IDC_HS1:" & GridView2.GetRowCellValue(rowHandleEmp, "MachineCard").ToString.Trim)
                                    'vbytEnrollData = System.Text.Encoding.UTF8.GetBytes(GridView2.GetRowCellValue(rowHandleEmp, "MachineCard").ToString.Trim)
                                    'Array.Copy(vbytEnrollData, mbytEnrollData, PWD_DATA_SIZE)
                                    vPrivilege = 0
                                    Dim vnIsSuppoterd As Long
                                    FK_IsSupportedEnrollData(mnCommHandleIndex, 11, vnIsSuppoterd)
                                    If isStringId Then
                                        vRet = FK_PutEnrollData_StringID(mnCommHandleIndex,
                                                 vStrEnrollNumber,
                                                 11,
                                                 vPrivilege,
                                                 vbytEnrollData,
                                                 mlngPasswordData)
                                    Else
                                        vRet = FK_PutEnrollData(mnCommHandleIndex,
                                                vEnrollNumber,
                                                11,
                                                vPrivilege,
                                                vbytEnrollData,
                                                mlngPasswordData)
                                    End If
                                    vRet = FK_SaveEnrollData(mnCommHandleIndex)
                                End If

                                If isStringId Then
                                    vRet = FK_SetUserName_StringID(vRet, vStrEnrollNumber, Trim(Uname))
                                Else
                                    vRet = FK_SetUserName(vRet, vEnrollNumber, Trim(Uname))
                                End If

                                If CheckEdit1.Checked = True Then
                                    'finger validity

                                    If (vRet <> CType(RUN_SUCCESS, Integer)) Then
                                        Continue For
                                    End If
                                    Dim vSize As Integer = (20 + 264)
                                    Dim bytExtCmd_USERDOORINFO() As Byte = New Byte((vSize) - 1) {}
                                    Dim vExtCmd_USERDOORINFO As Common.ExtCmd_USERDOORINFO = New Common.ExtCmd_USERDOORINFO
                                    vStrEnrollNumber = vEnrollNumber.ToString.Trim
                                    'vEnrollNumber = FKAttendDLL.GetInt(vStrEnrollNumber)
                                    vExtCmd_USERDOORINFO.Init("ECMD_SetUserDoorInfo", vEnrollNumber)

                                    If CheckFrmDt.Checked = True Then
                                        vExtCmd_USERDOORINFO.StartYear = CType(DateEditStart.DateTime.ToString("yyyy"), Short)
                                        vExtCmd_USERDOORINFO.StartMonth = CType(DateEditStart.DateTime.ToString("MM"), Byte)
                                        vExtCmd_USERDOORINFO.StartDay = CType(DateEditStart.DateTime.ToString("dd"), Byte)
                                        vExtCmd_USERDOORINFO.EndYear = CType(DateEditEnd.DateTime.ToString("yyyy"), Short)
                                        vExtCmd_USERDOORINFO.EndMonth = CType(DateEditEnd.DateTime.ToString("MM"), Byte)
                                        vExtCmd_USERDOORINFO.EndDay = CType(DateEditEnd.DateTime.ToString("dd"), Byte)
                                    Else
                                        'EnrollNumber = Convert.ToDouble(EnrollNumber)
                                        Dim s As String = "select ValidityStartDate, ValidityEndDate from TblEmployee where PRESENTCARDNO = '" & GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim & "'"
                                        Dim adap As SqlDataAdapter
                                        Dim adapA As OleDbDataAdapter
                                        Dim ds As DataSet = New DataSet
                                        If Common.servername = "Access" Then
                                            adapA = New OleDbDataAdapter(s, Common.con1)
                                            adapA.Fill(ds)
                                        Else
                                            adap = New SqlDataAdapter(s, Common.con)
                                            adap.Fill(ds)
                                        End If
                                        Try
                                            Dim startdate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ValidityStartDate").ToString.Trim)
                                            Dim enddate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ValidityEndDate").ToString.Trim)
                                            vExtCmd_USERDOORINFO.StartYear = CType(startdate.ToString("yyyy"), Short)
                                            vExtCmd_USERDOORINFO.StartMonth = CType(startdate.ToString("MM"), Byte)
                                            vExtCmd_USERDOORINFO.StartDay = CType(startdate.ToString("dd"), Byte)
                                            vExtCmd_USERDOORINFO.EndYear = CType(enddate.ToString("yyyy"), Short)
                                            vExtCmd_USERDOORINFO.EndMonth = CType(enddate.ToString("MM"), Byte)
                                            vExtCmd_USERDOORINFO.EndDay = CType(enddate.ToString("dd"), Byte)
                                        Catch ex As Exception

                                        End Try
                                    End If

                                    ConvertStructureToByteArray(vExtCmd_USERDOORINFO, bytExtCmd_USERDOORINFO)
                                    vRet = FK_ExtCommand(nCommHandleIndex, bytExtCmd_USERDOORINFO)


                                    'TimeZone Sart 
                                    Dim bytUserPassTime(SIZE_USER_WEEK_PASS_TIME_STRUCT - 1) As Byte
                                    Dim vUserPassTime As USER_WEEK_PASS_TIME
                                    Dim nFKRetCode As Integer
                                    vUserPassTime.Init()
                                    GetUserWeekPassTimeValue(vStrEnrollNumber, vUserPassTime)
                                    'nFKRetCode = FK_EnableDevice(vRet, 0)
                                    If nFKRetCode <> RUN_SUCCESS Then
                                        Return
                                    End If
                                    ConvertStructureToByteArray(vUserPassTime, bytUserPassTime)
                                    nFKRetCode = FK_HS_SetUserWeekPassTime(vRet, bytUserPassTime)
                                    If nFKRetCode = RUN_SUCCESS Then
                                        'lblMessage.Text = "Success !"
                                    Else
                                        'lblMessage.Text = ReturnResultPrint(nFKRetCode)
                                        ' XtraMessageBox.Show(ulf, "<size=10>" & ReturnResultPrint(nFKRetCode) & "</size>", "Error")
                                        Exit Sub
                                    End If
                                End If
                                'End finger validity
                                'cmdSetAllEnrollData_bio_All(vnMachineNumber, EnrollNumber, FingerNumber, Uname)
                            End If
                        Next x
                        vRet = FK_EnableDevice(nCommHandleIndex, 1)
                    Else
                        'MsgBox("Device No: " & LstMachineId & " Not connected..")
                        failIP.Add(vpszIPAddress.ToString)
                        Continue For
                        'XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                    End If
                    'FK623Attend.Disconnect
                    FK_DisConnect(nCommHandleIndex)
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

                    Dim sdwEnrollNumber As String
                    Dim sName As String = ""
                    Dim sPassword As String = ""
                    Dim iPrivilege As Integer
                    Dim idwFingerIndex As Integer
                    Dim sTmpData As String = ""
                    Dim sEnabled As String = ""
                    Dim bEnabled As Boolean = False
                    Dim iflag As Integer
                    commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, "commkey").ToString.Trim)
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)
                    XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    Application.DoEvents()
                    Dim bIsConnected = False
                    Dim iMachineNumber As Integer = result(i)
                    Dim idwErrorCode As Integer
                    Dim com As Common = New Common
                    Dim axCZKEM1 As New zkemkeeper.CZKEM
                    'Dim axCZKEM1 As zkemkeeper.CZKEMClass = New zkemkeeper.CZKEMClass
                    axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                    bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                    If bIsConnected = True Then
                        axCZKEM1.EnableDevice(iMachineNumber, False)

                        'loop for fingerprint
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Integer = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim adap As SqlDataAdapter
                                Dim adapA As OleDbDataAdapter
                                'Dim RsFp As DataSet = New DataSet
                                Dim mUser_ID As String
                                Try
                                    mUser_ID = Convert.ToDouble(GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim) 'GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                                Catch ex As Exception
                                    mUser_ID = GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim 'GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                                End Try
                                XtraMasterTest.LabelControlStatus.Text = "Uploading User " & mUser_ID
                                Application.DoEvents()

                                sdwEnrollNumber = mUser_ID ' Convert.ToInt32(RsFp.Tables(0).Rows(0)("enrollnumber").ToString())

                                Dim rs As Integer
                                Dim ret
                                Dim strCardno = ""

                                ret = axCZKEM1.SSR_GetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled)  'get the user info if user is already in device
                                If ret = False Then
                                    idwFingerIndex = 6 'Convert.ToInt32(RsFp.Tables(0).Rows(0)("FingerNumber").ToString().Trim())
                                    sTmpData = "" ' RsFp.Tables(0).Rows(0)("Template").ToString().Trim()
                                    iPrivilege = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)("Privilege").ToString().Trim()) 'told by ajitesh(used for admin)
                                    sPassword = "" ' RsFp.Tables(0).Rows(0)("Password").ToString().Trim()
                                    sEnabled = "true" 'RsFp.Tables(0).Rows(0)(6).ToString().Trim()
                                    iflag = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)(7).ToString())' as per old s/w
                                    If sEnabled.ToString().ToLower() = "true" Then
                                        bEnabled = True
                                    Else
                                        bEnabled = False
                                    End If
                                Else
                                    axCZKEM1.GetStrCardNumber(strCardno)
                                    If strCardno = "0" Then
                                        strCardno = ""
                                    End If
                                End If
                                sName = GridView2.GetRowCellValue(rowHandleEmp, "EMPNAME").ToString.Trim() 'dstmp.Tables(0).Rows(0)("EMPNAME").ToString().Trim()
                                If CheckEditCopyCard.Checked And GridView2.GetRowCellValue(rowHandleEmp, "MachineCard").ToString.Trim() <> "" Then
                                    strCardno = GridView2.GetRowCellValue(rowHandleEmp, "MachineCard").ToString.Trim()
                                End If
                                axCZKEM1.SetStrCardNumber(strCardno)
                                If axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, 0, bEnabled) Then 'upload user information to the device
                                    rs = axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iflag, sTmpData) 'upload templates information to the device
                                Else
                                    axCZKEM1.GetLastError(idwErrorCode)
                                    axCZKEM1.EnableDevice(iMachineNumber, True)
                                    Return
                                End If
                                If CheckEdit1.Checked = True Then
                                    If CheckFrmDt.Checked = True Then
                                        ret = axCZKEM1.SetUserValidDate(iMachineNumber, sdwEnrollNumber, 1, 0, DateEditStart.DateTime.ToString("yyyy-MM-dd HH:mm:00"), DateEditEnd.DateTime.ToString("yyyy-MM-dd HH:mm:59"))  'add valdity
                                    Else
                                        Dim s As String = "select ValidityStartDate, ValidityEndDate from TblEmployee where PRESENTCARDNO = '" & GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim & "'"
                                        Dim adap1 As SqlDataAdapter
                                        Dim adapA1 As OleDbDataAdapter
                                        Dim ds As DataSet = New DataSet
                                        If Common.servername = "Access" Then
                                            adapA1 = New OleDbDataAdapter(s, Common.con1)
                                            adapA1.Fill(ds)
                                        Else
                                            adap1 = New SqlDataAdapter(s, Common.con)
                                            adap1.Fill(ds)
                                        End If
                                        Dim startdate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ValidityStartDate").ToString.Trim)
                                        Dim enddate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ValidityEndDate").ToString.Trim)
                                        ret = axCZKEM1.SetUserValidDate(iMachineNumber, sdwEnrollNumber, 1, 0, startdate.ToString("yyyy-MM-dd HH:mm:00"), enddate.ToString("yyyy-MM-dd HH:mm:59"))  'add valdity
                                    End If
                                End If
                                axCZKEM1.RefreshData(iMachineNumber)
                            End If
                        Next
                        axCZKEM1.EnableDevice(iMachineNumber, True)
                        axCZKEM1.Disconnect()
                    Else
                        axCZKEM1.GetLastError(idwErrorCode)
                        failIP.Add(vpszIPAddress.ToString)
                        Continue For
                    End If



                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio1Eco" Then
                    vpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim()
                    XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    Application.DoEvents()
                    Dim adap, adap1 As SqlDataAdapter
                    Dim adapA, adapA1 As OleDbDataAdapter
                    Dim ds, ds1 As DataSet
                    'Dim commkey As Integer
                    commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                    Dim mOpenFlag As Boolean
                    Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

                    mMK8001Device = False
                    Dim si As SySystemInfo = New SySystemInfo
                    si.cSerialNum = New Byte((20) - 1) {}
                    si.cManuName = New Byte((24) - 1) {}
                    si.cDevName = New Byte((24) - 1) {}
                    si.cAlgVer = New Byte((16) - 1) {}
                    si.cFirmwareVer = New Byte((24) - 1) {}
                    Dim rret As Integer = 66
                    Try
                        rret = SyFunctions.CmdGetSystemInfo(si)
                    Catch ex As Exception

                    End Try

                    If (rret = CType(SyLastError.sleSuss, Int32)) Then
                        Dim sFirmwareVer As String = System.Text.Encoding.Default.GetString(si.cFirmwareVer)
                        If sFirmwareVer.Contains("MK8001/8002") Then
                            mMK8001Device = True
                        Else
                            mMK8001Device = False
                        End If
                    End If
                    Dim iFingerSize
                    If mMK8001Device Then
                        iFingerSize = 768
                    Else
                        iFingerSize = 512
                    End If
                    Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
                    Dim equNo As UInt32 = Convert.ToUInt32(LstMachineId)
                    Dim gEquNo As UInt32 = equNo
                    If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "T" Then
                        Dim netCfg As SyNetCfg
                        Dim pswd As UInt32 = 0
                        Dim tmpPswd As String = commkey
                        If (tmpPswd = "0") Then
                            tmpPswd = "FFFFFFFF"
                        End If
                        Try
                            pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                        Catch ex As Exception
                            'XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                            ret = CType(SyLastError.slePasswordError, Int32)
                            failIP.Add(vpszIPAddress.ToString)
                            Continue For
                        End Try
                        If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                            netCfg.mIsTCP = 1
                            netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                            netCfg.mIPAddr = New Byte((4) - 1) {}
                            Dim sArray() As String = vpszIPAddress.Split(Microsoft.VisualBasic.ChrW(46))
                            Dim j As Byte = 0
                            For Each i1 As String In sArray
                                Try
                                    netCfg.mIPAddr(j) = Convert.ToByte(i1.Trim)
                                Catch ex As System.Exception
                                    netCfg.mIPAddr(j) = 255
                                End Try
                                j = (j + 1)
                                If j > 3 Then
                                    Exit For
                                End If

                            Next
                        End If

                        Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                        Try
                            Marshal.StructureToPtr(netCfg, pnt, False)
                            '        If optWifiDevice.Checked Then
                            'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                            '        Else
                            ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                            '        End If
                            '                'TODO: Warning!!!, inline IF is not supported ?
                            '                chkTranceIO.Checked()
                            '0:
                            '                '
                        Finally
                            Marshal.FreeHGlobal(pnt)
                        End Try
                    ElseIf GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        Dim equtype As String = "Finger Module USB Device"
                        Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                        ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
                    End If
                    If (ret = CType(SyLastError.sleSuss, Int32)) Then
                        ret = SyFunctions.CmdTestConn2Device

                        If Common.servername = "Access" Then
                            If Common.con1.State <> System.Data.ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                        Else
                            If Common.con.State <> System.Data.ConnectionState.Open Then
                                Common.con.Open()
                            End If
                        End If


                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                'If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString.Trim() = "Bio1Eco" Then
                                XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString().Trim
                                Application.DoEvents()
                                Dim uie As SyUserInfoExt = New SyUserInfoExt
                                Dim t1 As Byte = 0, t2 = 0, t3 = 0, t5 = 0, kind = 0
                                uie.dwCardID = 0
                                uie.dwPws = 0
                                uie.sUserInfo.name = GridView2.GetRowCellDisplayText(rowHandleEmp, "EMPNAME").ToString.Trim()
                                uie.sUserInfo.cValidTime = New Byte((5) - 1) {}
                                uie.sUserInfo.MbIndex = New System.UInt16((5) - 1) {}
                                Dim sdwEnrollNumber
                                If IsNumeric(GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString().Trim) = True Then
                                    sdwEnrollNumber = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString().Trim)
                                Else
                                    sdwEnrollNumber = GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString().Trim
                                End If

                                uie.sUserInfo.wUserID = Convert.ToUInt16(sdwEnrollNumber)
                                uie.sUserInfo.cVerifyType = Convert.ToByte(0)
                                uie.sUserInfo.sUserType = CType(1, Byte) 'privilege
                                'If ToggleMakeAdmin.IsOn = True Then
                                uie.sUserInfo.sUserType = CType(0, Byte) 'privilege                                       
                                'Else
                                '    uie.sUserInfo.sUserType = CType(GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString().Trim, Byte) 'privilege                                       
                                'End If

                                Try
                                    uie.sUserInfo.cValidTime(0) = Convert.ToByte("")
                                Catch ex As Exception

                                End Try

                                Try
                                    uie.sUserInfo.cValidTime(1) = Convert.ToByte("")
                                Catch ex As Exception

                                End Try

                                Try
                                    uie.sUserInfo.cValidTime(2) = Convert.ToByte("")
                                Catch ex As Exception

                                End Try

                                Try
                                    uie.dwCardID = Convert.ToInt32(GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString().Trim)
                                Catch ex As Exception
                                End Try

                                Try
                                    uie.dwPws = Convert.ToInt32(GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString().Trim)
                                Catch ex As Exception
                                End Try

                                'Dim strEnrollData As String = GridView2.GetRowCellValue(rowHandleEmp, "Template").ToString().Trim                                  
                                'Dim ft As SyFingerTemplate = New SyFingerTemplate
                                'Dim ftEx As SyFingerTemplateEx = New SyFingerTemplateEx
                                'If (iFingerSize = 768) Then
                                '    ftEx.data = New Byte((768) - 1) {}
                                '    For n As Integer = 0 To 768 - 1
                                '        If (strEnrollData.Length >= (n + 1) * 2) Then
                                '            ftEx.data(n) = Convert.ToByte(strEnrollData.Substring(n * 2, 2), 16)
                                '        Else
                                '            ftEx.data(n) = 0
                                '        End If
                                '    Next
                                '    ret = SyFunctions.CmdSetUserFingerPrint(ftEx, CType(uie.sUserInfo.wUserID, System.UInt16), CType((1), System.UInt16))
                                'Else
                                '    ft.data = New Byte((512) - 1) {}
                                '    For n As Integer = 0 To 512 - 1
                                '        If (strEnrollData.Length >= (n + 1) * 2) Then
                                '            ft.data(n) = Convert.ToByte(strEnrollData.Substring(n * 2, 2), 16)
                                '        Else
                                '            ft.data(n) = 0
                                '        End If
                                '    Next
                                '    ret = SyFunctions.CmdSetUserFingerPrint(ft, CType(uie.sUserInfo.wUserID, System.UInt16), CType((1), System.UInt16))
                                'End If

                                ''t1 = CType(lstEnrollData.Items.Count, Byte)
                                t1 = CType(1, Byte)
                                t2 = 0
                                If (uie.dwPws > 0) Then
                                    t2 = 1
                                    kind = 2
                                End If

                                If (uie.dwCardID > 0) Then
                                    t3 = 1
                                    kind = 3
                                End If
                                t5 = 3
                                uie.sUserInfo.sEnrollStatus = CType((t1 _
                                            Or ((t2 * 16) _
                                            Or ((t3 * 32) _
                                            Or (t5 * 64)))), Byte)
                                ret = SyFunctions.CmdSetUserInfo(uie, uie.sUserInfo.wUserID)
                                If (ret = CType(SyLastError.sleSuss, Int32)) Then
                                    'lblMessage.Text = "SetEnrollData OK"
                                Else
                                    'lblMessage.Text = util.ErrorPrint(ret)
                                End If
                                'End If
                            End If
                        Next
                        'End loop for fingerprint
conclose:               If Common.servername = "Access" Then
                            If Common.con1.State <> System.Data.ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> System.Data.ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                        SyFunctions.CloseCom()
                        ' close com 
                        SyFunctions.CmdUnLockDevice()
                        SyFunctions.CloseCom()
                    Else
                        'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                        failIP.Add(vpszIPAddress.ToString)
                        Continue For
                    End If


                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "EF45" Then
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)
                    XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    Application.DoEvents()
                    Dim cn As Common = New Common
                    Dim _client As Client = Nothing
                    Dim connectOk As Boolean = False
                    Try
                        _client = cn.initClientEF45(vpszIPAddress)
                        connectOk = _client.StealConnect '_client.Connect()
                        If connectOk Then
                            ' loop for fingerprint
                            Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                            Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                            For x As Integer = 0 To selectedRowsEmp.Length - 1
                                Dim rowHandleEmp As Long = selectedRowsEmp(x)
                                If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                    XtraMasterTest.LabelControlStatus.Text = "Uploading User " & GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString().Trim
                                    Application.DoEvents()
                                    Dim sdwEnrollNumber
                                    If IsNumeric(GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString().Trim) = True Then
                                        sdwEnrollNumber = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString().Trim)
                                    Else
                                        sdwEnrollNumber = GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString().Trim
                                    End If

                                    Dim LeftEyeTemplate() As Byte
                                    Dim RightEyeTemplate() As Byte
                                    Dim subject As CMITech.UMXClient.Entities.Subject
                                    Dim subjectInComing As CMITech.UMXClient.Entities.Subject
                                    Dim userInfo As Entities.UserInfo = New Entities.UserInfo()
                                    Dim userInfoInComing As Entities.UserInfo = New Entities.UserInfo()
                                    Dim admin As Integer = 0
                                    Try
                                        subjectInComing = _client.GetSubject(sdwEnrollNumber) 'check for duplicate 
                                        userInfoInComing = _client.GetUserInfoByUUID(sdwEnrollNumber)
                                        admin = userInfoInComing.Admin
                                        'important to rite this part in both cases add and update 
                                        'becayse _client.GetSubject("9999") will overrite all values
                                        Try
                                            'Dim Template As CMITech.UMXClient.Entities.EnrolTemplate '= New CMITech.UMXClient.Entities.EnrolTemplate(Rs.Tables(0).Rows(0).Item("LeftEye"), Rs.Tables(0).Rows(0).Item("RightEye"))
                                            subject = New CMITech.UMXClient.Entities.Subject
                                            'Template = New CMITech.UMXClient.Entities.EnrolTemplate(Nothing, Nothing)
                                            subject.EnrolTemplate = subjectInComing.EnrolTemplate
                                            subject.AccessAllowed = True
                                            subject.LastName = GridView2.GetRowCellDisplayText(rowHandleEmp, "EMPNAME").ToString.Trim()
                                            subject.SubjectUID = sdwEnrollNumber
                                            subject.MatchUntil = ""
                                            subject.FirstName = ""
                                            subject.WiegandCustom = "Iamtestingnow"
                                            subject.WiegandCode = -1
                                            subject.WiegandFacility = -1
                                            subject.WiegandSite = -1
                                            _client.UpdateSubject(subject)  'update if exists
                                        Catch ex As Exception

                                        End Try
                                        Try
                                            userInfo.UUID = sdwEnrollNumber
                                            userInfo.Card = userInfoInComing.Card 'GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString().Trim ' /* Card CSN : 9A 99 1F 3E ...*/
                                            userInfo.Pin = userInfoInComing.Pin 'GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString().Trim
                                            userInfo.Admin = admin
                                            userInfo.GroupIndex = 0
                                            userInfo.ByPassCard = 0
                                            userInfo.Indivisual = 0
                                            userInfo.ThreeOutStatus = 0
                                            userInfo.ThreeOutAccessAllowed = 1
                                            _client.UpdateUserInfoByUUID(userInfo) 'update if exists
                                        Catch ex As Exception

                                        End Try
                                    Catch ex As Exception
                                        If ex.Message.ToString = "Subject " & sdwEnrollNumber & " does not exist" Then
                                            Try
                                                Dim Template As CMITech.UMXClient.Entities.EnrolTemplate '= New CMITech.UMXClient.Entities.EnrolTemplate(Rs.Tables(0).Rows(0).Item("LeftEye"), Rs.Tables(0).Rows(0).Item("RightEye"))
                                                subject = New CMITech.UMXClient.Entities.Subject
                                                Template = New CMITech.UMXClient.Entities.EnrolTemplate(Nothing, Nothing)
                                                subject.EnrolTemplate = Template

                                                subject.AccessAllowed = True
                                                subject.LastName = GridView2.GetRowCellDisplayText(rowHandleEmp, "EMPNAME").ToString.Trim()
                                                subject.SubjectUID = sdwEnrollNumber
                                                subject.MatchUntil = ""
                                                subject.FirstName = ""
                                                subject.WiegandCustom = "Iamtestingnow"
                                                subject.WiegandCode = -1
                                                subject.WiegandFacility = -1
                                                subject.WiegandSite = -1
                                                _client.AddSubject(subject) 'add if not exists
                                            Catch ex1 As Exception
                                            End Try
                                            Try
                                                userInfo.UUID = sdwEnrollNumber
                                                userInfo.Card = "" 'GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString().Trim ' /* Card CSN : 9A 99 1F 3E ...*/
                                                userInfo.Pin = "" 'GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString().Trim
                                                userInfo.Admin = admin
                                                userInfo.GroupIndex = 0
                                                userInfo.ByPassCard = 0
                                                userInfo.Indivisual = 0
                                                userInfo.ThreeOutStatus = 0
                                                userInfo.ThreeOutAccessAllowed = 1
                                                _client.AddUserInfo(userInfo) 'add if not exists 'to save user info in device. needed while adding new user
                                            Catch ex1 As Exception

                                            End Try
                                        End If
                                    End Try
                                End If
                            Next
                            'End loop for fingerprint
                            _client.Disconnect()
                        Else
                            failIP.Add(vpszIPAddress.ToString)
                            Continue For
                        End If
                    Catch ex As Exception
                        Continue For
                    End Try
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "TF-01" Then
                    sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    Dim bConn As Boolean = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 7881 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)
                    vpszNetPort = CLng("5005")
                    vpszNetPassword = CLng("0")
                    vnTimeOut = CLng("5000")
                    XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    Application.DoEvents()
                    vnProtocolType = 0 'PROTOCOL_TCPIP
                    Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                    Dim bRet As Boolean
                    bRet = AxFP_CLOCK1.SetIPAddress(vpszIPAddress, vpszNetPort, vpszNetPassword)
                    Me.AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
                    If Not bRet Then
                        Continue For
                    End If
                    bRet = AxFP_CLOCK1.EnableDevice(vnMachineNumber, False)
                    If Not bRet Then
                        AxFP_CLOCK1.CloseCommPort()
                        Continue For
                    End If
                    If bRet Then
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Integer = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim EnrollNumber As String = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim)
                                Dim FingerNumber As Long = 0 'GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
                                Dim Uname As String = GridView2.GetRowCellValue(rowHandleEmp, "EMPNAME").ToString.Trim

                                XtraMasterTest.LabelControlStatus.Text = "Uploading User " & EnrollNumber
                                Application.DoEvents()

                                Dim vStrEnrollNumber As String = Trim(EnrollNumber)
                                Dim vEnrollNumber As Long = Convert.ToUInt64(vStrEnrollNumber)
                                'If F9.FK_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
                                '    vRet = F9.FK_SetUserName_StringID(vRet, vStrEnrollNumber, Trim(Uname))
                                'Else
                                '    vRet = F9.FK_SetUserName(vRet, vEnrollNumber, Trim(Uname))
                                'End If
                                Dim obj As Object
                                obj = New System.Runtime.InteropServices.VariantWrapper(Uname)
                                vStrEnrollNumber = vEnrollNumber
                                bRet = AxFP_CLOCK1.SetUserName(0, vnMachineNumber, vStrEnrollNumber, vnMachineNumber, obj)
                                Dim dwWeekTimeID As Integer = 0
                                Dim dwGroupID As Integer = 0

                                Dim dtSart As DateTime = DateEditStart.DateTime
                                Dim dtEnd As DateTime = DateEditEnd.DateTime
                                If CheckEdit1.Checked = True Then
                                    If CheckFrmDt.Checked = True Then

                                    Else
                                        'EnrollNumber = Convert.ToDouble(EnrollNumber)
                                        Dim s As String = "select ValidityStartDate, ValidityEndDate from TblEmployee where PRESENTCARDNO = '" & GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim & "'"
                                        Dim adap As SqlDataAdapter
                                        Dim adapA As OleDbDataAdapter
                                        Dim ds As DataSet = New DataSet
                                        If Common.servername = "Access" Then
                                            adapA = New OleDbDataAdapter(s, Common.con1)
                                            adapA.Fill(ds)
                                        Else
                                            adap = New SqlDataAdapter(s, Common.con)
                                            adap.Fill(ds)
                                        End If
                                        Try
                                            dtSart = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ValidityStartDate").ToString.Trim)
                                            dtEnd = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ValidityEndDate").ToString.Trim)

                                        Catch ex As Exception

                                        End Try
                                    End If
                                    bRet = AxFP_CLOCK1.SetUserCtrl(vnMachineNumber, vStrEnrollNumber, dwWeekTimeID, dwGroupID, dtSart.Year, dtSart.Month, dtSart.Day, dtEnd.Year, dtEnd.Month, dtEnd.Day)


                                End If

                                'End finger validity
                                'cmdSetAllEnrollData_bio_All(vnMachineNumber, EnrollNumber, FingerNumber, Uname)
                            End If
                        Next x
                        AxFP_CLOCK1.EnableDevice(vnMachineNumber, True)
                        AxFP_CLOCK1.CloseCommPort()
                    Else
                        'MsgBox("Device No: " & LstMachineId & " Not connected..")
                        failIP.Add(vpszIPAddress.ToString)
                        Continue For
                        'XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                    End If
                    'FK623Attend.Disconnect
                    AxFP_CLOCK1.EnableDevice(vnMachineNumber, True)
                    AxFP_CLOCK1.CloseCommPort()
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "F9" Then
                    sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    Dim bConn As Boolean = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 7881 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)
                    vpszNetPort = CLng("5005")
                    vpszNetPassword = CLng("0")
                    vnTimeOut = CLng("5000")
                    XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    Application.DoEvents()
                    vnProtocolType = 0 'PROTOCOL_TCPIP
                    Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                    'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        vRet = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
                    Else
                        vpszIPAddress = Trim(lpszIPAddress)
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vRet = F9.FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        nCommHandleIndex = vRet
                    End If
                    If vRet > 0 Then
                        bConn = True
                    Else
                        bConn = False
                    End If
                    If bConn Then
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Integer = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim EnrollNumber As String = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim)
                                Dim FingerNumber As Long = 0 'GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
                                Dim Uname As String = GridView2.GetRowCellValue(rowHandleEmp, "EMPNAME").ToString.Trim

                                XtraMasterTest.LabelControlStatus.Text = "Uploading User " & EnrollNumber
                                Application.DoEvents()

                                Dim vStrEnrollNumber As String = Trim(EnrollNumber)
                                Dim vEnrollNumber As Long = Convert.ToUInt64(vStrEnrollNumber)
                                If F9.FK_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
                                    vRet = F9.FK_SetUserName_StringID(vRet, vStrEnrollNumber, Trim(Uname))
                                Else
                                    vRet = F9.FK_SetUserName(vRet, vEnrollNumber, Trim(Uname))
                                End If

                                'finger validity
                                vRet = F9.FK_EnableDevice(nCommHandleIndex, 0)
                                If (vRet <> CType(RUN_SUCCESS, Integer)) Then
                                    Continue For
                                End If
                                Dim vSize As Integer = (20 + 264)
                                Dim bytExtCmd_USERDOORINFO() As Byte = New Byte((vSize) - 1) {}
                                Dim vExtCmd_USERDOORINFO As Common.ExtCmd_USERDOORINFO = New Common.ExtCmd_USERDOORINFO
                                vStrEnrollNumber = vEnrollNumber.ToString.Trim
                                'vEnrollNumber = FKAttendDLL.GetInt(vStrEnrollNumber)
                                vExtCmd_USERDOORINFO.Init("ECMD_SetUserDoorInfo", vEnrollNumber)
                                If CheckEdit1.Checked = True Then
                                    If CheckFrmDt.Checked = True Then
                                        vExtCmd_USERDOORINFO.StartYear = CType(DateEditStart.DateTime.ToString("yyyy"), Short)
                                        vExtCmd_USERDOORINFO.StartMonth = CType(DateEditStart.DateTime.ToString("MM"), Byte)
                                        vExtCmd_USERDOORINFO.StartDay = CType(DateEditStart.DateTime.ToString("dd"), Byte)
                                        vExtCmd_USERDOORINFO.EndYear = CType(DateEditEnd.DateTime.ToString("yyyy"), Short)
                                        vExtCmd_USERDOORINFO.EndMonth = CType(DateEditEnd.DateTime.ToString("MM"), Byte)
                                        vExtCmd_USERDOORINFO.EndDay = CType(DateEditEnd.DateTime.ToString("dd"), Byte)
                                    Else
                                        'EnrollNumber = Convert.ToDouble(EnrollNumber)
                                        Dim s As String = "select ValidityStartDate, ValidityEndDate from TblEmployee where PRESENTCARDNO = '" & GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim & "'"
                                        Dim adap As SqlDataAdapter
                                        Dim adapA As OleDbDataAdapter
                                        Dim ds As DataSet = New DataSet
                                        If Common.servername = "Access" Then
                                            adapA = New OleDbDataAdapter(s, Common.con1)
                                            adapA.Fill(ds)
                                        Else
                                            adap = New SqlDataAdapter(s, Common.con)
                                            adap.Fill(ds)
                                        End If
                                        Try
                                            Dim startdate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ValidityStartDate").ToString.Trim)
                                            Dim enddate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ValidityEndDate").ToString.Trim)
                                            vExtCmd_USERDOORINFO.StartYear = CType(startdate.ToString("yyyy"), Short)
                                            vExtCmd_USERDOORINFO.StartMonth = CType(startdate.ToString("MM"), Byte)
                                            vExtCmd_USERDOORINFO.StartDay = CType(startdate.ToString("dd"), Byte)
                                            vExtCmd_USERDOORINFO.EndYear = CType(enddate.ToString("yyyy"), Short)
                                            vExtCmd_USERDOORINFO.EndMonth = CType(enddate.ToString("MM"), Byte)
                                            vExtCmd_USERDOORINFO.EndDay = CType(enddate.ToString("dd"), Byte)
                                        Catch ex As Exception

                                        End Try
                                    End If
                                End If
                                ConvertStructureToByteArray(vExtCmd_USERDOORINFO, bytExtCmd_USERDOORINFO)
                                vRet = F9.FK_ExtCommand(nCommHandleIndex, bytExtCmd_USERDOORINFO)
                                vRet = F9.FK_EnableDevice(nCommHandleIndex, 1)
                                'End finger validity
                                'cmdSetAllEnrollData_bio_All(vnMachineNumber, EnrollNumber, FingerNumber, Uname)
                            End If
                        Next x
                    Else
                        'MsgBox("Device No: " & LstMachineId & " Not connected..")
                        failIP.Add(vpszIPAddress.ToString)
                        Continue For
                        'XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                    End If
                    'FK623Attend.Disconnect
                    F9.FK_DisConnect(nCommHandleIndex)
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ATF686n" Then
                    sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    Dim bConn As Boolean = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 7881 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)
                    vpszNetPort = CLng("5005")
                    vpszNetPassword = CLng("0")
                    vnTimeOut = CLng("5000")
                    XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    Application.DoEvents()
                    vnProtocolType = 0 'PROTOCOL_TCPIP
                    Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
                    'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    'If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                    '    vRet = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
                    'Else
                    vpszIPAddress = Trim(lpszIPAddress)
                    vRet = atf686n.ConnectNet(vpszIPAddress)
                    nCommHandleIndex = vRet
                    'End If
                    If vRet > 0 Then
                        bConn = True
                    Else
                        bConn = False
                    End If
                    If bConn Then
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Integer = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim EnrollNumber As String = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim)
                                Dim FingerNumber As Long = 0 'GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
                                Dim Uname As String = GridView2.GetRowCellValue(rowHandleEmp, "EMPNAME").ToString.Trim

                                XtraMasterTest.LabelControlStatus.Text = "Uploading User " & EnrollNumber
                                Application.DoEvents()

                                Dim vStrEnrollNumber As String = Trim(EnrollNumber)
                                Dim vEnrollNumber As Long = Convert.ToUInt64(vStrEnrollNumber)
                                If atf686n.ST_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
                                    vRet = atf686n.ST_SetUserName_SID(vRet, vStrEnrollNumber, Trim(Uname))
                                Else
                                    'vRet = F9.FK_SetUserName(vRet, vEnrollNumber, Trim(Uname))
                                End If

                                'finger validity
                                vRet = atf686n.ST_EnableDevice(nCommHandleIndex, 0)
                                If (vRet <> CType(RUN_SUCCESS, Integer)) Then
                                    Continue For
                                End If
                                Dim vSize As Integer = (20 + 264)
                                Dim bytExtCmd_USERDOORINFO() As Byte = New Byte((vSize) - 1) {}
                                Dim vExtCmd_USERDOORINFO As Common.ExtCmd_USERDOORINFO = New Common.ExtCmd_USERDOORINFO
                                vStrEnrollNumber = vEnrollNumber.ToString.Trim
                                'vEnrollNumber = FKAttendDLL.GetInt(vStrEnrollNumber)
                                vExtCmd_USERDOORINFO.Init("ECMD_SetUserDoorInfo", vEnrollNumber)
                                If CheckEdit1.Checked = True Then
                                    If CheckFrmDt.Checked = True Then
                                        vExtCmd_USERDOORINFO.StartYear = CType(DateEditStart.DateTime.ToString("yyyy"), Short)
                                        vExtCmd_USERDOORINFO.StartMonth = CType(DateEditStart.DateTime.ToString("MM"), Byte)
                                        vExtCmd_USERDOORINFO.StartDay = CType(DateEditStart.DateTime.ToString("dd"), Byte)
                                        vExtCmd_USERDOORINFO.EndYear = CType(DateEditEnd.DateTime.ToString("yyyy"), Short)
                                        vExtCmd_USERDOORINFO.EndMonth = CType(DateEditEnd.DateTime.ToString("MM"), Byte)
                                        vExtCmd_USERDOORINFO.EndDay = CType(DateEditEnd.DateTime.ToString("dd"), Byte)
                                    Else
                                        'EnrollNumber = Convert.ToDouble(EnrollNumber)
                                        Dim s As String = "select ValidityStartDate, ValidityEndDate from TblEmployee where PRESENTCARDNO = '" & GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim & "'"
                                        Dim adap As SqlDataAdapter
                                        Dim adapA As OleDbDataAdapter
                                        Dim ds As DataSet = New DataSet
                                        If Common.servername = "Access" Then
                                            adapA = New OleDbDataAdapter(s, Common.con1)
                                            adapA.Fill(ds)
                                        Else
                                            adap = New SqlDataAdapter(s, Common.con)
                                            adap.Fill(ds)
                                        End If
                                        Try
                                            Dim startdate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ValidityStartDate").ToString.Trim)
                                            Dim enddate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ValidityEndDate").ToString.Trim)
                                            vExtCmd_USERDOORINFO.StartYear = CType(startdate.ToString("yyyy"), Short)
                                            vExtCmd_USERDOORINFO.StartMonth = CType(startdate.ToString("MM"), Byte)
                                            vExtCmd_USERDOORINFO.StartDay = CType(startdate.ToString("dd"), Byte)
                                            vExtCmd_USERDOORINFO.EndYear = CType(enddate.ToString("yyyy"), Short)
                                            vExtCmd_USERDOORINFO.EndMonth = CType(enddate.ToString("MM"), Byte)
                                            vExtCmd_USERDOORINFO.EndDay = CType(enddate.ToString("dd"), Byte)
                                        Catch ex As Exception

                                        End Try
                                    End If
                                End If
                                ConvertStructureToByteArray(vExtCmd_USERDOORINFO, bytExtCmd_USERDOORINFO)
                                'vRet = atf686n.ST_ExtCommand(nCommHandleIndex, bytExtCmd_USERDOORINFO)
                                vRet = atf686n.ST_EnableDevice(nCommHandleIndex, 1)
                                'End finger validity
                                'cmdSetAllEnrollData_bio_All(vnMachineNumber, EnrollNumber, FingerNumber, Uname)
                            End If
                        Next x
                    Else
                        'MsgBox("Device No: " & LstMachineId & " Not connected..")
                        failIP.Add(vpszIPAddress.ToString)
                        Continue For
                        'XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                    End If
                    'FK623Attend.Disconnect
                    atf686n.ST_DisConnect(nCommHandleIndex)
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK Controller" Then
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim
                    Dim ConnectT = "protocol=TCP,ipaddress=" & lpszIPAddress.ToString.Trim & ",port=4370,timeout=2000,passwd="
                    Dim Data As String = ""
                    Dim Data1 As String = ""
                    Dim ret As Integer = 0
                    Dim datafile As String = System.Environment.CurrentDirectory & "\Data\Enroll.txt"
                    h = IntPtr.Zero
                    If IntPtr.Zero = h Then
                        h = ZKController.Connect(ConnectT)
                    End If
                    If h <> IntPtr.Zero Then

                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            Dim rowHandleEmp As Integer = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Try

                                    Dim EnrollNumber As String = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "PRESENTCARDNO").ToString.Trim)
                                    Dim PCard As String = GridView2.GetRowCellValue(rowHandleEmp, "MachineCard").ToString.Trim

                                    XtraMasterTest.LabelControlStatus.Text = "Uploading User " & EnrollNumber
                                    Application.DoEvents()

                                    Data = "Pin=" & EnrollNumber.Trim & vbTab & "CardNo=" & PCard.Trim() & vbTab & "Password=1"
                                    'Data = Data.Replace(" ", "")
                                    Try
                                        'insert in text file

                                        Dim fs As FileStream = New FileStream(datafile, FileMode.OpenOrCreate, FileAccess.Write)
                                        Dim sw As StreamWriter = New StreamWriter(fs)
                                        'find the end of the underlying filestream
                                        sw.BaseStream.Seek(0, SeekOrigin.End)
                                        sw.WriteLine(Data.Trim())
                                        sw.Flush()
                                        sw.Close()
                                        'end insert in text file
                                    Catch ex As Exception

                                    End Try

                                    ret = ZKController.SetDeviceData(h, "user", Data, "")

                                    ' MsgBox("user " & ret & " " & ZKController.PullLastError.ToString)
                                    Data1 = "Pin=" & EnrollNumber.Trim & vbTab & "AuthorizeTimezoneId=1" & vbTab & "AuthorizeDoorId=1" & vbTab & "Pin=" & EnrollNumber.Trim & vbTab & "AuthorizeTimezoneId=1" & vbTab & "AuthorizeDoorId=3"
                                    'Data1 = Data1.Replace(" ", "")
                                    Try
                                        'insert in text file

                                        Dim fs As FileStream = New FileStream(datafile, FileMode.OpenOrCreate, FileAccess.Write)
                                        Dim sw As StreamWriter = New StreamWriter(fs)
                                        'find the end of the underlying filestream
                                        sw.BaseStream.Seek(0, SeekOrigin.End)
                                        sw.WriteLine(Data1.Trim())
                                        sw.Flush()
                                        sw.Close()
                                        'end insert in text file
                                    Catch ex As Exception

                                    End Try
                                    ret = ZKController.SetDeviceData(h, "userauthorize", Data1, "")
                                    ' MsgBox("userauthorize " & ret)
                                Catch ex As Exception
                                    'MessageBox.Show(ex.Message)
                                    Continue For
                                End Try

                            End If

                        Next
                        If IntPtr.Zero = h Then
                            ZKController.Disconnect(ConnectT)
                        End If
                    Else
                        failIP.Add(lpszIPAddress.ToString)
                        Continue For
                    End If
                End If
            End If
        Next
        Me.Cursor = Cursors.Default
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()

        Dim failedIpArr() As String = failIP.ToArray
        Dim failIpStr As String = ""
        If failedIpArr.Length > 0 Then
            For i As Integer = 0 To failedIpArr.Length - 1
                failIpStr = failIpStr & "," & failedIpArr(i)
            Next
            failIpStr = failIpStr.TrimStart(",")
            XtraMessageBox.Show(ulf, "<size=10>" & failIpStr & " failed to download data</size>", "<size=9>Failed</size>")
        Else
            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Success</size>")
        End If
        'XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
    End Sub
    Private Sub GetUserWeekPassTimeValue(ByVal EnrollNumber As String, ByRef aUserWeekPassTime As USER_WEEK_PASS_TIME)
        aUserWeekPassTime.UserId = EnrollNumber 'GetInt(txtEnrollNumber.Text)
        aUserWeekPassTime.WeekPassTime(0) = CByte(GetInt("1"))
        aUserWeekPassTime.WeekPassTime(1) = CByte(GetInt("1"))
        aUserWeekPassTime.WeekPassTime(2) = CByte(GetInt("1"))
        aUserWeekPassTime.WeekPassTime(3) = CByte(GetInt("1"))
        aUserWeekPassTime.WeekPassTime(4) = CByte(GetInt("1"))
        aUserWeekPassTime.WeekPassTime(5) = CByte(GetInt("1"))
        aUserWeekPassTime.WeekPassTime(6) = CByte(GetInt("1"))
    End Sub
    '    Private Sub cmdSetAllEnrollData_bio_All(mMachine_ID As Long, mUser_ID As String, FpNo As Long, Uname As String)
    '        Dim vEnrollNumber As Long
    '        Dim vEMachineNumber As Long
    '        Dim vFingerNumber As Long
    '        Dim vPrivilege As Long
    '        'Dim vEnable As Long
    '        Dim vFlag As Boolean
    '        Dim vRet As Long
    '        Dim vnIsSuppoterd As Long
    '        'Dim RsFp As New DataSet 'ADODB.Recordset
    '        Dim vbytConvFpData() As Byte
    '        Dim vbytEnrollData() As Byte
    '        Dim vStrEnrollNumber As String
    '        ' Dim Verify As Long
    '        Dim vnResultCode As Long
    '        Dim verifymode As Long
    '        'Dim vnii As Long
    '        'Dim vnLen As Long
    '        'Dim Uname As String
    '        '    lstEnrollData.Clear
    '        '    vTitle = frmEnroll.Caption
    '        '    lblMessage.Caption = "Working..."
    '        '    DoEvents
    '        Dim mMachineNumber As String = mMachine_ID
    '        vFlag = False
    '        'RsFp = Cn.Execute(sSql)
    '        'If RsFp.Tables(0).Rows.Count > 0 Then
    '        'With RsFp
    '        'If RsFp.Tables(0).Rows.Count = 0 Then GoTo EEE
    '        vEMachineNumber = mMachine_ID 'RsFp.Tables(0).Rows(0).Item("EMachineNumber").ToString.Trim '!EMachineNumber
    '        vEnrollNumber = mUser_ID 'RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim '!EnrollNumber
    '        vFingerNumber = FpNo 'RsFp.Tables(0).Rows(0).Item("FingerNumber").ToString.Trim '!FingerNumber
    '        vPrivilege = 0 'RsFp.Tables(0).Rows(0).Item("privilege").ToString.Trim ' !privilege
    '        'verifymode = IIf(IsNull(!verifymode), 0, !verifymode)
    '        'If RsFp.Tables(0).Rows(0).Item("verifymode").ToString.Trim = "" Then
    '        verifymode = 0

    '        'ZeroMemory(mbytEnrollData(0), UBound(mbytEnrollData) + 1)
    '        'If vFingerNumber = BACKUP_PSW Or vFingerNumber = BACKUP_CARD Then
    '        '    vbytEnrollData = RsFp.Tables(0).Rows(0).Item("Template_Tw") 'System.Text.Encoding.Unicode.GetBytes(RsFp.Tables(0).Rows(0).Item("Template_Tw").ToString.Trim) '.Fields("FPdata").Value
    '        '    'Array.Copy(vbytEnrollData, mbytEnrollData, PWD_DATA_SIZE)
    '        '    Array.Copy(vbytEnrollData, mbytEnrollData, PWD_DATA_SIZE)
    '        'ElseIf vFingerNumber >= BACKUP_FP_0 And vFingerNumber <= BACKUP_FP_9 Then
    '        '    vbytConvFpData = RsFp.Tables(0).Rows(0).Item("Template_Tw")
    '        '    ReDim vbytEnrollData(FP_DATA_SIZE - 1)
    '        '    ConvFpDataAfterReadFromDbForCompatibility(vbytConvFpData, vbytEnrollData)
    '        '    Array.Copy(vbytEnrollData, mbytEnrollData, FP_DATA_SIZE)
    '        'ElseIf vFingerNumber = BACKUP_FACE Then
    '        '    vbytEnrollData = RsFp.Tables(0).Rows(0).Item("Template_Tw") 'System.Text.Encoding.Unicode.GetBytes(RsFp.Tables(0).Rows(0).Item("Template_Tw").ToString.Trim) '.Fields("FPdata").Value
    '        '    Array.Copy(vbytEnrollData, mbytEnrollData, FACE_DATA_SIZE)
    '        'ElseIf vFingerNumber = BACKUP_VEIN_0 Then
    '        '    vbytEnrollData = RsFp.Tables(0).Rows(0).Item("Template_Tw") 'System.Text.Encoding.Unicode.GetBytes(RsFp.Tables(0).Rows(0).Item("Template_Tw").ToString.Trim) ''.Fields("FPdata").Value
    '        '    Array.Copy(vbytEnrollData, mbytEnrollData, VEIN_DATA_SIZE)
    '        'End If

    '        vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
    '        If vnResultCode <> RUN_SUCCESS Then
    '            Exit Sub
    '        End If

    '        FK_IsSupportedEnrollData(mnCommHandleIndex, vFingerNumber, vnIsSuppoterd)
    '        If vnIsSuppoterd = 0 Then
    '            XtraMessageBox.Show(ulf, "<size=10>Template Not supported</size>", "<size=9>Information</size>")
    '            vRet = FK_EnableDevice(mnCommHandleIndex, 1)
    '            GoTo EEE
    '        End If

    '        vnResultCode = FK_PutEnrollData(mnCommHandleIndex, _
    '                                              vEnrollNumber, _
    '                                              vFingerNumber, _
    '                                              vPrivilege, _
    '                                              mbytEnrollData, _
    '                                              mlngPasswordData)
    '        If vnResultCode <> RUN_SUCCESS Then
    '            GoTo LLL
    '        End If

    '        If vnResultCode = RUN_SUCCESS Then
    '            'lblMessage.Caption = "Saving..."
    '            'DoEvents
    '            vnResultCode = FK_SaveEnrollData(mnCommHandleIndex)
    '            If vnResultCode = RUN_SUCCESS Then
    '                'lblMessage.Caption = "SetAllEnrollData OK"
    '            Else
    '                'lblMessage.Caption = ReturnResultPrint(vnResultCode)
    '            End If
    '        Else
    '            'lblMessage.Caption = vStr & " : " & ReturnResultPrint(vnResultCode)
    '        End If
    '        vnResultCode = 0
    '        'vnResultCode = FK623Attend.SetUserName(vEnrollNumber, Trim(!UserName))
    '        'Uname = Uname 'RsFp.Tables(0).Rows(0).Item("Username") 'Trim(!Username)
    '        vStrEnrollNumber = vEnrollNumber
    '        If FK_GetIsSupportStringID(mnCommHandleIndex) = RUN_SUCCESS Then
    '            vnResultCode = FK_SetUserName_StringID(mnCommHandleIndex, vStrEnrollNumber, Uname)
    '        Else
    '            vnResultCode = FK_SetUserName(mnCommHandleIndex, vEnrollNumber, Uname)
    '        End If
    'LLL:
    '        'End If
    'EEE:
    '        FK_EnableDevice(mnCommHandleIndex, 1)
    '    End Sub
    Private Sub CheckEdit1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdit1.CheckedChanged
        If CheckFrmDt.Checked = True Then
            PanelControl1.Enabled = True
            If Common.IsNepali = "Y" Then
                If CheckEdit1.Checked = True Then
                    ComboNepaliVStartDate.Enabled = True
                    ComboNepaliVStartMonth.Enabled = True
                    ComboNepaliVStartYear.Enabled = True

                    ComboNepaliVEndDate.Enabled = True
                    ComboNepaliVEndMonth.Enabled = True
                    ComboNepaliVEndYear.Enabled = True
                Else
                    ComboNepaliVStartDate.Enabled = False
                    ComboNepaliVStartMonth.Enabled = False
                    ComboNepaliVStartYear.Enabled = False

                    ComboNepaliVEndDate.Enabled = False
                    ComboNepaliVEndMonth.Enabled = False
                    ComboNepaliVEndYear.Enabled = False
                End If
            Else
                If CheckEdit1.Checked = True Then
                    DateEditStart.Enabled = True
                    DateEditEnd.Enabled = True
                    'CheckCreate.Enabled = True
                Else
                    DateEditStart.Enabled = False
                    DateEditEnd.Enabled = False
                    'CheckCreate.Enabled = False
                End If
            End If
        Else
            PanelControl1.Enabled = False
        End If
    End Sub
    Private Sub CheckFrmDt_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckFrmDt.CheckedChanged
        If CheckFrmDt.Checked = True Then
            PanelControl1.Enabled = True
        Else
            PanelControl1.Enabled = False
        End If
    End Sub
End Class