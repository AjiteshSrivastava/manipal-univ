﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Public Class XtraVisitorOut
    Dim VISIT_NO As String
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim ulf As UserLookAndFeel
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.servername = "Access" Then
            Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        Else
            TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
    End Sub
    Private Sub XtraVisitorOut_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If Common.servername = "Access" Then
            Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine1
        Else
            Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine
        End If
        GridControl1.DataSource = Common.MachineNonAdmin

        VISIT_NO = XtraVisitorGrid.VISIT_NO
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "Select * from tblVisitorTransaction where VISIT_NO = '" & VISIT_NO & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        TextEditInTime.Text = ds.Tables(0).Rows(0).Item("INTIME").ToString.Trim
        TextEditPhone.Text = ds.Tables(0).Rows(0).Item("FV_No").ToString.Trim
        TextEditSlNo.Text = ds.Tables(0).Rows(0).Item("VISIT_NO").ToString.Trim
        If ds.Tables(0).Rows(0).Item("OUTTIME").ToString.Trim = "" Then
            TextEditOutTime.Text = Now.ToString("HH:mm")
        Else
            TextEditOutTime.Text = ds.Tables(0).Rows(0).Item("OUTTIME").ToString.Trim
        End If
        PopupContainerEdit1.EditValue = ""
        setDeviceGrid()
    End Sub

    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "Select * from tblVisitorTransaction where VISIT_NO = '" & VISIT_NO & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows(0).Item("OUTTIME").ToString.Trim <> "" And TextEditOutTime.Text.Trim <> ds.Tables(0).Rows(0).Item("OUTTIME").ToString.Trim Then
            If XtraMessageBox.Show(ulf, "<size=10>Out Time already Present. " & vbCrLf & " Are You sure to update existing Out Time.</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                SaveOutTime()
            End If
        Else
            SaveOutTime()
        End If
        If PopupContainerEdit1.EditValue.ToString <> "" Then
            deleteFromDevice()
        End If
        Me.Close()
    End Sub
    Private Sub SaveOutTime()
        Dim sSql As String = "update tblVisitorTransaction set OUTTIME = '" & TextEditOutTime.Text.Trim & "' where VISIT_NO='" & VISIT_NO & "'"
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
    End Sub
    Private Sub deleteFromDevice()
        Me.Cursor = Cursors.WaitCursor
        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        Dim vnMachineNumber As Integer
        Dim vnLicense As Integer
        Dim lpszIPAddress As String
        Dim vpszIPAddress As String
        Dim commkey As Integer
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                commkey = Convert.ToDouble(GridView1.GetRowCellValue(rowHandle, "commkey").ToString.Trim)
                'sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                Dim bConn As Boolean = False
                vnMachineNumber = LstMachineId
                vnLicense = 1261 '1789 '
                lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                vpszIPAddress = Trim(lpszIPAddress)
                vpszIPAddress = Trim(lpszIPAddress)

                Dim mEmp As String = TextEditPhone.Text
                Dim fingerNum As Integer = 12
                Dim axCZKEM1 As New zkemkeeper.CZKEM
                axCZKEM1.SetCommPassword(commkey)  'to check device commkey and db commkey matches
                Dim bIsConnected As Boolean = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                If bIsConnected = True Then
                    axCZKEM1.EnableDevice(vnMachineNumber, False)
                    Dim tmp As Boolean '= axCZKEM1.DelUserTmpExt(vnMachineNumber, mEmp, vnMachineNumber, fingerNum)
                    tmp = axCZKEM1.SSR_DeleteEnrollData(vnMachineNumber, mEmp, fingerNum)
                    If tmp = True Then
                        ' MsgBox "User has been deleted successfully", vbOKOnly
                    Else
                        XtraMessageBox.Show(ulf, "<size=10>Unable to delete the user " & mEmp & " from device " & vnMachineNumber & "</size>", "<size=9>Information</size>")
                    End If
                Else
                    XtraMessageBox.Show(ulf, "<size=10>Device No: " & vnMachineNumber & " Not connected..</size>", "<size=9>Information</size>")
                End If
                axCZKEM1.EnableDevice(vnMachineNumber, True)
                axCZKEM1.Disconnect()
            End If
        Next
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub PopupContainerEdit1_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit1.QueryPopUp
        Dim val As Object = PopupContainerEdit1.EditValue
        If (val Is Nothing) Then
            GridView1.ClearSelection()
        Else
            'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
                Dim rowHandle As Integer = GridView1.LocateByValue("ID_NO", text)
                GridView1.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        Dim selectedRows() As Integer = GridView1.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridView1.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("ID_NO"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub setDeviceGrid()
        Dim gridtblregisterselet As String
        gridtblregisterselet = "select * from tblMachine where DeviceType = 'ZK(TFT)' or  DeviceType = 'Bio-1Pro/ATF305Pro/ATF686Pro' "
        Dim WTDataTable As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        End If
        GridControl1.DataSource = WTDataTable
    End Sub
End Class