﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports DevExpress.LookAndFeel
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraEditors

Public Class XtraRosterUpload
    Dim APP As New Excel.Application
    Dim worksheet As Excel.Worksheet
    Dim workbook As Excel.Workbook
    Dim numRow As Integer
    Dim numCol As Integer
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim comclass As Common = New Common
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraEmployeeUpload_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        TextEdit1.Text = ""     
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If TextEdit1.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select file to upload</size>", "<size>Error</size>")
        Else
            UploadRoster()
        End If
    End Sub
    Private Sub TextEdit1_Click(sender As System.Object, e As System.EventArgs) Handles TextEdit1.Click
        Dim dlg As New OpenFileDialog()
        ' Filter by Office Files
        dlg.Filter = "Office Files|*.xlsx;*.xls;"
        dlg.ShowDialog()
        TextEdit1.Text = dlg.FileName
    End Sub
    Private Sub UploadRoster()
        Dim sSql As String
        Dim appWorldtmp As Excel.Application
        Dim wbWorld As Excel.Workbook
        Dim mPath
        Dim rsA As DataSet 'ADODB.Recordset
        Dim bCardRepl As Boolean
        Dim i As Integer
        Dim rowcnt As Integer
        Dim mShift As String
        'Dim rsEmp As DataSet, rsComp As DataSet, rsdept As DataSet, rscat As DataSet, RsGrade As DataSet, RsDivision As DataSet, rsShift As DataSet
        Dim mupdate As Integer, madded As Integer, mtotrec As Integer
        Dim intFile As Integer
        Dim mPaycode As String
        'Dim mTicket As String
        Dim mDate As DateTime
        'Dim mPunchTime As Object
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter

        madded = 0
        mupdate = 0
        mtotrec = 0
        rowcnt = 0
        Me.Cursor = Cursors.WaitCursor
        mPath = TextEdit1.Text.Trim
        Dim appWorld As Excel.Worksheet
        Try
            appWorldtmp = GetObject(, "Excel.Application")  'look for a running copy of Excel
            If Err.Number <> 0 Then 'If Excel is not running then
                appWorldtmp = CreateObject("Excel.Application") 'run it
            End If
            bCardRepl = False
            i = 0
            intFile = FreeFile()
            Err.Clear()
            wbWorld = appWorldtmp.Workbooks.Open(mPath)
            rowcnt = 2
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.ToString.Trim & "</size>", "<size=9>Error</size>")
            Exit Sub
        End Try
        Try
            appWorld = wbWorld.Worksheets(1)
        Catch ex As Exception
            appWorldtmp.Quit()
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>Sheet not found</size>", "<size=9>Success</size>")
            Exit Sub
        End Try
        Try
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next


            Do While appWorld.Cells(rowcnt, 1).value <> Nothing ' Do While appWorld.Cells(rowcnt, 1).ToString.Trim <> ""
                mtotrec = mtotrec + 3
                mPaycode = ""
                mPaycode = Trim(appWorld.Cells(rowcnt, 1).value)
                If Trim(mPaycode) = "" Then GoTo 1
                'mTicket = Trim(appWorld.Cells(rowcnt, 5).value)
                If Trim(mPaycode) = "" Then GoTo 1

                mDate = Convert.ToDateTime(Trim(appWorld.Cells(rowcnt, 2).value)).ToString("yyyy-MM-dd")
                mShift = Trim(appWorld.Cells(rowcnt, 3).value)

                Dim rsE As DataSet = New DataSet
                sSql = "select * from tblshiftmaster where SHIFT='" & mShift & "'"
                rsE = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsE)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsE)
                End If
                If rsE.Tables(0).Rows.Count = 0 Then
                    Continue Do
                End If

                rsE = New DataSet
                sSql = "select TblEmployee.paycode, TblEmployee.PRESENTCARDNO,tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.ID, TblEmployee.EMPNAME from tblEmployeeShiftMaster, TblEmployee, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId and tblemployee.payCode ='" & mPaycode & "'"
                rsE = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsE)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsE)
                End If
                If rsE.Tables(0).Rows.Count = 0 Then
                    Continue Do
                End If

                ' mPaycode = rsE.Tables(0).Rows(0)("Paycode").ToString().Trim()
                'Dim cellcount As Integer = 6
                'Dim frmDate As DateTime = Convert.ToDateTime(Trim(appWorld.Cells(3, 6).value)).ToString("yyyy-MM-dd")
                'Do While appWorld.Cells(rowcnt, cellcount).value <> Nothing
                '    mDate = Convert.ToDateTime(Trim(appWorld.Cells(3, cellcount).value)).ToString("yyyy-MM-dd")
                '    mShift = Trim(appWorld.Cells(rowcnt, cellcount).value)
                Dim OFFICEPUNCH As DateTime = mDate.ToString("yyyy-MM-dd") & " 00:00:00"
                XtraMasterTest.LabelControlStatus.Text = "Uploading Roster " & mPaycode & " " & OFFICEPUNCH.ToString("dd/MM/yyyy")
                Application.DoEvents()

                sSql = "select shift, StartTime,EndTime  from tblshiftmaster where Shift ='" & mShift & "' "
                rsA = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsA)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsA)
                End If
                'If rsA.Tables(0).Rows.Count = 0 And mShift <> "OFF" Then
                '    cellcount = cellcount + 1
                '    Continue Do
                'Else
                Try
                    If mShift = "OFF" Then
                        sSql = "update tbltimeregister set ManualRoster='Y', Status='WO', Wo_Value=1, leavevalue=0, PresentValue=0, AbsentValue=0, Holiday_Value=0, WBR_Flag='1', shift='OFF', shiftattended='OFF',FLAG='1' where dateoffice='" & mDate.ToString("yyyy-MM-dd") & "' AND  paycode='" & mPaycode & "'"
                    Else
                        sSql = "update tbltimeregister set  ManualRoster='Y', shiftstarttime='" & Convert.ToDateTime(rsA.Tables(0).Rows(0)("StartTime")).ToString("yyyy-MM-dd HH:mm:ss") & " ',shiftendtime='" & Convert.ToDateTime(rsA.Tables(0).Rows(0)("EndTime")).ToString("yyyy-MM-dd HH:mm:ss") & " ',  shift='" & mShift & "',shiftattended='" & mShift & "',FLAG='1',status='A'  where dateoffice='" & mDate.ToString("yyyy-MM-dd") & "' AND  paycode='" & mPaycode & "'"
                    End If

                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()

                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()

                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If

                Catch ex As Exception

                End Try
                'End If

                '    cellcount = cellcount + 1
                'Loop
                'Dim ToDate As DateTime = Convert.ToDateTime(Trim(appWorld.Cells(3, cellcount - 1).value)).ToString("yyyy-MM-dd")

                'If frmDate < Now Then
                If rsE.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                    comclass.Process_AllRTC(mDate.AddDays(-1), Now.Date, mPaycode, mPaycode, rsE.Tables(0).Rows(i).Item("Id"))
                Else
                    comclass.Process_AllnonRTC(mDate, Now.Date, mPaycode, mPaycode, rsE.Tables(0).Rows(i).Item("Id"))
                    If Common.EmpGrpArr(rsE.Tables(0).Rows(i).Item("Id")).SHIFTTYPE = "M" Then
                        comclass.Process_AllnonRTCMulti(mDate, Now.Date, mPaycode, mPaycode, rsE.Tables(0).Rows(i).Item("Id"))
                    End If
                End If
                'End If
1:
                rowcnt = rowcnt + 1
            Loop
            Common.LogPost("Roster Excel Upload")
        Catch ex As Exception
            appWorldtmp.Quit()
            Me.Cursor = Cursors.Default
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
            XtraMessageBox.Show(ulf, "<size=10> Paycode: " & mPaycode & vbCrLf & ex.Message.ToString.Trim & "</size>", "<size=9>Error</size>")
            Exit Sub
        End Try
        appWorldtmp.Quit()
        Me.Cursor = Cursors.Default
        For Each c As Control In Me.Controls
            c.Enabled = True
        Next
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        XtraMessageBox.Show(ulf, "<size=10>Upload Completed</size>", "<size=9>Success</size>")
        Me.Close()
    End Sub
End Class