﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Public Class XtraTimeOfficePolicyEdit
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub XtraTimeOfficePolicyEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        LabelControlETOP.Text = "End Time for Out punch(Next Date)" & vbCrLf & "for RTC Employee with Multiple Punch"
        LabelControlMarkWO.Text = "Mark WO as Absent when No of" & vbCrLf & "Present<No of Present for WO"
        setDefault()
        'Dim adap As SqlDataAdapter
        'Dim adapA As OleDbDataAdapter
        'Dim ds As DataSet = New DataSet
        'Dim sSql As String = "select max(SETUPID) from tblSetup "
        'If Common.servername = "Access" Then
        '    adapA = New OleDbDataAdapter(sSql, Common.con1)
        '    adapA.Fill(ds)
        'Else
        '    adap = New SqlDataAdapter(sSql, Common.con)
        '    adap.Fill(ds)
        'End If       
    End Sub
    Private Sub setDefault()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        'Dim sSql As String = "select * from tblSetup where SETUPID= (select MAX(SETUPID) from tblSetup)"
        Dim sSql As String = "Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblSetup )"
        If Common.servername = "Access" Then
            sSql = "Select * from tblSetUp where setupid =(Select MAX(val(Setupid)) from tblSetup )"
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        TxtSetRegNo.Text = (Convert.ToInt32(ds.Tables(0).Rows(0).Item("SETUPID").ToString) + 1)
        If CheckEditOT3.Checked = True Then
            LabelControl42.Enabled = True
            ToggleOTisAllowedInEarlyComing.Enabled = True
            GroupControl4.Enabled = True
        Else
            LabelControl42.Enabled = False
            ToggleOTisAllowedInEarlyComing.Enabled = False
            GroupControl4.Enabled = False
        End If
        TxtPermisLateArr.Text = ds.Tables(0).Rows(0).Item("PERMISLATEARR").ToString.Trim
        TxtPermisEarlyDpt.Text = ds.Tables(0).Rows(0).Item("PERMISEARLYDEP").ToString.Trim
        TxtPermisEarlyDpt.Text = ds.Tables(0).Rows(0).Item("PERMISEARLYDEP").ToString.Trim
        TxtDuplicateCheck.Text = ds.Tables(0).Rows(0).Item("DUPLICATECHECKMIN").ToString.Trim
        TxtInTimeForINPunch.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("S_END").ToString.Trim).ToString("HH:mm")
        TxtEndTimeForOutPunch.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("S_OUT").ToString.Trim).ToString("HH:mm")
        If ds.Tables(0).Rows(0).Item("ISOTOUTMINUSSHIFTENDTIME").ToString.Trim = "Y" Then
            CheckEditOT1.Checked = True
            CheckEditOT2.Checked = False
            CheckEditOT3.Checked = False
        ElseIf ds.Tables(0).Rows(0).Item("ISOTWRKGHRSMINUSSHIFTHRS").ToString.Trim = "Y" Then
            CheckEditOT1.Checked = False
            CheckEditOT2.Checked = True
            CheckEditOT3.Checked = False
        ElseIf ds.Tables(0).Rows(0).Item("ISOTEARLYCOMEPLUSLATEDEP").ToString.Trim = "Y" Then
            CheckEditOT1.Checked = False
            CheckEditOT2.Checked = False
            CheckEditOT3.Checked = True
        End If
        If ds.Tables(0).Rows(0).Item("ISOTEARLYCOMING").ToString.Trim = "Y" Then
            ToggleOTisAllowedInEarlyComing.IsOn = True
        Else
            ToggleOTisAllowedInEarlyComing.IsOn = False
        End If
        TxtOtLateDur.Text = ds.Tables(0).Rows(0).Item("OTLATECOMINGDUR").ToString.Trim
        TxtOtRestrictDur.Text = ds.Tables(0).Rows(0).Item("OTRESTRICTENDDUR").ToString.Trim

        TxtOtEarlyDur.Text = ds.Tables(0).Rows(0).Item("OTEARLYDUR").ToString.Trim
        TxtDeductOTinWO.Text = ds.Tables(0).Rows(0).Item("DEDUCTWOOT").ToString.Trim
        TxtDeductOTinHLD.Text = ds.Tables(0).Rows(0).Item("DEDUCTHOLIDAYOT").ToString.Trim
        If ds.Tables(0).Rows(0).Item("ISPRESENTONWOPRESENT").ToString.Trim = "Y" Then
            ToggleIsPresentOnWeekOff.IsOn = True
        Else
            ToggleIsPresentOnWeekOff.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("ISPRESENTONHLDPRESENT").ToString.Trim = "Y" Then
            ToggleIsPResentOnHLD.IsOn = True
        Else
            ToggleIsPResentOnHLD.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("ISAUTOABSENT").ToString.Trim = "Y" Then
            ToggleIsAutoAbsentAllowed.IsOn = True
        Else
            ToggleIsAutoAbsentAllowed.IsOn = False
        End If
        TxtPermisEarlyMinAutoShift.Text = ds.Tables(0).Rows(0).Item("AUTOSHIFT_LOW").ToString.Trim()
        TxtPermisLateMinAutoShift.Text = ds.Tables(0).Rows(0).Item("AUTOSHIFT_UP").ToString.Trim()

        If ds.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString.Trim = "Y" Then
            ToggleAutoShiftAllowed.IsOn = True
        Else
            ToggleAutoShiftAllowed.IsOn = False
        End If
       
        If ds.Tables(0).Rows(0).Item("WOINCLUDE").ToString.Trim = "Y" Then
            ToggleWeekOffIncludeInDUtyRoster.IsOn = True
        Else
            ToggleWeekOffIncludeInDUtyRoster.IsOn = False
        End If
       
        If ds.Tables(0).Rows(0).Item("IsOutWork").ToString.Trim = "Y" Then
            ToggleOutWorkAllowed.IsOn = True
        Else
            ToggleOutWorkAllowed.IsOn = False
        End If

        Dim NightShiftFourPunch As String
        If ToggleFourPunchNight.IsOn = True Then
            NightShiftFourPunch = "Y"
        Else
            NightShiftFourPunch = "N"
        End If
        Dim LinesPerPage As String = "58"  'hardcoded




        If ds.Tables(0).Rows(0).Item("OTROUND").ToString.Trim = "Y" Then
            ToggleRoundOverTime.IsOn = True
        Else
            ToggleRoundOverTime.IsOn = False
        End If
       
        TxtNoOffpresentOnweekOff.Text = ds.Tables(0).Rows(0).Item("PREWO").ToString.Trim
        If ds.Tables(0).Rows(0).Item("ISAWA").ToString.Trim = "Y" Then
            ToggleMarkAWAasAAA.IsOn = True
        Else
            ToggleMarkAWAasAAA.IsOn = False
        End If

        
        If ds.Tables(0).Rows(0).Item("ISPREWO").ToString.Trim = "Y" Then
            ToggleMarkWOasAbsent.IsOn = True
        Else
            ToggleMarkWOasAbsent.IsOn = False
        End If
        
        If ds.Tables(0).Rows(0).Item("LeaveFinancialYear").ToString.Trim = "Y" Then
            ToggleLeaveAsPerFinancialYear.IsOn = True
        Else
            ToggleLeaveAsPerFinancialYear.IsOn = False
        End If
       
        If ds.Tables(0).Rows(0).Item("Online").ToString.Trim = "Y" Then
            ToggleOnlineEvents.IsOn = True
        Else
            ToggleOnlineEvents.IsOn = False
        End If
       
        If ds.Tables(0).Rows(0).Item("AutoDownload").ToString.Trim = "Y" Then
            ToggleDownloadAtStartUp.IsOn = True
        Else
            ToggleDownloadAtStartUp.IsOn = False
        End If
       
        If ds.Tables(0).Rows(0).Item("MIS").ToString.Trim = "Y" Then
            ToggleMarkMisAsAbsent.IsOn = True
        Else
            ToggleMarkMisAsAbsent.IsOn = False
        End If
        
        If ds.Tables(0).Rows(0).Item("OwMinus").ToString.Trim = "Y" Then
            ToggleOutWorkMins.IsOn = True
        Else
            ToggleOutWorkMins.IsOn = False
        End If

        'CreateEmpPunchDwnld
        If ds.Tables(0).Rows(0).Item("StartRealTime").ToString.Trim = "Y" Then
            ToggleRealTime.IsOn = True
        Else
            ToggleRealTime.IsOn = False
        End If

        'Dim LateVerification As String
        TxtPwd.Text = ds.Tables(0).Rows(0).Item("SMSPassword").ToString.Trim
        TxtUserId.Text = ds.Tables(0).Rows(0).Item("SMSUserID").ToString.Trim
        TxtMsg.Text = ds.Tables(0).Rows(0).Item("SMSMessage").ToString.Trim
        TextEditBioPort.Text = ds.Tables(0).Rows(0).Item("BioPort").ToString.Trim
        TextEditZKPort.Text = ds.Tables(0).Rows(0).Item("ZKPort").ToString.Trim
        TextTWIR102Port.Text = ds.Tables(0).Rows(0).Item("TWIR102Port").ToString.Trim
        TextEditTimerDur.Text = ds.Tables(0).Rows(0).Item("TimerDur").ToString.Trim
        If ds.Tables(0).Rows(0).Item("Canteen").ToString.Trim = "Y" Then
            ToggleSwitchCanteen.IsOn = True
        ElseIf ds.Tables(0).Rows(0).Item("Canteen").ToString.Trim = "N" Then
            ToggleSwitchCanteen.IsOn = False
        End If
        If ds.Tables(0).Rows(0).Item("Visitor").ToString.Trim = "Y" Then
            ToggleSwitchVisitor.IsOn = True
        ElseIf ds.Tables(0).Rows(0).Item("Visitor").ToString.Trim = "N" Then
            ToggleSwitchVisitor.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("IsNepali").ToString.Trim = "Y" Then
            ToggleSwitchIsNepali.IsOn = True
        ElseIf ds.Tables(0).Rows(0).Item("IsNepali").ToString.Trim = "N" Then
            ToggleSwitchIsNepali.IsOn = False
        End If
        TextEditAutoDwnDur.Text = ds.Tables(0).Rows(0).Item("AutoDwnDur").ToString.Trim

    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        Dim SETUPID As String = TxtSetRegNo.Text.Trim
        Dim PERMISLATEARR As String = TxtPermisLateArr.Text.Trim
        Dim PERMISEARLYDEP As String = TxtPermisEarlyDpt.Text.Trim
        Dim DUPLICATECHECKMIN As String = TxtDuplicateCheck.Text.Trim
        'Dim ISOVERSTAY As String
        'If ToggleOverStayAllowed.IsOn = True Then
        '    ISOVERSTAY = "Y"
        'Else
        '    ISOVERSTAY = "N"
        'End If
        Dim S_END As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd ") & TxtInTimeForINPunch.Text.Trim & ":00")
        Dim S_OUT As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd ") & TxtEndTimeForOutPunch.Text.Trim & ":00")

        Dim ISOTOUTMINUSSHIFTENDTIME As String
        Dim ISOTWRKGHRSMINUSSHIFTHRS As String
        Dim ISOTEARLYCOMEPLUSLATEDEP As String
        If CheckEditOT1.Checked = True Then
            ISOTOUTMINUSSHIFTENDTIME = "Y"
            ISOTWRKGHRSMINUSSHIFTHRS = "N"
            ISOTEARLYCOMEPLUSLATEDEP = "N"
        ElseIf CheckEditOT2.Checked = True Then
            ISOTOUTMINUSSHIFTENDTIME = "N"
            ISOTWRKGHRSMINUSSHIFTHRS = "Y"
            ISOTEARLYCOMEPLUSLATEDEP = "N"
        ElseIf CheckEditOT3.Checked = True Then
            ISOTOUTMINUSSHIFTENDTIME = "N"
            ISOTWRKGHRSMINUSSHIFTHRS = "N"
            ISOTEARLYCOMEPLUSLATEDEP = "Y"
        End If
        'Dim ISOTALL As String
        'If ToggleOverTimeAllowed.IsOn = True Then
        '    ISOTALL = "Y"
        'Else
        '    ISOTALL = "N"
        'End If
        Dim ISOTEARLYCOMING As String
        If ToggleOTisAllowedInEarlyComing.IsOn = False Then
            ISOTEARLYCOMING = "Y"
        Else
            ISOTEARLYCOMING = "N"
        End If


        Dim StartRealTime As String
        If ToggleRealTime.IsOn = True Then
            'If XtraMessageBox.Show(ulf, "<size=10>'Create Emp While  Downloading Logs' Is On. This may slow down the download log process." & vbCrLf & " Are You Sure To Keep This On?</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
            '                 MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
            '    Exit Sub
            'End If
            StartRealTime = "Y"
        Else
            StartRealTime = "N"
        End If

        Dim OTEARLYDUR As String = TxtOtEarlyDur.Text.Trim
        Dim OTLATECOMINGDUR As String = TxtOtLateDur.Text.Trim
        Dim OTRESTRICTENDDUR As String = TxtOtRestrictDur.Text.Trim
        'MsgBox(TxtOtEarlyDur.Text.Trim)
        Dim OTEARLYDEPARTUREDUR As String = TxtOtEarlyDur.Text.Trim

        Dim DEDUCTWOOT As String = TxtDeductOTinWO.Text.Trim
        Dim DEDUCTHOLIDAYOT As String = TxtDeductOTinHLD.Text.Trim
        Dim ISPRESENTONWOPRESENT As String
        If ToggleIsPresentOnWeekOff.IsOn = True Then
            ISPRESENTONWOPRESENT = "Y"
        Else
            ISPRESENTONWOPRESENT = "N"
        End If
        Dim ISPRESENTONHLDPRESENT As String
        If ToggleIsPResentOnHLD.IsOn = True Then
            ISPRESENTONHLDPRESENT = "Y"
        Else
            ISPRESENTONHLDPRESENT = "N"
        End If
        'Dim MAXWRKDURATION As String = TxtMaxWorkDur.Text.Trim
        'Dim TIME1 As String = TxtMaxWorkingMin.Text.Trim
        'Dim CHECKLATE As String = TxtMaxLateArrivalMin.Text.Trim
        'Dim CHECKEARLY As String = TxtMaxEarlyDeparture.Text.Trim
        'Dim TIME As String = TxtPresentMarkingDur.Text.Trim
        'Dim HALF As String = TxtMaxWorkHourForHalfDay.Text.Trim
        'Dim SHORTTimeoffice As String = TxtMaxWorkHourForShortDay.Text.Trim
        Dim ISAUTOABSENT As String
        If ToggleIsAutoAbsentAllowed.IsOn = True Then
            ISAUTOABSENT = "Y"
        Else
            ISAUTOABSENT = "N"
        End If
        Dim AUTOSHIFT_LOW As String = TxtPermisEarlyMinAutoShift.Text.Trim
        Dim AUTOSHIFT_UP As String = TxtPermisLateMinAutoShift.Text.Trim
        Dim ISAUTOSHIFT As String
        If ToggleAutoShiftAllowed.IsOn = True Then
            ISAUTOSHIFT = "Y"
        Else
            ISAUTOSHIFT = "N"
        End If
        'Dim ISHALFDAY As String
        'If ToggleHalfDayMarking.IsOn = True Then
        '    ISHALFDAY = "Y"
        'Else
        '    ISHALFDAY = "N"
        'End If
        'Dim ISSHORT As String
        'If ToggleShortleaveMarking.IsOn = True Then
        '    ISSHORT = "Y"
        'Else
        '    ISSHORT = "N"
        'End If
        'Dim TWO As String
        Dim WOINCLUDE As String
        If ToggleWeekOffIncludeInDUtyRoster.IsOn = True Then
            WOINCLUDE = "Y"
        Else
            WOINCLUDE = "N"
        End If
        Dim IsOutWork As String
        If ToggleOutWorkAllowed.IsOn = True Then
            IsOutWork = "Y"
        Else
            IsOutWork = "N"
        End If
        Dim NightShiftFourPunch As String
        If ToggleFourPunchNight.IsOn = True Then
            NightShiftFourPunch = "Y"
        Else
            NightShiftFourPunch = "N"
        End If
        Dim LinesPerPage As String = "58"  'hardcoded
        'Dim SkipAfterDepartment As String
        'If ToggleSkipPageOnDept.IsOn = True Then
        '    SkipAfterDepartment = "Y"
        'Else
        '    SkipAfterDepartment = "N"
        'End If
        'Dim meals_rate As String
        'Dim INOUT As String
        'Dim SMART As String
        'Dim ISHELP As String
        'Dim ATT_ACC As String
        Dim OTROUND As String
        If ToggleRoundOverTime.IsOn = True Then
            OTROUND = "Y"
        Else
            OTROUND = "N"
        End If
        Dim PREWO As String = TxtNoOffpresentOnweekOff.Text.Trim
        Dim ISAWA As String
        If ToggleMarkAWAasAAA.IsOn = True Then
            ISAWA = "Y"
        Else
            ISAWA = "N"
        End If
        Dim ISPREWO As String
        If ToggleMarkWOasAbsent.IsOn = True Then
            ISPREWO = "Y"
        Else
            ISPREWO = "N"
        End If
        Dim LeaveFinancialYear As String
        If ToggleLeaveAsPerFinancialYear.IsOn = True Then
            LeaveFinancialYear = "Y"
        Else
            LeaveFinancialYear = "N"
        End If
        Dim Online As String
        If ToggleOnlineEvents.IsOn = True Then
            Online = "Y"
        Else
            Online = "N"
        End If
        Dim AutoDownload As String
        If ToggleDownloadAtStartUp.IsOn = True Then
            AutoDownload = "Y"
        Else
            AutoDownload = "N"
        End If
        Dim MIS As String
        If ToggleMarkMisAsAbsent.IsOn = True Then
            MIS = "Y"
        Else
            MIS = "N"
        End If
        Dim OwMinus As String
        If ToggleOutWorkMins.IsOn = True Then
            OwMinus = "Y"
        Else
            OwMinus = "N"
        End If
        'Dim LateVerification As String
        Dim SMSPassword As String = TxtPwd.Text.Trim
        Dim SMSUserID As String = TxtUserId.Text.Trim
        Dim SMSMessage As String = TxtMsg.Text.Trim
        Dim BioPort As String = TextEditBioPort.Text.Trim
        Dim ZKPort As String = TextEditZKPort.Text.Trim
        Dim TWIR102Port As String = TextTWIR102Port.Text.Trim
        Dim Canteen As String
        Dim Visitor As String
        If ToggleSwitchCanteen.IsOn = True Then
            Canteen = "Y"
        Else
            Canteen = "N"
        End If
        If ToggleSwitchVisitor.IsOn = True Then
            Visitor = "Y"
        Else
            Visitor = "N"
        End If
        Dim IsNepali As String = "N"
        If ToggleSwitchIsNepali.IsOn = True Then
            IsNepali = "Y"
        Else
            IsNepali = "N"
        End If
        Dim TimerDur As String = TextEditTimerDur.Text.Trim
        If TextEditTimerDur.Text.Trim = "" Then
            TimerDur = 10
        End If
        Dim AutoDwnDur As String = TextEditAutoDwnDur.Text.Trim
        If AutoDwnDur = "" Then
            AutoDwnDur = "0"
        End If
        'Dim BackDays As String
        Dim sSql As String
        If Common.servername = "Access" Then
            sSql = "insert into tblSetup(SETUPID,PERMISLATEARR,PERMISEARLYDEP,DUPLICATECHECKMIN,S_END,S_OUT,ISOTOUTMINUSSHIFTENDTIME,ISOTWRKGHRSMINUSSHIFTHRS,ISOTEARLYCOMEPLUSLATEDEP,ISOTEARLYCOMING,OTLATECOMINGDUR,OTRESTRICTENDDUR,OTEARLYDEPARTUREDUR,DEDUCTWOOT,DEDUCTHOLIDAYOT,ISPRESENTONWOPRESENT,ISPRESENTONHLDPRESENT,ISAUTOABSENT,AUTOSHIFT_LOW,AUTOSHIFT_UP,ISAUTOSHIFT,WOINCLUDE,IsOutWork,NightShiftFourPunch,LinesPerPage,OTROUND,PREWO,ISAWA,ISPREWO,LeaveFinancialYear,Online,AutoDownload,MIS,OwMinus,SMSPassword,SMSUserID,SMSMessage,OTEARLYDUR, BioPort, ZKPort, Canteen, Visitor, IsNepali, CreateEmpPunchDwnld,StartRealTime, TimerDur, TWIR102Port, AutoDwnDur) " & _
                         "VALUES('" & SETUPID & "','" & PERMISLATEARR & "','" & PERMISEARLYDEP & "','" & DUPLICATECHECKMIN & "','" & S_END.ToString("yyyy-MM-dd HH:mm:ss") & "','" & S_OUT.ToString("yyyy-MM-dd HH:mm:ss") & "','" & ISOTOUTMINUSSHIFTENDTIME & "','" & ISOTWRKGHRSMINUSSHIFTHRS & "','" & ISOTEARLYCOMEPLUSLATEDEP & "','" & ISOTEARLYCOMING & "','" & OTLATECOMINGDUR & "','" & OTRESTRICTENDDUR & "','" & OTEARLYDEPARTUREDUR & "','" & DEDUCTWOOT & "','" & DEDUCTHOLIDAYOT & "','" & ISPRESENTONWOPRESENT & "','" & ISPRESENTONHLDPRESENT & "','" & ISAUTOABSENT & "','" & AUTOSHIFT_LOW & "','" & AUTOSHIFT_UP & "','" & ISAUTOSHIFT & "','" & WOINCLUDE & "','" & IsOutWork & "','" & NightShiftFourPunch & "','" & LinesPerPage & "','" & OTROUND & "','" & PREWO & "','" & ISAWA & "','" & ISPREWO & "','" & LeaveFinancialYear & "','" & Online & "','" & AutoDownload & "','" & MIS & "','" & OwMinus & "','" & SMSPassword & "','" & SMSUserID & "','" & SMSMessage & "','" & OTEARLYDUR & "','" & BioPort & "', '" & ZKPort & "','" & Canteen & "','" & Visitor & "','" & IsNepali & "','N', '" & StartRealTime & "','" & TimerDur & "','" & TWIR102Port & "','" & AutoDwnDur & "')"

            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()

            'sSql = "Update tblEmployeeShiftMaster set Time1=" & TIME1 & " ,isOT = '" & ISOTALL & "', PERMISLATEARRIVAL = " & PERMISLATEARR & ", PERMISEARLYDEPRT = " & PERMISEARLYDEP & ", isAutoShift = '" & ISAUTOSHIFT & "', MAXDAYMIN = " & MAXWRKDURATION & ", ISOS = '" & ISOVERSTAY & "',  Half = " & HALF & ", isHalfDay = '" & ISHALFDAY & "', isShort = '" & ISSHORT & "'"
            'cmd1 = New OleDbCommand(sSql, Common.con1)
            'cmd1.ExecuteNonQuery()

            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            sSql = "insert into tblSetup(SETUPID,PERMISLATEARR,PERMISEARLYDEP,DUPLICATECHECKMIN,S_END,S_OUT,ISOTOUTMINUSSHIFTENDTIME,ISOTWRKGHRSMINUSSHIFTHRS,ISOTEARLYCOMEPLUSLATEDEP,ISOTEARLYCOMING,OTLATECOMINGDUR,OTRESTRICTENDDUR,OTEARLYDEPARTUREDUR,DEDUCTWOOT,DEDUCTHOLIDAYOT,ISPRESENTONWOPRESENT,ISPRESENTONHLDPRESENT,ISAUTOABSENT,AUTOSHIFT_LOW,AUTOSHIFT_UP,ISAUTOSHIFT,WOINCLUDE,IsOutWork,NightShiftFourPunch,LinesPerPage,OTROUND,PREWO,ISAWA,ISPREWO,LeaveFinancialYear,Online,AutoDownload,MIS,OwMinus,SMSPassword,SMSUserID,SMSMessage, BioPort, ZKPort, Canteen, Visitor, IsNepali, CreateEmpPunchDwnld,StartRealTime, TimerDur, TWIR102Port, AutoDwnDur) " & _
                            "VALUES('" & SETUPID & "','" & PERMISLATEARR & "','" & PERMISEARLYDEP & "','" & DUPLICATECHECKMIN & "','" & S_END.ToString("yyyy-MM-dd HH:mm:ss") & "','" & S_OUT.ToString("yyyy-MM-dd HH:mm:ss") & "','" & ISOTOUTMINUSSHIFTENDTIME & "','" & ISOTWRKGHRSMINUSSHIFTHRS & "','" & ISOTEARLYCOMEPLUSLATEDEP & "','" & ISOTEARLYCOMING & "','" & OTLATECOMINGDUR & "','" & OTRESTRICTENDDUR & "','" & OTEARLYDEPARTUREDUR & "','" & DEDUCTWOOT & "','" & DEDUCTHOLIDAYOT & "','" & ISPRESENTONWOPRESENT & "','" & ISPRESENTONHLDPRESENT & "','" & ISAUTOABSENT & "','" & AUTOSHIFT_LOW & "','" & AUTOSHIFT_UP & "','" & ISAUTOSHIFT & "','" & WOINCLUDE & "','" & IsOutWork & "','" & NightShiftFourPunch & "','" & LinesPerPage & "','" & OTROUND & "','" & PREWO & "','" & ISAWA & "','" & ISPREWO & "','" & LeaveFinancialYear & "','" & Online & "','" & AutoDownload & "','" & MIS & "','" & OwMinus & "','" & SMSPassword & "','" & SMSUserID & "','" & SMSMessage & "','" & BioPort & "', '" & ZKPort & "','" & Canteen & "','" & Visitor & "','" & IsNepali & "','N', '" & StartRealTime & "','" & TimerDur & "','" & TWIR102Port & "','" & AutoDwnDur & "')"
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()

            'sSql = "Update tblEmployeeShiftMaster set Time=" & TIME & " ,isOT = '" & ISOTALL & "', PERMISLATEARRIVAL = " & PERMISLATEARR & ", PERMISEARLYDEPRT = " & PERMISEARLYDEP & ", isAutoShift = '" & ISAUTOSHIFT & "', MAXDAYMIN = " & MAXWRKDURATION & ", ISOS = '" & ISOVERSTAY & "',  Half = " & HALF & ", isHalfDay = '" & ISHALFDAY & "', isShort = '" & ISSHORT & "'"
            'cmd = New SqlCommand(sSql, Common.con)
            'cmd.ExecuteNonQuery()

            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Common.IsNepali = IsNepali
        Common.TimerDur = TimerDur
        Common.online = Online
        Common.Load_Corporate_PolicySql()
        Me.Close()
    End Sub
    Private Sub CheckEditOT3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditOT3.CheckedChanged
        If CheckEditOT3.Checked = True Then
            LabelControl42.Enabled = True
            ToggleOTisAllowedInEarlyComing.Enabled = True
            GroupControl4.Enabled = True
        Else
            LabelControl42.Enabled = False
            ToggleOTisAllowedInEarlyComing.Enabled = False
            GroupControl4.Enabled = False
        End If
    End Sub
End Class