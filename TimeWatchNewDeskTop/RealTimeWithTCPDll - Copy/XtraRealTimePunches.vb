﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports DevExpress.LookAndFeel
Imports System.Net.Sockets
Imports System.Threading
Imports System.Net
Imports System.Text
Imports ZKPushDemoNitinCSharp
Imports Newtonsoft.Json.Linq

Public Class XtraRealTimePunches
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim ulf As UserLookAndFeel
    Dim adap As SqlDataAdapter
    Dim adapA As OleDbDataAdapter
    Dim ds As DataSet = New DataSet
    Dim Paycode As String
    Dim Name As String
    Dim VerifyMode As String
    Dim PunchDate As String
    Dim PunchTime As String
    Dim Cn As Common '= New Common
    Dim BioPort As Integer
    Dim ZKPort As Integer

    'Zk
    Dim tcp As TcpListener
    Dim listenThread As Thread
    Dim result() As DataRow
    Dim listening As Boolean = False
    Dim ATTLOGStamp As String = "0"
    Dim OPERLOGStamp As String = "0"
    Dim USERINFOStamp As String = "0"
    Dim ATTPHOTOStamp As String = "0"
    Dim fingertmpStamp As String = "0"
    Dim strImageNumber As String = ""
    Dim PathofImage As String = ""
    'Dim machine As String = ""
    Dim sysIp As String

    Dim flagDownLoadPause As Boolean = False
    Dim flagDownLoadResume As Boolean = False
    Dim flagreboot As Boolean = False
    Dim flagshell As Boolean = False
    Dim flagcheck As Boolean = False
    Dim flaginfo As Boolean = False
    Dim flagoption As Boolean = False
    Dim flagclear As Boolean = False
    Dim flagupduserinfo As Boolean = False
    Dim flagunlock As Boolean = False
    Dim flagdeluserinfo As Boolean = False
    Dim flagsendbell As Boolean = False
    Dim flagdeluserpic As Boolean = False
    Dim flagsetuserpic As Boolean = False
    Dim deluserpicinput As String = ""
    Dim setuserpicinput As String = ""
    Dim shellinput As String = ""
    Dim optioninput As String = ""
    Dim clearinput As String = ""
    Dim upduserinfoinput As String = ""
    Dim deluserinfoinput As String = ""
    'End ZK

    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
    End Sub
    Private Sub XtraRealTimePunches_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Cn = New Common
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = " Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup )"
        If Common.servername = "Access" Then
            sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
            adapAc = New OleDbDataAdapter(sSql, Common.con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, Common.con)
            adapS.Fill(Rs)
        End If
        BioPort = Convert.ToInt32(Rs.Tables(0).Rows(0).Item("BioPort").ToString.Trim)
        ZKPort = Convert.ToInt32(Rs.Tables(0).Rows(0).Item("ZKPort").ToString.Trim)
        lblTotal.Text = "Total : 0"
        sysIp = GetIP()
        StartRealTime()
    End Sub
    Private Sub XtraRealTimePunches_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to stop Real Time Monitor?</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Dim svrPort As Integer = 0
            svrPort = BioPort
            If (Me.mbOpenFlag = True) Then
                AxRealSvrOcxTcp1.CloseNetwork(svrPort)
                Me.mbOpenFlag = False
                'Me.EnableButtons(True)
            End If
            'Try
            '    tcp.Stop()   'for zk
            '    listenThread.Abort()
            'Catch ex As Exception
            'End Try
        Else
            e.Cancel = True
        End If

    End Sub
    'for real time listning
    '=============== VerifyMode of GeneralLogData ===============//
    Public Enum enumGLogVerifyMode
        LOG_FPVERIFY = 1
        LOG_PASSVERIFY = 2
        LOG_CARDVERIFY = 3
        LOG_FPPASS_VERIFY = 4
        LOG_FPCARD_VERIFY = 5
        LOG_PASSFP_VERIFY = 6
        LOG_CARDFP_VERIFY = 7
        LOG_JOB_NO_VERIFY = 8
        LOG_CARDPASS_VERIFY = 9
        LOG_FACEVERIFY = 20
        LOG_FACECARDVERIFY = 21
        LOG_FACEPASSVERIFY = 22
        LOG_CARDFACEVERIFY = 23
        LOG_PASSFACEVERIFY = 24
    End Enum
    '=============== IOMode of GeneralLogData ===============//
    Public Enum enumGLogIOMode
        LOG_IOMODE_IO = 0
        LOG_IOMODE_IN1 = 1
        LOG_IOMODE_OUT1 = 2
        LOG_IOMODE_IN2 = 3
        LOG_IOMODE_OUT2 = 4
        LOG_IOMODE_IN3 = 5
        LOG_IOMODE_OUT3 = 6
        LOG_IOMODE_IN4 = 7
        LOG_IOMODE_OUT4 = 8
    End Enum
    Public Enum enumGLogDoorMode
        LOG_CLOSE_DOOR = 1
        LOG_OPEN_HAND = 2
        LOG_PROG_OPEN = 3
        LOG_PROG_CLOSE = 4
        LOG_OPEN_IREGAL = 5
        LOG_CLOSE_IREGAL = 6
        LOG_OPEN_COVER = 7
        LOG_CLOSE_COVER = 8
        LOG_OPEN_DOOR = 9
        LOG_OPEN_DOOR_THREAT = 10
    End Enum
    Public Enum enumVerifyKind
        VK_NONE = 0
        VK_FP = 1
        VK_PASS = 2
        VK_CARD = 3
        VK_FACE = 4
        VK_VEIN = 5
        VK_IRIS = 6
    End Enum
    Public Shared Function GetStringVerifyMode(ByVal nVerifyMode As Int32) As String
        Dim vRet As String = ""
        Dim vByteCount As Integer = 4
        Dim vbyteKind() As Byte = New Byte((vByteCount) - 1) {}
        Dim vSecondKind As Integer
        Dim vFirstKind As Integer
        vbyteKind = BitConverter.GetBytes(nVerifyMode)
        Dim nIndex As Integer = (vByteCount - 1)
        Do While (nIndex >= 0)
            vSecondKind = vbyteKind(nIndex)
            vFirstKind = vbyteKind(nIndex)
            vFirstKind = (vFirstKind And 240)
            vSecondKind = (vSecondKind And 15)
            vFirstKind = (vFirstKind + 4)
            If (vFirstKind = 0) Then
                Exit Do
            End If

            If (nIndex _
                        < (vByteCount - 1)) Then
                vRet = (vRet + "+")
            End If

            Select Case (vFirstKind)
                Case CType(enumVerifyKind.VK_FP, Integer)
                    vRet = (vRet + "FP")
                Case CType(enumVerifyKind.VK_PASS, Integer)
                    vRet = (vRet + "PASS")
                Case CType(enumVerifyKind.VK_CARD, Integer)
                    vRet = (vRet + "CARD")
                Case CType(enumVerifyKind.VK_FACE, Integer)
                    vRet = (vRet + "FACE")
                Case CType(enumVerifyKind.VK_VEIN, Integer)
                    vRet = (vRet + "VEIN")
                Case CType(enumVerifyKind.VK_IRIS, Integer)
                    vRet = (vRet + "IRIS")
            End Select

            If (vSecondKind = 0) Then
                Exit Do
            End If

            vRet = (vRet + "+")
            Select Case (vSecondKind)
                Case CType(enumVerifyKind.VK_FP, Integer)
                    vRet = (vRet + "FP")
                Case CType(enumVerifyKind.VK_PASS, Integer)
                    vRet = (vRet + "PASS")
                Case CType(enumVerifyKind.VK_CARD, Integer)
                    vRet = (vRet + "CARD")
                Case CType(enumVerifyKind.VK_FACE, Integer)
                    vRet = (vRet + "FACE")
                Case CType(enumVerifyKind.VK_VEIN, Integer)
                    vRet = (vRet + "VEIN")
                Case CType(enumVerifyKind.VK_IRIS, Integer)
                    vRet = (vRet + "IRIS")
            End Select

            nIndex = (nIndex - 1)
        Loop

        'nVerifyMode.
        If (vRet = "") Then
            vRet = "--"
        End If

        Return vRet
    End Function
    Public Shared Sub GetIoModeAndDoorMode(ByVal nIoMode As Int32, ByRef vIoMode As Integer, ByRef vDoorMode As Integer)
        Dim vByteCount As Integer = 4
        Dim vbyteKind() As Byte = New Byte((vByteCount) - 1) {}
        Dim vbyteDoorMode() As Byte = New Byte((vByteCount) - 1) {}
        vbyteKind = BitConverter.GetBytes(nIoMode)
        vIoMode = vbyteKind(0)
        Dim nIndex As Integer = 0
        Do While (nIndex < 3)
            vbyteDoorMode(nIndex) = vbyteKind((nIndex + 1))
            nIndex = (nIndex + 1)
        Loop

        vbyteDoorMode(3) = 0
        vDoorMode = BitConverter.ToInt32(vbyteDoorMode, 0)
    End Sub
    Public Shared Function GetLong(ByVal asVal As String) As Long
        Try
            Return Convert.ToInt32(asVal)
        Catch  'As System.Exception
            Return 0
        End Try

    End Function
    Public Shared Function GetInt(ByVal asVal As String) As Integer
        Try
            Return Convert.ToInt32(asVal)
        Catch  'As System.Exception
            Return 0
        End Try

    End Function
    Public Shared Function StringToByteArray(ByVal hex As String) As Byte()
        Return Enumerable.Range(0, hex.Length).Where(Function(x) x Mod 2 = 0).[Select](Function(x) Convert.ToByte(hex.Substring(x, 2), 16)).ToArray()
    End Function
    '=============== Error code ===============//
    Public Const RUN_SUCCESS As Integer = 1
    Public Const RUNERR_NOSUPPORT As Integer = 0
    Public Const RUNERR_UNKNOWNERROR As Integer = -1
    Public Const RUNERR_NO_OPEN_COMM As Integer = -2
    Public Const RUNERR_WRITE_FAIL As Integer = -3
    Public Const RUNERR_READ_FAIL As Integer = -4
    Public Const RUNERR_INVALID_PARAM As Integer = -5
    Public Const RUNERR_NON_CARRYOUT As Integer = -6
    Public Const RUNERR_DATAARRAY_END As Integer = -7
    Public Const RUNERR_DATAARRAY_NONE As Integer = -8
    Public Const RUNERR_MEMORY As Integer = -9
    Public Const RUNERR_MIS_PASSWORD As Integer = -10
    Public Const RUNERR_MEMORYOVER As Integer = -11
    Public Const RUNERR_DATADOUBLE As Integer = -12
    Public Const RUNERR_MANAGEROVER As Integer = -14
    Public Const RUNERR_FPDATAVERSION As Integer = -15
    Public Const gstrNoDevice As String = "No Device"
    '==========================================//
    Private mbOpenFlag As Boolean
    Private fnCount As Integer = 0
    Private Sub StartRealTime()
        Dim svrPortBio As Integer = 0
        Dim retCodeBio As Integer = 0

        Dim svrPortZK As Integer = 0
        Dim retCodeZK As Integer = 0
        Try
            svrPortBio = BioPort
            'SimpleButtonStart.Enabled = False
            retCodeBio = AxRealSvrOcxTcp1.OpenNetwork(svrPortBio)
            If (retCodeBio = RUN_SUCCESS) Then
                Me.mbOpenFlag = True
                'Me.EnableButtons(False)
            Else
                XtraMessageBox.Show(ulf, "<size=10 " & Me.ReturnResultPrint(retCodeBio) & "</size>", "<size=9>Error</size>")
                'SimpleButtonStart.Enabled = True
            End If
            'Me.SimpleButtonStop(sender, e)
        Catch
            XtraMessageBox.Show(ulf, "<size=10>Invalid Port For BIO</size>", "<size=9>Error</size>")
        End Try
        ''ZK
        ''Me.listenThread = New Thread(ListenClient)
        ''listenThread = New Thread(Sub() ListenClient())
        'Me.listenThread = New Thread(New ThreadStart(AddressOf Me.ListenClient))
        'Me.listenThread.Start()
        'Me.listenThread.IsBackground = True


        openTcpPort()


        'ListenClient()
        listening = True
        'End ZK
        lblTotal.Text = "Total : 0"
        SetupLogListview()
    End Sub
    Private Sub openTcpPort()
        tcpServer1.Port = Convert.ToInt32(ZKPort)                
        tcpServer1.Open()
    End Sub

    Private Sub SetupLogListview()
        GridControl1.DataSource = Nothing
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("No")
        dt.Columns.Add("EnrollNo")
        dt.Columns.Add("Paycode")
        dt.Columns.Add("Name")
        dt.Columns.Add("VerifyMode")
        'dt.Columns.Add("InOut")
        dt.Columns.Add("Punch Date")
        dt.Columns.Add("Punch Time")
        dt.Columns.Add("IP/Serial No")
        'dt.Columns.Add("Port")
        dt.Columns.Add("DevID")
        'dt.Columns.Add("SerialNo")
        'dt.Columns.Add("RouterIP")

        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridControl1.DataSource = dt
        GridView1.Columns.Item(0).Width = 40
        GridView1.Columns.Item(1).Width = 100
        GridView1.Columns.Item(7).Width = 100
        Dim strLogItem() As String = New String() {"No", "EnrollNo", "VerifyMode", "InOut", "DateTime", "IP", "Port", "DevID", "SerialNo", "RouterIP"}
        lvwLogList.Items.Clear()
        lvwLogList.Columns.Clear()
        For Each strTmp As String In strLogItem
            lvwLogList.Columns.Add(strTmp, 80, HorizontalAlignment.Center)
        Next
        lvwLogList.Columns(0).Width = 60
        lvwLogList.Columns(0).TextAlign = HorizontalAlignment.Right
        lvwLogList.Columns(1).Width = 120
        lvwLogList.Columns(2).Width = 120
        lvwLogList.Columns(4).Width = 140
        lvwLogList.Columns(7).Width = 130
    End Sub
    Public Function ReturnResultPrint(ByVal anResultCode As Integer) As String
        Dim retval As String = Nothing
        Select Case (anResultCode)
            Case RUN_SUCCESS
                retval = "Successful!"
            Case RUNERR_NOSUPPORT
                retval = "No support"
            Case RUNERR_UNKNOWNERROR
                retval = "Unknown error"
            Case RUNERR_NO_OPEN_COMM
                retval = "Cannot Open Connection"
            Case RUNERR_WRITE_FAIL
                retval = "Write Error"
            Case RUNERR_READ_FAIL
                retval = "Read Error"
            Case RUNERR_INVALID_PARAM
                retval = "Parameter Error"
            Case RUNERR_NON_CARRYOUT
                retval = "execution of command failed"
            Case RUNERR_DATAARRAY_END
                retval = "End of data"
            Case RUNERR_DATAARRAY_NONE
                retval = "Nonexistence data"
            Case RUNERR_MEMORY
                retval = "Memory Allocating Error"
            Case RUNERR_MIS_PASSWORD
                retval = "License Error"
            Case RUNERR_MEMORYOVER
                retval = "full enrolldata & can`t put enrolldata"
            Case RUNERR_DATADOUBLE
                retval = "this ID is already  existed."
            Case RUNERR_MANAGEROVER
                retval = "full manager & can`t put manager."
            Case RUNERR_FPDATAVERSION
                retval = "mistake fp data version."
            Case Else
                retval = "Unknown error"
        End Select

        Return retval
    End Function
    Private Sub showLogImage(ByVal astrBuffer As String)
        Dim bytImg() As Byte = Nothing
        Dim varImg As Object
        Try
            bytImg = StringToByteArray(astrBuffer)
            If (bytImg.Length < 1) Then
                Return
            End If

            'varImg = New System.Runtime.InteropServices.VariantWrapper(bytImg)
            'axAxImage1.SetJpgImage(varImg)
        Catch e As Exception

        End Try

    End Sub
    Private Function funcShowGeneralLogDataToGrid(ByVal aEnrollNumber As Long, ByVal aVerifyMode As Long, ByVal aInOutMode As Long, ByVal adtLog As DateTime, ByVal abDrawFlag As Boolean, ByVal astrRemoteIP As String, ByVal anRemotePort As Long, ByVal anDeviceID As Long, ByVal astrSerialNo As String, ByVal astrRouterIP As String) As Boolean
        If aEnrollNumber = 0 Then
            Exit Function
        End If
        Dim vItem As ListViewItem
        Dim strTmp As String = Nothing
        Dim vDoorMode As Integer = 0
        Dim vIoMode As Integer = 0
        vItem = New ListViewItem
        vItem.Text = Convert.ToString((lvwLogList.Items.Count + 1))
        vItem.SubItems.Add(Convert.ToString(aEnrollNumber))
        Select Case (aVerifyMode)
            Case CType(enumGLogVerifyMode.LOG_FPVERIFY, Integer)
                '1
                strTmp = "Fp"
            Case CType(enumGLogVerifyMode.LOG_PASSVERIFY, Integer)
                '2
                strTmp = "Password"
            Case CType(enumGLogVerifyMode.LOG_CARDVERIFY, Integer)
                '3
                strTmp = "Card"
            Case CType(enumGLogVerifyMode.LOG_FPPASS_VERIFY, Integer)
                '4
                strTmp = "Fp+Password"
            Case CType(enumGLogVerifyMode.LOG_FPCARD_VERIFY, Integer)
                '5
                strTmp = "Fp+Card"
            Case CType(enumGLogVerifyMode.LOG_PASSFP_VERIFY, Integer)
                '6
                strTmp = "Password+Fp"
            Case CType(enumGLogVerifyMode.LOG_CARDFP_VERIFY, Integer)
                '7
                strTmp = "Card+Fp"
            Case CType(enumGLogVerifyMode.LOG_FACEVERIFY, Integer)
                '20
                strTmp = "Face"
            Case CType(enumGLogVerifyMode.LOG_FACECARDVERIFY, Integer)
                '21
                strTmp = "Face+Card"
            Case CType(enumGLogVerifyMode.LOG_FACEPASSVERIFY, Integer)
                '22
                strTmp = "Face+Pass"
            Case CType(enumGLogVerifyMode.LOG_PASSFACEVERIFY, Integer)
                '21
                strTmp = "Pass+Face"
            Case CType(enumGLogVerifyMode.LOG_CARDFACEVERIFY, Integer)
                '22
                strTmp = "Card+Face"
            Case Else
                strTmp = GetStringVerifyMode(CType(aVerifyMode, Integer))
        End Select
        VerifyMode = strTmp

        vItem.SubItems.Add(strTmp)
        GetIoModeAndDoorMode(CType(aInOutMode, Integer), vIoMode, vDoorMode)
        If (vDoorMode = 0) Then
            'MessageBox.Show(vIoMode)
            strTmp = "( " & vIoMode & " )"
        Else
            strTmp = "( " & vIoMode & " ) "
        End If
        Select Case (vDoorMode)
            Case CType(enumGLogDoorMode.LOG_CLOSE_DOOR, Integer)
                strTmp = (strTmp & "Close Door")
            Case CType(enumGLogDoorMode.LOG_OPEN_HAND, Integer)
                strTmp = (strTmp & "Hand Open")
            Case CType(enumGLogDoorMode.LOG_PROG_OPEN, Integer)
                strTmp = (strTmp & "Prog Open")
            Case CType(enumGLogDoorMode.LOG_PROG_CLOSE, Integer)
                strTmp = (strTmp & "Prog Close")
            Case CType(enumGLogDoorMode.LOG_OPEN_IREGAL, Integer)
                strTmp = (strTmp & "Illegal Open")
            Case CType(enumGLogDoorMode.LOG_CLOSE_IREGAL, Integer)
                strTmp = (strTmp & "Illegal Close")
            Case CType(enumGLogDoorMode.LOG_OPEN_COVER, Integer)
                strTmp = (strTmp & "Cover Open")
            Case CType(enumGLogDoorMode.LOG_CLOSE_COVER, Integer)
                strTmp = (strTmp & "Cover Close")
            Case CType(enumGLogDoorMode.LOG_OPEN_DOOR, Integer)
                strTmp = (strTmp & "Open door")
            Case CType(enumGLogDoorMode.LOG_OPEN_DOOR_THREAT, Integer)
                strTmp = (strTmp & "Open Door as Threat")
        End Select
        If (strTmp = "") Then
            strTmp = "--"
        End If
        vItem.SubItems.Add(strTmp)
        strTmp = Convert.ToString(adtLog)
        PunchDate = adtLog.ToString("dd MMM yyyy")
        PunchTime = adtLog.ToString("HH:mm")
        vItem.SubItems.Add(strTmp)
        vItem.SubItems.Add(astrRemoteIP)
        vItem.SubItems.Add(Convert.ToString(anRemotePort))
        vItem.SubItems.Add(Convert.ToString(anDeviceID))
        vItem.SubItems.Add(astrSerialNo)
        vItem.SubItems.Add(astrRouterIP)
        lvwLogList.Items.Add(vItem)
        If (lvwLogList.Items.Count > 12) Then
            lvwLogList.EnsureVisible((lvwLogList.Items.Count - 12))
        End If
        Dim ds As DataSet = New DataSet
        Dim IN_OUT As String = ""
        Dim ssql As String = "SELECT ID_NO, IN_OUT from tblMachine where MAC_ADDRESS='" & astrSerialNo & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            anDeviceID = ds.Tables(0).Rows(0).Item("ID_NO").ToString.Trim
            IN_OUT = ds.Tables(0).Rows(0).Item("IN_OUT").ToString.Trim
        End If
        
        ssql = "select * from tblemployee where PRESENTCARDNO='" & aEnrollNumber.ToString("000000000000") & "'"
        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            Name = ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim
            Paycode = ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim

            If IN_OUT = "B" Then
                Dim ds2 As DataSet = New DataSet
                Dim adapT As SqlDataAdapter
                Dim adapTA As OleDbDataAdapter
                If Common.servername = "Access" Then
                    Dim sSql1 As String = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & aEnrollNumber.ToString("000000000000") & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & adtLog.ToString("yyyy-MM-dd") & "'"
                    adapTA = New OleDbDataAdapter(sSql1, Common.con1)
                    adapTA.Fill(ds)
                Else
                    Dim sSql1 As String = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & aEnrollNumber.ToString("000000000000") & "' and convert(varchar, OFFICEPUNCH, 23) = '" & adtLog.ToString("yyyy-MM-dd") & "'"
                    adapT = New SqlDataAdapter(sSql1, Common.con)
                    adapT.Fill(ds)
                End If

                adapT.Fill(ds2)
                If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                    IN_OUT = "I"
                Else
                    If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                        IN_OUT = "I"
                    Else
                        IN_OUT = "O"
                    End If
                End If
            End If

            ssql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & aEnrollNumber.ToString("000000000000") & "','" & adtLog.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & anDeviceID & "','','" & Paycode & "' ) "
            Try
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(ssql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(ssql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If

                'sms
                If Common.g_SMSApplicable = "" Then
                    Common.Load_SMS_Policy()
                End If
                If Common.g_SMSApplicable = "Y" Then
                    If Common.g_isAllSMS = "Y" Then
                        Try
                            If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then
                                Dim SmsContent As String = Common.g_AllContent1 & " " & adtLog.ToString("yyyy-MM-dd HH:mm") & " " & Common.g_AllContent2
                                Cn.sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                End If
                'sms end

            Catch ex As Exception

            End Try
            Cn.Remove_Duplicate_Punches(adtLog)

            Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PAYCODE = '" & Paycode & "'"
            ds = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSqltmp, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                'Dim mindatetmp As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                    Cn.Process_AllRTC(adtLog.AddDays(-1), Now.Date, Paycode, Paycode)
                Else
                    Cn.Process_AllnonRTC(adtLog, Now.Date, Paycode, Paycode)
                End If
            End If
            XtraMaster.LabelControl4.Text = ""
            Application.DoEvents()
        Else
            Name = "Not registered"
            Paycode = ""
        End If

        Try
            GridView1.AddNewRow()
            Dim rowHandle As Integer = GridView1.GetRowHandle(GridView1.DataRowCount)
            If GridView1.IsNewItemRow(rowHandle) Then
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(0), vItem.Text)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(1), aEnrollNumber.ToString("000000000000"))
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(2), Paycode)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(3), Name)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), VerifyMode)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(5), PunchDate)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(6), PunchTime)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(7), astrRemoteIP)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(8), anDeviceID)
            End If
            lblTotal.Text = ("Total : " & lvwLogList.Items.Count)
        Catch ex As Exception

        End Try
        Return True
    End Function
    Private Function ShowGeneralLogDataToGrid(ByVal anCount As Integer, ByVal astrLogId As String, ByVal astrEnrollNumber As String, ByVal astrVerifyMode As String, ByVal astrInOutMode As String, ByVal adtLog As String, ByVal abDrawFlag As Boolean, ByVal astrRemoteIP As String, ByVal anRemotePort As Long, ByVal astrDeviceID As String) As Boolean
        Dim vItem As ListViewItem
        Dim strTmp As String = Nothing
        Dim vDoorMode As Integer = 0
        Dim vIoMode As Integer = 0
        vItem = New ListViewItem
        vItem.Text = Convert.ToString((lvwLogList.Items.Count + 1))
        vItem.SubItems.Add(astrEnrollNumber)
        vItem.SubItems.Add(astrVerifyMode)
        vItem.SubItems.Add(astrInOutMode)
        'strTmp = Convert.ToString(adtLog);
        vItem.SubItems.Add(adtLog)
        vItem.SubItems.Add(astrRemoteIP)
        vItem.SubItems.Add(Convert.ToString(anRemotePort))
        vItem.SubItems.Add(astrDeviceID)
        vItem.SubItems.Add(astrLogId)
        lvwLogList.Items.Add(vItem)
        If (lvwLogList.Items.Count > 12) Then
            lvwLogList.EnsureVisible((lvwLogList.Items.Count - 12))
        End If
        Dim ds As DataSet = New DataSet
        Dim ssql As String = "SELECT ID_NO from tblMachine where LOCATION='" & astrRemoteIP & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        Dim anDeviceID As Long
        If ds.Tables(0).Rows.Count > 0 Then
            anDeviceID = ds.Tables(0).Rows(0).Item("ID_NO").ToString.Trim
        Else
            anDeviceID = astrDeviceID
        End If
        If IsNumeric(astrEnrollNumber) Then
            astrEnrollNumber = Convert.ToDouble(astrEnrollNumber).ToString("000000000000")
        End If
        GridView1.AddNewRow()
        Dim rowHandle As Integer = GridView1.GetRowHandle(GridView1.DataRowCount)
        If GridView1.IsNewItemRow(rowHandle) Then
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(0), vItem.Text)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(1), astrEnrollNumber)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(2), Paycode)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(3), Name)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), VerifyMode)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(5), PunchDate)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(6), PunchTime)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(7), astrRemoteIP)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(8), anDeviceID)
        End If

        lblTotal.Text = ("Total : " & lvwLogList.Items.Count)
        Return True
    End Function
    Private Sub AxRealSvrOcxTcp1_OnReceiveGLogDataExtend(ByVal sender As Object, ByVal e As AxRealSvrOcxTcpLib._DRealSvrOcxTcpEvents_OnReceiveGLogDataExtendEvent) Handles AxRealSvrOcxTcp1.OnReceiveGLogDataExtend
        'Dim userid As String '= Convert.ToInt32(e.anSEnrollNumber.ToString("X8"), 16)
        'If e.anSEnrollNumber < 0 Then
        '    Dim x As UInt64 = Convert.ToInt64(e.anSEnrollNumber) ' e.anSEnrollNumber.ToString.Substring(1, 10)
        '    Dim userid1 As double = CDbl(Val("&H" & Hex(x))) ' Convert.ToInt32(x.ToString("X8"), 16) ' Convert.ToInt64(x.ToString, 16)
        '    'Dim userid1 As UInt32 = CType(x, UInt32)
        '    userid = userid1
        'Else
        '    userid = e.anSEnrollNumber
        'End If
        Me.funcShowGeneralLogDataToGrid(e.anSEnrollNumber, e.anVerifyMode, e.anInOutMode, e.anLogDate, True, e.astrDeviceIP, e.anDevicePort, e.anDeviceID, e.astrSerialNo, e.astrRootIP)
    End Sub
    Private Sub AxRealSvrOcxTcp1_OnReceiveGLogTextOnDoorOpen(ByVal sender As Object, ByVal e As AxRealSvrOcxTcpLib._DRealSvrOcxTcpEvents_OnReceiveGLogTextOnDoorOpenEvent) Handles AxRealSvrOcxTcp1.OnReceiveGLogTextOnDoorOpen
        Dim vstrLogId As String
        Dim vstrEnrollNumber As String
        Dim vstrDeviceID As String
        Dim vstrVerifyMode As String
        Dim vstrInOutMode As String
        Dim vstrDate As String
        Dim vDate As String
        Dim vstrResponse As String
        Dim vstrEmergency As String
        Dim vstrIsSupportStringID As String
        Dim jobjTest As JObject = JObject.Parse(e.astrLogText)
        vstrLogId = jobjTest("log_id").ToString()
        vstrEnrollNumber = jobjTest("user_id").ToString()
        vstrDeviceID = jobjTest("fk_device_id").ToString()
        vstrVerifyMode = jobjTest("verify_mode").ToString()
        If vstrVerifyMode = "FP" Then
            vstrVerifyMode = 1
        End If
        vstrInOutMode = jobjTest("io_mode").ToString()
        vstrDate = jobjTest("io_time").ToString()
        vDate = vstrDate.Substring(0, 4) & "-" & vstrDate.Substring(4, 2) & "-" & vstrDate.Substring(6, 2) & " " & vstrDate.Substring(8, 2) & ":" & vstrDate.Substring(10, 2) & ":" & vstrDate.Substring(12, 2)
        fnCount = fnCount + 1
        'ShowGeneralLogDataToGrid(fnCount, vstrLogId, vstrEnrollNumber, vstrVerifyMode, vstrInOutMode, vDate, True, e.astrClientIP, e.anClientPort, vstrDeviceID)
        'txtStatus.Text = "Last Log :{ user_id: " & vstrEnrollNumber & ", io_time :" + vDate & "}"
        showLogImage(e.astrLogImage)
        Dim jobjRespond As JObject = New JObject()
        jobjRespond.Add("log_id", vstrLogId)
        jobjRespond.Add("result", "OK")

        'Try
        '    vstrEmergency = jobjTest("emergency").ToString()
        '    vstrIsSupportStringID = jobjTest("is_support_string_id").ToString()

        '    If vstrEmergency.Equals("yes") Then
        '        If txtActiveId.Text <> "" AndAlso txtActiveId.Text = vstrEnrollNumber Then
        '            jobjRespond.Add("mode", "open")
        '        Else
        '            jobjRespond.Add("mode", "nothing")
        '        End If
        '    Else
        '        jobjRespond.Add("mode", "nothing")
        '    End If

        'Catch ex As Exception
        '    jobjRespond.Add("mode", "nothing")
        'End Try

        vstrResponse = jobjRespond.ToString()
        AxRealSvrOcxTcp1.SendRtLogResponseV3(e.astrClientIP, e.anClientPort, vstrResponse)
    End Sub
    Private Sub AxRealSvrOcxTcp1_OnReceiveGLogTextAndImage(ByVal sender As Object, ByVal e As AxRealSvrOcxTcpLib._DRealSvrOcxTcpEvents_OnReceiveGLogTextAndImageEvent) Handles AxRealSvrOcxTcp1.OnReceiveGLogTextAndImage
        'MsgBox("test")
    End Sub
    'end for real time listning for BIO




    'Zk listning
    ' Start TCP listening process
    Private Sub ListenClient()
        Try
            'If (txtIP.Text.Trim = String.Empty) Then
            '    Me.tcp = New TcpListener(Convert.ToInt32(txtPort.Text))
            'Else
            Me.tcp = New TcpListener(IPAddress.Parse(sysIp), ZKPort)
            'End If
            Me.tcp.Start()
            'Me.WriteText(("Listening on: "  + (Me.tcp.LocalEndpoint + "" & vbCrLf)), Color.DarkRed)
            listening = True
            While listening
                Dim mySocket As Socket = Nothing
                ' Block until a device client has connected to the server 
                Try
                    mySocket = Me.tcp.AcceptSocket
                    Thread.Sleep(500)
                    Dim bReceive() As Byte = New Byte(((1024 * (1024 * 2))) - 1) {}
                    Dim i As Integer = mySocket.Receive(bReceive)
                    Dim dataStr As String = Encoding.ASCII.GetString(bReceive)
                    'MsgBox(dataStr)
                    Dim databyte() As Byte = New Byte((i) - 1) {}
                    Dim j As Integer = 0
                    'Do While (j < i)
                    For j = 0 To i - 1
                        databyte(j) = bReceive(j)
                    Next
                    ''j = (j + 1)
                    ''Loop
                    ''Handle data or requests of device.

                    ''Dim zk As ZKPushDemoNitinCSharp.Form1 = New ZKPushDemoNitinCSharp.Form1
                    ''Dim attstrtmp As String = ""
                    ''zk.Analysis(bReceive, mySocket, attstrtmp)

                    ''If dataStr.IndexOf("Stamp", 1) > 0 AndAlso dataStr.IndexOf("OPERLOG", 1) < 0 AndAlso dataStr.IndexOf("ATTLOG", 1) > 0 AndAlso dataStr.IndexOf("OPLOG", 1) < 0 Then
                    ''    attlog(dataStr)
                    ''End If

                    'Analysis(databyte, mySocket)
                Catch
                End Try
            End While
            Me.tcp.Stop()
        Catch
            listening = False
            'XtraMessageBox.Show(ulf, "<size=10>Invalid Port</size>", "<size=9>Error</size>")
        End Try
    End Sub
    Public Sub attlog(ByVal sBuffer As String, ByVal machineSN As String)
        'Dim machineSN As String = sBuffer.Substring((sBuffer.IndexOf("SN=") + 3))
        'Dim arr() As String = machineSN.Split(Microsoft.VisualBasic.ChrW(38))
        'machine = arr(0)

        'If machine.Substring(0, 3) = "AF6" Or machine.Substring(0, 3) = "AEH" Or machine.Substring(0, 3) = "AJV" Or machine.Substring(0, 3) = "AOO" Or machine.Substring(0, 3) = "719" Or machine.Substring(0, 3) = "ALM" Or machine.Substring(0, 3) = "701" Or machine.Substring(0, 3) = "BYR" Or machine.Substring(0, 3) = "OIN" Or machine.Substring(0, 3) = "AF6" Or machine.Substring(0, 3) = "BYX" Or machine.Substring(0, 3) = "AIJ" Or machine.Substring(0, 3) = "A6G" Or machine.Substring(0, 3) = "AEH" Or machine.Substring(0, 3) = "CCG" Or machine.Substring(0, 3) = "BJV" _
        '               Or Mid(Trim(machine), 1, 1) = "A" Or Mid(Trim(machine), 1, 3) = "OIN" Or Mid(Trim(machine), 1, 5) = "FPCTA" Or Mid(Trim(machine), 1, 4) = "0000" Or Mid(Trim(machine), 1, 4) = "1808" Or Mid(Trim(machine), 1, 4) = "ATPL" Or Mid(Trim(machine), 1, 4) = "TIPL" Or Mid(Trim(machine), 1, 4) = "ZXJK" Or Mid(Trim(machine), 1, 8) = "10122013" Or Mid(Trim(machine), 1, 8) = "76140122" Or Mid(Trim(machine), 1, 8) = "17614012" Or Mid(Trim(machine), 1, 8) = "17214012" Or Mid(Trim(machine), 1, 8) = "27022014" Or Mid(Trim(machine), 1, 8) = "24042014" Or Mid(Trim(machine), 1, 8) = "25042014" Or Mid(Trim(machine), 1, 8) = "26042014" Or Mid(Trim(machine), 1, 7) = "SN:0000" Then
        'Else
        '    XtraMaster.LabelControl4.Text = "Invalid serial Number " & machine
        '    Application.DoEvents()
        '    Exit Sub
        'End If
        ''string machine = machineSN.Substring('&');
        Dim SN As String = ""
        'Me.getnumber(machineSN, SN)
        ' Get Serial Number of iclock Device
        Dim machinestamp As String = sBuffer.Substring((sBuffer.IndexOf("Stamp=") + 6))
        Dim Stamp As String = ""
        Me.getnumber(machinestamp, Stamp)
        ' Get TimeStamp 
        ATTLOGStamp = Stamp
        Dim attindex As Integer = sBuffer.IndexOf("" & vbCrLf & vbCrLf, 1)  ' sBuffer.IndexOf("\r\n\r\n", 1) 
        Dim attstr As String = sBuffer.Substring((attindex + 4))
        '''////////////////////////////////////////////////////////////////////////////////////
        Dim words() As String = attstr.Split(vbTab)
        For Each word As String In words

        Next
        'MsgBox(attstr)
        '''//////////////////////////////////////////////////////////////////////////////////////////
        attlogprocess(attstr, machineSN)
    End Sub
    Public Sub attlogprocess(ByVal sBuffer As String, machineSN As String)
        Try
            Dim attindex As Integer = sBuffer.IndexOf("" & vbLf, 1)
            Dim attstr As String = ""
            If (attindex > 0) Then
                attstr = sBuffer.Substring(0, attindex)
            End If
            saveattlog(attstr, machineSN)
            Dim endatt As String = sBuffer.Substring((attindex + 1))
            If (endatt <> "") Then
                attlogprocess(endatt, machineSN)
            End If
        Catch
        End Try

    End Sub
    Public Sub saveattlog(ByVal attlog As String, machine As String)
        Dim conS As SqlConnection = New SqlConnection
        conS = Common.con
        Dim conS1 As OleDbConnection = New OleDbConnection
        conS1 = Common.con1

        Dim EnrollNumber As String = attlog.Substring(0, attlog.IndexOf("" & vbTab, 1))
        If EnrollNumber = "" Then
            Return
        End If
        'EnrollNumber = string.Format("{00000000}");
        EnrollNumber = EnrollNumber.PadLeft(6, Microsoft.VisualBasic.ChrW(48))
        Dim still1 As String = attlog.Substring((attlog.IndexOf("" & vbTab, 1) + 1))
        Dim atttime As String = still1.Substring(0, still1.IndexOf("" & vbTab, 1))
        Dim still2 As String = still1.Substring((still1.IndexOf("" & vbTab, 1) + 1))
        Dim status As String = still2.Substring(0, still2.IndexOf("" & vbTab, 1))
        Dim still3 As String = still2.Substring((still2.IndexOf("" & vbTab, 1) + 1))
        ' WorkCode and DeviceID only exists on Firmware Professional version
        Dim verifymode As String = ""
        Try
            ' Verify Mode
            verifymode = still3.Substring(0, still3.IndexOf("" & vbTab, 1))
        Catch
            verifymode = still3
        End Try
        Dim sSql As String
        '''//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        'Indian Code Master
        PunchTime = Convert.ToDateTime(atttime.ToString)
        Dim EnrNum As String
        If IsNumeric(EnrollNumber) Then
            EnrNum = Convert.ToInt64(EnrollNumber).ToString("000000000000")
        Else
            EnrNum = EnrollNumber
        End If
        Dim deviceId As String = ""
        Dim IN_OUT As String = ""
        sSql = "SELECT ID_NO, IN_OUT from tblMachine where MAC_ADDRESS='" & machine & "'"
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, conS)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            deviceId = ds.Tables(0).Rows(0).Item("ID_NO").ToString.Trim
            IN_OUT = ds.Tables(0).Rows(0).Item("IN_OUT").ToString.Trim
        End If

        If IN_OUT = "B" Then
            Dim ds2 As DataSet = New DataSet
            Dim sSql1 As String '= "select count (CARDNO) from MachineRawPunch where CARDNO = '" & EnrNum & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd") & "'"
            Dim adapAT As OleDbDataAdapter
            Dim adapT As SqlDataAdapter
            If Common.servername = "Access" Then
                sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & EnrNum & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd") & "'"
                adapAT = New OleDbDataAdapter(sSql1, Common.con1)
                adapAT.Fill(ds2)
            Else
                sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & EnrNum & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd") & "'"
                adapT = New SqlDataAdapter(sSql1, conS)
                adapT.Fill(ds2)
            End If

            If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                IN_OUT = "I"
            Else
                If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                    IN_OUT = "I"
                Else
                    IN_OUT = "O"
                End If
            End If
        End If

        sSql = "select * from tblemployee where PRESENTCARDNO='" & EnrNum & "'"
        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, conS)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            Name = ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim
            Paycode = ds.Tables(0).Rows(0).Item("Paycode").ToString.Trim
            sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & EnrNum & "','" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & deviceId & "','" & IN_OUT & "','" & Paycode & "' ) "
            Try
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If conS.State <> ConnectionState.Open Then
                        conS.Open()
                    End If
                    cmd = New SqlCommand(sSql, conS)
                    cmd.ExecuteNonQuery()
                    If conS.State <> ConnectionState.Closed Then
                        conS.Close()
                    End If
                End If
                SetText(EnrNum, Paycode, Name, verifymode, Convert.ToDateTime(atttime.ToString), machine, deviceId)

                'sms
                If Common.g_SMSApplicable = "" Then
                    Common.Load_SMS_Policy()
                End If
                If Common.g_SMSApplicable = "Y" Then
                    If Common.g_isAllSMS = "Y" Then
                        Try
                            If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then
                                Dim SmsContent As String = Common.g_AllContent1 & " " & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd HH:mm") & " " & Common.g_AllContent2
                                Cn.sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                End If
                'sms end

                'GridView1.AddNewRow()
                'Dim rowHandle As Integer = GridView1.GetRowHandle(GridView1.DataRowCount)
                'If GridView1.IsNewItemRow(rowHandle) Then
                '    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(0), GridView1.RowCount + 1)
                '    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(1), EnrNum)
                '    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(2), Paycode)
                '    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(3), Name)
                '    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), verifymode)
                '    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(5), Convert.ToDateTime(atttime.ToString).ToString("dd MMM yyyy"))
                '    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(6), Convert.ToDateTime(atttime.ToString).ToString("HH:mm"))
                '    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(7), "")
                '    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(8), "")
                'End If
            Catch ex As Exception

            End Try
        Else
            SetText(EnrNum, "", "Not registered", verifymode, Convert.ToDateTime(atttime.ToString), machine, deviceId)
        End If
            'result = dsEmp.Tables(0).Select(("PresentCardno=" _
            '                + (EnrollNumber + "")))
            'If (result.Length > 0) Then
            '    sSql = ("Insert Into MachineRawPunch (Cardno, OfficePunch, ReasonCode, IsManual,inout,PAYCODE,p_day,MC_No) Val" & _
            '   "ues " + ("('" _
            '               + (EnrollNumber.ToString.Trim + ("', '" _
            '               + (PunchTime.ToString("yyyy-MM-dd HH:mm") + ("', '  ', 'N','N','" _
            '               + (mPaycode.ToString.Trim + ("','N','" _
            '               + (machine + "')")))))))))
            '    Try
            '        mResult = Con.execute_NonQuery(sSql)
            '        'sSql = "Insert into PW_SWIPE_TBL(Emplid,PW_PUCH_DATE,TIMEOUT_ACT,Descr254,Descr254A)Values('" + mPaycode.ToString().Trim() + "','" + PunchTime.ToString("yyyy-MM-dd") + "','" + PunchTime.ToString("HH:mm") + "',' ','" + string.Format("{0:000}", Convert.ToInt32(MacNum)) + "')";
            '        'MyConn.execute_NonQuery2(sSql);
            '        If (mResult > 0) Then
            '            mod.Remove_Duplicate_Punches(PunchTime.ToString("yyyy-MM-dd HH:mm:ss"), mPaycode)
            '            ProcDate = Convert.ToDateTime(PunchTime)
            '            If (mRTC = "Y") Then
            '                lastProcDate = ProcDate.AddDays(-1)
            '                mod.Process_AllRTC(lastProcDate, ProcDate, mPaycode, mPaycode)
            '            Else
            '                mod.Process_AllnonRTC(ProcDate, ProcDate, mPaycode, mPaycode)
            '            End If

            '        End If

            '        Me.WriteAttlog(mPaycode, EmpName, machine, atttime)
            '    Catch  As System.Exception
            '        '}
            '    End Try

            'Else
            '    mPaycode = EnrollNumber.ToString
            '    row = New String() {mPaycode, "", machine, PunchTime.ToString("yyyy-MM-dd HH:mm:ss")}
            '    Dim listViewItem = New ListViewItem(row)
            '    lvAttLog.Items.Add(listViewItem)
            '    Return
            '    rCount = (rCount + 1)
            '    If (rCount > 50) Then
            '        dataGridView1.Rows.Clear()
            '        rCount = 0
            '    End If

            '    sSql = ("Insert Into MachineRawPunch (Cardno, OfficePunch, ReasonCode, IsManual,inout,PAYCODE,p_day,MC_NO) Val" & _
            '    "ues " + ("('" _
            '                + (EnrollNumber.ToString.Trim + ("', '" _
            '                + (PunchTime.ToString("yyyy-MM-dd HH:mm") + ("', '  ', 'N','N','" _
            '                + (mPaycode.ToString.Trim + ("','N','" _
            '                + (String.Format("{0:000}", Convert.ToInt32(MacNum)) + "')")))))))))
            '    Try
            '        ' mResult = Con.execute_NonQuery(sSql);
            '    Catch  As System.Exception

            '    End Try

            'End If
            Return
    End Sub
    'to access grid from tread
    Public Delegate Sub SetTextCallback(EnrNum As String, Paycode As String, name As String, verifymode As String, PunchDate As DateTime, machine As String, deviceId As String)
    'ZK show in grid
    Private Sub SetText(EnrNum As String, Paycode As String, name As String, verifymode As String, PunchDate As DateTime, machine As String, deviceId As String)
        ' InvokeRequired required compares the thread ID of the
        ' calling thread to the thread ID of the creating thread.
        ' If these threads are different, it returns true.
        If Me.GridControl1.InvokeRequired Then
            Dim d As SetTextCallback = New SetTextCallback(AddressOf Me.SetText)
            Me.Invoke(d, New Object() {EnrNum, Paycode, name, verifymode, PunchDate, machine, deviceId})
        Else
            'Me.textBox1.Text = Text
            GridView1.AddNewRow()
            Dim rowHandle As Integer = GridView1.GetRowHandle(GridView1.DataRowCount)
            If GridView1.IsNewItemRow(rowHandle) Then
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(0), GridView1.RowCount)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(1), EnrNum)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(2), Paycode)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(3), name)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), verifymode)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(5), PunchDate.ToString("dd MMM yyyy"))
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(6), PunchDate.ToString("HH:mm"))
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(7), machine)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(8), deviceId)
            End If
            lblTotal.Text = ("Total : " & GridView1.RowCount)
            Cn.Remove_Duplicate_Punches(PunchDate)
            Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PAYCODE = '" & Paycode & "'"
            ds = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSqltmp, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                'Dim mindatetmp As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                    Cn.Process_AllRTC(PunchDate.AddDays(-1), Now.Date, Paycode, Paycode)
                Else
                    Cn.Process_AllnonRTC(PunchDate, Now.Date, Paycode, Paycode)
                End If
                XtraMaster.LabelControl4.Text = ""
                Application.DoEvents()
            End If
        End If
    End Sub
    Public Sub oplogprocess(ByVal sBuffer As String)
        Try
            Dim opindex As Integer = sBuffer.IndexOf("" & vbLf, 1)
            Dim opstr As String = ""
            If (opindex > 0) Then
                opstr = sBuffer.Substring(0, opindex)
            End If

            saveoplog(opstr)
            Dim endop As String = sBuffer.Substring((opindex + 1))
            If (endop <> "") Then
                oplogprocess(endop)
            End If
        Catch
        End Try
    End Sub
    ' Display AttLog in customized format    ZK
    Public Sub saveoplog(ByVal oplog As String)
        If (oplog.IndexOf("PLOG", 1) <= 0) Then
            Return
        End If

        Dim Optype As String = oplog.Substring(0, oplog.IndexOf("" & vbTab, 1))
        Dim Optype_tem As String = Optype.Substring(6)
        Dim stillop1 As String = oplog.Substring((oplog.IndexOf("" & vbTab, 1) + 1))
        Dim admin As String = stillop1.Substring(0, stillop1.IndexOf("" & vbTab, 1))
        Dim stillop2 As String = stillop1.Substring((stillop1.IndexOf("" & vbTab, 1) + 1))
        Dim optime As String = stillop2.Substring(0, stillop2.IndexOf("" & vbTab, 1))
        Dim stillop3 As String = stillop2.Substring((stillop2.IndexOf("" & vbTab, 1) + 1))
        Dim opnum1 As String = stillop3.Substring(0, stillop3.IndexOf("" & vbTab, 1))
        Dim stillop4 As String = stillop3.Substring((stillop3.IndexOf("" & vbTab, 1) + 1))
        Dim opnum2 As String = stillop4.Substring(0, stillop4.IndexOf("" & vbTab, 1))
        Dim stillop5 As String = stillop4.Substring((stillop4.IndexOf("" & vbTab, 1) + 1))
        Dim opnum3 As String = stillop5.Substring(0, stillop5.IndexOf("" & vbTab, 1))
        Dim stillop6 As String = stillop5.Substring((stillop5.IndexOf("" & vbTab, 1) + 1))
        Dim opnum4 As String = stillop6
        Dim Optype_tem_de As String = Optype_tem.TrimEnd
        Dim Optype_de As String = ""
        Select Case (Optype_tem_de)
            Case "0"
                Optype_de = "Power ON Device"
            Case "1"
                Optype_de = "Power OFF Device"
            Case "2"
                Optype_de = "Fail Verification"
            Case "3"
                Optype_de = "Generate an Alarm"
            Case "4"
                Optype_de = "Enter Menu"
            Case "5"
                Optype_de = "Modify Configuration"
            Case "6"
                Optype_de = "Enroll FingerPrint"
            Case "7"
                Optype_de = "Enroll Password"
            Case "8"
                Optype_de = "Enroll HID Card"
            Case "9"
                Optype_de = "Delete one User"
            Case "10"
                Optype_de = "Delete FingerPrint"
            Case "11"
                Optype_de = "Delete Password"
            Case "12"
                Optype_de = "Delete RF Card"
            Case "13"
                Optype_de = "Purge Data"
            Case "14"
                Optype_de = "Create MF Card"
            Case "15"
                Optype_de = "Enroll MF Card"
            Case "16"
                Optype_de = "Register MF Card"
            Case "17"
                Optype_de = "Delete MF Card"
            Case "18"
                Optype_de = "Clean MF Card"
            Case "19"
                Optype_de = "Copy Data to Card"
            Case "20"
                Optype_de = "Copy C. D. to Device"
            Case "21"
                Optype_de = "Set New Time"
            Case "22"
                Optype_de = "Factory Setting"
            Case "23"
                Optype_de = "Delete Att. Records"
            Case "24"
                Optype_de = "Clean Admin. Privilege"
            Case "25"
                Optype_de = "Modify Group Settings"
            Case "26"
                Optype_de = "Modify User AC Settings"
            Case "27"
                Optype_de = "Modify Time Zone"
            Case "28"
                Optype_de = "Modify Unlock Settings"
            Case "29"
                Optype_de = "Open Door Lock"
            Case "30"
                Optype_de = "Enroll one User"
            Case "31"
                Optype_de = "Modify FP Template"
            Case "32"
                Optype_de = "Duress Alarm"
            Case "68"
                Optype_de = "User pic"
        End Select

        '' Display oplog in customized format
        'Me.WriteOplog(Optype_de, admin, optime, opnum1, opnum2, opnum3, opnum4)
        'If (tabOptions.SelectedIndex = 1) Then
        '    lblRecords.Text = "OpLogs Count:"
        '    lblCount.Text = lvOpLog.Items.Count.ToString
        'End If

    End Sub
    Public Sub enfplogprocess(ByVal sBuffer As String)
        Try
            Dim enfpindex As Integer = sBuffer.IndexOf("" & vbLf, 1)
            Dim enfpstr As String = ""
            If (enfpindex > 0) Then
                enfpstr = sBuffer.Substring(0, enfpindex)
            End If
            saveenfplog(enfpstr)
            Dim endop As String = sBuffer.Substring((enfpindex + 1))
            If (endop <> "") Then
                enfplogprocess(endop)
            End If
        Catch
        End Try
    End Sub
    Public Sub saveenfplog(ByVal enfplog As String)
        'if (enfplog.IndexOf("PIN", 0) > 0)
        If ((enfplog.IndexOf("FP PIN", 0) >= 0) _
                    AndAlso (enfplog.IndexOf("FID", 0) > 0)) Then
            Dim EnFPid As String = enfplog.Substring(0, enfplog.IndexOf("" & vbTab, 0))
            Dim stillenfp1 As String = enfplog.Substring((enfplog.IndexOf("" & vbTab, 0) + 1))
            Dim enfpnum1 As String = stillenfp1.Substring(0, stillenfp1.IndexOf("" & vbTab, 0))
            Dim stillenfp2 As String = stillenfp1.Substring((stillenfp1.IndexOf("" & vbTab, 0) + 1))
            Dim enfpnum2 As String = stillenfp2.Substring(0, stillenfp2.IndexOf("" & vbTab, 0))
            Dim fplen As String = ""
            fplen = enfpnum2.Substring(5)
            Dim stillenfp3 As String = stillenfp2.Substring((stillenfp2.IndexOf("" & vbTab, 0) + 1))
            Dim enfpnum3 As String = stillenfp3.Substring(0, stillenfp3.IndexOf("" & vbTab, 0))
            Dim stillenfp4 As String = stillenfp3.Substring((stillenfp3.IndexOf("" & vbTab, 0) + 1))
            Dim enfpnum4 As String = stillenfp4.Substring(0, Convert.ToInt32(fplen))
            '' Display enfplog in customized format
            'Me.WriteEnFPlog(EnFPid.Replace("FP PIN=", ""), enfpnum1.Replace("FID=", ""), enfpnum2.Replace("Size=", ""), enfpnum3.Replace("Valid=", ""), enfpnum4.Replace("TMP=", ""))
            'If (tabOptions.SelectedIndex = 3) Then
            '    lblRecords.Text = "Enroll FP Count:"
            '    lblCount.Text = lvEnrollFP.Items.Count.ToString
            'End If
        End If
    End Sub
    ' Get Host IP
    Protected Function GetIP() As String
        Dim ipHost As IPHostEntry = Dns.Resolve(Dns.GetHostName)
        Dim ipAddr As IPAddress = ipHost.AddressList(0)
        Return ipAddr.ToString
    End Function



    Public Sub cdataProcess(ByVal bReceive As Byte(), ByVal connection As tcpServer.TcpServerConnection, ByVal machineSN As String)
        Dim sBuffer As String = Encoding.ASCII.GetString(bReceive)
        Dim sHttpVersion As String = ""
        Dim sMimeType1 As String = "text/plain"
        Dim SendLength As Integer = 0
        sHttpVersion = sBuffer.Substring(sBuffer.IndexOf("HTTP", 1), 8)

        If sBuffer.Substring(0, 3) = "GET" Then
            Dim SN As String = ""
            Dim Stamp As String = "", OpStamp As String = "", PhotoStamp As String = "", ErrorDelay As String = "30", Delay As String = "15", TransTimes As String = "", TransInterval As String = "", TransFlag As String = "1111101111", Realtime As String = "", Encrypt As String = "", TimeZoneclock As String = ""
            Dim machineSNTmp As String = sBuffer.Substring(sBuffer.IndexOf("SN=") + 3)
            Me.getnumber(machineSNTmp, SN)

            If ATTLOGStamp.Length > 0 Then
                Stamp = ATTLOGStamp
            Else
                Stamp = "1"
            End If

            Realtime = "1"
            TransTimes = "00:00;14:05"
            Encrypt = "0"
            TimeZoneclock = "1"
            TransInterval = "1"

            If OPERLOGStamp.Length > 0 Then
                Stamp = OPERLOGStamp
            Else
                Stamp = "1"
            End If

            Realtime = "1"
            TransTimes = "00:00;14:05"
            Encrypt = "0"
            TimeZoneclock = "1"
            TransInterval = "1"

            If USERINFOStamp.Length > 0 Then
                Stamp = USERINFOStamp
            Else
                Stamp = "1"
            End If

            Realtime = "1"
            TransTimes = "00:00;14:05"
            Encrypt = "0"
            TimeZoneclock = "1"
            TransInterval = "1"
            Dim Time_Zone As String = "0"
            Dim optionConfig As String = "GET OPTION FROM:" & SN & vbCrLf & "Stamp=" & 1 & vbCrLf & "OpStamp=" & OpStamp & vbCrLf & _
               "PhotoStamp=" & PhotoStamp & vbCrLf & "ErrorDelay=" & ErrorDelay & vbCrLf & "Delay=" & Delay & vbCrLf & _
               "TransTimes=" & TransTimes & vbCrLf & "TransInterval=" & TransInterval & vbCrLf & "TransFlag=" & TransFlag & vbCrLf & _
               "Realtime=" & Realtime & vbCrLf & "Encrypt=" & Encrypt & vbCrLf & "TimeZoneclock=" & TimeZoneclock & vbCrLf & _
            "TimeZone=" & Time_Zone & vbCrLf

            Dim bOption As Byte() = Encoding.GetEncoding("gb2312").GetBytes(optionConfig)
            SendLength = bOption.Length
            SendHeader(sHttpVersion, sMimeType1, SendLength, " 200 OK", connection)
            SendToBrowser(bOption, connection)
            connection.Socket.Close()
            Return
        End If

        If sBuffer.Substring(0, 4) = "POST" Then

            If sBuffer.IndexOf("Stamp", 1) > 0 AndAlso sBuffer.IndexOf("OPERLOG", 1) < 0 AndAlso sBuffer.IndexOf("ATTLOG", 1) > 0 AndAlso sBuffer.IndexOf("OPLOG", 1) < 0 Then
                attlog(sBuffer, machineSN)
            End If

            'If sBuffer.IndexOf("Stamp", 1) > 0 AndAlso sBuffer.IndexOf("OPERLOG", 1) > 0 AndAlso sBuffer.IndexOf("OPLOG", 1) > 0 Then
            '    oplog(sBuffer)
            'End If

            'If sBuffer.IndexOf("Stamp", 1) > 0 AndAlso sBuffer.IndexOf("OPERLOG", 1) > 0 AndAlso sBuffer.IndexOf("OPLOG 6", 1) > 0 Then
            '    enfplog(sBuffer)
            'End If

            'If sBuffer.IndexOf("Stamp", 1) > 0 AndAlso sBuffer.IndexOf("USERINFO", 1) > 0 Then
            '    usinlog(sBuffer)
            'End If

            Dim replyok As Byte() = Encoding.ASCII.GetBytes("OK")
            SendLength = replyok.Length
            SendHeader(sHttpVersion, sMimeType1, SendLength, " 200 OK", connection)
            SendToBrowser(replyok, connection)
            connection.Socket.Close()
        End If
    End Sub
    Public Sub Analysis(ByVal bReceive As Byte(), ByVal connection As tcpServer.TcpServerConnection)
        Dim strReceive As String = Encoding.ASCII.GetString(bReceive)
        Dim temp As String = System.Text.Encoding.Default.GetString(bReceive)
        Console.WriteLine(strReceive)
        'DEBUGWIN.Text += strReceive
        'DEBUGWIN.[Select](DEBUGWIN.TextLength, 0)
        'DEBUGWIN.ScrollToCaret()
        Dim subReceive As String = strReceive.Substring(0, 70)
        Dim machineSN As String = GetDeviceSerialNumber(subReceive)



        If strReceive.IndexOf("cdata?") > 0 Then
            cdataProcess(bReceive, connection, machineSN)
            Return
        End If

        If strReceive.IndexOf("getrequest?") > 0 Then
            getrequestProcess(bReceive, connection)
            Return
        End If

        If strReceive.IndexOf("devicecmd?") > 0 Then
            getrequestProcess(bReceive, connection)
            devicecmdProcess(bReceive)
            Return
        End If

        If strReceive.IndexOf("fdata?") > 0 Then
            Dim imgrecindex As Integer = 0

            For i As Integer = 0 To bReceive.Length - 1
                imgrecindex = i
                If CInt(bReceive(i)) = 0 Then Exit For
            Next

            imgrecindex += 1
            getrequestProcess(bReceive, connection)
            Dim imgReceive As Byte() = New Byte(bReceive.Length - imgrecindex - 1) {}
            Array.Copy(bReceive, imgrecindex, imgReceive, 0, bReceive.Length - imgrecindex)
            Dim path As String = System.Environment.CurrentDirectory & "\RealTimeImages"
            PathofImage = path
            path += "\" & strImageNumber.Replace("PIN=", "")
            System.IO.File.WriteAllBytes(path, imgReceive)

            If PathofImage IsNot Nothing AndAlso PathofImage <> "" Then
                Try
                    'picAttPhoto.Image = Image.FromFile(path)
                    'picAttPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
                    'Me.picAttPhoto.Update()
                Catch
                End Try
            End If

            Return
        End If
    End Sub
    Public Sub getnumber(ByVal sBuffer As String, ByRef numberstr As String)
        numberstr = ""
        For i As Integer = 0 To sBuffer.Length - 1
            Try
                If sBuffer(i).ToString > 47 AndAlso sBuffer(i).ToString < 58 Then
                    numberstr += sBuffer(i)
                Else
                    Exit For
                End If
            Catch ex As Exception

            End Try
        Next
    End Sub
    Public Sub getrequestProcess(ByVal bReceive As Byte(), ByVal connection As tcpServer.TcpServerConnection)
        Dim sBuffer As String = Encoding.GetEncoding("gb2312").GetString(bReceive)
        Dim sHttpVersion As String = ""
        Dim sMimeType1 As String = "text/plain", cmdstring As String = ""
        Dim SendLength As Integer = 0
        sHttpVersion = sBuffer.Substring(sBuffer.IndexOf("HTTP", 1), 8)
        'Dim SN As String = Nothing
        'Dim machineSN As String = sBuffer.Substring(sBuffer.IndexOf("SN=") + 3)
        'Me.getnumber(machineSN, SN)
        cmdstring = "OK"

        If sBuffer.Substring(0, 3) = "GET" AndAlso sBuffer.Substring(12, 10) = "getrequest" Then

            If sBuffer.IndexOf("INFO") > 0 Then
                cmdstring = "OK"
            ElseIf sBuffer.IndexOf("options") > 0 Then
                cmdstring = "OK"
            ElseIf flagreboot = True Then
                cmdstring = "C:ID1:REBOOT" & vbLf
                flagreboot = False
            ElseIf flagshell = True Then
                cmdstring = "C:ID10:SHELL " & shellinput & vbLf
                flagshell = False
                shellinput = ""
            ElseIf flagcheck = True Then
                cmdstring = "C:20:CHECK" & vbLf
                flagcheck = False
            ElseIf flaginfo = True Then
                cmdstring = "C:ID30:INFO ls" & vbLf
                flaginfo = False
            ElseIf flagoption = True Then
                cmdstring = "C:ID40:SET OPTION " & optioninput & vbLf
                flagoption = False
                optioninput = ""
            ElseIf flagclear = True Then
                cmdstring = "C:ID50:" & clearinput & vbLf
                flagclear = False
            ElseIf flagupduserinfo = True Then
                cmdstring = "C:ID60:" & upduserinfoinput & vbLf
                flagupduserinfo = False
                'ElseIf flagusersms = True Then
                '    cmdstring = "C:ID70:" & usersmsinput & vbLf
                '    flagusersms = False
                'ElseIf flaguserpersonalsms = True Then
                '    cmdstring = "C:ID75:" & userpersonalsmsinput & vbLf
                '    flaguserpersonalsms = False
            ElseIf flagunlock = True Then
                cmdstring = "C:ID80:CHECK" & vbLf
                flagunlock = False
            ElseIf flagdeluserinfo = True Then
                cmdstring = "C:ID65:" & deluserinfoinput & vbLf
                flagdeluserinfo = False
                'ElseIf flagsetfp = True Then
                '    cmdstring = "C:ID45:" & setfpinput & vbLf
                '    flagsetfp = False
                'ElseIf flagdelfp = True Then
                '    cmdstring = "C:ID55:" & delfpinput & vbLf
                '    flagdelfp = False
            End If
        End If

        If sBuffer.Substring(0, 4) = "POST" Then
            If sBuffer.IndexOf("Stamp", 1) > 0 AndAlso (sBuffer.IndexOf("Photo", 1) > 0 OrElse sBuffer.IndexOf("ATTPHOTO", 1) > 0) AndAlso sBuffer.IndexOf("OPLOG", 1) < 0 Then
                attpholog(sBuffer)
            End If
        End If

        Dim replyok As Byte() = Encoding.GetEncoding("gb2312").GetBytes(cmdstring)
        SendLength = replyok.Length
        SendHeader(sHttpVersion, sMimeType1, SendLength, " 200 OK", connection)
        SendToBrowser(replyok, connection)
        connection.Socket.Close()
        cmdstring = ""
    End Sub
    Public Sub devicecmdProcess(ByVal bReceive As Byte())
        Dim sBuffer As String = Encoding.ASCII.GetString(bReceive)
        Dim strReceive As String = sBuffer

        If strReceive.IndexOf("Return=0") <= 0 Then
            MessageBox.Show("Error! Please try again!", "OK")
            Return
        End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=Shell") > 0 Then
        '    cmdshell(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=CHECK") > 0 AndAlso strReceive.IndexOf("ID=ID20") > 0 Then
        '    cmdcheck(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=INFO") > 0 Then
        '    cmdinfo(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=SET") > 0 Then
        '    cmdoption(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=CLEAR") > 0 Then
        '    cmdclear(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=DATA") > 0 AndAlso strReceive.IndexOf("ID=ID60") > 0 Then
        '    cmdudpuserinfo(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=DATA") > 0 AndAlso strReceive.IndexOf("ID=ID65") > 0 Then
        '    cmddeluserinfo(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=DATA") > 0 AndAlso strReceive.IndexOf("ID=ID70") > 0 Then
        '    cmdusersms(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=DATA") > 0 AndAlso strReceive.IndexOf("ID=ID75") > 0 Then
        '    cmduserpersonalsms(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=CHECK") > 0 AndAlso strReceive.IndexOf("ID=ID80") > 0 Then
        '    cmdunlock(sBuffer)
        '    Return
        'End If
    End Sub
    Public Sub SendHeader(ByVal sHttpVersion As String, ByVal sMIMEHeader As String, ByVal iTotBytes As Integer, ByVal sStatusCode As String, ByRef connection As tcpServer.TcpServerConnection)
        Dim sBuffer As String = Nothing

        If sMIMEHeader.Length = 0 Then
            sMIMEHeader = "text/html"
        End If

        sBuffer = sBuffer & sHttpVersion & sStatusCode & vbCrLf
        sBuffer = sBuffer & "Server: cx1193719-b" & vbCrLf
        sBuffer = sBuffer & "Content-Type: " & sMIMEHeader & vbCrLf
        sBuffer = sBuffer & "Accept-Ranges: bytes" & vbCrLf
        'nitin original
        'Dim WeekDay As String = DateTime.UtcNow.DayOfWeek.ToString().Remove(3)  'DateTime.Now.DayOfWeek.ToString().Remove(3) '
        'Dim Month As String = DateTime.UtcNow.Month.ToString()  'DateTime.Now.Month.ToString() '

        'new
        Dim TimeZone As Integer = 330
        Dim WeekDay As String = DateTime.UtcNow.AddMinutes(TimeZone).DayOfWeek.ToString.Remove(3)
        Dim Month As String = DateTime.UtcNow.AddMinutes(TimeZone).Month.ToString

        If Month = "1" Then Month = "Jan"
        If Month = "2" Then Month = "Feb"
        If Month = "3" Then Month = "Mar"
        If Month = "4" Then Month = "Apr"
        If Month = "5" Then Month = "May"
        If Month = "6" Then Month = "Jun"
        If Month = "7" Then Month = "Jul"
        If Month = "8" Then Month = "Aug"
        If Month = "9" Then Month = "Sep"
        If Month = "10" Then Month = "Oct"
        If Month = "11" Then Month = "Nov"
        If Month = "12" Then Month = "Dec"
        ''nitin original
        'Dim Hour As String = DateTime.UtcNow.Hour.ToString() 'DateTime.Now.Hour.ToString() ' 
        'Dim Minute As String = DateTime.UtcNow.Minute.ToString() 'DateTime.Now.Minute.ToString() '
        'Dim Second As String = DateTime.UtcNow.Second.ToString()  'DateTime.Now.Second.ToString() '

        'new
        Dim DT As DateTime = DateTime.UtcNow.AddMinutes(TimeZone)
        Dim Hour As String = DT.Hour.ToString
        Dim Minute As String = DT.Minute.ToString
        Dim Second As String = DT.Second.ToString

        If Hour.Length = 1 Then Hour = "0" & Hour
        If Minute.Length = 1 Then Minute = "0" & Minute
        If Second.Length = 1 Then Second = "0" & Second
        'sBuffer = sBuffer & "Date: " & WeekDay & ", " & DateTime.UtcNow.Day & " " & Month & " " & DateTime.UtcNow.Year & " " & Hour & ":" & Minute & ":" & Second & " GMT" & vbCrLf
        sBuffer = sBuffer & "Date: " & WeekDay & ", " & DateTime.Now.Day & " " & Month & " " & DateTime.Now.Year & " " & Hour & ":" & Minute & ":" & Second & " GMT" & vbCrLf
        sBuffer = sBuffer & "Content-Length: " & iTotBytes & vbCrLf & vbCrLf
        Dim bSendData As Byte() = Encoding.GetEncoding("gb2312").GetBytes(sBuffer)
        SendToBrowser(bSendData, connection)
    End Sub
    Public Sub SendToBrowser(ByVal sData As String, ByRef connection As tcpServer.TcpServerConnection)
        SendToBrowser(Encoding.GetEncoding("gb2312").GetBytes(sData), connection)
    End Sub
    Public Sub SendToBrowser(ByVal bSendData As Byte(), ByRef connection As tcpServer.TcpServerConnection)
        Dim numBytes As Integer = 0
        Try
            If connection.Socket.Connected Then
                numBytes = connection.Socket.Client.Send(bSendData, bSendData.Length, 0)
                'If mySocket.Send(bSendData, bSendData.Length, 0) = -1 Then
                If numBytes = -1 Then
                    MessageBox.Show("Socket Error: Cannot Send Packet", "Error", MessageBoxButtons.OK, MessageBoxIcon.[Error])
                Else
                End If
            Else
                MessageBox.Show("Link Failed...", "Error", MessageBoxButtons.OK, MessageBoxIcon.[Error])
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.[Error])
        End Try
    End Sub
    Public Sub attpholog(ByVal sBuffer As String)
        Dim machineSN As String = sBuffer.Substring(sBuffer.IndexOf("SN=") + 3)
        Dim SN As String = ""
        Me.getnumber(machineSN, SN)
        Dim machinestamp As String = sBuffer.Substring(sBuffer.IndexOf("Stamp=") + 6)
        Dim Stamp As String = ""
        Me.getnumber(machinestamp, Stamp)
        ATTPHOTOStamp = Stamp
        Dim attphoindex As Integer = sBuffer.IndexOf(vbCrLf & vbCrLf, 1)
        Dim attphostr As String = sBuffer.Substring(attphoindex + 4)
        'for photo
        attphoprocess(attphostr)
    End Sub
    Public Sub attphoprocess(ByVal sBuffer As String)
        Try
            Dim attphoindex As Integer = sBuffer.IndexOf("" & vbLf, 1)
            Dim attphostr As String = ""
            If (attphoindex > 0) Then
                attphostr = sBuffer
            End If
            saveattpho(attphostr)
            Dim endattpho As String = sBuffer.Substring((attphoindex + 1))
            If ((endattpho <> "") _
                        AndAlso (Not (endattpho) Is Nothing)) Then
                attphoprocess(endattpho)
            End If
        Catch ex As System.Exception
        End Try
    End Sub
    Public Sub saveattpho(ByVal attpho As String)
        Dim imgtemp As String = attpho.Substring(0, attpho.IndexOf(vbNullChar, 1))
        Dim pintemp As String = imgtemp.Substring(0, imgtemp.IndexOf("" & vbLf, 1))
        Dim attphostill1 As String = imgtemp.Substring((imgtemp.IndexOf("" & vbLf, 1) + 1))
        Dim sntemp As String = attphostill1.Substring(0, attphostill1.IndexOf("" & vbLf, 1))
        Dim attphostill2 As String = attphostill1.Substring((attphostill1.IndexOf("" & vbLf, 1) + 1))
        Dim sizetemp As String = attphostill2.Substring(0, attphostill2.IndexOf("" & vbLf, 1))
        Dim attphostill3 As String = attphostill2.Substring((attphostill2.IndexOf("" & vbLf, 1) + 1))
        Dim cmdtemp As String = attphostill3
        '' Display AttPhoto in customized format
        'Me.WriteAttPholog(pintemp.Replace("PIN=", ""), sizetemp.Replace("size=", ""), cmdtemp.Replace("CMD=", ""))
        'If (tabOptions.SelectedIndex = 4) Then
        '    lblRecords.Text = "AttPhotoLogs Count:"
        '    lblCount.Text = lvAttPhoto.Items.Count.ToString
        'End If

        strImageNumber = pintemp
    End Sub
    '' Write AttPhoto for iclock Device
    'Public Delegate Sub CallBackAttPhoto(ByVal PhotoPinTemp As String, ByVal PhotoSizeTemp As String, ByVal PhotoCmdTemp As String)
    'Private Sub WriteAttPholog(ByVal PhotoPinTemp As String, ByVal PhotoSizeTemp As String, ByVal PhotoCmdTemp As String)
    '    If Me.lvAttPhoto.InvokeRequired Then
    '        Dim write As CallBackAttPhoto = New CallBackAttPhoto(WriteAttPholog)
    '        Me.Invoke(write, New Object() {PhotoPinTemp, PhotoSizeTemp, PhotoCmdTemp})
    '    Else
    '        lvAttPhoto.Items.Add(PhotoPinTemp)
    '        lvAttPhoto.Items((lvAttPhoto.Items.Count - 1)).SubItems.Add(PhotoSizeTemp)
    '        lvAttPhoto.Items((lvAttPhoto.Items.Count - 1)).SubItems.Add(PhotoCmdTemp)
    '        ' Select the last one (correspond with the Photo)
    '        lvAttPhoto.Items((lvAttPhoto.Items.Count - 1)).Selected = True
    '    End If

    'End Sub
    'zk listening end
    Private Sub SimpleButtonClear_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonClear.Click
        Me.fnCount = 0
        lblTotal.Text = "Total : 0"
        Me.SetupLogListview()
    End Sub

    Private Sub XtraRealTimePunches_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            tcpServer1.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub tcpServer1_OnDataAvailable(connection As tcpServer.TcpServerConnection) Handles tcpServer1.OnDataAvailable
        Thread.Sleep(500)
        Dim databyte() As Byte
        Try
            databyte = readStream(connection.Socket)
            Dim strReceive As String = Encoding.ASCII.GetString(databyte)
            If (databyte.Length > 0) Then
                'Thread AnalysisThread = new Thread(() => this.Analysis(databyte, connection));
                'AnalysisThread.Start();                  
                Analysis(databyte, connection)
            End If
        Catch ex As Exception
            Return
        End Try
    End Sub
    Protected Function readStream(client As TcpClient) As Byte()
        Try
            Dim stream As NetworkStream = client.GetStream()
            If stream.DataAvailable Then
                Dim data As Byte() = New Byte(client.Available - 1) {}
                Dim bytesRead As Integer = 0
                Try
                    bytesRead = stream.Read(data, 0, data.Length)
                Catch generatedExceptionName As IOException
                End Try
                If bytesRead < data.Length Then
                    Dim lastData As Byte() = data
                    data = New Byte(bytesRead - 1) {}
                    Array.ConstrainedCopy(lastData, 0, data, 0, bytesRead)
                End If
                Return data
            End If
            Return Nothing
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function GetDeviceSerialNumber(ByVal strReceive As String) As String
        Dim SerialNo As String = ""
        Try
            Dim FirstIndex As Integer = 0
            FirstIndex = strReceive.IndexOf("?SN=")
            Dim FistPart As String = strReceive.Substring(FirstIndex, 30)
            Dim SecondIndexs As Integer = 0
            If (FistPart.IndexOf(" ") > 0) Then
                SecondIndexs = FistPart.IndexOf(" ")
            Else
                SecondIndexs = FistPart.IndexOf("&")
            End If

            SerialNo = FistPart.Substring(4, (SecondIndexs - 4))
        Catch
            SerialNo = ""
        End Try

        'MessageBox.Show(SerialNo);
        Return SerialNo
    End Function
End Class
