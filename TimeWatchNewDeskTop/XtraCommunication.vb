﻿Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO

Public Class XtraCommunication
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info

    'Dim servername As String
    'Dim con As SqlConnection
    'Dim con1 As OleDbConnection
    Dim ConnectionString As String
    Public Sub New()
        InitializeComponent()
        'Dim fs As FileStream = New FileStream("db.txt", FileMode.Open, FileAccess.Read)
        'Dim sr As StreamReader = New StreamReader(fs)
        'Dim str As String
        'Dim str1() As String
        'Do While sr.Peek <> -1
        '    str = sr.ReadLine
        '    str1 = str.Split(",")
        '    servername = str1(0)
        'Loop
        'sr.Close()
        'fs.Close()

        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            'Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            TblMachineTableAdapter1.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            'Me.TblMachineTableAdapter1.Fill(Me.SSSDBDataSet.tblMachine)
        End If




        If Common.servername = "Access" Then
            GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblMachine1
            GridLookUpEdit1.Properties.DisplayMember = SSSDBDataSet.tblMachine1.LOCATIONColumn.ColumnName
        Else
            GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblMachine
            GridLookUpEdit1.Properties.DisplayMember = SSSDBDataSet.tblMachine.LOCATIONColumn.ColumnName
        End If
    End Sub
    Private Sub XtraUserControl1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.Width = SplitContainerControl1.Parent.Width  'Common.splitforMasterMenuWidth '
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100 
        'MsgBox(SplitContainerControl1.Width)
        'MsgBox(Common.SplitterPosition)
        'MsgBox(SplitContainerControl1.SplitterPosition)

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraDevice).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")
        LabelControl1.Text = Common.res_man.GetString("selectdevice", Common.cul)
        LabelControl2.Text = Common.res_man.GetString("download_from_all", Common.cul)
        LabelControl3.Text = Common.res_man.GetString("delete_from_device", Common.cul)
        ToggleSwitch1.Properties.OffText = Common.res_man.GetString("off", Common.cul)
        ToggleSwitch1.Properties.OnText = Common.res_man.GetString("onn", Common.cul)
        ToggleSwitch2.Properties.OffText = Common.res_man.GetString("off", Common.cul)
        ToggleSwitch2.Properties.OnText = Common.res_man.GetString("onn", Common.cul)
        CheckEdit1.Text = Common.res_man.GetString("oldlog", Common.cul)
        CheckEdit2.Text = Common.res_man.GetString("newlog", Common.cul)
        SimpleButton1.Text = Common.res_man.GetString("readdate", Common.cul)
        SimpleButton2.Text = Common.res_man.GetString("setdate", Common.cul)
        SimpleButton3.Text = Common.res_man.GetString("downloadlogs", Common.cul)
        GridColumn1.Caption = Common.res_man.GetString("controller_id", Common.cul)
        GridColumn2.Caption = Common.res_man.GetString("device_ip", Common.cul)
        GridColumn3.Caption = Common.res_man.GetString("location", Common.cul)


    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim displayValue = GridLookUpEdit1.Properties.GetDisplayValueByKeyValue(GridLookUpEdit1.EditValue)
        Dim displayValue1 = GridLookUpEdit1.Properties.GetRowByKeyValue(GridLookUpEdit1.EditValue)
        MsgBox(displayValue1.ToString)


        Dim edit As DevExpress.XtraEditors.GridLookUpEdit = CType(sender, DevExpress.XtraEditors.GridLookUpEdit)
        Dim row As DataRow = edit.Properties.View.GetDataRow(edit.Properties.View.FocusedRowHandle)

        MsgBox(row.Item(0).ToString)
    End Sub
    Private Sub GridLookUpEdit1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GridLookUpEdit1.EditValueChanged
        Try
            Dim edit As DevExpress.XtraEditors.GridLookUpEdit = CType(sender, DevExpress.XtraEditors.GridLookUpEdit)
            Dim row As DataRow = edit.Properties.View.GetDataRow(edit.Properties.View.FocusedRowHandle)
            'your code here
            MsgBox(row.Item(4).ToString)

        Catch ex As Exception

        End Try
        
    End Sub

End Class
