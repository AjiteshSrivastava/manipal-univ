﻿Imports System.IO
Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Public Class XtraCustomisedReportEdit
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim pageload As Boolean = True
    Public Sub New()
        InitializeComponent()        
    End Sub
    Private Sub XtraCustomisedReportEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        pageload = True
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        GroupControl2.Width = GroupControl2.Parent.Width / 2
        loadData()
        pageload = False
        'loadData()
        If Common.IsNepali = "Y" Then
            TextDateFormat.Visible = False
            LabelControl18.Visible = False
        Else
            TextDateFormat.Visible = True
            LabelControl18.Visible = True
        End If
    End Sub
    Private Sub loadData()
        Dim sSql As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        sSql = "select * from CustomisedReport"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            CheckEmpPay.EditValue = "N"
            CheckEmpName.EditValue = "N"
            CheckUserNo.EditValue = "N"
            CheckCompany.EditValue = "N"
            CheckLocation.EditValue = "N"
            CheckDept.EditValue = "N"
            CheckGrade.EditValue = "N"
            CheckCat.EditValue = "N"
            CheckEmpGrp.EditValue = "N"

            CheckShift.EditValue = "N"
            CheckShiftStart.EditValue = "N"
            CheckShiftEnd.EditValue = "N"
            CheckIN1.EditValue = "N"
            CheckOut1.EditValue = "N"
            CheckIN2.EditValue = "N"
            CheckOut2.EditValue = "N"
            CheckStatus.EditValue = "N"
            CheckHourWorked.EditValue = "N"
            CheckLate.EditValue = "N"
            CheckEarlyDeparture.EditValue = "N"
            CheckOT.EditValue = "N"
            CheckOS.EditValue = "N"
            CheckGName.EditValue = "N"
            CheckDes.EditValue = "N"
            CheckDoj.EditValue = "N"
            CheckGender.EditValue = "N"


            ComboBKType.SelectedIndex = 0
            CheckPrefixPreCard.EditValue = "N"
            CheckPrefixPrePaycode.EditValue = "N"
            CheckEditUID.EditValue = "N"

            TextPayCap.Text = ""
            TextNameCap.Text = ""
            TextUserNoCap.Text = ""
            TextComCap.Text = ""
            TextLocCap.Text = ""
            TextDeptCap.Text = ""
            TextGradeCap.Text = ""
            TextCatCap.Text = ""
            TextEmpGrpCap.Text = ""
            TextSAttCap.Text = ""
            TextShiftStartCap.Text = ""
            TextShiftEndCap.Text = ""
            TextIn1Cap.Text = ""
            TextOut1Cap.Text = ""
            TextIn2Cap.Text = ""
            TextOut2Cap.Text = ""
            TextStatusCap.Text = ""
            TextHourWorkedCap.Text = ""
            TextLateCap.Text = ""
            TextEarlyDCap.Text = ""
            TextOTCap.Text = ""
            TextOSCap.Text = ""
            TextGNameCap.Text = ""
            TextDesCap.Text = ""
            TextDojCap.Text = ""
            TextGenCap.Text = ""
            TextPayPos.Text = ""
            TextNamePos.Text = ""
            TextUserNoPos.Text = ""
            TextComPos.Text = ""
            TextLocPos.Text = ""
            TextDeptPos.Text = ""
            TextGradPos.Text = ""
            TextCatPos.Text = ""
            TextEmpGrpPos.Text = ""
            TextSAttPos.Text = ""
            TextShiftStartPos.Text = ""
            TextShiftEndPos.Text = ""
            TextIn1Pos.Text = ""
            TextOut1Pos.Text = ""
            TextIn2Pos.Text = ""
            TextOut2Pos.Text = ""
            TextStatusPos.Text = ""
            TextHourWorkedPos.Text = ""
            TextLatePos.Text = ""
            TextEarlyDPos.Text = ""
            TextOTPos.Text = ""
            TextOSPos.Text = ""
            TextGNamePos.Text = ""
            TextDesPos.Text = ""
            TextDojPos.Text = ""
            TextGenPos.Text = ""           
            TextDateFormat.Text = ""           
            TextPriFixLenghtPre.Text = ""
            TextPriTextPre.Text = ""            
            TextPriFixLenghtPay.Text = ""
            TextPriTextPay.Text = ""
            CheckDate.EditValue = "N"
            TextDateCap.Text = ""
            TextDatePos.Text = ""
            CheckExcessLunch.EditValue = "N"
            TextExcessLunchCap.Text = ""
            TextExcessLunchPos.Text = ""
            TextUidCap.Text = ""
            TextUidPos.Text = ""
        Else
            CheckEmpPay.EditValue = ds.Tables(0).Rows(0).Item("Paycode").ToString.Trim
            CheckEmpName.EditValue = ds.Tables(0).Rows(0).Item("Name").ToString.Trim
            CheckUserNo.EditValue = ds.Tables(0).Rows(0).Item("CardNumber").ToString.Trim
            CheckCompany.EditValue = ds.Tables(0).Rows(0).Item("Company").ToString.Trim
            CheckLocation.EditValue = ds.Tables(0).Rows(0).Item("Location").ToString.Trim
            CheckDept.EditValue = ds.Tables(0).Rows(0).Item("Department").ToString.Trim
            CheckGrade.EditValue = ds.Tables(0).Rows(0).Item("Grade").ToString.Trim
            CheckCat.EditValue = ds.Tables(0).Rows(0).Item("Catagory").ToString.Trim
            CheckEmpGrp.EditValue = ds.Tables(0).Rows(0).Item("EmpGrp").ToString.Trim

            CheckShift.EditValue = ds.Tables(0).Rows(0).Item("SHIFTATTENDED").ToString.Trim
            CheckShiftStart.EditValue = ds.Tables(0).Rows(0).Item("SHIFTSTARTTIME").ToString.Trim
            CheckShiftEnd.EditValue = ds.Tables(0).Rows(0).Item("SHIFTENDTIME").ToString.Trim
            CheckIN1.EditValue = ds.Tables(0).Rows(0).Item("IN1").ToString.Trim
            CheckOut1.EditValue = ds.Tables(0).Rows(0).Item("OUT1").ToString.Trim
            CheckIN2.EditValue = ds.Tables(0).Rows(0).Item("IN2").ToString.Trim
            CheckOut2.EditValue = ds.Tables(0).Rows(0).Item("OUT2").ToString.Trim
            CheckStatus.EditValue = ds.Tables(0).Rows(0).Item("STATUS").ToString.Trim
            CheckHourWorked.EditValue = ds.Tables(0).Rows(0).Item("HOURSWORKED").ToString.Trim
            CheckLate.EditValue = ds.Tables(0).Rows(0).Item("LATEARRIVAL").ToString.Trim
            CheckEarlyDeparture.EditValue = ds.Tables(0).Rows(0).Item("EARLYDEPARTURE").ToString.Trim
            CheckOT.EditValue = ds.Tables(0).Rows(0).Item("OTDURATION").ToString.Trim
            CheckOS.EditValue = ds.Tables(0).Rows(0).Item("OSDURATION").ToString.Trim

            CheckGName.EditValue = ds.Tables(0).Rows(0).Item("GuardianName").ToString.Trim
            CheckDes.EditValue = ds.Tables(0).Rows(0).Item("Designation").ToString.Trim
            CheckDoj.EditValue = ds.Tables(0).Rows(0).Item("DateOfJoining").ToString.Trim
            CheckGender.EditValue = ds.Tables(0).Rows(0).Item("Gender").ToString.Trim

            TextPayCap.Text = ds.Tables(0).Rows(0).Item("PaycodeCap").ToString.Trim
            TextNameCap.Text = ds.Tables(0).Rows(0).Item("NameCap").ToString.Trim
            TextUserNoCap.Text = ds.Tables(0).Rows(0).Item("CardNumberCap").ToString.Trim
            TextComCap.Text = ds.Tables(0).Rows(0).Item("CompanyCap").ToString.Trim
            TextLocCap.Text = ds.Tables(0).Rows(0).Item("LocationCap").ToString.Trim
            TextDeptCap.Text = ds.Tables(0).Rows(0).Item("DepartmentCap").ToString.Trim
            TextGradeCap.Text = ds.Tables(0).Rows(0).Item("GradeCap").ToString.Trim
            TextCatCap.Text = ds.Tables(0).Rows(0).Item("CatagoryCap").ToString.Trim
            TextEmpGrpCap.Text = ds.Tables(0).Rows(0).Item("EmpGrpCap").ToString.Trim
            TextSAttCap.Text = ds.Tables(0).Rows(0).Item("SHIFTATTENDEDCap").ToString.Trim
            TextShiftStartCap.Text = ds.Tables(0).Rows(0).Item("SHIFTSTARTTIMECap").ToString.Trim
            TextShiftEndCap.Text = ds.Tables(0).Rows(0).Item("SHIFTENDTIMECap").ToString.Trim
            TextIn1Cap.Text = ds.Tables(0).Rows(0).Item("IN1Cap").ToString.Trim
            TextOut1Cap.Text = ds.Tables(0).Rows(0).Item("OUT1Cap").ToString.Trim
            TextIn2Cap.Text = ds.Tables(0).Rows(0).Item("IN2Cap").ToString.Trim
            TextOut2Cap.Text = ds.Tables(0).Rows(0).Item("OUT2Cap").ToString.Trim
            TextStatusCap.Text = ds.Tables(0).Rows(0).Item("STATUSCap").ToString.Trim
            TextHourWorkedCap.Text = ds.Tables(0).Rows(0).Item("HOURSWORKEDCap").ToString.Trim
            TextLateCap.Text = ds.Tables(0).Rows(0).Item("LATEARRIVALCap").ToString.Trim
            TextEarlyDCap.Text = ds.Tables(0).Rows(0).Item("EARLYDEPARTURECap").ToString.Trim
            TextOTCap.Text = ds.Tables(0).Rows(0).Item("OTDURATIONCap").ToString.Trim
            TextOSCap.Text = ds.Tables(0).Rows(0).Item("OSDURATIONCap").ToString.Trim
            TextGNameCap.Text = ds.Tables(0).Rows(0).Item("GuardianNameCap").ToString.Trim
            TextDesCap.Text = ds.Tables(0).Rows(0).Item("DesignationCap").ToString.Trim
            TextDojCap.Text = ds.Tables(0).Rows(0).Item("DateOfJoiningCap").ToString.Trim
            TextGenCap.Text = ds.Tables(0).Rows(0).Item("GenderCap").ToString.Trim
            TextPayPos.Text = ds.Tables(0).Rows(0).Item("PaycodePos").ToString.Trim
            TextNamePos.Text = ds.Tables(0).Rows(0).Item("NamePos").ToString.Trim
            TextUserNoPos.Text = ds.Tables(0).Rows(0).Item("CardNumberPos").ToString.Trim
            TextComPos.Text = ds.Tables(0).Rows(0).Item("CompanyPos").ToString.Trim
            TextLocPos.Text = ds.Tables(0).Rows(0).Item("LocationPos").ToString.Trim
            TextDeptPos.Text = ds.Tables(0).Rows(0).Item("DepartmentPos").ToString.Trim
            TextGradPos.Text = ds.Tables(0).Rows(0).Item("GradePos").ToString.Trim
            TextCatPos.Text = ds.Tables(0).Rows(0).Item("CatagoryPos").ToString.Trim
            TextEmpGrpPos.Text = ds.Tables(0).Rows(0).Item("EmpGrpPos").ToString.Trim
            TextSAttPos.Text = ds.Tables(0).Rows(0).Item("SHIFTATTENDEDPos").ToString.Trim
            TextShiftStartPos.Text = ds.Tables(0).Rows(0).Item("SHIFTSTARTTIMEPos").ToString.Trim
            TextShiftEndPos.Text = ds.Tables(0).Rows(0).Item("SHIFTENDTIMEPos").ToString.Trim
            TextIn1Pos.Text = ds.Tables(0).Rows(0).Item("IN1Pos").ToString.Trim
            TextOut1Pos.Text = ds.Tables(0).Rows(0).Item("OUT1Pos").ToString.Trim
            TextIn2Pos.Text = ds.Tables(0).Rows(0).Item("IN2Pos").ToString.Trim
            TextOut2Pos.Text = ds.Tables(0).Rows(0).Item("OUT2Pos").ToString.Trim
            TextStatusPos.Text = ds.Tables(0).Rows(0).Item("STATUSPos").ToString.Trim
            TextHourWorkedPos.Text = ds.Tables(0).Rows(0).Item("HOURSWORKEDPos").ToString.Trim
            TextLatePos.Text = ds.Tables(0).Rows(0).Item("LATEARRIVALPos").ToString.Trim
            TextEarlyDPos.Text = ds.Tables(0).Rows(0).Item("EARLYDEPARTUREPos").ToString.Trim
            TextOTPos.Text = ds.Tables(0).Rows(0).Item("OTDURATIONPos").ToString.Trim
            TextOSPos.Text = ds.Tables(0).Rows(0).Item("OSDURATIONPos").ToString.Trim
            TextGNamePos.Text = ds.Tables(0).Rows(0).Item("GuardianNamePos").ToString.Trim
            TextDesPos.Text = ds.Tables(0).Rows(0).Item("DesignationPos").ToString.Trim
            TextDojPos.Text = ds.Tables(0).Rows(0).Item("DateOfJoiningPos").ToString.Trim
            TextGenPos.Text = ds.Tables(0).Rows(0).Item("GenderPos").ToString.Trim
            ComboBKType.EditValue = ds.Tables(0).Rows(0).Item("Format").ToString.Trim
            TextDateFormat.Text = ds.Tables(0).Rows(0).Item("DateFormat").ToString.Trim
            CheckPrefixPreCard.EditValue = ds.Tables(0).Rows(0).Item("PreIsPrifix").ToString.Trim
            TextPriFixLenghtPre.Text = ds.Tables(0).Rows(0).Item("PreLength").ToString.Trim
            TextPriTextPre.Text = ds.Tables(0).Rows(0).Item("PreText").ToString.Trim
            CheckPrefixPrePaycode.EditValue = ds.Tables(0).Rows(0).Item("PayIsPrefix").ToString.Trim
            TextPriFixLenghtPay.Text = ds.Tables(0).Rows(0).Item("PayLength").ToString.Trim
            TextPriTextPay.Text = ds.Tables(0).Rows(0).Item("PayText").ToString.Trim

            CheckDate.EditValue = ds.Tables(0).Rows(0).Item("DateCol").ToString.Trim
            TextDateCap.Text = ds.Tables(0).Rows(0).Item("DateColCap").ToString.Trim
            TextDatePos.Text = ds.Tables(0).Rows(0).Item("DateColPos").ToString.Trim

            CheckExcessLunch.EditValue = ds.Tables(0).Rows(0).Item("EXCLUNCHHOURS").ToString.Trim
            TextExcessLunchCap.Text = ds.Tables(0).Rows(0).Item("EXCLUNCHHOURSCap").ToString.Trim
            TextExcessLunchPos.Text = ds.Tables(0).Rows(0).Item("EXCLUNCHHOURSPos").ToString.Trim

            CheckEditUID.EditValue = ds.Tables(0).Rows(0).Item("TELEPHONE2").ToString.Trim
            TextUidCap.Text = ds.Tables(0).Rows(0).Item("TELEPHONE2Cap").ToString.Trim
            TextUidPos.Text = ds.Tables(0).Rows(0).Item("TELEPHONE2Pos").ToString.Trim

        End If
        If CheckPrefixPreCard.Checked Then
            TextPriFixLenghtPre.Enabled = True
            TextPriTextPre.Enabled = True
        Else
            TextPriFixLenghtPre.Enabled = False
            TextPriTextPre.Enabled = False
        End If
        If CheckPrefixPrePaycode.Checked Then
            TextPriFixLenghtPay.Enabled = True
            TextPriTextPay.Enabled = True
        Else
            TextPriFixLenghtPay.Enabled = False
            TextPriTextPay.Enabled = False
        End If
    End Sub
    Private Sub CheckPrefixPreCard_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckPrefixPreCard.CheckedChanged
        If CheckPrefixPreCard.Checked Then
            TextPriFixLenghtPre.Enabled = True
            TextPriTextPre.Enabled = True
        Else
            TextPriFixLenghtPre.Enabled = False
            TextPriTextPre.Enabled = False
        End If
    End Sub
    Private Sub CheckPrefixPrePaycode_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckPrefixPrePaycode.CheckedChanged
        If CheckPrefixPrePaycode.Checked Then
            TextPriFixLenghtPay.Enabled = True
            TextPriTextPay.Enabled = True
        Else
            TextPriFixLenghtPay.Enabled = False
            TextPriTextPay.Enabled = False
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim Paycode As String = CheckEmpPay.EditValue
        If Paycode = "N" Then
            XtraMessageBox.Show(ulf, "<size=10>Employee Paycode must be selected</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim Name As String = CheckEmpName.EditValue
        Dim CardNumber As String = CheckUserNo.EditValue
        Dim Company As String = CheckCompany.EditValue
        Dim Location As String = CheckLocation.EditValue
        Dim Department As String = CheckDept.EditValue
        Dim Grade As String = CheckGrade.EditValue
        Dim Catagory As String = CheckCat.EditValue
        Dim EmpGrp As String = CheckEmpGrp.EditValue
        Dim SHIFTATTENDED As String = CheckShift.EditValue
        Dim SHIFTSTARTTIME As String = CheckShiftStart.EditValue
        Dim SHIFTENDTIME As String = CheckShiftEnd.EditValue
        Dim IN1 As String = CheckIN1.EditValue
        Dim OUT1 As String = CheckOut1.EditValue
        Dim IN2 As String = CheckIN2.EditValue
        Dim OUT2 As String = CheckOut2.EditValue
        Dim STATUS As String = CheckStatus.EditValue
        Dim HOURSWORKED As String = CheckHourWorked.EditValue
        Dim LATEARRIVAL As String = CheckLate.EditValue
        Dim EARLYDEPARTURE As String = CheckEarlyDeparture.EditValue
        Dim OTDURATION As String = CheckOT.EditValue
        Dim OSDURATION As String = CheckOS.EditValue



        Dim GuardianName As String = CheckGName.EditValue
        Dim Designation As String = CheckDes.EditValue
        Dim DateOfJoining As String = CheckDoj.EditValue
        Dim Gender As String = CheckGender.EditValue
        Dim PaycodeCap As String = TextPayCap.Text.Trim
        Dim NameCap As String = TextNameCap.Text.Trim
        Dim CardNumberCap As String = TextUserNoCap.Text.Trim
        Dim CompanyCap As String = TextComCap.Text.Trim
        Dim LocationCap As String = TextLocCap.Text.Trim
        Dim DepartmentCap As String = TextDeptCap.Text.Trim
        Dim GradeCap As String = TextGradeCap.Text.Trim
        Dim CatagoryCap As String = TextCatCap.Text.Trim
        Dim EmpGrpCap As String = TextEmpGrpCap.Text.Trim
        Dim SHIFTATTENDEDCap As String = TextSAttCap.Text.Trim
        Dim SHIFTSTARTTIMECap As String = TextShiftStartCap.Text.Trim
        Dim SHIFTENDTIMECap As String = TextShiftEndCap.Text.Trim
        Dim IN1Cap As String = TextIn1Cap.Text.Trim
        Dim OUT1Cap As String = TextOut1Cap.Text.Trim
        Dim IN2Cap As String = TextIn2Cap.Text.Trim
        Dim OUT2Cap As String = TextOut2Cap.Text.Trim
        Dim STATUSCap As String = TextStatusCap.Text.Trim
        Dim HOURSWORKEDCap As String = TextHourWorkedCap.Text.Trim
        Dim LATEARRIVALCap As String = TextLateCap.Text.Trim
        Dim EARLYDEPARTURECap As String = TextEarlyDCap.Text.Trim
        Dim OTDURATIONCap As String = TextOTCap.Text.Trim
        Dim OSDURATIONCap As String = TextOSCap.Text.Trim
        Dim GuardianNameCap As String = TextGNameCap.Text.Trim
        Dim DesignationCap As String = TextDesCap.Text.Trim
        Dim DateOfJoiningCap As String = TextDojCap.Text.Trim
        Dim GenderCap As String = TextGenCap.Text.Trim
        Dim PaycodePos As String = TextPayPos.Text.Trim
        Dim NamePos As String = TextNamePos.Text.Trim
        Dim CardNumberPos As String = TextUserNoPos.Text.Trim
        Dim CompanyPos As String = TextComPos.Text.Trim
        Dim LocationPos As String = TextLocPos.Text.Trim
        Dim DepartmentPos As String = TextDeptPos.Text.Trim
        Dim GradePos As String = TextGradPos.Text.Trim
        Dim CatagoryPos As String = TextCatPos.Text.Trim
        Dim EmpGrpPos As String = TextEmpGrpPos.Text.Trim
        Dim SHIFTATTENDEDPos As String = TextSAttPos.Text.Trim
        Dim SHIFTSTARTTIMEPos As String = TextShiftStartPos.Text.Trim
        Dim SHIFTENDTIMEPos As String = TextShiftEndPos.Text.Trim
        Dim IN1Pos As String = TextIn1Pos.Text.Trim
        Dim OUT1Pos As String = TextOut1Pos.Text.Trim
        Dim IN2Pos As String = TextIn2Pos.Text.Trim
        Dim OUT2Pos As String = TextOut2Pos.Text.Trim
        Dim STATUSPos As String = TextStatusPos.Text.Trim
        Dim HOURSWORKEDPos As String = TextHourWorkedPos.Text.Trim
        Dim LATEARRIVALPos As String = TextLatePos.Text.Trim
        Dim EARLYDEPARTUREPos As String = TextEarlyDPos.Text.Trim
        Dim OTDURATIONPos As String = TextOTPos.Text.Trim
        Dim OSDURATIONPos As String = TextOSPos.Text.Trim
        Dim GuardianNamePos As String = TextGNamePos.Text.Trim
        Dim DesignationPos As String = TextDesPos.Text.Trim
        Dim DateOfJoiningPos As String = TextDojPos.Text.Trim
        Dim GenderPos As String = TextGenPos.Text.Trim
        Dim Format As String = ComboBKType.EditValue

        Dim DateFormat As String = TextDateFormat.Text.Trim
        Dim tmpdate As String = ""
        Dim tmpdate1 As DateTime
        If DateFormat <> "" Then
            Try
                tmpdate = Microsoft.VisualBasic.Strings.Format(System.DateTime.Now, DateFormat)
                tmpdate1 = Convert.ToDateTime(tmpdate)
            Catch ex As Exception
                Me.Cursor = Cursors.Default
                XtraMessageBox.Show(ulf, "<size=10>Invalid Date Format</size>", "<size=9>Error</size>")
                TextDateFormat.Select()
                Exit Sub
            End Try
            'ElseIf DateFormat = "" Then
            '    DateFormat = "dd/MM/yyyy"
        End If
        Dim PreIsPrifix As String = CheckPrefixPreCard.EditValue
        Dim PreLength As String = TextPriFixLenghtPre.Text.Trim
        Dim PreText As String = TextPriTextPre.Text.Trim
        If CheckPrefixPreCard.Checked = False Then
            PreLength = ""
            PreText = ""
        End If
        Dim PayIsPrefix As String = CheckPrefixPrePaycode.EditValue
        Dim PayLength As String = TextPriFixLenghtPay.Text.Trim
        Dim PayText As String = TextPriTextPay.Text.Trim
        If CheckPrefixPrePaycode.Checked = False Then
            PayLength = ""
            PreText = ""
        End If
        Dim DateCol As String = CheckDate.EditValue
        Dim DateColCap As String = TextDateCap.Text.Trim
        Dim DateColPos As String = TextDatePos.Text.Trim

        Dim EXCLUNCHHOURS As String = CheckExcessLunch.EditValue
        Dim EXCLUNCHHOURSCap As String = TextExcessLunchCap.Text.Trim
        Dim EXCLUNCHHOURSPos As String = TextExcessLunchPos.Text.Trim

        Dim TELEPHONE2 As String = CheckEditUID.EditValue
        Dim TELEPHONE2Cap As String = TextUidCap.Text.Trim
        Dim TELEPHONE2Pos As String = TextUidPos.Text.Trim

        Dim sSql As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        sSql = "select * from CustomisedReport"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            'insert
            sSql = "INSERT INTO [CustomisedReport]([Paycode],[Name],[CardNumber] ,[Company],[Location],[Department] ,[Grade],[Catagory],[EmpGrp]," & _
                "[SHIFTATTENDED],[SHIFTSTARTTIME] ,[SHIFTENDTIME],[IN1],[OUT1],[IN2],[OUT2],[STATUS],[HOURSWORKED],[LATEARRIVAL],[EARLYDEPARTURE],[OTDURATION],[OSDURATION]" & _
                ",[GuardianName],[Designation],[DateOfJoining],[Gender],[PaycodeCap],[NameCap],[CardNumberCap],[CompanyCap]" & _
                ",[LocationCap],[DepartmentCap],[GradeCap],[CatagoryCap],[EmpGrpCap],[SHIFTATTENDEDCap],[SHIFTSTARTTIMECap],[SHIFTENDTIMECap],[IN1Cap],[OUT1Cap]" & _
                ",[IN2Cap],[OUT2Cap],[STATUSCap],[HOURSWORKEDCap],[LATEARRIVALCap],[EARLYDEPARTURECap],[OTDURATIONCap],[OSDURATIONCap],[GuardianNameCap],[DesignationCap]," & _
                "[DateOfJoiningCap],[GenderCap],[PaycodePos],[NamePos],[CardNumberPos],[CompanyPos],[LocationPos],[DepartmentPos],[GradePos],[CatagoryPos],[EmpGrpPos]," & _
                "[SHIFTATTENDEDPos],[SHIFTSTARTTIMEPos],[SHIFTENDTIMEPos],[IN1Pos],[OUT1Pos],[IN2Pos],[OUT2Pos],[STATUSPos],[HOURSWORKEDPos],[LATEARRIVALPos]," & _
                "[EARLYDEPARTUREPos],[OTDURATIONPos],[OSDURATIONPos],[GuardianNamePos],[DesignationPos],[DateOfJoiningPos],[GenderPos],[Format],[DateFormat]," & _
                "[PreIsPrifix],[PreLength],[PreText],[PayIsPrefix],[PayLength],[PayText],[DateCol],[DateColCap],[DateColPos],[EXCLUNCHHOURS],[EXCLUNCHHOURSCap],[EXCLUNCHHOURSPos], " & _
                "[TELEPHONE2],[TELEPHONE2Cap],[TELEPHONE2Pos])" & _
                " VALUES('" & Paycode & "','" & Name & "','" & CardNumber & "','" & Company & "','" & Location & "','" & Department & "','" & Grade & "', " & _
                "'" & Catagory & "','" & EmpGrp & "','" & SHIFTATTENDED & "','" & SHIFTSTARTTIME & "','" & SHIFTENDTIME & "','" & IN1 & "','" & OUT1 & "', " & _
                "'" & IN2 & "','" & OUT2 & "','" & STATUS & "','" & HOURSWORKED & "','" & LATEARRIVAL & "','" & EARLYDEPARTURE & "','" & OTDURATION & "', " & _
                "'" & OSDURATION & "','" & GuardianName & "','" & Designation & "','" & DateOfJoining & "','" & Gender & "','" & PaycodeCap & "','" & NameCap & "', " & _
                "'" & CardNumberCap & "','" & CompanyCap & "','" & LocationCap & "','" & DepartmentCap & "','" & GradeCap & "','" & CatagoryCap & "'," & _
                "'" & EmpGrpCap & "','" & SHIFTATTENDEDCap & "','" & SHIFTSTARTTIMECap & "','" & SHIFTENDTIMECap & "','" & IN1Cap & "','" & OUT1Cap & "'," & _
                "'" & IN2Cap & "','" & OUT2Cap & "','" & STATUSCap & "','" & HOURSWORKEDCap & "','" & LATEARRIVALCap & "','" & EARLYDEPARTURECap & "'," & _
                "'" & OTDURATIONCap & "','" & OSDURATIONCap & "','" & GuardianNameCap & "','" & DesignationCap & "','" & DateOfJoiningCap & "','" & GenderCap & "'," & _
                "'" & PaycodePos & "','" & NamePos & "','" & CardNumberPos & "','" & CompanyPos & "','" & LocationPos & "','" & DepartmentPos & "','" & GradePos & "'," & _
                "'" & CatagoryPos & "','" & EmpGrpPos & "','" & SHIFTATTENDEDPos & "','" & SHIFTSTARTTIMEPos & "','" & SHIFTENDTIMEPos & "','" & IN1Pos & "'," & _
                "'" & OUT1Pos & "','" & IN2Pos & "','" & OUT2Pos & "','" & STATUSPos & "','" & HOURSWORKEDPos & "','" & LATEARRIVALPos & "','" & EARLYDEPARTUREPos & "'," & _
                "'" & OTDURATIONPos & "','" & OSDURATIONPos & "','" & GuardianNamePos & "','" & DesignationPos & "','" & DateOfJoiningPos & "','" & GenderPos & "'," & _
                "'" & Format & "','" & DateFormat & "','" & PreIsPrifix & "','" & PreLength & "','" & PreText & "','" & PayIsPrefix & "','" & PayLength & "','" & PayText & "'," & _
                "'" & DateCol & "','" & DateColCap & "','" & DateColPos & "','" & EXCLUNCHHOURS & "','" & EXCLUNCHHOURSCap & "','" & EXCLUNCHHOURSPos & "', " & _
                "'" & TELEPHONE2 & "','" & TELEPHONE2Cap & "','" & TELEPHONE2Pos & "')"
        Else
            'update
            sSql = "UPDATE [CustomisedReport] " & _
   "SET [Paycode] =  '" & Paycode & "' " & _
      ",[Name] =  '" & Name & "' " & _
      ",[CardNumber] =  '" & CardNumber & "' " & _
      ",[Company] =  '" & Company & "' " & _
      ",[Location] =  '" & Location & "' " & _
      ",[Department] =  '" & Department & "' " & _
      ",[Grade] =  '" & Grade & "' " & _
      ",[Catagory] =  '" & Catagory & "' " & _
      ",[EmpGrp] =  '" & EmpGrp & "' " & _
      ",[SHIFTATTENDED] =  '" & SHIFTATTENDED & "' " & _
      ",[SHIFTSTARTTIME] =  '" & SHIFTSTARTTIME & "' " & _
      ",[SHIFTENDTIME] =  '" & SHIFTENDTIME & "' " & _
      ",[IN1] =  '" & IN1 & "' " & _
      ",[OUT1] =  '" & OUT1 & "' " & _
      ",[IN2] =  '" & IN2 & "' " & _
      ",[OUT2] =  '" & OUT2 & "' " & _
      ",[STATUS] =  '" & STATUS & "' " & _
      ",[HOURSWORKED] =  '" & HOURSWORKED & "' " & _
      ",[LATEARRIVAL] =  '" & LATEARRIVAL & "' " & _
      ",[EARLYDEPARTURE] =  '" & EARLYDEPARTURE & "' " & _
      ",[OTDURATION] =  '" & OTDURATION & "' " & _
      ",[OSDURATION] =  '" & OSDURATION & "' " & _
      ",[GuardianName] =  '" & GuardianName & "' " & _
        ",[Designation] =  '" & Designation & "' " & _
        ",[DateOfJoining] =  '" & DateOfJoining & "' " & _
        ",[Gender] =  '" & Gender & "' " & _
        ",[PaycodeCap] =  '" & PaycodeCap & "' " & _
        ",[NameCap] =  '" & NameCap & "' " & _
        ",[CardNumberCap] =  '" & CardNumberCap & "' " & _
        ",[CompanyCap] =  '" & CompanyCap & "' " & _
        ",[LocationCap] =  '" & LocationCap & "' " & _
        ",[DepartmentCap] =  '" & DepartmentCap & "' " & _
        ",[GradeCap] =  '" & GradeCap & "' " & _
        ",[CatagoryCap] =  '" & CatagoryCap & "' " & _
        ",[EmpGrpCap] =  '" & EmpGrpCap & "' " & _
        ",[SHIFTATTENDEDCap] =  '" & SHIFTATTENDEDCap & "' " & _
        ",[SHIFTSTARTTIMECap] =  '" & SHIFTSTARTTIMECap & "' " & _
        ",[SHIFTENDTIMECap] =  '" & SHIFTENDTIMECap & "' " & _
        ",[IN1Cap] =  '" & IN1Cap & "' " & _
        ",[OUT1Cap] =  '" & OUT1Cap & "' " & _
        ",[IN2Cap] =  '" & IN2Cap & "' " & _
        ",[OUT2Cap] =  '" & OUT2Cap & "' " & _
        ",[STATUSCap] =  '" & STATUSCap & "' " & _
        ",[HOURSWORKEDCap] =  '" & HOURSWORKEDCap & "' " & _
        ",[LATEARRIVALCap] =  '" & LATEARRIVALCap & "' " & _
        ",[EARLYDEPARTURECap] =  '" & EARLYDEPARTURECap & "' " & _
        ",[OTDURATIONCap] =  '" & OTDURATIONCap & "' " & _
        ",[OSDURATIONCap] =  '" & OSDURATIONCap & "' " & _
        ",[GuardianNameCap] =  '" & GuardianNameCap & "' " & _
        ",[DesignationCap] =  '" & DesignationCap & "' " & _
        ",[DateOfJoiningCap] =  '" & DateOfJoiningCap & "' " & _
        ",[GenderCap] =  '" & GenderCap & "' " & _
        ",[PaycodePos] =  '" & PaycodePos & "' " & _
        ",[NamePos] =  '" & NamePos & "' " & _
        ",[CardNumberPos] =  '" & CardNumberPos & "' " & _
        ",[CompanyPos] =  '" & CompanyPos & "' " & _
        ",[LocationPos] =  '" & LocationPos & "' " & _
        ",[DepartmentPos] =  '" & DepartmentPos & "' " & _
        ",[GradePos] =  '" & GradePos & "' " & _
        ",[CatagoryPos] =  '" & CatagoryPos & "' " & _
        ",[EmpGrpPos] =  '" & EmpGrpPos & "' " & _
        ",[SHIFTATTENDEDPos] =  '" & SHIFTATTENDEDPos & "' " & _
        ",[SHIFTSTARTTIMEPos] =  '" & SHIFTSTARTTIMEPos & "' " & _
        ",[SHIFTENDTIMEPos] =  '" & SHIFTENDTIMEPos & "' " & _
        ",[IN1Pos] =  '" & IN1Pos & "' " & _
        ",[OUT1Pos] =  '" & OUT1Pos & "' " & _
        ",[IN2Pos] =  '" & IN2Pos & "' " & _
        ",[OUT2Pos] =  '" & OUT2Pos & "' " & _
        ",[STATUSPos] =  '" & STATUSPos & "' " & _
        ",[HOURSWORKEDPos] =  '" & HOURSWORKEDPos & "' " & _
        ",[LATEARRIVALPos] =  '" & LATEARRIVALPos & "' " & _
        ",[EARLYDEPARTUREPos] =  '" & EARLYDEPARTUREPos & "' " & _
        ",[OTDURATIONPos] =  '" & OTDURATIONPos & "' " & _
        ",[OSDURATIONPos] =  '" & OSDURATIONPos & "' " & _
        ",[GuardianNamePos] =  '" & GuardianNamePos & "' " & _
        ",[DesignationPos] =  '" & DesignationPos & "' " & _
        ",[DateOfJoiningPos] =  '" & DateOfJoiningPos & "' " & _
        ",[GenderPos] =  '" & GenderPos & "' " & _
        ",[Format] =  '" & Format & "' " & _
        ",[DateFormat] =  '" & DateFormat & "' " & _
        ",[PreIsPrifix] =  '" & PreIsPrifix & "' " & _
        ",[PreLength] =  '" & PreLength & "' " & _
        ",[PreText] =  '" & PreText & "' " & _
        ",[PayIsPrefix] =  '" & PayIsPrefix & "' " & _
        ",[PayLength] =  '" & PayLength & "' " & _
        ",[PayText] =  '" & PayText & "' " & _
        ",[DateCol] =  '" & DateCol & "' " & _
        ",[DateColCap] =  '" & DateColCap & "' " & _
        ",[DateColPos] =  '" & DateColPos & "' " & _
        ",[EXCLUNCHHOURS] =  '" & EXCLUNCHHOURS & "' " & _
        ",[EXCLUNCHHOURSCap] =  '" & EXCLUNCHHOURSCap & "' " & _
        ",[EXCLUNCHHOURSPos] =  '" & EXCLUNCHHOURSPos & "' " & _
            ",[TELEPHONE2] =  '" & TELEPHONE2 & "' " & _
            ",[TELEPHONE2Cap] =  '" & TELEPHONE2Cap & "' " & _
            ",[TELEPHONE2Pos] =  '" & TELEPHONE2Pos & "' "

        End If

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfully </size>", "<size=9>Success</size>")
    End Sub
    Private Function setPosTextBoxChecked(control As TextEdit) ' As Integer  'findTopTextBox
        If pageload = False Then 'only to restrict event at page load
            Dim maxValue As String = 0
            'Dim maxTextBox As Integer
            For Each txt In New TextEdit() {TextPayPos, TextNamePos, TextUserNoPos, TextComPos, TextLocPos, TextDeptPos,
                                            TextGradPos, TextCatPos, TextEmpGrpPos, TextGNamePos, TextDesPos, TextDojPos,
                                            TextGenPos, TextSAttPos, TextShiftStartPos, TextShiftEndPos, TextIn1Pos, TextOut1Pos,
                                            TextIn2Pos, TextOut2Pos, TextStatusPos, TextHourWorkedPos, TextLatePos, TextEarlyDPos,
                                            TextOTPos, TextOSPos, TextDatePos, TextExcessLunchPos, TextUidPos}
                Dim tmp As Integer = 0
                Try
                    tmp = Convert.ToInt32(txt.Text)
                Catch ex As Exception

                End Try
                If tmp > maxValue Then
                    maxValue = Convert.ToInt32(txt.Text)
                    'maxTextBox = txt
                End If
            Next
            control.Text = maxValue + 1
            'Return maxValue
        End If
    End Function
    Private Function setPosTextBoxUnChecked(control As TextEdit) ' As Integer  'findTopTextBox
        If pageload = False Then 'only to restrict event at page load
            For Each txt In New TextEdit() {TextPayPos, TextNamePos, TextUserNoPos, TextComPos, TextLocPos, TextDeptPos,
                                              TextGradPos, TextCatPos, TextEmpGrpPos, TextGNamePos, TextDesPos, TextDojPos,
                                              TextGenPos, TextSAttPos, TextShiftStartPos, TextShiftEndPos, TextIn1Pos, TextOut1Pos,
                                              TextIn2Pos, TextOut2Pos, TextStatusPos, TextHourWorkedPos, TextLatePos, TextEarlyDPos,
                                              TextOTPos, TextOSPos, TextDatePos, TextExcessLunchPos, TextUidPos}
                Dim tmp As Integer = 0
                Try
                    tmp = Convert.ToInt32(txt.Text)
                Catch ex As Exception

                End Try
                If control.Text <> "" Then
                    If tmp > control.Text Then
                        txt.Text = txt.Text - 1
                    End If
                End If
            Next
            control.Text = ""          
        End If
    End Function
    Private Sub CheckEmpPay_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEmpPay.CheckedChanged
         If CheckEmpPay.Checked = True Then
            setPosTextBoxChecked(TextPayPos)
        Else
            setPosTextBoxUnChecked(TextPayPos)
        End If      
    End Sub

    Private Sub CheckEmpName_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEmpName.CheckedChanged
        If CheckEmpName.Checked = True Then
            setPosTextBoxChecked(TextNamePos)
        Else
            setPosTextBoxUnChecked(TextNamePos)
        End If    
    End Sub

    Private Sub CheckUserNo_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckUserNo.CheckedChanged
         If CheckUserNo.Checked = True Then
            setPosTextBoxChecked(TextUserNoPos)
        Else
            setPosTextBoxUnChecked(TextUserNoPos)
        End If       
    End Sub

    Private Sub CheckCompany_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckCompany.CheckedChanged
        If CheckCompany.Checked = True Then
            setPosTextBoxChecked(TextComPos)
        Else
            setPosTextBoxUnChecked(TextComPos)
        End If       
    End Sub

    Private Sub CheckLocation_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckLocation.CheckedChanged
        If CheckLocation.Checked = True Then
            setPosTextBoxChecked(TextLocPos)
        Else
            setPosTextBoxUnChecked(TextLocPos)
        End If
    End Sub

    Private Sub CheckDept_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckDept.CheckedChanged
        If CheckDept.Checked = True Then
            setPosTextBoxChecked(TextDeptPos)
        Else
            setPosTextBoxUnChecked(TextDeptPos)
        End If
    End Sub

    Private Sub CheckGrade_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckGrade.CheckedChanged
        If CheckGrade.Checked = True Then
            setPosTextBoxChecked(TextGradPos)
        Else
            setPosTextBoxUnChecked(TextGradPos)
        End If
    End Sub

    Private Sub CheckCat_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckCat.CheckedChanged
        If CheckCat.Checked = True Then
            setPosTextBoxChecked(TextCatPos)
        Else
            setPosTextBoxUnChecked(TextCatPos)
        End If
    End Sub

    Private Sub CheckEmpGrp_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEmpGrp.CheckedChanged
        If CheckEmpGrp.Checked = True Then
            setPosTextBoxChecked(TextEmpGrpPos)
        Else
            setPosTextBoxUnChecked(TextEmpGrpPos)
        End If
    End Sub

    Private Sub CheckGName_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckGName.CheckedChanged
        If CheckGName.Checked = True Then
            setPosTextBoxChecked(TextGNamePos)
        Else
            setPosTextBoxUnChecked(TextGNamePos)
        End If
    End Sub

    Private Sub CheckDes_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckDes.CheckedChanged
        If CheckDes.Checked = True Then
            setPosTextBoxChecked(TextDesPos)
        Else
            setPosTextBoxUnChecked(TextDesPos)
        End If
    End Sub

    Private Sub CheckDoj_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckDoj.CheckedChanged
        If CheckDoj.Checked = True Then
            setPosTextBoxChecked(TextDojPos)
        Else
            setPosTextBoxUnChecked(TextDojPos)
        End If
    End Sub

    Private Sub CheckGender_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckGender.CheckedChanged
        If CheckGender.Checked = True Then
            setPosTextBoxChecked(TextGenPos)
        Else
            setPosTextBoxUnChecked(TextGenPos)
        End If
    End Sub

    Private Sub CheckShift_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckShift.CheckedChanged
        If CheckShift.Checked = True Then
            setPosTextBoxChecked(TextSAttPos)
        Else
            setPosTextBoxUnChecked(TextSAttPos)
        End If
    End Sub

    Private Sub CheckShiftStart_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckShiftStart.CheckedChanged
        If CheckShiftStart.Checked = True Then
            setPosTextBoxChecked(TextShiftStartPos)
        Else
            setPosTextBoxUnChecked(TextShiftStartPos)
        End If
    End Sub

    Private Sub CheckShiftEnd_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckShiftEnd.CheckedChanged
        If CheckShiftEnd.Checked = True Then
            setPosTextBoxChecked(TextShiftEndPos)
        Else
            setPosTextBoxUnChecked(TextShiftEndPos)
        End If
    End Sub

    Private Sub CheckIN1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckIN1.CheckedChanged
        If CheckIN1.Checked = True Then
            setPosTextBoxChecked(TextIn1Pos)
        Else
            setPosTextBoxUnChecked(TextIn1Pos)
        End If
    End Sub

    Private Sub CheckOut1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckOut1.CheckedChanged
        If CheckOut1.Checked = True Then
            setPosTextBoxChecked(TextOut1Pos)
        Else
            setPosTextBoxUnChecked(TextOut1Pos)
        End If
    End Sub

    Private Sub CheckIN2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckIN2.CheckedChanged
        If CheckIN2.Checked = True Then
            setPosTextBoxChecked(TextIn2Pos)
        Else
            setPosTextBoxUnChecked(TextIn2Pos)
        End If
    End Sub

    Private Sub CheckOut2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckOut2.CheckedChanged
        If CheckOut2.Checked = True Then
            setPosTextBoxChecked(TextOut2Pos)
        Else
            setPosTextBoxUnChecked(TextOut2Pos)
        End If
    End Sub

    Private Sub CheckStatus_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckStatus.CheckedChanged
        If CheckStatus.Checked = True Then
            setPosTextBoxChecked(TextStatusPos)
        Else
            setPosTextBoxUnChecked(TextStatusPos)
        End If
    End Sub

    Private Sub CheckHourWorked_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckHourWorked.CheckedChanged
        If CheckHourWorked.Checked = True Then
            setPosTextBoxChecked(TextHourWorkedPos)
        Else
            setPosTextBoxUnChecked(TextHourWorkedPos)
        End If
    End Sub

    Private Sub CheckLate_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckLate.CheckedChanged
        If CheckLate.Checked = True Then
            setPosTextBoxChecked(TextLatePos)
        Else
            setPosTextBoxUnChecked(TextLatePos)
        End If
    End Sub

    Private Sub CheckEarlyDeparture_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEarlyDeparture.CheckedChanged
        If CheckEarlyDeparture.Checked = True Then
            setPosTextBoxChecked(TextEarlyDPos)
        Else
            setPosTextBoxUnChecked(TextEarlyDPos)
        End If
    End Sub

    Private Sub CheckOT_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckOT.CheckedChanged
        If CheckOT.Checked = True Then
            setPosTextBoxChecked(TextOTPos)
        Else
            setPosTextBoxUnChecked(TextOTPos)
        End If
    End Sub

    Private Sub CheckOS_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckOS.CheckedChanged
        If CheckOS.Checked = True Then
            setPosTextBoxChecked(TextOSPos)
        Else
            setPosTextBoxUnChecked(TextOSPos)
        End If
    End Sub

    Private Sub CheckDate_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckDate.CheckedChanged
        If CheckDate.Checked = True Then
            setPosTextBoxChecked(TextDatePos)
        Else
            setPosTextBoxUnChecked(TextDatePos)
        End If
    End Sub

    Private Sub CheckExcessLunch_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckExcessLunch.CheckedChanged
        If CheckExcessLunch.Checked = True Then
            setPosTextBoxChecked(TextExcessLunchPos)
        Else
            setPosTextBoxUnChecked(TextExcessLunchPos)
        End If
    End Sub

    Private Sub CheckEditUID_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditUID.CheckedChanged
        If CheckEditUID.Checked = True Then
            setPosTextBoxChecked(TextUidPos)
        Else
            setPosTextBoxUnChecked(TextUidPos)
        End If
    End Sub
End Class
