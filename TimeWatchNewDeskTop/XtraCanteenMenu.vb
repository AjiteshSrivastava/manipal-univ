﻿Imports System.Resources
Imports System.Globalization
Imports System.Data.OleDb
Public Class XtraCanteenMenu
    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub XtraCanteenMenu_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        NavigationPane1.Width = NavigationPane1.Parent.Width
        NavigationPage1.Width = NavigationPage1.Parent.Width
        Me.Width = Me.Parent.Width
        Me.Height = Me.Parent.Height
        'MsgBox(NavigationPane1.Width - NavigationPage1.Width)
        Common.NavHeight = NavigationPage1.Height
        Common.NavWidth = NavigationPage1.Width
    End Sub
    Private Sub NavigationPane1_SelectedPageIndexChanged(sender As System.Object, e As System.EventArgs) Handles NavigationPane1.SelectedPageIndexChanged
        If NavigationPane1.SelectedPageIndex = 0 Then
            NavigationPage1.Controls.Clear()
            Dim form As UserControl = New XtraCanteenTimeSlab '
            form.Dock = DockStyle.Fill
            NavigationPage1.Controls.Add(form)
            form.Show()
        End If
        If NavigationPane1.SelectedPageIndex = 1 Then
            NavigationPage2.Controls.Clear()
            Dim form As UserControl = New XtraCanteenMealMenu '
            form.Dock = DockStyle.Fill
            NavigationPage2.Controls.Add(form)
            form.Show()
        End If
        If NavigationPane1.SelectedPageIndex = 2 Then
            NavigationPage3.Controls.Clear()
            Dim form As UserControl = New XtraCanteenReport '
            form.Dock = DockStyle.Fill
            NavigationPage3.Controls.Add(form)
            form.Show()
        End If
    End Sub
End Class
