﻿Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Net.NetworkInformation
Imports System.Text.RegularExpressions

Public Class XtraPfBank
    Dim ulf As UserLookAndFeel
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        Common.pfBankName = TextEditToBank.Text.Trim
        Common.BankName = TextEditBName.Text.Trim
        Common.BankBranch = TextEditBranch.Text.Trim
        Common.ChequeAmt = IIf(TextEditChAmt.Text.Trim = "", 0, TextEditChAmt.Text.Trim)
        Common.ChequeDate = DateEdit1.DateTime
        Common.ChequeNo = TextEditChNo.Text.Trim
        Common.Depositor = TextEditDepositor.Text.Trim
        Me.Close()
    End Sub
    Private Sub XtraCompanyInfo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        'MAC =  getMacAddress()
        GetDefault()

    End Sub
    Private Sub GetDefault()
        DateEdit1.EditValue = Now
        TextEditToBank.Text = ""
        TextEditBName.Text = ""
        TextEditBranch.Text = ""
        TextEditChNo.Text = ""
        TextEditChAmt.Text = ""
        TextEditDepositor.Text = ""

        Common.pfBankName = ""
        Common.BankName = ""
        Common.BankBranch = ""
        Common.ChequeAmt = "0" 'IIf(mskChequeAmt = "", 0, mskChequeAmt)
        Common.ChequeDate = DateEdit1.EditValue
        Common.ChequeNo = ""
        Common.Depositor = ""

    End Sub  
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
End Class