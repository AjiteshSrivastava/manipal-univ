﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraAutoDownloadLogs
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraAutoDownloadLogs))
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblMachineTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLOCATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIN_OUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TblMachine1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblMachineTableAdapter
        '
        Me.TblMachineTableAdapter.ClearBeforeFill = True
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.LabelControl1)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(0, 281)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(505, 23)
        Me.SidePanel1.TabIndex = 0
        Me.SidePanel1.Text = "SidePanel1"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseForeColor = True
        Me.LabelControl1.Location = New System.Drawing.Point(4, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "            "
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblMachineBindingSource
        Me.GridControl1.Location = New System.Drawing.Point(0, 20)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(505, 265)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID_NO, Me.colA_R, Me.colLOCATION, Me.colbranch, Me.colIN_OUT})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        '
        'colID_NO
        '
        Me.colID_NO.Caption = "Controller Id"
        Me.colID_NO.FieldName = "ID_NO"
        Me.colID_NO.Name = "colID_NO"
        Me.colID_NO.Visible = True
        Me.colID_NO.VisibleIndex = 0
        '
        'colA_R
        '
        Me.colA_R.Caption = "Protocol"
        Me.colA_R.FieldName = "A_R"
        Me.colA_R.Name = "colA_R"
        Me.colA_R.Visible = True
        Me.colA_R.VisibleIndex = 1
        '
        'colLOCATION
        '
        Me.colLOCATION.Caption = "Device IP"
        Me.colLOCATION.FieldName = "LOCATION"
        Me.colLOCATION.Name = "colLOCATION"
        Me.colLOCATION.Visible = True
        Me.colLOCATION.VisibleIndex = 2
        '
        'colbranch
        '
        Me.colbranch.Caption = "Location"
        Me.colbranch.FieldName = "branch"
        Me.colbranch.Name = "colbranch"
        Me.colbranch.Visible = True
        Me.colbranch.VisibleIndex = 3
        '
        'colIN_OUT
        '
        Me.colIN_OUT.Caption = "IN_OUT Type"
        Me.colIN_OUT.FieldName = "IN_OUT"
        Me.colIN_OUT.Name = "colIN_OUT"
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'XtraAutoDownloadLogs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(505, 304)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.SidePanel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraAutoDownloadLogs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Auto Download Logs"
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblMachineTableAdapter As iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colA_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLOCATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblMachine1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents colIN_OUT As DevExpress.XtraGrid.Columns.GridColumn
End Class
