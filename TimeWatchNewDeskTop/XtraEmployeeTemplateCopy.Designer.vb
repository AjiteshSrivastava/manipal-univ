﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraEmployeeTemplateCopy
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblMachineTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        Me.TblMachine1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLOCATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeviceType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButtonStop = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonInit = New DevExpress.XtraEditors.SimpleButton()
        Me.picFPImg = New DevExpress.XtraEditors.PictureEdit()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.textRes = New DevExpress.XtraEditors.TextEdit()
        Me.TextFingerNo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picFPImg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textRes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextFingerNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblMachineTableAdapter
        '
        Me.TblMachineTableAdapter.ClearBeforeFill = True
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.GridControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(539, 257)
        Me.GroupControl1.TabIndex = 17
        Me.GroupControl1.Text = "Device List"
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblMachineBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        GridLevelNode1.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControl1.Location = New System.Drawing.Point(2, 23)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(535, 232)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID_NO, Me.colLOCATION, Me.colbranch, Me.colDeviceType, Me.colA_R})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colID_NO, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colID_NO
        '
        Me.colID_NO.Caption = "Controller Id"
        Me.colID_NO.FieldName = "ID_NO"
        Me.colID_NO.Name = "colID_NO"
        Me.colID_NO.Visible = True
        Me.colID_NO.VisibleIndex = 1
        '
        'colLOCATION
        '
        Me.colLOCATION.Caption = "Device IP"
        Me.colLOCATION.FieldName = "LOCATION"
        Me.colLOCATION.Name = "colLOCATION"
        Me.colLOCATION.Visible = True
        Me.colLOCATION.VisibleIndex = 2
        '
        'colbranch
        '
        Me.colbranch.Caption = "Location"
        Me.colbranch.FieldName = "branch"
        Me.colbranch.Name = "colbranch"
        Me.colbranch.Visible = True
        Me.colbranch.VisibleIndex = 3
        '
        'colDeviceType
        '
        Me.colDeviceType.Caption = "Device Type"
        Me.colDeviceType.FieldName = "DeviceType"
        Me.colDeviceType.Name = "colDeviceType"
        Me.colDeviceType.Visible = True
        Me.colDeviceType.VisibleIndex = 4
        '
        'colA_R
        '
        Me.colA_R.FieldName = "A_R"
        Me.colA_R.Name = "colA_R"
        '
        'SimpleButtonStop
        '
        Me.SimpleButtonStop.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonStop.Appearance.Options.UseFont = True
        Me.SimpleButtonStop.Location = New System.Drawing.Point(753, 41)
        Me.SimpleButtonStop.Name = "SimpleButtonStop"
        Me.SimpleButtonStop.Size = New System.Drawing.Size(96, 23)
        Me.SimpleButtonStop.TabIndex = 43
        Me.SimpleButtonStop.Text = "Stop Reader"
        '
        'SimpleButtonInit
        '
        Me.SimpleButtonInit.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonInit.Appearance.Options.UseFont = True
        Me.SimpleButtonInit.Location = New System.Drawing.Point(753, 12)
        Me.SimpleButtonInit.Name = "SimpleButtonInit"
        Me.SimpleButtonInit.Size = New System.Drawing.Size(96, 23)
        Me.SimpleButtonInit.TabIndex = 42
        Me.SimpleButtonInit.Text = "Init Reader"
        '
        'picFPImg
        '
        Me.picFPImg.Cursor = System.Windows.Forms.Cursors.Default
        Me.picFPImg.Location = New System.Drawing.Point(555, 41)
        Me.picFPImg.Name = "picFPImg"
        Me.picFPImg.Properties.NullText = " "
        Me.picFPImg.Properties.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.ByteArray
        Me.picFPImg.Properties.ZoomPercent = 60.0R
        Me.picFPImg.Size = New System.Drawing.Size(190, 228)
        Me.picFPImg.TabIndex = 41
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(751, 246)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(98, 23)
        Me.SimpleButtonSave.TabIndex = 40
        Me.SimpleButtonSave.Text = "Transfer"
        '
        'textRes
        '
        Me.textRes.Location = New System.Drawing.Point(555, 291)
        Me.textRes.Name = "textRes"
        Me.textRes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.textRes.Properties.Appearance.Options.UseFont = True
        Me.textRes.Properties.ReadOnly = True
        Me.textRes.Size = New System.Drawing.Size(190, 20)
        Me.textRes.TabIndex = 44
        Me.textRes.Visible = False
        '
        'TextFingerNo
        '
        Me.TextFingerNo.EditValue = "6"
        Me.TextFingerNo.Location = New System.Drawing.Point(644, 15)
        Me.TextFingerNo.Name = "TextFingerNo"
        Me.TextFingerNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextFingerNo.Properties.Appearance.Options.UseFont = True
        Me.TextFingerNo.Properties.Mask.EditMask = "[0-9]"
        Me.TextFingerNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextFingerNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextFingerNo.Properties.MaxLength = 1
        Me.TextFingerNo.Size = New System.Drawing.Size(47, 20)
        Me.TextFingerNo.TabIndex = 45
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(558, 16)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(80, 14)
        Me.LabelControl1.TabIndex = 46
        Me.LabelControl1.Text = "Finger Number"
        '
        'XtraEmployeeTemplateCopy
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(874, 339)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.TextFingerNo)
        Me.Controls.Add(Me.textRes)
        Me.Controls.Add(Me.SimpleButtonStop)
        Me.Controls.Add(Me.SimpleButtonInit)
        Me.Controls.Add(Me.picFPImg)
        Me.Controls.Add(Me.SimpleButtonSave)
        Me.Controls.Add(Me.GroupControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraEmployeeTemplateCopy"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picFPImg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textRes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextFingerNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblMachineTableAdapter As iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents TblMachine1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLOCATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeviceType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colA_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButtonStop As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonInit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents picFPImg As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents textRes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextFingerNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
End Class
