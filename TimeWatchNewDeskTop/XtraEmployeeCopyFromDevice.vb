﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraSplashScreen
Imports iAS.HSeriesSampleCSharp
Imports System.Runtime.InteropServices
Imports CMITech.UMXClient

Public Class XtraEmployeeCopyFromDevice
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim h As IntPtr = IntPtr.Zero
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraEmployeeCopyFromDevice_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'If Common.servername = "Access" Then
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        'Else
        '    TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        'End If
        GridControl1.DataSource = Common.MachineNonAdmin
        GridView1.ClearSelection()
    End Sub
    Private Sub btnUploadFinger_Click(sender As System.Object, e As System.EventArgs) Handles btnUploadFinger.Click
        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If

        ' Dim Shift As String, mComp As String, mDept As String, mBranch As String, mEmpGrp As String, mmgrade As String, mmcat As String, _
        ' SHIFTTYPE As String, SHIFTPATTERN As String, _
        ' SHIFTREMAINDAYS As String, LASTSHIFTPERFORMED As String, INONLY As String, ISPUNCHALL As String, _
        'ISTIMELOSSALLOWED As String, ALTERNATE_OFF_DAYS As String, CDAYS As String, ISROUNDTHECLOCKWORK As String, _
        'ISOT As String, OTRATE As String, FIRSTOFFDAY As String, SECONDOFFTYPE As String, HALFDAYSHIFT As String, _
        'SECONDOFFDAY As String, PERMISLATEARRIVAL As String, PERMISEARLYDEPRT As String, ISAUTOSHIFT As String, _
        'ISOUTWORK As String, MAXDAYMIN As String, ISOS As String, AUTH_SHIFTS As String, TIME1 As String, _
        'SHORT1 As String, HALF As String, ISHALFDAY As String, ISSHORT As String, TWO As String
        ' Dim mDateofjoin As DateTime, mdateofbirth As DateTime
        ' Common.LoadEmpOtherDetailsWhileDataDownload(Shift, mComp, mDept, mBranch, mEmpGrp, mmgrade, mmcat, mDateofjoin, mdateofbirth, SHIFTTYPE, _
        '         SHIFTPATTERN, SHIFTREMAINDAYS, LASTSHIFTPERFORMED, INONLY, ISPUNCHALL, ISTIMELOSSALLOWED, ALTERNATE_OFF_DAYS, CDAYS, ISROUNDTHECLOCKWORK, _
        '        ISOT, OTRATE, FIRSTOFFDAY, SECONDOFFTYPE, HALFDAYSHIFT, SECONDOFFDAY, PERMISLATEARRIVAL, PERMISEARLYDEPRT, ISAUTOSHIFT, _
        '        ISOUTWORK, MAXDAYMIN, ISOS, AUTH_SHIFTS, TIME1, SHORT1, HALF, ISHALFDAY, ISSHORT, TWO)

        Dim sdwEnrollNumber As String = ""
        Dim com As Common = New Common

        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer

        vnReadMark = 0 'All logs
        Dim clearLog As Boolean
        clearLog = False

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R, Purpose As Object
        Dim commkey As Integer
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
        Dim paycodelist As New List(Of String)()
        Dim paycodeArray '= paycodelist.Distinct.ToArray ' paycodelist.ToArray
        For Each rowHandle As Integer In RowHandles
            IP = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("LOCATION"))
            ID_NO = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("ID_NO"))
            DeviceType = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("DeviceType"))
            A_R = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("A_R"))
            Purpose = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("Purpose"))
            XtraMasterTest.LabelControlStatus.Text = "Connecting " & IP & "..."
            Application.DoEvents()
            Common.LogPost("Import from Device; Device Id: " & ID_NO)
            If DeviceType.ToString.Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 1261

                Dim gnCommHandleIndex As Long
                If A_R = "S" Then
                    gnCommHandleIndex = FK_ConnectUSB(vnMachineNumber, vnLicense)
                Else
                    gnCommHandleIndex = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                End If
                If gnCommHandleIndex > 0 Then
                    'MsgBox("Connection success")
                Else
                    'Return "Connection fail"
                    Continue For
                End If
                Dim vnResultCode As Integer = FK_EnableDevice(gnCommHandleIndex, 0)
                If vnResultCode <> RUN_SUCCESS Then
                    FK_DisConnect(gnCommHandleIndex)
                    'Return gstrNoDevice
                    Continue For
                End If
                vnResultCode = FK_ReadAllUserID(gnCommHandleIndex)
                If vnResultCode <> RUN_SUCCESS Then
                    FK_EnableDevice(gnCommHandleIndex, 1)
                    Continue For
                End If
                Dim vEnrollNumber As Integer, vBackupNumber As Integer, vPrivilege As Integer, vnEnableFlag As Integer
                Dim vStrEnrollNumber As String = ""
                If FK_GetIsSupportStringID(gnCommHandleIndex) = RUN_SUCCESS Then
                    Do
                        vnResultCode = FK_GetAllUserID_StringID(gnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
                        If vnResultCode <> RUN_SUCCESS Then
                            If vnResultCode = RUNERR_DATAARRAY_END Then
                                vnResultCode = RUN_SUCCESS
                            End If
                            Exit Do
                        End If
                        Dim vName As String = New String(ChrW(&H20), 256)
                        vnResultCode = FK_GetUserName_StringID(gnCommHandleIndex, vStrEnrollNumber.Trim, vName)
                        paycodelist.Add(vStrEnrollNumber & ";" & vName)
                    Loop
                Else
                    Do
                        vnResultCode = FK_GetAllUserID(gnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
                        If vnResultCode <> RUN_SUCCESS Then
                            If vnResultCode = RUNERR_DATAARRAY_END Then
                                vnResultCode = RUN_SUCCESS
                            End If
                            Exit Do
                        End If
                        Dim vName As String = New String(ChrW(&H20), 256)
                        vnResultCode = FK_GetUserName(gnCommHandleIndex, vEnrollNumber, vName)
                        paycodelist.Add(vEnrollNumber & ";" & vName)
                    Loop
                End If



                FK_EnableDevice(gnCommHandleIndex, 1)
                FK_DisConnect(gnCommHandleIndex)
            ElseIf DeviceType.ToString.Trim = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                'Dim CreateEmpPunchDwnld As String
                commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                Dim comZK As CommonZK = New CommonZK
                Dim bIsConnected = False
                Dim iMachineNumber As Integer
                Dim idwErrorCode As Integer
                Dim axCZKEM1 As New zkemkeeper.CZKEM
                axCZKEM1.SetCommPassword(commkey)  'to check device commkey and db commkey matches
                bIsConnected = axCZKEM1.Connect_Net(IP, 4370)
                If bIsConnected = True Then
                    iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                    axCZKEM1.RegEvent(iMachineNumber, 65535) 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                    axCZKEM1.EnableDevice(iMachineNumber, False) 'disable the device

                    Dim McSrno As String = ""
                    Dim vRet As Boolean = axCZKEM1.GetSerialNumber(iMachineNumber, McSrno)
                    'If McSrno.Substring(0, 3) = "AF6" Or McSrno.Substring(0, 3) = "AEH" Or McSrno.Substring(0, 3) = "AJV" Or McSrno.Substring(0, 3) = "AOO" Or McSrno.Substring(0, 3) = "719" Or McSrno.Substring(0, 3) = "ALM" Or McSrno.Substring(0, 3) = "701" Or McSrno.Substring(0, 3) = "BYR" Or McSrno.Substring(0, 3) = "OIN" Or McSrno.Substring(0, 3) = "AF6" Or McSrno.Substring(0, 3) = "BYX" Or McSrno.Substring(0, 3) = "AIJ" Or McSrno.Substring(0, 3) = "A6G" Or McSrno.Substring(0, 3) = "AEH" Or McSrno.Substring(0, 3) = "CCG" Or McSrno.Substring(0, 3) = "BJV" _
                    '    Or Mid(Trim(McSrno), 1, 1) = "A" Or Mid(Trim(McSrno), 1, 3) = "OIN" Or Mid(Trim(McSrno), 1, 5) = "FPCTA" Or Mid(Trim(McSrno), 1, 4) = "0000" Or Mid(Trim(McSrno), 1, 4) = "1808" Or Mid(Trim(McSrno), 1, 4) = "ATPL" Or Mid(Trim(McSrno), 1, 4) = "TIPL" Or Mid(Trim(McSrno), 1, 4) = "ZXJK" Or Mid(Trim(McSrno), 1, 8) = "10122013" Or Mid(Trim(McSrno), 1, 8) = "76140122" Or Mid(Trim(McSrno), 1, 8) = "17614012" Or Mid(Trim(McSrno), 1, 8) = "17214012" Or Mid(Trim(McSrno), 1, 8) = "27022014" Or Mid(Trim(McSrno), 1, 8) = "24042014" Or Mid(Trim(McSrno), 1, 8) = "25042014" _
                    '    Or Mid(Trim(McSrno), 1, 8) = "26042014" Or Mid(Trim(McSrno), 1, 7) = "SN:0000" Or Mid(Trim(McSrno), 1, 4) = "BOCK" Or McSrno.Substring(0, 1) = "B" Or McSrno.Substring(0, 4) = "2455" Or McSrno.Substring(0, 4) = "CDSL" _
                    '     Or McSrno = "6583151100400" Then
                    If license.DeviceSerialNo.Contains(McSrno.Trim) Then
                    Else
                        SplashScreenManager.CloseForm(False)
                        axCZKEM1.EnableDevice(iMachineNumber, True)
                        axCZKEM1.Disconnect()
                        XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & McSrno & "</size>", "<size=9>Error</size>")
                        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
                        Continue For
                    End If
                    Dim LogResult As Boolean
                    'LogResult = axCZKEM1.ReadGeneralLogData(iMachineNumber)  'all logs
                    'If LogResult Then 'read all the attendance records to the memory
                    XtraMasterTest.LabelControlStatus.Text = "Downloading from " & IP & "..."
                    Application.DoEvents()
                    'get records from the memory
                    Dim x As Integer = 0
                    'Dim paycodelist As New List(Of String)()
                    'While axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)
                    Dim sName As String, sPassword As String
                    Dim iPrivilege As Integer
                    Dim bEnabled As Boolean
                    While axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled)
                        paycodelist.Add(sdwEnrollNumber & ";" & sName)
                        'com.funcGetGeneralLogDataZK(sdwEnrollNumber, idwVerifyMode, idwInOutMode, punchdate, idwWorkcode, Purpose)
                        x = x + 1
                    End While
                    axCZKEM1.EnableDevice(iMachineNumber, True)
                    If clearLog = True Then
                        axCZKEM1.ClearGLog(iMachineNumber)
                    End If
                Else
                    axCZKEM1.GetLastError(idwErrorCode)
                    failIP.Add(IP.ToString)
                    Continue For
                End If
                axCZKEM1.Disconnect()
            ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio1Eco" Then
                vpszIPAddress = IP
                XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                Application.DoEvents()
                Dim adap, adap1 As SqlDataAdapter
                Dim adapA, adapA1 As OleDbDataAdapter
                Dim ds, ds1 As DataSet
                commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                Dim mOpenFlag As Boolean
                Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

                mMK8001Device = False
                Dim si As SySystemInfo = New SySystemInfo
                si.cSerialNum = New Byte((20) - 1) {}
                si.cManuName = New Byte((24) - 1) {}
                si.cDevName = New Byte((24) - 1) {}
                si.cAlgVer = New Byte((16) - 1) {}
                si.cFirmwareVer = New Byte((24) - 1) {}
                Dim rret As Integer = 66
                Try
                    rret = SyFunctions.CmdGetSystemInfo(si)
                Catch ex As Exception

                End Try

                If (rret = CType(SyLastError.sleSuss, Int32)) Then
                    Dim sFirmwareVer As String = System.Text.Encoding.Default.GetString(si.cFirmwareVer)
                    If sFirmwareVer.Contains("MK8001/8002") Then
                        mMK8001Device = True
                    Else
                        mMK8001Device = False
                    End If
                End If

                Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
                Dim equNo As UInt32 = Convert.ToUInt32(ID_NO)
                Dim gEquNo As UInt32 = equNo
                If A_R = "T" Then
                    Dim netCfg As SyNetCfg
                    Dim pswd As UInt32 = 0
                    Dim tmpPswd As String = commkey
                    If (tmpPswd = "0") Then
                        tmpPswd = "FFFFFFFF"
                    End If
                    Try
                        pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                    Catch ex As Exception
                        XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                        ret = CType(SyLastError.slePasswordError, Int32)
                    End Try
                    If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                        netCfg.mIsTCP = 1
                        netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                        netCfg.mIPAddr = New Byte((4) - 1) {}
                        Dim sArray() As String = vpszIPAddress.Split(Microsoft.VisualBasic.ChrW(46))
                        Dim j As Byte = 0
                        For Each i1 As String In sArray
                            Try
                                netCfg.mIPAddr(j) = Convert.ToByte(i1.Trim)
                            Catch ex As System.Exception
                                netCfg.mIPAddr(j) = 255
                            End Try
                            j = (j + 1)
                            If j > 3 Then
                                Exit For
                            End If

                        Next
                    End If

                    Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                    Try
                        Marshal.StructureToPtr(netCfg, pnt, False)
                        '        If optWifiDevice.Checked Then
                        'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                        '        Else
                        ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                        '        End If
                        '                'TODO: Warning!!!, inline IF is not supported ?
                        '                chkTranceIO.Checked()
                        '0:
                        '                '
                    Finally
                        Marshal.FreeHGlobal(pnt)
                    End Try
                ElseIf A_R = "S" Then
                    Dim equtype As String = "Finger Module USB Device"
                    Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                    ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
                End If
                If (ret = CType(SyLastError.sleSuss, Int32)) Then
                    ret = SyFunctions.CmdTestConn2Device

                    Dim maxUserCnt As System.UInt16 = 0
                    ret = SyFunctions.CmdGetCount(maxUserCnt)
                    If ((maxUserCnt > 0) And (ret = CType(SyLastError.sleSuss, Int32))) Then
                        If Common.servername = "Access" Then
                            If Common.con1.State <> System.Data.ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                        Else
                            If Common.con.State <> System.Data.ConnectionState.Open Then
                                Common.con.Open()
                            End If
                        End If
                        Dim aID() As UInt16 = New UInt16((maxUserCnt) - 1) {}
                        Array.Clear(aID, 0, aID.Length)
                        ret = SyFunctions.CmdGetAllValidUserID(aID, maxUserCnt)
                        Dim uie As SyUserInfoExt = New SyUserInfoExt
                        'Dim i As Integer = 1

                        For Each userid As UInt16 In aID
                            paycodelist.Add(userid & ";" & uie.sUserInfo.name)
                            If (userid = 0) Then
                                Exit For
                            End If
                            'Application.DoEvents()
                        Next


conclose:               If Common.servername = "Access" Then
                            If Common.con1.State <> System.Data.ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> System.Data.ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                    End If

                    SyFunctions.CloseCom()
                    ' close com 
                    SyFunctions.CmdUnLockDevice()
                    SyFunctions.CloseCom()
                Else
                    'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    Continue For
                End If

            ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "EF45" Then
                vpszIPAddress = IP
                XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                Application.DoEvents()
                Dim _client As Client = Nothing
                Dim connectOk As Boolean = False
                Try
                    _client = cn.initClientEF45(vpszIPAddress)
                    connectOk = _client.StealConnect '_client.Connect()
                    If connectOk Then
                        Dim sSql As String
                        Dim adapA As OleDbDataAdapter
                        Dim adap As SqlDataAdapter
                        Dim dsRecord As DataSet
                        Dim allUserInfo As List(Of CMITech.UMXClient.Entities.UserInfo) = _client.GetAllUserInfo
                        For Each userinfo As CMITech.UMXClient.Entities.UserInfo In allUserInfo
                            Try
                                Dim subject As CMITech.UMXClient.Entities.Subject = _client.GetSubject(userinfo.UUID)
                                paycodelist.Add(userinfo.UUID & ";" & subject.LastName)
                            Catch ex As Exception

                            End Try
                        Next
                        _client.Disconnect()
                    Else
                        failIP.Add(vpszIPAddress.ToString)
                        Continue For
                    End If
                Catch ex As Exception
                    failIP.Add(vpszIPAddress.ToString)
                    Continue For
                End Try

            ElseIf DeviceType.ToString.Trim = "F9" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881
                Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                Dim gnCommHandleIndex As Long
                If A_R = "S" Then
                    gnCommHandleIndex = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
                Else
                    gnCommHandleIndex = F9.FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                End If
                If gnCommHandleIndex > 0 Then
                    'MsgBox("Connection success")
                Else
                    'Return "Connection fail"
                    Continue For
                End If
                Dim vnResultCode As Integer = F9.FK_EnableDevice(gnCommHandleIndex, 0)
                If vnResultCode <> RUN_SUCCESS Then
                    FK_DisConnect(gnCommHandleIndex)
                    'Return gstrNoDevice
                    Continue For
                End If
                vnResultCode = F9.FK_ReadAllUserID(gnCommHandleIndex)
                If vnResultCode <> RUN_SUCCESS Then
                    F9.FK_EnableDevice(gnCommHandleIndex, 1)
                    Continue For
                End If
                Dim vEnrollNumber As Integer, vBackupNumber As Integer, vPrivilege As Integer, vnEnableFlag As Integer
                Dim vStrEnrollNumber As String = ""
                If F9.FK_GetIsSupportStringID(gnCommHandleIndex) = RUN_SUCCESS Then
                    Do
                        vnResultCode = F9.FK_GetAllUserID_StringID(gnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
                        If vnResultCode <> RUN_SUCCESS Then
                            If vnResultCode = RUNERR_DATAARRAY_END Then
                                vnResultCode = RUN_SUCCESS
                            End If
                            Exit Do
                        End If
                        Dim vName As String = New String(ChrW(&H20), 256)
                        vnResultCode = F9.FK_GetUserName_StringID(gnCommHandleIndex, vStrEnrollNumber.Trim, vName)
                        paycodelist.Add(vStrEnrollNumber & ";" & vName)
                    Loop
                Else
                    Do
                        vnResultCode = F9.FK_GetAllUserID(gnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
                        If vnResultCode <> RUN_SUCCESS Then
                            If vnResultCode = RUNERR_DATAARRAY_END Then
                                vnResultCode = RUN_SUCCESS
                            End If
                            Exit Do
                        End If
                        Dim vName As String = New String(ChrW(&H20), 256)
                        vnResultCode = F9.FK_GetUserName(gnCommHandleIndex, vEnrollNumber, vName)
                        paycodelist.Add(vEnrollNumber & ";" & vName)
                    Loop
                End If
                '0:
                F9.FK_EnableDevice(gnCommHandleIndex, 1)
                F9.FK_DisConnect(gnCommHandleIndex)
            ElseIf DeviceType.ToString.Trim = "ATF686n" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881
                Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
                Dim gnCommHandleIndex As Long

                gnCommHandleIndex = atf686n.ConnectNet(vpszIPAddress)
                If gnCommHandleIndex > 0 Then
                    'MsgBox("Connection success")
                Else
                    'Return "Connection fail"
                    Continue For
                End If
                Dim vnResultCode As Integer = atf686n.ST_EnableDevice(gnCommHandleIndex, 0)
                If vnResultCode <> RUN_SUCCESS Then
                    atf686n.ST_DisConnect(gnCommHandleIndex)
                    'Return gstrNoDevice
                    Continue For
                End If
                vnResultCode = atf686n.ST_ReadAllUserID(gnCommHandleIndex)
                If vnResultCode <> RUN_SUCCESS Then
                    atf686n.ST_EnableDevice(gnCommHandleIndex, 1)
                    Continue For
                End If
                Dim vEnrollNumber As Integer, vBackupNumber As Integer, vPrivilege As Integer, vnEnableFlag As Integer
                Dim vStrEnrollNumber As String = ""
                If atf686n.ST_GetIsSupportStringID(gnCommHandleIndex) = RUN_SUCCESS Then
                    Do
                        vnResultCode = atf686n.ST_GetAllUserID_SID(gnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
                        If vnResultCode <> RUN_SUCCESS Then
                            If vnResultCode = RUNERR_DATAARRAY_END Then
                                vnResultCode = RUN_SUCCESS
                            End If
                            Exit Do
                        End If
                        Try
                            vStrEnrollNumber = vStrEnrollNumber.Substring(0, vStrEnrollNumber.IndexOf(" ")).Trim
                        Catch ex As Exception

                        End Try

                        Dim vName As String = ""
                        Dim vnInfoSize As Integer = Marshal.SizeOf(GetType(USER_NAME_STRING_ID))
                        Dim bytNameData(vnInfoSize - 1) As Byte
                        Dim mUserNameInfo As USER_NAME_STRING_ID
                        vnResultCode = atf686n.ST_GetOnlyUserName_SID(gnCommHandleIndex, vStrEnrollNumber.Trim, bytNameData)
                        Dim vobj As Object = ConvertByteArrayToStructure(bytNameData, GetType(USER_NAME_STRING_ID))
                        If vobj Is Nothing Then
                            Return
                        End If
                        mUserNameInfo = DirectCast(vobj, USER_NAME_STRING_ID)
                        vName = ByteArrayUtf16ToString(mUserNameInfo.UserName)

                        paycodelist.Add(vStrEnrollNumber & ";" & vName.Trim)
                    Loop
                Else
                    'Do
                    '    vnResultCode = F9.FK_GetAllUserID(gnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
                    '    If vnResultCode <> RUN_SUCCESS Then
                    '        If vnResultCode = RUNERR_DATAARRAY_END Then
                    '            vnResultCode = RUN_SUCCESS
                    '        End If
                    '        Exit Do
                    '    End If
                    '    Dim vName As String = New String(ChrW(&H20), 256)
                    '    vnResultCode = F9.FK_GetUserName(gnCommHandleIndex, vEnrollNumber, vName)
                    '    paycodelist.Add(vEnrollNumber & ";" & vName)
                    'Loop
                End If
                '0:
                atf686n.ST_EnableDevice(gnCommHandleIndex, 1)
                atf686n.ST_DisConnect(gnCommHandleIndex)
            ElseIf DeviceType.ToString.Trim = "TF-01" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881
                Dim F9 As mdlPublic_f9 = New mdlPublic_f9

                Dim bRet As Boolean
                bRet = AxFP_CLOCK1.SetIPAddress(vpszIPAddress, vpszNetPort, vpszNetPassword)
                Me.AxFP_CLOCK1.OpenCommPort(vnMachineNumber)

                If Not bRet Then
                    Continue For

                End If

                bRet = AxFP_CLOCK1.EnableDevice(vnMachineNumber, False)

                If Not bRet Then
                    AxFP_CLOCK1.CloseCommPort()
                    Continue For
                End If
                bRet = AxFP_CLOCK1.ReadAllUserID(vnMachineNumber)
                If Not bRet Then
                    AxFP_CLOCK1.CloseCommPort()
                    Continue For
                End If
                Dim dwEnrollNumber As Integer = 0
                Dim dwMachineNumber As Integer = 0
                Dim dwBackupNumber As Integer = 0
                Dim dwUserPrivilege As Integer = 0
                Dim dwAttendenceEnable As Integer = 0
                Dim IsOver As Boolean = True

                Dim vStrEnrollNumber As String = ""
                While (IsOver)
                    IsOver = AxFP_CLOCK1.GetAllUserID(vnMachineNumber, dwEnrollNumber, dwMachineNumber, dwBackupNumber, dwUserPrivilege, dwAttendenceEnable)
                    Dim strName As String = ""
                    Dim obj As Object = New System.Runtime.InteropServices.VariantWrapper(strName)
                    Dim ob As New Object()
                    ob = strName

                    bRet = AxFP_CLOCK1.GetUserName(0, vnMachineNumber, dwEnrollNumber, dwEnrollNumber, obj)

                    If Not bRet Then
                        obj = dwEnrollNumber
                    End If
                    paycodelist.Add(dwEnrollNumber.ToString & ";" & obj)

                End While
                AxFP_CLOCK1.EnableDevice(vnMachineNumber, True)
                AxFP_CLOCK1.CloseCommPort()
            ElseIf DeviceType.ToString.Trim = "ZK Controller" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                Dim ret As Integer = 0
                Dim str As String = "CardNo" & vbTab & " Pin" & vbTab & " Password" & vbTab & " Group" & vbTab & " StartTime" & vbTab & " EndTime"
                Dim BUFFERSIZE As Integer = 1 * 1024 * 1024
                Dim buffer(BUFFERSIZE - 1) As Byte
                Dim options As String = ""

                Dim ConnectT = "protocol=TCP,ipaddress=" & vpszIPAddress.ToString.Trim & ",port=4370,timeout=2000,passwd="
                Dim Data As String = ""
                Dim Data1 As String = ""

                If IntPtr.Zero = h Then
                    h = ZKController.Connect(ConnectT)
                End If
                If h <> IntPtr.Zero Then
                    ret = ZKController.GetDeviceData(h, buffer(0), BUFFERSIZE - 1, "user", str.Replace(" ", ""), "", options)
                    If ret > 0 Then
                        Dim Record As String = System.Text.Encoding.UTF8.GetString(buffer)
                        Dim RecordArr() As String = Record.Split(vbNewLine)

                        For m As Integer = 1 To RecordArr.Length - 1
                            If RecordArr(m).Trim(vbNullChar).Trim(vbLf) = "" Then
                                Continue For
                            End If
                            Dim TZArrRowData() As String = RecordArr(m).Split(",")
                            Dim dwEnrollNumber As String = TZArrRowData(1).Trim(vbNullChar).Trim(vbLf)
                            Dim MachineCard As String = TZArrRowData(0).Trim(vbNullChar).Trim(vbLf)
                            paycodelist.Add(dwEnrollNumber.ToString & ";" & dwEnrollNumber & "," & MachineCard)
                        Next

                    End If
                End If


                ' paycodelist.Add(dwEnrollNumber.ToString & ";" & obj)

            End If
        Next
        paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
        Common.CreateEmployeeZKAccess(paycodeArray)

        ''add employee while downloading logs
        ''add employee while downloading logs
        ''Dim adapS As SqlDataAdapter
        ''Dim adapAc As OleDbDataAdapter
        ''Dim Rs As DataSet = New DataSet

        'Dim adap As SqlDataAdapter
        'Dim adapA As OleDbDataAdapter
        'Dim ds As DataSet

        ''Dim sSql1 As String = " Select CreateEmpPunchDwnld, WOInclude from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup )"
        ''If Common.servername = "Access" Then
        ''    sSql1 = " Select CreateEmpPunchDwnld, WOInclude from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
        ''    adapAc = New OleDbDataAdapter(sSql1, Common.con1)
        ''    adapAc.Fill(Rs)
        ''Else
        ''    adapS = New SqlDataAdapter(sSql1, Common.con)
        ''    adapS.Fill(Rs)
        ''End If
        'Dim g_Wo_Include As String = Common.g_Wo_Include 'Rs.Tables(0).Rows(0).Item("WOInclude").ToString.Trim

        'For i As Integer = 0 To paycodeArray.Length - 1
        '    'Dim sSqlXX As String = "select PAYCODE from TblEmployee where PAYCODE='" & paycodeArray(i) & "'"
        '    Dim tmpPay() As String = paycodeArray(i).ToString.Split(";")
        '    Dim tmpEmpPre As String
        '    Dim tmpEmpName As String = ""
        '    Try
        '        tmpEmpName = tmpPay(1)
        '    Catch ex As Exception
        '        tmpEmpName = ""
        '    End Try
        '    If IsNumeric(paycodeArray(i)) = True Then
        '        'tmpEmpPre = Convert.ToDouble(paycodeArray(i)).ToString("000000000000")
        '        tmpEmpPre = Convert.ToDouble(tmpPay(0)).ToString("000000000000")
        '    Else
        '        'tmpEmpPre = paycodeArray(i)
        '        tmpEmpPre = tmpPay(0)
        '    End If
        '    Dim sSqlXX As String = "select PAYCODE from TblEmployee where PRESENTCARDNO='" & tmpEmpPre & "'"
        '    ds = New DataSet
        '    If Common.servername = "Access" Then
        '        adapA = New OleDbDataAdapter(sSqlXX, Common.con1)
        '        adapA.Fill(ds)
        '    Else
        '        adap = New SqlDataAdapter(sSqlXX, Common.con)
        '        adap.Fill(ds)
        '    End If
        '    If ds.Tables(0).Rows.Count = 0 Then
        '        Common.InsertEmpOtherDetailsWhileDataDownload(tmpPay(0), Shift, mComp, mDept, mBranch, mEmpGrp, mmgrade, mmcat, mDateofjoin, mdateofbirth, _
        '               SHIFTTYPE, SHIFTPATTERN, SHIFTREMAINDAYS, LASTSHIFTPERFORMED, INONLY, ISPUNCHALL, ISTIMELOSSALLOWED, ALTERNATE_OFF_DAYS, CDAYS, ISROUNDTHECLOCKWORK, _
        '           ISOT, OTRATE, FIRSTOFFDAY, SECONDOFFTYPE, HALFDAYSHIFT, SECONDOFFDAY, PERMISLATEARRIVAL, PERMISEARLYDEPRT, ISAUTOSHIFT, ISOUTWORK, MAXDAYMIN, ISOS, AUTH_SHIFTS, TIME1, _
        '           SHORT1, HALF, ISHALFDAY, ISSHORT, TWO, tmpEmpName)
        '        com.DutyRoster(tmpPay(0), Now.ToString("01/" & Now.Month & "/" & Now.Year), g_Wo_Include)
        '    End If
        '    'end add employee while downloading logs
        'Next
        SplashScreenManager.CloseForm(False)
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        Me.Close()
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
End Class