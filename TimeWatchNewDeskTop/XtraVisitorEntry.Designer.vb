﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraVisitorEntry
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridLevelNode2 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraVisitorEntry))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditCard = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEdit1 = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControl1 = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLOCATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeviceType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcommkey = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEditEnd = New DevExpress.XtraEditors.DateEdit()
        Me.DateEditStart = New DevExpress.XtraEditors.DateEdit()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButtonFPCopy = New DevExpress.XtraEditors.SimpleButton()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.textRes = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButtonStop = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonInit = New DevExpress.XtraEditors.SimpleButton()
        Me.picFPImg = New DevExpress.XtraEditors.PictureEdit()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.VisitorPictureEdit = New DevExpress.XtraEditors.PictureEdit()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEditTools = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditIssuedBy = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditInTime = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditAppTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LookUpEditDept = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblDepartmentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LookUpEditEmp = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditCoVisi2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditCoVisi1 = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEditPersonal = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditOfficial = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditVehiNo = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditConNo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEditAddress = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditVComp = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditName = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditSlNo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPhone = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LookUpEditComp = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblCompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblCompanyTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter()
        Me.TblCompany1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter()
        Me.TblEmployee1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.TblEmployeeTableAdapter = New iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblDepartmentTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter()
        Me.TblDepartment1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter()
        Me.TblMachineTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        Me.TblMachine1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.TextEditCard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditEnd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textRes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picFPImg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.VisitorPictureEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.MemoEditTools.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditIssuedBy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditInTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditAppTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEditDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEditEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditCoVisi2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditCoVisi1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.CheckEditPersonal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditOfficial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditVehiNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditConNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEditAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditVComp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditSlNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPhone.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEditComp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.LabelControl22)
        Me.GroupControl1.Controls.Add(Me.LabelControl42)
        Me.GroupControl1.Controls.Add(Me.TextEditCard)
        Me.GroupControl1.Controls.Add(Me.SimpleButton3)
        Me.GroupControl1.Controls.Add(Me.LabelControl21)
        Me.GroupControl1.Controls.Add(Me.LabelControl20)
        Me.GroupControl1.Controls.Add(Me.PopupContainerEdit1)
        Me.GroupControl1.Controls.Add(Me.LabelControl19)
        Me.GroupControl1.Controls.Add(Me.LabelControl18)
        Me.GroupControl1.Controls.Add(Me.DateEditEnd)
        Me.GroupControl1.Controls.Add(Me.DateEditStart)
        Me.GroupControl1.Controls.Add(Me.GroupControl4)
        Me.GroupControl1.Controls.Add(Me.PopupContainerControl1)
        Me.GroupControl1.Controls.Add(Me.PictureBox1)
        Me.GroupControl1.Controls.Add(Me.textRes)
        Me.GroupControl1.Controls.Add(Me.SimpleButtonStop)
        Me.GroupControl1.Controls.Add(Me.SimpleButtonInit)
        Me.GroupControl1.Controls.Add(Me.picFPImg)
        Me.GroupControl1.Controls.Add(Me.SimpleButtonSave)
        Me.GroupControl1.Controls.Add(Me.GroupControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl17)
        Me.GroupControl1.Controls.Add(Me.MemoEditTools)
        Me.GroupControl1.Controls.Add(Me.LabelControl16)
        Me.GroupControl1.Controls.Add(Me.TextEditIssuedBy)
        Me.GroupControl1.Controls.Add(Me.LabelControl15)
        Me.GroupControl1.Controls.Add(Me.LabelControl14)
        Me.GroupControl1.Controls.Add(Me.TextEditInTime)
        Me.GroupControl1.Controls.Add(Me.TextEditAppTime)
        Me.GroupControl1.Controls.Add(Me.LabelControl13)
        Me.GroupControl1.Controls.Add(Me.LookUpEditDept)
        Me.GroupControl1.Controls.Add(Me.LabelControl12)
        Me.GroupControl1.Controls.Add(Me.LookUpEditEmp)
        Me.GroupControl1.Controls.Add(Me.LabelControl11)
        Me.GroupControl1.Controls.Add(Me.LabelControl10)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Controls.Add(Me.TextEditCoVisi2)
        Me.GroupControl1.Controls.Add(Me.TextEditCoVisi1)
        Me.GroupControl1.Controls.Add(Me.GroupControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.TextEditVehiNo)
        Me.GroupControl1.Controls.Add(Me.TextEditConNo)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.MemoEditAddress)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.TextEditVComp)
        Me.GroupControl1.Controls.Add(Me.TextEditName)
        Me.GroupControl1.Controls.Add(Me.TextEditSlNo)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.TextEditPhone)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LookUpEditComp)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(1036, 568)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Visitor Pass"
        '
        'LabelControl42
        '
        Me.LabelControl42.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl42.Appearance.Options.UseFont = True
        Me.LabelControl42.Location = New System.Drawing.Point(351, 542)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(24, 14)
        Me.LabelControl42.TabIndex = 64
        Me.LabelControl42.Text = "Card"
        '
        'TextEditCard
        '
        Me.TextEditCard.EditValue = ""
        Me.TextEditCard.Location = New System.Drawing.Point(395, 541)
        Me.TextEditCard.Name = "TextEditCard"
        Me.TextEditCard.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditCard.Properties.Appearance.Options.UseFont = True
        Me.TextEditCard.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditCard.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditCard.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditCard.Properties.MaxLength = 10
        Me.TextEditCard.Size = New System.Drawing.Size(135, 20)
        Me.TextEditCard.TabIndex = 63
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton3.Appearance.Options.UseFont = True
        Me.SimpleButton3.Location = New System.Drawing.Point(730, 493)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(77, 23)
        Me.SimpleButton3.TabIndex = 51
        Me.SimpleButton3.Text = "Print"
        Me.SimpleButton3.Visible = False
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(16, 544)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(136, 14)
        Me.LabelControl21.TabIndex = 50
        Me.LabelControl21.Text = "Finger Template to Copy"
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Location = New System.Drawing.Point(351, 518)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(21, 14)
        Me.LabelControl20.TabIndex = 49
        Me.LabelControl20.Text = "End"
        '
        'PopupContainerEdit1
        '
        Me.PopupContainerEdit1.Location = New System.Drawing.Point(175, 541)
        Me.PopupContainerEdit1.Name = "PopupContainerEdit1"
        Me.PopupContainerEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEdit1.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEdit1.Properties.PopupControl = Me.PopupContainerControl1
        Me.PopupContainerEdit1.Size = New System.Drawing.Size(150, 20)
        Me.PopupContainerEdit1.TabIndex = 43
        '
        'PopupContainerControl1
        '
        Me.PopupContainerControl1.Controls.Add(Me.GridControl1)
        Me.PopupContainerControl1.Location = New System.Drawing.Point(813, 493)
        Me.PopupContainerControl1.Name = "PopupContainerControl1"
        Me.PopupContainerControl1.Size = New System.Drawing.Size(340, 175)
        Me.PopupContainerControl1.TabIndex = 42
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblMachineBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        GridLevelNode2.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode2})
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(340, 175)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID_NO, Me.colLOCATION, Me.colbranch, Me.colDeviceType, Me.colA_R, Me.GridColumn1, Me.colcommkey})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'colID_NO
        '
        Me.colID_NO.FieldName = "ID_NO"
        Me.colID_NO.Name = "colID_NO"
        Me.colID_NO.Visible = True
        Me.colID_NO.VisibleIndex = 1
        '
        'colLOCATION
        '
        Me.colLOCATION.FieldName = "LOCATION"
        Me.colLOCATION.Name = "colLOCATION"
        Me.colLOCATION.Visible = True
        Me.colLOCATION.VisibleIndex = 3
        '
        'colbranch
        '
        Me.colbranch.Caption = "GridColumn1"
        Me.colbranch.FieldName = "branch"
        Me.colbranch.Name = "colbranch"
        Me.colbranch.Visible = True
        Me.colbranch.VisibleIndex = 2
        '
        'colDeviceType
        '
        Me.colDeviceType.Caption = "GridColumn1"
        Me.colDeviceType.FieldName = "DeviceType"
        Me.colDeviceType.Name = "colDeviceType"
        '
        'colA_R
        '
        Me.colA_R.FieldName = "A_R"
        Me.colA_R.Name = "colA_R"
        '
        'GridColumn1
        '
        Me.GridColumn1.FieldName = "Purpose"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'colcommkey
        '
        Me.colcommkey.Caption = "GridColumn2"
        Me.colcommkey.FieldName = "commkey"
        Me.colcommkey.Name = "colcommkey"
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(129, 518)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(27, 14)
        Me.LabelControl19.TabIndex = 48
        Me.LabelControl19.Text = "Start"
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(16, 518)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(38, 14)
        Me.LabelControl18.TabIndex = 47
        Me.LabelControl18.Text = "Validity"
        '
        'DateEditEnd
        '
        Me.DateEditEnd.EditValue = Nothing
        Me.DateEditEnd.Location = New System.Drawing.Point(395, 515)
        Me.DateEditEnd.Name = "DateEditEnd"
        Me.DateEditEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditEnd.Properties.Appearance.Options.UseFont = True
        Me.DateEditEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditEnd.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm"
        Me.DateEditEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditEnd.Size = New System.Drawing.Size(135, 20)
        Me.DateEditEnd.TabIndex = 46
        '
        'DateEditStart
        '
        Me.DateEditStart.EditValue = Nothing
        Me.DateEditStart.Location = New System.Drawing.Point(175, 515)
        Me.DateEditStart.Name = "DateEditStart"
        Me.DateEditStart.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditStart.Properties.Appearance.Options.UseFont = True
        Me.DateEditStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditStart.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm"
        Me.DateEditStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditStart.Size = New System.Drawing.Size(135, 20)
        Me.DateEditStart.TabIndex = 45
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControl4.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl4.Controls.Add(Me.SimpleButtonFPCopy)
        Me.GroupControl4.Location = New System.Drawing.Point(732, 399)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(264, 88)
        Me.GroupControl4.TabIndex = 44
        Me.GroupControl4.Text = "Finger Template to Copy"
        Me.GroupControl4.Visible = False
        '
        'SimpleButtonFPCopy
        '
        Me.SimpleButtonFPCopy.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonFPCopy.Appearance.Options.UseFont = True
        Me.SimpleButtonFPCopy.Location = New System.Drawing.Point(5, 51)
        Me.SimpleButtonFPCopy.Name = "SimpleButtonFPCopy"
        Me.SimpleButtonFPCopy.Size = New System.Drawing.Size(96, 23)
        Me.SimpleButtonFPCopy.TabIndex = 44
        Me.SimpleButtonFPCopy.Text = "Copy"
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(849, 175)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(147, 174)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 41
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'textRes
        '
        Me.textRes.Location = New System.Drawing.Point(848, 355)
        Me.textRes.Name = "textRes"
        Me.textRes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.textRes.Properties.Appearance.Options.UseFont = True
        Me.textRes.Properties.ReadOnly = True
        Me.textRes.Size = New System.Drawing.Size(190, 20)
        Me.textRes.TabIndex = 40
        Me.textRes.Visible = False
        '
        'SimpleButtonStop
        '
        Me.SimpleButtonStop.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonStop.Appearance.Options.UseFont = True
        Me.SimpleButtonStop.Location = New System.Drawing.Point(732, 362)
        Me.SimpleButtonStop.Name = "SimpleButtonStop"
        Me.SimpleButtonStop.Size = New System.Drawing.Size(96, 23)
        Me.SimpleButtonStop.TabIndex = 39
        Me.SimpleButtonStop.Text = "Stop Reader"
        '
        'SimpleButtonInit
        '
        Me.SimpleButtonInit.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonInit.Appearance.Options.UseFont = True
        Me.SimpleButtonInit.Location = New System.Drawing.Point(732, 333)
        Me.SimpleButtonInit.Name = "SimpleButtonInit"
        Me.SimpleButtonInit.Size = New System.Drawing.Size(96, 23)
        Me.SimpleButtonInit.TabIndex = 38
        Me.SimpleButtonInit.Text = "Init Reader"
        '
        'picFPImg
        '
        Me.picFPImg.Cursor = System.Windows.Forms.Cursors.Default
        Me.picFPImg.Location = New System.Drawing.Point(536, 333)
        Me.picFPImg.Name = "picFPImg"
        Me.picFPImg.Properties.NullText = " "
        Me.picFPImg.Properties.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.ByteArray
        Me.picFPImg.Properties.ZoomPercent = 60.0R
        Me.picFPImg.Size = New System.Drawing.Size(190, 228)
        Me.picFPImg.TabIndex = 37
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(732, 538)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 36
        Me.SimpleButtonSave.Text = "Save"
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.VisitorPictureEdit)
        Me.GroupControl3.Controls.Add(Me.SidePanel1)
        Me.GroupControl3.Location = New System.Drawing.Point(536, 32)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(292, 277)
        Me.GroupControl3.TabIndex = 35
        Me.GroupControl3.Text = "Picture"
        '
        'VisitorPictureEdit
        '
        Me.VisitorPictureEdit.Cursor = System.Windows.Forms.Cursors.Default
        Me.VisitorPictureEdit.EditValue = "<Null>"
        Me.VisitorPictureEdit.Location = New System.Drawing.Point(6, 25)
        Me.VisitorPictureEdit.Name = "VisitorPictureEdit"
        Me.VisitorPictureEdit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.VisitorPictureEdit.Properties.Appearance.Options.UseFont = True
        Me.VisitorPictureEdit.Properties.LookAndFeel.SkinName = "iMaginary"
        Me.VisitorPictureEdit.Properties.LookAndFeel.UseDefaultLookAndFeel = False
        Me.VisitorPictureEdit.Properties.NullText = "Double click to open Camera"
        Me.VisitorPictureEdit.Properties.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.ByteArray
        Me.VisitorPictureEdit.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Always
        Me.VisitorPictureEdit.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.VisitorPictureEdit.Size = New System.Drawing.Size(281, 202)
        Me.VisitorPictureEdit.TabIndex = 36
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.SimpleButton2)
        Me.SidePanel1.Controls.Add(Me.SimpleButton1)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(2, 233)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(288, 42)
        Me.SidePanel1.TabIndex = 37
        Me.SidePanel1.Text = "SidePanel1"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(85, 10)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 1
        Me.SimpleButton2.Text = "Stop"
        Me.SimpleButton2.Visible = False
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(4, 10)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 0
        Me.SimpleButton1.Text = "Start"
        Me.SimpleButton1.Visible = False
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(16, 450)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(151, 14)
        Me.LabelControl17.TabIndex = 34
        Me.LabelControl17.Text = "Returnable Materials / Tools"
        '
        'MemoEditTools
        '
        Me.MemoEditTools.Location = New System.Drawing.Point(175, 448)
        Me.MemoEditTools.Name = "MemoEditTools"
        Me.MemoEditTools.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.MemoEditTools.Properties.Appearance.Options.UseFont = True
        Me.MemoEditTools.Properties.MaxLength = 50
        Me.MemoEditTools.Size = New System.Drawing.Size(355, 61)
        Me.MemoEditTools.TabIndex = 33
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(16, 425)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(52, 14)
        Me.LabelControl16.TabIndex = 32
        Me.LabelControl16.Text = "Issued By"
        '
        'TextEditIssuedBy
        '
        Me.TextEditIssuedBy.Enabled = False
        Me.TextEditIssuedBy.Location = New System.Drawing.Point(175, 422)
        Me.TextEditIssuedBy.Name = "TextEditIssuedBy"
        Me.TextEditIssuedBy.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditIssuedBy.Properties.Appearance.Options.UseFont = True
        Me.TextEditIssuedBy.Properties.MaxLength = 50
        Me.TextEditIssuedBy.Size = New System.Drawing.Size(355, 20)
        Me.TextEditIssuedBy.TabIndex = 31
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(388, 399)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(42, 14)
        Me.LabelControl15.TabIndex = 30
        Me.LabelControl15.Text = "In Time"
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(16, 399)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(98, 14)
        Me.LabelControl14.TabIndex = 29
        Me.LabelControl14.Text = "Appoinment Time"
        '
        'TextEditInTime
        '
        Me.TextEditInTime.EditValue = "00:10"
        Me.TextEditInTime.Location = New System.Drawing.Point(458, 396)
        Me.TextEditInTime.Name = "TextEditInTime"
        Me.TextEditInTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditInTime.Properties.Appearance.Options.UseFont = True
        Me.TextEditInTime.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditInTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditInTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditInTime.Properties.MaxLength = 5
        Me.TextEditInTime.Size = New System.Drawing.Size(72, 20)
        Me.TextEditInTime.TabIndex = 28
        '
        'TextEditAppTime
        '
        Me.TextEditAppTime.EditValue = "00:00"
        Me.TextEditAppTime.Location = New System.Drawing.Point(175, 396)
        Me.TextEditAppTime.Name = "TextEditAppTime"
        Me.TextEditAppTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditAppTime.Properties.Appearance.Options.UseFont = True
        Me.TextEditAppTime.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditAppTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditAppTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditAppTime.Properties.MaxLength = 5
        Me.TextEditAppTime.Size = New System.Drawing.Size(72, 20)
        Me.TextEditAppTime.TabIndex = 27
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(16, 373)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl13.TabIndex = 26
        Me.LabelControl13.Text = "Department"
        '
        'LookUpEditDept
        '
        Me.LookUpEditDept.Location = New System.Drawing.Point(175, 370)
        Me.LookUpEditDept.Name = "LookUpEditDept"
        Me.LookUpEditDept.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditDept.Properties.Appearance.Options.UseFont = True
        Me.LookUpEditDept.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditDept.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEditDept.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditDept.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEditDept.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditDept.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEditDept.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEditDept.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEditDept.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("DEPARTMENTCODE", "Dept Code", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("DEPARTMENTNAME", "Name")})
        Me.LookUpEditDept.Properties.DataSource = Me.TblDepartmentBindingSource
        Me.LookUpEditDept.Properties.DisplayMember = "DEPARTMENTCODE"
        Me.LookUpEditDept.Properties.MaxLength = 12
        Me.LookUpEditDept.Properties.NullText = ""
        Me.LookUpEditDept.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEditDept.Properties.ValueMember = "DEPARTMENTCODE"
        Me.LookUpEditDept.Size = New System.Drawing.Size(355, 20)
        Me.LookUpEditDept.TabIndex = 25
        '
        'TblDepartmentBindingSource
        '
        Me.TblDepartmentBindingSource.DataMember = "tblDepartment"
        Me.TblDepartmentBindingSource.DataSource = Me.SSSDBDataSet
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(16, 347)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl12.TabIndex = 24
        Me.LabelControl12.Text = "To Meet"
        '
        'LookUpEditEmp
        '
        Me.LookUpEditEmp.Location = New System.Drawing.Point(175, 344)
        Me.LookUpEditEmp.Name = "LookUpEditEmp"
        Me.LookUpEditEmp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditEmp.Properties.Appearance.Options.UseFont = True
        Me.LookUpEditEmp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditEmp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEditEmp.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditEmp.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEditEmp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditEmp.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEditEmp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEditEmp.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEditEmp.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PAYCODE", "PAYCODE", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EMPNAME", "Name")})
        Me.LookUpEditEmp.Properties.DisplayMember = "PAYCODE"
        Me.LookUpEditEmp.Properties.MaxLength = 12
        Me.LookUpEditEmp.Properties.NullText = ""
        Me.LookUpEditEmp.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEditEmp.Properties.ValueMember = "PAYCODE"
        Me.LookUpEditEmp.Size = New System.Drawing.Size(355, 20)
        Me.LookUpEditEmp.TabIndex = 23
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(158, 295)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(11, 14)
        Me.LabelControl10.TabIndex = 21
        Me.LabelControl10.Text = "1."
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(16, 295)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(51, 14)
        Me.LabelControl9.TabIndex = 20
        Me.LabelControl9.Text = "Co-Visitor"
        '
        'TextEditCoVisi2
        '
        Me.TextEditCoVisi2.Location = New System.Drawing.Point(175, 318)
        Me.TextEditCoVisi2.Name = "TextEditCoVisi2"
        Me.TextEditCoVisi2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditCoVisi2.Properties.Appearance.Options.UseFont = True
        Me.TextEditCoVisi2.Properties.MaxLength = 50
        Me.TextEditCoVisi2.Size = New System.Drawing.Size(355, 20)
        Me.TextEditCoVisi2.TabIndex = 19
        '
        'TextEditCoVisi1
        '
        Me.TextEditCoVisi1.Location = New System.Drawing.Point(175, 292)
        Me.TextEditCoVisi1.Name = "TextEditCoVisi1"
        Me.TextEditCoVisi1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditCoVisi1.Properties.Appearance.Options.UseFont = True
        Me.TextEditCoVisi1.Properties.MaxLength = 50
        Me.TextEditCoVisi1.Size = New System.Drawing.Size(355, 20)
        Me.TextEditCoVisi1.TabIndex = 18
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.CheckEditPersonal)
        Me.GroupControl2.Controls.Add(Me.CheckEditOfficial)
        Me.GroupControl2.Location = New System.Drawing.Point(303, 240)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(227, 46)
        Me.GroupControl2.TabIndex = 17
        Me.GroupControl2.Text = "Purpose"
        '
        'CheckEditPersonal
        '
        Me.CheckEditPersonal.Location = New System.Drawing.Point(119, 20)
        Me.CheckEditPersonal.Name = "CheckEditPersonal"
        Me.CheckEditPersonal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPersonal.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPersonal.Properties.Caption = "Personal"
        Me.CheckEditPersonal.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditPersonal.Properties.RadioGroupIndex = 0
        Me.CheckEditPersonal.Size = New System.Drawing.Size(79, 19)
        Me.CheckEditPersonal.TabIndex = 1
        Me.CheckEditPersonal.TabStop = False
        '
        'CheckEditOfficial
        '
        Me.CheckEditOfficial.EditValue = True
        Me.CheckEditOfficial.Location = New System.Drawing.Point(15, 20)
        Me.CheckEditOfficial.Name = "CheckEditOfficial"
        Me.CheckEditOfficial.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditOfficial.Properties.Appearance.Options.UseFont = True
        Me.CheckEditOfficial.Properties.Caption = "Official"
        Me.CheckEditOfficial.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditOfficial.Properties.RadioGroupIndex = 0
        Me.CheckEditOfficial.Size = New System.Drawing.Size(79, 19)
        Me.CheckEditOfficial.TabIndex = 0
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(16, 269)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl8.TabIndex = 16
        Me.LabelControl8.Text = "Vehicle No"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(16, 243)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(62, 14)
        Me.LabelControl7.TabIndex = 15
        Me.LabelControl7.Text = "Contact No"
        '
        'TextEditVehiNo
        '
        Me.TextEditVehiNo.Location = New System.Drawing.Point(175, 266)
        Me.TextEditVehiNo.Name = "TextEditVehiNo"
        Me.TextEditVehiNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditVehiNo.Properties.Appearance.Options.UseFont = True
        Me.TextEditVehiNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditVehiNo.Properties.MaxLength = 10
        Me.TextEditVehiNo.Size = New System.Drawing.Size(122, 20)
        Me.TextEditVehiNo.TabIndex = 14
        '
        'TextEditConNo
        '
        Me.TextEditConNo.Location = New System.Drawing.Point(175, 240)
        Me.TextEditConNo.Name = "TextEditConNo"
        Me.TextEditConNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditConNo.Properties.Appearance.Options.UseFont = True
        Me.TextEditConNo.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditConNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditConNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditConNo.Properties.MaxLength = 16
        Me.TextEditConNo.Size = New System.Drawing.Size(122, 20)
        Me.TextEditConNo.TabIndex = 13
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(16, 139)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(80, 14)
        Me.LabelControl6.TabIndex = 12
        Me.LabelControl6.Text = "Visitor Address"
        '
        'MemoEditAddress
        '
        Me.MemoEditAddress.Location = New System.Drawing.Point(175, 138)
        Me.MemoEditAddress.Name = "MemoEditAddress"
        Me.MemoEditAddress.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.MemoEditAddress.Properties.Appearance.Options.UseFont = True
        Me.MemoEditAddress.Properties.MaxLength = 50
        Me.MemoEditAddress.Size = New System.Drawing.Size(355, 96)
        Me.MemoEditAddress.TabIndex = 11
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(16, 114)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(87, 14)
        Me.LabelControl5.TabIndex = 10
        Me.LabelControl5.Text = "Visitor Company"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(16, 88)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(68, 14)
        Me.LabelControl4.TabIndex = 9
        Me.LabelControl4.Text = "Visitor Name"
        '
        'TextEditVComp
        '
        Me.TextEditVComp.Location = New System.Drawing.Point(175, 111)
        Me.TextEditVComp.Name = "TextEditVComp"
        Me.TextEditVComp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditVComp.Properties.Appearance.Options.UseFont = True
        Me.TextEditVComp.Properties.MaxLength = 50
        Me.TextEditVComp.Size = New System.Drawing.Size(355, 20)
        Me.TextEditVComp.TabIndex = 8
        '
        'TextEditName
        '
        Me.TextEditName.Location = New System.Drawing.Point(175, 85)
        Me.TextEditName.Name = "TextEditName"
        Me.TextEditName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditName.Properties.Appearance.Options.UseFont = True
        Me.TextEditName.Properties.MaxLength = 50
        Me.TextEditName.Size = New System.Drawing.Size(355, 20)
        Me.TextEditName.TabIndex = 7
        '
        'TextEditSlNo
        '
        Me.TextEditSlNo.Enabled = False
        Me.TextEditSlNo.Location = New System.Drawing.Point(408, 59)
        Me.TextEditSlNo.Name = "TextEditSlNo"
        Me.TextEditSlNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditSlNo.Properties.Appearance.Options.UseFont = True
        Me.TextEditSlNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditSlNo.Properties.MaxLength = 25
        Me.TextEditSlNo.Size = New System.Drawing.Size(122, 20)
        Me.TextEditSlNo.TabIndex = 6
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(360, 62)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(28, 14)
        Me.LabelControl3.TabIndex = 5
        Me.LabelControl3.Text = "Sl No"
        '
        'TextEditPhone
        '
        Me.TextEditPhone.Location = New System.Drawing.Point(175, 59)
        Me.TextEditPhone.Name = "TextEditPhone"
        Me.TextEditPhone.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPhone.Properties.Appearance.Options.UseFont = True
        Me.TextEditPhone.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditPhone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditPhone.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditPhone.Properties.MaxLength = 15
        Me.TextEditPhone.Size = New System.Drawing.Size(122, 20)
        Me.TextEditPhone.TabIndex = 4
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(16, 65)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(51, 14)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "Adhar No"
        '
        'LookUpEditComp
        '
        Me.LookUpEditComp.Location = New System.Drawing.Point(175, 33)
        Me.LookUpEditComp.Name = "LookUpEditComp"
        Me.LookUpEditComp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditComp.Properties.Appearance.Options.UseFont = True
        Me.LookUpEditComp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditComp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEditComp.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditComp.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEditComp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditComp.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEditComp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEditComp.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEditComp.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("COMPANYCODE", "Company Code", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("COMPANYNAME", "Name")})
        Me.LookUpEditComp.Properties.DataSource = Me.TblCompanyBindingSource
        Me.LookUpEditComp.Properties.DisplayMember = "COMPANYCODE"
        Me.LookUpEditComp.Properties.MaxLength = 12
        Me.LookUpEditComp.Properties.NullText = ""
        Me.LookUpEditComp.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEditComp.Properties.ValueMember = "COMPANYCODE"
        Me.LookUpEditComp.Size = New System.Drawing.Size(355, 20)
        Me.LookUpEditComp.TabIndex = 2
        '
        'TblCompanyBindingSource
        '
        Me.TblCompanyBindingSource.DataMember = "tblCompany"
        Me.TblCompanyBindingSource.DataSource = Me.SSSDBDataSet
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(16, 36)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(122, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Company to be visited"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 1
        '
        'TblCompanyTableAdapter
        '
        Me.TblCompanyTableAdapter.ClearBeforeFill = True
        '
        'TblCompany1TableAdapter1
        '
        Me.TblCompany1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblDepartmentTableAdapter
        '
        Me.TblDepartmentTableAdapter.ClearBeforeFill = True
        '
        'TblDepartment1TableAdapter1
        '
        Me.TblDepartment1TableAdapter1.ClearBeforeFill = True
        '
        'TblMachineTableAdapter
        '
        Me.TblMachineTableAdapter.ClearBeforeFill = True
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Location = New System.Drawing.Point(16, 321)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl22.TabIndex = 65
        Me.LabelControl22.Text = "Escorting Name"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(158, 321)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(11, 14)
        Me.LabelControl11.TabIndex = 22
        Me.LabelControl11.Text = "2."
        Me.LabelControl11.Visible = False
        '
        'XtraVisitorEntry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraVisitorEntry"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.TextEditCard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditEnd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textRes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picFPImg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.VisitorPictureEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        CType(Me.MemoEditTools.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditIssuedBy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditInTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditAppTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEditDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEditEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditCoVisi2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditCoVisi1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.CheckEditPersonal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditOfficial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditVehiNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditConNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEditAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditVComp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditSlNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPhone.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEditComp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LookUpEditComp As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents TblCompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblCompanyTableAdapter As iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter
    Friend WithEvents TblCompany1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPhone As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditSlNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditVComp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEditAddress As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEditVehiNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditConNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEditPersonal As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditOfficial As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditCoVisi2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditCoVisi1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LookUpEditEmp As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents TblEmployee1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents TblEmployeeTableAdapter As iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LookUpEditDept As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents TblDepartmentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblDepartmentTableAdapter As iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter
    Friend WithEvents TblDepartment1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditInTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditAppTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditIssuedBy As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEditTools As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents VisitorPictureEdit As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents picFPImg As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents SimpleButtonInit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonStop As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents textRes As DevExpress.XtraEditors.TextEdit
    Private WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PopupContainerControl1 As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLOCATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeviceType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colA_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblMachineTableAdapter As iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents TblMachine1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents PopupContainerEdit1 As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButtonFPCopy As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEditEnd As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateEditStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colcommkey As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditCard As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
End Class
