﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

'<Assembly: AssemblyTitle("TimeWatchNewDeskTop")> 
'<Assembly: AssemblyDescription("")> 
'<Assembly: AssemblyCompany("")> 
'<Assembly: AssemblyProduct("TimeWatchNewDeskTop")> 
'<Assembly: AssemblyCopyright("Copyright ©  2017")> 
'<Assembly: AssemblyTrademark("")> 

<Assembly: AssemblyTitle("iAS")> 
<Assembly: AssemblyDescription("Intehrated Attendance System")> 
<Assembly: AssemblyCompany("TimeWatch")> 
<Assembly: AssemblyProduct("iAS")> 
<Assembly: AssemblyCopyright("Copyright ©  2019")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("950ed56e-c19a-47b6-af9d-df912ce73ca0")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.3.0.35")>
<Assembly: AssemblyFileVersion("2.3.0.35")>
