﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class XtraTimeOfficePolicyNew
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraTimeOfficePolicyNew))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.ToggleSwitchCanteen = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleSwitchIsNepali = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl56 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchVisitor = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleDownloadAtStartUp = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl55 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl54 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEditOnline = New DevExpress.XtraEditors.DateEdit()
        Me.ToggleonlineStartUp = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControlCloud = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditiTwCDB = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditiDMSDB = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditTWAccessDB = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEditTWAccess = New DevExpress.XtraEditors.CheckEdit()
        Me.TextBioSecurityDBName = New DevExpress.XtraEditors.TextEdit()
        Me.TextZKAccessDBName = New DevExpress.XtraEditors.TextEdit()
        Me.TextWDMSDBName = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEditBioSecurity = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAccess = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditWDMS = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditIDMS = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditTWCloud = New DevExpress.XtraEditors.CheckEdit()
        Me.ToggleOnlineEvents = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl58 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditTimerDur = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.PanelControlIOCL = New DevExpress.XtraEditors.PanelControl()
        Me.TextLink = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TextIoclInterval = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleIsIOCL = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TextBio1EcoPort = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextInActiveDays = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditAutoDwnDur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl60 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleRealTime = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl57 = New DevExpress.XtraEditors.LabelControl()
        Me.TextTWIR102Port = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl59 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditZKPort = New DevExpress.XtraEditors.TextEdit()
        Me.ToggleLeaveAsPerFinancialYear = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl53 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditBioPort = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblSetupBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblSetupTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblSetupTableAdapter()
        Me.TblSetup1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblSetup1TableAdapter()
        Me.TextTF01Port = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.ToggleSwitchCanteen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchIsNepali.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchVisitor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleDownloadAtStartUp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.DateEditOnline.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditOnline.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleonlineStartUp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControlCloud, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControlCloud.SuspendLayout()
        CType(Me.TextEditiTwCDB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditiDMSDB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditTWAccessDB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditTWAccess.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBioSecurityDBName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextZKAccessDBName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextWDMSDBName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditBioSecurity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAccess.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditWDMS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditIDMS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditTWCloud.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleOnlineEvents.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditTimerDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PanelControlIOCL, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControlIOCL.SuspendLayout()
        CType(Me.TextLink.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextIoclInterval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleIsIOCL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBio1EcoPort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextInActiveDays.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditAutoDwnDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleRealTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextTWIR102Port.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditZKPort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleLeaveAsPerFinancialYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBioPort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblSetupBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextTF01Port.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl4)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.SimpleButtonSave)
        Me.PanelControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl4.Location = New System.Drawing.Point(0, 515)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(1036, 53)
        Me.PanelControl4.TabIndex = 4
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(14, 12)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 1
        Me.SimpleButtonSave.Text = "Save"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.ToggleSwitchCanteen)
        Me.PanelControl3.Controls.Add(Me.ToggleSwitchIsNepali)
        Me.PanelControl3.Controls.Add(Me.LabelControl56)
        Me.PanelControl3.Controls.Add(Me.LabelControl40)
        Me.PanelControl3.Controls.Add(Me.ToggleSwitchVisitor)
        Me.PanelControl3.Controls.Add(Me.LabelControl2)
        Me.PanelControl3.Controls.Add(Me.ToggleDownloadAtStartUp)
        Me.PanelControl3.Controls.Add(Me.TextInActiveDays)
        Me.PanelControl3.Controls.Add(Me.LabelControl55)
        Me.PanelControl3.Controls.Add(Me.LabelControl1)
        Me.PanelControl3.Controls.Add(Me.LabelControl54)
        Me.PanelControl3.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl3.Location = New System.Drawing.Point(0, 399)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(1036, 116)
        Me.PanelControl3.TabIndex = 3
        '
        'ToggleSwitchCanteen
        '
        Me.ToggleSwitchCanteen.Location = New System.Drawing.Point(240, 15)
        Me.ToggleSwitchCanteen.Name = "ToggleSwitchCanteen"
        Me.ToggleSwitchCanteen.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchCanteen.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchCanteen.Properties.OffText = "No"
        Me.ToggleSwitchCanteen.Properties.OnText = "Yes"
        Me.ToggleSwitchCanteen.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchCanteen.TabIndex = 1
        '
        'ToggleSwitchIsNepali
        '
        Me.ToggleSwitchIsNepali.Location = New System.Drawing.Point(240, 71)
        Me.ToggleSwitchIsNepali.Name = "ToggleSwitchIsNepali"
        Me.ToggleSwitchIsNepali.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchIsNepali.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchIsNepali.Properties.OffText = "No"
        Me.ToggleSwitchIsNepali.Properties.OnText = "Yes"
        Me.ToggleSwitchIsNepali.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchIsNepali.TabIndex = 3
        '
        'LabelControl56
        '
        Me.LabelControl56.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl56.Appearance.Options.UseFont = True
        Me.LabelControl56.Location = New System.Drawing.Point(18, 76)
        Me.LabelControl56.Name = "LabelControl56"
        Me.LabelControl56.Size = New System.Drawing.Size(106, 14)
        Me.LabelControl56.TabIndex = 91
        Me.LabelControl56.Text = "Use Nepali Calendar"
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl40.Appearance.Options.UseFont = True
        Me.LabelControl40.Location = New System.Drawing.Point(521, 20)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(114, 14)
        Me.LabelControl40.TabIndex = 85
        Me.LabelControl40.Text = "Download at Startup"
        '
        'ToggleSwitchVisitor
        '
        Me.ToggleSwitchVisitor.Location = New System.Drawing.Point(240, 43)
        Me.ToggleSwitchVisitor.Name = "ToggleSwitchVisitor"
        Me.ToggleSwitchVisitor.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchVisitor.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchVisitor.Properties.OffText = "No"
        Me.ToggleSwitchVisitor.Properties.OnText = "Yes"
        Me.ToggleSwitchVisitor.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchVisitor.TabIndex = 2
        '
        'ToggleDownloadAtStartUp
        '
        Me.ToggleDownloadAtStartUp.Location = New System.Drawing.Point(743, 15)
        Me.ToggleDownloadAtStartUp.Name = "ToggleDownloadAtStartUp"
        Me.ToggleDownloadAtStartUp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleDownloadAtStartUp.Properties.Appearance.Options.UseFont = True
        Me.ToggleDownloadAtStartUp.Properties.OffText = "No"
        Me.ToggleDownloadAtStartUp.Properties.OnText = "Yes"
        Me.ToggleDownloadAtStartUp.Size = New System.Drawing.Size(95, 25)
        Me.ToggleDownloadAtStartUp.TabIndex = 4
        '
        'LabelControl55
        '
        Me.LabelControl55.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl55.Appearance.Options.UseFont = True
        Me.LabelControl55.Location = New System.Drawing.Point(18, 20)
        Me.LabelControl55.Name = "LabelControl55"
        Me.LabelControl55.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl55.TabIndex = 89
        Me.LabelControl55.Text = "Enable Canteen"
        '
        'LabelControl54
        '
        Me.LabelControl54.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl54.Appearance.Options.UseFont = True
        Me.LabelControl54.Location = New System.Drawing.Point(18, 48)
        Me.LabelControl54.Name = "LabelControl54"
        Me.LabelControl54.Size = New System.Drawing.Size(73, 14)
        Me.LabelControl54.TabIndex = 90
        Me.LabelControl54.Text = "Enable Visitor"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.LabelControl9)
        Me.PanelControl2.Controls.Add(Me.SimpleButton1)
        Me.PanelControl2.Controls.Add(Me.DateEditOnline)
        Me.PanelControl2.Controls.Add(Me.ToggleonlineStartUp)
        Me.PanelControl2.Controls.Add(Me.LabelControl8)
        Me.PanelControl2.Controls.Add(Me.PanelControlCloud)
        Me.PanelControl2.Controls.Add(Me.ToggleOnlineEvents)
        Me.PanelControl2.Controls.Add(Me.LabelControl39)
        Me.PanelControl2.Controls.Add(Me.LabelControl58)
        Me.PanelControl2.Controls.Add(Me.TextEditTimerDur)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl2.Location = New System.Drawing.Point(0, 200)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(1036, 199)
        Me.PanelControl2.TabIndex = 2
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Appearance.Options.UseForeColor = True
        Me.LabelControl9.Location = New System.Drawing.Point(19, 128)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(266, 28)
        Me.LabelControl9.TabIndex = 98
        Me.LabelControl9.Text = "This will reset the selected 3rd party events and " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "It will download the data fro" &
    "m selected date"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(240, 98)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(150, 23)
        Me.SimpleButton1.TabIndex = 97
        Me.SimpleButton1.Text = "Reset 3rd Party Events"
        '
        'DateEditOnline
        '
        Me.DateEditOnline.EditValue = Nothing
        Me.DateEditOnline.Location = New System.Drawing.Point(19, 101)
        Me.DateEditOnline.Name = "DateEditOnline"
        Me.DateEditOnline.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditOnline.Properties.Appearance.Options.UseFont = True
        Me.DateEditOnline.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditOnline.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditOnline.Size = New System.Drawing.Size(120, 20)
        Me.DateEditOnline.TabIndex = 4
        '
        'ToggleonlineStartUp
        '
        Me.ToggleonlineStartUp.Location = New System.Drawing.Point(240, 67)
        Me.ToggleonlineStartUp.Name = "ToggleonlineStartUp"
        Me.ToggleonlineStartUp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleonlineStartUp.Properties.Appearance.Options.UseFont = True
        Me.ToggleonlineStartUp.Properties.NullText = "N"
        Me.ToggleonlineStartUp.Properties.OffText = "No"
        Me.ToggleonlineStartUp.Properties.OnText = "Yes"
        Me.ToggleonlineStartUp.Properties.Tag = "N"
        Me.ToggleonlineStartUp.Properties.ValueOff = "N"
        Me.ToggleonlineStartUp.Properties.ValueOn = "Y"
        Me.ToggleonlineStartUp.Size = New System.Drawing.Size(95, 25)
        Me.ToggleonlineStartUp.TabIndex = 3
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(18, 72)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(157, 14)
        Me.LabelControl8.TabIndex = 94
        Me.LabelControl8.Text = "3rd Party Events On StartUp"
        '
        'PanelControlCloud
        '
        Me.PanelControlCloud.Controls.Add(Me.LabelControl7)
        Me.PanelControlCloud.Controls.Add(Me.TextEditiTwCDB)
        Me.PanelControlCloud.Controls.Add(Me.TextEditiDMSDB)
        Me.PanelControlCloud.Controls.Add(Me.TextEditTWAccessDB)
        Me.PanelControlCloud.Controls.Add(Me.CheckEditTWAccess)
        Me.PanelControlCloud.Controls.Add(Me.TextBioSecurityDBName)
        Me.PanelControlCloud.Controls.Add(Me.TextZKAccessDBName)
        Me.PanelControlCloud.Controls.Add(Me.TextWDMSDBName)
        Me.PanelControlCloud.Controls.Add(Me.CheckEditBioSecurity)
        Me.PanelControlCloud.Controls.Add(Me.CheckEditAccess)
        Me.PanelControlCloud.Controls.Add(Me.CheckEditWDMS)
        Me.PanelControlCloud.Controls.Add(Me.CheckEditIDMS)
        Me.PanelControlCloud.Controls.Add(Me.CheckEditTWCloud)
        Me.PanelControlCloud.Location = New System.Drawing.Point(513, 5)
        Me.PanelControlCloud.Name = "PanelControlCloud"
        Me.PanelControlCloud.Size = New System.Drawing.Size(381, 188)
        Me.PanelControlCloud.TabIndex = 4
        Me.PanelControlCloud.Visible = False
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(210, 10)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl7.TabIndex = 85
        Me.LabelControl7.Text = "Database Name"
        '
        'TextEditiTwCDB
        '
        Me.TextEditiTwCDB.EditValue = ""
        Me.TextEditiTwCDB.Location = New System.Drawing.Point(154, 30)
        Me.TextEditiTwCDB.Name = "TextEditiTwCDB"
        Me.TextEditiTwCDB.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditiTwCDB.Properties.Appearance.Options.UseFont = True
        Me.TextEditiTwCDB.Properties.MaxLength = 50
        Me.TextEditiTwCDB.Size = New System.Drawing.Size(211, 20)
        Me.TextEditiTwCDB.TabIndex = 2
        Me.TextEditiTwCDB.Visible = False
        '
        'TextEditiDMSDB
        '
        Me.TextEditiDMSDB.EditValue = ""
        Me.TextEditiDMSDB.Location = New System.Drawing.Point(154, 56)
        Me.TextEditiDMSDB.Name = "TextEditiDMSDB"
        Me.TextEditiDMSDB.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditiDMSDB.Properties.Appearance.Options.UseFont = True
        Me.TextEditiDMSDB.Properties.MaxLength = 50
        Me.TextEditiDMSDB.Size = New System.Drawing.Size(211, 20)
        Me.TextEditiDMSDB.TabIndex = 4
        Me.TextEditiDMSDB.Visible = False
        '
        'TextEditTWAccessDB
        '
        Me.TextEditTWAccessDB.EditValue = ""
        Me.TextEditTWAccessDB.Location = New System.Drawing.Point(154, 158)
        Me.TextEditTWAccessDB.Name = "TextEditTWAccessDB"
        Me.TextEditTWAccessDB.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditTWAccessDB.Properties.Appearance.Options.UseFont = True
        Me.TextEditTWAccessDB.Properties.MaxLength = 50
        Me.TextEditTWAccessDB.Size = New System.Drawing.Size(211, 20)
        Me.TextEditTWAccessDB.TabIndex = 12
        Me.TextEditTWAccessDB.Visible = False
        '
        'CheckEditTWAccess
        '
        Me.CheckEditTWAccess.EditValue = "N"
        Me.CheckEditTWAccess.Location = New System.Drawing.Point(11, 159)
        Me.CheckEditTWAccess.Name = "CheckEditTWAccess"
        Me.CheckEditTWAccess.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditTWAccess.Properties.Appearance.Options.UseFont = True
        Me.CheckEditTWAccess.Properties.Caption = "TW Access"
        Me.CheckEditTWAccess.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditTWAccess.Properties.NullText = "N"
        Me.CheckEditTWAccess.Properties.ValueChecked = "Y"
        Me.CheckEditTWAccess.Properties.ValueGrayed = "N"
        Me.CheckEditTWAccess.Properties.ValueUnchecked = "N"
        Me.CheckEditTWAccess.Size = New System.Drawing.Size(120, 19)
        Me.CheckEditTWAccess.TabIndex = 11
        '
        'TextBioSecurityDBName
        '
        Me.TextBioSecurityDBName.EditValue = ""
        Me.TextBioSecurityDBName.Location = New System.Drawing.Point(154, 132)
        Me.TextBioSecurityDBName.Name = "TextBioSecurityDBName"
        Me.TextBioSecurityDBName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextBioSecurityDBName.Properties.Appearance.Options.UseFont = True
        Me.TextBioSecurityDBName.Properties.MaxLength = 50
        Me.TextBioSecurityDBName.Size = New System.Drawing.Size(211, 20)
        Me.TextBioSecurityDBName.TabIndex = 10
        Me.TextBioSecurityDBName.Visible = False
        '
        'TextZKAccessDBName
        '
        Me.TextZKAccessDBName.EditValue = ""
        Me.TextZKAccessDBName.Location = New System.Drawing.Point(154, 107)
        Me.TextZKAccessDBName.Name = "TextZKAccessDBName"
        Me.TextZKAccessDBName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextZKAccessDBName.Properties.Appearance.Options.UseFont = True
        Me.TextZKAccessDBName.Properties.MaxLength = 50
        Me.TextZKAccessDBName.Size = New System.Drawing.Size(211, 20)
        Me.TextZKAccessDBName.TabIndex = 8
        Me.TextZKAccessDBName.Visible = False
        '
        'TextWDMSDBName
        '
        Me.TextWDMSDBName.EditValue = ""
        Me.TextWDMSDBName.Location = New System.Drawing.Point(154, 82)
        Me.TextWDMSDBName.Name = "TextWDMSDBName"
        Me.TextWDMSDBName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextWDMSDBName.Properties.Appearance.Options.UseFont = True
        Me.TextWDMSDBName.Properties.MaxLength = 50
        Me.TextWDMSDBName.Size = New System.Drawing.Size(211, 20)
        Me.TextWDMSDBName.TabIndex = 6
        Me.TextWDMSDBName.Visible = False
        '
        'CheckEditBioSecurity
        '
        Me.CheckEditBioSecurity.EditValue = "N"
        Me.CheckEditBioSecurity.Location = New System.Drawing.Point(11, 133)
        Me.CheckEditBioSecurity.Name = "CheckEditBioSecurity"
        Me.CheckEditBioSecurity.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditBioSecurity.Properties.Appearance.Options.UseFont = True
        Me.CheckEditBioSecurity.Properties.Caption = "ZK Bio Security"
        Me.CheckEditBioSecurity.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditBioSecurity.Properties.NullText = "N"
        Me.CheckEditBioSecurity.Properties.ValueChecked = "Y"
        Me.CheckEditBioSecurity.Properties.ValueGrayed = "N"
        Me.CheckEditBioSecurity.Properties.ValueUnchecked = "N"
        Me.CheckEditBioSecurity.Size = New System.Drawing.Size(120, 19)
        Me.CheckEditBioSecurity.TabIndex = 9
        '
        'CheckEditAccess
        '
        Me.CheckEditAccess.EditValue = "N"
        Me.CheckEditAccess.Location = New System.Drawing.Point(11, 108)
        Me.CheckEditAccess.Name = "CheckEditAccess"
        Me.CheckEditAccess.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAccess.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAccess.Properties.Caption = "ZK Access 3.5"
        Me.CheckEditAccess.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditAccess.Properties.NullText = "N"
        Me.CheckEditAccess.Properties.ValueChecked = "Y"
        Me.CheckEditAccess.Properties.ValueGrayed = "N"
        Me.CheckEditAccess.Properties.ValueUnchecked = "N"
        Me.CheckEditAccess.Size = New System.Drawing.Size(120, 19)
        Me.CheckEditAccess.TabIndex = 7
        '
        'CheckEditWDMS
        '
        Me.CheckEditWDMS.EditValue = "N"
        Me.CheckEditWDMS.Location = New System.Drawing.Point(11, 83)
        Me.CheckEditWDMS.Name = "CheckEditWDMS"
        Me.CheckEditWDMS.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditWDMS.Properties.Appearance.Options.UseFont = True
        Me.CheckEditWDMS.Properties.Caption = "ZK WDMS"
        Me.CheckEditWDMS.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditWDMS.Properties.NullText = "N"
        Me.CheckEditWDMS.Properties.ValueChecked = "Y"
        Me.CheckEditWDMS.Properties.ValueGrayed = "N"
        Me.CheckEditWDMS.Properties.ValueUnchecked = "N"
        Me.CheckEditWDMS.Size = New System.Drawing.Size(120, 19)
        Me.CheckEditWDMS.TabIndex = 5
        '
        'CheckEditIDMS
        '
        Me.CheckEditIDMS.EditValue = "N"
        Me.CheckEditIDMS.Location = New System.Drawing.Point(11, 58)
        Me.CheckEditIDMS.Name = "CheckEditIDMS"
        Me.CheckEditIDMS.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditIDMS.Properties.Appearance.Options.UseFont = True
        Me.CheckEditIDMS.Properties.Caption = "iDMS"
        Me.CheckEditIDMS.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditIDMS.Properties.NullText = "N"
        Me.CheckEditIDMS.Properties.ValueChecked = "Y"
        Me.CheckEditIDMS.Properties.ValueGrayed = "N"
        Me.CheckEditIDMS.Properties.ValueUnchecked = "N"
        Me.CheckEditIDMS.Size = New System.Drawing.Size(120, 19)
        Me.CheckEditIDMS.TabIndex = 3
        '
        'CheckEditTWCloud
        '
        Me.CheckEditTWCloud.EditValue = "N"
        Me.CheckEditTWCloud.Location = New System.Drawing.Point(11, 33)
        Me.CheckEditTWCloud.Name = "CheckEditTWCloud"
        Me.CheckEditTWCloud.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditTWCloud.Properties.Appearance.Options.UseFont = True
        Me.CheckEditTWCloud.Properties.Caption = "TW Cloud"
        Me.CheckEditTWCloud.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditTWCloud.Properties.NullText = "N"
        Me.CheckEditTWCloud.Properties.ValueChecked = "Y"
        Me.CheckEditTWCloud.Properties.ValueGrayed = "N"
        Me.CheckEditTWCloud.Properties.ValueUnchecked = "N"
        Me.CheckEditTWCloud.Size = New System.Drawing.Size(120, 19)
        Me.CheckEditTWCloud.TabIndex = 1
        '
        'ToggleOnlineEvents
        '
        Me.ToggleOnlineEvents.Location = New System.Drawing.Point(240, 17)
        Me.ToggleOnlineEvents.Name = "ToggleOnlineEvents"
        Me.ToggleOnlineEvents.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleOnlineEvents.Properties.Appearance.Options.UseFont = True
        Me.ToggleOnlineEvents.Properties.OffText = "No"
        Me.ToggleOnlineEvents.Properties.OnText = "Yes"
        Me.ToggleOnlineEvents.Size = New System.Drawing.Size(95, 25)
        Me.ToggleOnlineEvents.TabIndex = 1
        '
        'LabelControl39
        '
        Me.LabelControl39.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl39.Appearance.Options.UseFont = True
        Me.LabelControl39.Location = New System.Drawing.Point(18, 22)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl39.TabIndex = 84
        Me.LabelControl39.Text = "3rd Party Events"
        '
        'LabelControl58
        '
        Me.LabelControl58.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl58.Appearance.Options.UseFont = True
        Me.LabelControl58.Location = New System.Drawing.Point(19, 47)
        Me.LabelControl58.Name = "LabelControl58"
        Me.LabelControl58.Size = New System.Drawing.Size(182, 14)
        Me.LabelControl58.TabIndex = 92
        Me.LabelControl58.Text = "3rd Party Events Timer (Minutes)"
        '
        'TextEditTimerDur
        '
        Me.TextEditTimerDur.EditValue = "10"
        Me.TextEditTimerDur.Location = New System.Drawing.Point(240, 44)
        Me.TextEditTimerDur.Name = "TextEditTimerDur"
        Me.TextEditTimerDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditTimerDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditTimerDur.Properties.Appearance.Options.UseFont = True
        Me.TextEditTimerDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditTimerDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditTimerDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditTimerDur.Properties.MaxLength = 4
        Me.TextEditTimerDur.Size = New System.Drawing.Size(72, 20)
        Me.TextEditTimerDur.TabIndex = 2
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.TextTF01Port)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.PanelControlIOCL)
        Me.PanelControl1.Controls.Add(Me.ToggleIsIOCL)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.TextBio1EcoPort)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.TextEditAutoDwnDur)
        Me.PanelControl1.Controls.Add(Me.LabelControl60)
        Me.PanelControl1.Controls.Add(Me.ToggleRealTime)
        Me.PanelControl1.Controls.Add(Me.LabelControl57)
        Me.PanelControl1.Controls.Add(Me.TextTWIR102Port)
        Me.PanelControl1.Controls.Add(Me.LabelControl59)
        Me.PanelControl1.Controls.Add(Me.TextEditZKPort)
        Me.PanelControl1.Controls.Add(Me.ToggleLeaveAsPerFinancialYear)
        Me.PanelControl1.Controls.Add(Me.LabelControl41)
        Me.PanelControl1.Controls.Add(Me.LabelControl53)
        Me.PanelControl1.Controls.Add(Me.TextEditBioPort)
        Me.PanelControl1.Controls.Add(Me.LabelControl30)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1036, 200)
        Me.PanelControl1.TabIndex = 1
        '
        'PanelControlIOCL
        '
        Me.PanelControlIOCL.Controls.Add(Me.TextLink)
        Me.PanelControlIOCL.Controls.Add(Me.LabelControl6)
        Me.PanelControlIOCL.Controls.Add(Me.TextIoclInterval)
        Me.PanelControlIOCL.Controls.Add(Me.LabelControl5)
        Me.PanelControlIOCL.Location = New System.Drawing.Point(513, 69)
        Me.PanelControlIOCL.Name = "PanelControlIOCL"
        Me.PanelControlIOCL.Size = New System.Drawing.Size(381, 121)
        Me.PanelControlIOCL.TabIndex = 105
        '
        'TextLink
        '
        Me.TextLink.Location = New System.Drawing.Point(114, 38)
        Me.TextLink.Name = "TextLink"
        Me.TextLink.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextLink.Properties.Appearance.Options.UseFont = True
        Me.TextLink.Size = New System.Drawing.Size(251, 69)
        Me.TextLink.TabIndex = 27
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(17, 41)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl6.TabIndex = 8
        Me.LabelControl6.Text = "Upload Link"
        '
        'TextIoclInterval
        '
        Me.TextIoclInterval.EditValue = "5"
        Me.TextIoclInterval.Location = New System.Drawing.Point(222, 12)
        Me.TextIoclInterval.Name = "TextIoclInterval"
        Me.TextIoclInterval.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextIoclInterval.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextIoclInterval.Properties.Appearance.Options.UseFont = True
        Me.TextIoclInterval.Properties.Mask.EditMask = "[0-9]*"
        Me.TextIoclInterval.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextIoclInterval.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextIoclInterval.Properties.MaxLength = 4
        Me.TextIoclInterval.Size = New System.Drawing.Size(72, 20)
        Me.TextIoclInterval.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(17, 15)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(138, 14)
        Me.LabelControl5.TabIndex = 6
        Me.LabelControl5.Text = "Upload Interval (Minutes)"
        '
        'ToggleIsIOCL
        '
        Me.ToggleIsIOCL.Location = New System.Drawing.Point(743, 38)
        Me.ToggleIsIOCL.Name = "ToggleIsIOCL"
        Me.ToggleIsIOCL.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleIsIOCL.Properties.Appearance.Options.UseFont = True
        Me.ToggleIsIOCL.Properties.OffText = "No"
        Me.ToggleIsIOCL.Properties.OnText = "Yes"
        Me.ToggleIsIOCL.Size = New System.Drawing.Size(95, 25)
        Me.ToggleIsIOCL.TabIndex = 103
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(521, 43)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(39, 14)
        Me.LabelControl4.TabIndex = 104
        Me.LabelControl4.Text = "Is IOCL"
        '
        'TextBio1EcoPort
        '
        Me.TextBio1EcoPort.EditValue = "9000"
        Me.TextBio1EcoPort.Location = New System.Drawing.Point(240, 148)
        Me.TextBio1EcoPort.Name = "TextBio1EcoPort"
        Me.TextBio1EcoPort.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextBio1EcoPort.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextBio1EcoPort.Properties.Appearance.Options.UseFont = True
        Me.TextBio1EcoPort.Properties.Mask.EditMask = "[0-9]*"
        Me.TextBio1EcoPort.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextBio1EcoPort.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextBio1EcoPort.Properties.MaxLength = 5
        Me.TextBio1EcoPort.Size = New System.Drawing.Size(72, 20)
        Me.TextBio1EcoPort.TabIndex = 6
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(17, 151)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(185, 14)
        Me.LabelControl3.TabIndex = 102
        Me.LabelControl3.Text = "Bio1Eco Real Time Download Port"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(831, 48)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(134, 14)
        Me.LabelControl2.TabIndex = 100
        Me.LabelControl2.Text = "Days continuous absent "
        '
        'TextInActiveDays
        '
        Me.TextInActiveDays.EditValue = "0"
        Me.TextInActiveDays.Location = New System.Drawing.Point(744, 45)
        Me.TextInActiveDays.Name = "TextInActiveDays"
        Me.TextInActiveDays.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextInActiveDays.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextInActiveDays.Properties.Appearance.Options.UseFont = True
        Me.TextInActiveDays.Properties.Mask.EditMask = "[0-9]*"
        Me.TextInActiveDays.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextInActiveDays.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextInActiveDays.Properties.MaxLength = 3
        Me.TextInActiveDays.Size = New System.Drawing.Size(72, 20)
        Me.TextInActiveDays.TabIndex = 7
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(521, 48)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(167, 14)
        Me.LabelControl1.TabIndex = 99
        Me.LabelControl1.Text = "Employee Mark Inactive  after "
        '
        'TextEditAutoDwnDur
        '
        Me.TextEditAutoDwnDur.EditValue = "240"
        Me.TextEditAutoDwnDur.Location = New System.Drawing.Point(240, 44)
        Me.TextEditAutoDwnDur.Name = "TextEditAutoDwnDur"
        Me.TextEditAutoDwnDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditAutoDwnDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditAutoDwnDur.Properties.Appearance.Options.UseFont = True
        Me.TextEditAutoDwnDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditAutoDwnDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditAutoDwnDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditAutoDwnDur.Properties.MaxLength = 3
        Me.TextEditAutoDwnDur.Size = New System.Drawing.Size(72, 20)
        Me.TextEditAutoDwnDur.TabIndex = 2
        '
        'LabelControl60
        '
        Me.LabelControl60.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl60.Appearance.Options.UseFont = True
        Me.LabelControl60.Location = New System.Drawing.Point(18, 47)
        Me.LabelControl60.Name = "LabelControl60"
        Me.LabelControl60.Size = New System.Drawing.Size(191, 14)
        Me.LabelControl60.TabIndex = 97
        Me.LabelControl60.Text = "Auto Download Duration (Minutes)"
        '
        'ToggleRealTime
        '
        Me.ToggleRealTime.Location = New System.Drawing.Point(240, 16)
        Me.ToggleRealTime.Name = "ToggleRealTime"
        Me.ToggleRealTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleRealTime.Properties.Appearance.Options.UseFont = True
        Me.ToggleRealTime.Properties.OffText = "No"
        Me.ToggleRealTime.Properties.OnText = "Yes"
        Me.ToggleRealTime.Size = New System.Drawing.Size(95, 25)
        Me.ToggleRealTime.TabIndex = 1
        '
        'LabelControl57
        '
        Me.LabelControl57.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl57.Appearance.Options.UseFont = True
        Me.LabelControl57.Location = New System.Drawing.Point(18, 21)
        Me.LabelControl57.Name = "LabelControl57"
        Me.LabelControl57.Size = New System.Drawing.Size(150, 14)
        Me.LabelControl57.TabIndex = 96
        Me.LabelControl57.Text = "Start Real Time On StartUp"
        '
        'TextTWIR102Port
        '
        Me.TextTWIR102Port.EditValue = "15001"
        Me.TextTWIR102Port.Location = New System.Drawing.Point(240, 122)
        Me.TextTWIR102Port.Name = "TextTWIR102Port"
        Me.TextTWIR102Port.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextTWIR102Port.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextTWIR102Port.Properties.Appearance.Options.UseFont = True
        Me.TextTWIR102Port.Properties.Mask.EditMask = "[0-9]*"
        Me.TextTWIR102Port.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextTWIR102Port.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextTWIR102Port.Properties.MaxLength = 5
        Me.TextTWIR102Port.Size = New System.Drawing.Size(72, 20)
        Me.TextTWIR102Port.TabIndex = 5
        '
        'LabelControl59
        '
        Me.LabelControl59.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl59.Appearance.Options.UseFont = True
        Me.LabelControl59.Location = New System.Drawing.Point(17, 125)
        Me.LabelControl59.Name = "LabelControl59"
        Me.LabelControl59.Size = New System.Drawing.Size(194, 14)
        Me.LabelControl59.TabIndex = 93
        Me.LabelControl59.Text = "TWIR102 Real Time Download Port"
        '
        'TextEditZKPort
        '
        Me.TextEditZKPort.EditValue = "8081"
        Me.TextEditZKPort.Location = New System.Drawing.Point(240, 96)
        Me.TextEditZKPort.Name = "TextEditZKPort"
        Me.TextEditZKPort.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditZKPort.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditZKPort.Properties.Appearance.Options.UseFont = True
        Me.TextEditZKPort.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditZKPort.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditZKPort.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditZKPort.Properties.MaxLength = 4
        Me.TextEditZKPort.Size = New System.Drawing.Size(72, 20)
        Me.TextEditZKPort.TabIndex = 4
        '
        'ToggleLeaveAsPerFinancialYear
        '
        Me.ToggleLeaveAsPerFinancialYear.Location = New System.Drawing.Point(743, 10)
        Me.ToggleLeaveAsPerFinancialYear.Name = "ToggleLeaveAsPerFinancialYear"
        Me.ToggleLeaveAsPerFinancialYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleLeaveAsPerFinancialYear.Properties.Appearance.Options.UseFont = True
        Me.ToggleLeaveAsPerFinancialYear.Properties.OffText = "No"
        Me.ToggleLeaveAsPerFinancialYear.Properties.OnText = "Yes"
        Me.ToggleLeaveAsPerFinancialYear.Size = New System.Drawing.Size(95, 25)
        Me.ToggleLeaveAsPerFinancialYear.TabIndex = 77
        '
        'LabelControl41
        '
        Me.LabelControl41.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl41.Appearance.Options.UseFont = True
        Me.LabelControl41.Location = New System.Drawing.Point(521, 15)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(146, 14)
        Me.LabelControl41.TabIndex = 86
        Me.LabelControl41.Text = "Leave as per Financial Year"
        '
        'LabelControl53
        '
        Me.LabelControl53.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl53.Appearance.Options.UseFont = True
        Me.LabelControl53.Location = New System.Drawing.Point(17, 99)
        Me.LabelControl53.Name = "LabelControl53"
        Me.LabelControl53.Size = New System.Drawing.Size(199, 14)
        Me.LabelControl53.TabIndex = 88
        Me.LabelControl53.Text = "ZK/Bio Pro Real Time Download Port"
        '
        'TextEditBioPort
        '
        Me.TextEditBioPort.EditValue = "7005"
        Me.TextEditBioPort.Location = New System.Drawing.Point(240, 70)
        Me.TextEditBioPort.Name = "TextEditBioPort"
        Me.TextEditBioPort.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditBioPort.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBioPort.Properties.Appearance.Options.UseFont = True
        Me.TextEditBioPort.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditBioPort.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditBioPort.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditBioPort.Properties.MaxLength = 4
        Me.TextEditBioPort.Size = New System.Drawing.Size(72, 20)
        Me.TextEditBioPort.TabIndex = 3
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl30.Appearance.Options.UseFont = True
        Me.LabelControl30.Location = New System.Drawing.Point(18, 73)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(212, 14)
        Me.LabelControl30.TabIndex = 87
        Me.LabelControl30.Text = "Bio Series/F9 Real Time Download Port"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(104, 568)
        Me.MemoEdit1.TabIndex = 1
        '
        'TblSetupBindingSource
        '
        Me.TblSetupBindingSource.DataMember = "tblSetup"
        Me.TblSetupBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblSetupTableAdapter
        '
        Me.TblSetupTableAdapter.ClearBeforeFill = True
        '
        'TblSetup1TableAdapter1
        '
        Me.TblSetup1TableAdapter1.ClearBeforeFill = True
        '
        'TextTF01Port
        '
        Me.TextTF01Port.EditValue = "7788"
        Me.TextTF01Port.Location = New System.Drawing.Point(239, 173)
        Me.TextTF01Port.Name = "TextTF01Port"
        Me.TextTF01Port.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextTF01Port.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextTF01Port.Properties.Appearance.Options.UseFont = True
        Me.TextTF01Port.Properties.Mask.EditMask = "[0-9]*"
        Me.TextTF01Port.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextTF01Port.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextTF01Port.Properties.MaxLength = 5
        Me.TextTF01Port.Size = New System.Drawing.Size(72, 20)
        Me.TextTF01Port.TabIndex = 106
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(16, 176)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(174, 14)
        Me.LabelControl10.TabIndex = 107
        Me.LabelControl10.Text = "TF-01 Real Time Download Port"
        '
        'XtraTimeOfficePolicyNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "XtraTimeOfficePolicyNew"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.ToggleSwitchCanteen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchIsNepali.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchVisitor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleDownloadAtStartUp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.DateEditOnline.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditOnline.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleonlineStartUp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControlCloud, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControlCloud.ResumeLayout(False)
        Me.PanelControlCloud.PerformLayout()
        CType(Me.TextEditiTwCDB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditiDMSDB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditTWAccessDB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditTWAccess.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBioSecurityDBName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextZKAccessDBName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextWDMSDBName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditBioSecurity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAccess.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditWDMS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditIDMS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditTWCloud.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleOnlineEvents.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditTimerDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.PanelControlIOCL, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControlIOCL.ResumeLayout(False)
        Me.PanelControlIOCL.PerformLayout()
        CType(Me.TextLink.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextIoclInterval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleIsIOCL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBio1EcoPort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextInActiveDays.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditAutoDwnDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleRealTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextTWIR102Port.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditZKPort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleLeaveAsPerFinancialYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBioPort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblSetupBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextTF01Port.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TblSetupBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblSetupTableAdapter As iAS.SSSDBDataSetTableAdapters.tblSetupTableAdapter
    Friend WithEvents TblSetup1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblSetup1TableAdapter
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TextTWIR102Port As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl59 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditTimerDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl58 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchIsNepali As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl56 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchVisitor As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleSwitchCanteen As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl54 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl55 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditZKPort As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl53 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditBioPort As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleLeaveAsPerFinancialYear As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleOnlineEvents As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TextEditAutoDwnDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl60 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleRealTime As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl57 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextInActiveDays As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextBio1EcoPort As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleIsIOCL As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControlIOCL As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TextIoclInterval As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextLink As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents PanelControlCloud As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckEditWDMS As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditIDMS As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditTWCloud As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditBioSecurity As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAccess As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextWDMSDBName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextBioSecurityDBName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextZKAccessDBName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditTWAccessDB As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEditTWAccess As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEditiDMSDB As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleDownloadAtStartUp As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents TextEditiTwCDB As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleonlineStartUp As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEditOnline As DevExpress.XtraEditors.DateEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextTF01Port As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
End Class
