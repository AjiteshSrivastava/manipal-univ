﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraRealTimePunches
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraRealTimePunches))
        Me.lvwLogList = New System.Windows.Forms.ListView()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonClear = New DevExpress.XtraEditors.SimpleButton()
        Me.lblTotal = New DevExpress.XtraEditors.LabelControl()
        Me.AxRealSvrOcxTcp1 = New AxRealSvrOcxTcpLib.AxRealSvrOcxTcp()
        Me.SidePanel3 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.AxFPCLOCK_Svr1 = New AxFPCLOCK_SVRLib.AxFPCLOCK_Svr()
        Me.PictureEdit = New DevExpress.XtraEditors.PictureEdit()
        Me.TimerLEDStop = New System.Windows.Forms.Timer(Me.components)
        Me.TimerBioEco = New System.Windows.Forms.Timer(Me.components)
        Me.tcpServer1 = New tcpServer.TcpServer(Me.components)
        Me.TimerZK = New System.Windows.Forms.Timer(Me.components)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel2.SuspendLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxRealSvrOcxTcp1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel3.SuspendLayout()
        Me.SidePanel1.SuspendLayout()
        CType(Me.AxFPCLOCK_Svr1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lvwLogList
        '
        Me.lvwLogList.HideSelection = False
        Me.lvwLogList.Location = New System.Drawing.Point(225, 0)
        Me.lvwLogList.Name = "lvwLogList"
        Me.lvwLogList.Size = New System.Drawing.Size(284, 229)
        Me.lvwLogList.TabIndex = 5
        Me.lvwLogList.UseCompatibleStateImageBehavior = False
        Me.lvwLogList.View = System.Windows.Forms.View.Details
        Me.lvwLogList.Visible = False
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(653, 302)
        Me.GridControl1.TabIndex = 2
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        '
        'SidePanel2
        '
        Me.SidePanel2.Controls.Add(Me.TextEdit1)
        Me.SidePanel2.Controls.Add(Me.LabelControl1)
        Me.SidePanel2.Controls.Add(Me.SimpleButtonClear)
        Me.SidePanel2.Controls.Add(Me.lblTotal)
        Me.SidePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel2.Location = New System.Drawing.Point(0, 302)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(984, 59)
        Me.SidePanel2.TabIndex = 1
        Me.SidePanel2.Text = "SidePanel2"
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = ""
        Me.TextEdit1.Location = New System.Drawing.Point(323, 27)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Size = New System.Drawing.Size(601, 20)
        Me.TextEdit1.TabIndex = 38
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(478, 21)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(0, 16)
        Me.LabelControl1.TabIndex = 22
        '
        'SimpleButtonClear
        '
        Me.SimpleButtonClear.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.SimpleButtonClear.Appearance.Options.UseFont = True
        Me.SimpleButtonClear.Location = New System.Drawing.Point(12, 15)
        Me.SimpleButtonClear.Name = "SimpleButtonClear"
        Me.SimpleButtonClear.Size = New System.Drawing.Size(101, 23)
        Me.SimpleButtonClear.TabIndex = 20
        Me.SimpleButtonClear.Text = "Clear"
        '
        'lblTotal
        '
        Me.lblTotal.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.lblTotal.Appearance.Options.UseFont = True
        Me.lblTotal.Location = New System.Drawing.Point(165, 20)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(29, 16)
        Me.lblTotal.TabIndex = 21
        Me.lblTotal.Text = "Total"
        '
        'AxRealSvrOcxTcp1
        '
        Me.AxRealSvrOcxTcp1.Enabled = True
        Me.AxRealSvrOcxTcp1.Location = New System.Drawing.Point(168, 253)
        Me.AxRealSvrOcxTcp1.Name = "AxRealSvrOcxTcp1"
        Me.AxRealSvrOcxTcp1.OcxState = CType(resources.GetObject("AxRealSvrOcxTcp1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxRealSvrOcxTcp1.Size = New System.Drawing.Size(32, 32)
        Me.AxRealSvrOcxTcp1.TabIndex = 19
        Me.AxRealSvrOcxTcp1.Visible = False
        '
        'SidePanel3
        '
        Me.SidePanel3.Controls.Add(Me.GridControl1)
        Me.SidePanel3.Controls.Add(Me.lvwLogList)
        Me.SidePanel3.Dock = System.Windows.Forms.DockStyle.Left
        Me.SidePanel3.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel3.Name = "SidePanel3"
        Me.SidePanel3.Size = New System.Drawing.Size(654, 302)
        Me.SidePanel3.TabIndex = 2
        Me.SidePanel3.Text = "SidePanel3"
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.AxFPCLOCK_Svr1)
        Me.SidePanel1.Controls.Add(Me.PictureEdit)
        Me.SidePanel1.Controls.Add(Me.AxRealSvrOcxTcp1)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanel1.Location = New System.Drawing.Point(654, 0)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(330, 302)
        Me.SidePanel1.TabIndex = 3
        Me.SidePanel1.Text = "SidePanel1"
        '
        'AxFPCLOCK_Svr1
        '
        Me.AxFPCLOCK_Svr1.Enabled = True
        Me.AxFPCLOCK_Svr1.Location = New System.Drawing.Point(54, 235)
        Me.AxFPCLOCK_Svr1.Name = "AxFPCLOCK_Svr1"
        Me.AxFPCLOCK_Svr1.OcxState = CType(resources.GetObject("AxFPCLOCK_Svr1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxFPCLOCK_Svr1.Size = New System.Drawing.Size(100, 50)
        Me.AxFPCLOCK_Svr1.TabIndex = 38
        Me.AxFPCLOCK_Svr1.Visible = False
        '
        'PictureEdit
        '
        Me.PictureEdit.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit.EditValue = "<Null>"
        Me.PictureEdit.Location = New System.Drawing.Point(0, 0)
        Me.PictureEdit.Name = "PictureEdit"
        Me.PictureEdit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PictureEdit.Properties.Appearance.Options.UseFont = True
        Me.PictureEdit.Properties.LookAndFeel.SkinName = "iMaginary"
        Me.PictureEdit.Properties.LookAndFeel.UseDefaultLookAndFeel = False
        Me.PictureEdit.Properties.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.ByteArray
        Me.PictureEdit.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Always
        Me.PictureEdit.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.PictureEdit.Size = New System.Drawing.Size(250, 200)
        Me.PictureEdit.TabIndex = 37
        '
        'TimerLEDStop
        '
        Me.TimerLEDStop.Interval = 20000
        '
        'TimerBioEco
        '
        Me.TimerBioEco.Interval = 2500
        '
        'tcpServer1
        '
        Me.tcpServer1.Encoding = CType(resources.GetObject("tcpServer1.Encoding"), System.Text.Encoding)
        Me.tcpServer1.IdleTime = 50
        Me.tcpServer1.IsOpen = False
        Me.tcpServer1.MaxCallbackThreads = 1000
        Me.tcpServer1.MaxSendAttempts = 3
        Me.tcpServer1.Port = 6666
        Me.tcpServer1.VerifyConnectionInterval = 0
        '
        'TimerZK
        '
        Me.TimerZK.Interval = 5000
        '
        'XtraRealTimePunches
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 361)
        Me.Controls.Add(Me.SidePanel1)
        Me.Controls.Add(Me.SidePanel3)
        Me.Controls.Add(Me.SidePanel2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraRealTimePunches"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Real Time Monitor"
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel2.ResumeLayout(False)
        Me.SidePanel2.PerformLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxRealSvrOcxTcp1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel3.ResumeLayout(False)
        Me.SidePanel1.ResumeLayout(False)
        CType(Me.AxFPCLOCK_Svr1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents AxRealSvrOcxTcp1 As AxRealSvrOcxTcpLib.AxRealSvrOcxTcp
    Friend WithEvents SimpleButtonClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblTotal As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents lvwLogList As System.Windows.Forms.ListView
    Friend WithEvents SidePanel3 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents PictureEdit As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents TimerLEDStop As System.Windows.Forms.Timer
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TimerBioEco As System.Windows.Forms.Timer
    Private WithEvents tcpServer1 As tcpServer.TcpServer
    Friend WithEvents AxFPCLOCK_Svr1 As AxFPCLOCK_SVRLib.AxFPCLOCK_Svr
    Friend WithEvents TimerZK As Timer
End Class
