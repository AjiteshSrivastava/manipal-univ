﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraGrid

Public Class XtraTimeOfficePolicyNew
    Dim ulf As UserLookAndFeel
    Public Sub New()
        InitializeComponent()      
    End Sub
    Private Sub XtraTimeOfficePolicy_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Me.Width = My.Computer.Screen.WorkingArea.Width
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100

        'not to do in all pages
        Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        Common.SplitterPosition = SplitContainerControl1.SplitterPosition

        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        DateEditOnline.EditValue = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) ' Now.ToString("01/MM/yyyy")

        setDefault()
    End Sub
    Private Sub setDefault()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        'Dim sSql As String = "select * from tblSetup where SETUPID= (select MAX(SETUPID) from tblSetup)"
        Dim sSql As String = "Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblSetup )"
        If Common.servername = "Access" Then
            sSql = "Select * from tblSetUp where setupid =(Select MAX(val(Setupid)) from tblSetup )"
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If

        If ds.Tables(0).Rows(0).Item("LeaveFinancialYear").ToString.Trim = "Y" Then
            ToggleLeaveAsPerFinancialYear.IsOn = True
        Else
            ToggleLeaveAsPerFinancialYear.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("Online").ToString.Trim = "Y" Then
            ToggleOnlineEvents.IsOn = True
            PanelControlCloud.Visible = True
        Else
            ToggleOnlineEvents.IsOn = False
            PanelControlCloud.Visible = False
        End If

        If ds.Tables(0).Rows(0).Item("AutoDownload").ToString.Trim = "Y" Then
            ToggleDownloadAtStartUp.IsOn = True
        Else
            ToggleDownloadAtStartUp.IsOn = False
        End If

        'CreateEmpPunchDwnld
        If ds.Tables(0).Rows(0).Item("StartRealTime").ToString.Trim = "Y" Then
            ToggleRealTime.IsOn = True
        Else
            ToggleRealTime.IsOn = False
        End If

        TextEditBioPort.Text = ds.Tables(0).Rows(0).Item("BioPort").ToString.Trim
        TextEditZKPort.Text = ds.Tables(0).Rows(0).Item("ZKPort").ToString.Trim
        TextTWIR102Port.Text = ds.Tables(0).Rows(0).Item("TWIR102Port").ToString.Trim
        TextEditTimerDur.Text = ds.Tables(0).Rows(0).Item("TimerDur").ToString.Trim
        If ds.Tables(0).Rows(0).Item("Canteen").ToString.Trim = "Y" Then
            ToggleSwitchCanteen.IsOn = True
        ElseIf ds.Tables(0).Rows(0).Item("Canteen").ToString.Trim = "N" Then
            ToggleSwitchCanteen.IsOn = False
        End If
        If ds.Tables(0).Rows(0).Item("Visitor").ToString.Trim = "Y" Then
            ToggleSwitchVisitor.IsOn = True
        ElseIf ds.Tables(0).Rows(0).Item("Visitor").ToString.Trim = "N" Then
            ToggleSwitchVisitor.IsOn = False
        End If

        If ds.Tables(0).Rows(0).Item("IsNepali").ToString.Trim = "Y" Then
            ToggleSwitchIsNepali.IsOn = True
        ElseIf ds.Tables(0).Rows(0).Item("IsNepali").ToString.Trim = "N" Then
            ToggleSwitchIsNepali.IsOn = False
        End If
        TextEditAutoDwnDur.Text = ds.Tables(0).Rows(0).Item("AutoDwnDur").ToString.Trim
        TextInActiveDays.Text = ds.Tables(0).Rows(0).Item("MarkInactivDurDay").ToString.Trim
        TextBio1EcoPort.Text = ds.Tables(0).Rows(0).Item("Bio1EcoPort").ToString.Trim
        TextTF01Port.Text = ds.Tables(0).Rows(0).Item("TF01Port").ToString.Trim

        If ds.Tables(0).Rows(0).Item("IsIOCL").ToString.Trim = "Y" Then
            PanelControlIOCL.Visible = True
            ToggleIsIOCL.IsOn = True
            TextIoclInterval.Text = ds.Tables(0).Rows(0).Item("IOCLInterval").ToString.Trim ' (ds.Tables(0).Rows(0).Item("IOCLInterval").ToString.Trim \ 60) & ":" & ds.Tables(0).Rows(0).Item("IOCLInterval").ToString.Trim Mod 60
            TextLink.Text = ds.Tables(0).Rows(0).Item("IOCLLink").ToString.Trim
        Else 'If ds.Tables(0).Rows(0).Item("IsIOCL").ToString.Trim = "N" Then
            ToggleIsIOCL.IsOn = False
            PanelControlIOCL.Visible = False

        End If

        CheckEditTWCloud.EditValue = ds.Tables(0).Rows(0).Item("TWCloud").ToString.Trim
        CheckEditWDMS.EditValue = ds.Tables(0).Rows(0).Item("IsWDMS").ToString.Trim
        CheckEditIDMS.EditValue = ds.Tables(0).Rows(0).Item("iDMS").ToString.Trim
        TextEditiDMSDB.Text = ds.Tables(0).Rows(0).Item("iDMSDB").ToString.Trim

        TextWDMSDBName.Text = ds.Tables(0).Rows(0).Item("WDMSDBName").ToString.Trim
        CheckEditBioSecurity.EditValue = ds.Tables(0).Rows(0).Item("IsBioSecurity").ToString.Trim
        TextZKAccessDBName.Text = ds.Tables(0).Rows(0).Item("ZKAccessDBName").ToString.Trim
        CheckEditAccess.EditValue = ds.Tables(0).Rows(0).Item("IsZKAccess").ToString.Trim
        TextBioSecurityDBName.Text = ds.Tables(0).Rows(0).Item("BioSecurityDBName").ToString.Trim

        CheckEditTWAccess.EditValue = ds.Tables(0).Rows(0).Item("IsTWAccess").ToString.Trim
        TextEditTWAccessDB.Text = ds.Tables(0).Rows(0).Item("TWAccessDBName").ToString.Trim

        TextEditiTwCDB.Text = ds.Tables(0).Rows(0).Item("TWCloudDB").ToString.Trim
        If Common.servername = "Access" Then
            PanelControl2.Visible = False
        End If

        ToggleonlineStartUp.EditValue = ds.Tables(0).Rows(0).Item("OnlineStartUp").ToString.Trim
        'ToggleCompl.EditValue = ds.Tables(0).Rows(0).Item("IsCompliance").ToString.Trim
    End Sub
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        Me.Cursor = Cursors.WaitCursor
        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand

        Dim StartRealTime As String
        If ToggleRealTime.IsOn = True Then
            StartRealTime = "Y"
        Else
            StartRealTime = "N"
        End If

        Dim LeaveFinancialYear As String
        If ToggleLeaveAsPerFinancialYear.IsOn = True Then
            LeaveFinancialYear = "Y"
        Else
            LeaveFinancialYear = "N"
        End If
        Dim Online As String
        If ToggleOnlineEvents.IsOn = True Then
            If TextEditTimerDur.Text.Trim = "" Or TextEditTimerDur.Text.Trim = "0" Then
                XtraMessageBox.Show(ulf, "<size=10>3rd Party Events Timer Cannot be empty or 0</size>", "<size=9>Error</size>")
                TextEditTimerDur.Select()
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            Online = "Y"
        Else
            Online = "N"
        End If
        Dim AutoDownload As String
        If ToggleDownloadAtStartUp.IsOn = True Then
            AutoDownload = "Y"
        Else
            AutoDownload = "N"
        End If

        Dim BioPort As String = TextEditBioPort.Text.Trim
        Dim ZKPort As String = TextEditZKPort.Text.Trim
        Dim TWIR102Port As String = TextTWIR102Port.Text.Trim
        Dim TF01Port As String = TextTF01Port.Text.Trim
        Dim Canteen As String
        Dim Visitor As String
        If ToggleSwitchCanteen.IsOn = True Then
            Canteen = "Y"
        Else
            Canteen = "N"
        End If
        If ToggleSwitchVisitor.IsOn = True Then
            Visitor = "Y"
        Else
            Visitor = "N"
        End If
        Dim IsNepali As String = "N"
        If ToggleSwitchIsNepali.IsOn = True Then
            IsNepali = "Y"
        Else
            IsNepali = "N"
        End If
        Dim TimerDur As String = TextEditTimerDur.Text.Trim
        If TextEditTimerDur.Text.Trim = "" Then
            TimerDur = 10
        End If
        Dim AutoDwnDur As String = TextEditAutoDwnDur.Text.Trim
        If AutoDwnDur = "" Then
            AutoDwnDur = "0"
        End If
        Dim MarkInactivDurDay As String = TextInActiveDays.Text.Trim
        Dim Bio1EcoPort As String = TextBio1EcoPort.Text.Trim

        Dim IsIOCL As String = "N"
        Dim IOCLInterval As Integer = 0
        Dim IOCLLink As String = ""
        If ToggleIsIOCL.IsOn = True Then
            'Dim preMarkDur() As String = TextIoclInterval.Text.Trim.Split(":")
            If TextIoclInterval.Text.Trim = "" Or TextIoclInterval.Text.Trim = "0" Then
                XtraMessageBox.Show(ulf, "<size=10>Upload Interval Cannot be empty or 0</size>", "<size=9>Error</size>")
                TextIoclInterval.Select()
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            IOCLInterval = TextIoclInterval.Text.Trim 'preMarkDur(0) * 60 + preMarkDur(1)
            IsIOCL = "Y"
            If TextLink.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Upload Link Cannot be empty</size>", "<size=9>Error</size>")
                TextLink.Select()
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            IOCLLink = TextLink.Text.Trim
        End If
        'Dim IsCompliance As String
        'If ToggleCompl.IsOn = True Then
        '    IsCompliance = "Y"
        'Else
        '    IsCompliance = "N"
        'End If

        Dim TWCloud As String = CheckEditTWCloud.EditValue
        Dim IsWDMS As String = CheckEditWDMS.EditValue
        Dim iDMS As String = CheckEditIDMS.EditValue
        Dim iDMSDB As String = TextEditiDMSDB.Text.Trim
        Dim TWCloudDB As String = TextEditiTwCDB.Text.Trim

        Dim WDMSDBName As String = TextWDMSDBName.Text.Trim
        Dim IsBioSecurity As String = CheckEditBioSecurity.EditValue
        Dim BioSecurityDBName As String = TextBioSecurityDBName.Text.Trim
        Dim IsZKAccess As String = CheckEditAccess.EditValue
        Dim ZKAccessDBName As String = TextZKAccessDBName.Text.Trim

        Dim IsTWAccess As String = CheckEditTWAccess.EditValue
        Dim TWAccessDBName As String = TextEditTWAccessDB.Text.Trim

        Dim OnlineStartUp As String = ToggleonlineStartUp.EditValue

        If CheckEditTWCloud.Checked = True And TextEditiTwCDB.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>TW Cloud DataBase Name Cannot be empty</size>", "<size=9>Error</size>")
            TextEditiTwCDB.Select()
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        If CheckEditIDMS.Checked = True And TextEditiDMSDB.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>iDMS DataBase Name Cannot be empty</size>", "<size=9>Error</size>")
            TextEditiDMSDB.Select()
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        If CheckEditWDMS.Checked = True And TextWDMSDBName.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>ZK WDMS DataBase Name Cannot be empty</size>", "<size=9>Error</size>")
            TextWDMSDBName.Select()
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        If CheckEditAccess.Checked = True And TextZKAccessDBName.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>ZK Access 3.5 DataBase Name Cannot be empty</size>", "<size=9>Error</size>")
            TextZKAccessDBName.Select()
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        If CheckEditBioSecurity.Checked = True And TextBioSecurityDBName.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>ZK Bio Security DataBase Name Cannot be empty</size>", "<size=9>Error</size>")
            TextBioSecurityDBName.Select()
            Me.Cursor = Cursors.Default
            Exit Sub
        End If

        If CheckEditTWAccess.Checked = True And TextEditTWAccessDB.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>TW Access DataBase Name Cannot be empty</size>", "<size=9>Error</size>")
            TextEditTWAccessDB.Select()
            Me.Cursor = Cursors.Default
            Exit Sub
        End If

        Dim sSql As String = "Update tblSetup set TF01Port='" & TF01Port & "',StartRealTime='" & StartRealTime & "', LeaveFinancialYear='" & LeaveFinancialYear & "', Online='" & Online & "', " &
            "AutoDownload ='" & AutoDownload & "', BioPort='" & BioPort & "', ZKPort='" & ZKPort & "', TWIR102Port='" & TWIR102Port & "', Canteen='" & Canteen & "', " &
            "Visitor='" & Visitor & "', IsNepali='" & IsNepali & "', TimerDur='" & TimerDur & "', AutoDwnDur='" & AutoDwnDur & "' , MarkInactivDurDay='" & MarkInactivDurDay & "', " &
            "Bio1EcoPort='" & Bio1EcoPort & "', IsIOCL ='" & IsIOCL & "', IOCLInterval ='" & IOCLInterval & "', IOCLLink ='" & IOCLLink & "', " &
            "TWCloud='" & TWCloud & "', IsWDMS ='" & IsWDMS & "', iDMS ='" & iDMS & "', WDMSDBName='" & WDMSDBName & "', IsBioSecurity='" & IsBioSecurity & "', " &
            "BioSecurityDBName='" & BioSecurityDBName & "', IsZKAccess='" & IsZKAccess & "', ZKAccessDBName='" & ZKAccessDBName & "', IsTWAccess='" & IsTWAccess & "', " &
            "TWAccessDBName='" & TWAccessDBName & "', iDMSDB='" & iDMSDB & "', TWCloudDB='" & TWCloudDB & "', OnlineStartUp='" & OnlineStartUp & "'"
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Common.IsNepali = IsNepali
        Common.TimerDur = TimerDur
        Common.online = Online
        Common.Load_Corporate_PolicySql()
        Common.LogPost("Update Common Settings")
        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>iAS</size>")
    End Sub
    Private Sub ToggleIsIOCL_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleIsIOCL.Toggled
        If ToggleIsIOCL.IsOn = True Then
            PanelControlIOCL.Visible = True
        ElseIf ToggleIsIOCL.IsOn = False Then
            PanelControlIOCL.Visible = False
        End If
    End Sub
    Private Sub ToggleOnlineEvents_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleOnlineEvents.Toggled
        If ToggleOnlineEvents.IsOn = True Then
            PanelControlCloud.Visible = True
        Else
            PanelControlCloud.Visible = False
        End If
    End Sub
    Private Sub CheckEditWDMS_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditWDMS.CheckedChanged
        If CheckEditWDMS.Checked Then
            TextWDMSDBName.Visible = True
        Else
            TextWDMSDBName.Visible = False
        End If
    End Sub
    Private Sub CheckEditAccess_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditAccess.CheckedChanged
        If CheckEditAccess.Checked Then
            TextZKAccessDBName.Visible = True
        Else
            TextZKAccessDBName.Visible = False
        End If
    End Sub
    Private Sub CheckEditBioSecurity_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditBioSecurity.CheckedChanged
        If CheckEditBioSecurity.Checked Then
            TextBioSecurityDBName.Visible = True
        Else
            TextBioSecurityDBName.Visible = False
        End If
    End Sub
    Private Sub CheckEditTWAccess_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditTWAccess.CheckedChanged
        If CheckEditTWAccess.Checked = True Then
            TextEditTWAccessDB.Visible = True
        Else
            TextEditTWAccessDB.Visible = False
        End If
    End Sub
    Private Sub CheckEditIDMS_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditIDMS.CheckedChanged
        If CheckEditIDMS.Checked = True Then
            TextEditiDMSDB.Visible = True
        Else
            TextEditiDMSDB.Visible = False
        End If
    End Sub
    Private Sub CheckEditTWCloud_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditTWCloud.CheckedChanged
        If CheckEditTWCloud.Checked = True Then
            TextEditiTwCDB.Visible = True
        Else
            TextEditiTwCDB.Visible = False
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Try
            Dim sSqlUpdate As String = "Update MIN_MAX_DATE set "
            Dim InsertQuery As String = "Insert into MIN_MAX_DATE ("
            Dim InsertQueryValues As String = " values ("
            If CheckEditTWCloud.Checked Then
                sSqlUpdate = sSqlUpdate & "PUNCH_DATE_TWCloud = '" & DateEditOnline.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' ,"
                InsertQuery = InsertQuery & " PUNCH_DATE_TWCloud"
                InsertQueryValues = InsertQueryValues & " '" & DateEditOnline.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'"
            End If
            If CheckEditIDMS.Checked Then
                sSqlUpdate = sSqlUpdate & "PUNCH_DATE_iDMS = '" & DateEditOnline.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' ,"
                InsertQuery = InsertQuery & ", PUNCH_DATE_iDMS"
                InsertQueryValues = InsertQueryValues & ", '" & DateEditOnline.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'"
            End If
            If CheckEditWDMS.Checked Then
                sSqlUpdate = sSqlUpdate & "PUNCH_DATE_WDMS = '" & DateEditOnline.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' ,"
                InsertQuery = InsertQuery & ", PUNCH_DATE_WDMS"
                InsertQueryValues = InsertQueryValues & ", '" & DateEditOnline.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'"
            End If
            If CheckEditAccess.Checked Then
                sSqlUpdate = sSqlUpdate & "PUNCH_DATE_ZKAccess = '" & DateEditOnline.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' ,"
                InsertQuery = InsertQuery & ", PUNCH_DATE_ZKAccess"
                InsertQueryValues = InsertQueryValues & ", '" & DateEditOnline.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'"
            End If
            If CheckEditBioSecurity.Checked Then
                sSqlUpdate = sSqlUpdate & "PUNCH_DATE_BioSecurity = '" & DateEditOnline.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' ,"
                InsertQuery = InsertQuery & ", PUNCH_DATE_BioSecurity"
                InsertQueryValues = InsertQueryValues & ", '" & DateEditOnline.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'"
            End If
            If CheckEditTWAccess.Checked Then
                sSqlUpdate = sSqlUpdate & "PUNCH_DATE_TWAccess = '" & DateEditOnline.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' ,"
                InsertQuery = InsertQuery & ", PUNCH_DATE_TWAccess"
                InsertQueryValues = InsertQueryValues & ", '" & DateEditOnline.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'"
            End If
            sSqlUpdate = sSqlUpdate.TrimEnd(",")

            InsertQuery = InsertQuery.Replace("(,", "(") & ")"
            InsertQueryValues = InsertQueryValues.Replace("(,", "(") & ")"
            Dim insert As String = InsertQuery & InsertQueryValues '.Replace("'", "''")

            Dim sSql As String = sSqlUpdate & " IF @@ROWCOUNT=0 " & insert
            If sSqlUpdate = "Update MIN_MAX_DATE set " Then
                XtraMessageBox.Show(ulf, "<size=10>Please select Events to reset</size>", "<size=9>Error</size>")
                Exit Sub
            End If
            Dim cmd As New SqlCommand
            Dim cmd1 As New OleDbCommand
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If           
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message & "</size>", "<size=9>iAS</size>")
        End Try
        If CheckEditIDMS.Checked Then
            Try
                Dim connectionString_iDMS = Common.ConnectionString.Replace(Common.DB, TextEditiDMSDB.Text.Trim) ' "Data Source=" & Common.servername & ";Initial Catalog=TimeWatch;User Id=sa;Password=Passw0rd!;"
                Dim coniDMS As SqlConnection = New SqlConnection(connectionString_iDMS)
                Dim sSql As String = "update UserAttendance set iASCheck ='N' where AttDateTime >= '" & DateEditOnline.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'"
                If coniDMS.State <> ConnectionState.Open Then
                    coniDMS.Open()
                End If
                Dim cmd As New SqlCommand
                cmd = New SqlCommand(sSql, coniDMS)
                cmd.ExecuteNonQuery()
                If coniDMS.State <> ConnectionState.Closed Then
                    coniDMS.Close()
                End If
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>" & ex.Message & "</size>", "<size=9>iAS</size>")
                Exit Sub
            End Try
        End If
        Common.LogPost("Reset 3rd Party Events to " & DateEditOnline.DateTime.ToString("dd/MM/yyyy"))
        XtraMessageBox.Show(ulf, "<size=10>Reset Success</size>", "<size=9>iAS</size>")
    End Sub
End Class
