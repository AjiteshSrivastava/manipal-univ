﻿'Imports System.Resources
'Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
'Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
'Imports DevExpress.XtraGrid
'Imports System.ComponentModel
'Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
'Imports DevExpress.XtraSplashScreen
'Imports System.Runtime.CompilerServices
'Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel
Imports iAS.HorizontalMerging
'Imports DevExpress.Export
'Imports DevExpress.XtraPrinting
'Imports DevExpress.Printing.ExportHelpers
'Imports DevExpress.Export.Xl
Imports System.Text.RegularExpressions

Public Class XtraReportsPay
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim WhichReport As Integer
    Dim g_WhereClause As String
    Dim g_WhereClause_rpt As String
    Dim g_SkipAfterDept As Boolean
    Dim g_LinesPerPage As Integer
    Dim mStrProcName As String
    Dim mstrDepartmentCode As String
    Dim mintPageNo As Integer
    Dim mintLine As Integer
    Dim mFileNumber As Integer
    Dim mblnCheckReport As Boolean
    Dim mlngStartingNoOfDays As Long
    Dim mstrFile_Name As String
    Dim mShift As String
    Dim g_MachineRawPunch As String = "admin"
    Dim mCode As String
    Dim isPdf As Boolean
    Private ViewHandler As MyGridViewHandler = Nothing
    'Public Shared Common.tbl As New Data.DataTable()
    Dim sSql As String
    Dim mstrSqlEmpTypeClause As String

    Dim adap As SqlDataAdapter
    Dim adapA As OleDbDataAdapter
    Dim ds As DataSet
    Dim g_pfmaxlimit As Double
    Dim n As Integer, mReimbstr As String, mReimbstr1 As String, mReimbstr2 As String, mReimbstr3 As String
    Dim rsPaysetup As DataSet
    Public Sub New()
        InitializeComponent()
        ViewHandler = New MyGridViewHandler(GridView1)
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraReportsPay_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        DateEditFrom.DateTime = Now.AddMonths(-1) 'New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
        DateEditTo.EditValue = Now

        If Common.IsNepali = "Y" Then
            ComboNepaliYearFrm.Visible = True
            ComboNEpaliMonthFrm.Visible = True
            'ComboNepaliDateFrm.Visible = True
            ComboNepaliYearTo.Visible = False
            ComboNEpaliMonthTo.Visible = False
            'ComboNepaliDateTo.Visible = True

            DateEditFrom.Visible = False
            DateEditTo.Visible = False

            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(DateEditFrom.DateTime.Year, DateEditFrom.DateTime.Month, DateEditFrom.DateTime.Day))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYearFrm.EditValue = dojTmp(0)
            ComboNEpaliMonthFrm.SelectedIndex = dojTmp(1) - 1

            doj = DC.ToBS(New Date(DateEditTo.DateTime.Year, DateEditTo.DateTime.Month, DateEditTo.DateTime.Day))
            dojTmp = doj.Split("-")
            ComboNepaliYearTo.EditValue = dojTmp(0)
            ComboNEpaliMonthTo.SelectedIndex = dojTmp(1) - 1
        Else
            ComboNepaliYearFrm.Visible = False
            ComboNEpaliMonthFrm.Visible = False
            'ComboNepaliDateFrm.Visible = False
            ComboNepaliYearTo.Visible = False
            ComboNEpaliMonthTo.Visible = False
            'ComboNepaliDateTo.Visible = False
            DateEditFrom.Visible = True
            'DateEditTo.Visible = True
        End If

        If Common.servername = "Access" Then
            'Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            'GridControlComp.DataSource = SSSDBDataSet.tblCompany1

            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee1

            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment1

            Me.TblCatagory1TableAdapter1.Fill(Me.SSSDBDataSet.tblCatagory1)
            GridControlCat.DataSource = SSSDBDataSet.tblCatagory1

            Me.TblGrade1TableAdapter1.Fill(Me.SSSDBDataSet.tblGrade1)
            GridControlGrade.DataSource = SSSDBDataSet.tblGrade1

            'Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
            'GridControlBranch.DataSource = SSSDBDataSet.tblbranch1
        Else
            'TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            'GridControlComp.DataSource = SSSDBDataSet.tblCompany

            'TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee

            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment

            TblCatagoryTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblCatagoryTableAdapter.Fill(Me.SSSDBDataSet.tblCatagory)
            GridControlCat.DataSource = SSSDBDataSet.tblCatagory

            TblGradeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblGradeTableAdapter.Fill(Me.SSSDBDataSet.tblGrade)
            GridControlGrade.DataSource = SSSDBDataSet.tblGrade

            'TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
            'GridControlBranch.DataSource = SSSDBDataSet.tblbranch
        End If
        GridControlEmp.DataSource = Common.EmpNonAdmin
        GridControlBranch.DataSource = Common.LocationNonAdmin
        GridControlComp.DataSource = Common.CompanyNonAdmin

        Common.SetGridFont(GridViewComp, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewEmp, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewCat, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewDept, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewGrade, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewBranch, New System.Drawing.Font("Tahoma", 10))


        If Common.servername = "Access" Then
            CheckEditEmpWiseCtc.Visible = False
        Else
            CheckEditEmpWiseCtc.Visible = True
        End If

        If Common.IsFullPayroll Then
            CheckSalaryCheque.Visible = True
            CheckSalaryCash.Visible = True
            CheckSalaryBank.Visible = True
            CheckSalaryNonBank.Visible = True
            CheckEditPfSt.Visible = True
            CheckEditPfCh.Visible = True

            CheckEditESICh.Visible = True
            CheckEditESISt.Visible = True
            CheckEditArrearReg.Visible = True
            CheckEditArrearSlip.Visible = True
            CheckEditPieceEmp.Visible = True
            CheckEditPieceM.Visible = True
            CheckEditReemSt.Visible = True
            CheckEditReesPyReg.Visible = True
            CheckEdidFullNFinal.Visible = True
            CheckEditSummaryStat.Visible = True
            CheckEditCashAdj.Visible = True
            CheckEditPendingReEm.Visible = True
            CheckEditLEaveEncash.Visible = True
        Else
            CheckSalaryCheque.Visible = False
            CheckSalaryCash.Visible = False
            CheckSalaryBank.Visible = False
            CheckSalaryNonBank.Visible = False
            CheckEditPfSt.Visible = False
            CheckEditPfCh.Visible = False

            CheckEditESICh.Visible = False
            CheckEditESISt.Visible = False
            CheckEditArrearReg.Visible = False
            CheckEditArrearSlip.Visible = False
            CheckEditPieceEmp.Visible = False
            CheckEditPieceM.Visible = False
            CheckEditReemSt.Visible = False
            CheckEditReesPyReg.Visible = False
            CheckEdidFullNFinal.Visible = False
            CheckEditSummaryStat.Visible = False
            CheckEditCashAdj.Visible = False
            CheckEditPendingReEm.Visible = False
            CheckEditLEaveEncash.Visible = False
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click

        Dim startdate As DateTime = Convert.ToDateTime(DateEditFrom.DateTime.Year & "-" & DateEditFrom.DateTime.Month & "-01 00:00:00")
        Dim enddate As DateTime = Convert.ToDateTime(DateEditTo.DateTime.Year & "-" & DateEditTo.DateTime.Month & "-" & System.DateTime.DaysInMonth(DateEditTo.DateTime.Year, DateEditTo.DateTime.Month))


        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            'Dim tmpNow As DateTime = DC.ToBS(New Date(Now.Year, Now.Month, Now.Day))
            Try
                'DateEditFrom.DateTime = DC.ToAD(New Date(ComboNepaliYearFrm.EditValue, ComboNEpaliMonthFrm.SelectedIndex + 1, 1))
                DateEditFrom.DateTime = DC.ToAD(ComboNepaliYearFrm.EditValue & "-" & ComboNEpaliMonthFrm.SelectedIndex + 1 & "-" & 1)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNEpaliMonthFrm.Select()
                Exit Sub
            End Try
            Try
                'DateEditTo.DateTime = DC.ToAD(New Date(ComboNepaliYearTo.EditValue, ComboNEpaliMonthTo.SelectedIndex + 1, 1))
                DateEditTo.DateTime = DC.ToAD(ComboNepaliYearTo.EditValue & "-" & ComboNEpaliMonthTo.SelectedIndex + 1 & "-" & 1)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid To Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNEpaliMonthTo.Select()
                Exit Sub
            End Try
        End If
       
        Common.SelectionFormula = ""
        Set_Filter_Crystal()
        Set_Filter_Crystal_rpt()
        If SidePanelSelection.Visible = False Then
            g_WhereClause = ""
            g_WhereClause_rpt = ""
        End If

        mReimbstr = ""
        mReimbstr1 = ""
        mReimbstr2 = ""
        mReimbstr3 = ""

        rsPaysetup = New DataSet
        Dim sSql As String = "select * from PAY_SETUP"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsPaysetup)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsPaysetup)
        End If
        For n = 1 To Len(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")))
            If n = 1 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    mReimbstr3 = mReimbstr3 & IIf(Len(mReimbstr3) > 0, ",", "") & "Vconv_rate=0 and Vconv_amt=0 "
                    mReimbstr = mReimbstr & IIf(Len(mReimbstr) > 0, ",", "") & "Vconv_rate=0,Vconv_amt=0"
                    mReimbstr1 = mReimbstr1 & IIf(Len(mReimbstr1) > 0, ",", "") & "arrearconv=0,arrearconv_amt=0"
                Else
                    mReimbstr2 = mReimbstr2 & IIf(Len(mReimbstr2) > 0, ",", "") & "Vconv_rate=0,Vconv_amt=0"
                End If
            End If
            If n = 2 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    mReimbstr3 = mReimbstr3 & IIf(Len(mReimbstr3) > 0, " and ", "") & "VMED_rate=0 and VMED_amt=0 "
                    mReimbstr = mReimbstr & IIf(Len(mReimbstr) > 0, ",", "") & "VMED_rate=0,VMED_amt=0"
                    mReimbstr1 = mReimbstr1 & IIf(Len(mReimbstr1) > 0, ",", "") & "arrearmed=0,arrearmed_amt=0"
                Else
                    mReimbstr2 = mReimbstr2 & IIf(Len(mReimbstr2) > 0, ",", "") & "VMED_rate=0,VMED_amt=0"
                End If
            End If
            If n = 3 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    mReimbstr3 = mReimbstr3 & IIf(Len(mReimbstr3) > 0, " and ", "") & "VI_1=0 and VI_1_AMT=0 "
                    mReimbstr = mReimbstr & IIf(Len(mReimbstr) > 0, ",", "") & "VI_1=0,VI_1_AMT=0,VIES_1=''"
                    mReimbstr1 = mReimbstr1 & IIf(Len(mReimbstr1) > 0, ",", "") & "arrearamount1=0,arrearamount1_amt=0,VIES_1=''"
                Else
                    mReimbstr2 = mReimbstr2 & IIf(Len(mReimbstr2) > 0, ",", "") & "VI_1=0,VI_1_AMT=0,VIES_1=''"
                End If
            End If
            If n = 4 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    mReimbstr3 = mReimbstr3 & IIf(Len(mReimbstr3) > 0, " and ", "") & "VI_2=0 and VI_2_AMT=0 "
                    mReimbstr = mReimbstr & IIf(Len(mReimbstr) > 0, ",", "") & "VI_2=0,VI_2_AMT=0,VIES_2=''"
                    mReimbstr1 = mReimbstr1 & IIf(Len(mReimbstr1) > 0, ",", "") & "arrearamount2=0,arrearamount2_amt=0,VIES_2=''"
                Else
                    mReimbstr2 = mReimbstr2 & IIf(Len(mReimbstr2) > 0, ",", "") & "VI_2=0,VI_2_AMT=0,VIES_2=''"
                End If
            End If
            If n = 5 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    mReimbstr3 = mReimbstr3 & IIf(Len(mReimbstr3) > 0, " and ", "") & "VI_3=0 and VI_3_AMT=0 "
                    mReimbstr = mReimbstr & IIf(Len(mReimbstr) > 0, ",", "") & "VI_3=0,VI_3_AMT=0,VIES_3=''"
                    mReimbstr1 = mReimbstr1 & IIf(Len(mReimbstr1) > 0, ",", "") & "arrearamount3=0,arrearamount3_amt=0,VIES_3=''"
                Else
                    mReimbstr2 = mReimbstr2 & IIf(Len(mReimbstr2) > 0, ",", "") & "VI_3=0,VI_3_AMT=0,VIES_3=''"
                End If
            End If
            If n = 6 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    mReimbstr3 = mReimbstr3 & IIf(Len(mReimbstr3) > 0, " and ", "") & "VI_4=0 and VI_4_AMT=0 "
                    mReimbstr = mReimbstr & IIf(Len(mReimbstr) > 0, ",", "") & "VI_4=0,VI_4_AMT=0,VIES_4=''"
                    mReimbstr1 = mReimbstr1 & IIf(Len(mReimbstr1) > 0, ",", "") & "arrearamount4=0,arrearamount4_amt=0,VIES_4=''"
                Else
                    mReimbstr2 = mReimbstr2 & IIf(Len(mReimbstr2) > 0, ",", "") & "VI_4=0,VI_4_AMT=0,VIES_4=''"
                End If
            End If
            If n = 7 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    mReimbstr3 = mReimbstr3 & IIf(Len(mReimbstr3) > 0, " and ", "") & "VI_5=0 and VI_5_AMT=0 "
                    mReimbstr = mReimbstr & IIf(Len(mReimbstr) > 0, ",", "") & "VI_5=0,VI_5_AMT=0,VIES_5=''"
                    mReimbstr1 = mReimbstr1 & IIf(Len(mReimbstr1) > 0, ",", "") & "arrearamount5=0,arrearamount5_amt=0,VIES_5=''"
                Else
                    mReimbstr2 = mReimbstr2 & IIf(Len(mReimbstr2) > 0, ",", "") & "VI_5=0,VI_5_AMT=0,VIES_5=''"
                End If
            End If
            If n = 8 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    mReimbstr3 = mReimbstr3 & IIf(Len(mReimbstr3) > 0, " and ", "") & "VI_6=0 and VI_6_AMT=0 "
                    mReimbstr = mReimbstr & IIf(Len(mReimbstr) > 0, ",", "") & "VI_6=0,VI_6_AMT=0,VIES_6=''"
                    mReimbstr1 = mReimbstr1 & IIf(Len(mReimbstr1) > 0, ",", "") & "arrearamount6=0,arrearamount6_amt=0,VIES_6=''"
                Else
                    mReimbstr2 = mReimbstr2 & IIf(Len(mReimbstr2) > 0, ",", "") & "VI_6=0,VI_6_AMT=0,VIES_6=''"
                End If
            End If
            If n = 9 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    mReimbstr3 = mReimbstr3 & IIf(Len(mReimbstr3) > 0, " and ", "") & "VI_7=0 and VI_7_AMT=0 "
                    mReimbstr = mReimbstr & IIf(Len(mReimbstr) > 0, ",", "") & "VI_7=0,VI_7_AMT=0,VIES_7=''"
                    mReimbstr1 = mReimbstr1 & IIf(Len(mReimbstr1) > 0, ",", "") & "arrearamount7=0,arrearamount7_amt=0,VIES_7=''"
                Else
                    mReimbstr2 = mReimbstr2 & IIf(Len(mReimbstr2) > 0, ",", "") & "VI_7=0,VI_7_AMT=0,VIES_7=''"
                End If
            End If
            If n = 10 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    mReimbstr3 = mReimbstr3 & IIf(Len(mReimbstr3) > 0, " and ", "") & "VI_8=0 and VI_8_AMT=0 "
                    mReimbstr = mReimbstr & IIf(Len(mReimbstr) > 0, ",", "") & "VI_8=0,VI_8_AMT=0,VIES_8=''"
                    mReimbstr1 = mReimbstr1 & IIf(Len(mReimbstr1) > 0, ",", "") & "arrearamount8=0,arrearamount8_amt=0,VIES_8=''"
                Else
                    mReimbstr2 = mReimbstr2 & IIf(Len(mReimbstr2) > 0, ",", "") & "VI_8=0,VI_8_AMT=0,VIES_8=''"
                End If
            End If
            If n = 11 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    mReimbstr3 = mReimbstr3 & IIf(Len(mReimbstr3) > 0, " and ", "") & "VI_9=0 and VI_9_AMT=0 "
                    mReimbstr = mReimbstr & IIf(Len(mReimbstr) > 0, ",", "") & "VI_9=0,VI_9_AMT=0,VIES_9=''"
                    mReimbstr1 = mReimbstr1 & IIf(Len(mReimbstr1) > 0, ",", "") & "arrearamount9=0,arrearamount9_amt=0,VIES_9=''"
                Else
                    mReimbstr2 = mReimbstr2 & IIf(Len(mReimbstr2) > 0, ",", "") & "VI_9=0,VI_9_AMT=0,VIES_9=''"
                End If
            End If
            If n = 12 Then
                If Mid(Trim(rsPaysetup.Tables(0).Rows(0)("REIMBREL")), n, 1) = "1" Then
                    mReimbstr3 = mReimbstr3 & IIf(Len(mReimbstr3) > 0, " and ", "") & "VI_10=0 and VI_10_AMT=0 "
                    mReimbstr = mReimbstr & IIf(Len(mReimbstr) > 0, ",", "") & "VI_10=0,VI_10_AMT=0,VIES_10=''"
                    mReimbstr1 = mReimbstr1 & IIf(Len(mReimbstr1) > 0, ",", "") & "arrearamount10=0,arrearamount10_amt=0,VIES_10=''"
                Else
                    mReimbstr2 = mReimbstr2 & IIf(Len(mReimbstr2) > 0, ",", "") & "VI_10=0,VI_10_AMT=0,VIES_10=''"
                End If
            End If
        Next n
        If Len(mReimbstr) > 0 Then
            mReimbstr = "UPDATE PAY_TEMP SET " & mReimbstr
            mReimbstr1 = "UPDATE ARREARREG SET " & mReimbstr1
        End If
        If Len(mReimbstr2) > 0 Then
            mReimbstr2 = "UPDATE PAY_TEMP SET " & mReimbstr2
        End If


        Dim TitleTemp As String = ""
        Dim TitleTemp1 As String = ""
        If Common.IsNepali = "Y" Then
            TitleTemp = ComboNEpaliMonthFrm.Text & " " & ComboNepaliYearFrm.Text
            TitleTemp1 = ComboNEpaliMonthFrm.Text & " " & ComboNepaliYearFrm.Text & " To " & ComboNEpaliMonthTo.Text & " " & ComboNepaliYearTo.Text
        Else
            TitleTemp = DateEditFrom.DateTime.ToString("MMM yyyy")
            TitleTemp1 = DateEditFrom.DateTime.ToString("MMM yyyy") & " To " & DateEditTo.DateTime.ToString("MMM yyyy")
        End If

        If CheckSalarySlip.Checked = True Then
            Common.CrystalReportTitle = "Salary Slip for Month of " & TitleTemp
            Common.CryReportType = "SalarySlip"
            salarySlip()
        ElseIf CheckSalaryRegister.Checked = True Then
            Common.CrystalReportTitle = "Salary Register for Month of " & TitleTemp
            Common.CryReportType = "SalaryRegister"
            salarySlip()
        ElseIf CheckSalaryCheque.Checked = True Then
            Common.CrystalReportTitle = "Salary Statement by Cheque For the Month of " & TitleTemp
            Common.CryReportType = "CheckSalaryCheque"
            salarySlip()
        ElseIf CheckSalaryCash.Checked = True Then
            Common.CrystalReportTitle = "Salary Statement by Cash For the Month of " & TitleTemp
            Common.CryReportType = "CheckSalaryCash"
            salarySlip()
        ElseIf CheckSalaryBank.Checked = True Then
            Common.CrystalReportTitle = "SALARY STATEMENT OF OUR EMPLOYEES FOR THE MONTH OF " & TitleTemp
            Common.CryReportType = "CheckSalaryBank"
            salarySlip()
        ElseIf CheckSalaryNonBank.Checked = True Then
            Common.CrystalReportTitle = "Non Bank Salary Statement For the Month of " & TitleTemp
            Common.CryReportType = "CheckSalaryNonBank"
            salarySlip()
        ElseIf CheckEditSummaryStat.Checked = True Then
            Common.CrystalReportTitle = "Summary Statement for Month of " & TitleTemp
            Common.CryReportType = "CheckEditSummaryStat"
            salarySlip()
        ElseIf CheckEditEmpWiseCtc.Checked = True Then
            Common.CrystalReportTitle = "Cost To Company (Employee Wise)"
            Common.CryReportType = "EmpWiseCTC"
            MasterRep()
        ElseIf CheckEditLoanEntry.Checked = True Then
            Common.CryReportType = "LoanEntryStatement"
            Common.CrystalReportTitle = "Loan Entry Statement From " & TitleTemp1
            If g_WhereClause_rpt <> "" Then
                Common.SelectionFormula = g_WhereClause_rpt & " AND {TBLADVANCE.A_L}='L' AND {TBLADVANCE.MON_YEAR} >= #" & startdate.ToString("yyyy-MM-dd 00:00:00") & "# and {TBLADVANCE.MON_YEAR} <= #" & enddate.ToString("yyyy-MM-dd 00:00:00") & "# "
            Else
                Common.SelectionFormula = " {TBLADVANCE.A_L}='L' AND {TBLADVANCE.MON_YEAR} >= #" & startdate.ToString("yyyy-MM-dd 00:00:00") & "# and {TBLADVANCE.MON_YEAR} <= #" & enddate.ToString("yyyy-MM-dd 00:00:00") & "#"
            End If
            XtraDocVIewer.ShowDialog()
        ElseIf CheckEditAdvEntry.Checked = True Then
            Common.CryReportType = "LoanEntryStatement"
            Common.CrystalReportTitle = "Advance Entry Statement From " & TitleTemp1
            If g_WhereClause_rpt <> "" Then
                Common.SelectionFormula = g_WhereClause_rpt & " AND {TBLADVANCE.A_L}='A' AND {TBLADVANCE.MON_YEAR} >= #" & startdate.ToString("yyyy-MM-dd 00:00:00") & "# and {TBLADVANCE.MON_YEAR} <= #" & enddate.ToString("yyyy-MM-dd 00:00:00") & "#"
            Else
                Common.SelectionFormula = " {TBLADVANCE.A_L}='A' AND {TBLADVANCE.MON_YEAR} >= #" & startdate.ToString("yyyy-MM-dd 00:00:00") & "# and {TBLADVANCE.MON_YEAR} <= #" & enddate.ToString("yyyy-MM-dd 00:00:00") & "#"
            End If
            XtraDocVIewer.ShowDialog()
        ElseIf CheckEditLoanEntryDetails.Checked = True Then
            Common.CrystalReportTitle = "Loan Entry Details From " & TitleTemp1
            Common.CryReportType = "LoanEntryDetails"

            If g_WhereClause_rpt <> "" Then
                Common.SelectionFormula = g_WhereClause_rpt & " AND {TBLADVANCE.A_L}='L' AND {TBLADVANCE.MON_YEAR} >= #" & startdate.ToString("yyyy-MM-dd 00:00:00") & "# and {TBLADVANCE.MON_YEAR} <= #" & enddate.ToString("yyyy-MM-dd 00:00:00") & "#"
            Else
                Common.SelectionFormula = " {TBLADVANCE.A_L}='L' AND {TBLADVANCE.MON_YEAR} >= #" & startdate.ToString("yyyy-MM-dd 00:00:00") & "# and {TBLADVANCE.MON_YEAR} <= #" & enddate.ToString("yyyy-MM-dd 00:00:00") & "#"
            End If
            XtraDocVIewer.ShowDialog()
        ElseIf CheckEditAdvEntryDetails.Checked = True Then
            Common.CrystalReportTitle = "Advance Entry Details From " & TitleTemp1
            Common.CryReportType = "LoanEntryDetails"

            If g_WhereClause_rpt <> "" Then
                Common.SelectionFormula = g_WhereClause_rpt & " AND {TBLADVANCE.A_L}='A' AND {TBLADVANCE.MON_YEAR} >= #" & startdate.ToString("yyyy-MM-dd 00:00:00") & "# and {TBLADVANCE.MON_YEAR} <= #" & enddate.ToString("yyyy-MM-dd 00:00:00") & "#"
            Else
                Common.SelectionFormula = " {TBLADVANCE.A_L}='A' AND {TBLADVANCE.MON_YEAR} >= #" & startdate.ToString("yyyy-MM-dd 00:00:00") & "# and {TBLADVANCE.MON_YEAR} <= #" & enddate.ToString("yyyy-MM-dd 00:00:00") & "#"
            End If
            XtraDocVIewer.ShowDialog()
        ElseIf CheckEditPfSt.Checked = True Then
            Common.CrystalReportTitle = "PF Statement for " & TitleTemp
            Common.CryReportType = "PFStatement"
            PF()
        ElseIf CheckEditPfCh.Checked = True Then
            Common.CrystalReportTitle = "PF Chalan for " & TitleTemp
            Common.CryReportType = "PFChalan"
            PF()
        ElseIf CheckEditESISt.Checked = True Then
            Common.CrystalReportTitle = "ESI Statement for " & TitleTemp
            Common.CryReportType = "ESItatement"
            ESI()
        ElseIf CheckEditESICh.Checked = True Then
            Common.CrystalReportTitle = "ESI Chalan for " & TitleTemp
            Common.CryReportType = "ESIChalan"
            ESI()
        ElseIf CheckEditArrearSlip.Checked = True Then
            Common.CrystalReportTitle = "Arrear Slip for " & TitleTemp
            Common.CryReportType = "ArrearSlip"
            ArrearRep()
        ElseIf CheckEditArrearReg.Checked = True Then
            Common.CrystalReportTitle = "Arrear Register for " & TitleTemp
            Common.CryReportType = "ArrearReg"
            ArrearRep()
        ElseIf CheckEditPieceM.Checked = True Then
            Common.CrystalReportTitle = "Piece Master Information"
            Common.CryReportType = "PieceMaster"
            Common.SelectionFormula = g_WhereClause_rpt
            XtraDocVIewer.ShowDialog()
        ElseIf CheckEditPieceEmp.Checked = True Then
            DateEditTo.DateTime = DateEditFrom.DateTime.AddMonths(1).AddDays(-1)
            Common.CrystalReportTitle = "Piece Details for " & DateEditFrom.DateTime.ToString("MMMM yyyy") '& " To " & DateEditTo.DateTime.ToString("MMMM yyyy")
            Common.CryReportType = "PieceEmp"

            If g_WhereClause_rpt <> "" Then
                Common.SelectionFormula = g_WhereClause_rpt & " and {TBLPIECEDATA.ENTRY_DATE} >= #" & startdate.ToString("yyyy-MM-dd 00:00:00") & "# and  {TBLPIECEDATA.ENTRY_DATE} <= #" & enddate.ToString("yyyy-MM-dd 00:00:00") & "#"
            Else
                Common.SelectionFormula = " {TBLPIECEDATA.ENTRY_DATE} >= #" & startdate.ToString("yyyy-MM-dd 00:00:00") & "# and  {TBLPIECEDATA.ENTRY_DATE} <= #" & enddate.ToString("yyyy-MM-dd 00:00:00") & "#"
            End If

            XtraDocVIewer.ShowDialog()
        ElseIf CheckEditReemSt.Checked = True Then
            Common.CrystalReportTitle = "Reimbursment Statement"
            Common.CryReportType = "CheckEditReemSt"
            salarySlip()
        ElseIf CheckEditReesPyReg.Checked = True Then
            Common.CrystalReportTitle = "Reimbursment Payment Register"
            Common.CryReportType = "CheckEditReesPyReg"
            salarySlip()
        ElseIf CheckEdidFullNFinal.Checked = True Then
            Common.CrystalReportTitle = "Full and Final Statement"
            Common.CryReportType = "CheckEdidFullNFinal"
            YearlyRep()
        ElseIf CheckEditCashAdj.Checked = True Then
            Common.CryReportType = "CheckEditCashAdj"
            Common.CrystalReportTitle = "Cash Adjustment Details from " & Format(DateEditFrom.DateTime, "MMMM yyyy") & " To " & Format(DateEditTo.DateTime, "MMMM yyyy") & "'"
            If g_WhereClause_rpt <> "" Then
                Common.SelectionFormula = g_WhereClause_rpt & " AND {TBLADVANCEDATA.CASH_AMT}>0 AND {TBLADVANCEdata.MON_YEAR} >= #" & startdate.ToString("yyyy-MM-dd 00:00:00") & "# and {TBLADVANCEdata.MON_YEAR} <= #" & enddate.ToString("yyyy-MM-dd 00:00:00") & "#"
            Else
                Common.SelectionFormula = " {TBLADVANCEDATA.CASH_AMT}>0 AND {TBLADVANCEdata.MON_YEAR} >= #" & startdate.ToString("yyyy-MM-dd 00:00:00") & "# and {TBLADVANCEdata.MON_YEAR} <= #" & enddate.ToString("yyyy-MM-dd 00:00:00") & "#"
            End If
            XtraDocVIewer.ShowDialog()
        ElseIf CheckEditPendingReEm.Checked = True Then
            Common.CrystalReportTitle = "Pending Reimbursement List"
            Common.CryReportType = "CheckEditPendingReEm"
            YearlyRep()
        ElseIf CheckEditLEaveEncash.Checked = True Then
            Common.CrystalReportTitle = "Leave Encashment for Accounting Year " & Format(DateEditFrom.DateTime, "yyyy") & "-" & Format(DateEditTo.DateTime, "yyyy")
            Common.CryReportType = "CheckEditLEaveEncash"
            YearlyRep()
        End If
    End Sub

    Public Shared hbank As String
    Public Shared hk As String
    Public Shared amtpfmember1 As String
    Public Shared amtpfmember2 As String
    Public Shared esiamount As String
    Public Shared edli As String
    Private Sub salarySlip()
        If g_WhereClause <> "" Then
            g_WhereClause = " AND " & g_WhereClause
            g_WhereClause = g_WhereClause.Replace("tblEmployee", "b")
        End If
        'If Common.servername = "Access" Then
        '    sSql = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND FORMAT(A.Mon_Year,'MM')=" & DateEditFrom.DateTime.ToString("MM") & " And FORMAT(a.Mon_Year,'YYYY')=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
        'Else
        '    sSql = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
        'End If
        If Common.servername = "Access" Then
            sSql = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND FORMAT(A.Mon_Year,'MM')=" & DateEditFrom.DateTime.ToString("MM") & " And FORMAT(a.Mon_Year,'YYYY')=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
        Else
            sSql = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
        End If

        If Common.CryReportType = "CheckSalaryCheque" Then
            'sSql = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b ,PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND C.VPAYMENTBY='C' AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
            sSql = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND C.VPAYMENTBY='C' AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
        ElseIf Common.CryReportType = "CheckSalaryCash" Then
            sSql = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND C.VPAYMENTBY='R' AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
        ElseIf Common.CryReportType = "CheckSalaryBank" Then
            sSql = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b ,PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND C.VPAYMENTBY='C' AND C.VBANKACC<>'' and c.bankcode<>'' AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
        ElseIf Common.CryReportType = "CheckSalaryNonBank" Then
            sSql = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND C.VPAYMENTBY='C' AND C.VBANKACC='' AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
        ElseIf Common.CryReportType = "CheckEditReemSt" Then
            sSql = "Insert into Pay_Temp Select a.PAYCODE,0,0,0,'',0,0,'',a.vmed_amt,0,'',a.vconv_amt,0,'',0,0,0,'',0,0,'',a.VI_1_AMT,0,'',a.VI_2_AMT,0,'',a.VI_3_AMT,0,'',a.VI_4_AMT,0,'',a.VI_5_AMT,0,'',a.VI_6_AMT,0,'',a.VI_7_AMT,0,'',a.VI_8_AMT,0,'',a.VI_9_AMT,0,'',a.VI_10_AMT,'','','','',0,0,0,0,0,0,0,'',0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,A.MON_YEAR,0,0,0,0,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From PAY_REIMURSH a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
            hbank = "Bank A/c No"
        ElseIf Common.CryReportType = "CheckEditReesPyReg" Then
            sSql = "Insert into Pay_Temp Select a.PAYCODE,0,0,0,'',0,0,'',a.vmed_amt,0,'',a.vconv_amt,0,'',0,0,0,'',0,0,'',a.VI_1_AMT,0,'',a.VI_2_AMT,0,'',a.VI_3_AMT,0,'',a.VI_4_AMT,0,'',a.VI_5_AMT,0,'',a.VI_6_AMT,0,'',a.VI_7_AMT,0,'',a.VI_8_AMT,0,'',a.VI_9_AMT,0,'',a.VI_10_AMT,'','','','',0,0,0,0,0,0,0,'',0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,A.MON_YEAR,0,0,0,0,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From PAY_REIMURSH a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND PAID='Y' AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
            hbank = "Bank A/c No"
        ElseIf Common.CryReportType = "CheckEditSummaryStat" Then
            Dim startdate As DateTime = Convert.ToDateTime(DateEditFrom.DateTime.Year & "-" & DateEditFrom.DateTime.Month & "-01 00:00:00")
            Dim enddate As DateTime = Convert.ToDateTime(DateEditTo.DateTime.Year & "-" & DateEditTo.DateTime.Month & "-" & System.DateTime.DaysInMonth(DateEditTo.DateTime.Year, DateEditTo.DateTime.Month))

            hk = Format(DateEditFrom.DateTime, "MMMM yyyy")
            Dim totsal As Double, totVpf_amt As Double, totvot_amt As Double, totvhra_amt As Double, totvesi_amt As Double, totvda_amt As Double, totarrear_amt As Double, totarrearpf_amt As Double, totarrearVpf_amt As Double, totarrearesi_amt As Double
            Dim totded1 As Double, totded2 As Double, totded3 As Double, totded4 As Double, totded5 As Double, totded6 As Double, totded7 As Double, totded8 As Double, totded9 As Double, totded10 As Double
            Dim totear1 As Double, totear2 As Double, totear3 As Double, totear4 As Double, totear5 As Double, totear6 As Double, totear7 As Double, totear8 As Double, totear9 As Double, totear10 As Double
            Dim tempdept As String, mtotemployee As Integer, mesiemployee As Integer, mpfemployee1 As Integer, mpfemployee2 As Integer, mVpfemployee1 As Integer, mVpfemployee2 As Integer
            Dim mpfamt1 As Double, mpfamt2 As Double, mpfamt3 As Double, mVpfamt1 As Double, mVpfamt2 As Double, totadvance As Double, totfine As Double, esiamount As Double
            Dim totvMed_amt As Double, totvConv_amt As Double, totvTds_amt As Double, totvProf_amt As Double
            Dim totpf_amt As Double
            'If gLDuplicate = "Y" And Not g_Report_view Then
            '    sSql = "select A.*,B.departmentcode from pay_result A,tblemployee B,pay_masterD C where A.paycode=B.paycode and B.paycode=C.paycode and A.mon_year between '" & _
            '            Format(txtFromDate.Value, "dd/MMM/yyyy") & "' and '" & _
            '            Format(TxtToDate.Value, "dd/MMM/yyyy") & "' and " & gcompclause & " and " & gdeptclause & " " & frmPayOption.mstrSqlEmpTypeClause & whereclausesalreg & " order by B.departmentcode,A.paycode,A.mon_year"
            'Else
            sSql = "select A.*,B.departmentcode from pay_result A,tblemployee B,pay_master C where A.paycode=B.paycode and B.paycode=C.paycode and A.mon_year between '" & _
                    startdate.ToString("yyyy-MM-dd") & "' and '" & _
                     enddate.ToString("yyyy-MM-dd") & "' " & g_WhereClause & " order by B.departmentcode,A.paycode,A.mon_year"
            'End If
            Dim rsform6 As DataSet = New DataSet 'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsform6)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsform6)
            End If

            If rsform6.Tables(0).Rows.Count > 0 Then
                sSql = "truncate table pay_summary"
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If

                mpfamt3 = 0
                totsal = 0 : totpf_amt = 0 : totVpf_amt = 0 : totvot_amt = 0 : totvhra_amt = 0 : totvesi_amt = 0 : totvda_amt = 0 : totarrear_amt = 0 : totarrearpf_amt = 0 : totarrearVpf_amt = 0 : totarrearesi_amt = 0
                totded1 = 0 : totded2 = 0 : totded3 = 0 : totded4 = 0 : totded5 = 0 : totded6 = 0 : totded7 = 0 : totded8 = 0 : totded9 = 0 : totded10 = 0
                totear1 = 0 : totear2 = 0 : totear3 = 0 : totear4 = 0 : totear5 = 0 : totear6 = 0 : totear7 = 0 : totear8 = 0 : totear9 = 0 : totear10 = 0
                tempdept = 0 : mtotemployee = 0 : mesiemployee = 0 : mpfemployee1 = 0 : mpfemployee2 = 0 : mVpfemployee1 = 0 : mVpfemployee2 = 0 : mpfamt1 = 0 : mpfamt2 = 0 : mVpfamt1 = 0 : mVpfamt2 = 0 : esiamount = 0 : totadvance = 0 : totfine = 0
                totvMed_amt = 0 : totvConv_amt = 0 : totvTds_amt = 0 : totvProf_amt = 0
                For i As Integer = 0 To rsform6.Tables(0).Rows.Count - 1 '  Do While Not rsform6.EOF
                    tempdept = rsform6.Tables(0).Rows(i)("DepartmentCode")
                    totsal = 0 : totpf_amt = 0 : totVpf_amt = 0 : totvot_amt = 0 : totvhra_amt = 0 : totvesi_amt = 0 : totvda_amt = 0 : totarrear_amt = 0 : totarrearpf_amt = 0 : totarrearVpf_amt = 0 : totarrearesi_amt = 0
                    totded1 = 0 : totded2 = 0 : totded3 = 0 : totded4 = 0 : totded5 = 0 : totded6 = 0 : totded7 = 0 : totded8 = 0 : totded9 = 0 : totded10 = 0
                    totear1 = 0 : totear2 = 0 : totear3 = 0 : totear4 = 0 : totear5 = 0 : totear6 = 0 : totear7 = 0 : totear8 = 0 : totear9 = 0 : totear10 = 0
                    mtotemployee = 0 : mesiemployee = 0 : mpfemployee1 = 0 : mpfemployee2 = 0 : mVpfemployee1 = 0 : mVpfemployee2 = 0 : mVpfamt1 = 0 : mVpfamt2 = 0 : totadvance = 0 : totfine = 0
                    totvMed_amt = 0 : totvConv_amt = 0 : totvTds_amt = 0 : totvProf_amt = 0 : esiamount = 0

                    'sSql = "select * from arrear where paycode IN(SELECT a.PAYCODE FROM TBLEMPLOYEE a,pay_master c WHERE a.paycode=c.paycode " & frmPayOption.mstrSqlEmpTypeClause & " and a.DEPARTMENTCODE='" & rsform6.Tables(0).Rows(i)("DepartmentCode") & "') and  mon_year between '" & _
                    sSql = "select * from arrear where paycode IN(SELECT a.PAYCODE FROM TBLEMPLOYEE a,pay_master c WHERE a.paycode=c.paycode and a.DEPARTMENTCODE='" & rsform6.Tables(0).Rows(i)("DepartmentCode") & "') and  mon_year between '" & _
                 startdate.ToString("yyyy-MM-dd") & "' and '" & _
                 enddate.ToString("yyyy-MM-dd") & "' order by paycode,mon_year"
                    Dim rsArriar As DataSet = New DataSet 'Cn.Execute(sSql)
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(rsArriar)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(rsArriar)
                    End If
                    Do While tempdept = rsform6.Tables(0).Rows(i)("DepartmentCode")
                        mtotemployee = mtotemployee + 1
                        If rsform6.Tables(0).Rows(i)("VESI_AMT") > 0 Then
                            mesiemployee = mesiemployee + 1
                        End If
                        If rsform6.Tables(0).Rows(i)("vpf_amt") > 0 Then
                            If rsform6.Tables(0).Rows(i)("AMTONPF") > rsPaysetup.Tables(0).Rows(0)("PFLIMIT") Then
                                mpfemployee1 = mpfemployee1 + 1
                                mpfamt1 = mpfamt1 + rsform6.Tables(0).Rows(i)("AMTONPF")
                                mpfamt3 = mpfamt3 + rsPaysetup.Tables(0).Rows(0)("PFLIMIT")
                            Else
                                mpfemployee2 = mpfemployee2 + 1
                                mpfamt2 = mpfamt2 + rsform6.Tables(0).Rows(i)("AMTONPF")
                                mpfamt3 = mpfamt3 + rsform6.Tables(0).Rows(i)("AMTONPF")
                            End If
                        End If
                        If rsform6.Tables(0).Rows(i)("VESI_AMT") > 0 Then
                            esiamount = esiamount + rsform6.Tables(0).Rows(i)("AMTONESI")
                        End If
                        If rsform6.Tables(0).Rows(i)("esionot") > 0 Then
                            esiamount = esiamount + rsform6.Tables(0).Rows(i)("VOT_AMT")
                        End If
                        totsal = totsal + rsform6.Tables(0).Rows(i)("VSALARY")
                        totvhra_amt = totvhra_amt + rsform6.Tables(0).Rows(i)("VHRA_AMT")
                        totvMed_amt = totvMed_amt + rsform6.Tables(0).Rows(i)("VMED_AMT")
                        totvConv_amt = totvConv_amt + rsform6.Tables(0).Rows(i)("VCONV_AMT")
                        totear1 = totear1 + rsform6.Tables(0).Rows(i)("VI_1_AMT")
                        totear2 = totear2 + rsform6.Tables(0).Rows(i)("VI_2_AMT")
                        totear3 = totear3 + rsform6.Tables(0).Rows(i)("VI_3_AMT")
                        totear4 = totear4 + rsform6.Tables(0).Rows(i)("VI_4_AMT")
                        totear5 = totear5 + rsform6.Tables(0).Rows(i)("VI_5_AMT")
                        totear6 = totear6 + rsform6.Tables(0).Rows(i)("VI_6_AMT")
                        totear7 = totear7 + rsform6.Tables(0).Rows(i)("VI_7_AMT")
                        totear8 = totear8 + rsform6.Tables(0).Rows(i)("VI_8_AMT")
                        totear9 = totear9 + rsform6.Tables(0).Rows(i)("VI_9_AMT")
                        totear10 = totear10 + rsform6.Tables(0).Rows(i)("VI_10_AMT")
                        totvda_amt = totvda_amt + rsform6.Tables(0).Rows(i)("VDA_AMT")
                        totvot_amt = totvot_amt + rsform6.Tables(0).Rows(i)("VOT_AMT")

                        totpf_amt = totpf_amt + rsform6.Tables(0).Rows(i)("vpf_amt")
                        totVpf_amt = totVpf_amt + rsform6.Tables(0).Rows(i)("VVPF_AMT")
                        totvesi_amt = totvesi_amt + rsform6.Tables(0).Rows(i)("VESI_AMT") + rsform6.Tables(0).Rows(i)("esionot")
                        totvTds_amt = totvTds_amt + rsform6.Tables(0).Rows(i)("VTDS_AMT")
                        totvProf_amt = totvProf_amt + rsform6.Tables(0).Rows(i)("prof_tax_amt")
                        totded1 = totded1 + rsform6.Tables(0).Rows(i)("VD_1_AMT")
                        totded2 = totded2 + rsform6.Tables(0).Rows(i)("VD_2_AMT")
                        totded3 = totded3 + rsform6.Tables(0).Rows(i)("VD_3_AMT")
                        totded4 = totded4 + rsform6.Tables(0).Rows(i)("VD_4_AMT")
                        totded5 = totded5 + rsform6.Tables(0).Rows(i)("VD_5_AMT")
                        totded6 = totded6 + rsform6.Tables(0).Rows(i)("VD_6_AMT")
                        totded7 = totded7 + rsform6.Tables(0).Rows(i)("VD_7_AMT")
                        totded8 = totded8 + rsform6.Tables(0).Rows(i)("VD_8_AMT")
                        totded9 = totded9 + rsform6.Tables(0).Rows(i)("VD_9_AMT")
                        totded10 = totded10 + rsform6.Tables(0).Rows(i)("VD_10_AMT")
                        mVpfamt1 = mVpfamt1 + rsform6.Tables(0).Rows(i)("vepf_amt")
                        mVpfamt2 = mVpfamt2 + rsform6.Tables(0).Rows(i)("vfpf_amt")


                        If Not IsNull(rsform6.Tables(0).Rows(i)("VADVANCE")) Then
                            totadvance = totadvance + rsform6.Tables(0).Rows(i)("VADVANCE")
                        End If
                        If Not IsNull(rsform6.Tables(0).Rows(i)("VFINE")) Then
                            totfine = totfine + rsform6.Tables(0).Rows(i)("VFINE")
                        End If
                        'rsform6.MoveNext()
                        i = i + 1
                        If i > rsform6.Tables(0).Rows.Count - 1 Then
                            If rsArriar.Tables(0).Rows.Count > 0 Then ' And g_Report_view
                                For j As Integer = 0 To rsArriar.Tables(0).Rows.Count - 1 ' Do While Not rsArriar.EOF
                                    mpfamt1 = mpfamt1 + rsArriar.Tables(0).Rows(j)("ARREARPFONAMT")
                                    If rsArriar.Tables(0).Rows(j)("ARREARPFONAMT") > rsPaysetup.Tables(0).Rows(0)("PFLIMIT") Then
                                        mpfamt3 = mpfamt3 + rsPaysetup.Tables(0).Rows(0)("PFLIMIT")
                                    Else
                                        mpfamt3 = mpfamt3 + rsArriar.Tables(0).Rows(j)("ARREARPFONAMT")
                                    End If
                                    totsal = totsal + (rsArriar.Tables(0).Rows(j)("arrearbasic_amt") + rsArriar.Tables(0).Rows(j)("arrearda_amt") + rsArriar.Tables(0).Rows(j)("arrearhra_amt") + rsArriar.Tables(0).Rows(j)("arrearConv_amt") + rsArriar.Tables(0).Rows(j)("arrearMed_amt") + rsArriar.Tables(0).Rows(j)("arrearamount1_amt") + rsArriar.Tables(0).Rows(j)("arrearamount2_amt") + rsArriar.Tables(0).Rows(j)("arrearamount3_amt") + rsArriar.Tables(0).Rows(j)("arrearamount4_amt") + rsArriar.Tables(0).Rows(j)("arrearamount5_amt") + rsArriar.Tables(0).Rows(j)("arrearamount6_amt") + rsArriar.Tables(0).Rows(j)("arrearamount7_amt") + rsArriar.Tables(0).Rows(j)("arrearamount8_amt") + rsArriar.Tables(0).Rows(j)("arrearamount9_amt") + rsArriar.Tables(0).Rows(j)("arrearamount10_amt") - (rsArriar.Tables(0).Rows(j)("ARREARPF") + rsArriar.Tables(0).Rows(j)("arrearvpf") + rsArriar.Tables(0).Rows(j)("ARREARESI")))
                                    mVpfamt1 = mVpfamt1 + rsArriar.Tables(0).Rows(j)("ARREAREPF_AMT")
                                    mVpfamt2 = mVpfamt2 + rsArriar.Tables(0).Rows(j)("ARREARFPF_AMT")
                                    If Not IsNull(rsArriar.Tables(0).Rows(j)("ARREARPF")) Then
                                        totpf_amt = totpf_amt + rsArriar.Tables(0).Rows(j)("ARREARPF")
                                    End If
                                    If Not IsNull(rsArriar.Tables(0).Rows(j)("arrearvpf")) Then
                                        totVpf_amt = totVpf_amt + rsArriar.Tables(0).Rows(j)("arrearvpf")
                                    End If
                                    If Not IsNull(rsArriar.Tables(0).Rows(j)("ARREARESI")) Then
                                        totvesi_amt = totvesi_amt + rsArriar.Tables(0).Rows(j)("ARREARESI")
                                    End If
                                    'rsArriar.MoveNext()
                                    'If rsArriar.EOF Then
                                    '    Exit Do
                                    'End If
                                Next ' Loop
                            End If
                            sSql = "insert into pay_summary (DEPARTMENTCODE,VSALARY,VPF_AMT,VVPF_AMT,VHRA_AMT,VMED_AMT,VCONV_AMT,VESI_AMT,VPROF_TAX_AMT,VTDS_AMT," & _
                                "VD_1_AMT,VD_2_AMT,VD_3_AMT,VD_4_AMT,VD_5_AMT,VD_6_AMT,VD_7_AMT,VD_8_AMT,VD_9_AMT,VD_10_AMT," & _
                                "VI_1_AMT,VI_2_AMT,VI_3_AMT,VI_4_AMT,VI_5_AMT,VI_6_AMT,VI_7_AMT,VI_8_AMT,VI_9_AMT,VI_10_AMT," & _
                                "VDA_AMT,amtonesi,TOTEMPLOYEE,ESIMEMBER,PFMEMBER1,PFMEMBER2,VPFMEMBER1,VPFMEMBER2,VADVANCE,VFINE,VOT_AMT,VEPF_AMT,VFPF_AMT) values('" & _
                                tempdept & "'," & totsal & "," & totpf_amt & "," & totVpf_amt & "," & totvhra_amt & "," & totvMed_amt & "," & totvConv_amt & "," & _
                                totvesi_amt & "," & totvProf_amt & "," & totvTds_amt & "," & totded1 & "," & totded2 & "," & totded3 & "," & totded4 & "," & totded5 & "," & _
                                totded6 & "," & totded7 & "," & totded8 & "," & totded9 & "," & totded10 & "," & totear1 & "," & _
                                totear2 & "," & totear3 & "," & totear4 & "," & totear5 & "," & totear6 & "," & totear7 & "," & _
                                totear8 & "," & totear9 & "," & totear10 & "," & totvda_amt & "," & esiamount & "," & mtotemployee & "," & mesiemployee & "," & mpfemployee1 & "," & mpfemployee2 & "," & mVpfemployee1 & "," & mVpfemployee2 & "," & totadvance & "," & totfine & "," & totvot_amt & "," & mVpfamt1 & "," & mVpfamt2 & ")"
                            'Cn.Execute(sSql)
                            If Common.servername = "Access" Then
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(sSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                            Exit Do
                        End If
                        If tempdept <> rsform6.Tables(0).Rows(i)("DepartmentCode") Then
                            If rsArriar.Tables(0).Rows.Count > 0 Then  'And g_Report_view
                                For j As Integer = 0 To rsArriar.Tables(0).Rows.Count - 1 ' Do While Not rsArriar.EOF
                                    totarrear_amt = totarrear_amt + (rsArriar.Tables(0).Rows(j)("arrearbasic_amt") + rsArriar.Tables(0).Rows(j)("arrearda_amt") + rsArriar.Tables(0).Rows(j)("arrearhra_amt") + rsArriar.Tables(0).Rows(j)("arrearConv_amt") + rsArriar.Tables(0).Rows(j)("arrearMed_amt") + rsArriar.Tables(0).Rows(j)("arrearamount1_amt") + rsArriar.Tables(0).Rows(j)("arrearamount2_amt") + rsArriar.Tables(0).Rows(j)("arrearamount3_amt") + rsArriar.Tables(0).Rows(j)("arrearamount4_amt") + rsArriar.Tables(0).Rows(j)("arrearamount5_amt") + rsArriar.Tables(0).Rows(j)("arrearamount6_amt") + rsArriar.Tables(0).Rows(j)("arrearamount7_amt") + rsArriar.Tables(0).Rows(j)("arrearamount8_amt") + rsArriar.Tables(0).Rows(j)("arrearamount9_amt") + rsArriar.Tables(0).Rows(j)("arrearamount10_amt") - (rsArriar.Tables(0).Rows(j)("ARREARPF") + rsArriar.Tables(0).Rows(j)("arrearvpf") + rsArriar.Tables(0).Rows(j)("ARREARESI")))
                                    If Not IsNull(rsArriar.Tables(0).Rows(j)("ARREARPF")) Then
                                        totarrearpf_amt = totarrearpf_amt + rsArriar.Tables(0).Rows(j)("ARREARPF")
                                    End If
                                    If Not IsNull(rsArriar.Tables(0).Rows(j)("ARREARESI")) Then
                                        totarrearesi_amt = totarrearesi_amt + rsArriar.Tables(0).Rows(j)("ARREARESI")
                                    End If
                                    'rsArriar.MoveNext()
                                    'If rsArriar.EOF Then
                                    '    Exit Do
                                    'End If
                                Next ' Loop
                            End If
                            sSql = "insert into pay_summary (DEPARTMENTCODE,VSALARY,VPF_AMT,VVPF_AMT,VHRA_AMT,VMED_AMT,VCONV_AMT,VESI_AMT,VPROF_TAX_AMT,VTDS_AMT," & _
                                "VD_1_AMT,VD_2_AMT,VD_3_AMT,VD_4_AMT,VD_5_AMT,VD_6_AMT,VD_7_AMT,VD_8_AMT,VD_9_AMT,VD_10_AMT," & _
                                "VI_1_AMT,VI_2_AMT,VI_3_AMT,VI_4_AMT,VI_5_AMT,VI_6_AMT,VI_7_AMT,VI_8_AMT,VI_9_AMT,VI_10_AMT," & _
                                "VDA_AMT,amtonesi,TOTEMPLOYEE,ESIMEMBER,PFMEMBER1,PFMEMBER2,VPFMEMBER1,VPFMEMBER2,VADVANCE,VFINE,VOT_AMT,VEPF_AMT,VFPF_AMT) values('" & _
                                tempdept & "'," & totsal & "," & totpf_amt & "," & totVpf_amt & "," & totvhra_amt & "," & totvMed_amt & "," & totvConv_amt & "," & _
                                totvesi_amt & "," & totvProf_amt & "," & totvTds_amt & "," & totded1 & "," & totded2 & "," & totded3 & "," & totded4 & "," & totded5 & "," & _
                                totded6 & "," & totded7 & "," & totded8 & "," & totded9 & "," & totded10 & "," & totear1 & "," & _
                                totear2 & "," & totear3 & "," & totear4 & "," & totear5 & "," & totear6 & "," & totear7 & "," & _
                                totear8 & "," & totear9 & "," & totear10 & "," & totvda_amt & "," & esiamount & "," & mtotemployee & "," & mesiemployee & "," & mpfemployee1 & "," & mpfemployee2 & "," & mVpfemployee1 & "," & mVpfemployee2 & "," & totadvance & "," & totfine & "," & totvot_amt & "," & mVpfamt1 & "," & mVpfamt2 & ")"
                            'Cn.Execute(sSql)
                            If Common.servername = "Access" Then
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(sSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                        End If
                    Loop
                Next '  Loop
            End If
            amtpfmember1 = Format(mpfamt1, "0000000.00")
            amtpfmember2 = Format(mpfamt2, "0000000.00")
            esiamount = Format(Math.Round(esiamount, 2), "0000000.00")
            edli = mpfamt3
            'frmMdiMain.rptPayroll.Formulas(1) = sTitle2
            'frmMdiMain.rptPayroll.Formulas(2) = sTitle3
            'frmMdiMain.rptPayroll.Formulas(3) = sTitle4
            'frmMdiMain.rptPayroll.Formulas(4) = sTitle5
            'sSort = "" : sSort1 = ""
            XtraDocVIewer.ShowDialog()
            Exit Sub
        End If
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand("delete from Pay_Temp", Common.con1)
            cmd1.ExecuteNonQuery()
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand("delete from Pay_Temp", Common.con)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If

        sSql = "select * from pay_temp"
        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available<size>", "<size=9>Error</size>")
            Exit Sub
        End If
        XtraDocVIewer.ShowDialog()
        'Dim sb As StringBuilder = New System.Text.StringBuilder()
        'For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
        '    sb.Append("<table width='100%' cellpadding='0' cellspacing='0'  border='1'>")
        '    sb.Append("<tr>")
        '    sb.Append("<center>")
        '    sb.Append("<h7>TimeWatch Infocom</h7>")
        '    sb.Append("<br>")
        '    sb.Append("<br>")
        '    sb.Append("<h7>Salary Slip for the month of " & DateEditTo.DateTime.ToString("MMM yyyy") & "</h7>")
        '    sb.Append("</center>")

        '    sb.Append("<br>")
        '    sb.Append("Name : test")
        '    sb.Append("</tr>")


        '    sb.Append("<tr>")
        '    sb.Append("<td>") : sb.Append(" Guardian : ") : sb.Append("</td>")
        '    sb.Append("<td>") : sb.Append(" Designation : ") : sb.Append("</td>")
        '    sb.Append("<td align='right'>") : sb.Append(" Run Date : " & Now.ToString("dd MMM yyyy")) : sb.Append("</td>")
        '    sb.Append("</tr>")

        '    sb.Append("</table>")
        'Next
        'Dim mstrFile_Name As String = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".html"
        'Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        'objWriter.WriteLine(sb)
        'objWriter.Close()
        'Process.Start(mstrFile_Name)
    End Sub
    Private Function IsNull(tmpStr As Object) As Boolean
        If tmpStr.ToString.Trim = "" Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub MasterRep()
        If g_WhereClause.Trim <> "" Then
            'g_WhereClause = " AND " & g_WhereClause
        End If
        
        If g_WhereClause <> "" Then
            g_WhereClause = g_WhereClause.Replace("tblEmployee", "c")
            sSql = "insert into pay_temp select c.PAYCODE,c.VBASIC,0,c.VDA_RATE,c.VDA_F,0,c.VMED_RATE,c.VMED_F,0,c.VCONV_RATE,c.VCONV_F,0,c.VHRA_RATE,c.VHRA_F,0,0,c.VOT_RATE,c.VOT_FLAG,0,c.VI_1,c.VIT_1,0,c.VI_2,c.VIT_2,0,c.VI_3,c.VIT_3,0,c.VI_4,c.VIT_4,0,c.VI_5,c.VIT_5,0,c.VI_6,c.VIT_6,0,c.VI_7,c.VIT_7,0,c.VI_8,c.VIT_8,0,c.VI_9,c.VIT_9,0,c.VI_10,c.VIT_10,0,c.PF_ALLOWED,c.ESI_ALLOWED,c.VPF_ALLOWED,c.PROF_TAX_ALLOWED,0,0,0,0,0,0,c.VTDS_RATE,c.VTDS_F,0,c.VDT_1,c.VD_1,0,c.VDT_2,c.VD_2,0,c.VDT_3,c.VD_3,0,c.VDT_4,c.VD_4,0,c.VDT_5,c.VD_5,0,c.VDT_6,c.VD_6,0,c.VDT_7,c.VD_7,0,c.VDT_8,c.VD_8,0,c.VDT_9,c.VD_9,0,c.VDT_10,c.VD_10,0,0,0,0,0,0,0,0,0,0,0,0," & Date.DaysInMonth(Now.Year, Now.Month) & ",0,0,0,0,0,'" & Now.ToString("yyyy-MM-01") & "',0,0,0,0,b.vdes_1,b.vdes_2,b.vdes_3,b.vdes_4,b.vdes_5,b.vdes_6,b.vdes_7,b.vdes_8,b.vdes_9,b.vdes_10,b.vIes_1,b.vies_2,b.vies_3,b.vies_4,b.vies_5,b.vies_6,b.vies_7,b.vies_8,b.vies_9,b.vies_10 From Pay_master c,pay_setup b where " & g_WhereClause
        Else
            sSql = "insert into pay_temp select c.PAYCODE,c.VBASIC,0,c.VDA_RATE,c.VDA_F,0,c.VMED_RATE,c.VMED_F,0,c.VCONV_RATE,c.VCONV_F,0,c.VHRA_RATE,c.VHRA_F,0,0,c.VOT_RATE,c.VOT_FLAG,0,c.VI_1,c.VIT_1,0,c.VI_2,c.VIT_2,0,c.VI_3,c.VIT_3,0,c.VI_4,c.VIT_4,0,c.VI_5,c.VIT_5,0,c.VI_6,c.VIT_6,0,c.VI_7,c.VIT_7,0,c.VI_8,c.VIT_8,0,c.VI_9,c.VIT_9,0,c.VI_10,c.VIT_10,0,c.PF_ALLOWED,c.ESI_ALLOWED,c.VPF_ALLOWED,c.PROF_TAX_ALLOWED,0,0,0,0,0,0,c.VTDS_RATE,c.VTDS_F,0,c.VDT_1,c.VD_1,0,c.VDT_2,c.VD_2,0,c.VDT_3,c.VD_3,0,c.VDT_4,c.VD_4,0,c.VDT_5,c.VD_5,0,c.VDT_6,c.VD_6,0,c.VDT_7,c.VD_7,0,c.VDT_8,c.VD_8,0,c.VDT_9,c.VD_9,0,c.VDT_10,c.VD_10,0,0,0,0,0,0,0,0,0,0,0,0," & Date.DaysInMonth(Now.Year, Now.Month) & ",0,0,0,0,0,'" & Now.ToString("yyyy-MM-01") & "',0,0,0,0,b.vdes_1,b.vdes_2,b.vdes_3,b.vdes_4,b.vdes_5,b.vdes_6,b.vdes_7,b.vdes_8,b.vdes_9,b.vdes_10,b.vIes_1,b.vies_2,b.vies_3,b.vies_4,b.vies_5,b.vies_6,b.vies_7,b.vies_8,b.vies_9,b.vies_10 From Pay_master c,pay_setup b "
        End If

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand("delete from Pay_Temp", Common.con1)
            cmd1.ExecuteNonQuery()
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand("delete from Pay_Temp", Common.con)
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If

        sSql = "select A.*,B.VEmployeeType,B.pfulimit from pay_temp A,PAY_MASTER B where A.PAYCODE=B.PAYCODE"
        Dim tProcess As Boolean = DoTProcess(sSql)

        hbank = "Bank A/c No"
        Esiemployer = rsPaysetup.Tables(0).Rows(0)("esi")
        Esiemp = rsPaysetup.Tables(0).Rows(0)("esie")

        sSql = "select * from pay_temp"
        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available<size>", "<size=9>Error</size>")
            Exit Sub
        End If
        XtraDocVIewer.ShowDialog()

        'Cn.Execute(sSql)
        'sSql = "select A.*,B.VEmployeeType,B.pfulimit from pay_temp A,PAY_MASTER B where A.PAYCODE=B.PAYCODE"
        'tProcess = DoTProcess(sSql)
        'frmMdiMain.rptPayroll.ReportFileName = App.Path & "\empexpnmon.rpt"
        'sTitle1 = "Akhilesh='Cost to Company (Employee Wise)'"
        'sTitle2 = "hbank='Bank A/c No'"
        'frmMdiMain.rptPayroll.Formulas(1) = sTitle2
        'sTitle3 = "Esiemployer=" & rsPaysetup.Tables(0).Rows(0).Item("esi") & ""
        'sTitle4 = "Esiemp=" & rsPaysetup.Tables(0).Rows(0).Item("esie") & ""
        'frmMdiMain.rptPayroll.Formulas(2) = sTitle3
        'frmMdiMain.rptPayroll.Formulas(3) = sTitle4



        'ElseIf gReportName = "MasterRep3" Then 'CTC Company Wise
        '    sSql = "truncate table pay_temp"
        '    Cn.Execute(sSql)
        '    If Trim(frmPayOption.mstrSqlEmpTypeClause) = "" Then
        '        sSql = "insert into pay_temp select c.PAYCODE,c.VBASIC,0,c.VDA_RATE,c.VDA_F,0,c.VMED_RATE,c.VMED_F,0,c.VCONV_RATE,c.VCONV_F,0,c.VHRA_RATE,c.VHRA_F,0,0,c.VOT_RATE,c.VOT_FLAG,0,c.VI_1,c.VIT_1,0,c.VI_2,c.VIT_2,0,c.VI_3,c.VIT_3,0,c.VI_4,c.VIT_4,0,c.VI_5,c.VIT_5,0,c.VI_6,c.VIT_6,0,c.VI_7,c.VIT_7,0,c.VI_8,c.VIT_8,0,c.VI_9,c.VIT_9,0,c.VI_10,c.VIT_10,0,c.PF_ALLOWED,c.ESI_ALLOWED,c.VPF_ALLOWED,c.PROF_TAX_ALLOWED,0,0,0,0,0,0,c.VTDS_RATE,c.VTDS_F,0,c.VDT_1,c.VD_1,0,c.VDT_2,c.VD_2,0,c.VDT_3,c.VD_3,0,c.VDT_4,c.VD_4,0,c.VDT_5,c.VD_5,0,c.VDT_6,c.VD_6,0,c.VDT_7,c.VD_7,0,c.VDT_8,c.VD_8,0,c.VDT_9,c.VD_9,0,c.VDT_10,c.VD_10,0,0,0,0,0,0,0,0,0,0,0,0," & NoOfDay(Now()) & ",0,0,0,0,0,'" & Format("01/" & Format(Now(), "mm/yyyy"), "MMM dd yyyy") & "',0,0,0,0,b.vdes_1,b.vdes_2,b.vdes_3,b.vdes_4,b.vdes_5,b.vdes_6,b.vdes_7,b.vdes_8,b.vdes_9,b.vdes_10,b.vIes_1,b.vies_2,b.vies_3,b.vies_4,b.vies_5,b.vies_6,b.vies_7,b.vies_8,b.vies_9,b.vies_10 From Pay_master c,pay_setup b "
        '    Else
        '        sSql = "insert into pay_temp select c.PAYCODE,c.VBASIC,0,c.VDA_RATE,c.VDA_F,0,c.VMED_RATE,c.VMED_F,0,c.VCONV_RATE,c.VCONV_F,0,c.VHRA_RATE,c.VHRA_F,0,0,c.VOT_RATE,c.VOT_FLAG,0,c.VI_1,c.VIT_1,0,c.VI_2,c.VIT_2,0,c.VI_3,c.VIT_3,0,c.VI_4,c.VIT_4,0,c.VI_5,c.VIT_5,0,c.VI_6,c.VIT_6,0,c.VI_7,c.VIT_7,0,c.VI_8,c.VIT_8,0,c.VI_9,c.VIT_9,0,c.VI_10,c.VIT_10,0,c.PF_ALLOWED,c.ESI_ALLOWED,c.VPF_ALLOWED,c.PROF_TAX_ALLOWED,0,0,0,0,0,0,c.VTDS_RATE,c.VTDS_F,0,c.VDT_1,c.VD_1,0,c.VDT_2,c.VD_2,0,c.VDT_3,c.VD_3,0,c.VDT_4,c.VD_4,0,c.VDT_5,c.VD_5,0,c.VDT_6,c.VD_6,0,c.VDT_7,c.VD_7,0,c.VDT_8,c.VD_8,0,c.VDT_9,c.VD_9,0,c.VDT_10,c.VD_10,0,0,0,0,0,0,0,0,0,0,0,0," & NoOfDay(Now()) & ",0,0,0,0,0,'" & Format("01/" & Format(Now(), "mm/yyyy"), "MMM dd yyyy") & "',0,0,0,0,b.vdes_1,b.vdes_2,b.vdes_3,b.vdes_4,b.vdes_5,b.vdes_6,b.vdes_7,b.vdes_8,b.vdes_9,b.vdes_10,b.vIes_1,b.vies_2,b.vies_3,b.vies_4,b.vies_5,b.vies_6,b.vies_7,b.vies_8,b.vies_9,b.vies_10 From Pay_master c,pay_setup b where c.VEmployeeType='" & _
        '                Mid(Trim(frmPayOption.mstrSqlEmpTypeClause), 22, 1) & "'"
        '    End If
        '    Cn.Execute(sSql)
        '    sSql = "select A.*,B.VEmployeeType,B.pfulimit from pay_temp A,PAY_MASTER B where A.PAYCODE=B.PAYCODE"
        '    tProcess = DoTProcess(sSql)
        '    frmMdiMain.rptPayroll.ReportFileName = App.Path & "\ctcsummc.rpt"
        '    sTitle1 = "Akhilesh=' Cost to Company (Company Wise)'"
        '    sTitle2 = "hbank='Bank A/c No'"
        '    frmMdiMain.rptPayroll.Formulas(1) = sTitle2
        '    sTitle3 = "Esiemployer=" & rsPaysetup.Tables(0).Rows(0).Item("esi") & ""
        '    sTitle4 = "Esiemp=" & rsPaysetup.Tables(0).Rows(0).Item("esie") & ""
        '    frmMdiMain.rptPayroll.Formulas(2) = sTitle3
        '    frmMdiMain.rptPayroll.Formulas(3) = sTitle4
        'ElseIf gReportName = "MasterRep4" Then 'CTC Department Wise
        '    sSql = "truncate table pay_temp"
        '    Cn.Execute(sSql)
        '    If Trim(frmPayOption.mstrSqlEmpTypeClause) = "" Then
        '        sSql = "insert into pay_temp select c.PAYCODE,c.VBASIC,0,c.VDA_RATE,c.VDA_F,0,c.VMED_RATE,c.VMED_F,0,c.VCONV_RATE,c.VCONV_F,0,c.VHRA_RATE,c.VHRA_F,0,0,c.VOT_RATE,c.VOT_FLAG,0,c.VI_1,c.VIT_1,0,c.VI_2,c.VIT_2,0,c.VI_3,c.VIT_3,0,c.VI_4,c.VIT_4,0,c.VI_5,c.VIT_5,0,c.VI_6,c.VIT_6,0,c.VI_7,c.VIT_7,0,c.VI_8,c.VIT_8,0,c.VI_9,c.VIT_9,0,c.VI_10,c.VIT_10,0,c.PF_ALLOWED,c.ESI_ALLOWED,c.VPF_ALLOWED,c.PROF_TAX_ALLOWED,0,0,0,0,0,0,c.VTDS_RATE,c.VTDS_F,0,c.VDT_1,c.VD_1,0,c.VDT_2,c.VD_2,0,c.VDT_3,c.VD_3,0,c.VDT_4,c.VD_4,0,c.VDT_5,c.VD_5,0,c.VDT_6,c.VD_6,0,c.VDT_7,c.VD_7,0,c.VDT_8,c.VD_8,0,c.VDT_9,c.VD_9,0,c.VDT_10,c.VD_10,0,0,0,0,0,0,0,0,0,0,0,0," & NoOfDay(Now()) & ",0,0,0,0,0,'" & Format("01/" & Format(Now(), "mm/yyyy"), "MMM dd yyyy") & "',0,0,0,0,b.vdes_1,b.vdes_2,b.vdes_3,b.vdes_4,b.vdes_5,b.vdes_6,b.vdes_7,b.vdes_8,b.vdes_9,b.vdes_10,b.vIes_1,b.vies_2,b.vies_3,b.vies_4,b.vies_5,b.vies_6,b.vies_7,b.vies_8,b.vies_9,b.vies_10 From Pay_master c,pay_setup b "
        '    Else
        '        sSql = "insert into pay_temp select c.PAYCODE,c.VBASIC,0,c.VDA_RATE,c.VDA_F,0,c.VMED_RATE,c.VMED_F,0,c.VCONV_RATE,c.VCONV_F,0,c.VHRA_RATE,c.VHRA_F,0,0,c.VOT_RATE,c.VOT_FLAG,0,c.VI_1,c.VIT_1,0,c.VI_2,c.VIT_2,0,c.VI_3,c.VIT_3,0,c.VI_4,c.VIT_4,0,c.VI_5,c.VIT_5,0,c.VI_6,c.VIT_6,0,c.VI_7,c.VIT_7,0,c.VI_8,c.VIT_8,0,c.VI_9,c.VIT_9,0,c.VI_10,c.VIT_10,0,c.PF_ALLOWED,c.ESI_ALLOWED,c.VPF_ALLOWED,c.PROF_TAX_ALLOWED,0,0,0,0,0,0,c.VTDS_RATE,c.VTDS_F,0,c.VDT_1,c.VD_1,0,c.VDT_2,c.VD_2,0,c.VDT_3,c.VD_3,0,c.VDT_4,c.VD_4,0,c.VDT_5,c.VD_5,0,c.VDT_6,c.VD_6,0,c.VDT_7,c.VD_7,0,c.VDT_8,c.VD_8,0,c.VDT_9,c.VD_9,0,c.VDT_10,c.VD_10,0,0,0,0,0,0,0,0,0,0,0,0," & NoOfDay(Now()) & ",0,0,0,0,0,'" & Format("01/" & Format(Now(), "mm/yyyy"), "MMM dd yyyy") & "',0,0,0,0,b.vdes_1,b.vdes_2,b.vdes_3,b.vdes_4,b.vdes_5,b.vdes_6,b.vdes_7,b.vdes_8,b.vdes_9,b.vdes_10,b.vIes_1,b.vies_2,b.vies_3,b.vies_4,b.vies_5,b.vies_6,b.vies_7,b.vies_8,b.vies_9,b.vies_10 From Pay_master c,pay_setup b where c.VEmployeeType='" & _
        '                Mid(Trim(frmPayOption.mstrSqlEmpTypeClause), 22, 1) & "'"
        '    End If
        '    Cn.Execute(sSql)
        '    sSql = "select A.*,B.VEmployeeType,B.pfulimit from pay_temp A,PAY_MASTER B where A.PAYCODE=B.PAYCODE"
        '    tProcess = DoTProcess(sSql)
        '    frmMdiMain.rptPayroll.ReportFileName = App.Path & "\ctcsummd.rpt"
        '    sTitle1 = "Akhilesh=' Cost to Company (Department Wise)'"
        '    sTitle2 = "hbank='Bank A/c No'"
        '    frmMdiMain.rptPayroll.Formulas(1) = sTitle2
        '    sTitle3 = "Esiemployer=" & rsPaysetup.Tables(0).Rows(0).Item("esi") & ""
        '    sTitle4 = "Esiemp=" & rsPaysetup.Tables(0).Rows(0).Item("esie") & ""
        '    frmMdiMain.rptPayroll.Formulas(2) = sTitle3
        '    frmMdiMain.rptPayroll.Formulas(3) = sTitle4
        'ElseIf gReportName = "MasterRep5" Then 'Reibursment Details
        '    sSql = "truncate table pay_temp"
        '    Cn.Execute(sSql)
        '    If Trim(frmPayOption.mstrSqlEmpTypeClause) = "" Then
        '        sSql = "insert into pay_temp select c.PAYCODE,c.VBASIC,0,c.VDA_RATE,c.VDA_F,0,c.VMED_RATE,c.VMED_F,0,c.VCONV_RATE,c.VCONV_F,0,c.VHRA_RATE,c.VHRA_F,0,0,c.VOT_RATE,c.VOT_FLAG,0,c.VI_1,c.VIT_1,0,c.VI_2,c.VIT_2,0,c.VI_3,c.VIT_3,0,c.VI_4,c.VIT_4,0,c.VI_5,c.VIT_5,0,c.VI_6,c.VIT_6,0,c.VI_7,c.VIT_7,0,c.VI_8,c.VIT_8,0,c.VI_9,c.VIT_9,0,c.VI_10,c.VIT_10,0,c.PF_ALLOWED,c.ESI_ALLOWED,c.VPF_ALLOWED,c.PROF_TAX_ALLOWED,0,0,0,0,0,0,c.VTDS_RATE,c.VTDS_F,0,c.VDT_1,c.VD_1,0,c.VDT_2,c.VD_2,0,c.VDT_3,c.VD_3,0,c.VDT_4,c.VD_4,0,c.VDT_5,c.VD_5,0,c.VDT_6,c.VD_6,0,c.VDT_7,c.VD_7,0,c.VDT_8,c.VD_8,0,c.VDT_9,c.VD_9,0,c.VDT_10,c.VD_10,0,0,0,0,0,0,0,0,0,0,0,0," & NoOfDay(Now()) & ",0,0,0,0,0,'" & Format("01/" & Format(Now(), "mm/yyyy"), "MMM dd yyyy") & "',0,0,0,0,b.vdes_1,b.vdes_2,b.vdes_3,b.vdes_4,b.vdes_5,b.vdes_6,b.vdes_7,b.vdes_8,b.vdes_9,b.vdes_10,b.vIes_1,b.vies_2,b.vies_3,b.vies_4,b.vies_5,b.vies_6,b.vies_7,b.vies_8,b.vies_9,b.vies_10 From Pay_master c,pay_setup b "
        '    Else
        '        sSql = "insert into pay_temp select c.PAYCODE,c.VBASIC,0,c.VDA_RATE,c.VDA_F,0,c.VMED_RATE,c.VMED_F,0,c.VCONV_RATE,c.VCONV_F,0,c.VHRA_RATE,c.VHRA_F,0,0,c.VOT_RATE,c.VOT_FLAG,0,c.VI_1,c.VIT_1,0,c.VI_2,c.VIT_2,0,c.VI_3,c.VIT_3,0,c.VI_4,c.VIT_4,0,c.VI_5,c.VIT_5,0,c.VI_6,c.VIT_6,0,c.VI_7,c.VIT_7,0,c.VI_8,c.VIT_8,0,c.VI_9,c.VIT_9,0,c.VI_10,c.VIT_10,0,c.PF_ALLOWED,c.ESI_ALLOWED,c.VPF_ALLOWED,c.PROF_TAX_ALLOWED,0,0,0,0,0,0,c.VTDS_RATE,c.VTDS_F,0,c.VDT_1,c.VD_1,0,c.VDT_2,c.VD_2,0,c.VDT_3,c.VD_3,0,c.VDT_4,c.VD_4,0,c.VDT_5,c.VD_5,0,c.VDT_6,c.VD_6,0,c.VDT_7,c.VD_7,0,c.VDT_8,c.VD_8,0,c.VDT_9,c.VD_9,0,c.VDT_10,c.VD_10,0,0,0,0,0,0,0,0,0,0,0,0," & NoOfDay(Now()) & ",0,0,0,0,0,'" & Format("01/" & Format(Now(), "mm/yyyy"), "MMM dd yyyy") & "',0,0,0,0,b.vdes_1,b.vdes_2,b.vdes_3,b.vdes_4,b.vdes_5,b.vdes_6,b.vdes_7,b.vdes_8,b.vdes_9,b.vdes_10,b.vIes_1,b.vies_2,b.vies_3,b.vies_4,b.vies_5,b.vies_6,b.vies_7,b.vies_8,b.vies_9,b.vies_10 From Pay_master c,pay_setup b where c.VEmployeeType='" & _
        '                Mid(Trim(frmPayOption.mstrSqlEmpTypeClause), 22, 1) & "'"
        '    End If
        '    Cn.Execute(sSql)
        '    sSql = "select A.*,B.VEmployeeType,B.pfulimit from pay_temp A,PAY_MASTER B where A.PAYCODE=B.PAYCODE"
        '    tProcess = DoTProcess(sSql)
        '    If mReimbstr2 <> "" Then
        '        Cn.Execute(mReimbstr2)
        '    End If
        '    If mReimbstr3 <> "" Then
        '        Cn.Execute("delete from pay_temp where " & mReimbstr3)
        '    End If
        '    frmMdiMain.rptPayroll.ReportFileName = App.Path & "\empreimb.rpt"
        '    sTitle1 = "Akhilesh=' Reimbursment Statement'"
        '    sTitle2 = "hbank='Bank A/c No'"
        '    frmMdiMain.rptPayroll.Formulas(1) = sTitle2
        'ElseIf gReportName = "MasterRep6" Then 'LABOUR WELFARE
        '    akFromDate = DateAdd("m", -6, TxtToDate.Value)
        '    sSql = "select A.PAYCODE FROM TBLEMPLOYEE A ,PAY_MASTER B WHERE A.PAYCODE=B.PAYCODE AND A.DATEOFJOIN BETWEEN 'JAN 01 1900' AND '" & Format(akFromDate, "MMM dd yyyy") & "' AND A.CAT<>'MAN' AND A.CAT<>'SUP' And (A.LeavingDate>='" & Format(akFromDate, "MMM dd yyyy") & "' or A.LeavingDate is null) AND B.VBASIC+B.VDA_RATE<2500"
        '    rsform6 = Cn.Execute(sSql)
        '    If rsform6.RecordCount > 0 Then
        '        sTitle1 = "EMP=" & rsform6.RecordCount
        '        sTitle2 = ""
        '        sSort = ""
        '    Else
        '        MsgBox("No Data for this Report", vbCritical, "Error")
        '    End If
        'End If
    End Sub

    Public Shared totemp As String
    Public Shared totepf As String
    Public Shared totfpf As String
    Public Shared totpf As String
    Public Shared amtonpf As String
    Public Shared pfac21 As String
    
    Public Shared pfac02 As String
    Public Shared pfac22 As String
    Public Shared amt2 As String
    
    Public Shared PFBank As String
    Public Shared Bank As String
    Public Shared Branch As String
    Public Shared Cheqamt As String
    Public Shared CheqDate As String
    Public Shared CheqNo As String
    Public Shared Depositor As String
    Public Shared formonth As String

    Public Shared totemp1 As String
    Public Shared AMTONPF1 As String
    Public Shared AMTONPF2 As String

    Private Sub PF()
        If g_WhereClause <> "" Then
            g_WhereClause = " AND " & g_WhereClause
            g_WhereClause = g_WhereClause.Replace("tblEmployee", "b")
        End If
        If Common.CryReportType = "PFStatement" Then
            sSql = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand("delete from Pay_Temp", Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand("delete from Pay_Temp", Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            sSql = "select * from pay_temp"
            Dim rsform6 As DataSet = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsform6)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsform6)
            End If
            If rsform6.Tables(0).Rows.Count = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>No Data Available<size>", "<size=9>Error</size>")
                Exit Sub
            End If


            For i As Integer = 0 To rsform6.Tables(0).Rows.Count - 1 ' Do While Not rsform6.EOF
                Dim firstpaycode = rsform6.Tables(0).Rows(i)("paycode").ToString.Trim
                sSql = "select * from arrear where paycode='" & firstpaycode & "' and  mon_year between '" & _
                Format(rsform6.Tables(0).Rows(i)("mon_year"), "dd-MMM-yyyy") & "' and '" & _
                Format(rsform6.Tables(0).Rows(i)("mon_year"), "dd-MMM-yyyy") & "'"
                Dim rsArriar As DataSet = New DataSet ' Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsArriar)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsArriar)
                End If
                'DateEditTo.DateTime = DateEditFrom.DateTime.AddMonths(1).AddDays(-1)
                If rsArriar.Tables(0).Rows.Count > 0 Then  'And g_Report_view
                    For j As Integer = 0 To rsArriar.Tables(0).Rows.Count - 1
                        If rsArriar.Tables(0).Rows(j)("paycode") = firstpaycode Then
                            sSql = "update pay_temp set AMTONPF=amtonpf+" & rsArriar.Tables(0).Rows(j)("ARREARPFONAMT") & ",VPF_AMT=vpf_amt+" & rsArriar.Tables(0).Rows(j)("ARREARPF") & ",VFPF_AMT=VFPF_AMT+" & rsArriar.Tables(0).Rows(j)("ARREARFPF_AMT") & ",VEPF_AMT=VEPF_AMT+" & rsArriar.Tables(0).Rows(j)("ARREAREPF_AMT") & " where paycode='" & rsArriar.Tables(0).Rows(j)("paycode") & "' and  mon_year between '" & _
                                Format(rsArriar.Tables(0).Rows(j)("mon_year"), "dd-MMM-yyyy") & "' and '" & _
                                Format(rsArriar.Tables(0).Rows(j)("mon_year"), "dd-MMM-yyyy") & "'"
                            If Common.servername = "Access" Then
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(sSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                        End If                       
                    Next
                    'Do While rsArriar!paycode = firstpaycode
                    '    sSql = "update pay_temp set AMTONPF=amtonpf+" & rsArriar!ARREARPFONAMT & ",VPF_AMT=vpf_amt+" & rsArriar!ARREARPF & ",VFPF_AMT=VFPF_AMT+" & rsArriar!ARREARFPF_AMT & ",VEPF_AMT=VEPF_AMT+" & rsArriar!ARREAREPF_AMT & " where paycode='" & rsArriar!paycode & "' and  mon_year between '" & _
                    '            Format(rsArriar("mon_year"), "dd-MMM-yyyy") & "' and '" & _
                    '            Format(rsArriar("mon_year"), "dd-MMM-yyyy") & "'"
                    '    Cn.Execute(sSql)
                    '    rsArriar.MoveNext()
                    '    If rsArriar.EOF Then
                    '        Exit Do
                    '    End If
                    'Loop
                End If
                'rsform6.MoveNext()
                'If rsform6.EOF Then
                '    Exit Do
            Next
            If Trim(Common.SelectionFormula) = "" Then
                Common.SelectionFormula = "{PAY_TEMP.VPF_AMT} > 0 "
            Else
                Common.SelectionFormula = Common.SelectionFormula & " And {PAY_TEMP.VPF_AMT} > 0 "
            End If
            XtraDocVIewer.ShowDialog()
        ElseIf Common.CryReportType = "PFChalan" Then
            Dim totpf_amt = 0
            Dim totVpf_amt = 0
            Dim mpfamt1 = 0
            Dim mVpfamt1 = 0
            Dim mVpfamt2 = 0
            Dim mpfemployee1 = 0
            Dim mpfemployee2 = 0
            Dim mpfamt3 = 0
            Dim mpfamt2 = 0
            'Dim rsPaysetup As DataSet = New DataSet
            'Dim sSql As String = "select * from PAY_SETUP"
            'If Common.servername = "Access" Then
            '    adapA = New OleDbDataAdapter(sSql, Common.con1)
            '    adapA.Fill(rsPaysetup)
            'Else
            '    adap = New SqlDataAdapter(sSql, Common.con)
            '    adap.Fill(rsPaysetup)
            'End If
            DateEditTo.DateTime = DateEditFrom.DateTime.AddMonths(1).AddDays(-1)
            sSql = "select A.*,D.COMPANYNAME,D.COMPANYADDRESS from Pay_result A,tblemployee B,pay_master C,TBLCOMPANY D where A.paycode=B.paycode and B.paycode=C.paycode AND B.COMPANYCODE=D.COMPANYCODE AND A.VPF_AMT>0 and A.mon_year between '" & _
                DateEditFrom.DateTime.ToString("yyyy-MM-dd") & "' and '" & _
                DateEditTo.DateTime.ToString("yyyy-MM-dd") & "' " & g_WhereClause
            Dim rsform6 As DataSet = New DataSet ' Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsform6)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsform6)
            End If

            If rsform6.Tables(0).Rows.Count > 0 Then
                sSql = "select A.* from arrear A,TBLEMPLOYEE B,PAY_MASTER C where A.PAYCODE=B.PAYCODE AND B.PAYCODE=C.PAYCODE AND A.mon_year between '" & _
                     DateEditFrom.DateTime.ToString("dd/MMM/yyyy") & "' and '" & _
                    DateEditTo.DateTime.ToString("dd/MMM/yyyy") & "'" & g_WhereClause & " order by A.paycode,A.mon_year"
                Dim rsArriar As DataSet = New DataSet ' Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsArriar)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsArriar)
                End If

                'sTitle10 = "COMPESTBL='" & Trim(rsform6("COMPANYNAME")) & "," & Trim(rsform6("COMPANYADDRESS")) & "'"
                sSql = "truncate table pay_summary"
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If

                For i As Integer = 0 To rsform6.Tables(0).Rows.Count - 1 ' Do While Not rsform6.EOF
                    If rsform6.Tables(0).Rows(i)("vpf_amt") > 0 Then
                        mpfamt1 = mpfamt1 + rsform6.Tables(0).Rows(i)("AMTONPF")
                        If rsform6.Tables(0).Rows(i)("AMTONPF") > rsPaysetup.Tables(0).Rows(0)("PFLIMIT") Then
                            mpfamt3 = mpfamt3 + rsPaysetup.Tables(0).Rows(0)("PFLIMIT")
                        Else
                            mpfamt3 = mpfamt3 + rsform6.Tables(0).Rows(i)("AMTONPF")
                        End If
                        ' mpfamt3 = mpfamt3 + rsform6!AMTONPF

                        If rsform6.Tables(0).Rows(i)("vepf_amt") > 0 And rsform6.Tables(0).Rows(i)("vfpf_amt") = 0 Then
                            mpfamt2 = mpfamt2 + rsform6.Tables(0).Rows(i)("AMTONPF")
                            mpfemployee2 = mpfemployee2 + 1
                        End If
                        If rsform6.Tables(0).Rows(i)("vepf_amt") > 0 And rsform6.Tables(0).Rows(i)("vfpf_amt") > 0 Then
                            mVpfamt1 = mVpfamt1 + rsform6.Tables(0).Rows(i)("vepf_amt")
                            'mVpfamt2 = mVpfamt2 + rsform6!vfpf_amt
                        End If
                        mVpfamt2 = mVpfamt2 + rsform6.Tables(0).Rows(i)("vfpf_amt")
                        totpf_amt = totpf_amt + rsform6.Tables(0).Rows(i)("vpf_amt")
                        totVpf_amt = totVpf_amt + rsform6.Tables(0).Rows(i)("VVPF_AMT")
                        mpfemployee1 = mpfemployee1 + 1
                    End If
                    'rsform6.MoveNext()
                    'If rsform6.EOF Then
                    '    Exit Do
                    'End If
                Next '   Loop
                If rsArriar.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To rsArriar.Tables(0).Rows.Count - 1 '  Do While Not rsArriar.EOF
                        If rsArriar.Tables(0).Rows(i)("ARREARPF") > 0 Then
                            mpfamt1 = mpfamt1 + rsArriar.Tables(0).Rows(i)("ARREARPFONAMT")
                            If rsArriar.Tables(0).Rows(i)("ARREAREPF_AMT") > 0 And rsArriar.Tables(0).Rows(i)("ARREARFPF_AMT") > 0 Then
                                mVpfamt1 = mVpfamt1 + rsArriar.Tables(0).Rows(i)("ARREAREPF_AMT")
                                mVpfamt2 = mVpfamt2 + rsArriar.Tables(0).Rows(i)("ARREARFPF_AMT")
                            End If
                            If rsArriar.Tables(0).Rows(i)("ARREARPFONAMT") > rsPaysetup.Tables(0).Rows(0)("PFLIMIT") Then
                                mpfamt3 = mpfamt3 + rsPaysetup.Tables(0).Rows(0)("PFLIMIT")
                            Else
                                mpfamt3 = mpfamt3 + rsArriar.Tables(0).Rows(i)("ARREARPFONAMT")
                            End If
                            If rsArriar.Tables(0).Rows(i)("ARREAREPF_AMT") > 0 And rsArriar.Tables(0).Rows(i)("ARREARFPF_AMT") = 0 Then
                                mpfamt2 = mpfamt2 + rsArriar.Tables(0).Rows(i)("ARREARPFONAMT")
                            End If
                            totpf_amt = totpf_amt + rsArriar.Tables(0).Rows(i)("ARREARPF")
                            totVpf_amt = totVpf_amt + rsArriar.Tables(0).Rows(i)("arrearvpf")
                        End If
                        'rsArriar.MoveNext()
                        'If rsArriar.EOF Then
                        '    Exit Do
                        'End If
                    Next ' Loop
                End If
            End If

            totemp = mpfemployee1
            totepf = mVpfamt1
            totfpf = mVpfamt2
            totpf = totpf_amt
            amtonpf = mpfamt1
            pfac21 = Math.Round((mpfamt3 * rsPaysetup.Tables(0).Rows(0)("pfac21") / 100), 0)
            pfac21 = Math.Round((mpfamt1 * rsPaysetup.Tables(0).Rows(0)("pfac21") / 100), 0)

            pfac02 = Math.Round((mpfamt1 * rsPaysetup.Tables(0).Rows(0)("pfac02") / 100), 0)
            pfac22 = Math.Round((mpfamt3 * rsPaysetup.Tables(0).Rows(0)("pfac22") / 100), 0)
            amt2 = Common.AmtAsText(mVpfamt2 + mVpfamt1 + Math.Round((mpfamt3 * rsPaysetup.Tables(0).Rows(0)("pfac21") / 100), 0) + totpf_amt + Math.Round((mpfamt1 * rsPaysetup.Tables(0).Rows(0)("pfac02") / 100), 0) + Math.Round((mpfamt3 * rsPaysetup.Tables(0).Rows(0)("pfac22") / 100), 0))
            amt2 = Common.AmtAsText(mVpfamt2 + mVpfamt1 + Math.Round((mpfamt1 * rsPaysetup.Tables(0).Rows(0)("pfac21") / 100), 0) + totpf_amt + Math.Round((mpfamt1 * rsPaysetup.Tables(0).Rows(0)("pfac02") / 100), 0) + Math.Round((mpfamt3 * rsPaysetup.Tables(0).Rows(0)("pfac22") / 100), 0))

            PFBank = Common.pfBankName
            Bank = Common.BankName
            Branch = Common.BankBranch
            Cheqamt = Format(Common.ChequeAmt, "0000000000.00")
            CheqDate = Format(Common.ChequeDate, "dd/MM/yyyy")
            CheqNo = Common.ChequeNo
            Depositor = Common.Depositor
            formonth = Format(DateEditFrom.DateTime, "MMMMM-yyyy")

            totemp1 = mpfemployee1 - mpfemployee2
            AMTONPF1 = mpfamt1 - mpfamt2
            AMTONPF2 = mpfamt3
            XtraDocVIewer.ShowDialog()
        End If
    End Sub

    Public Shared Esiemployer As String
    Public Shared Esiemp As String


    Public Shared TOTESI As String = ""
    Public Shared TOTEESI As String = ""
    Public Shared TOTAMT As String = ""
    Public Shared TOTAMTTEXT As String = ""   
    Public Shared AMTONWHICH As String = ""
    Private Sub ESI()
        If g_WhereClause <> "" Then
            g_WhereClause = " AND " & g_WhereClause
            g_WhereClause = g_WhereClause.Replace("tblEmployee", "b")
        End If
        Dim firstpaycode As String = ""
        If Common.CryReportType = "ESItatement" Then

            Dim sSql As String = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand("delete from Pay_Temp", Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand("delete from Pay_Temp", Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

            'Dim rsPaysetup As DataSet = New DataSet
            'sSql = "select * from PAY_SETUP"
            'If Common.servername = "Access" Then
            '    adapA = New OleDbDataAdapter(sSql, Common.con1)
            '    adapA.Fill(rsPaysetup)
            'Else
            '    adap = New SqlDataAdapter(sSql, Common.con)
            '    adap.Fill(rsPaysetup)
            'End If

            sSql = "select * from Pay_Temp"
            Dim rsform6 As DataSet = New DataSet ' Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsform6)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsform6)
            End If
            If rsform6.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To rsform6.Tables(0).Rows.Count - 1 '   Do While Not rsform6.EOF
                    firstpaycode = rsform6.Tables(0).Rows(i)("paycode").ToString.Trim
                    sSql = "select * from arrear where paycode='" & firstpaycode & "' and  mon_year between '" & _
                    Format(rsform6.Tables(0).Rows(i)("mon_year"), "yyyy-MM-dd") & "' and '" & _
                    Format(rsform6.Tables(0).Rows(i)("mon_year"), "yyyy-MM-dd") & "'"
                    'rsArriar = Cn.Execute(sSql)
                    Dim rsArriar As DataSet = New DataSet ' Cn.Execute(sSql)
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(rsArriar)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(rsArriar)
                    End If
                    If rsArriar.Tables(0).Rows.Count > 0 Then  ' And g_Report_view
                        For j As Integer = 0 To rsArriar.Tables(0).Rows.Count - 1 '  Do While rsArriar!paycode = firstpaycode
                            sSql = "update pay_temp set VESI_AMT=VESI_AMT+" & rsArriar.Tables(0).Rows(j)("ARREARESI") & ",AMTONESI=AMTONESI+" & rsArriar.Tables(0).Rows(j)("ARREARESIONAMT") & " where paycode='" & rsArriar.Tables(0).Rows(j)("paycode") & "' and  mon_year between '" & _
                                    Format(rsArriar.Tables(0).Rows(j)("mon_year"), "yyyy-MM-dd") & "' and '" & _
                                    Format(rsArriar.Tables(0).Rows(j)("mon_year"), "yyyy-MM-dd") & "'"
                            'Cn.Execute(sSql)
                            If Common.servername = "Access" Then
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(sSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                            'rsArriar.MoveNext()
                            'If rsArriar.EOF Then
                            '    Exit Do
                            'End If
                        Next ' Loop
                    End If
                    'rsform6.MoveNext()
                    'If rsform6.EOF Then
                    '    Exit Do
                    'End If
                Next ' Loop
            End If

            'frmMdiMain.rptPayroll.ReportFileName = App.Path & "\ESIStatement.rpt"
            'sTitle1 = "Header=' ESI Statement For the Month of " & DateEditTo.DateTime.ToString("MMMM yyyy") & "'"
            If Trim(Common.SelectionFormula) = "" Then
                Common.SelectionFormula = "{PAY_TEMP.VESI_AMT} > 0 "
            Else
                Common.SelectionFormula = g_WhereClause_rpt & " And {PAY_TEMP.VESI_AMT} > 0 "
            End If
            Esiemployer = rsPaysetup.Tables(0).Rows(0)("esi")
            Esiemp = rsPaysetup.Tables(0).Rows(0)("esie")
            XtraDocVIewer.ShowDialog()
        ElseIf Common.CryReportType = "ESIChalan" Then
            Dim sSql As String = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand("delete from Pay_Temp", Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand("delete from Pay_Temp", Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If


            Dim totpre As Double = 0
            Dim TOTSALARY As Double = 0
            Dim totesi_amt As Double = 0
            Dim totEesi_amt As Double = 0
            Dim dbl_EesiAmt As Double
            Dim int_EesiAmt As Integer
            dbl_EesiAmt = 0
           
            sSql = "SELECT * FROM PAY_TEMP WHERE PAY_TEMP.VESI_AMT>0"
            Dim rsform6 As DataSet = New DataSet ' Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsform6)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsform6)
            End If
            'rsform6 = Cn.Execute(sSql)

            DateEditTo.DateTime = DateEditFrom.DateTime.AddMonths(1).AddDays(-1)
            If rsform6.Tables(0).Rows.Count > 0 Then
                totpre = rsform6.Tables(0).Rows.Count
                sSql = "select * from arrear where paycode IN (SELECT PAYCODE FROM PAY_TEMP) AND mon_year between '" & _
                    DateEditFrom.DateTime.ToString("yyyy-MM-dd") & "' and '" & _
                    DateEditTo.DateTime.ToString("yyyy-MM-dd") & "' order by paycode,mon_year"
                'rsArriar = Cn.Execute(sSql)
                Dim rsArriar As DataSet = New DataSet ' Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsArriar)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsArriar)
                End If

                'rsform6.MoveFirst()
                For i As Integer = 0 To rsform6.Tables(0).Rows.Count - 1 '  Do While Not rsform6.EOF
                    TOTSALARY = TOTSALARY + rsform6.Tables(0).Rows(i)("AMTONESI") '+ rsform6!VOT_AMT
                    totesi_amt = totesi_amt + rsform6.Tables(0).Rows(i)("VESI_AMT") ' + rsform6!esionot
                    If rsform6.Tables(0).Rows(i)("VESI_AMT") > 0 Then
                        dbl_EesiAmt = (rsform6.Tables(0).Rows(i)("AMTONESI") * rsPaysetup.Tables(0).Rows(0)("esi")) / 100
                        int_EesiAmt = Int(dbl_EesiAmt)
                        If (dbl_EesiAmt - int_EesiAmt) > 0 Then
                            int_EesiAmt = int_EesiAmt + 1
                        End If
                        totEesi_amt = totEesi_amt + int_EesiAmt
                    End If
                    'rsform6.MoveNext()
                    'If rsform6.EOF Then
                    '    Exit Do
                    'End If
                Next ' Loop
                If rsArriar.Tables(0).Rows.Count > 0 Then 'And g_Report_view 
                    'rsArriar.MoveFirst()
                    For i As Integer = 0 To rsArriar.Tables(0).Rows.Count - 1 '  Do While Not rsArriar.EOF
                        TOTSALARY = TOTSALARY + rsArriar.Tables(0).Rows(i)("ARREARESIONAMT")
                        totesi_amt = totesi_amt + rsArriar.Tables(0).Rows(i)("ARREARESI")
                        If rsArriar.Tables(0).Rows(i)("ARREARESIONAMT") > 0 Then
                            totEesi_amt = totEesi_amt + IIf(((rsArriar.Tables(0).Rows(i)("ARREARESIONAMT") * rsPaysetup.Tables(0).Rows(0)("esi")) / 100) - Int(((rsArriar.Tables(0).Rows(i)("ARREARESIONAMT") * rsPaysetup.Tables(0).Rows(0)("esi")) / 100)) > 0, Int(((rsArriar.Tables(0).Rows(i)("ARREARESIONAMT") * rsPaysetup.Tables(0).Rows(0)("esi")) / 100)) + 1, Int(((rsArriar.Tables(0).Rows(i)("ARREARESIONAMT") * rsPaysetup.Tables(0).Rows(0)("esi")) / 100)))
                        End If
                        'rsArriar.MoveNext()
                        'If rsArriar.EOF Then
                        '    Exit Do
                        'End If
                    Next '  Loop
                End If
            End If

            If Trim(Common.SelectionFormula) = "" Then
                Common.SelectionFormula = "{PAY_TEMP.VESI_AMT} > 0 "
            Else
                Common.SelectionFormula = g_WhereClause_rpt & " And {PAY_TEMP.VESI_AMT} > 0 "
            End If
            TOTESI = totesi_amt
            TOTEESI = totEesi_amt
            TOTAMT = totesi_amt + totEesi_amt
            TOTAMTTEXT = (totesi_amt + totEesi_amt)
            PFBank = Common.pfBankName
            Bank = "drawn in " & Common.BankName
            CheqDate = "Dated " & Format(Common.ChequeDate, "dd/MM/yyyy")
            CheqNo = Common.ChequeNo
            AMTONWHICH = TOTSALARY
            XtraDocVIewer.ShowDialog()
        End If
    End Sub

    Public Shared EMPNO As String = ""
    Public Shared EMPPF As String = ""
    Public Shared EMPESI As String = ""
    Private Sub ArrearRep()
        If g_WhereClause <> "" Then
            g_WhereClause = " AND " & g_WhereClause
            g_WhereClause = g_WhereClause.Replace("tblEmployee", "b")
        End If
        Dim firstpaycode As String = ""
        If Common.CryReportType = "ArrearSlip" Then
            Dim sSql As String = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand("delete from Pay_Temp", Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand("delete from Pay_Temp", Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

            sSql = "truncate table arrearreg"
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

            DateEditTo.DateTime = DateEditFrom.DateTime '.AddMonths(1).AddDays(-1)
            'sSql = "Insert into arrearreg Select a.*,D.vIes_1,D.vies_2,D.vies_3,D.vies_4,D.vies_5,D.vies_6,D.vies_7,D.vies_8,D.vies_9,D.vies_10 From arrear a,TBLEMPLOYEE B,PAY_MASTER C,pay_setup D " & " Where a.paycode in (select paycode from tblemployee where " & g_WhereClause & ") and a.paycode=B.paycode AND B.PAYCODE=C.PAYCODE and DatePart(MM,a.Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy") & g_WhereClause
            sSql = "Insert into arrearreg Select a.*,D.vIes_1,D.vies_2,D.vies_3,D.vies_4,D.vies_5,D.vies_6,D.vies_7,D.vies_8,D.vies_9,D.vies_10 From arrear a,TBLEMPLOYEE B,PAY_MASTER C,pay_setup D " & " Where a.paycode in (select paycode from tblemployee) and a.paycode=B.paycode AND B.PAYCODE=C.PAYCODE and DatePart(MM,a.Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy") & g_WhereClause
            'Cn.Execute(sSql)
            Dim count As Integer
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                count = cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

            If mReimbstr1 <> "" Then
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(mReimbstr1, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(mReimbstr1, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            End If
            If count = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>No Data Available<size>", "<size=9>Error</size>")
                Exit Sub
            End If
            XtraDocVIewer.ShowDialog()
        ElseIf Common.CryReportType = "ArrearReg" Then
            sSql = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand("delete from Pay_Temp", Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand("delete from Pay_Temp", Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            sSql = "truncate table arrearreg"
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

            DateEditTo.DateTime = DateEditFrom.DateTime '.AddMonths(1).AddDays(-1)
            'sSql = "Insert into arrearreg Select a.*,D.vIes_1,D.vies_2,D.vies_3,D.vies_4,D.vies_5,D.vies_6,D.vies_7,D.vies_8,D.vies_9,D.vies_10 From arrear a,TBLEMPLOYEE B,PAY_MASTER C,pay_setup D " & " Where a.paycode in (select paycode from tblemployee where " & gcompclause & " and " & gdeptclause & ") and a.paycode=B.paycode AND B.PAYCODE=C.PAYCODE and DatePart(mm,a.Mon_Year)=" & Format(TxtToDate.Value, "mm") & " And DatePart(yy,a.Mon_Year)=" & Format(TxtToDate.Value, "yyyy") & frmPayOption.mstrSqlEmpTypeClause & whereclausesalreg
            sSql = "Insert into arrearreg Select a.*,D.vIes_1,D.vies_2,D.vies_3,D.vies_4,D.vies_5,D.vies_6,D.vies_7,D.vies_8,D.vies_9,D.vies_10 From arrear a,TBLEMPLOYEE B,PAY_MASTER C,pay_setup D " & " Where a.paycode in (select paycode from tblemployee ) and a.paycode=B.paycode AND B.PAYCODE=C.PAYCODE and DatePart(mm,a.Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy") & g_WhereClause
            'Cn.Execute(sSql)
            Dim count As Integer
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                count = cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            If count = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>No Data Available<size>", "<size=9>Error</size>")
                Exit Sub
            End If
            If mReimbstr1 <> "" Then
                'Cn.Execute(mReimbstr1)
                If mReimbstr1 <> "" Then
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(mReimbstr1, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(mReimbstr1, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                End If
            End If
            'sTitle1 = "Akhilesh=' Arriear Register Paid in the Month of " & Format(TxtToDate.Value, "MMMM yyyy") & "'"
          
            sSql = "SELECT * FROM ARREARREG"
            Dim rsform6 As DataSet = New DataSet ' Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsform6)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsform6)
            End If
            If rsform6.Tables(0).Rows.Count > 0 Then
                EMPNO = Format(rsform6.Tables(0).Rows.Count, "0000")
            End If
            sSql = "SELECT * FROM ARREARREG WHERE ARREARPF>0"
            'rsform6 = Cn.Execute(sSql)
            rsform6 = New DataSet ' Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsform6)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsform6)
            End If
            If rsform6.Tables(0).Rows.Count > 0 Then
                EMPPF = Format(rsform6.Tables(0).Rows.Count, "0000")
            End If
            sSql = "SELECT * FROM ARREARREG WHERE ARREARESI>0"
            'rsform6 = Cn.Execute(sSql)
            rsform6 = New DataSet ' Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsform6)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsform6)
            End If
            If rsform6.Tables(0).Rows.Count > 0 Then
                EMPESI = Format(rsform6.Tables(0).Rows.Count, "0000")
            End If
            XtraDocVIewer.ShowDialog()
            'frmMdiMain.rptPayroll.Formulas(1) = sTitle2
            'frmMdiMain.rptPayroll.Formulas(2) = sTitle3
            'frmMdiMain.rptPayroll.Formulas(3) = sTitle4
        End If

    End Sub
    ''********************************************************************
    ''    This function is use for processing purpose of selected employee
    ''    through TR------- table
    ''********************************************************************
    Private Function DoTProcess(sSql As String) As Boolean
        Dim tmpsSql As String = sSql
        Dim sAdvFn As String
        Dim rsProcess As DataSet = New DataSet 'ADODB.Recordset
        Dim rsAdvance As DataSet = New DataSet 'ADODB.Recordset
        Dim rsFine As DataSet = New DataSet 'ADODB.Recordset
        Dim rsProf As DataSet = New DataSet 'ADODB.Recordset
        Dim sWhichFormula As String '* 355
        Dim sPaycode As String '* 10
        Dim decpos As Integer
        Dim iPFAMT As Double, iFPFAMT As Double, iOtAmt As Double, iHRAAMT As Double, iESIAMT As Double
        Dim iDedAmt1 As Double, iDedAmt2 As Double, iDedAmt3 As Double, iDedAmt4 As Double, iDedAmt5 As Double, iDedAmt6 As Double, iDedAmt7 As Double, iDedAmt8 As Double, iDedAmt9 As Double, iDedAmt10 As Double
        Dim iErnAmt1 As Double, iErnAmt2 As Double, iErnAmt3 As Double, iErnAmt4 As Double, iErnAmt5 As Double, iErnAmt6 As Double, iErnAmt7 As Double, iErnAmt8 As Double, iErnAmt9 As Double, iErnAmt10 As Double
        Dim iSalary As Double, iNetSal As Double, iGrossSal As Double
        Dim BASIC As Double, DA As Double, CONV As Double, MED As Double, PF As Double, FPF As Double, OTRate As Double
        Dim HRA As Double, ESI As Double, Fine As Double, Advance As Double
        Dim PRE As Double, ABS1 As Double, HLD As Double, LATE As Double
        Dim EARLY As Double, OT As Double, CL As Double, SL As Double, PL_EL As Double
        Dim OTHER_LV As Double, LEAVE As Double, TDAYS As Double, T_LATE As Double, T_EARLY As Double
        Dim D_1TOD_10 As Double, E_1TOE_10 As Double, OT_RATE As Double, MON_DAY As Double
        Dim ided1 As Double, ided2 As Double, ided3 As Double, ided4 As Double, ided5 As Double, ided6 As Double, ided7 As Double, ided8 As Double, ided9 As Double, ided10 As Double
        Dim iern1 As Double, iern2 As Double, iern3 As Double, iern4 As Double, iern5 As Double, iern6 As Double, iern7 As Double, iern8 As Double, iern9 As Double, iern10 As Double
        Dim iRESULT As Double
        Dim iWOValue As Integer
        Dim test As Double, mArrearSalary As Double
        Dim rsarrear As DataSet = New DataSet 'ADODB.Recordset
        Dim rsarrearpayday As DataSet = New DataSet 'As ADODB.Recordset
        Dim marrear As Double, marresiamt As Double, marrpfamt As Double, marrearesi As Double, marrearpf As Double, marrearfpf As Double, marrearpfonamt As Double, marrearvpf As Double
        Dim uptodate As Date, arrearpayday As Double, arrearmonday As Double
        Dim amt1_amt As Double, amt2_amt As Double, amt3_amt As Double, amt4_amt As Double, amt5_amt As Double, n As Integer
        Dim conv_amt As Double, medical_amt As Double, tds_amt As Double, prof_tax_amt As Double, amt_on_pf As Double, amt_on_esi As Double, iVPFAMT As Double, iEPFAMT As Double, TDS As Double, PROF_TAX As Double
        'Dim rsPaysetup As DataSet = New DataSet 'ADODB.Recordset
        Dim Reimbconv_amt As Double, Reimmedical_amt As Double, ReimErnAmt1 As Double, ReimErnAmt2 As Double, ReimErnAmt3 As Double, ReimErnAmt4 As Double, ReimErnAmt5 As Double, ReimErnAmt6 As Double, ReimErnAmt7 As Double, ReimErnAmt8 As Double, ReimErnAmt9 As Double, ReimErnAmt10 As Double
        On Error GoTo ErrHand

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        'Try
        'rsProcess = Cn.Execute(sSql)
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsProcess)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsProcess)
        End If
        'sSql = "select * from pay_setup"
        'rsPaysetup = New DataSet
        'If Common.servername = "Access" Then
        '    adapA = New OleDbDataAdapter(sSql, Common.con1)
        '    adapA.Fill(rsPaysetup)
        'Else
        '    adap = New SqlDataAdapter(sSql, Common.con)
        '    adap.Fill(rsPaysetup)
        'End If
        g_pfmaxlimit = rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT")
        For i As Integer = 0 To rsProcess.Tables(0).Rows.Count - 1 '  Do While Not rsProcess.EOF
            iPFAMT = 0 : iFPFAMT = 0 : iOtAmt = 0 : iHRAAMT = 0 : Advance = 0 : Fine = 0 : iDedAmt1 = 0 : iDedAmt2 = 0 : iDedAmt3 = 0 : iDedAmt4 = 0 : iDedAmt5 = 0 : iDedAmt6 = 0 : iDedAmt7 = 0 : iDedAmt8 = 0 : iDedAmt9 = 0 : iDedAmt10 = 0
            iErnAmt1 = 0 : iErnAmt2 = 0 : iErnAmt3 = 0 : iErnAmt4 = 0 : iErnAmt5 = 0 : iErnAmt6 = 0 : iErnAmt7 = 0 : iErnAmt8 = 0 : iErnAmt9 = 0 : iErnAmt10 = 0 : iESIAMT = 0
            conv_amt = 0 : medical_amt = 0 : tds_amt = 0 : prof_tax_amt = 0 : amt_on_pf = 0 : amt_on_esi = 0 : iVPFAMT = 0 : iEPFAMT = 0
            sPaycode = rsProcess.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim : BASIC = rsProcess.Tables(0).Rows(i).Item("VBASIC")
            If rsProcess.Tables(0).Rows(i).Item("VDA_RATE").ToString.Trim = "" Then : DA = 0 : Else : DA = rsProcess.Tables(0).Rows(i).Item("VDA_RATE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VHRA_RATE").ToString.Trim = "" Then : HRA = 0 : Else : HRA = rsProcess.Tables(0).Rows(i).Item("VHRA_RATE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("Vmed_rate").ToString.Trim Then : MED = 0 : Else : MED = rsProcess.Tables(0).Rows(i).Item("Vmed_rate").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("Vconv_rate").ToString.Trim Then : CONV = 0 : Else : CONV = rsProcess.Tables(0).Rows(i).Item("Vconv_rate").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VPRE").ToString.Trim = "" Then : PRE = 0 : Else : PRE = rsProcess.Tables(0).Rows(i).Item("VPRE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VABS").ToString.Trim = "" Then : ABS1 = 0 : Else : ABS1 = rsProcess.Tables(0).Rows(i).Item("VABS").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VHLD").ToString.Trim Then : HLD = 0 : Else : HLD = rsProcess.Tables(0).Rows(i).Item("VHLD") : End If
            If rsProcess.Tables(0).Rows(i).Item("VLATE").ToString.Trim = "" Then : LATE = 0 : Else : LATE = rsProcess.Tables(0).Rows(i).Item("VLATE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VEARLY").ToString.Trim = "" Then : EARLY = 0 : Else : EARLY = rsProcess.Tables(0).Rows(i).Item("VEARLY").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VOT").ToString.Trim = "" Then : OT = 0 : Else : OT = rsProcess.Tables(0).Rows(i).Item("VOT").ToString.Trim : End If
            OT = OT * 60 ' Hr2Min(OT)
            If rsProcess.Tables(0).Rows(i).Item("VCL").ToString.Trim = "" Then : CL = 0 : Else : CL = rsProcess.Tables(0).Rows(i).Item("VCL").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VSL").ToString.Trim = "" Then : SL = 0 : Else : SL = rsProcess.Tables(0).Rows(i).Item("VSL").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VPL_EL").ToString.Trim = "" Then : PL_EL = 0 : Else : PL_EL = rsProcess.Tables(0).Rows(i).Item("VPL_EL").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VOTHER_LV").ToString.Trim = "" Then : OTHER_LV = 0 : Else : OTHER_LV = rsProcess.Tables(0).Rows(i).Item("VOTHER_LV").ToString.Trim : End If
            LEAVE = CL + SL + PL_EL + OTHER_LV
            'iWOValue = IIf(rsProcess.Tables(0).Rows(i).Item("VWO").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(i).Item("VWO").ToString.Trim)
            If rsProcess.Tables(0).Rows(i).Item("VWO").ToString.Trim = "" Then : iWOValue = 0 : Else : iWOValue = rsProcess.Tables(0).Rows(i).Item("VWO").ToString.Trim : End If
            'TDAYS = PRE + HLD + LEAVE + iWOValue
            TDAYS = rsProcess.Tables(0).Rows(i).Item("vtdays")
            If rsProcess.Tables(0).Rows(i).Item("VLATE").ToString.Trim = "" Then : T_LATE = 0 : Else : T_LATE = rsProcess.Tables(0).Rows(i).Item("VLATE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VEARLY").ToString.Trim = "" Then : T_EARLY = 0 : Else : T_EARLY = rsProcess.Tables(0).Rows(i).Item("VEARLY").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("Vtds_rate").ToString.Trim = "" Then : TDS = 0 : Else : TDS = rsProcess.Tables(0).Rows(i).Item("Vtds_rate").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_1").ToString.Trim = "" Then : ided1 = 0 : Else : ided1 = rsProcess.Tables(0).Rows(i).Item("VD_1").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_2").ToString.Trim = "" Then : ided2 = 0 : Else : ided2 = rsProcess.Tables(0).Rows(i).Item("VD_2").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_3").ToString.Trim = "" Then : ided3 = 0 : Else : ided3 = rsProcess.Tables(0).Rows(i).Item("VD_3").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_4").ToString.Trim = "" Then : ided4 = 0 : Else : ided4 = rsProcess.Tables(0).Rows(i).Item("VD_4").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_5").ToString.Trim = "" Then : ided5 = 0 : Else : ided5 = rsProcess.Tables(0).Rows(i).Item("VD_5").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_6").ToString.Trim = "" Then : ided6 = 0 : Else : ided6 = rsProcess.Tables(0).Rows(i).Item("VD_6").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_7").ToString.Trim = "" Then : ided7 = 0 : Else : ided7 = rsProcess.Tables(0).Rows(i).Item("VD_7").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_8").ToString.Trim = "" Then : ided8 = 0 : Else : ided8 = rsProcess.Tables(0).Rows(i).Item("VD_8").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_9").ToString.Trim = "" Then : ided9 = 0 : Else : ided9 = rsProcess.Tables(0).Rows(i).Item("VD_9").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_10").ToString.Trim = "" Then : ided10 = 0 : Else : ided10 = rsProcess.Tables(0).Rows(i).Item("VD_10").ToString.Trim : End If

            If rsProcess.Tables(0).Rows(i).Item("VI_1").ToString.Trim = "" Then : iern1 = 0 : Else : iern1 = rsProcess.Tables(0).Rows(i).Item("VI_1").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_2").ToString.Trim = "" Then : iern2 = 0 : Else : iern2 = rsProcess.Tables(0).Rows(i).Item("VI_2").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_3").ToString.Trim = "" Then : iern3 = 0 : Else : iern3 = rsProcess.Tables(0).Rows(i).Item("VI_3").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_4").ToString.Trim = "" Then : iern4 = 0 : Else : iern4 = rsProcess.Tables(0).Rows(i).Item("VI_4").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_5").ToString.Trim = "" Then : iern5 = 0 : Else : iern5 = rsProcess.Tables(0).Rows(i).Item("VI_5").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_6").ToString.Trim = "" Then : iern6 = 0 : Else : iern6 = rsProcess.Tables(0).Rows(i).Item("VI_6").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_7").ToString.Trim = "" Then : iern7 = 0 : Else : iern7 = rsProcess.Tables(0).Rows(i).Item("VI_7").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_8").ToString.Trim = "" Then : iern8 = 0 : Else : iern8 = rsProcess.Tables(0).Rows(i).Item("VI_8").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_9").ToString.Trim = "" Then : iern9 = 0 : Else : iern9 = rsProcess.Tables(0).Rows(i).Item("VI_9").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_10").ToString.Trim = "" Then : iern10 = 0 : Else : iern10 = rsProcess.Tables(0).Rows(i).Item("VI_10").ToString.Trim : End If

            OT_RATE = rsProcess.Tables(0).Rows(i).Item("VOT_RATE").ToString.Trim
            MON_DAY = Date.DaysInMonth(DateEditTo.DateTime.Year, DateEditTo.DateTime.Month) 'NoOfDay(txtDateTo.Value)
            '******-FOR BASIC SALARY-*************

            If rsPaysetup.Tables(0).Rows(0).Item("BASIC_RND").ToString.Trim = "Y" Then
                iSalary = Math.Round((BASIC / MON_DAY) * TDAYS, 0)
            ElseIf rsProcess.Tables(0).Rows(i).Item("VEmployeeType").ToString.Trim = "P" Then
                iSalary = BASIC
            Else
                iSalary = Math.Round((BASIC / MON_DAY) * TDAYS, 2)
            End If
            '******-FOR DA*************
            If rsProcess.Tables(0).Rows(i).Item("Vda_f").ToString.Trim = "F" Then
                DA = rsProcess.Tables(0).Rows(i).Item("vda_RATE")
            Else
                DA = (rsProcess.Tables(0).Rows(i).Item("vda_RATE") / MON_DAY) * TDAYS
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DA_RND").ToString.Trim = "Y" Then
                DA = Math.Round(DA, 0)
            Else
                DA = Math.Round(DA, 2)
            End If
            '******-FOR HRA -*************
            If rsProcess.Tables(0).Rows(i).Item("VHRA_F").ToString.Trim = "F" Then
                iHRAAMT = rsProcess.Tables(0).Rows(i).Item("VHRA_RATE")
            Else
                iHRAAMT = (rsProcess.Tables(0).Rows(i).Item("vHRA_RATE") / MON_DAY) * TDAYS
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("HRA_RND").ToString.Trim = "Y" Then
                iHRAAMT = Math.Round(iHRAAMT, 0)
            Else
                iHRAAMT = Math.Round(iHRAAMT, 2)
            End If
            '******-FOR CONVEYANCE*************
            If rsProcess.Tables(0).Rows(i).Item("Vconv_f").ToString.Trim = "F" Then
                conv_amt = rsProcess.Tables(0).Rows(i).Item("Vconv_rate")
            Else
                conv_amt = (rsProcess.Tables(0).Rows(i).Item("Vconv_rate") / MON_DAY) * TDAYS
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("CONV_RND").ToString.Trim = "Y" Then
                conv_amt = Math.Round(conv_amt, 0)
            Else
                conv_amt = Math.Round(conv_amt, 2)
            End If
            '******-FOR MEDICAL*************

            If rsProcess.Tables(0).Rows(i).Item("Vmed_f").ToString.Trim = "F" Then
                medical_amt = rsProcess.Tables(0).Rows(i).Item("Vmed_rate")
            Else
                medical_amt = (rsProcess.Tables(0).Rows(i).Item("Vmed_rate") / MON_DAY) * TDAYS
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("MEDICAL_RND") = "Y" Then
                medical_amt = Math.Round(medical_amt, 0)
            Else
                medical_amt = Math.Round(medical_amt, 2)
            End If
            '******-FOR Earning   Formula -**********
            If ((rsProcess.Tables(0).Rows(i).Item("VIT_1").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VIT_1") <> " " And rsProcess.Tables(0).Rows(i).Item("VIT_1").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_1"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt1 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt1 = iern1
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN1_RND") = "Y" Then
                iErnAmt1 = Math.Round(iErnAmt1, 0)
            Else
                iErnAmt1 = Math.Round(iErnAmt1, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VIT_2").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VIT_2") <> " " And rsProcess.Tables(0).Rows(i).Item("VIT_2").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_2"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt2 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt2 = iern2
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN2_RND") = "Y" Then
                iErnAmt2 = Math.Round(iErnAmt2, 0)
            Else
                iErnAmt2 = Math.Round(iErnAmt2, 2)
            End If
            If ((Trim(rsProcess.Tables(0).Rows(i).Item("VIT_3").ToString.Trim <> "")) And rsProcess.Tables(0).Rows(i).Item("VIT_3") <> " " And rsProcess.Tables(0).Rows(i).Item("VIT_3").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_3"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt3 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt3 = iern3
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN3_RND") = "Y" Then
                iErnAmt3 = Math.Round(iErnAmt3, 0)
            Else
                iErnAmt3 = Math.Round(iErnAmt3, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VIT_4").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VIT_4") <> " " And rsProcess.Tables(0).Rows(i).Item("VIT_4").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_4"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt4 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt4 = iern4
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN4_RND") = "Y" Then
                iErnAmt4 = Math.Round(iErnAmt4, 0)
            Else
                iErnAmt4 = Math.Round(iErnAmt4, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VIT_5").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VIT_5") <> " " And rsProcess.Tables(0).Rows(i).Item("VIT_5").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_5"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt5 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt5 = iern5
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN5_RND") = "Y" Then
                iErnAmt5 = Math.Round(iErnAmt5, 0)
            Else
                iErnAmt5 = Math.Round(iErnAmt5, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VIT_6").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VIT_6") <> " " And rsProcess.Tables(0).Rows(i).Item("VIT_6").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_6"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt6 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt6 = iern6
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN6_RND") = "Y" Then
                iErnAmt6 = Math.Round(iErnAmt6, 0)
            Else
                iErnAmt6 = Math.Round(iErnAmt6, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VIT_7").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VIT_7") <> " " And rsProcess.Tables(0).Rows(i).Item("VIT_7").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_7"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt7 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt7 = iern7
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN7_RND") = "Y" Then
                iErnAmt7 = Math.Round(iErnAmt7, 0)
            Else
                iErnAmt7 = Math.Round(iErnAmt7, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VIT_8").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VIT_8") <> " " And rsProcess.Tables(0).Rows(i).Item("VIT_8").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_8"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt8 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt8 = iern8
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN8_RND") = "Y" Then
                iErnAmt8 = Math.Round(iErnAmt8, 0)
            Else
                iErnAmt8 = Math.Round(iErnAmt8, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VIT_9").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VIT_9") <> " " And rsProcess.Tables(0).Rows(i).Item("VIT_9").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_9"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt9 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt9 = iern9
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN9_RND") = "Y" Then
                iErnAmt9 = Math.Round(iErnAmt9, 0)
            Else
                iErnAmt9 = Math.Round(iErnAmt9, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VIT_10").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VIT_10") <> " " And rsProcess.Tables(0).Rows(i).Item("VIT_10").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_10"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt10 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt10 = iern10
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN10_RND") = "Y" Then
                iErnAmt10 = Math.Round(iErnAmt10, 0)
            Else
                iErnAmt10 = Math.Round(iErnAmt10, 2)
            End If
            '******-FOR OT  AMT FORMULA-*************
            iOtAmt = 0
            If (rsProcess.Tables(0).Rows(i).Item("VOT_FLAG").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VOT_FLAG") <> " " Then
                OT = OT
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VOT_FLAG"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iOtAmt = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iOtAmt = OT_RATE
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("OTAMT_RND") = "Y" Then
                iOtAmt = Math.Round(iOtAmt, 0)
            Else
                iOtAmt = Math.Round(iOtAmt, 2)
            End If
            '******-FOR TDS*************
            If (rsProcess.Tables(0).Rows(i).Item("VTDS_F").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VTDS_F") <> " " Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VTDS_F"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    tds_amt = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                tds_amt = rsProcess.Tables(0).Rows(i).Item("VTDS_RATE")
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("TDS_RND") = "Y" Then
                tds_amt = Math.Round(tds_amt, 0)
            Else
                tds_amt = Math.Round(tds_amt, 2)
            End If
            '******-FOR Deduction Formula -**********
            D_1TOD_10 = 0
            If ((rsProcess.Tables(0).Rows(i).Item("VDT_1").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VDT_1") <> " " And rsProcess.Tables(0).Rows(i).Item("VDT_1").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_1"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt1 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt1 = ided1
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED1_RND") = "Y" Then
                iDedAmt1 = Math.Round(iDedAmt1, 0)
            Else
                iDedAmt1 = Math.Round(iDedAmt1, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VDT_2").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VDT_2") <> " " And rsProcess.Tables(0).Rows(i).Item("VDT_2").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_2"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt2 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt2 = ided2
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED2_RND") = "Y" Then
                iDedAmt2 = Math.Round(iDedAmt2, 0)
            Else
                iDedAmt2 = Math.Round(iDedAmt2, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VDT_3").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VDT_3") <> " " And rsProcess.Tables(0).Rows(i).Item("VDT_3").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_3"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt3 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt3 = ided3
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED3_RND") = "Y" Then
                iDedAmt3 = Math.Round(iDedAmt3, 0)
            Else
                iDedAmt3 = Math.Round(iDedAmt3, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VDT_4").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VDT_4") <> " " And rsProcess.Tables(0).Rows(i).Item("VDT_4").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_4"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt4 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt4 = ided4
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED4_RND") = "Y" Then
                iDedAmt4 = Math.Round(iDedAmt4, 0)
            Else
                iDedAmt4 = Math.Round(iDedAmt4, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VDT_5").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VDT_5") <> " " And rsProcess.Tables(0).Rows(i).Item("VDT_5").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_5"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt5 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt5 = ided5
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED5_RND") = "Y" Then
                iDedAmt5 = Math.Round(iDedAmt5, 0)
            Else
                iDedAmt5 = Math.Round(iDedAmt5, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VDT_6").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VDT_6") <> " " And rsProcess.Tables(0).Rows(i).Item("VDT_6").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_6"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt6 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt6 = ided6
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED6_RND") = "Y" Then
                iDedAmt6 = Math.Round(iDedAmt6, 0)
            Else
                iDedAmt6 = Math.Round(iDedAmt6, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VDT_7").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VDT_7") <> " " And rsProcess.Tables(0).Rows(i).Item("VDT_7").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_7"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt7 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt7 = ided7
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED7_RND") = "Y" Then
                iDedAmt7 = Math.Round(iDedAmt7, 0)
            Else
                iDedAmt7 = Math.Round(iDedAmt7, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VDT_8").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VDT_8") <> " " And rsProcess.Tables(0).Rows(i).Item("VDT_8").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_8"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt8 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt8 = ided8
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED8_RND") = "Y" Then
                iDedAmt8 = Math.Round(iDedAmt8, 0)
            Else
                iDedAmt8 = Math.Round(iDedAmt8, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VDT_9").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VDT_9") <> " " And rsProcess.Tables(0).Rows(i).Item("VDT_9").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_9"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt9 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt9 = ided9
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED9_RND") = "Y" Then
                iDedAmt9 = Math.Round(iDedAmt9, 0)
            Else
                iDedAmt9 = Math.Round(iDedAmt9, 2)
            End If
            If ((rsProcess.Tables(0).Rows(i).Item("VDT_10").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VDT_10") <> " " And rsProcess.Tables(0).Rows(i).Item("VDT_10").ToString.Trim <> "0") Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_10"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt10 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt10 = ided10
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED10_RND") = "Y" Then
                iDedAmt10 = Math.Round(iDedAmt10, 0)
            Else
                iDedAmt10 = Math.Round(iDedAmt10, 2)
            End If
            '******-FOR PF AMT FORMULA-**************
            If rsProcess.Tables(0).Rows(i).Item("PF_ALLOWED") = "Y" Then
                For n = 1 To Len(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")))
                    If n = 1 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iSalary
                    End If
                    If n = 2 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iHRAAMT
                    End If
                    If n = 3 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + DA
                    End If
                    If n = 4 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + conv_amt
                    End If
                    If n = 5 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + medical_amt
                    End If
                    If n = 6 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt1
                    End If
                    If n = 7 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt2
                    End If
                    If n = 8 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt3
                    End If
                    If n = 9 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt4
                    End If
                    If n = 10 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt5
                    End If
                    If n = 11 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt6
                    End If
                    If n = 12 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt7
                    End If
                    If n = 13 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt8
                    End If
                    If n = 14 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt9
                    End If
                    If n = 15 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt10
                    End If
                Next n
                If rsProcess.Tables(0).Rows(i).Item("pfulimit") = "Y" Then
                    If amt_on_pf > rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT") Then
                        amt_on_pf = rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT")
                    End If
                End If
                'iPFAMT = Math.Round((amt_on_pf * Convert.ToDouble(rsPaysetup.Tables(0).Rows(0).Item("PF"))) / 100, rsPaysetup.Tables(0).Rows(0).Item("PFRND"))
                iPFAMT = Math.Round((amt_on_pf * Convert.ToDouble(rsPaysetup.Tables(0).Rows(0).Item("PF"))) / 100)
                'iEPFAMT = Math.Round((IIf(amt_on_pf > Convert.ToDouble(rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT")), Convert.ToDouble(rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT")), amt_on_pf) * Convert.ToDouble(rsPaysetup.Tables(0).Rows(0).Item("EPF"))) / 100, rsPaysetup.Tables(0).Rows(0).Item("PFRND"))
                iEPFAMT = Math.Round((IIf(amt_on_pf > Convert.ToDouble(rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT")), Convert.ToDouble(rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT")), amt_on_pf) * Convert.ToDouble(rsPaysetup.Tables(0).Rows(0).Item("EPF"))) / 100)
                iFPFAMT = iPFAMT - iEPFAMT
            End If
            '******-FOR ESI AMT FORMULA-*************
            If rsProcess.Tables(0).Rows(i).Item("ESI_ALLOWED") = "Y" Then
                For n = 1 To Len(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")))
                    If n = 1 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iSalary
                    End If
                    If n = 2 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iHRAAMT
                    End If
                    If n = 3 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + DA
                    End If
                    If n = 4 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + conv_amt
                    End If
                    If n = 5 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + medical_amt
                    End If
                    If n = 6 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt1
                    End If
                    If n = 7 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt2
                    End If
                    If n = 8 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt3
                    End If
                    If n = 9 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt4
                    End If
                    If n = 10 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt5
                    End If
                    If n = 11 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt6
                    End If
                    If n = 12 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt7
                    End If
                    If n = 13 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt8
                    End If
                    If n = 14 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt9
                    End If
                    If n = 15 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt10
                    End If
                Next n
                If (Format(rsProcess.Tables(0).Rows(i).Item("MON_YEAR"), "MM") = "04" Or Format(rsProcess.Tables(0).Rows(i).Item("MON_YEAR"), "MM") = "10") And amt_on_esi > rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT") Then
                    amt_on_esi = 0
                    iESIAMT = 0
                Else
                    iESIAMT = IIf(amt_on_esi > rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT"), 0, (amt_on_esi * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100)
                    iESIAMT = Math.Round(iESIAMT, 0) + IIf(iESIAMT - Math.Round(iESIAMT, 0) > 0, 1, 0)
                    'iESIAMT = IIf(amt_on_esi > rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT"), math.round((rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT") * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100, rsPaysetup.Tables(0).Rows(0).Item("ESIRND")), math.round((amt_on_esi * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100, rsPaysetup.Tables(0).Rows(0).Item("ESIRND")))
                End If
            End If
            Advance = 0
            Fine = 0
            D_1TOD_10 = iDedAmt1 + iDedAmt2 + iDedAmt3 + iDedAmt4 + iDedAmt5 + iDedAmt6 + iDedAmt7 + iDedAmt8 + iDedAmt9 + iDedAmt10
            E_1TOE_10 = iErnAmt1 + iErnAmt2 + iErnAmt3 + iErnAmt4 + iErnAmt5 + iErnAmt6 + iErnAmt7 + iErnAmt8 + iErnAmt9 + iErnAmt10
            iGrossSal = iSalary + DA + iHRAAMT + iOtAmt + E_1TOE_10 + conv_amt + medical_amt
            '******-FOR PROFESSIONAL TAX*************
            If rsProcess.Tables(0).Rows(i).Item("PROF_TAX_ALLOWED") = "Y" Then
                sSql = "select * from professionaltax where lowerlimit<=" & iGrossSal & " and upperlimit>=" & iGrossSal
                rsProf = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsProf)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsProf)
                End If
                If rsProf.Tables(0).Rows.Count > 0 Then
                    prof_tax_amt = rsProf.Tables(0).Rows(0).Item("taxamount")
                End If
                If rsPaysetup.Tables(0).Rows(0).Item("PROF_TAX_RND") = "Y" Then
                    prof_tax_amt = Math.Round(prof_tax_amt, 0)
                Else
                    prof_tax_amt = Math.Round(prof_tax_amt, 2)
                End If
            End If
            iNetSal = iGrossSal - (D_1TOD_10 + Fine + Advance + iESIAMT + iPFAMT + iFPFAMT + tds_amt + prof_tax_amt)
            sSql = "Update pay_temp Set AMTONPF=" & amt_on_pf & ",VPF_AMT=" & iPFAMT & ",VEPF_AMT=" & iEPFAMT & ",VFPF_AMT=" & iFPFAMT & ",VVPF_AMT=" & iVPFAMT & _
                     ",VTDS_AMT=" & tds_amt & ",PROF_TAX_AMT=" & prof_tax_amt & ",VHRA_AMT=" & iHRAAMT & ",VCONV_AMT=" & conv_amt & ",VMED_AMT=" & medical_amt & ",AMTONESI=" & amt_on_esi & ",ESIONOT=" & IIf(iESIAMT > 0, Math.Round((iOtAmt * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100, 0) + IIf((iOtAmt * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100 - Math.Round((iOtAmt * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100, 0) > 0, 1, 0), 0) & ",VESI_AMT=" & iESIAMT & ",vot_amt=" & iOtAmt & _
                     ",VFINE=" & Fine & ",VADVANCE=" & Advance & ",VDA_AMT=" & DA & _
                     ",VD_1_AMT=" & iDedAmt1 & ",VD_2_AMT=" & iDedAmt2 & _
                     ",VD_3_AMT=" & iDedAmt3 & ",VD_4_AMT=" & iDedAmt4 & _
                     ",VD_5_AMT=" & iDedAmt5 & ",VD_6_AMT=" & iDedAmt6 & _
                     ",VD_7_AMT=" & iDedAmt7 & ",VD_8_AMT=" & iDedAmt8 & _
                     ",VD_9_AMT=" & iDedAmt9 & ",VD_10_AMT=" & iDedAmt10 & _
                     ",VI_1_AMT=" & iErnAmt1 & ",VI_2_AMT=" & iErnAmt2 & _
                     ",VI_3_AMT=" & iErnAmt3 & ",VI_4_AMT=" & iErnAmt4 & _
                     ",VI_5_AMT=" & iErnAmt5 & ",VI_6_AMT=" & iErnAmt6 & _
                     ",VI_7_AMT=" & iErnAmt7 & ",VI_8_AMT=" & iErnAmt8 & _
                     ",VI_9_AMT=" & iErnAmt9 & ",VI_10_AMT=" & iErnAmt10 & _
                     ",VSALARY=" & iSalary & ",VTDAYS=" & TDAYS & " Where Paycode='" & Trim(sPaycode) & "'"
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            'rsProcess.MoveNext()
        Next '  Loop
        'sSql = "delete from pay_result where mon_year='" & Format("01/" & Format(txtDateTo, "mm/yyyy"), "dd/MMM/yyyy") & "' and paycode not in (select paycode from Pay_master)"
        'Cn.Execute (sSql)
        rsProcess = Nothing
        rsAdvance = Nothing
        rsFine = Nothing
        DoTProcess = True
        Exit Function
ErrHand:
        'Screen.MousePointer = vbNormal
        'MsgBox(Err.Number & " --> AKDoProcess " & Chr(13) & Err.Description)
        Resume Next
        rsProcess = Nothing
        rsAdvance = Nothing
        rsFine = Nothing
        DoTProcess = False
    End Function
    ''****************************************************************
    ''    This function is use for checking "If" statement in formula.
    ''****************************************************************
    Function ProcFormula(sFORMULA As String) As Object
        Dim rsFormula As DataSet = New DataSet 'ADODB.Recordset
        Dim sSql As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        'On Error GoTo ErrHand
        sSql = "Select CODE,FORM From Pay_Formula Where Code='" & sFORMULA & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsFormula)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsFormula)
        End If
        'rsFormula = Cn.Execute(sSql)
        If rsFormula.Tables(0).Rows.Count = 1 Then 'RecordCount = 1 Then
            ProcFormula = Trim(rsFormula.Tables(0).Rows(0).Item("FORM").ToString.Trim)
        Else
            ProcFormula = ""
        End If
        Return ProcFormula
        'Exit Function
        'ErrHand:
        '        MsgBox(Err.Number & " -> AKProcFormula " & Chr(13) & Err.Description, vbCritical)
    End Function
    ''**************************************************************************************
    ''    This function is use for calculating Division,Multiplication,Addition,Subtraction
    ''    in a formula.
    ''**************************************************************************************
    Private Sub YearlyRep()
        If Common.CryReportType = "CheckEdidFullNFinal" Then
            Common.SelectionFormula = g_WhereClause_rpt
            'sSql = "Insert into Pay_Temp Select a.*,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From Pay_result a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND DatePart(mm,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yyyy,a.Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy") & g_WhereClause
            'If Common.servername = "Access" Then
            '    If Common.con1.State <> ConnectionState.Open Then
            '        Common.con1.Open()
            '    End If
            '    cmd1 = New OleDbCommand("delete from Pay_Temp", Common.con1)
            '    cmd1.ExecuteNonQuery()
            '    cmd1 = New OleDbCommand(sSql, Common.con1)
            '    cmd1.ExecuteNonQuery()
            '    If Common.con1.State <> ConnectionState.Closed Then
            '        Common.con1.Close()
            '    End If
            'Else
            '    If Common.con.State <> ConnectionState.Open Then
            '        Common.con.Open()
            '    End If
            '    cmd = New SqlCommand("delete from Pay_Temp", Common.con)
            '    cmd.ExecuteNonQuery()
            '    cmd = New SqlCommand(sSql, Common.con)
            '    cmd.ExecuteNonQuery()
            '    If Common.con.State <> ConnectionState.Closed Then
            '        Common.con.Close()
            '    End If
            'End If
            XtraDocVIewer.ShowDialog()
        ElseIf Common.CryReportType = "CheckEditPendingReEm" Then
            If g_WhereClause <> "" Then
                g_WhereClause = " and " & g_WhereClause
            End If
            sSql = "Insert into Pay_Temp Select a.PAYCODE,0,0,0,'',0,0,'',a.vmed_amt,0,'',a.vconv_amt,0,'',0,0,0,'',0,0,'',a.VI_1_AMT,0,'',a.VI_2_AMT,0,'',a.VI_3_AMT,0,'',a.VI_4_AMT,0,'',a.VI_5_AMT,0,'',a.VI_6_AMT,0,'',a.VI_7_AMT,0,'',a.VI_8_AMT,0,'',a.VI_9_AMT,0,'',a.VI_10_AMT,'','','','',0,0,0,0,0,0,0,'',0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,A.MON_YEAR,0,0,0,0,d.vdes_1,d.vdes_2,d.vdes_3,d.vdes_4,d.vdes_5,d.vdes_6,d.vdes_7,d.vdes_8,d.vdes_9,d.vdes_10,d.vIes_1,d.vies_2,d.vies_3,d.vies_4,d.vies_5,d.vies_6,d.vies_7,d.vies_8,d.vies_9,d.vies_10 From PAY_REIMURSH a,tblemployee b, PAY_MASTER C,pay_setup d " & " Where A.PAYCODE=b.PAYCODE AND B.PAYCODE=C.PAYCODE AND a.PAID='N' " & g_WhereClause.Replace("tblEmployee", "a")
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand("delete from Pay_Temp", Common.con1)
                cmd1.ExecuteNonQuery()
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand("delete from Pay_Temp", Common.con)
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            hbank = "Bank A/c No"
            XtraDocVIewer.ShowDialog()
        ElseIf Common.CryReportType = "CheckEditLEaveEncash" Then
            Common.SelectionFormula = g_WhereClause_rpt
            XtraDocVIewer.ShowDialog()
        End If

    End Sub
    Public Function FormulaCal(a As String, BASIC As Double, DA As Double, HRA As Double, PF As Double, FPF As Double, ESI As Double, PRE As Double, ABS1 As Double, HLD As Double, LATE As Double, EARLY As Double, OT As Double, CL As Double, SL As Double, PL_EL As Double, OTHER_LV As Double, LEAVE As Double, TDAYS As Double, T_LATE As Double, T_EARLY As Double, OT_RATE As Double, MON_DAY As Double, ided1 As Double, ided2 As Double, ided3 As Double, ided4 As Double, ided5 As Double, ided6 As Double, ided7 As Double, ided8 As Double, ided9 As Double, ided10 As Double, iern1 As Double, iern2 As Double, iern3 As Double, iern4 As Double, iern5 As Double, iern6 As Double, iern7 As Double, iern8 As Double, iern9 As Double, iern10 As Double, iSalary As Double, iHRAAMT As Double, iOtAmt As Double, iErnAmt1 As Double, iErnAmt2 As Double, iErnAmt3 As Double, iErnAmt4 As Double, iErnAmt5 As Double, iErnAmt6 As Double, iErnAmt7 As Double, iErnAmt8 As Double, iErnAmt9 As Double, iErnAmt10 As Double, _
        TDS As Double, PROF_TAX As Double, CONV As Double, MED As Double) As Double
        Dim B As String
        Dim Operand As String '* 1
        Dim iLeftPos1 As Integer
        Dim iRightPos1 As Integer
        Dim iPos As Integer, iPos1 As Integer, iPos2 As Integer
        Dim iLen As Integer
        Dim AK As Integer
        Dim ak1, AK2, AK3, AK4
        Dim bAkIf As Boolean
        Dim mValue As Double
        On Error GoTo ErrHand
        ak1 = FormulaValueSet(a, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
        mValue = New System.Data.DataTable().Compute(ak1, Nothing) 'Trim(ak1) ' frmCapture.ScriptControl1.Eval(Trim(ak1))
        FormulaCal = mValue
        Exit Function
ErrHand:
        XtraMessageBox.Show(ulf, "<size=10>" & Err.Number & "  -> AKFormulaCal " & Chr(13) & Err.Description & "</size>", "<size=9>Error<siez>")
    End Function
    Function FormulaValueSet(a As String, BASIC As Double, DA As Double, HRA As Double, PF As Double, FPF As Double, ESI As Double, PRE As Double, ABS1 As Double, HLD As Double, LATE As Double, EARLY As Double, OT As Double, CL As Double, SL As Double, PL_EL As Double, OTHER_LV As Double, LEAVE As Double, TDAYS As Double, T_LATE As Double, T_EARLY As Double, OT_RATE As Double, MON_DAY As Double, ided1 As Double, ided2 As Double, ided3 As Double, ided4 As Double, ided5 As Double, ided6 As Double, ided7 As Double, ided8 As Double, ided9 As Double, ided10 As Double, iern1 As Double, iern2 As Double, iern3 As Double, iern4 As Double, iern5 As Double, iern6 As Double, iern7 As Double, iern8 As Double, iern9 As Double, iern10 As Double, iSalary As Double, iHRAAMT As Double, iOtAmt As Double, iErnAmt1 As Double, iErnAmt2 As Double, iErnAmt3 As Double, iErnAmt4 As Double, iErnAmt5 As Double, iErnAmt6 As Double, iErnAmt7 As Double, iErnAmt8 As Double, iErnAmt9 As Double, iErnAmt10 As Double, _
TDS As Double, PROF_TAX As Double, CONV As Double, MED As Double) As String

        Dim mform As DataSet = New DataSet 'ADODB.Recordset
        Dim bm As String, a1 As String, m As Integer, mifChk As Boolean
        Dim St As String, mCtr As Integer, Rt As String, Tstring As String, Fstring As String, lCnt As Integer, Pcnt As Integer
        bm = a
        a = a & " "
        'On Error GoTo ErrHand
a:      If InStr(1, UCase(a), "#A#") <> 0 Then
            'd = InStr(1, UCase(a), "#A#")
            'sSql = "select * from pay_formula where code='A'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "A") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#A#") <> 0 Then GoTo a
        End If

B:      If InStr(1, UCase(a), "#B#") <> 0 Then
            'd = InStr(1, UCase(a), "#B#")
            'sSql = "select * from pay_formula where code='B'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "B") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#B#") <> 0 Then GoTo B
        End If
C:      If InStr(1, UCase(a), "#C#") <> 0 Then
            'd = InStr(1, UCase(a), "#C#")
            'sSql = "select * from pay_formula where code='C'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "C") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#C#") <> 0 Then GoTo C
        End If

d:      If InStr(1, UCase(a), "#D#") <> 0 Then
            'd = InStr(1, UCase(a), "#D#")
            'sSql = "select * from pay_formula where code='D'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "D") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#D#") <> 0 Then GoTo d
        End If

e:      If InStr(1, UCase(a), "#E#") <> 0 Then
            'd = InStr(1, UCase(a), "#E#")
            'sSql = "select * from pay_formula where code='E'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "E") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#E#") <> 0 Then GoTo e
        End If

f:      If InStr(1, UCase(a), "#F#") <> 0 Then
            'd = InStr(1, UCase(a), "#F#")
            'sSql = "select * from pay_formula where code='F'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "F") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#F#") <> 0 Then GoTo f
        End If

G:      If InStr(1, UCase(a), "#G#") <> 0 Then
            'd = InStr(1, UCase(a), "#G#")
            'sSql = "select * from pay_formula where code='G'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "G") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#G#") <> 0 Then GoTo G
        End If

h:      If InStr(1, UCase(a), "#H#") <> 0 Then
            'd = InStr(1, UCase(a), "#H#")
            'sSql = "select * from pay_formula where code='H'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "H") ' fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#H#") <> 0 Then GoTo h
        End If

i:      If InStr(1, UCase(a), "#I#") <> 0 Then
            'd = InStr(1, UCase(a), "#I#")
            'sSql = "select * from pay_formula where code='I'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "I") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#I#") <> 0 Then GoTo i
        End If

j:      If InStr(1, UCase(a), "#J#") <> 0 Then
            'd = InStr(1, UCase(a), "#J#")
            'sSql = "select * from pay_formula where code='J'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "J") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#J#") <> 0 Then GoTo j
        End If

k:      If InStr(1, UCase(a), "#K#") <> 0 Then
            'd = InStr(1, UCase(a), "#K#")
            'sSql = "select * from pay_formula where code='K'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "K") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#K#") <> 0 Then GoTo k
        End If

l:      If InStr(1, UCase(a), "#L#") <> 0 Then
            'd = InStr(1, UCase(a), "#L#")
            'sSql = "select * from pay_formula where code='L'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "L") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#L#") <> 0 Then GoTo l
        End If

m:      If InStr(1, UCase(a), "#M#") <> 0 Then
            'd = InStr(1, UCase(a), "#M#")
            'sSql = "select * from pay_formula where code='M'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "M") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#M#") <> 0 Then GoTo m
        End If

n:      If InStr(1, UCase(a), "#N#") <> 0 Then
            'd = InStr(1, UCase(a), "#N#")
            'sSql = "select * from pay_formula where code='N'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "N") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#N#") <> 0 Then GoTo n
        End If

o:      If InStr(1, UCase(a), "#O#") <> 0 Then
            'd = InStr(1, UCase(a), "#O#")
            'sSql = "select * from pay_formula where code='O'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "O") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#O#") <> 0 Then GoTo o
        End If

p:      If InStr(1, UCase(a), "#P#") <> 0 Then
            'd = InStr(1, UCase(a), "#P#")
            'sSql = "select * from pay_formula where code='P'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "P") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#P#") <> 0 Then GoTo p
        End If

q:      If InStr(1, UCase(a), "#Q#") <> 0 Then
            'd = InStr(1, UCase(a), "#Q#")
            'sSql = "select * from pay_formula where code='Q'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "Q") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#Q#") <> 0 Then GoTo q
        End If

R:      If InStr(1, UCase(a), "#R#") <> 0 Then
            'd = InStr(1, UCase(a), "#R#")
            'sSql = "select * from pay_formula where code='R'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "R") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#R#") <> 0 Then GoTo R
        End If

s:      If InStr(1, UCase(a), "#S#") <> 0 Then
            'd = InStr(1, UCase(a), "#S#")
            'sSql = "select * from pay_formula where code='S'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "S") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#S#") <> 0 Then GoTo s
        End If

t:      If InStr(1, UCase(a), "#T#") <> 0 Then
            'd = InStr(1, UCase(a), "#T#")
            'sSql = "select * from pay_formula where code='T'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "T") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#T#") <> 0 Then GoTo t
        End If

u:      If InStr(1, UCase(a), "#U#") <> 0 Then
            'd = InStr(1, UCase(a), "#U#")
            'sSql = "select * from pay_formula where code='U'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "U") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#U#") <> 0 Then GoTo u
        End If

v:      If InStr(1, UCase(a), "#V#") <> 0 Then
            'd = InStr(1, UCase(a), "#V#")
            'sSql = "select * from pay_formula where code='V'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "V") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#V#") <> 0 Then GoTo v
        End If

W:      If InStr(1, UCase(a), "#W#") <> 0 Then
            'd = InStr(1, UCase(a), "#W#")
            'sSql = "select * from pay_formula where code='W'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "W") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#W#") <> 0 Then GoTo W
        End If

X:      If InStr(1, UCase(a), "#X#") <> 0 Then
            'd = InStr(1, UCase(a), "#X#")
            'sSql = "select * from pay_formula where code='X'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "X") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#X#") <> 0 Then GoTo X
        End If

y:      If InStr(1, UCase(a), "#Y#") <> 0 Then
            'd = InStr(1, UCase(a), "#Y#")
            'sSql = "select * from pay_formula where code='Y'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "Y") ' fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#Y#") <> 0 Then GoTo y
        End If

z:      If InStr(1, UCase(a), "#Z#") <> 0 Then
            'd = InStr(1, UCase(a), "#Z#")
            'sSql = "select * from pay_formula where code='Z'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "Z") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#Z#") <> 0 Then GoTo z
        End If

1:      If InStr(1, UCase(a), "BASIC") <> 0 Then
            a = Regex.Replace(a, "\bBASIC\b", Format(BASIC, "000000"))
        End If
2:      If InStr(1, UCase(a), "HRA") <> 0 Then
            a = Regex.Replace(a, "\bHRA\b", Format(HRA, "000000"))
        End If
3:      If InStr(1, UCase(a), "DA") <> 0 Then
            a = Regex.Replace(a, "\bDA\b", Format(DA, "000000"))
        End If
4:      If InStr(1, UCase(a), "PF") <> 0 Then
            a = Regex.Replace(a, "\bPF\b", Format(PF, "00.00"))
        End If
5:      If InStr(1, UCase(a), "FPF") <> 0 Then
            a = Regex.Replace(a, "\bFPF\b", Format(FPF, "00.00"))
        End If
6:      If InStr(1, UCase(a), "ESI") <> 0 Then
            a = Regex.Replace(a, "\bESI\b", Format(ESI, "00.00"))
        End If
7:      If InStr(1, UCase(a), "PRE") <> 0 Then
            a = Regex.Replace(a, "\bPRE\b", Format(PRE, "00.00"))
        End If
8:      If InStr(1, UCase(a), "ABS") <> 0 Then
            a = Regex.Replace(a, "\bABS\b", Format(ABS1, "00.00"))
        End If
9:      If InStr(1, UCase(a), "HLD") <> 0 Then
            a = Regex.Replace(a, "\bHLD\b", Format(HLD, "00.00"))
        End If
10:     If InStr(1, UCase(a), "LATE") <> 0 Then
            a = Regex.Replace(a, "\bLATE\b", Format(LATE, "0000"))
        End If
11:     If InStr(1, UCase(a), "EARLY") <> 0 Then
            a = Regex.Replace(a, "\bEARLY\b", Format(EARLY, "0000"))
        End If
12:     If InStr(1, UCase(a), "O.T.") <> 0 Then
            a = Regex.Replace(a, "\bO.T.\b", Format(OT, "000.00"))
        End If
13:     If InStr(1, UCase(a), "C.L.") <> 0 Then
            a = Regex.Replace(a, "\bC.L.\b", Format(CL, "00.00"))
        End If
14:     If InStr(1, UCase(a), "S.L.") <> 0 Then
            a = Regex.Replace(a, "\bS.L.\b", Format(SL, "00.00"))
        End If
15:     If InStr(1, UCase(a), "PL_EL") <> 0 Then
            a = Regex.Replace(a, "\bPL_EL\b", Format(PL_EL, "00.00"))
        End If
16:     If InStr(1, UCase(a), "OTHER_LV") <> 0 Then
            a = Regex.Replace(a, "\bOTHER_LV\b", Format(OTHER_LV, "00.00"))
        End If
17:     If InStr(1, UCase(a), "LEAVE") <> 0 Then
            a = Regex.Replace(a, "\bLEAVE\b", Format(LEAVE, "00.00"))
        End If
18:     If InStr(1, UCase(a), "TDAYS") <> 0 Then
            a = Regex.Replace(a, "\bTDAYS\b", Format(TDAYS, "00.00"))
        End If
19:     If InStr(1, UCase(a), "T_LATE") <> 0 Then
            a = Regex.Replace(a, "\bT_LATE\b", Format(T_LATE, "000.00"))
        End If
20:     If InStr(1, UCase(a), "T_EARLY") <> 0 Then
            a = Regex.Replace(a, "\bT_EARLY\b", Format(T_EARLY, "000.00"))
        End If
21:     If InStr(1, UCase(a), "EARN_1") <> 0 Then
            a = Regex.Replace(a, "\bEARN_1\b", Format(iern1, "00000.00"))
        End If
211:    If InStr(1, UCase(a), "EARN_2") <> 0 Then
            a = Regex.Replace(a, "\bEARN_2\b", Format(iern2, "00000.00"))
        End If

212:    If InStr(1, UCase(a), "EARN_3") <> 0 Then
            a = Regex.Replace(a, "\bEARN_3\b", Format(iern3, "00000.00"))
        End If

213:    If InStr(1, UCase(a), "EARN_4") <> 0 Then
            a = Regex.Replace(a, "\bEARN_4\b", Format(iern4, "00000.00"))
        End If

214:    If InStr(1, UCase(a), "EARN_5") <> 0 Then
            a = Regex.Replace(a, "\bEARN_5\b", Format(iern5, "00000.00"))
        End If

215:    If InStr(1, UCase(a), "EARN_6") <> 0 Then
            a = Regex.Replace(a, "\bEARN_6\b", Format(iern6, "00000.00"))
        End If

216:    If InStr(1, UCase(a), "EARN_7") <> 0 Then
            a = Regex.Replace(a, "\bEARN_7\b", Format(iern7, "00000.00"))
        End If

217:    If InStr(1, UCase(a), "EARN_8") <> 0 Then
            a = Regex.Replace(a, "\bEARN_8\b", Format(iern8, "00000.00"))
        End If

218:    If InStr(1, UCase(a), "EARN_9") <> 0 Then
            a = Regex.Replace(a, "\bEARN_9\b", Format(iern9, "00000.00"))
        End If

219:    If InStr(1, UCase(a), "EARN_10") <> 0 Then
            a = Regex.Replace(a, "\bEARN_10\b", Format(iern10, "00000.00"))
        End If

221:    If InStr(1, UCase(a), "DEDUCT_1") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_1\b", Format(ided1, "00000.00"))
        End If
222:    If InStr(1, UCase(a), "DEDUCT_2") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_2\b", Format(ided2, "00000.00"))
        End If

223:    If InStr(1, UCase(a), "DEDUCT_3") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_3\b", Format(ided3, "00000.00"))
        End If

224:    If InStr(1, UCase(a), "DEDUCT_4") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_4\b", Format(ided4, "00000.00"))
        End If

225:    If InStr(1, UCase(a), "DEDUCT_5") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_5\b", Format(ided5, "00000.00"))
        End If

226:    If InStr(1, UCase(a), "DEDUCT_6") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_6\b", Format(ided6, "00000.00"))
        End If

227:    If InStr(1, UCase(a), "DEDUCT_7") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_7\b", Format(ided7, "00000.00"))
        End If

228:    If InStr(1, UCase(a), "DEDUCT_8") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_8\b", Format(ided8, "00000.00"))
        End If

229:    If InStr(1, UCase(a), "DEDUCT_9") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_9\b", Format(ided9, "00000.00"))
        End If

230:    If InStr(1, UCase(a), "DEDUCT_10") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_10\b", Format(ided10, "00000.00"))
        End If

23:     If InStr(1, UCase(a), "OT_RATE") <> 0 Then
            a = Regex.Replace(a, "\bOT_RATE\b", Format(OT_RATE, "0000.00"))
        End If
24:     If InStr(1, UCase(a), "MON_DAY") <> 0 Then
            a = Regex.Replace(a, "\bMON_DAY\b", Format(MON_DAY, "0000"))
        End If
25:     If InStr(1, UCase(a), "CONV") <> 0 Then
            a = Regex.Replace(a, "\bCONV\b", Format(CONV, "000000"))
        End If
26:     If InStr(1, UCase(a), "MED") <> 0 Then
            a = Regex.Replace(a, "\bMED\b", Format(MED, "000000"))
        End If

27:     If InStr(1, UCase(a), "TDS") <> 0 Then
            a = Regex.Replace(a, "\bTDS\b", Format(TDS, "000000"))
        End If
28:     If InStr(1, UCase(a), "PROF_TAX") <> 0 Then
            a = Regex.Replace(a, "\bPROF_TAX\b", Format(PROF_TAX, "000000"))
        End If

99:     If InStr(1, UCase(a), "ISALARY") <> 0 Then
            a = Regex.Replace(a, "\bISALARY\b", Format(iSalary, "000000.00"))
        End If
98:     If InStr(1, UCase(a), "IOTAMT") <> 0 Then
            a = Regex.Replace(a, "\bIOTAMT\b", Format(iOtAmt, "00000.00"))
        End If
97:     If InStr(1, UCase(a), "IHRAAMT") <> 0 Then
            a = Regex.Replace(a, "\bIHRAAMT\b", Format(iHRAAMT, "000000.00"))
        End If
96:     If InStr(1, UCase(a), "IERNAMT1") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT1\b", Format(iErnAmt1, "0000000.00"))
        End If
95:     If InStr(1, UCase(a), "IERNAMT2") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT2\b", Format(iErnAmt2, "0000000.00"))
        End If
94:     If InStr(1, UCase(a), "IERNAMT3") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT3\b", Format(iErnAmt3, "0000000.00"))
        End If
93:     If InStr(1, UCase(a), "IERNAMT4") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT4\b", Format(iErnAmt4, "0000000.00"))
        End If
92:     If InStr(1, UCase(a), "IERNAMT5") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT5\b", Format(iErnAmt5, "0000000.00"))
        End If
91:     If InStr(1, UCase(a), "IERNAMT6") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT6\b", Format(iErnAmt6, "0000000.00"))
        End If
90:     If InStr(1, UCase(a), "IERNAMT7") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT7\b", Format(iErnAmt7, "0000000.00"))
        End If
89:     If InStr(1, UCase(a), "IERNAMT8") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT8\b", Format(iErnAmt8, "0000000.00"))
        End If
87:     If InStr(1, UCase(a), "IERNAMT9") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT9\b", Format(iErnAmt9, "0000000.00"))
        End If
86:     If InStr(1, UCase(a), "IERNAMT10") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT10\b", Format(iErnAmt10, "0000000.00"))
        End If
PF:     If InStr(1, UCase(a), "PFMAX") <> 0 Then
            a = Regex.Replace(a, "\bPFMAX\b", Format(g_pfmaxlimit, "0000000"))
        End If
        a1 = ""
        m = 1
        For m = 1 To Len(Trim(a))
            If Mid(Trim(a), m, 1) <> " " Then
                a1 = a1 & Mid(Trim(a), m, 1)
            End If
        Next
333:    If Mid(Trim(a1), 1, 2) = "IF" Then
            lCnt = 0
            Pcnt = 0
            mCtr = 1
            Do While InStr(mCtr, Trim(a1), "IF") <> 0
                lCnt = InStr(mCtr, Trim(a1), "IF")
                mCtr = lCnt + 1
            Loop
            St = Mid(Trim(a1), 1, lCnt - 1)
            If lCnt > 1 Then
                mCtr = lCnt
                Do While InStr(mCtr, Trim(a1), "]") <> 0
                    Pcnt = Pcnt + 1
                    If Pcnt = 2 Then
                        Pcnt = InStr(mCtr, Trim(a1), "]")
                        Exit Do
                    End If
                    mCtr = InStr(mCtr, Trim(a1), "]") + 1
                Loop
                Rt = Mid(Trim(a1), Pcnt + 2, Len(Trim(a1)) - (Pcnt + 1))
                a1 = Mid(Trim(a1), lCnt, Pcnt + 2 - lCnt)
            Else
                Rt = ""
            End If
            mifChk = False
            a = Mid(Trim(a1), InStr(1, Trim(a1), "(") + 1, InStr(1, Trim(a1), ",") - (InStr(1, Trim(a1), "(") + 1))
            mifChk = Mid(a, 1, Len(a)) 'frmCapture.ScriptControl1.Eval(Mid(a, 1, Len(a)))
            a = Mid(Trim(a1), InStr(1, Trim(a1), ",") + 1, Len(Trim(a1)) - (InStr(1, Trim(a1), ",") + 1))
            Tstring = Mid(Trim(a), 2, (InStr(1, Trim(a), ",") - 3))
            Fstring = Mid(Trim(a), (InStr(1, Trim(a), ",") + 2), (Len(Trim(a)) - (InStr(1, Trim(a), ",") + 2)))
            If mifChk Then
                a1 = Tstring
            Else
                a1 = Fstring
            End If
            a = St + a1 + Rt
            a1 = a
            GoTo 333
        End If
        FormulaValueSet = a1
    End Function
    Private Function selectQuery(a As String, formula As String) As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim d As Integer, formulalen As Integer
        Dim fhalf As String
        Dim lhalf As String
        Dim mform As DataSet = New DataSet
        d = InStr(1, UCase(a), "#" & formula & "#")
        Dim sSql As String = "select * from pay_formula where code='" & formula & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(mform)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(mform)
        End If
        'mform = Cn.Execute(sSql)
        formulalen = Len(mform.Tables(0).Rows(0).Item("Form").ToString.Trim)
        fhalf = Mid(a, 1, d - 1)
        formulalen = d + 3
        lhalf = Mid(a, formulalen)
        a = fhalf & "[ " & mform.Tables(0).Rows(0).Item("Form").ToString.Trim & " ]" & lhalf
    End Function
    Private Sub Set_Filter_Crystal() 'Optional ByVal Selection As String
        Dim mCompanyString As String, mDepartmentString As String, mCatagoryString As String
        Dim mEmployeeString As String, mLocationString As String ' mShiftString As String
        Dim mDivisionString As String, mGradeString As String, mActive As String

        mCompanyString = ""
        mActive = ""
        mDepartmentString = ""
        mCatagoryString = ""
        mEmployeeString = ""
        'mShiftString = ""
        mDivisionString = ""
        mGradeString = ""
        mLocationString = ""
        Dim g_CompanyNames As String = ""
        'For CompanyCode
        Dim selectedRowsComp As Integer() = GridViewComp.GetSelectedRows()
        Dim resultComp As Object() = New Object(selectedRowsComp.Length - 1) {}
        For i As Integer = 0 To selectedRowsComp.Length - 1
            Dim rowHandle As Integer = selectedRowsComp(i)
            If Not GridViewComp.IsGroupRow(rowHandle) Then
                resultComp(i) = GridViewComp.GetRowCellValue(rowHandle, "COMPANYCODE")
                If i <> 0 Then
                    mCompanyString = mCompanyString + " OR "
                End If
                mCompanyString = mCompanyString + "tblEmployee.CompanyCode="
                mCompanyString = mCompanyString + "'" + resultComp(i) + "'"
                g_CompanyNames = g_CompanyNames & GridViewComp.GetRowCellValue(rowHandle, "COMPANYNAME") & ", "
            End If
        Next
        CommonReport.g_CompanyNames = g_CompanyNames.Trim.TrimStart(",").TrimEnd(",")
      
        'For DepartmentCode
        Dim selectedRowsDept As Integer() = GridViewDept.GetSelectedRows()
        Dim resultDept As Object() = New Object(selectedRowsDept.Length - 1) {}
        For i As Integer = 0 To selectedRowsDept.Length - 1
            Dim rowHandle As Integer = selectedRowsDept(i)
            If Not GridViewDept.IsGroupRow(rowHandle) Then
                resultDept(i) = GridViewDept.GetRowCellValue(rowHandle, "DEPARTMENTCODE")
                If i <> 0 Then
                    mDepartmentString = mDepartmentString + " OR "
                End If
                mDepartmentString = mDepartmentString + "tblEmployee.DepartmentCode = "
                mDepartmentString = mDepartmentString + "'" + resultDept(i) + "'"
            End If
        Next

        'For CatagoryCode
        Dim selectedRowsCat As Integer() = GridViewCat.GetSelectedRows()
        Dim resultCat As Object() = New Object(selectedRowsCat.Length - 1) {}
        For i As Integer = 0 To selectedRowsCat.Length - 1
            Dim rowHandle As Integer = selectedRowsCat(i)
            If Not GridViewCat.IsGroupRow(rowHandle) Then
                resultCat(i) = GridViewCat.GetRowCellValue(rowHandle, "CAT")
                If i <> 0 Then
                    mCatagoryString = mCatagoryString + " OR "
                End If
                mCatagoryString = mCatagoryString + "tblEmployee.CAT = "
                mCatagoryString = mCatagoryString + "'" + resultCat(i) + "'"
            End If
        Next

        ' For Grade Code
        Dim selectedRowsGrade As Integer() = GridViewGrade.GetSelectedRows()
        Dim resultGrade As Object() = New Object(selectedRowsGrade.Length - 1) {}
        For i As Integer = 0 To selectedRowsGrade.Length - 1
            Dim rowHandle As Integer = selectedRowsGrade(i)
            If Not GridViewGrade.IsGroupRow(rowHandle) Then
                resultGrade(i) = GridViewGrade.GetRowCellValue(rowHandle, "GradeCode")
                If i <> 0 Then
                    mGradeString = mGradeString + " OR "
                End If
                mGradeString = mGradeString + "tblEmployee.GradeCode = "
                mGradeString = mGradeString + "'" + resultGrade(i) + "'"
            End If
        Next

        'For EmployeeCode  - Check for late arrival
        Dim selectedRowsEmp As Integer() = GridViewEmp.GetSelectedRows()
        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
        For i As Integer = 0 To selectedRowsEmp.Length - 1
            Dim rowHandle As Integer = selectedRowsEmp(i)
            If Not GridViewEmp.IsGroupRow(rowHandle) Then
                resultEmp(i) = GridViewEmp.GetRowCellValue(rowHandle, "PAYCODE")
                Dim emp As String = resultEmp(i)
                If i <> 0 Then
                    mEmployeeString = mEmployeeString + " OR "
                End If
                mEmployeeString = mEmployeeString & "tblEmployee.Paycode="
                mEmployeeString = mEmployeeString & "'" & emp & "'"
            End If
        Next

        ''For Shift
        'Dim selectedRowsShift As Integer() = GridViewShift.GetSelectedRows()
        'Dim resultShift As Object() = New Object(selectedRowsShift.Length - 1) {}
        'For i As Integer = 0 To selectedRowsShift.Length - 1
        '    Dim rowHandle As Integer = selectedRowsShift(i)
        '    If Not GridViewShift.IsGroupRow(rowHandle) Then
        '        resultShift(i) = GridViewShift.GetRowCellValue(rowHandle, "SHIFT")
        '        If i <> 0 Then
        '            mShiftString = mShiftString + " OR "
        '        End If
        '        If WhichReport = 1 Then
        '            mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
        '        Else
        '            mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
        '        End If
        '        mShiftString = mShiftString + "'" + resultShift(i) + "'"
        '    End If
        'Next

        'For Location
        Dim selectedRowsLocation As Integer() = GridViewBranch.GetSelectedRows()
        Dim resultLocation As Object() = New Object(selectedRowsLocation.Length - 1) {}
        For i As Integer = 0 To selectedRowsLocation.Length - 1
            Dim rowHandle As Integer = selectedRowsLocation(i)
            If Not GridViewBranch.IsGroupRow(rowHandle) Then
                resultLocation(i) = GridViewBranch.GetRowCellValue(rowHandle, "BRANCHCODE")
                If i <> 0 Then
                    mLocationString = mLocationString + " OR "
                End If
                If WhichReport = 1 Then
                    mLocationString = mLocationString + "tblEmployee.BRANCHCODE = "
                Else
                    mLocationString = mLocationString + "tblEmployee.BRANCHCODE = "
                End If
                mLocationString = mLocationString + "'" + resultLocation(i) + "'"
            End If
        Next
        'For i = 0 To (LstShiftTarget.ListCount - 1)
        '    LstShiftTarget.ListIndex = i
        '    If i <> 0 Then
        '        mShiftString = mShiftString + " OR "
        '    End If
        '    If WhichReport = 1 Then
        '        'If g_Report_view Then
        '        mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
        '        'Else
        '        '    mShiftString = mShiftString + "tblTimeRegisterd.ShiftAttended = "
        '        'End If
        '    Else
        '        'If g_Report_view Then
        '        mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
        '        'Else
        '        '    mShiftString = mShiftString + "tblTimeRegisterd.ShiftAttended = "
        '        'End If
        '    End If
        '    mShiftString = mShiftString + "'" + Left(LstShiftTarget.Text, 3) + "'"
        'Next i

        If CheckEdit1.Checked And CheckEdit2.Checked Then
            mActive = ""
        Else
            If CheckEdit1.Checked Then
                mActive = "tblEmployee.Active = 'Y'"
            End If
            If CheckEdit2.Checked Then
                mActive = "tblEmployee.Active = 'N'"
            End If
        End If
        Dim mCondition As String
        'Dim g_WhereClause As String
        mCondition = IIf(Len(Trim(mEmployeeString)) = 0, "(", "(" & Trim(mEmployeeString) & ") AND (") & _
                     IIf(Len(Trim(mCatagoryString)) = 0, "", " " & Trim(mCatagoryString) & ") AND (") & _
                     IIf(Len(Trim(mDepartmentString)) = 0, "", " " & Trim(mDepartmentString) & ") AND (") & _
                     IIf(Len(Trim(mCompanyString)) = 0, "", " " & Trim(mCompanyString) & ") AND (") & _
                     IIf(Len(Trim(mGradeString)) = 0, "", " " & Trim(mGradeString) & ") AND (") & _
                     IIf(Len(Trim(mLocationString)) = 0, "", " " & Trim(mLocationString) & ") AND (") & _
                     IIf(Len(Trim(mActive)) = 0, "", " " & Trim(mActive) & ") AND (")
        If mCondition = "(" Then
            mCondition = ""
        End If
        If Not Len(Trim(mCondition)) = 0 Then
            mCondition = mCondition.Remove(mCondition.Trim.Length - 5)
            'mCondition = LTrim(mCondition, Len(Trim(mCondition)) - 5)
        End If
        g_WhereClause = mCondition
    End Sub
    Private Sub Set_Filter_Crystal_rpt() 'Optional ByVal Selection As String
        Dim mCompanyString As String, mDepartmentString As String, mCatagoryString As String
        Dim mEmployeeString As String, mLocationString As String ' mShiftString As String
        Dim mDivisionString As String, mGradeString As String, mActive As String

        mCompanyString = ""
        mActive = ""
        mDepartmentString = ""
        mCatagoryString = ""
        mEmployeeString = ""
        'mShiftString = ""
        mDivisionString = ""
        mGradeString = ""
        mLocationString = ""
        Dim g_CompanyNames As String = ""
        'For CompanyCode
        Dim selectedRowsComp As Integer() = GridViewComp.GetSelectedRows()
        Dim resultComp As Object() = New Object(selectedRowsComp.Length - 1) {}
        For i As Integer = 0 To selectedRowsComp.Length - 1
            Dim rowHandle As Integer = selectedRowsComp(i)
            If Not GridViewComp.IsGroupRow(rowHandle) Then
                resultComp(i) = GridViewComp.GetRowCellValue(rowHandle, "COMPANYCODE")
                If i <> 0 Then
                    mCompanyString = mCompanyString + " OR "
                End If
                mCompanyString = mCompanyString + "{tblEmployee.CompanyCode}="
                mCompanyString = mCompanyString + "'" + resultComp(i) + "'"
                'g_CompanyNames = g_CompanyNames & GridViewComp.GetRowCellValue(rowHandle, "COMPANYNAME") & ", "
            End If
        Next
        'CommonReport.g_CompanyNames = g_CompanyNames.Trim.TrimStart(",").TrimEnd(",")

        'For DepartmentCode
        Dim selectedRowsDept As Integer() = GridViewDept.GetSelectedRows()
        Dim resultDept As Object() = New Object(selectedRowsDept.Length - 1) {}
        For i As Integer = 0 To selectedRowsDept.Length - 1
            Dim rowHandle As Integer = selectedRowsDept(i)
            If Not GridViewDept.IsGroupRow(rowHandle) Then
                resultDept(i) = GridViewDept.GetRowCellValue(rowHandle, "DEPARTMENTCODE")
                If i <> 0 Then
                    mDepartmentString = mDepartmentString + " OR "
                End If
                mDepartmentString = mDepartmentString + "{tblEmployee.DepartmentCode} = "
                mDepartmentString = mDepartmentString + "'" + resultDept(i) + "'"
            End If
        Next

        'For CatagoryCode
        Dim selectedRowsCat As Integer() = GridViewCat.GetSelectedRows()
        Dim resultCat As Object() = New Object(selectedRowsCat.Length - 1) {}
        For i As Integer = 0 To selectedRowsCat.Length - 1
            Dim rowHandle As Integer = selectedRowsCat(i)
            If Not GridViewCat.IsGroupRow(rowHandle) Then
                resultCat(i) = GridViewCat.GetRowCellValue(rowHandle, "CAT")
                If i <> 0 Then
                    mCatagoryString = mCatagoryString + " OR "
                End If
                mCatagoryString = mCatagoryString + "{tblEmployee.CAT} = "
                mCatagoryString = mCatagoryString + "'" + resultCat(i) + "'"
            End If
        Next

        ' For Grade Code
        Dim selectedRowsGrade As Integer() = GridViewGrade.GetSelectedRows()
        Dim resultGrade As Object() = New Object(selectedRowsGrade.Length - 1) {}
        For i As Integer = 0 To selectedRowsGrade.Length - 1
            Dim rowHandle As Integer = selectedRowsGrade(i)
            If Not GridViewGrade.IsGroupRow(rowHandle) Then
                resultGrade(i) = GridViewGrade.GetRowCellValue(rowHandle, "GradeCode")
                If i <> 0 Then
                    mGradeString = mGradeString + " OR "
                End If
                mGradeString = mGradeString + "{tblEmployee.GradeCode} = "
                mGradeString = mGradeString + "'" + resultGrade(i) + "'"
            End If
        Next

        'For EmployeeCode  - Check for late arrival
        Dim selectedRowsEmp As Integer() = GridViewEmp.GetSelectedRows()
        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
        For i As Integer = 0 To selectedRowsEmp.Length - 1
            Dim rowHandle As Integer = selectedRowsEmp(i)
            If Not GridViewEmp.IsGroupRow(rowHandle) Then
                resultEmp(i) = GridViewEmp.GetRowCellValue(rowHandle, "PAYCODE")
                Dim emp As String = resultEmp(i)
                If i <> 0 Then
                    mEmployeeString = mEmployeeString + " OR "
                End If
                mEmployeeString = mEmployeeString & "{tblEmployee.Paycode}="
                mEmployeeString = mEmployeeString & "'" & emp & "'"
            End If
        Next

        'For Location
        Dim selectedRowsLocation As Integer() = GridViewBranch.GetSelectedRows()
        Dim resultLocation As Object() = New Object(selectedRowsLocation.Length - 1) {}
        For i As Integer = 0 To selectedRowsLocation.Length - 1
            Dim rowHandle As Integer = selectedRowsLocation(i)
            If Not GridViewBranch.IsGroupRow(rowHandle) Then
                resultLocation(i) = GridViewBranch.GetRowCellValue(rowHandle, "BRANCHCODE")
                If i <> 0 Then
                    mLocationString = mLocationString + " OR "
                End If
                If WhichReport = 1 Then
                    mLocationString = mLocationString + "{tblEmployee.BRANCHCODE} = "
                Else
                    mLocationString = mLocationString + "{tblEmployee.BRANCHCODE} = "
                End If
                mLocationString = mLocationString + "'" + resultLocation(i) + "'"
            End If
        Next


        If CheckEdit1.Checked And CheckEdit2.Checked Then
            mActive = ""
        Else
            If CheckEdit1.Checked Then
                mActive = "tblEmployee.Active = 'Y'"
            End If
            If CheckEdit2.Checked Then
                mActive = "tblEmployee.Active = 'N'"
            End If
        End If
        Dim mCondition As String
        'Dim g_WhereClause As String
        mCondition = IIf(Len(Trim(mEmployeeString)) = 0, "(", "(" & Trim(mEmployeeString) & ") AND (") & _
                     IIf(Len(Trim(mCatagoryString)) = 0, "", " " & Trim(mCatagoryString) & ") AND (") & _
                     IIf(Len(Trim(mDepartmentString)) = 0, "", " " & Trim(mDepartmentString) & ") AND (") & _
                     IIf(Len(Trim(mCompanyString)) = 0, "", " " & Trim(mCompanyString) & ") AND (") & _
                     IIf(Len(Trim(mGradeString)) = 0, "", " " & Trim(mGradeString) & ") AND (") & _
                     IIf(Len(Trim(mLocationString)) = 0, "", " " & Trim(mLocationString) & ") AND (") & _
                     IIf(Len(Trim(mActive)) = 0, "", " " & Trim(mActive) & ") AND (")
        If mCondition = "(" Then
            mCondition = ""
        End If
        If Not Len(Trim(mCondition)) = 0 Then
            mCondition = mCondition.Remove(mCondition.Trim.Length - 5)
            'mCondition = LTrim(mCondition, Len(Trim(mCondition)) - 5)
        End If
        g_WhereClause_rpt = mCondition
    End Sub
    Private Sub PopupContainerEditEmp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditEmp.QueryResultValue
        Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("PAYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditComp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditComp.QueryResultValue
        Dim selectedRows() As Integer = GridViewComp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewComp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("COMPANYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditDept_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditDept.QueryResultValue
        Dim selectedRows() As Integer = GridViewDept.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewDept.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("DEPARTMENTCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditCat_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditCat.QueryResultValue
        Dim selectedRows() As Integer = GridViewCat.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewCat.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("CAT"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditGrade_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditGrade.QueryResultValue
        Dim selectedRows() As Integer = GridViewGrade.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewGrade.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("GradeCode"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditLocation_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditLocation.QueryResultValue
        Dim selectedRows() As Integer = GridViewBranch.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewBranch.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("BRANCHCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Function Length7(ByVal dblA As Double) As String
        Dim strB As String '* 7
        strB = Format(dblA, "0.00")
        Length7 = strB  '.Substring(0, 6)
        'MsgBox("Length7 " & Length7)
    End Function
    Function Length5(ByVal dblA As Double) As String
        Dim strB As String '* 5
        strB = Format(dblA, "0.00")
        Length5 = strB  '.Substring(0, 5)
        'MsgBox("Length5 " & Length5)
    End Function
    Private Sub CheckExcel_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckExcel.CheckedChanged
        If CheckExcel.Checked = True Then
            XtraShortOrder.ShowDialog()
        End If
    End Sub
    Private Sub CheckExcel_VisibleChanged(sender As System.Object, e As System.EventArgs) Handles CheckExcel.VisibleChanged
        If CheckExcel.Visible = False Then
            CheckText.Checked = True
        End If
    End Sub
    Private Sub DateEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles DateEditFrom.Leave
        If DateEditTo.Visible = False Then
            DateEditFrom.DateTime = DateEditFrom.DateTime.ToString("yyyy-MM-01")
            DateEditTo.DateTime = DateEditFrom.DateTime
        Else
            DateEditFrom.DateTime = DateEditFrom.DateTime.ToString("yyyy-MM-01")
            DateEditTo.DateTime = DateEditFrom.DateTime.AddMonths(1).AddDays(-1)
        End If
       
    End Sub
    Private Sub DateEdit2_Leave(sender As System.Object, e As System.EventArgs) Handles DateEditTo.Leave
        If DateEditTo.DateTime < DateEditFrom.DateTime Then
            DateEditTo.DateTime = DateEditFrom.DateTime.AddMonths(1).AddDays(-1)
        End If
    End Sub
    Private Sub GridView1_RowStyle(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles GridView1.RowStyle
        Dim View As GridView = CType(sender, GridView)
        If (e.RowHandle >= 0) Then
            Dim priority As String = View.GetRowCellDisplayText(e.RowHandle, View.Columns("Date"))
            If (priority = "Present") Then
                e.Appearance.BackColor = Color.FromArgb(150, Color.LightCoral)
                e.Appearance.BackColor2 = Color.Green
                e.Appearance.ForeColor = Color.White
            End If

        End If
    End Sub
    Private Sub op_CustomizeCell(ea As DevExpress.Export.CustomizeCellEventArgs)
        Dim View As GridView = CType(GridView1, GridView)
        Dim priority As String = View.GetRowCellDisplayText(ea.RowHandle, View.Columns("Date"))
        If (priority = "Present") Then
            ea.Formatting.BackColor = Color.Green
            ea.Formatting.Font.Color = Color.White
            ea.Handled = True
        End If
        'If ea.ColumnFieldName = "Date" Then
        '    ea.Formatting.BackColor = Color.Pink
        '    ea.Handled = True
        'End If
    End Sub
    Private Sub CheckEditLoanEntry_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditLoanEntry.CheckedChanged
        If CheckEditLoanEntry.Checked = True Then
            If Common.IsNepali = "Y" Then
                ComboNEpaliMonthTo.Visible = True
                ComboNepaliYearTo.Visible = True
                DateEditTo.Visible = False
            Else
                ComboNEpaliMonthTo.Visible = False
                ComboNepaliYearTo.Visible = False
                DateEditTo.Visible = True
            End If
            LabelControl3.Visible = True
        Else
            DateEditTo.Visible = False
            LabelControl3.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliYearTo.Visible = False
        End If
    End Sub
    Private Sub CheckEditLoanEntryDetails_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditLoanEntryDetails.CheckedChanged
        If CheckEditLoanEntryDetails.Checked = True Then
            If Common.IsNepali = "Y" Then
                ComboNEpaliMonthTo.Visible = True
                ComboNepaliYearTo.Visible = True
                DateEditTo.Visible = False
            Else
                ComboNEpaliMonthTo.Visible = False
                ComboNepaliYearTo.Visible = False
                DateEditTo.Visible = True
            End If
            LabelControl3.Visible = True
        Else
            DateEditTo.Visible = False
            LabelControl3.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliYearTo.Visible = False
        End If
    End Sub
    Private Sub CheckEditPfCh_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditPfCh.CheckedChanged
        If CheckEditPfCh.Checked = True Then
            XtraPfBank.ShowDialog()
        End If
    End Sub
    Private Sub CheckEditESICh_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditESICh.CheckedChanged
        If CheckEditESICh.Checked = True Then
            XtraPfBank.ShowDialog()
        End If
    End Sub
    Private Sub CheckEditAdvEntry_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditAdvEntry.CheckedChanged
        If CheckEditAdvEntry.Checked = True Then
            If Common.IsNepali = "Y" Then
                ComboNEpaliMonthTo.Visible = True
                ComboNepaliYearTo.Visible = True
                DateEditTo.Visible = False
            Else
                ComboNEpaliMonthTo.Visible = False
                ComboNepaliYearTo.Visible = False
                DateEditTo.Visible = True
            End If
            LabelControl3.Visible = True
        Else
            DateEditTo.Visible = False
            LabelControl3.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliYearTo.Visible = False
        End If
    End Sub
    Private Sub CheckEditAdvEntryDetails_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditAdvEntryDetails.CheckedChanged
        If CheckEditAdvEntryDetails.Checked = True Then
            If Common.IsNepali = "Y" Then
                ComboNEpaliMonthTo.Visible = True
                ComboNepaliYearTo.Visible = True
                DateEditTo.Visible = False
            Else
                ComboNEpaliMonthTo.Visible = False
                ComboNepaliYearTo.Visible = False
                DateEditTo.Visible = True
            End If
            LabelControl3.Visible = True
        Else
            DateEditTo.Visible = False
            LabelControl3.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliYearTo.Visible = False
        End If
    End Sub
    Private Sub CheckEditCashAdj_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditCashAdj.CheckedChanged
        If CheckEditCashAdj.Checked = True Then
            If Common.IsNepali = "Y" Then
                ComboNEpaliMonthTo.Visible = True
                ComboNepaliYearTo.Visible = True
                DateEditTo.Visible = False
            Else
                ComboNEpaliMonthTo.Visible = False
                ComboNepaliYearTo.Visible = False
                DateEditTo.Visible = True
            End If
            LabelControl3.Visible = True
        Else
            DateEditTo.Visible = False
            LabelControl3.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliYearTo.Visible = False
        End If
    End Sub
    Private Sub CheckEditPendingReEm_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditPendingReEm.CheckedChanged
        If CheckEditPendingReEm.Checked = True Then
            If Common.IsNepali = "Y" Then
                ComboNEpaliMonthTo.Visible = False
                ComboNepaliYearTo.Visible = False
                DateEditTo.Visible = False
                ComboNEpaliMonthFrm.Visible = False
                ComboNepaliYearFrm.Visible = False
                DateEditFrom.Visible = False
            Else
                ComboNEpaliMonthTo.Visible = False
                ComboNepaliYearTo.Visible = False
                DateEditTo.Visible = False
                DateEditFrom.Visible = False
            End If
            LabelControl3.Visible = False
            LabelControl2.Visible = False
        Else
            DateEditTo.Visible = False
            LabelControl3.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliYearTo.Visible = False

            LabelControl2.Visible = True
            If Common.IsNepali = "Y" Then
                ComboNEpaliMonthFrm.Visible = True
                ComboNepaliYearFrm.Visible = True
                DateEditFrom.Visible = False
            Else
                ComboNEpaliMonthFrm.Visible = False
                ComboNepaliYearFrm.Visible = False
                DateEditFrom.Visible = True
            End If
        End If
    End Sub
    Private Sub CheckEditLEaveEncash_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditLEaveEncash.CheckedChanged
        If CheckEditLEaveEncash.Checked = True Then
            If Common.IsNepali = "Y" Then
                ComboNEpaliMonthTo.Visible = True
                ComboNepaliYearTo.Visible = True
                DateEditTo.Visible = False
            Else
                ComboNEpaliMonthTo.Visible = False
                ComboNepaliYearTo.Visible = False
                DateEditTo.Visible = True
            End If
            LabelControl3.Visible = True
        Else
            DateEditTo.Visible = False
            LabelControl3.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliYearTo.Visible = False
        End If
    End Sub
End Class