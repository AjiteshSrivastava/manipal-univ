﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraCanteenTimeSlab
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraCanteenTimeSlab))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.TblTimeSlabBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colShift = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBStart = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colBEnd = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLStart = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLEnd = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDStart = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEnd = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditDEnd = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditDStrt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditLEnd = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditLStrt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditBFEnd = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditBFStrt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.ToggleSwitch1 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblTimeSlabTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblTimeSlabTableAdapter()
        Me.TblTimeSlab1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblTimeSlab1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        Me.SidePanel1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblTimeSlabBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.TextEditDEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDStrt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditLEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditLStrt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBFEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBFStrt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl2)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.GridControl1)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel1.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1036, 424)
        Me.SidePanel1.TabIndex = 4
        Me.SidePanel1.Text = "SidePanel1"
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblTimeSlabBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(1036, 423)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'TblTimeSlabBindingSource
        '
        Me.TblTimeSlabBindingSource.DataMember = "tblTimeSlab"
        Me.TblTimeSlabBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GridView1.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Blue
        Me.GridView1.Appearance.TopNewRow.Options.UseFont = True
        Me.GridView1.Appearance.TopNewRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colShift, Me.colBStart, Me.colBEnd, Me.colLStart, Me.colLEnd, Me.colDStart, Me.colDEnd})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.NewItemRowText = "Click here to add new Meal Slab"
        Me.GridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridView1.OptionsEditForm.EditFormColumnCount = 1
        Me.GridView1.OptionsEditForm.PopupEditFormWidth = 400
        Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.GridView1.OptionsView.ShowFooter = True
        '
        'colShift
        '
        Me.colShift.Caption = "Id"
        Me.colShift.FieldName = "Shift"
        Me.colShift.Name = "colShift"
        Me.colShift.Visible = True
        Me.colShift.VisibleIndex = 0
        '
        'colBStart
        '
        Me.colBStart.Caption = "Start Time"
        Me.colBStart.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colBStart.FieldName = "BStart"
        Me.colBStart.Name = "colBStart"
        Me.colBStart.Visible = True
        Me.colBStart.VisibleIndex = 1
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'colBEnd
        '
        Me.colBEnd.Caption = "End Time"
        Me.colBEnd.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colBEnd.FieldName = "BEnd"
        Me.colBEnd.Name = "colBEnd"
        Me.colBEnd.Visible = True
        Me.colBEnd.VisibleIndex = 2
        '
        'colLStart
        '
        Me.colLStart.Caption = "Lunch Start"
        Me.colLStart.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colLStart.FieldName = "LStart"
        Me.colLStart.Name = "colLStart"
        '
        'colLEnd
        '
        Me.colLEnd.Caption = "Lunch End"
        Me.colLEnd.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colLEnd.FieldName = "LEnd"
        Me.colLEnd.Name = "colLEnd"
        '
        'colDStart
        '
        Me.colDStart.Caption = "Dinner Start"
        Me.colDStart.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colDStart.FieldName = "DStart"
        Me.colDStart.Name = "colDStart"
        '
        'colDEnd
        '
        Me.colDEnd.Caption = "Dinner End"
        Me.colDEnd.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colDEnd.FieldName = "DEnd"
        Me.colDEnd.Name = "colDEnd"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.SimpleButton1)
        Me.GroupControl1.Controls.Add(Me.TextEditDEnd)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.TextEditDStrt)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.TextEditLEnd)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.TextEditLStrt)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.TextEditBFEnd)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.TextEditBFStrt)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(580, 443)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(551, 215)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Meal Time Slab"
        Me.GroupControl1.Visible = False
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(391, 134)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 18
        Me.SimpleButton1.Text = "Save"
        '
        'TextEditDEnd
        '
        Me.TextEditDEnd.EditValue = ""
        Me.TextEditDEnd.Location = New System.Drawing.Point(394, 89)
        Me.TextEditDEnd.Name = "TextEditDEnd"
        Me.TextEditDEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDEnd.Properties.Appearance.Options.UseFont = True
        Me.TextEditDEnd.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditDEnd.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditDEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditDEnd.Properties.MaxLength = 5
        Me.TextEditDEnd.Size = New System.Drawing.Size(72, 20)
        Me.TextEditDEnd.TabIndex = 17
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(311, 92)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(52, 14)
        Me.LabelControl6.TabIndex = 16
        Me.LabelControl6.Text = "End Time"
        '
        'TextEditDStrt
        '
        Me.TextEditDStrt.EditValue = ""
        Me.TextEditDStrt.Location = New System.Drawing.Point(151, 89)
        Me.TextEditDStrt.Name = "TextEditDStrt"
        Me.TextEditDStrt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDStrt.Properties.Appearance.Options.UseFont = True
        Me.TextEditDStrt.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditDStrt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditDStrt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditDStrt.Properties.MaxLength = 5
        Me.TextEditDStrt.Size = New System.Drawing.Size(72, 20)
        Me.TextEditDStrt.TabIndex = 15
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(17, 92)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(97, 14)
        Me.LabelControl5.TabIndex = 14
        Me.LabelControl5.Text = "Dinner Start Time"
        '
        'TextEditLEnd
        '
        Me.TextEditLEnd.EditValue = ""
        Me.TextEditLEnd.Location = New System.Drawing.Point(394, 63)
        Me.TextEditLEnd.Name = "TextEditLEnd"
        Me.TextEditLEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditLEnd.Properties.Appearance.Options.UseFont = True
        Me.TextEditLEnd.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditLEnd.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditLEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditLEnd.Properties.MaxLength = 5
        Me.TextEditLEnd.Size = New System.Drawing.Size(72, 20)
        Me.TextEditLEnd.TabIndex = 13
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(311, 66)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(52, 14)
        Me.LabelControl4.TabIndex = 12
        Me.LabelControl4.Text = "End Time"
        '
        'TextEditLStrt
        '
        Me.TextEditLStrt.EditValue = ""
        Me.TextEditLStrt.Location = New System.Drawing.Point(151, 63)
        Me.TextEditLStrt.Name = "TextEditLStrt"
        Me.TextEditLStrt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditLStrt.Properties.Appearance.Options.UseFont = True
        Me.TextEditLStrt.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditLStrt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditLStrt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditLStrt.Properties.MaxLength = 5
        Me.TextEditLStrt.Size = New System.Drawing.Size(72, 20)
        Me.TextEditLStrt.TabIndex = 11
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(17, 66)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(95, 14)
        Me.LabelControl3.TabIndex = 10
        Me.LabelControl3.Text = "Lunch Start Time"
        '
        'TextEditBFEnd
        '
        Me.TextEditBFEnd.EditValue = ""
        Me.TextEditBFEnd.Location = New System.Drawing.Point(394, 37)
        Me.TextEditBFEnd.Name = "TextEditBFEnd"
        Me.TextEditBFEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBFEnd.Properties.Appearance.Options.UseFont = True
        Me.TextEditBFEnd.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditBFEnd.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditBFEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditBFEnd.Properties.MaxLength = 5
        Me.TextEditBFEnd.Size = New System.Drawing.Size(72, 20)
        Me.TextEditBFEnd.TabIndex = 9
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(311, 40)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(52, 14)
        Me.LabelControl2.TabIndex = 8
        Me.LabelControl2.Text = "End Time"
        '
        'TextEditBFStrt
        '
        Me.TextEditBFStrt.EditValue = ""
        Me.TextEditBFStrt.Location = New System.Drawing.Point(151, 37)
        Me.TextEditBFStrt.Name = "TextEditBFStrt"
        Me.TextEditBFStrt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBFStrt.Properties.Appearance.Options.UseFont = True
        Me.TextEditBFStrt.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditBFStrt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditBFStrt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditBFStrt.Properties.MaxLength = 5
        Me.TextEditBFStrt.Size = New System.Drawing.Size(72, 20)
        Me.TextEditBFStrt.TabIndex = 7
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(17, 40)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(112, 14)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Breakfast Start Time"
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl2.Controls.Add(Me.ToggleSwitch1)
        Me.GroupControl2.Controls.Add(Me.LabelControl7)
        Me.GroupControl2.Controls.Add(Me.SimpleButton2)
        Me.GroupControl2.Location = New System.Drawing.Point(23, 443)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(551, 93)
        Me.GroupControl2.TabIndex = 3
        Me.GroupControl2.Text = "Meal Slab"
        '
        'ToggleSwitch1
        '
        Me.ToggleSwitch1.Location = New System.Drawing.Point(196, 42)
        Me.ToggleSwitch1.Name = "ToggleSwitch1"
        Me.ToggleSwitch1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch1.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch1.Properties.OffText = "Off"
        Me.ToggleSwitch1.Properties.OnText = "On"
        Me.ToggleSwitch1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ToggleSwitch1.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch1.TabIndex = 20
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(17, 47)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(139, 14)
        Me.LabelControl7.TabIndex = 19
        Me.LabelControl7.Text = "Meal Time Slab Applicable"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(438, 38)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 18
        Me.SimpleButton2.Text = "Save"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 1
        '
        'TblTimeSlabTableAdapter
        '
        Me.TblTimeSlabTableAdapter.ClearBeforeFill = True
        '
        'TblTimeSlab1TableAdapter1
        '
        Me.TblTimeSlab1TableAdapter1.ClearBeforeFill = True
        '
        'XtraCanteenTimeSlab
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraCanteenTimeSlab"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        Me.SidePanel1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblTimeSlabBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.TextEditDEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDStrt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditLEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditLStrt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBFEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBFStrt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditDEnd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditDStrt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditLEnd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditLStrt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditBFEnd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditBFStrt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitch1 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TblTimeSlabBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents colShift As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBStart As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents colBEnd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLStart As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLEnd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDStart As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEnd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblTimeSlabTableAdapter As iAS.SSSDBDataSetTableAdapters.tblTimeSlabTableAdapter
    Friend WithEvents TblTimeSlab1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblTimeSlab1TableAdapter

End Class
