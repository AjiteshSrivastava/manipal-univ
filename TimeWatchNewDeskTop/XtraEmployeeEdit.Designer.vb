﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraEmployeeEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraEmployeeEdit))
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.DateEditLeaving = New DevExpress.XtraEditors.DateEdit()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEdit5 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblGradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colGradeCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGradeName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEdit4 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.EmployeeGroupBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colGroupId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGroupName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEdit3 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblCatagoryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCATAGORYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEdit2 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblDepartmentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEdit1 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblCompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCOMPANYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitch1 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GridLookUpEdit8 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblbranchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEdit6 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblDESPANSARYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDESPANSARYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDESPANSARYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditPF = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditPF1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditESI = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEditNepaliYearLeave = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNEpaliMonthLeave = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNepaliDateLeave = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.ComboBoxEditNepaliYearDOB = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNEpaliMonthDOB = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNepaliDateDOB = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNepaliYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNEpaliMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.TextEditUid = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit19 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit2 = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEditNepaliDate = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.TextEdit17 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit16 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit15 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit14 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit13 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit12 = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEdit7 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblBankBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView6 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBANKCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBANKNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ComboBoxEdit2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.TextEdit11 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit10 = New DevExpress.XtraEditors.TextEdit()
        Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ToggleSwitch2 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.DateEditBirth = New DevExpress.XtraEditors.DateEdit()
        Me.DateEditJoin = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.TblCompanyTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter()
        Me.TblCompany1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter()
        Me.TblDepartmentTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter()
        Me.TblDepartment1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter()
        Me.TblCatagoryTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter()
        Me.TblCatagory1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter()
        Me.EmployeeGroupTableAdapter = New iAS.SSSDBDataSetTableAdapters.EmployeeGroupTableAdapter()
        Me.EmployeeGroup1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.EmployeeGroup1TableAdapter()
        Me.TblGradeTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblGradeTableAdapter()
        Me.TblGrade1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblGrade1TableAdapter()
        Me.TblDESPANSARYTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblDESPANSARYTableAdapter()
        Me.TblBankTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblBankTableAdapter()
        Me.TblBank1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblBank1TableAdapter()
        Me.TblDESPANSARY1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblDESPANSARY1TableAdapter()
        Me.TblbranchTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter()
        Me.Tblbranch1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter()
        Me.TblMachineTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        Me.TblMachine1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.PopupContainerControl1 = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlMachine = New DevExpress.XtraGrid.GridControl()
        Me.GridViewMachine = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLOCATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeviceType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TabPane1 = New DevExpress.XtraBars.Navigation.TabPane()
        Me.TabNavigationPage1 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.TabNavigationPage2 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.textRes = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl()
        Me.AxFP_CLOCK1 = New AxFP_CLOCKLib.AxFP_CLOCK()
        Me.CheckEnrollFace = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEnroll = New DevExpress.XtraEditors.CheckEdit()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckValidity = New DevExpress.XtraEditors.CheckEdit()
        Me.ComboNepaliVEndYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliVEndMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliVEndDate = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliVStartYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliVStartMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliVStartDate = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.DateEdit5 = New DevExpress.XtraEditors.DateEdit()
        Me.DateEdit4 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditCard = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControlFPscanner = New DevExpress.XtraEditors.GroupControl()
        Me.picFPImg = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonInit = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonStop = New DevExpress.XtraEditors.SimpleButton()
        Me.TextFingerNo = New DevExpress.XtraEditors.TextEdit()
        Me.CheckCopyEmpty = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditTemplate = New DevExpress.XtraEditors.CheckEdit()
        Me.PopupContainerEdit1 = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonSave1 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControlZKDevice = New DevExpress.XtraGrid.GridControl()
        Me.GridViewZKDevice = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.VisitorPictureEdit = New DevExpress.XtraEditors.PictureEdit()
        Me.TextEditImage = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.DateEditLeaving.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditLeaving.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmployeeGroupBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GridLookUpEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDESPANSARYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPF1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditESI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNepaliYearLeave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNEpaliMonthLeave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNepaliDateLeave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.ComboBoxEditNepaliYearDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNEpaliMonthDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNepaliDateDOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNepaliYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.TextEditUid.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNepaliDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.TextEdit17.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit16.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit15.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblBankBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditBirth.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditBirth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditJoin.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditJoin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControl1.SuspendLayout()
        CType(Me.GridControlMachine, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewMachine, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabPane1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPane1.SuspendLayout()
        Me.TabNavigationPage1.SuspendLayout()
        Me.TabNavigationPage2.SuspendLayout()
        CType(Me.textRes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.AxFP_CLOCK1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEnrollFace.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEnroll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.CheckValidity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliVEndYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliVEndMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliVEndDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliVStartYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliVStartMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliVStartDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit5.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit4.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditCard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControlFPscanner, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControlFPscanner.SuspendLayout()
        CType(Me.picFPImg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextFingerNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckCopyEmpty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditTemplate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.GridControlZKDevice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewZKDevice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.VisitorPictureEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditImage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DateEditLeaving
        '
        Me.DateEditLeaving.EditValue = Nothing
        Me.DateEditLeaving.Location = New System.Drawing.Point(120, 338)
        Me.DateEditLeaving.Name = "DateEditLeaving"
        Me.DateEditLeaving.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditLeaving.Properties.Appearance.Options.UseFont = True
        Me.DateEditLeaving.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditLeaving.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditLeaving.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.DateEditLeaving.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditLeaving.Size = New System.Drawing.Size(135, 20)
        Me.DateEditLeaving.TabIndex = 13
        '
        'TextEdit6
        '
        Me.TextEdit6.Location = New System.Drawing.Point(120, 362)
        Me.TextEdit6.Name = "TextEdit6"
        Me.TextEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit6.Properties.Appearance.Options.UseFont = True
        Me.TextEdit6.Properties.MaxLength = 35
        Me.TextEdit6.Size = New System.Drawing.Size(135, 20)
        Me.TextEdit6.TabIndex = 14
        '
        'GridLookUpEdit5
        '
        Me.GridLookUpEdit5.Location = New System.Drawing.Point(117, 271)
        Me.GridLookUpEdit5.Name = "GridLookUpEdit5"
        Me.GridLookUpEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit5.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit5.Properties.DataSource = Me.TblGradeBindingSource
        Me.GridLookUpEdit5.Properties.DisplayMember = "GradeCode"
        Me.GridLookUpEdit5.Properties.NullText = ""
        Me.GridLookUpEdit5.Properties.ValueMember = "GradeCode"
        Me.GridLookUpEdit5.Properties.View = Me.GridView4
        Me.GridLookUpEdit5.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEdit5.TabIndex = 11
        '
        'TblGradeBindingSource
        '
        Me.TblGradeBindingSource.DataMember = "tblGrade"
        Me.TblGradeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colGradeCode, Me.colGradeName})
        Me.GridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView4.OptionsView.ShowGroupPanel = False
        '
        'colGradeCode
        '
        Me.colGradeCode.FieldName = "GradeCode"
        Me.colGradeCode.Name = "colGradeCode"
        Me.colGradeCode.Visible = True
        Me.colGradeCode.VisibleIndex = 0
        '
        'colGradeName
        '
        Me.colGradeName.FieldName = "GradeName"
        Me.colGradeName.Name = "colGradeName"
        Me.colGradeName.Visible = True
        Me.colGradeName.VisibleIndex = 1
        '
        'GridLookUpEdit4
        '
        Me.GridLookUpEdit4.Location = New System.Drawing.Point(117, 247)
        Me.GridLookUpEdit4.Name = "GridLookUpEdit4"
        Me.GridLookUpEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit4.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit4.Properties.DataSource = Me.EmployeeGroupBindingSource
        Me.GridLookUpEdit4.Properties.DisplayMember = "GroupId"
        Me.GridLookUpEdit4.Properties.NullText = ""
        Me.GridLookUpEdit4.Properties.ValueMember = "GroupId"
        Me.GridLookUpEdit4.Properties.View = Me.GridView3
        Me.GridLookUpEdit4.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEdit4.TabIndex = 10
        '
        'EmployeeGroupBindingSource
        '
        Me.EmployeeGroupBindingSource.DataMember = "EmployeeGroup"
        Me.EmployeeGroupBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colGroupId, Me.colGroupName})
        Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'colGroupId
        '
        Me.colGroupId.FieldName = "GroupId"
        Me.colGroupId.Name = "colGroupId"
        Me.colGroupId.Visible = True
        Me.colGroupId.VisibleIndex = 0
        '
        'colGroupName
        '
        Me.colGroupName.FieldName = "GroupName"
        Me.colGroupName.Name = "colGroupName"
        Me.colGroupName.Visible = True
        Me.colGroupName.VisibleIndex = 1
        '
        'GridLookUpEdit3
        '
        Me.GridLookUpEdit3.Location = New System.Drawing.Point(117, 223)
        Me.GridLookUpEdit3.Name = "GridLookUpEdit3"
        Me.GridLookUpEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit3.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit3.Properties.DataSource = Me.TblCatagoryBindingSource
        Me.GridLookUpEdit3.Properties.DisplayMember = "CAT"
        Me.GridLookUpEdit3.Properties.NullText = ""
        Me.GridLookUpEdit3.Properties.ValueMember = "CAT"
        Me.GridLookUpEdit3.Properties.View = Me.GridView2
        Me.GridLookUpEdit3.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEdit3.TabIndex = 9
        '
        'TblCatagoryBindingSource
        '
        Me.TblCatagoryBindingSource.DataMember = "tblCatagory"
        Me.TblCatagoryBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCAT, Me.colCATAGORYNAME})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'colCAT
        '
        Me.colCAT.FieldName = "CAT"
        Me.colCAT.Name = "colCAT"
        Me.colCAT.Visible = True
        Me.colCAT.VisibleIndex = 0
        '
        'colCATAGORYNAME
        '
        Me.colCATAGORYNAME.FieldName = "CATAGORYNAME"
        Me.colCATAGORYNAME.Name = "colCATAGORYNAME"
        Me.colCATAGORYNAME.Visible = True
        Me.colCATAGORYNAME.VisibleIndex = 1
        '
        'GridLookUpEdit2
        '
        Me.GridLookUpEdit2.Location = New System.Drawing.Point(117, 199)
        Me.GridLookUpEdit2.Name = "GridLookUpEdit2"
        Me.GridLookUpEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit2.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit2.Properties.DataSource = Me.TblDepartmentBindingSource
        Me.GridLookUpEdit2.Properties.DisplayMember = "DEPARTMENTCODE"
        Me.GridLookUpEdit2.Properties.NullText = ""
        Me.GridLookUpEdit2.Properties.ValueMember = "DEPARTMENTCODE"
        Me.GridLookUpEdit2.Properties.View = Me.GridView1
        Me.GridLookUpEdit2.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEdit2.TabIndex = 8
        '
        'TblDepartmentBindingSource
        '
        Me.TblDepartmentBindingSource.DataMember = "tblDepartment"
        Me.TblDepartmentBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDEPARTMENTCODE, Me.colDEPARTMENTNAME})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Visible = True
        Me.colDEPARTMENTCODE.VisibleIndex = 0
        '
        'colDEPARTMENTNAME
        '
        Me.colDEPARTMENTNAME.FieldName = "DEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Name = "colDEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Visible = True
        Me.colDEPARTMENTNAME.VisibleIndex = 1
        '
        'GridLookUpEdit1
        '
        Me.GridLookUpEdit1.Location = New System.Drawing.Point(117, 147)
        Me.GridLookUpEdit1.Name = "GridLookUpEdit1"
        Me.GridLookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit1.Properties.DataSource = Me.TblCompanyBindingSource
        Me.GridLookUpEdit1.Properties.DisplayMember = "COMPANYCODE"
        Me.GridLookUpEdit1.Properties.NullText = ""
        Me.GridLookUpEdit1.Properties.ValueMember = "COMPANYCODE"
        Me.GridLookUpEdit1.Properties.View = Me.GridLookUpEdit1View
        Me.GridLookUpEdit1.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEdit1.TabIndex = 6
        '
        'TblCompanyBindingSource
        '
        Me.TblCompanyBindingSource.DataMember = "tblCompany"
        Me.TblCompanyBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCOMPANYCODE, Me.colCOMPANYNAME})
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colCOMPANYCODE
        '
        Me.colCOMPANYCODE.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE.Name = "colCOMPANYCODE"
        Me.colCOMPANYCODE.Visible = True
        Me.colCOMPANYCODE.VisibleIndex = 0
        '
        'colCOMPANYNAME
        '
        Me.colCOMPANYNAME.FieldName = "COMPANYNAME"
        Me.colCOMPANYNAME.Name = "colCOMPANYNAME"
        Me.colCOMPANYNAME.Visible = True
        Me.colCOMPANYNAME.VisibleIndex = 1
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(5, 366)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl13.TabIndex = 16
        Me.LabelControl13.Text = "Reason(leaving)"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(5, 341)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(71, 14)
        Me.LabelControl12.TabIndex = 15
        Me.LabelControl12.Text = "Leaving Date"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(5, 299)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl11.TabIndex = 14
        Me.LabelControl11.Text = "Designation"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(5, 275)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(32, 14)
        Me.LabelControl10.TabIndex = 13
        Me.LabelControl10.Text = "Grade"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(5, 251)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(101, 14)
        Me.LabelControl9.TabIndex = 12
        Me.LabelControl9.Text = "Employee Group *"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(5, 227)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl8.TabIndex = 11
        Me.LabelControl8.Text = "Category *"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(5, 203)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(77, 14)
        Me.LabelControl7.TabIndex = 10
        Me.LabelControl7.Text = "Department *"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(5, 151)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl6.TabIndex = 9
        Me.LabelControl6.Text = "Company *"
        '
        'TextEdit4
        '
        Me.TextEdit4.Location = New System.Drawing.Point(117, 121)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit4.Properties.Appearance.Options.UseFont = True
        Me.TextEdit4.Properties.MaxLength = 30
        Me.TextEdit4.Size = New System.Drawing.Size(135, 20)
        Me.TextEdit4.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(5, 125)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(82, 14)
        Me.LabelControl5.TabIndex = 7
        Me.LabelControl5.Text = "Guardian Name"
        '
        'TextEdit3
        '
        Me.TextEdit3.Location = New System.Drawing.Point(117, 97)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit3.Properties.Appearance.Options.UseFont = True
        Me.TextEdit3.Properties.MaxLength = 25
        Me.TextEdit3.Size = New System.Drawing.Size(135, 20)
        Me.TextEdit3.TabIndex = 4
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(5, 101)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl4.TabIndex = 5
        Me.LabelControl4.Text = "Emp Name *"
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(117, 73)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit2.Properties.Mask.EditMask = "[A-Z0-9]*"
        Me.TextEdit2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEdit2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit2.Properties.MaxLength = 12
        Me.TextEdit2.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit2.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(5, 77)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(71, 14)
        Me.LabelControl3.TabIndex = 3
        Me.LabelControl3.Text = "Emp Code. *"
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(117, 49)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit1.Properties.MaxLength = 12
        Me.TextEdit1.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit1.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(5, 53)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "User No. *"
        '
        'ToggleSwitch1
        '
        Me.ToggleSwitch1.Location = New System.Drawing.Point(117, 21)
        Me.ToggleSwitch1.Name = "ToggleSwitch1"
        Me.ToggleSwitch1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch1.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch1.Properties.OffText = "No"
        Me.ToggleSwitch1.Properties.OnText = "Yes"
        Me.ToggleSwitch1.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch1.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(5, 26)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(34, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Active"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.GridLookUpEdit8)
        Me.GroupControl1.Controls.Add(Me.LabelControl37)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEdit6)
        Me.GroupControl1.Controls.Add(Me.TextEditPF)
        Me.GroupControl1.Controls.Add(Me.TextEditPF1)
        Me.GroupControl1.Controls.Add(Me.TextEditESI)
        Me.GroupControl1.Controls.Add(Me.LabelControl16)
        Me.GroupControl1.Controls.Add(Me.LabelControl15)
        Me.GroupControl1.Controls.Add(Me.TextEdit5)
        Me.GroupControl1.Controls.Add(Me.LabelControl14)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.ToggleSwitch1)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.TextEdit1)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEdit5)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEdit4)
        Me.GroupControl1.Controls.Add(Me.TextEdit2)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEdit3)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEdit2)
        Me.GroupControl1.Controls.Add(Me.TextEdit3)
        Me.GroupControl1.Controls.Add(Me.GridLookUpEdit1)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.TextEdit4)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.LabelControl11)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.LabelControl10)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Location = New System.Drawing.Point(7, 3)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(320, 406)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Office Details"
        '
        'GridLookUpEdit8
        '
        Me.GridLookUpEdit8.Location = New System.Drawing.Point(117, 173)
        Me.GridLookUpEdit8.Name = "GridLookUpEdit8"
        Me.GridLookUpEdit8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit8.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit8.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit8.Properties.DataSource = Me.TblbranchBindingSource
        Me.GridLookUpEdit8.Properties.DisplayMember = "BRANCHCODE"
        Me.GridLookUpEdit8.Properties.NullText = ""
        Me.GridLookUpEdit8.Properties.ValueMember = "BRANCHCODE"
        Me.GridLookUpEdit8.Properties.View = Me.GridView7
        Me.GridLookUpEdit8.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEdit8.TabIndex = 7
        '
        'TblbranchBindingSource
        '
        Me.TblbranchBindingSource.DataMember = "tblbranch"
        Me.TblbranchBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView7
        '
        Me.GridView7.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView7.Name = "GridView7"
        Me.GridView7.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView7.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.FieldName = "BRANCHCODE"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.FieldName = "BRANCHNAME"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        '
        'LabelControl37
        '
        Me.LabelControl37.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl37.Appearance.Options.UseFont = True
        Me.LabelControl37.Location = New System.Drawing.Point(5, 176)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl37.TabIndex = 29
        Me.LabelControl37.Text = "Location *"
        '
        'GridLookUpEdit6
        '
        Me.GridLookUpEdit6.Location = New System.Drawing.Point(117, 373)
        Me.GridLookUpEdit6.Name = "GridLookUpEdit6"
        Me.GridLookUpEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit6.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit6.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit6.Properties.DataSource = Me.TblDESPANSARYBindingSource
        Me.GridLookUpEdit6.Properties.DisplayMember = "DESPANSARYCODE"
        Me.GridLookUpEdit6.Properties.NullText = ""
        Me.GridLookUpEdit6.Properties.ValueMember = "DESPANSARYCODE"
        Me.GridLookUpEdit6.Properties.View = Me.GridView5
        Me.GridLookUpEdit6.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEdit6.TabIndex = 16
        '
        'TblDESPANSARYBindingSource
        '
        Me.TblDESPANSARYBindingSource.DataMember = "tblDESPANSARY"
        Me.TblDESPANSARYBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView5
        '
        Me.GridView5.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDESPANSARYCODE, Me.colDESPANSARYNAME})
        Me.GridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView5.Name = "GridView5"
        Me.GridView5.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView5.OptionsView.ShowGroupPanel = False
        '
        'colDESPANSARYCODE
        '
        Me.colDESPANSARYCODE.FieldName = "DESPANSARYCODE"
        Me.colDESPANSARYCODE.Name = "colDESPANSARYCODE"
        Me.colDESPANSARYCODE.Visible = True
        Me.colDESPANSARYCODE.VisibleIndex = 0
        '
        'colDESPANSARYNAME
        '
        Me.colDESPANSARYNAME.FieldName = "DESPANSARYNAME"
        Me.colDESPANSARYNAME.Name = "colDESPANSARYNAME"
        Me.colDESPANSARYNAME.Visible = True
        Me.colDESPANSARYNAME.VisibleIndex = 1
        '
        'TextEditPF
        '
        Me.TextEditPF.Location = New System.Drawing.Point(117, 322)
        Me.TextEditPF.Name = "TextEditPF"
        Me.TextEditPF.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPF.Properties.Appearance.Options.UseFont = True
        Me.TextEditPF.Properties.Mask.EditMask = "[A-Z0-9]*"
        Me.TextEditPF.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditPF.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditPF.Properties.MaxLength = 12
        Me.TextEditPF.Size = New System.Drawing.Size(135, 20)
        Me.TextEditPF.TabIndex = 13
        '
        'TextEditPF1
        '
        Me.TextEditPF1.Location = New System.Drawing.Point(258, 323)
        Me.TextEditPF1.Name = "TextEditPF1"
        Me.TextEditPF1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPF1.Properties.Appearance.Options.UseFont = True
        Me.TextEditPF1.Properties.Mask.EditMask = "#####"
        Me.TextEditPF1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditPF1.Properties.MaxLength = 5
        Me.TextEditPF1.Size = New System.Drawing.Size(54, 20)
        Me.TextEditPF1.TabIndex = 14
        '
        'TextEditESI
        '
        Me.TextEditESI.Location = New System.Drawing.Point(117, 348)
        Me.TextEditESI.Name = "TextEditESI"
        Me.TextEditESI.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditESI.Properties.Appearance.Options.UseFont = True
        Me.TextEditESI.Properties.Mask.EditMask = "[A-Z0-9]*"
        Me.TextEditESI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditESI.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditESI.Properties.MaxLength = 12
        Me.TextEditESI.Size = New System.Drawing.Size(135, 20)
        Me.TextEditESI.TabIndex = 15
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(5, 376)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl16.TabIndex = 28
        Me.LabelControl16.Text = "Dispensary"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(5, 351)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(49, 14)
        Me.LabelControl15.TabIndex = 27
        Me.LabelControl15.Text = "ESI No   "
        '
        'TextEdit5
        '
        Me.TextEdit5.Location = New System.Drawing.Point(117, 296)
        Me.TextEdit5.Name = "TextEdit5"
        Me.TextEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit5.Properties.Appearance.Options.UseFont = True
        Me.TextEdit5.Properties.MaxLength = 25
        Me.TextEdit5.Size = New System.Drawing.Size(135, 20)
        Me.TextEdit5.TabIndex = 12
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(5, 328)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(44, 14)
        Me.LabelControl14.TabIndex = 25
        Me.LabelControl14.Text = "PF No   "
        '
        'ComboBoxEditNepaliYearLeave
        '
        Me.ComboBoxEditNepaliYearLeave.Location = New System.Drawing.Point(240, 338)
        Me.ComboBoxEditNepaliYearLeave.Name = "ComboBoxEditNepaliYearLeave"
        Me.ComboBoxEditNepaliYearLeave.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYearLeave.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliYearLeave.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYearLeave.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliYearLeave.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliYearLeave.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboBoxEditNepaliYearLeave.Size = New System.Drawing.Size(61, 20)
        Me.ComboBoxEditNepaliYearLeave.TabIndex = 35
        '
        'ComboBoxEditNEpaliMonthLeave
        '
        Me.ComboBoxEditNEpaliMonthLeave.Location = New System.Drawing.Point(168, 338)
        Me.ComboBoxEditNEpaliMonthLeave.Name = "ComboBoxEditNEpaliMonthLeave"
        Me.ComboBoxEditNEpaliMonthLeave.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonthLeave.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonthLeave.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonthLeave.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonthLeave.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNEpaliMonthLeave.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboBoxEditNEpaliMonthLeave.Size = New System.Drawing.Size(66, 20)
        Me.ComboBoxEditNEpaliMonthLeave.TabIndex = 34
        '
        'ComboBoxEditNepaliDateLeave
        '
        Me.ComboBoxEditNepaliDateLeave.Location = New System.Drawing.Point(120, 338)
        Me.ComboBoxEditNepaliDateLeave.Name = "ComboBoxEditNepaliDateLeave"
        Me.ComboBoxEditNepaliDateLeave.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDateLeave.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliDateLeave.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDateLeave.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliDateLeave.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliDateLeave.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboBoxEditNepaliDateLeave.Size = New System.Drawing.Size(42, 20)
        Me.ComboBoxEditNepaliDateLeave.TabIndex = 33
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.ComboBoxEditNepaliYearLeave)
        Me.GroupControl2.Controls.Add(Me.ComboBoxEditNepaliYearDOB)
        Me.GroupControl2.Controls.Add(Me.ComboBoxEditNEpaliMonthLeave)
        Me.GroupControl2.Controls.Add(Me.ComboBoxEditNEpaliMonthDOB)
        Me.GroupControl2.Controls.Add(Me.ComboBoxEditNepaliDateLeave)
        Me.GroupControl2.Controls.Add(Me.ComboBoxEditNepaliDateDOB)
        Me.GroupControl2.Controls.Add(Me.ComboBoxEditNepaliYear)
        Me.GroupControl2.Controls.Add(Me.ComboBoxEditNEpaliMonth)
        Me.GroupControl2.Controls.Add(Me.PanelControl2)
        Me.GroupControl2.Controls.Add(Me.ComboBoxEditNepaliDate)
        Me.GroupControl2.Controls.Add(Me.PanelControl1)
        Me.GroupControl2.Controls.Add(Me.TextEdit15)
        Me.GroupControl2.Controls.Add(Me.TextEdit14)
        Me.GroupControl2.Controls.Add(Me.TextEdit13)
        Me.GroupControl2.Controls.Add(Me.TextEdit12)
        Me.GroupControl2.Controls.Add(Me.GridLookUpEdit7)
        Me.GroupControl2.Controls.Add(Me.DateEditLeaving)
        Me.GroupControl2.Controls.Add(Me.ComboBoxEdit2)
        Me.GroupControl2.Controls.Add(Me.TextEdit11)
        Me.GroupControl2.Controls.Add(Me.TextEdit6)
        Me.GroupControl2.Controls.Add(Me.TextEdit10)
        Me.GroupControl2.Controls.Add(Me.ComboBoxEdit1)
        Me.GroupControl2.Controls.Add(Me.ToggleSwitch2)
        Me.GroupControl2.Controls.Add(Me.DateEditBirth)
        Me.GroupControl2.Controls.Add(Me.DateEditJoin)
        Me.GroupControl2.Controls.Add(Me.LabelControl28)
        Me.GroupControl2.Controls.Add(Me.LabelControl27)
        Me.GroupControl2.Controls.Add(Me.LabelControl26)
        Me.GroupControl2.Controls.Add(Me.LabelControl25)
        Me.GroupControl2.Controls.Add(Me.LabelControl24)
        Me.GroupControl2.Controls.Add(Me.LabelControl23)
        Me.GroupControl2.Controls.Add(Me.LabelControl22)
        Me.GroupControl2.Controls.Add(Me.LabelControl21)
        Me.GroupControl2.Controls.Add(Me.LabelControl13)
        Me.GroupControl2.Controls.Add(Me.LabelControl20)
        Me.GroupControl2.Controls.Add(Me.LabelControl19)
        Me.GroupControl2.Controls.Add(Me.LabelControl12)
        Me.GroupControl2.Controls.Add(Me.LabelControl18)
        Me.GroupControl2.Controls.Add(Me.LabelControl17)
        Me.GroupControl2.Location = New System.Drawing.Point(334, 3)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(523, 406)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Personal Details"
        '
        'ComboBoxEditNepaliYearDOB
        '
        Me.ComboBoxEditNepaliYearDOB.Location = New System.Drawing.Point(221, 53)
        Me.ComboBoxEditNepaliYearDOB.Name = "ComboBoxEditNepaliYearDOB"
        Me.ComboBoxEditNepaliYearDOB.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYearDOB.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliYearDOB.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYearDOB.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliYearDOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliYearDOB.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboBoxEditNepaliYearDOB.Size = New System.Drawing.Size(61, 20)
        Me.ComboBoxEditNepaliYearDOB.TabIndex = 35
        '
        'ComboBoxEditNEpaliMonthDOB
        '
        Me.ComboBoxEditNEpaliMonthDOB.Location = New System.Drawing.Point(149, 53)
        Me.ComboBoxEditNEpaliMonthDOB.Name = "ComboBoxEditNEpaliMonthDOB"
        Me.ComboBoxEditNEpaliMonthDOB.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonthDOB.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonthDOB.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonthDOB.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonthDOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNEpaliMonthDOB.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboBoxEditNEpaliMonthDOB.Size = New System.Drawing.Size(66, 20)
        Me.ComboBoxEditNEpaliMonthDOB.TabIndex = 34
        '
        'ComboBoxEditNepaliDateDOB
        '
        Me.ComboBoxEditNepaliDateDOB.Location = New System.Drawing.Point(101, 53)
        Me.ComboBoxEditNepaliDateDOB.Name = "ComboBoxEditNepaliDateDOB"
        Me.ComboBoxEditNepaliDateDOB.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDateDOB.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliDateDOB.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDateDOB.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliDateDOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliDateDOB.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboBoxEditNepaliDateDOB.Size = New System.Drawing.Size(42, 20)
        Me.ComboBoxEditNepaliDateDOB.TabIndex = 33
        '
        'ComboBoxEditNepaliYear
        '
        Me.ComboBoxEditNepaliYear.Location = New System.Drawing.Point(221, 26)
        Me.ComboBoxEditNepaliYear.Name = "ComboBoxEditNepaliYear"
        Me.ComboBoxEditNepaliYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYear.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboBoxEditNepaliYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboBoxEditNepaliYear.TabIndex = 32
        '
        'ComboBoxEditNEpaliMonth
        '
        Me.ComboBoxEditNEpaliMonth.Location = New System.Drawing.Point(149, 26)
        Me.ComboBoxEditNEpaliMonth.Name = "ComboBoxEditNEpaliMonth"
        Me.ComboBoxEditNEpaliMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNEpaliMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboBoxEditNEpaliMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboBoxEditNEpaliMonth.TabIndex = 31
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.TextEditUid)
        Me.PanelControl2.Controls.Add(Me.TextEdit19)
        Me.PanelControl2.Controls.Add(Me.LabelControl32)
        Me.PanelControl2.Controls.Add(Me.LabelControl33)
        Me.PanelControl2.Controls.Add(Me.MemoEdit2)
        Me.PanelControl2.Controls.Add(Me.LabelControl34)
        Me.PanelControl2.Location = New System.Drawing.Point(287, 185)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(231, 150)
        Me.PanelControl2.TabIndex = 14
        '
        'TextEditUid
        '
        Me.TextEditUid.Location = New System.Drawing.Point(104, 118)
        Me.TextEditUid.Name = "TextEditUid"
        Me.TextEditUid.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditUid.Properties.Appearance.Options.UseFont = True
        Me.TextEditUid.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditUid.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditUid.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditUid.Properties.MaxLength = 12
        Me.TextEditUid.Size = New System.Drawing.Size(122, 20)
        Me.TextEditUid.TabIndex = 3
        '
        'TextEdit19
        '
        Me.TextEdit19.Location = New System.Drawing.Point(104, 92)
        Me.TextEdit19.Name = "TextEdit19"
        Me.TextEdit19.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit19.Properties.Appearance.Options.UseFont = True
        Me.TextEdit19.Properties.Mask.EditMask = "[A-Z0-9]*"
        Me.TextEdit19.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEdit19.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit19.Properties.MaxLength = 10
        Me.TextEdit19.Size = New System.Drawing.Size(122, 20)
        Me.TextEdit19.TabIndex = 2
        '
        'LabelControl32
        '
        Me.LabelControl32.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl32.Appearance.Options.UseFont = True
        Me.LabelControl32.Location = New System.Drawing.Point(6, 120)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(43, 14)
        Me.LabelControl32.TabIndex = 5
        Me.LabelControl32.Text = "UID No."
        '
        'LabelControl33
        '
        Me.LabelControl33.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl33.Appearance.Options.UseFont = True
        Me.LabelControl33.Location = New System.Drawing.Point(6, 94)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(23, 14)
        Me.LabelControl33.TabIndex = 4
        Me.LabelControl33.Text = "PAN"
        '
        'MemoEdit2
        '
        Me.MemoEdit2.Location = New System.Drawing.Point(6, 23)
        Me.MemoEdit2.Name = "MemoEdit2"
        Me.MemoEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.MemoEdit2.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit2.Properties.MaxLength = 100
        Me.MemoEdit2.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MemoEdit2.Size = New System.Drawing.Size(220, 63)
        Me.MemoEdit2.TabIndex = 1
        '
        'LabelControl34
        '
        Me.LabelControl34.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl34.Appearance.Options.UseFont = True
        Me.LabelControl34.Location = New System.Drawing.Point(5, 5)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(106, 14)
        Me.LabelControl34.TabIndex = 2
        Me.LabelControl34.Text = "Temporary Address"
        '
        'ComboBoxEditNepaliDate
        '
        Me.ComboBoxEditNepaliDate.Location = New System.Drawing.Point(101, 26)
        Me.ComboBoxEditNepaliDate.Name = "ComboBoxEditNepaliDate"
        Me.ComboBoxEditNepaliDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDate.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliDate.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDate.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliDate.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboBoxEditNepaliDate.Size = New System.Drawing.Size(42, 20)
        Me.ComboBoxEditNepaliDate.TabIndex = 30
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.TextEdit17)
        Me.PanelControl1.Controls.Add(Me.TextEdit16)
        Me.PanelControl1.Controls.Add(Me.LabelControl31)
        Me.PanelControl1.Controls.Add(Me.LabelControl30)
        Me.PanelControl1.Controls.Add(Me.MemoEdit1)
        Me.PanelControl1.Controls.Add(Me.LabelControl29)
        Me.PanelControl1.Location = New System.Drawing.Point(287, 29)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(231, 150)
        Me.PanelControl1.TabIndex = 13
        '
        'TextEdit17
        '
        Me.TextEdit17.Location = New System.Drawing.Point(104, 118)
        Me.TextEdit17.Name = "TextEdit17"
        Me.TextEdit17.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit17.Properties.Appearance.Options.UseFont = True
        Me.TextEdit17.Properties.Mask.EditMask = "[0-9\,]*"
        Me.TextEdit17.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEdit17.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit17.Properties.MaxLength = 50
        Me.TextEdit17.Size = New System.Drawing.Size(122, 20)
        Me.TextEdit17.TabIndex = 3
        '
        'TextEdit16
        '
        Me.TextEdit16.Location = New System.Drawing.Point(104, 92)
        Me.TextEdit16.Name = "TextEdit16"
        Me.TextEdit16.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit16.Properties.Appearance.Options.UseFont = True
        Me.TextEdit16.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEdit16.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEdit16.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit16.Properties.MaxLength = 7
        Me.TextEdit16.Size = New System.Drawing.Size(122, 20)
        Me.TextEdit16.TabIndex = 2
        '
        'LabelControl31
        '
        Me.LabelControl31.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl31.Appearance.Options.UseFont = True
        Me.LabelControl31.Location = New System.Drawing.Point(6, 120)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(34, 14)
        Me.LabelControl31.TabIndex = 5
        Me.LabelControl31.Text = "Mobile"
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl30.Appearance.Options.UseFont = True
        Me.LabelControl30.Location = New System.Drawing.Point(6, 94)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(44, 14)
        Me.LabelControl30.TabIndex = 4
        Me.LabelControl30.Text = "PinCode"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Location = New System.Drawing.Point(6, 23)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100
        Me.MemoEdit1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MemoEdit1.Size = New System.Drawing.Size(220, 63)
        Me.MemoEdit1.TabIndex = 1
        '
        'LabelControl29
        '
        Me.LabelControl29.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl29.Appearance.Options.UseFont = True
        Me.LabelControl29.Location = New System.Drawing.Point(5, 5)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(107, 14)
        Me.LabelControl29.TabIndex = 2
        Me.LabelControl29.Text = "Permanent Address"
        '
        'TextEdit15
        '
        Me.TextEdit15.Location = New System.Drawing.Point(120, 312)
        Me.TextEdit15.Name = "TextEdit15"
        Me.TextEdit15.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit15.Properties.Appearance.Options.UseFont = True
        Me.TextEdit15.Properties.MaxLength = 15
        Me.TextEdit15.Size = New System.Drawing.Size(135, 20)
        Me.TextEdit15.TabIndex = 12
        '
        'TextEdit14
        '
        Me.TextEdit14.Location = New System.Drawing.Point(120, 286)
        Me.TextEdit14.Name = "TextEdit14"
        Me.TextEdit14.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit14.Properties.Appearance.Options.UseFont = True
        Me.TextEdit14.Properties.MaxLength = 10
        Me.TextEdit14.Size = New System.Drawing.Size(135, 20)
        Me.TextEdit14.TabIndex = 11
        '
        'TextEdit13
        '
        Me.TextEdit13.Location = New System.Drawing.Point(120, 259)
        Me.TextEdit13.Name = "TextEdit13"
        Me.TextEdit13.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit13.Properties.Appearance.Options.UseFont = True
        Me.TextEdit13.Properties.MaxLength = 50
        Me.TextEdit13.Size = New System.Drawing.Size(135, 20)
        Me.TextEdit13.TabIndex = 10
        '
        'TextEdit12
        '
        Me.TextEdit12.Location = New System.Drawing.Point(120, 232)
        Me.TextEdit12.Name = "TextEdit12"
        Me.TextEdit12.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit12.Properties.Appearance.Options.UseFont = True
        Me.TextEdit12.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEdit12.Properties.MaxLength = 25
        Me.TextEdit12.Size = New System.Drawing.Size(135, 20)
        Me.TextEdit12.TabIndex = 9
        '
        'GridLookUpEdit7
        '
        Me.GridLookUpEdit7.Location = New System.Drawing.Point(120, 206)
        Me.GridLookUpEdit7.Name = "GridLookUpEdit7"
        Me.GridLookUpEdit7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit7.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit7.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit7.Properties.DataSource = Me.TblBankBindingSource
        Me.GridLookUpEdit7.Properties.DisplayMember = "BANKCODE"
        Me.GridLookUpEdit7.Properties.NullText = ""
        Me.GridLookUpEdit7.Properties.ValueMember = "BANKCODE"
        Me.GridLookUpEdit7.Properties.View = Me.GridView6
        Me.GridLookUpEdit7.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEdit7.TabIndex = 8
        '
        'TblBankBindingSource
        '
        Me.TblBankBindingSource.DataMember = "tblBank"
        Me.TblBankBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView6
        '
        Me.GridView6.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBANKCODE, Me.colBANKNAME})
        Me.GridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView6.Name = "GridView6"
        Me.GridView6.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView6.OptionsView.ShowGroupPanel = False
        '
        'colBANKCODE
        '
        Me.colBANKCODE.FieldName = "BANKCODE"
        Me.colBANKCODE.Name = "colBANKCODE"
        Me.colBANKCODE.Visible = True
        Me.colBANKCODE.VisibleIndex = 0
        '
        'colBANKNAME
        '
        Me.colBANKNAME.FieldName = "BANKNAME"
        Me.colBANKNAME.Name = "colBANKNAME"
        Me.colBANKNAME.Visible = True
        Me.colBANKNAME.VisibleIndex = 1
        '
        'ComboBoxEdit2
        '
        Me.ComboBoxEdit2.EditValue = "Male"
        Me.ComboBoxEdit2.Location = New System.Drawing.Point(120, 180)
        Me.ComboBoxEdit2.Name = "ComboBoxEdit2"
        Me.ComboBoxEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit2.Properties.Items.AddRange(New Object() {"Male", "Female"})
        Me.ComboBoxEdit2.Size = New System.Drawing.Size(100, 20)
        Me.ComboBoxEdit2.TabIndex = 7
        '
        'TextEdit11
        '
        Me.TextEdit11.Location = New System.Drawing.Point(120, 154)
        Me.TextEdit11.Name = "TextEdit11"
        Me.TextEdit11.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit11.Properties.Appearance.Options.UseFont = True
        Me.TextEdit11.Properties.MaxLength = 20
        Me.TextEdit11.Size = New System.Drawing.Size(135, 20)
        Me.TextEdit11.TabIndex = 6
        '
        'TextEdit10
        '
        Me.TextEdit10.Location = New System.Drawing.Point(120, 128)
        Me.TextEdit10.Name = "TextEdit10"
        Me.TextEdit10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit10.Properties.Appearance.Options.UseFont = True
        Me.TextEdit10.Properties.MaxLength = 20
        Me.TextEdit10.Size = New System.Drawing.Size(135, 20)
        Me.TextEdit10.TabIndex = 5
        '
        'ComboBoxEdit1
        '
        Me.ComboBoxEdit1.EditValue = "N/A"
        Me.ComboBoxEdit1.Location = New System.Drawing.Point(120, 101)
        Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
        Me.ComboBoxEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit1.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit1.Properties.Items.AddRange(New Object() {"N/A", "O+ ", "O- ", "A+ ", "A- ", "B+ ", "B- ", "AB+", "AB-"})
        Me.ComboBoxEdit1.Size = New System.Drawing.Size(100, 20)
        Me.ComboBoxEdit1.TabIndex = 4
        '
        'ToggleSwitch2
        '
        Me.ToggleSwitch2.Location = New System.Drawing.Point(120, 74)
        Me.ToggleSwitch2.Name = "ToggleSwitch2"
        Me.ToggleSwitch2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch2.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch2.Properties.OffText = "No"
        Me.ToggleSwitch2.Properties.OnText = "Yes"
        Me.ToggleSwitch2.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch2.TabIndex = 3
        '
        'DateEditBirth
        '
        Me.DateEditBirth.EditValue = Nothing
        Me.DateEditBirth.Location = New System.Drawing.Point(120, 53)
        Me.DateEditBirth.Name = "DateEditBirth"
        Me.DateEditBirth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditBirth.Properties.Appearance.Options.UseFont = True
        Me.DateEditBirth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditBirth.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditBirth.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.DateEditBirth.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditBirth.Size = New System.Drawing.Size(135, 20)
        Me.DateEditBirth.TabIndex = 2
        '
        'DateEditJoin
        '
        Me.DateEditJoin.EditValue = Nothing
        Me.DateEditJoin.Location = New System.Drawing.Point(120, 26)
        Me.DateEditJoin.Name = "DateEditJoin"
        Me.DateEditJoin.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditJoin.Properties.Appearance.Options.UseFont = True
        Me.DateEditJoin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditJoin.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditJoin.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.DateEditJoin.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditJoin.Size = New System.Drawing.Size(135, 20)
        Me.DateEditJoin.TabIndex = 1
        '
        'LabelControl28
        '
        Me.LabelControl28.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl28.Appearance.Options.UseFont = True
        Me.LabelControl28.Location = New System.Drawing.Point(5, 315)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(62, 14)
        Me.LabelControl28.TabIndex = 12
        Me.LabelControl28.Text = "Vehicle No."
        '
        'LabelControl27
        '
        Me.LabelControl27.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl27.Appearance.Options.UseFont = True
        Me.LabelControl27.Location = New System.Drawing.Point(5, 289)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl27.TabIndex = 11
        Me.LabelControl27.Text = "Bus Route "
        '
        'LabelControl26
        '
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl26.Appearance.Options.UseFont = True
        Me.LabelControl26.Location = New System.Drawing.Point(5, 262)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(30, 14)
        Me.LabelControl26.TabIndex = 10
        Me.LabelControl26.Text = "E-Mail"
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl25.Appearance.Options.UseFont = True
        Me.LabelControl25.Location = New System.Drawing.Point(5, 235)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(38, 14)
        Me.LabelControl25.TabIndex = 9
        Me.LabelControl25.Text = "A/c No"
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl24.Appearance.Options.UseFont = True
        Me.LabelControl24.Location = New System.Drawing.Point(5, 209)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl24.TabIndex = 8
        Me.LabelControl24.Text = "Bank Name"
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(5, 183)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(40, 14)
        Me.LabelControl23.TabIndex = 7
        Me.LabelControl23.Text = "Gender"
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Location = New System.Drawing.Point(5, 157)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl22.TabIndex = 6
        Me.LabelControl22.Text = "Experience"
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(5, 131)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(65, 14)
        Me.LabelControl21.TabIndex = 5
        Me.LabelControl21.Text = "Qualification"
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Location = New System.Drawing.Point(5, 104)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(71, 14)
        Me.LabelControl20.TabIndex = 4
        Me.LabelControl20.Text = "Blood Group "
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(5, 79)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(39, 14)
        Me.LabelControl19.TabIndex = 3
        Me.LabelControl19.Text = "Married"
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(5, 56)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl18.TabIndex = 2
        Me.LabelControl18.Text = "Date of Birth"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(5, 29)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(82, 14)
        Me.LabelControl17.TabIndex = 1
        Me.LabelControl17.Text = "Date of Joining"
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(706, 8)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 101
        Me.SimpleButtonSave.Text = "Save"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(787, 8)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 102
        Me.SimpleButton2.Text = "Cancel"
        '
        'TblCompanyTableAdapter
        '
        Me.TblCompanyTableAdapter.ClearBeforeFill = True
        '
        'TblCompany1TableAdapter1
        '
        Me.TblCompany1TableAdapter1.ClearBeforeFill = True
        '
        'TblDepartmentTableAdapter
        '
        Me.TblDepartmentTableAdapter.ClearBeforeFill = True
        '
        'TblDepartment1TableAdapter1
        '
        Me.TblDepartment1TableAdapter1.ClearBeforeFill = True
        '
        'TblCatagoryTableAdapter
        '
        Me.TblCatagoryTableAdapter.ClearBeforeFill = True
        '
        'TblCatagory1TableAdapter1
        '
        Me.TblCatagory1TableAdapter1.ClearBeforeFill = True
        '
        'EmployeeGroupTableAdapter
        '
        Me.EmployeeGroupTableAdapter.ClearBeforeFill = True
        '
        'EmployeeGroup1TableAdapter1
        '
        Me.EmployeeGroup1TableAdapter1.ClearBeforeFill = True
        '
        'TblGradeTableAdapter
        '
        Me.TblGradeTableAdapter.ClearBeforeFill = True
        '
        'TblGrade1TableAdapter1
        '
        Me.TblGrade1TableAdapter1.ClearBeforeFill = True
        '
        'TblDESPANSARYTableAdapter
        '
        Me.TblDESPANSARYTableAdapter.ClearBeforeFill = True
        '
        'TblBankTableAdapter
        '
        Me.TblBankTableAdapter.ClearBeforeFill = True
        '
        'TblBank1TableAdapter1
        '
        Me.TblBank1TableAdapter1.ClearBeforeFill = True
        '
        'TblDESPANSARY1TableAdapter1
        '
        Me.TblDESPANSARY1TableAdapter1.ClearBeforeFill = True
        '
        'TblbranchTableAdapter
        '
        Me.TblbranchTableAdapter.ClearBeforeFill = True
        '
        'Tblbranch1TableAdapter1
        '
        Me.Tblbranch1TableAdapter1.ClearBeforeFill = True
        '
        'TblMachineTableAdapter
        '
        Me.TblMachineTableAdapter.ClearBeforeFill = True
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.PopupContainerControl1)
        Me.SidePanel1.Controls.Add(Me.SimpleButton2)
        Me.SidePanel1.Controls.Add(Me.SimpleButtonSave)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(0, 468)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(883, 43)
        Me.SidePanel1.TabIndex = 103
        Me.SidePanel1.Text = "SidePanel1"
        '
        'PopupContainerControl1
        '
        Me.PopupContainerControl1.Controls.Add(Me.GridControlMachine)
        Me.PopupContainerControl1.Location = New System.Drawing.Point(30, 6)
        Me.PopupContainerControl1.Name = "PopupContainerControl1"
        Me.PopupContainerControl1.Size = New System.Drawing.Size(340, 175)
        Me.PopupContainerControl1.TabIndex = 103
        '
        'GridControlMachine
        '
        Me.GridControlMachine.DataSource = Me.TblMachineBindingSource
        Me.GridControlMachine.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlMachine.Location = New System.Drawing.Point(0, 0)
        Me.GridControlMachine.MainView = Me.GridViewMachine
        Me.GridControlMachine.Name = "GridControlMachine"
        Me.GridControlMachine.Size = New System.Drawing.Size(340, 175)
        Me.GridControlMachine.TabIndex = 0
        Me.GridControlMachine.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewMachine})
        '
        'GridViewMachine
        '
        Me.GridViewMachine.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID_NO, Me.colLOCATION, Me.colbranch, Me.colDeviceType, Me.colA_R, Me.GridColumn3})
        Me.GridViewMachine.GridControl = Me.GridControlMachine
        Me.GridViewMachine.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridViewMachine.Name = "GridViewMachine"
        Me.GridViewMachine.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewMachine.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewMachine.OptionsBehavior.Editable = False
        Me.GridViewMachine.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewMachine.OptionsSelection.MultiSelect = True
        Me.GridViewMachine.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'colID_NO
        '
        Me.colID_NO.FieldName = "ID_NO"
        Me.colID_NO.Name = "colID_NO"
        Me.colID_NO.Visible = True
        Me.colID_NO.VisibleIndex = 1
        '
        'colLOCATION
        '
        Me.colLOCATION.FieldName = "LOCATION"
        Me.colLOCATION.Name = "colLOCATION"
        Me.colLOCATION.Visible = True
        Me.colLOCATION.VisibleIndex = 3
        '
        'colbranch
        '
        Me.colbranch.Caption = "GridColumn1"
        Me.colbranch.FieldName = "branch"
        Me.colbranch.Name = "colbranch"
        Me.colbranch.Visible = True
        Me.colbranch.VisibleIndex = 2
        '
        'colDeviceType
        '
        Me.colDeviceType.Caption = "GridColumn1"
        Me.colDeviceType.FieldName = "DeviceType"
        Me.colDeviceType.Name = "colDeviceType"
        '
        'colA_R
        '
        Me.colA_R.FieldName = "A_R"
        Me.colA_R.Name = "colA_R"
        '
        'GridColumn3
        '
        Me.GridColumn3.FieldName = "Purpose"
        Me.GridColumn3.Name = "GridColumn3"
        '
        'TabPane1
        '
        Me.TabPane1.AllowTransitionAnimation = DevExpress.Utils.DefaultBoolean.[True]
        Me.TabPane1.Appearance.FontStyleDelta = System.Drawing.FontStyle.Italic
        Me.TabPane1.Appearance.Options.UseFont = True
        Me.TabPane1.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Underline)
        Me.TabPane1.AppearanceButton.Hovered.Options.UseFont = True
        Me.TabPane1.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TabPane1.AppearanceButton.Normal.Options.UseFont = True
        Me.TabPane1.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TabPane1.AppearanceButton.Pressed.Options.UseFont = True
        Me.TabPane1.Controls.Add(Me.TabNavigationPage1)
        Me.TabPane1.Controls.Add(Me.TabNavigationPage2)
        Me.TabPane1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabPane1.Location = New System.Drawing.Point(0, 0)
        Me.TabPane1.LookAndFeel.SkinName = "iMaginary"
        Me.TabPane1.Name = "TabPane1"
        Me.TabPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.TabNavigationPage1, Me.TabNavigationPage2})
        Me.TabPane1.RegularSize = New System.Drawing.Size(883, 468)
        Me.TabPane1.SelectedPage = Me.TabNavigationPage1
        Me.TabPane1.Size = New System.Drawing.Size(883, 468)
        Me.TabPane1.TabIndex = 104
        Me.TabPane1.Text = "Template"
        Me.TabPane1.TransitionType = DevExpress.Utils.Animation.Transitions.Cover
        '
        'TabNavigationPage1
        '
        Me.TabNavigationPage1.Caption = "Office Details"
        Me.TabNavigationPage1.Controls.Add(Me.GroupControl1)
        Me.TabNavigationPage1.Controls.Add(Me.GroupControl2)
        Me.TabNavigationPage1.Name = "TabNavigationPage1"
        Me.TabNavigationPage1.Size = New System.Drawing.Size(863, 423)
        '
        'TabNavigationPage2
        '
        Me.TabNavigationPage2.Caption = "Template"
        Me.TabNavigationPage2.Controls.Add(Me.textRes)
        Me.TabNavigationPage2.Controls.Add(Me.GroupControl6)
        Me.TabNavigationPage2.Controls.Add(Me.SimpleButtonSave1)
        Me.TabNavigationPage2.Controls.Add(Me.GroupControl5)
        Me.TabNavigationPage2.Controls.Add(Me.GroupControl4)
        Me.TabNavigationPage2.Name = "TabNavigationPage2"
        Me.TabNavigationPage2.Size = New System.Drawing.Size(863, 423)
        '
        'textRes
        '
        Me.textRes.Location = New System.Drawing.Point(10, 454)
        Me.textRes.Name = "textRes"
        Me.textRes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.textRes.Properties.Appearance.Options.UseFont = True
        Me.textRes.Properties.ReadOnly = True
        Me.textRes.Size = New System.Drawing.Size(190, 20)
        Me.textRes.TabIndex = 62
        Me.textRes.Visible = False
        '
        'GroupControl6
        '
        Me.GroupControl6.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl6.AppearanceCaption.Options.UseFont = True
        Me.GroupControl6.Controls.Add(Me.AxFP_CLOCK1)
        Me.GroupControl6.Controls.Add(Me.CheckEnrollFace)
        Me.GroupControl6.Controls.Add(Me.CheckEnroll)
        Me.GroupControl6.Controls.Add(Me.GroupControl3)
        Me.GroupControl6.Controls.Add(Me.LabelControl42)
        Me.GroupControl6.Controls.Add(Me.TextEditCard)
        Me.GroupControl6.Controls.Add(Me.GroupControlFPscanner)
        Me.GroupControl6.Controls.Add(Me.CheckCopyEmpty)
        Me.GroupControl6.Controls.Add(Me.CheckEditTemplate)
        Me.GroupControl6.Controls.Add(Me.PopupContainerEdit1)
        Me.GroupControl6.Controls.Add(Me.LabelControl38)
        Me.GroupControl6.Location = New System.Drawing.Point(279, 3)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(581, 420)
        Me.GroupControl6.TabIndex = 61
        Me.GroupControl6.Text = "Device Transfer"
        '
        'AxFP_CLOCK1
        '
        Me.AxFP_CLOCK1.Enabled = True
        Me.AxFP_CLOCK1.Location = New System.Drawing.Point(373, 332)
        Me.AxFP_CLOCK1.Name = "AxFP_CLOCK1"
        Me.AxFP_CLOCK1.OcxState = CType(resources.GetObject("AxFP_CLOCK1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxFP_CLOCK1.Size = New System.Drawing.Size(60, 44)
        Me.AxFP_CLOCK1.TabIndex = 66
        Me.AxFP_CLOCK1.Visible = False
        '
        'CheckEnrollFace
        '
        Me.CheckEnrollFace.Location = New System.Drawing.Point(421, 38)
        Me.CheckEnrollFace.Name = "CheckEnrollFace"
        Me.CheckEnrollFace.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEnrollFace.Properties.Appearance.Options.UseFont = True
        Me.CheckEnrollFace.Properties.Caption = "Enroll Face(ZK)"
        Me.CheckEnrollFace.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEnrollFace.Properties.RadioGroupIndex = 0
        Me.CheckEnrollFace.Size = New System.Drawing.Size(122, 19)
        Me.CheckEnrollFace.TabIndex = 65
        Me.CheckEnrollFace.TabStop = False
        '
        'CheckEnroll
        '
        Me.CheckEnroll.Location = New System.Drawing.Point(279, 38)
        Me.CheckEnroll.Name = "CheckEnroll"
        Me.CheckEnroll.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEnroll.Properties.Appearance.Options.UseFont = True
        Me.CheckEnroll.Properties.Caption = "Enroll Finger(ZK)"
        Me.CheckEnroll.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEnroll.Properties.RadioGroupIndex = 0
        Me.CheckEnroll.Size = New System.Drawing.Size(136, 19)
        Me.CheckEnroll.TabIndex = 64
        Me.CheckEnroll.TabStop = False
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.CheckValidity)
        Me.GroupControl3.Controls.Add(Me.ComboNepaliVEndYear)
        Me.GroupControl3.Controls.Add(Me.ComboNepaliVEndMonth)
        Me.GroupControl3.Controls.Add(Me.ComboNepaliVEndDate)
        Me.GroupControl3.Controls.Add(Me.ComboNepaliVStartYear)
        Me.GroupControl3.Controls.Add(Me.ComboNepaliVStartMonth)
        Me.GroupControl3.Controls.Add(Me.ComboNepaliVStartDate)
        Me.GroupControl3.Controls.Add(Me.DateEdit5)
        Me.GroupControl3.Controls.Add(Me.DateEdit4)
        Me.GroupControl3.Controls.Add(Me.LabelControl36)
        Me.GroupControl3.Controls.Add(Me.LabelControl35)
        Me.GroupControl3.Location = New System.Drawing.Point(326, 117)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(247, 126)
        Me.GroupControl3.TabIndex = 63
        Me.GroupControl3.Text = "Validity"
        '
        'CheckValidity
        '
        Me.CheckValidity.Location = New System.Drawing.Point(4, 29)
        Me.CheckValidity.Name = "CheckValidity"
        Me.CheckValidity.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckValidity.Properties.Appearance.Options.UseFont = True
        Me.CheckValidity.Properties.Caption = "Assign Validity"
        Me.CheckValidity.Size = New System.Drawing.Size(121, 19)
        Me.CheckValidity.TabIndex = 55
        '
        'ComboNepaliVEndYear
        '
        Me.ComboNepaliVEndYear.Location = New System.Drawing.Point(167, 75)
        Me.ComboNepaliVEndYear.Name = "ComboNepaliVEndYear"
        Me.ComboNepaliVEndYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVEndYear.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliVEndYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVEndYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliVEndYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliVEndYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliVEndYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliVEndYear.TabIndex = 44
        '
        'ComboNepaliVEndMonth
        '
        Me.ComboNepaliVEndMonth.Location = New System.Drawing.Point(95, 75)
        Me.ComboNepaliVEndMonth.Name = "ComboNepaliVEndMonth"
        Me.ComboNepaliVEndMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVEndMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliVEndMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVEndMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliVEndMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliVEndMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNepaliVEndMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboNepaliVEndMonth.TabIndex = 43
        '
        'ComboNepaliVEndDate
        '
        Me.ComboNepaliVEndDate.Location = New System.Drawing.Point(47, 75)
        Me.ComboNepaliVEndDate.Name = "ComboNepaliVEndDate"
        Me.ComboNepaliVEndDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVEndDate.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliVEndDate.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVEndDate.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliVEndDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliVEndDate.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliVEndDate.Size = New System.Drawing.Size(42, 20)
        Me.ComboNepaliVEndDate.TabIndex = 42
        '
        'ComboNepaliVStartYear
        '
        Me.ComboNepaliVStartYear.Location = New System.Drawing.Point(167, 51)
        Me.ComboNepaliVStartYear.Name = "ComboNepaliVStartYear"
        Me.ComboNepaliVStartYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVStartYear.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliVStartYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVStartYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliVStartYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliVStartYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliVStartYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliVStartYear.TabIndex = 41
        '
        'ComboNepaliVStartMonth
        '
        Me.ComboNepaliVStartMonth.Location = New System.Drawing.Point(95, 51)
        Me.ComboNepaliVStartMonth.Name = "ComboNepaliVStartMonth"
        Me.ComboNepaliVStartMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVStartMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliVStartMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVStartMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliVStartMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliVStartMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNepaliVStartMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboNepaliVStartMonth.TabIndex = 40
        '
        'ComboNepaliVStartDate
        '
        Me.ComboNepaliVStartDate.Location = New System.Drawing.Point(47, 51)
        Me.ComboNepaliVStartDate.Name = "ComboNepaliVStartDate"
        Me.ComboNepaliVStartDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVStartDate.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliVStartDate.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVStartDate.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliVStartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliVStartDate.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliVStartDate.Size = New System.Drawing.Size(42, 20)
        Me.ComboNepaliVStartDate.TabIndex = 39
        '
        'DateEdit5
        '
        Me.DateEdit5.EditValue = New Date(2029, 1, 1, 21, 31, 31, 0)
        Me.DateEdit5.Location = New System.Drawing.Point(82, 75)
        Me.DateEdit5.Name = "DateEdit5"
        Me.DateEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit5.Properties.Appearance.Options.UseFont = True
        Me.DateEdit5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit5.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit5.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.DateEdit5.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEdit5.Size = New System.Drawing.Size(135, 20)
        Me.DateEdit5.TabIndex = 38
        '
        'DateEdit4
        '
        Me.DateEdit4.EditValue = New Date(2018, 1, 1, 15, 58, 36, 0)
        Me.DateEdit4.Location = New System.Drawing.Point(82, 51)
        Me.DateEdit4.Name = "DateEdit4"
        Me.DateEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit4.Properties.Appearance.Options.UseFont = True
        Me.DateEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit4.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit4.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.DateEdit4.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEdit4.Size = New System.Drawing.Size(135, 20)
        Me.DateEdit4.TabIndex = 37
        '
        'LabelControl36
        '
        Me.LabelControl36.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl36.Appearance.Options.UseFont = True
        Me.LabelControl36.Location = New System.Drawing.Point(5, 78)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(21, 14)
        Me.LabelControl36.TabIndex = 14
        Me.LabelControl36.Text = "End"
        '
        'LabelControl35
        '
        Me.LabelControl35.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl35.Appearance.Options.UseFont = True
        Me.LabelControl35.Location = New System.Drawing.Point(3, 54)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(27, 14)
        Me.LabelControl35.TabIndex = 13
        Me.LabelControl35.Text = "Start"
        '
        'LabelControl42
        '
        Me.LabelControl42.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl42.Appearance.Options.UseFont = True
        Me.LabelControl42.Location = New System.Drawing.Point(331, 262)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl42.TabIndex = 62
        Me.LabelControl42.Text = "Card(ZK)"
        '
        'TextEditCard
        '
        Me.TextEditCard.EditValue = ""
        Me.TextEditCard.Location = New System.Drawing.Point(393, 261)
        Me.TextEditCard.Name = "TextEditCard"
        Me.TextEditCard.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditCard.Properties.Appearance.Options.UseFont = True
        Me.TextEditCard.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditCard.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditCard.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditCard.Properties.MaxLength = 10
        Me.TextEditCard.Size = New System.Drawing.Size(122, 20)
        Me.TextEditCard.TabIndex = 61
        '
        'GroupControlFPscanner
        '
        Me.GroupControlFPscanner.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControlFPscanner.AppearanceCaption.Options.UseFont = True
        Me.GroupControlFPscanner.Controls.Add(Me.picFPImg)
        Me.GroupControlFPscanner.Controls.Add(Me.LabelControl39)
        Me.GroupControlFPscanner.Controls.Add(Me.SimpleButtonInit)
        Me.GroupControlFPscanner.Controls.Add(Me.SimpleButtonStop)
        Me.GroupControlFPscanner.Controls.Add(Me.TextFingerNo)
        Me.GroupControlFPscanner.Location = New System.Drawing.Point(15, 117)
        Me.GroupControlFPscanner.Name = "GroupControlFPscanner"
        Me.GroupControlFPscanner.Size = New System.Drawing.Size(305, 294)
        Me.GroupControlFPscanner.TabIndex = 37
        Me.GroupControlFPscanner.Text = "FingerPrint Reader(ZK)"
        '
        'picFPImg
        '
        Me.picFPImg.Cursor = System.Windows.Forms.Cursors.Default
        Me.picFPImg.Location = New System.Drawing.Point(5, 60)
        Me.picFPImg.Name = "picFPImg"
        Me.picFPImg.Properties.NullText = " "
        Me.picFPImg.Properties.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.ByteArray
        Me.picFPImg.Properties.ZoomPercent = 60.0R
        Me.picFPImg.Size = New System.Drawing.Size(190, 228)
        Me.picFPImg.TabIndex = 55
        '
        'LabelControl39
        '
        Me.LabelControl39.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl39.Appearance.Options.UseFont = True
        Me.LabelControl39.Location = New System.Drawing.Point(5, 35)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(80, 14)
        Me.LabelControl39.TabIndex = 54
        Me.LabelControl39.Text = "Finger Number"
        '
        'SimpleButtonInit
        '
        Me.SimpleButtonInit.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonInit.Appearance.Options.UseFont = True
        Me.SimpleButtonInit.Location = New System.Drawing.Point(200, 31)
        Me.SimpleButtonInit.Name = "SimpleButtonInit"
        Me.SimpleButtonInit.Size = New System.Drawing.Size(96, 23)
        Me.SimpleButtonInit.TabIndex = 50
        Me.SimpleButtonInit.Text = "Init Reader"
        '
        'SimpleButtonStop
        '
        Me.SimpleButtonStop.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonStop.Appearance.Options.UseFont = True
        Me.SimpleButtonStop.Location = New System.Drawing.Point(200, 60)
        Me.SimpleButtonStop.Name = "SimpleButtonStop"
        Me.SimpleButtonStop.Size = New System.Drawing.Size(96, 23)
        Me.SimpleButtonStop.TabIndex = 51
        Me.SimpleButtonStop.Text = "Stop Reader"
        '
        'TextFingerNo
        '
        Me.TextFingerNo.EditValue = "6"
        Me.TextFingerNo.Location = New System.Drawing.Point(100, 34)
        Me.TextFingerNo.Name = "TextFingerNo"
        Me.TextFingerNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextFingerNo.Properties.Appearance.Options.UseFont = True
        Me.TextFingerNo.Properties.Mask.EditMask = "[0-9]"
        Me.TextFingerNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextFingerNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextFingerNo.Properties.MaxLength = 1
        Me.TextFingerNo.Size = New System.Drawing.Size(47, 20)
        Me.TextFingerNo.TabIndex = 53
        '
        'CheckCopyEmpty
        '
        Me.CheckCopyEmpty.EditValue = True
        Me.CheckCopyEmpty.Location = New System.Drawing.Point(15, 38)
        Me.CheckCopyEmpty.Name = "CheckCopyEmpty"
        Me.CheckCopyEmpty.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckCopyEmpty.Properties.Appearance.Options.UseFont = True
        Me.CheckCopyEmpty.Properties.Caption = "Copy Empty"
        Me.CheckCopyEmpty.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckCopyEmpty.Properties.RadioGroupIndex = 0
        Me.CheckCopyEmpty.Size = New System.Drawing.Size(90, 19)
        Me.CheckCopyEmpty.TabIndex = 59
        '
        'CheckEditTemplate
        '
        Me.CheckEditTemplate.Location = New System.Drawing.Point(130, 38)
        Me.CheckEditTemplate.Name = "CheckEditTemplate"
        Me.CheckEditTemplate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditTemplate.Properties.Appearance.Options.UseFont = True
        Me.CheckEditTemplate.Properties.Caption = "Copy Template(ZK)"
        Me.CheckEditTemplate.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditTemplate.Properties.RadioGroupIndex = 0
        Me.CheckEditTemplate.Size = New System.Drawing.Size(143, 19)
        Me.CheckEditTemplate.TabIndex = 60
        Me.CheckEditTemplate.TabStop = False
        '
        'PopupContainerEdit1
        '
        Me.PopupContainerEdit1.Location = New System.Drawing.Point(15, 91)
        Me.PopupContainerEdit1.Name = "PopupContainerEdit1"
        Me.PopupContainerEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEdit1.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEdit1.Properties.PopupControl = Me.PopupContainerControl1
        Me.PopupContainerEdit1.Size = New System.Drawing.Size(305, 20)
        Me.PopupContainerEdit1.TabIndex = 57
        '
        'LabelControl38
        '
        Me.LabelControl38.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl38.Appearance.Options.UseFont = True
        Me.LabelControl38.Location = New System.Drawing.Point(15, 73)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(90, 14)
        Me.LabelControl38.TabIndex = 58
        Me.LabelControl38.Text = "Copy To  Device"
        '
        'SimpleButtonSave1
        '
        Me.SimpleButtonSave1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave1.Appearance.Options.UseFont = True
        Me.SimpleButtonSave1.Location = New System.Drawing.Point(39, 393)
        Me.SimpleButtonSave1.Name = "SimpleButtonSave1"
        Me.SimpleButtonSave1.Size = New System.Drawing.Size(98, 23)
        Me.SimpleButtonSave1.TabIndex = 48
        Me.SimpleButtonSave1.Text = "Transfer"
        Me.SimpleButtonSave1.Visible = False
        '
        'GroupControl5
        '
        Me.GroupControl5.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl5.AppearanceCaption.Options.UseFont = True
        Me.GroupControl5.Controls.Add(Me.GridControlZKDevice)
        Me.GroupControl5.Location = New System.Drawing.Point(151, 361)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(149, 120)
        Me.GroupControl5.TabIndex = 47
        Me.GroupControl5.Text = "Device List"
        Me.GroupControl5.Visible = False
        '
        'GridControlZKDevice
        '
        Me.GridControlZKDevice.DataSource = Me.TblMachineBindingSource
        Me.GridControlZKDevice.Dock = System.Windows.Forms.DockStyle.Fill
        GridLevelNode1.RelationName = "Level1"
        Me.GridControlZKDevice.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControlZKDevice.Location = New System.Drawing.Point(2, 23)
        Me.GridControlZKDevice.MainView = Me.GridViewZKDevice
        Me.GridControlZKDevice.Name = "GridControlZKDevice"
        Me.GridControlZKDevice.Size = New System.Drawing.Size(145, 95)
        Me.GridControlZKDevice.TabIndex = 1
        Me.GridControlZKDevice.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewZKDevice})
        '
        'GridViewZKDevice
        '
        Me.GridViewZKDevice.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8})
        Me.GridViewZKDevice.GridControl = Me.GridControlZKDevice
        Me.GridViewZKDevice.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridViewZKDevice.Name = "GridViewZKDevice"
        Me.GridViewZKDevice.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewZKDevice.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewZKDevice.OptionsBehavior.Editable = False
        Me.GridViewZKDevice.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewZKDevice.OptionsSelection.MultiSelect = True
        Me.GridViewZKDevice.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewZKDevice.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn4, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Controller Id"
        Me.GridColumn4.FieldName = "ID_NO"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 1
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Device IP"
        Me.GridColumn5.FieldName = "LOCATION"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 2
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Location"
        Me.GridColumn6.FieldName = "branch"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 3
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Device Type"
        Me.GridColumn7.FieldName = "DeviceType"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 4
        '
        'GridColumn8
        '
        Me.GridColumn8.FieldName = "A_R"
        Me.GridColumn8.Name = "GridColumn8"
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.LabelControl41)
        Me.GroupControl4.Controls.Add(Me.VisitorPictureEdit)
        Me.GroupControl4.Controls.Add(Me.TextEditImage)
        Me.GroupControl4.Controls.Add(Me.LabelControl40)
        Me.GroupControl4.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(272, 352)
        Me.GroupControl4.TabIndex = 36
        Me.GroupControl4.Text = "Picture"
        '
        'LabelControl41
        '
        Me.LabelControl41.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl41.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl41.Appearance.Options.UseFont = True
        Me.LabelControl41.Appearance.Options.UseForeColor = True
        Me.LabelControl41.Location = New System.Drawing.Point(135, 247)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(23, 19)
        Me.LabelControl41.TabIndex = 57
        Me.LabelControl41.Text = "OR"
        '
        'VisitorPictureEdit
        '
        Me.VisitorPictureEdit.Cursor = System.Windows.Forms.Cursors.Default
        Me.VisitorPictureEdit.EditValue = "<Null>"
        Me.VisitorPictureEdit.Location = New System.Drawing.Point(5, 38)
        Me.VisitorPictureEdit.Name = "VisitorPictureEdit"
        Me.VisitorPictureEdit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.VisitorPictureEdit.Properties.Appearance.Options.UseFont = True
        Me.VisitorPictureEdit.Properties.LookAndFeel.SkinName = "iMaginary"
        Me.VisitorPictureEdit.Properties.LookAndFeel.UseDefaultLookAndFeel = False
        Me.VisitorPictureEdit.Properties.NullText = "Double click to open Camera"
        Me.VisitorPictureEdit.Properties.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.ByteArray
        Me.VisitorPictureEdit.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Always
        Me.VisitorPictureEdit.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.VisitorPictureEdit.Size = New System.Drawing.Size(260, 200)
        Me.VisitorPictureEdit.TabIndex = 36
        '
        'TextEditImage
        '
        Me.TextEditImage.Location = New System.Drawing.Point(7, 303)
        Me.TextEditImage.Name = "TextEditImage"
        Me.TextEditImage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditImage.Properties.Appearance.Options.UseFont = True
        Me.TextEditImage.Size = New System.Drawing.Size(260, 20)
        Me.TextEditImage.TabIndex = 56
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl40.Appearance.Options.UseFont = True
        Me.LabelControl40.Location = New System.Drawing.Point(7, 280)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(78, 14)
        Me.LabelControl40.TabIndex = 55
        Me.LabelControl40.Text = "Browse Image"
        '
        'XtraEmployeeEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(883, 511)
        Me.Controls.Add(Me.TabPane1)
        Me.Controls.Add(Me.SidePanel1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraEmployeeEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.DateEditLeaving.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditLeaving.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmployeeGroupBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.GridLookUpEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDESPANSARYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPF1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditESI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNepaliYearLeave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNEpaliMonthLeave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNepaliDateLeave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.ComboBoxEditNepaliYearDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNEpaliMonthDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNepaliDateDOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNepaliYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.TextEditUid.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNepaliDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.TextEdit17.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit16.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit15.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblBankBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditBirth.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditBirth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditJoin.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditJoin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControl1.ResumeLayout(False)
        CType(Me.GridControlMachine, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewMachine, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabPane1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPane1.ResumeLayout(False)
        Me.TabNavigationPage1.ResumeLayout(False)
        Me.TabNavigationPage2.ResumeLayout(False)
        CType(Me.textRes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.AxFP_CLOCK1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEnrollFace.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEnroll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.CheckValidity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliVEndYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliVEndMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliVEndDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliVStartYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliVStartMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliVStartDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit5.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit4.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditCard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControlFPscanner, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControlFPscanner.ResumeLayout(False)
        Me.GroupControlFPscanner.PerformLayout()
        CType(Me.picFPImg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextFingerNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckCopyEmpty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditTemplate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.GridControlZKDevice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewZKDevice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.VisitorPictureEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditImage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitch1 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEditLeaving As DevExpress.XtraEditors.DateEdit
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridLookUpEdit5 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridLookUpEdit4 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridLookUpEdit3 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridLookUpEdit2 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridLookUpEdit1 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextEditESI As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridLookUpEdit6 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TextEditPF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditPF1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit15 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit14 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit13 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit12 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridLookUpEdit7 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView6 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ComboBoxEdit2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents TextEdit11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ToggleSwitch2 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents DateEditBirth As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateEditJoin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TextEditUid As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit19 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit2 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TextEdit17 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit16 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblCompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblCompanyTableAdapter As iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter
    Friend WithEvents colCOMPANYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblCompany1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter
    Friend WithEvents TblDepartmentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblDepartmentTableAdapter As iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter
    Friend WithEvents TblDepartment1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblCatagoryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblCatagoryTableAdapter As iAS.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter
    Friend WithEvents colCAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCATAGORYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblCatagory1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter
    Friend WithEvents EmployeeGroupBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EmployeeGroupTableAdapter As iAS.SSSDBDataSetTableAdapters.EmployeeGroupTableAdapter
    Friend WithEvents colGroupId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGroupName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents EmployeeGroup1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.EmployeeGroup1TableAdapter
    Friend WithEvents TblGradeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblGradeTableAdapter As iAS.SSSDBDataSetTableAdapters.tblGradeTableAdapter
    Friend WithEvents colGradeCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGradeName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblGrade1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblGrade1TableAdapter
    Friend WithEvents TblDESPANSARYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblDESPANSARYTableAdapter As iAS.SSSDBDataSetTableAdapters.tblDESPANSARYTableAdapter
    Friend WithEvents colDESPANSARYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDESPANSARYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblBankBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblBankTableAdapter As iAS.SSSDBDataSetTableAdapters.tblBankTableAdapter
    Friend WithEvents colBANKCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBANKNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblBank1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblBank1TableAdapter
    Friend WithEvents TblDESPANSARY1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblDESPANSARY1TableAdapter
    Friend WithEvents GridLookUpEdit8 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TblbranchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblbranchTableAdapter As iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter
    Friend WithEvents Tblbranch1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter
    Friend WithEvents ComboBoxEditNepaliDate As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNEpaliMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNepaliYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNepaliYearDOB As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNEpaliMonthDOB As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNepaliDateDOB As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNepaliYearLeave As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNEpaliMonthLeave As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNepaliDateLeave As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblMachineTableAdapter As iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents TblMachine1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents TabPane1 As DevExpress.XtraBars.Navigation.TabPane
    Friend WithEvents TabNavigationPage1 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents TabNavigationPage2 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents VisitorPictureEdit As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextFingerNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButtonStop As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonInit As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonSave1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControlZKDevice As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewZKDevice As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditImage As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEditTemplate As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckCopyEmpty As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEdit1 As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PopupContainerControl1 As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlMachine As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewMachine As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLOCATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeviceType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colA_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents textRes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControlFPscanner As DevExpress.XtraEditors.GroupControl
    Friend WithEvents picFPImg As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditCard As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ComboNepaliVEndYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliVEndMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliVEndDate As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliVStartYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliVStartMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliVStartDate As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents DateEdit5 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateEdit4 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckValidity As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEnroll As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEnrollFace As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents AxFP_CLOCK1 As AxFP_CLOCKLib.AxFP_CLOCK
End Class
