﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraSplashScreen
Imports System.Runtime.CompilerServices
Imports Microsoft.Office.Interop

'Imports Microsoft.Office.Interop.Excel
'Imports TimeWatchNewDeskTop.HorizontalMerging
Imports DevExpress.Export
Imports DevExpress.XtraPrinting
Imports DevExpress.Printing.ExportHelpers
Imports DevExpress.Export.Xl
Imports PdfSharp.Pdf
Imports PdfSharp.Drawing
Imports System.Drawing.Printing
Imports iAS.HorizontalMerging


Public Class XtraReportGrid
    Dim mstrFile_Name As String
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim g_WhereClause As String
    'Dim g_SkipAfterDept As Boolean
    Dim mintPageNo As Integer
    Dim mintLine As Integer
    Public Shared fromdatetodatetext As String

    Dim ViewHandler As MyGridViewHandler
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
        ViewHandler = New MyGridViewHandler(GridView1)
    End Sub
    Private Sub XtraReportGrid_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Me.Width = My.Computer.Screen.WorkingArea.Width - 200
        'Me.Height = My.Computer.Screen.WorkingArea.Height - 100
        ComboPageSize.SelectedIndex = 0
        ComboOrientation.SelectedIndex = 0

        fromdatetodatetext = Common.frodatetodatetoReportGrid
        GridView1.Columns.Clear()
        GridControl1.DataSource = Common.tbl
        For i As Integer = 0 To Common.tbl.Columns.Count - 1
            GridView1.Columns(i).Width = 100
        Next
        If XtraReportsMonthly.HAL = True Then
            For i As Integer = 0 To Common.tbl.Columns.Count - 1
                GridView1.Columns(i).Width = 70
            Next
        End If
        If Common.frodatetodatetoReportGrid.Contains("APPLICATION LOG REPORT FROM") Then
            For i As Integer = 1 To Common.tbl.Columns.Count - 1
                GridView1.Columns(i).Width = 200
            Next
        End If

        If Common.frodatetodatetoReportGrid.Contains("DAILY VISITOR TEMPERATURE REPORT FROM DATE") Then
            GridView1.RowHeight = 120
            GridView1.Columns(6).Width = 120
            GridView1.Columns(7).Width = 120
        Else
            GridView1.RowHeight = 20
        End If

        If Common.frodatetodatetoReportGrid.Contains("DAILY TEMPERATURE REPORT") Then
            GridView1.RowHeight = 120
            GridView1.Columns(11).Width = 120
            GridView1.Columns(12).Width = 120
        Else
            GridView1.RowHeight = 20
        End If

        'GridControl1.ForceInitialize()
        'If Common.frodatetodatetoReportGrid.ToString.Trim <> "" Then
        If Common.frodatetodatetoReportGrid.Contains(" Performance Register  from  ") And XtraReportsMonthly.HAL = False Then  'only for performance register
            'Dim MergedColumnList As New List(Of GridColumn)()
            'Dim MergedColumn() As GridColumn
            'If GridView1.Columns.Count < 10 Then
            '    For x As Integer = 0 To GridView1.Columns.Count - 1
            '        MergedColumnList.Add(GridView1.Columns(x))
            '    Next
            '    MergedColumn = MergedColumnList.ToArray
            'Else
            '    MergedColumn = New GridColumn() {GridView1.Columns(0), GridView1.Columns(1), GridView1.Columns(2), GridView1.Columns(3), GridView1.Columns(4), GridView1.Columns(5), GridView1.Columns(6), GridView1.Columns(7), GridView1.Columns(8), GridView1.Columns(9)}
            'End If

            'Dim tmp As Integer = 0
            'For i = 0 To Common.tbl.Rows.Count - 1
            '    If i = 1 Then
            '        tmp = 1
            '        ViewHandler.MergeCells(GridView1.GetRowCellValue(i, " ").ToString, GridView1.GetDataSourceRowIndex(1), MergedColumn)
            '    ElseIf i = tmp + 10 Then
            '        tmp = i
            '        'ViewHandler.MergeCells(GridView1.GetRowCellValue(tmp, " ").ToString, GridView1.GetDataSourceRowIndex(tmp), New GridColumn() {GridView1.Columns(0), GridView1.Columns(1), GridView1.Columns(2), GridView1.Columns(3), GridView1.Columns(4), GridView1.Columns(5), GridView1.Columns(6), GridView1.Columns(7), GridView1.Columns(8), GridView1.Columns(9)})
            '        ViewHandler.MergeCells(GridView1.GetRowCellValue(tmp, " ").ToString, GridView1.GetDataSourceRowIndex(tmp), MergedColumn)
            '    End If
            'Next

            Dim MergedColumnList As New List(Of GridColumn)()
            Dim MergedColumn() As GridColumn
            If GridView1.Columns.Count < 9 Then
                For x As Integer = 0 To GridView1.Columns.Count - 1
                    MergedColumnList.Add(GridView1.Columns(x))
                Next
                MergedColumn = MergedColumnList.ToArray
            Else
                MergedColumn = New GridColumn() {GridView1.Columns(0), GridView1.Columns(1), GridView1.Columns(2), GridView1.Columns(3), GridView1.Columns(4), GridView1.Columns(5), GridView1.Columns(6), GridView1.Columns(7), GridView1.Columns(8), GridView1.Columns(9)}
            End If

            Dim tmp As Integer = 0
            For i = 0 To Common.tbl.Rows.Count - 1
                If i = 1 Then
                    tmp = 1
                    ViewHandler.MergeCells(GridView1.GetRowCellValue(i, " ").ToString, GridView1.GetDataSourceRowIndex(1), MergedColumn)
                ElseIf i = tmp + 9 Then
                    tmp = i
                    'ViewHandler.MergeCells(GridView1.GetRowCellValue(tmp, " ").ToString, GridView1.GetDataSourceRowIndex(tmp), New GridColumn() {GridView1.Columns(0), GridView1.Columns(1), GridView1.Columns(2), GridView1.Columns(3), GridView1.Columns(4), GridView1.Columns(5), GridView1.Columns(6), GridView1.Columns(7), GridView1.Columns(8), GridView1.Columns(9)})
                    ViewHandler.MergeCells(GridView1.GetRowCellValue(tmp, " ").ToString, GridView1.GetDataSourceRowIndex(tmp), MergedColumn)
                End If
            Next

        End If


        If Common.frodatetodatetoReportGrid.Contains(" Performance Register  from   ") And XtraReportsMonthly.HAL = True Then  'only for performance register
            Dim MergedColumnList As New List(Of GridColumn)()
            Dim MergedColumn() As GridColumn
            If GridView1.Columns.Count < 6 Then
                For x As Integer = 0 To GridView1.Columns.Count - 1
                    MergedColumnList.Add(GridView1.Columns(x))
                Next
                MergedColumn = MergedColumnList.ToArray
            Else
                MergedColumn = New GridColumn() {GridView1.Columns(0), GridView1.Columns(1), GridView1.Columns(2), GridView1.Columns(3), GridView1.Columns(4), GridView1.Columns(5), GridView1.Columns(6), GridView1.Columns(7), GridView1.Columns(8), GridView1.Columns(9)}
            End If

            Dim tmp As Integer = 0
            For i = 0 To Common.tbl.Rows.Count - 1
                If i = 1 Then
                    tmp = 1
                    ViewHandler.MergeCells(GridView1.GetRowCellValue(i, " ").ToString, GridView1.GetDataSourceRowIndex(1), MergedColumn)
                ElseIf i = tmp + 6 Then
                    tmp = i
                    'ViewHandler.MergeCells(GridView1.GetRowCellValue(tmp, " ").ToString, GridView1.GetDataSourceRowIndex(tmp), New GridColumn() {GridView1.Columns(0), GridView1.Columns(1), GridView1.Columns(2), GridView1.Columns(3), GridView1.Columns(4), GridView1.Columns(5), GridView1.Columns(6), GridView1.Columns(7), GridView1.Columns(8), GridView1.Columns(9)})
                    ViewHandler.MergeCells(GridView1.GetRowCellValue(tmp, " ").ToString, GridView1.GetDataSourceRowIndex(tmp), MergedColumn)
                End If
            Next
        End If


        'End If
        If XtraReportsDaily.InOutPdf = True Then
            GridView1.Columns(6).Width = 250
        End If
        If Common.ReportMail = True Then
            If XtraReportsDaily.InOutPdf = True Then
                Dim PK As PaperKind
                PK = PaperKind.A5Extra
                'PK = PaperKind.MonarchEnvelope
                Dim Orientation As Boolean = False
                Dim link As New PrintableComponentLink() With { _
                   .PrintingSystemBase = New PrintingSystem(), _
                   .Component = GridControl1, _
                   .Landscape = Orientation, _
                   .PaperKind = PK, _
                   .Margins = New Margins(10, 10, 10, 10) _
                }
                DevExpress.Export.ExportSettings.DefaultExportType = DevExpress.Export.ExportType.DataAware
                AddHandler link.CreateReportHeaderArea, AddressOf link_CreateReportHeaderArea
                mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\" & Common.reportName
                link.ExportToXlsx(mstrFile_Name)
                Me.Close()
            Else
                Me.Hide()
                DevExpress.Export.ExportSettings.DefaultExportType = DevExpress.Export.ExportType.DataAware
                mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\" & Common.reportName
                ExportSettings.DefaultExportType = ExportType.DataAware
                Dim options = New XlsxExportOptionsEx()
                options.SheetName = "Sheet1"
                AddHandler options.CustomizeSheetHeader, AddressOf options_CustomizeSheetHeader
                GridControl1.ExportToXlsx(mstrFile_Name, options)
                'GridControl1.ExportToPdf("abc.pdf")
                Me.Close()
                'Process.Start(mstrFile_Name)
            End If
        End If


        If Common.IOCL = True Then
            LabelControl1.Visible = False
            ComboPageSize.Visible = False
            LabelControl2.Visible = False
            ComboOrientation.Visible = False
            SimpleButton1.Visible = False
        Else
            LabelControl1.Visible = True
            ComboPageSize.Visible = True
            LabelControl2.Visible = True
            ComboOrientation.Visible = True
            SimpleButton1.Visible = True
        End If
    End Sub
    Private Sub PreviewPrintableComponent(component As IPrintable, lookAndFeel As UserLookAndFeel)
        Dim PK As PaperKind
        If ComboPageSize.SelectedItem.ToString = "A4" Then
            PK = PaperKind.A4
        ElseIf ComboPageSize.SelectedItem.ToString = "A2" Then
            PK = PaperKind.A2
        ElseIf ComboPageSize.SelectedItem.ToString = "A3" Then
            PK = PaperKind.A3
        ElseIf ComboPageSize.SelectedItem.ToString = "Legal" Then
            PK = PaperKind.Legal
        ElseIf ComboPageSize.SelectedItem.ToString = "Letter" Then
            PK = PaperKind.Letter
        ElseIf ComboPageSize.SelectedItem.ToString = "Note" Then
            PK = PaperKind.Note
        Else
            PK = PaperKind.A4
        End If
        Dim Orientation As Boolean
        If ComboOrientation.SelectedItem.ToString = "Landscape" Then
            Orientation = True
        Else
            Orientation = False
        End If
        ' Create a link that will print a control.  
        Dim link As New PrintableComponentLink() With { _
            .PrintingSystemBase = New PrintingSystem(), _
            .Component = component, _
            .Landscape = Orientation, _
            .PaperKind = PK, _
            .Margins = New Margins(10, 10, 10, 10) _
        }
        ' Show the report. 
        'link.ShowRibbonPreview(lookAndFeel)
        'GridControl1.ForceInitialize()
        If Common.frodatetodatetoReportGrid.Contains(" Performance Register  from  ") Then 'only for performance register
            DevExpress.Export.ExportSettings.DefaultExportType = DevExpress.Export.ExportType.WYSIWYG
        Else
            DevExpress.Export.ExportSettings.DefaultExportType = DevExpress.Export.ExportType.DataAware
        End If

        'mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"

        AddHandler link.CreateReportHeaderArea, AddressOf link_CreateReportHeaderArea
        'link.ShowRibbonPreview(lookAndFeel)

        'link.ExportToXlsx(mstrFile_Name)
        'Process.Start(mstrFile_Name)
        'link.ExportToPdf("test.pdf")
        'Process.Start("test.pdf")

        If ComboType.EditValue = "Xlsx" Then
            mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
            link.ExportToXlsx(mstrFile_Name)
            Process.Start(mstrFile_Name)
        ElseIf ComboType.EditValue = "Xls" Then
            mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"
            link.ExportToXls(mstrFile_Name)
            Process.Start(mstrFile_Name)
        ElseIf ComboType.EditValue = "Pdf" Then
            mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".pdf"
            link.ExportToPdf(mstrFile_Name)
            Process.Start(mstrFile_Name)
        ElseIf ComboType.EditValue = "Csv" Then
            mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".csv"
            link.ExportToCsv(mstrFile_Name)
            Process.Start(mstrFile_Name)
        End If

    End Sub
    Private Sub link_CreateReportHeaderArea(ByVal sender As System.Object, ByVal e As CreateAreaEventArgs)
        Dim reportHeader As String = "Company Name:" & CommonReport.g_CompanyNames & vbCrLf & "Run Date & Time : " & _
            Common.runningDateTime & vbCrLf & fromdatetodatetext

        e.Graph.StringFormat = New BrickStringFormat(StringAlignment.Center)
        e.Graph.Font = New Font("Calibri", 12, FontStyle.Bold)
        Dim rec As RectangleF = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 70)
        e.Graph.DrawString(reportHeader, Color.Black, rec, BorderSide.None)
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        PreviewPrintableComponent(GridControl1, GridControl1.LookAndFeel)
       
        'Dim mstrFile_Name As String = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".pdf"
        'GridView1.ExportToPdf(mstrFile_Name)
        'Process.Start(mstrFile_Name)
    End Sub


    Private Sub SimpleButton5_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton5.Click
       
        'DevExpress.Export.ExportSettings.DefaultExportType = ExportType.WYSIWYG   'to get as it is from grid
        DevExpress.Export.ExportSettings.DefaultExportType = DevExpress.Export.ExportType.WYSIWYG
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"


        If Common.IOCL = True Then
            GridControl1.ExportToXlsx(mstrFile_Name)
            Common.IOCL = False
            Process.Start(mstrFile_Name)
            Exit Sub
        End If
        'If Common.isPDF = False Then
        'GridView1.ExportToXlsx(mstrFile_Name)

        ''for row color
        'Dim op As New XlsExportOptionsEx()
        'AddHandler op.CustomizeCell, AddressOf op_CustomizeCell
        'GridView1.ExportToXls(mstrFile_Name, op)
        'System.Diagnostics.Process.Start(mstrFile_Name)
        ''end for row color



        'for header text 'nitin
        ' Ensure that the data-aware export mode is enabled.
        ExportSettings.DefaultExportType = ExportType.DataAware
        ' Create a new object defining how a document is exported to the XLSX format.
        Dim options = New XlsxExportOptionsEx()
        ' Specify a name of the sheet in the created XLSX file.
        options.SheetName = "Sheet1"

        ' Subscribe to export customization events. 
        ''AddHandler options.CustomizeSheetSettings, AddressOf options_CustomizeSheetSettings
        AddHandler options.CustomizeSheetHeader, AddressOf options_CustomizeSheetHeader
        'AddHandler options.CustomizeCell, AddressOf options_CustomizeCell
        'AddHandler options.CustomizeSheetFooter, AddressOf options_CustomizeSheetFooter
        'AddHandler options.AfterAddRow, AddressOf options_AfterAddRow


        ' Export the grid data to the XLSX format.
        GridControl1.ExportToXlsx(mstrFile_Name, options)

        'Dim xlApp As Excel.ApplicationClass = CreateObject("Excel.Application")
        'Dim xlwb As Excel.Workbook = xlApp.Workbooks.Open(mstrFile_Name)
        'Dim xlst As Excel.Sheets = xlwb.Worksheets("Sheet1")
        'xlst("Sheet1").PageSetup.PaperSize = System.Drawing.Printing.PaperKind.A4
        'xlwb.SaveAs(mstrFile_Name)

        ' Open the created document.
        Process.Start(mstrFile_Name)
        'End for header text 'nitin

        ''for A4 size 
        'Using ps As New PrintingSystem
        '    Using link As New PrintableComponentLink(ps)
        '        link.Component = Me.GridControl1
        '        link.PaperKind = System.Drawing.Printing.PaperKind.A4
        '        link.Landscape = True

        '        DevExpress.Export.ExportSettings.DefaultExportType = DevExpress.Export.ExportType.DataAware

        '        Dim dialog As New SaveFileDialog()

        '        If (dialog.ShowDialog() = DialogResult.OK) Then
        '            link.ExportToXls(dialog.FileName,
        '                             New XlsExportOptionsEx(TextExportMode.Value) With
        '                             {
        '                                 .Suppress65536RowsWarning = True
        '                             })

        '            MessageBox.Show("Done!")
        '        End If
        '    End Using
        'End Using
        ''end for A4 size 



        'Dim sr As New StreamReader(mstrFile_Name)
        'Dim text As String = sr.ReadToEnd()
        'sr.Close()
        'Dim sw As New StreamWriter(mstrFile_Name)
        'Dim rundatetime As String = "         Company Name:" & CommonReport.g_CompanyNames
        'Dim caption2 As String = "           Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")

        'sw.WriteLine(rundatetime)
        'sw.WriteLine(caption2)
        'sw.WriteLine("Monthly Performance from " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy"))
        'sw.WriteLine("")
        'sw.WriteLine(text) ' Add original content back
        'sw.Close()
        'Process.Start(mstrFile_Name)





        'Else
        'mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
        'ExportSettings.DefaultExportType = ExportType.DataAware
        'Dim options = New XlsxExportOptionsEx()
        'options.SheetName = "Sheet1"
        'AddHandler options.CustomizeSheetHeader, AddressOf options_CustomizeSheetHeader
        'GridControl1.ExportToXlsx(mstrFile_Name, options)
        'Dim mstrFile_NamePdf As String = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".pdf"

        ''Dim xl As Object
        ''xl = CreateObject("Excel.Application")
        ''Dim xwb As Object = xl.Workbooks.Open(mstrFile_Name)
        ''xwb.ActiveSheet.
        ''xwb.ActiveSheet.ExportAsFixedFormat(0, mstrFile_NamePdf)
        ''xl.Quit()

        ''pdf start 
        'Try
        '    Dim line As String
        '    'Dim readFile As System.IO.TextReader = New StreamReader("Text.txt")
        '    Dim readFile As System.IO.TextReader = New StreamReader(mstrFile_Name)
        '    Dim yPoint As Integer = 0
        '    Dim pdf As PdfDocument = New PdfDocument
        '    pdf.Info.Title = "Text File to PDF"
        '    Dim pdfPage As PdfPage = pdf.AddPage
        '    Dim graph As XGraphics = XGraphics.FromPdfPage(pdfPage)
        '    Dim font As XFont = New XFont("Verdana", 8, XFontStyle.Regular)
        '    While True
        '        line = readFile.ReadLine()
        '        If line Is Nothing Then
        '            Exit While
        '        Else
        '            graph.DrawString(line, font, XBrushes.Black, New XRect(20, yPoint, pdfPage.Width.Point, pdfPage.Height.Point), XStringFormat.TopLeft)
        '            yPoint = yPoint + 10
        '            '--------------------------- here is the part added ----------------------------------
        '            If yPoint = 840 Then
        '                pdfPage = pdf.AddPage
        '                graph = XGraphics.FromPdfPage(pdfPage)
        '                yPoint = 0
        '            End If
        '            '-------------------------------------------------------------------------------------
        '        End If
        '    End While
        '    Dim pdfFilename As String = mstrFile_Name & ".pdf" '"txttopdf.pdf"
        '    pdf.Save(pdfFilename)
        '    readFile.Close()
        '    readFile = Nothing
        '    System.Diagnostics.Process.Start(pdfFilename)
        'Catch ex As Exception
        '    MsgBox(ex.ToString)
        'End Try
        ''pdf end 

        '' Open the created document.
        'Process.Start(mstrFile_NamePdf)
        'End If

    End Sub
  
#Region "#CustomizeSheetSettingsEvent"
    Private Sub options_CustomizeSheetSettings(ByVal e As CustomizeSheetEventArgs)
        ' Anchor the output document's header to the top and set its fixed height. 
        Const lastHeaderRowIndex As Integer = 15
        e.ExportContext.SetFixedHeader(lastHeaderRowIndex)
        ' Add the AutoFilter button to the document's cells corresponding to the grid column headers.
        e.ExportContext.AddAutoFilter(New XlCellRange(New XlCellPosition(0, lastHeaderRowIndex), New XlCellPosition(5, 100)))
    End Sub
#End Region ' #CustomizeSheetSettingsEvent


    'Private Sub GridView1_RowStyle(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles GridView1.RowStyle
    '    Dim View As GridView = CType(sender, GridView)
    '    If (e.RowHandle >= 0) Then
    '        Dim priority As String = View.GetRowCellDisplayText(e.RowHandle, View.Columns("Date"))
    '        If (priority = "Present") Then
    '            e.Appearance.BackColor = Color.Green
    '            e.Appearance.BackColor2 = Color.Green
    '            e.Appearance.ForeColor = Color.White
    '        End If
    '    End If
    'End Sub
    'currenty not using
    'Private Sub op_CustomizeCell(ea As DevExpress.Export.CustomizeCellEventArgs)
    '    Dim View As GridView = CType(GridView1, GridView)
    '   Dim priority As String = View.GetRowCellDisplayText(ea.RowHandle, View.Columns("Date"))
    '    If (priority = "Present") Then
    '        ea.Formatting.BackColor = Color.Green
    '        ea.Formatting.Font.Color = Color.White
    '        ea.Handled = True
    '    End If
    '    'If ea.ColumnFieldName = "Date" Then
    '    '    ea.Formatting.BackColor = Color.Pink
    '    '    ea.Handled = True
    '    'End If
    'End Sub
#Region "#CustomizeSheetHeaderEvent"
    Private Delegate Sub AddCells(ByVal e As ContextEventArgs, ByVal formatFirstCell As XlFormattingObject, ByVal formatSecondCell As XlFormattingObject)
    Private methods As Dictionary(Of Integer, AddCells) = CreateMethodSet()

    Private Sub options_CustomizeSheetHeader(ByVal e As ContextEventArgs)
        ' Specify cell formatting. 
        Dim formatFirstCell = CreateXlFormattingObject(True, 12)
        Dim formatSecondCell = CreateXlFormattingObject(True, 12)
        ' Add new rows displaying custom information. 
        For i = 0 To 3
            Dim addCellMethod As AddCells = Nothing
            If methods.TryGetValue(i, addCellMethod) Then
                addCellMethod(e, formatFirstCell, formatSecondCell)
            Else
                e.ExportContext.AddRow()
            End If
        Next i
        ' Merge specific cells.
        MergeCells(e)
        '' Add an image to the top of the document.
        'Dim file = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Resources.1.jpg")
        'If file IsNot Nothing Then
        '    Dim imageToHeader = New Bitmap(Image.FromStream(file))
        '    Dim imageToHeaderRange = New XlCellRange(New XlCellPosition(0, 0), New XlCellPosition(5, 7))
        '    e.ExportContext.MergeCells(imageToHeaderRange)
        '    e.ExportContext.InsertImage(imageToHeader, imageToHeaderRange)
        'End If
        'e.ExportContext.MergeCells(New XlCellRange(New XlCellPosition(0, 8), New XlCellPosition(5, 8)))
    End Sub
    Private Shared Function CreateMethodSet() As Dictionary(Of Integer, AddCells)
        Dim dictionary = New Dictionary(Of Integer, AddCells)()
        dictionary.Add(1, AddressOf ReportTitleRow)
        dictionary.Add(2, AddressOf ReportTitleRunDAteTimeRow)
        dictionary.Add(3, AddressOf ReportTitleFromDateRow)
        'dictionary.Add(11, AddressOf AddPhoneRow)
        'dictionary.Add(12, AddressOf AddFaxRow)
        'dictionary.Add(13, AddressOf AddEmailRow)
        Return dictionary
    End Function
    Private Shared Sub ReportTitleFromDateRow(ByVal e As ContextEventArgs, ByVal formatFirstCell As XlFormattingObject, ByVal formatSecondCell As XlFormattingObject)
        'Dim AddressLocationCityCell = CreateCell("Los Angeles CA 90731 USA", formatSecondCell)
        'e.ExportContext.AddRow({Nothing, Nothing, AddressLocationCityCell})

        'nitin
        Dim ReportTitleFromDate = CreateCell(fromdatetodatetext, formatSecondCell)
        e.ExportContext.AddRow({Nothing, Nothing, Nothing, Nothing, ReportTitleFromDate})
    End Sub
    Private Shared Sub ReportTitleRunDAteTimeRow(ByVal e As ContextEventArgs, ByVal formatFirstCell As XlFormattingObject, ByVal formatSecondCell As XlFormattingObject)
        'Dim AddressLocationCityCell = CreateCell("Los Angeles CA 90731 USA", formatSecondCell)
        'e.ExportContext.AddRow({Nothing, Nothing, AddressLocationCityCell})

        'nitin
        Dim ReportTitleRunDAteTime = CreateCell("Run Date & Time : " & Common.runningDateTime, formatSecondCell)
        e.ExportContext.AddRow({Nothing, Nothing, Nothing, Nothing, ReportTitleRunDAteTime})
    End Sub
    Private Shared Sub ReportTitleRow(ByVal e As ContextEventArgs, ByVal formatFirstCell As XlFormattingObject, ByVal formatSecondCell As XlFormattingObject)
        'Dim AddressCellName = CreateCell("Address: ", formatFirstCell)
        'Dim AddresssCellLocation = CreateCell("807 West Paseo Del Mar", formatSecondCell)
        'e.ExportContext.AddRow({AddressCellName, Nothing, AddresssCellLocation})


        'nitin
        Dim ReportTitle = CreateCell("Company Name:" & CommonReport.g_CompanyNames, formatFirstCell)
        'Dim AddresssCellLocation = CreateCell("807 West Paseo Del Mar", formatSecondCell)
        e.ExportContext.AddRow({Nothing, Nothing, Nothing, Nothing, ReportTitle})
    End Sub
    ' Create a new cell with a specified value and format settings.
    Private Shared Function CreateCell(ByVal value As Object, ByVal formatCell As XlFormattingObject) As CellObject
        Return New CellObject With {.Value = value, .Formatting = formatCell}
    End Function
    ' Merge specific cells.
    Private Shared Sub MergeCells(ByVal e As ContextEventArgs)
        'MergeCells(e, 0, 1, 5, 1)
        'MergeCells(e, 0, 9, 1, 10)
        'MergeCells(e, 2, 10, 5, 10)
        'MergeCells(e, 0, 11, 1, 11)
        'MergeCells(e, 2, 11, 5, 11)
        'MergeCells(e, 0, 12, 1, 12)
        'MergeCells(e, 2, 12, 5, 12)
        'MergeCells(e, 0, 13, 1, 13)
        'MergeCells(e, 2, 13, 5, 13)
        'MergeCells(e, 0, 14, 5, 14)
    End Sub
    Private Shared Sub MergeCells(ByVal e As ContextEventArgs, ByVal left As Integer, ByVal top As Integer, ByVal right As Integer, ByVal bottom As Integer)
        e.ExportContext.MergeCells(New XlCellRange(New XlCellPosition(left, top), New XlCellPosition(right, bottom)))
    End Sub
    ' Specify a cell's alignment and font settings. 
    Private Shared Function CreateXlFormattingObject(ByVal bold As Boolean, ByVal size As Double) As XlFormattingObject
        Dim cellFormat = New XlFormattingObject With { _
            .Font = New XlCellFont With {.Bold = bold, .Size = size}, _
            .Alignment = New XlCellAlignment With {.RelativeIndent = 10, .HorizontalAlignment = XlHorizontalAlignment.Center, .VerticalAlignment = XlVerticalAlignment.Center} _
        }
        Return cellFormat
    End Function
#End Region ' #CustomizeSheetHeaderEvent
#Region "#AfterAddRowEvent"
    Private Sub options_AfterAddRow(ByVal e As AfterAddRowEventArgs)
        ' Merge cells in rows that correspond to the grid's group rows.
        If e.DataSourceRowIndex < 0 Then
            e.ExportContext.MergeCells(New XlCellRange(New XlCellPosition(0, e.DocumentRow + 1), New XlCellPosition(5, e.DocumentRow + 1)))
        End If
    End Sub
#End Region ' #AfterAddRowEvent
    Private Sub XtraReportGrid_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        GridView1.Columns.Clear()
    End Sub

End Class