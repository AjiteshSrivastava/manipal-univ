﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraCanteenTimeSlabEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditShift = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDEnd = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditDStrt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditLEnd = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditLStrt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditBFEnd = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditBFStrt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDStrt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditLEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditLStrt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBFEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBFStrt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.ComboBoxEdit1)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.TextEditShift)
        Me.GroupControl1.Controls.Add(Me.TextEditDEnd)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.TextEditDStrt)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.TextEditLEnd)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.TextEditLStrt)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.TextEditBFEnd)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.TextEditBFStrt)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 23)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(506, 112)
        Me.GroupControl1.TabIndex = 3
        Me.GroupControl1.Text = "Meal Time Slab"
        '
        'ComboBoxEdit1
        '
        Me.ComboBoxEdit1.EditValue = "BreakFast"
        Me.ComboBoxEdit1.Location = New System.Drawing.Point(394, 28)
        Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
        Me.ComboBoxEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit1.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit1.Properties.Items.AddRange(New Object() {"BreakFast", "Lunch", "Dinner"})
        Me.ComboBoxEdit1.Size = New System.Drawing.Size(100, 20)
        Me.ComboBoxEdit1.TabIndex = 2
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(311, 31)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(56, 14)
        Me.LabelControl8.TabIndex = 20
        Me.LabelControl8.Text = "Meal Type"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(17, 31)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(38, 14)
        Me.LabelControl7.TabIndex = 19
        Me.LabelControl7.Text = "Slab ID"
        '
        'TextEditShift
        '
        Me.TextEditShift.Location = New System.Drawing.Point(151, 28)
        Me.TextEditShift.Name = "TextEditShift"
        Me.TextEditShift.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditShift.Properties.Appearance.Options.UseFont = True
        Me.TextEditShift.Size = New System.Drawing.Size(72, 20)
        Me.TextEditShift.TabIndex = 1
        '
        'TextEditDEnd
        '
        Me.TextEditDEnd.EditValue = ""
        Me.TextEditDEnd.Location = New System.Drawing.Point(394, 106)
        Me.TextEditDEnd.Name = "TextEditDEnd"
        Me.TextEditDEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDEnd.Properties.Appearance.Options.UseFont = True
        Me.TextEditDEnd.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditDEnd.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditDEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditDEnd.Properties.MaxLength = 5
        Me.TextEditDEnd.Size = New System.Drawing.Size(72, 20)
        Me.TextEditDEnd.TabIndex = 17
        Me.TextEditDEnd.Visible = False
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(311, 109)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(52, 14)
        Me.LabelControl6.TabIndex = 16
        Me.LabelControl6.Text = "End Time"
        Me.LabelControl6.Visible = False
        '
        'TextEditDStrt
        '
        Me.TextEditDStrt.EditValue = ""
        Me.TextEditDStrt.Location = New System.Drawing.Point(151, 106)
        Me.TextEditDStrt.Name = "TextEditDStrt"
        Me.TextEditDStrt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDStrt.Properties.Appearance.Options.UseFont = True
        Me.TextEditDStrt.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditDStrt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditDStrt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditDStrt.Properties.MaxLength = 5
        Me.TextEditDStrt.Size = New System.Drawing.Size(72, 20)
        Me.TextEditDStrt.TabIndex = 15
        Me.TextEditDStrt.Visible = False
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(17, 109)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(97, 14)
        Me.LabelControl5.TabIndex = 14
        Me.LabelControl5.Text = "Dinner Start Time"
        Me.LabelControl5.Visible = False
        '
        'TextEditLEnd
        '
        Me.TextEditLEnd.EditValue = ""
        Me.TextEditLEnd.Location = New System.Drawing.Point(394, 80)
        Me.TextEditLEnd.Name = "TextEditLEnd"
        Me.TextEditLEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditLEnd.Properties.Appearance.Options.UseFont = True
        Me.TextEditLEnd.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditLEnd.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditLEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditLEnd.Properties.MaxLength = 5
        Me.TextEditLEnd.Size = New System.Drawing.Size(72, 20)
        Me.TextEditLEnd.TabIndex = 13
        Me.TextEditLEnd.Visible = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(311, 83)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(52, 14)
        Me.LabelControl4.TabIndex = 12
        Me.LabelControl4.Text = "End Time"
        Me.LabelControl4.Visible = False
        '
        'TextEditLStrt
        '
        Me.TextEditLStrt.EditValue = ""
        Me.TextEditLStrt.Location = New System.Drawing.Point(151, 80)
        Me.TextEditLStrt.Name = "TextEditLStrt"
        Me.TextEditLStrt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditLStrt.Properties.Appearance.Options.UseFont = True
        Me.TextEditLStrt.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditLStrt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditLStrt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditLStrt.Properties.MaxLength = 5
        Me.TextEditLStrt.Size = New System.Drawing.Size(72, 20)
        Me.TextEditLStrt.TabIndex = 11
        Me.TextEditLStrt.Visible = False
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(17, 83)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(95, 14)
        Me.LabelControl3.TabIndex = 10
        Me.LabelControl3.Text = "Lunch Start Time"
        Me.LabelControl3.Visible = False
        '
        'TextEditBFEnd
        '
        Me.TextEditBFEnd.EditValue = ""
        Me.TextEditBFEnd.Location = New System.Drawing.Point(394, 54)
        Me.TextEditBFEnd.Name = "TextEditBFEnd"
        Me.TextEditBFEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBFEnd.Properties.Appearance.Options.UseFont = True
        Me.TextEditBFEnd.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditBFEnd.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditBFEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditBFEnd.Properties.MaxLength = 5
        Me.TextEditBFEnd.Size = New System.Drawing.Size(72, 20)
        Me.TextEditBFEnd.TabIndex = 4
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(311, 57)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(52, 14)
        Me.LabelControl2.TabIndex = 8
        Me.LabelControl2.Text = "End Time"
        '
        'TextEditBFStrt
        '
        Me.TextEditBFStrt.EditValue = ""
        Me.TextEditBFStrt.Location = New System.Drawing.Point(151, 54)
        Me.TextEditBFStrt.Name = "TextEditBFStrt"
        Me.TextEditBFStrt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBFStrt.Properties.Appearance.Options.UseFont = True
        Me.TextEditBFStrt.Properties.Mask.EditMask = "HH:mm"
        Me.TextEditBFStrt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEditBFStrt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditBFStrt.Properties.MaxLength = 5
        Me.TextEditBFStrt.Size = New System.Drawing.Size(72, 20)
        Me.TextEditBFStrt.TabIndex = 3
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(17, 57)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Start Time"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(362, 143)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 5
        Me.SimpleButton1.Text = "Save"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(443, 143)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 6
        Me.SimpleButton2.Text = "Close"
        '
        'XtraCanteenTimeSlabEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(534, 175)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.GroupControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraCanteenTimeSlabEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDStrt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditLEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditLStrt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBFEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBFStrt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditDEnd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditDStrt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditLEnd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditLStrt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditBFEnd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditBFStrt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditShift As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
End Class
