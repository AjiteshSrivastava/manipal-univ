﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports DevExpress.LookAndFeel
Imports System.Net.Sockets
Imports System.Threading
Imports System.Net
Imports System.Text
Imports ZKPushDemoNitinCSharp
Imports Newtonsoft.Json.Linq
Imports iAS.B100Demo
Imports iAS.HSeriesSampleCSharp
Imports Newtonsoft.Json

'Imports System.Runtime.InteropServices

Public Class XtraRealTimePunches
    Public Delegate Sub invokeDelegate()
    Dim TmpLED As String = ""
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim ulf As UserLookAndFeel
    Dim adap As SqlDataAdapter
    Dim adapA As OleDbDataAdapter
    'Dim ds As DataSet = New DataSet
    'Dim Paycode As String
    'Dim Name As String
    Dim VerifyMode As String
    Dim PunchDate As String
    Dim PunchTime As String
    Dim Cn As Common '= New Common
    Dim BioPort As Integer
    Dim ZKPort As Integer
    Dim TWIR102Port As Integer
    Dim Bio1EcoPort As Integer
    Dim TF01Port As Integer
    'Zk
    Dim tcp As TcpListener
    Dim listenThread As Thread
    Dim result() As DataRow
    Dim listening As Boolean = False
    'Dim ATTLOGStamp As String = "0"
    Dim OPERLOGStamp As String = "0"
    Dim USERINFOStamp As String = "0"
    Dim ATTPHOTOStamp As String = "0"
    Dim fingertmpStamp As String = "0"
    Dim strImageNumber As String = ""
    Dim PathofImage As String = ""
    Dim machine As String = ""
    Dim sysIp As String

    Dim flagDownLoadPause As Boolean = False
    Dim flagDownLoadResume As Boolean = False
    Dim flagreboot As Boolean = False
    Dim flagshell As Boolean = False
    Dim flagcheck As Boolean = False
    Dim flaginfo As Boolean = False
    Dim flagoption As Boolean = False
    Dim flagclear As Boolean = False
    Dim flagupduserinfo As Boolean = False
    Dim flagunlock As Boolean = False
    Dim flagdeluserinfo As Boolean = False
    Dim flagsendbell As Boolean = False
    Dim flagdeluserpic As Boolean = False
    Dim flagsetuserpic As Boolean = False
    Dim deluserpicinput As String = ""
    Dim setuserpicinput As String = ""
    Dim shellinput As String = ""
    Dim optioninput As String = ""
    Dim clearinput As String = ""
    Dim upduserinfoinput As String = ""
    Dim deluserinfoinput As String = ""
    Dim h As IntPtr = IntPtr.Zero

    'End ZK

    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
    End Sub
    Private Sub XtraRealTimePunches_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Common.LogPost("Device Real Time Log Download Started")
        SidePanel3.Width = Me.Width * 80 / 100
        SidePanel1.Width = Me.Width * 20 / 100
        PictureEdit.Width = SidePanel1.Width
        PictureEdit.Height = SidePanel1.Height * 3 / 4
        Application.DoEvents()
        Cn = New Common
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = " Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup )"
        If Common.servername = "Access" Then
            sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
            adapAc = New OleDbDataAdapter(sSql, Common.con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, Common.con)
            adapS.Fill(Rs)
        End If
        BioPort = Convert.ToInt32(Rs.Tables(0).Rows(0).Item("BioPort").ToString.Trim)
        ZKPort = Convert.ToInt32(Rs.Tables(0).Rows(0).Item("ZKPort").ToString.Trim)
        Try
            Bio1EcoPort = Convert.ToInt32(Rs.Tables(0).Rows(0).Item("Bio1EcoPort").ToString.Trim)
        Catch ex As Exception
            Bio1EcoPort = 10003
        End Try
        Try
            TF01Port = Convert.ToInt32(Rs.Tables(0).Rows(0).Item("TF01Port").ToString.Trim)
        Catch ex As Exception
            TF01Port = 7788
        End Try

        Try
            TWIR102Port = Convert.ToInt32(Rs.Tables(0).Rows(0).Item("TWIR102Port").ToString.Trim)
        Catch ex As Exception
            TWIR102Port = 15000
        End Try

        lblTotal.Text = "Total : 0"
        sysIp = GetIP()
        StartRealTime()
        ZKAccessGetData()
        'doortestbio()
    End Sub
    Private Sub XtraRealTimePunches_Resize(sender As System.Object, e As System.EventArgs) Handles MyBase.Resize
        SidePanel3.Width = Me.Width * 80 / 100
        PictureEdit.Width = SidePanel1.Width
        PictureEdit.Height = SidePanel1.Height * 3 / 4
    End Sub
    Private Sub XtraRealTimePunches_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to stop Real Time Monitor?</size>", Common.res_man.GetString("confirmdeleteion", Common.cul),
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Dim svrPort As Integer = 0
            svrPort = BioPort
            If (Me.mbOpenFlag = True) Then
                AxRealSvrOcxTcp1.CloseNetwork(svrPort)
                Me.mbOpenFlag = False
                'Me.EnableButtons(True)
            End If
            Try
                'tcp.Stop()   'for zk
                'listenThread.Abort()

                Try
                    tcpServer1.Close()
                Catch ex As Exception
                    'TraceService(ex.Message, "Line 2295")
                End Try
                'Application.Exit()

            Catch ex As Exception
            End Try

            Try
                Dim ret As Integer = B100API.F_StopRecord() 'for TWIR102
            Catch ex As Exception

            End Try

            Try
                TimerBioEco.Enabled = False
                SyFunctions.CmdEndDetectionNewDailyEntryRecord()
            Catch ex As Exception

            End Try
            'For TF-01


            Me.AxFPCLOCK_Svr1.CloseNetwork(TF01Port)




            Common.LogPost("Device Real Time Log Download End")
        Else
            e.Cancel = True
        End If



    End Sub
    'for real time listning
    '=============== VerifyMode of GeneralLogData ===============//
    Public Enum enumGLogVerifyMode
        LOG_FPVERIFY = 1
        LOG_PASSVERIFY = 2
        LOG_CARDVERIFY = 3
        LOG_FPPASS_VERIFY = 4
        LOG_FPCARD_VERIFY = 5
        LOG_PASSFP_VERIFY = 6
        LOG_CARDFP_VERIFY = 7
        LOG_JOB_NO_VERIFY = 8
        LOG_CARDPASS_VERIFY = 9
        LOG_FACEVERIFY = 20
        LOG_FACECARDVERIFY = 21
        LOG_FACEPASSVERIFY = 22
        LOG_CARDFACEVERIFY = 23
        LOG_PASSFACEVERIFY = 24
    End Enum
    '=============== IOMode of GeneralLogData ===============//
    Public Enum enumGLogIOMode
        LOG_IOMODE_IO = 0
        LOG_IOMODE_IN1 = 1
        LOG_IOMODE_OUT1 = 2
        LOG_IOMODE_IN2 = 3
        LOG_IOMODE_OUT2 = 4
        LOG_IOMODE_IN3 = 5
        LOG_IOMODE_OUT3 = 6
        LOG_IOMODE_IN4 = 7
        LOG_IOMODE_OUT4 = 8
    End Enum
    Public Enum enumGLogDoorMode
        LOG_CLOSE_DOOR = 1
        LOG_OPEN_HAND = 2
        LOG_PROG_OPEN = 3
        LOG_PROG_CLOSE = 4
        LOG_OPEN_IREGAL = 5
        LOG_CLOSE_IREGAL = 6
        LOG_OPEN_COVER = 7
        LOG_CLOSE_COVER = 8
        LOG_OPEN_DOOR = 9
        LOG_OPEN_DOOR_THREAT = 10
    End Enum
    Public Enum enumVerifyKind
        VK_NONE = 0
        VK_FP = 1
        VK_PASS = 2
        VK_CARD = 3
        VK_FACE = 4
        VK_VEIN = 5
        VK_IRIS = 6
    End Enum
    Public Shared Function GetStringVerifyMode(ByVal nVerifyMode As Int32) As String
        Dim vRet As String = ""
        Dim vByteCount As Integer = 4
        Dim vbyteKind() As Byte = New Byte((vByteCount) - 1) {}
        Dim vSecondKind As Integer
        Dim vFirstKind As Integer
        vbyteKind = BitConverter.GetBytes(nVerifyMode)
        Dim nIndex As Integer = (vByteCount - 1)
        Do While (nIndex >= 0)
            vSecondKind = vbyteKind(nIndex)
            vFirstKind = vbyteKind(nIndex)
            vFirstKind = (vFirstKind And 240)
            vSecondKind = (vSecondKind And 15)
            vFirstKind = (vFirstKind + 4)
            If (vFirstKind = 0) Then
                Exit Do
            End If

            If (nIndex _
                        < (vByteCount - 1)) Then
                vRet = (vRet + "+")
            End If

            Select Case (vFirstKind)
                Case CType(enumVerifyKind.VK_FP, Integer)
                    vRet = (vRet + "FP")
                Case CType(enumVerifyKind.VK_PASS, Integer)
                    vRet = (vRet + "PASS")
                Case CType(enumVerifyKind.VK_CARD, Integer)
                    vRet = (vRet + "CARD")
                Case CType(enumVerifyKind.VK_FACE, Integer)
                    vRet = (vRet + "FACE")
                Case CType(enumVerifyKind.VK_VEIN, Integer)
                    vRet = (vRet + "VEIN")
                Case CType(enumVerifyKind.VK_IRIS, Integer)
                    vRet = (vRet + "IRIS")
            End Select

            If (vSecondKind = 0) Then
                Exit Do
            End If

            vRet = (vRet + "+")
            Select Case (vSecondKind)
                Case CType(enumVerifyKind.VK_FP, Integer)
                    vRet = (vRet + "FP")
                Case CType(enumVerifyKind.VK_PASS, Integer)
                    vRet = (vRet + "PASS")
                Case CType(enumVerifyKind.VK_CARD, Integer)
                    vRet = (vRet + "CARD")
                Case CType(enumVerifyKind.VK_FACE, Integer)
                    vRet = (vRet + "FACE")
                Case CType(enumVerifyKind.VK_VEIN, Integer)
                    vRet = (vRet + "VEIN")
                Case CType(enumVerifyKind.VK_IRIS, Integer)
                    vRet = (vRet + "IRIS")
            End Select

            nIndex = (nIndex - 1)
        Loop

        'nVerifyMode.
        If (vRet = "") Then
            vRet = "--"
        End If

        Return vRet
    End Function
    Public Shared Sub GetIoModeAndDoorMode(ByVal nIoMode As Int32, ByRef vIoMode As Integer, ByRef vDoorMode As Integer)
        Dim vByteCount As Integer = 4
        Dim vbyteKind() As Byte = New Byte((vByteCount) - 1) {}
        Dim vbyteDoorMode() As Byte = New Byte((vByteCount) - 1) {}
        vbyteKind = BitConverter.GetBytes(nIoMode)
        vIoMode = vbyteKind(0)
        Dim nIndex As Integer = 0
        Do While (nIndex < 3)
            vbyteDoorMode(nIndex) = vbyteKind((nIndex + 1))
            nIndex = (nIndex + 1)
        Loop

        vbyteDoorMode(3) = 0
        vDoorMode = BitConverter.ToInt32(vbyteDoorMode, 0)
    End Sub
    Public Shared Function GetLong(ByVal asVal As String) As Long
        Try
            Return Convert.ToInt32(asVal)
        Catch  'As System.Exception
            Return 0
        End Try

    End Function
    Public Shared Function GetInt(ByVal asVal As String) As Integer
        Try
            Return Convert.ToInt32(asVal)
        Catch  'As System.Exception
            Return 0
        End Try

    End Function
    Public Shared Function StringToByteArray(ByVal hex As String) As Byte()
        Return Enumerable.Range(0, hex.Length).Where(Function(x) x Mod 2 = 0).[Select](Function(x) Convert.ToByte(hex.Substring(x, 2), 16)).ToArray()
    End Function
    '=============== Error code ===============//
    Public Const RUN_SUCCESS As Integer = 1
    Public Const RUNERR_NOSUPPORT As Integer = 0
    Public Const RUNERR_UNKNOWNERROR As Integer = -1
    Public Const RUNERR_NO_OPEN_COMM As Integer = -2
    Public Const RUNERR_WRITE_FAIL As Integer = -3
    Public Const RUNERR_READ_FAIL As Integer = -4
    Public Const RUNERR_INVALID_PARAM As Integer = -5
    Public Const RUNERR_NON_CARRYOUT As Integer = -6
    Public Const RUNERR_DATAARRAY_END As Integer = -7
    Public Const RUNERR_DATAARRAY_NONE As Integer = -8
    Public Const RUNERR_MEMORY As Integer = -9
    Public Const RUNERR_MIS_PASSWORD As Integer = -10
    Public Const RUNERR_MEMORYOVER As Integer = -11
    Public Const RUNERR_DATADOUBLE As Integer = -12
    Public Const RUNERR_MANAGEROVER As Integer = -14
    Public Const RUNERR_FPDATAVERSION As Integer = -15
    Public Const gstrNoDevice As String = "No Device"
    '==========================================//
    Private mbOpenFlag As Boolean
    Private fnCount As Integer = 0

    Private Sub doortestbio()
        'TextEdit1.Text = "{"log_id":"2","fk_device_id":"1","user_id":"2","verify_mode":"FP","io_mode":"OUT","io_time":"20190510193915","emergency":"yes"}""
        TextEdit1.Text = "{""device_port"":5005,""emergency"":""yes"",""fk_device_id"":""1"",""io_mode"":""On Duty"",""io_time"":""20200918154148"",""log_id"":""0"",""serial_number"":""TIPL9632803200124"",""user_id"":""1000627"",""verify_mode"":""CARD""}"
        ' "{"log_id":"9999","fk_device_id":"1","user_id":"9999","verify_mode":"FP","io_mode":"OUT","io_time":"20190412171949","emergency":"yes"}"
        'MsgBox(e.astrLogText)
        Dim RecText As String = TextEdit1.Text

        Dim s As String = RecText
        Dim e As Char = s((s.Length - 1))
        If e = """" Then
            RecText = s.Substring(0, (s.Length - 1))
        End If
        Dim vstrLogId As String
        Dim vstrEnrollNumber As String
        Dim vstrDeviceID As String
        Dim vstrVerifyMode As String
        Dim vstrInOutMode As String
        Dim vstrDate As String
        Dim vDate As String
        Dim vstrResponse As String
        Dim vstrEmergency As String
        Dim vstrIsSupportStringID As String
        Dim jobjTest As JObject = JObject.Parse(RecText)
        vstrLogId = jobjTest("log_id").ToString()
        vstrEnrollNumber = jobjTest("user_id").ToString()
        vstrDeviceID = jobjTest("fk_device_id").ToString()
        vstrVerifyMode = jobjTest("verify_mode").ToString()
        If vstrVerifyMode = "FP" Then
            vstrVerifyMode = 1
        ElseIf vstrVerifyMode.ToLower = "card" Then
            vstrVerifyMode = 3
        ElseIf vstrVerifyMode.ToLower = "face" Then
            vstrVerifyMode = 20
        End If
        vstrInOutMode = jobjTest("io_mode").ToString()
        vstrDate = jobjTest("io_time").ToString()
        vDate = vstrDate.Substring(0, 4) & "-" & vstrDate.Substring(4, 2) & "-" & vstrDate.Substring(6, 2) & " " & vstrDate.Substring(8, 2) & ":" & vstrDate.Substring(10, 2) & ":" & vstrDate.Substring(12, 2)
        fnCount = fnCount + 1
        'ShowGeneralLogDataToGrid(fnCount, vstrLogId, vstrEnrollNumber, vstrVerifyMode, vstrInOutMode, vDate, True, e.astrClientIP, e.anClientPort, vstrDeviceID)
        'txtStatus.Text = "Last Log :{ user_id: " & vstrEnrollNumber & ", io_time :" + vDate & "}"
        'showLogImage(e.astrLogImage)
        Dim jobjRespond As JObject = New JObject()
        jobjRespond.Add("log_id", vstrLogId)
        jobjRespond.Add("result", "OK")
        'MsgBox(vstrEnrollNumber & vbCrLf & vstrVerifyMode & vbCrLf & Convert.ToDateTime(vDate))
        'Try

        Me.funcShowGeneralLogDataToGrid(vstrEnrollNumber, vstrVerifyMode, 1, Convert.ToDateTime(vDate), True, "", 1, 1, "", "", "Bio")
        'Catch ex As Exception

        'End Try
    End Sub
    Private Sub StartRealTime()
        Dim svrPortBio As Integer = 0
        Dim retCodeBio As Integer = 0

        Dim svrPortZK As Integer = 0
        Dim retCodeZK As Integer = 0
        Try
            svrPortBio = BioPort
            'SimpleButtonStart.Enabled = False
            retCodeBio = AxRealSvrOcxTcp1.OpenNetwork(svrPortBio)
            If (retCodeBio = RUN_SUCCESS) Then
                Me.mbOpenFlag = True
                'Me.EnableButtons(False)
            Else
                XtraMessageBox.Show(ulf, "<size=10> BIO " & Me.ReturnResultPrint(retCodeBio) & "</size>", "<size=9>Error</size>")
                'SimpleButtonStart.Enabled = True
            End If
            'Me.SimpleButtonStop(sender, e)
        Catch
            XtraMessageBox.Show(ulf, "<size=10>Invalid Port For BIO</size>", "<size=9>Error</size>")
        End Try
        ''ZK
        ''Me.listenThread = New Thread(ListenClient)
        ''listenThread = New Thread(Sub() ListenClient())
        'Me.listenThread = New Thread(New ThreadStart(AddressOf Me.ListenClient))
        'Me.listenThread.Start()
        'Me.listenThread.IsBackground = True
        openTcpPort()

        'ListenClient()
        listening = True
        'End ZK

        startTWIR() 'TWIR102
        lblTotal.Text = "Total : 0"
        SetupLogListview()
        startBioEco()
        StartTF01()
    End Sub
    Private Sub StartTF01()
        Try
            'Dim str As String = "7006"
            'Dim nPort As Integer = Convert.ToInt32(TF01Port)
            Me.AxFPCLOCK_Svr1.OpenNetwork(TF01Port)
            ' Me.connect.Enabled = False
            ' Me.Disconnect.Enabled = True

        Catch ex As Exception

        End Try
    End Sub
    Private Sub startBioEco()
        Dim snc As SyNetCfg = New SyNetCfg
        Dim host As IPHostEntry
        host = Dns.GetHostEntry(Dns.GetHostName)
        snc.mIPAddr = New Byte((4) - 1) {}
        Array.Copy(host.AddressList(0).GetAddressBytes, 0, snc.mIPAddr, 0, 4)
        snc.mPortNo = Bio1EcoPort ' 9000 ' Convert.ToUInt16(txtPortNumber.Text)
        snc.mIPAddr(0) = 0
        snc.mIPAddr(1) = 0
        snc.mIPAddr(2) = 0
        snc.mIPAddr(3) = 0
        Dim ret As Int32 = SyFunctions.CmdStartDetectionNewDailyEntryRecord(snc)
        If (CType(SyLastError.sleSuss, Int32) <> ret) Then
            XtraMessageBox.Show(ulf, "<size=10> Bio1Eco " & util.ErrorPrint(ret) & "</size>", "<size=9>Error</size>")
            'MessageBox.Show(("Error " + util.ErrorPrint(ret)))
            Return
        End If
        TimerBioEco.Enabled = True
    End Sub
    Private Sub SetupLogListview()
        GridControl1.DataSource = Nothing
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("No")
        dt.Columns.Add("EnrollNo")
        dt.Columns.Add("Paycode")
        dt.Columns.Add("Name")
        dt.Columns.Add("VerifyMode")
        'dt.Columns.Add("InOut")
        dt.Columns.Add("Punch Date")
        dt.Columns.Add("Punch Time")
        dt.Columns.Add("IP/Serial No")
        'dt.Columns.Add("Port")
        dt.Columns.Add("DevID")
        'dt.Columns.Add("SerialNo")
        'dt.Columns.Add("RouterIP")

        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridControl1.DataSource = dt
        GridView1.Columns.Item(0).Width = 40
        GridView1.Columns.Item(1).Width = 100
        GridView1.Columns.Item(7).Width = 100
        Dim strLogItem() As String = New String() {"No", "EnrollNo", "VerifyMode", "InOut", "DateTime", "IP", "Port", "DevID", "SerialNo", "RouterIP"}
        lvwLogList.Items.Clear()
        lvwLogList.Columns.Clear()
        For Each strTmp As String In strLogItem
            lvwLogList.Columns.Add(strTmp, 80, HorizontalAlignment.Center)
        Next
        lvwLogList.Columns(0).Width = 60
        lvwLogList.Columns(0).TextAlign = HorizontalAlignment.Right
        lvwLogList.Columns(1).Width = 120
        lvwLogList.Columns(2).Width = 120
        lvwLogList.Columns(4).Width = 140
    End Sub
    Public Function ReturnResultPrint(ByVal anResultCode As Integer) As String
        Dim retval As String = Nothing
        Select Case (anResultCode)
            Case RUN_SUCCESS
                retval = "Successful!"
            Case RUNERR_NOSUPPORT
                retval = "No support"
            Case RUNERR_UNKNOWNERROR
                retval = "Unknown error"
            Case RUNERR_NO_OPEN_COMM
                retval = "Cannot Open Connection"
            Case RUNERR_WRITE_FAIL
                retval = "Write Error"
            Case RUNERR_READ_FAIL
                retval = "Read Error"
            Case RUNERR_INVALID_PARAM
                retval = "Parameter Error"
            Case RUNERR_NON_CARRYOUT
                retval = "execution of command failed"
            Case RUNERR_DATAARRAY_END
                retval = "End of data"
            Case RUNERR_DATAARRAY_NONE
                retval = "Nonexistence data"
            Case RUNERR_MEMORY
                retval = "Memory Allocating Error"
            Case RUNERR_MIS_PASSWORD
                retval = "License Error"
            Case RUNERR_MEMORYOVER
                retval = "full enrolldata & can`t put enrolldata"
            Case RUNERR_DATADOUBLE
                retval = "this ID is already  existed."
            Case RUNERR_MANAGEROVER
                retval = "full manager & can`t put manager."
            Case RUNERR_FPDATAVERSION
                retval = "mistake fp data version."
            Case Else
                retval = "Unknown error"
        End Select

        Return retval
    End Function
    Private Sub showLogImage(ByVal astrBuffer As String)
        Dim bytImg() As Byte = Nothing
        Dim varImg As Object
        Try
            bytImg = StringToByteArray(astrBuffer)
            If (bytImg.Length < 1) Then
                Return
            End If

            'varImg = New System.Runtime.InteropServices.VariantWrapper(bytImg)
            'axAxImage1.SetJpgImage(varImg)
        Catch e As Exception

        End Try

    End Sub
    Private Function funcShowGeneralLogDataToGrid(ByVal aEnrollNumber As Long, ByVal aVerifyMode As Long, ByVal aInOutMode As Long, ByVal adtLog As DateTime, ByVal abDrawFlag As Boolean, ByVal astrRemoteIP As String, ByVal anRemotePort As Long, ByVal anDeviceID As String, ByVal astrSerialNo As String, ByVal astrRouterIP As String, deviceModel As String) As Boolean
        If aEnrollNumber = 0 Then
            Exit Function
        End If
        'MsgBox(aEnrollNumber)
        'Dim vItem As ListViewItem
        Dim strTmp As String = Nothing
        Dim vDoorMode As Integer = 0
        Dim vIoMode As Integer = 0
        'vItem = New ListViewItem
        'vItem.Text = Convert.ToString((lvwLogList.Items.Count + 1))
        'vItem.SubItems.Add(Convert.ToString(aEnrollNumber))

        Select Case (aVerifyMode)
            Case CType(enumGLogVerifyMode.LOG_FPVERIFY, Integer)
                '1
                strTmp = "Fp"
            Case CType(enumGLogVerifyMode.LOG_PASSVERIFY, Integer)
                '2
                strTmp = "Password"
            Case CType(enumGLogVerifyMode.LOG_CARDVERIFY, Integer)
                '3
                strTmp = "Card"
            Case CType(enumGLogVerifyMode.LOG_FPPASS_VERIFY, Integer)
                '4
                strTmp = "Fp+Password"
            Case CType(enumGLogVerifyMode.LOG_FPCARD_VERIFY, Integer)
                '5
                strTmp = "Fp+Card"
            Case CType(enumGLogVerifyMode.LOG_PASSFP_VERIFY, Integer)
                '6
                strTmp = "Password+Fp"
            Case CType(enumGLogVerifyMode.LOG_CARDFP_VERIFY, Integer)
                '7
                strTmp = "Card+Fp"
            Case CType(enumGLogVerifyMode.LOG_FACEVERIFY, Integer)
                '20
                strTmp = "Face"
            Case CType(enumGLogVerifyMode.LOG_FACECARDVERIFY, Integer)
                '21
                strTmp = "Face+Card"
            Case CType(enumGLogVerifyMode.LOG_FACEPASSVERIFY, Integer)
                '22
                strTmp = "Face+Pass"
            Case CType(enumGLogVerifyMode.LOG_PASSFACEVERIFY, Integer)
                '21
                strTmp = "Pass+Face"
            Case CType(enumGLogVerifyMode.LOG_CARDFACEVERIFY, Integer)
                '22
                strTmp = "Card+Face"
            Case Else
                strTmp = GetStringVerifyMode(CType(aVerifyMode, Integer))
        End Select
        VerifyMode = strTmp
        If deviceModel.Trim = "TF-01" Then
            VerifyMode = ""
        End If

        'MsgBox("VerifyMode " & VerifyMode)
        GetIoModeAndDoorMode(CType(aInOutMode, Integer), vIoMode, vDoorMode)
        If (vDoorMode = 0) Then
            'MessageBox.Show(vIoMode)
            strTmp = "( " & vIoMode & " )"
        Else
            strTmp = "( " & vIoMode & " ) "
        End If
        Select Case (vDoorMode)
            Case CType(enumGLogDoorMode.LOG_CLOSE_DOOR, Integer)
                strTmp = (strTmp & "Close Door")
            Case CType(enumGLogDoorMode.LOG_OPEN_HAND, Integer)
                strTmp = (strTmp & "Hand Open")
            Case CType(enumGLogDoorMode.LOG_PROG_OPEN, Integer)
                strTmp = (strTmp & "Prog Open")
            Case CType(enumGLogDoorMode.LOG_PROG_CLOSE, Integer)
                strTmp = (strTmp & "Prog Close")
            Case CType(enumGLogDoorMode.LOG_OPEN_IREGAL, Integer)
                strTmp = (strTmp & "Illegal Open")
            Case CType(enumGLogDoorMode.LOG_CLOSE_IREGAL, Integer)
                strTmp = (strTmp & "Illegal Close")
            Case CType(enumGLogDoorMode.LOG_OPEN_COVER, Integer)
                strTmp = (strTmp & "Cover Open")
            Case CType(enumGLogDoorMode.LOG_CLOSE_COVER, Integer)
                strTmp = (strTmp & "Cover Close")
            Case CType(enumGLogDoorMode.LOG_OPEN_DOOR, Integer)
                strTmp = (strTmp & "Open door")
            Case CType(enumGLogDoorMode.LOG_OPEN_DOOR_THREAT, Integer)
                strTmp = (strTmp & "Open Door as Threat")
        End Select
        If (strTmp = "") Then
            strTmp = "--"
        End If
        strTmp = Convert.ToString(adtLog)
        PunchDate = adtLog.ToString("dd MMM yyyy")
        PunchTime = adtLog.ToString("HH:mm")

        'MsgBox("PunchDate " & adtLog.ToString("dd MMM yyyy HH:mm:ss"))

        Dim ds As DataSet = New DataSet
        Dim IN_OUT As String = ""
        Dim ssql As String = ""
        Dim LocName As String = "" 'IOCL
        Dim LocCode As String = "" 'IOCL
        If deviceModel = "Bio1Eco" Then
            ssql = "SELECT ID_NO, IN_OUT from tblMachine where ID_NO='" & anDeviceID & "'"
        Else
            ssql = "SELECT ID_NO, IN_OUT, branch from tblMachine where MAC_ADDRESS='" & astrSerialNo & "'"
            If astrSerialNo = "" Then
                'for IOCL
                ssql = "SELECT ID_NO, IN_OUT, branch, BRANCHCODE from tblMachine, tblbranch where LOCATION='" & astrRemoteIP & "' and tblMachine.branch=tblbranch.BRANCHNAME"
            Else
                'for IOCL
                ssql = "SELECT ID_NO, IN_OUT, branch, BRANCHCODE from tblMachine, tblbranch where MAC_ADDRESS='" & astrSerialNo & "' and tblMachine.branch=tblbranch.BRANCHNAME"
            End If

        End If

        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            anDeviceID = ds.Tables(0).Rows(0).Item("ID_NO").ToString.Trim
            IN_OUT = ds.Tables(0).Rows(0).Item("IN_OUT").ToString.Trim

            LocCode = ds.Tables(0).Rows(0).Item("BRANCHCODE").ToString.Trim
            LocName = ds.Tables(0).Rows(0).Item("branch").ToString.Trim
        Else
            anDeviceID = ""
            IN_OUT = ""
        End If

        ''hexa world
        'Try
        '    Common.JSONCallHexa(aEnrollNumber.ToString, adtLog, anDeviceID)
        'Catch ex As Exception
        '    TextEdit1.Text = ex.Message.ToString.Trim
        'End Try
        '' end hexa world



        ''end for one customer
        'Try
        '    Dim sSqlX As String = "insert into Rawdata ([CARDNO],[OFFICEPUNCH],[Id_No],[Inout]) values('" & aEnrollNumber.ToString("000000000000") & "','" & adtLog.ToString("yyyy-MM-dd HH:mm:ss") & "','" & anDeviceID & "','" & IN_OUT & "') "
        '    If Common.servername = "Access" Then
        '        If Common.con1.State <> ConnectionState.Open Then
        '            Common.con1.Open()
        '        End If
        '        cmd1 = New OleDbCommand(sSqlX, Common.con1)
        '        cmd1.ExecuteNonQuery()
        '        If Common.con1.State <> ConnectionState.Closed Then
        '            Common.con1.Close()
        '        End If
        '    Else
        '        If Common.con.State <> ConnectionState.Open Then
        '            Common.con.Open()
        '        End If
        '        cmd = New SqlCommand(sSqlX, Common.con)
        '        cmd.ExecuteNonQuery()
        '        If Common.con.State <> ConnectionState.Closed Then
        '            Common.con.Close()
        '        End If
        '    End If
        'Catch
        'End Try
        ''end for one customer


        ''Centum Learning 
        'Dim url1 As String = ""
        'Try
        '    url1 = "https://ptapi.centumlearning.com/api/Master/CardSwipe?paramAPIKey=AyB7RthnDxh4vzUtXHMg&paramEmpCode=" + aEnrollNumber.ToString.Trim() & "&paramDate=" + adtLog.ToString("dd-MMM-yyyy") + "&paramTime=" + adtLog.ToString("HH:mm:ss") + ""
        '    ' TraceServiceSuccess(url1.ToString.Trim)
        '    Dim WebRequestObject As HttpWebRequest = CType(HttpWebRequest.Create(url1), HttpWebRequest)
        '    WebRequestObject.Timeout = Timeout.Infinite
        '    WebRequestObject.KeepAlive = True
        '    Dim response As HttpWebResponse
        '    response = CType(WebRequestObject.GetResponse, HttpWebResponse)
        '    response.Close()
        '    'TraceServiceSuccess(("Sent Success:True  " + url1))
        '    'Return True
        '    Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
        '    Dim sw As StreamWriter = New StreamWriter(fs)
        '    sw.BaseStream.Seek(0, SeekOrigin.End)
        '    sw.WriteLine(vbCrLf & url1 & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        '    sw.Flush()
        '    sw.Close()

        'Catch Ex As Exception
        '    'Return False
        '    'TraceService(Ex.Message.ToString)
        '    Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
        '    Dim sw As StreamWriter = New StreamWriter(fs)
        '    sw.BaseStream.Seek(0, SeekOrigin.End)
        '    sw.WriteLine(vbCrLf & url1 & ":" & Ex.Message.ToString & ":" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        '    sw.Flush()
        '    sw.Close()
        'End Try


        ssql = "select * from tblemployee, EmployeeGroup where PRESENTCARDNO='" & aEnrollNumber.ToString("000000000000") & "' and EmployeeGroup.GroupId=TblEmployee.EmployeeGroupId"
        ' ssql = "select * from tblemployee where PRESENTCARDNO='" & aEnrollNumber.ToString("000000000000") & "'"
        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        Dim Paycode As String = ""
        Dim Name As String = ""
        Dim EId As Integer
        Dim ssql1 As String
        If ds.Tables(0).Rows.Count > 0 Then
            Name = ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim
            Paycode = ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim
            EId = ds.Tables(0).Rows(0).Item("Id")
            LocCode = ds.Tables(0).Rows(0).Item("COMPANYCODE").ToString.Trim


            '' only for Regal Systems & Services
            'Try
            '    Dim LastDate As DateTime = ds.Tables(0).Rows(0).Item("ValidityEndDate").ToString.Trim
            '    LastDate = DateAdd("D", -3, LastDate)
            '    If CDate(Format(Now(), "yyyy-MM-dd")) >= CDate(Format(LastDate, "yyyy-MM-dd")) Then
            '        XtraMessageBox.Show(ulf, "Hi, Membership for Paycode:" & Paycode & " and Name:" & Name & " is going to expire on " & Convert.ToDateTime(ds.Tables(0).Rows(0).Item("ValidityEndDate").ToString.Trim).ToString("dd-MMM-yyyy") & " Please renew this. Thanks", "iAS")
            '    End If
            'Catch ex As Exception

            'End Try
            ''end only for Regal Systems & Services


            'Else
            '    Name = "Not registered"
            '    Paycode = ""
            'End If
            If IN_OUT = "B" Then
                Dim ds2 As DataSet = New DataSet
                Dim adapT As SqlDataAdapter
                Dim adapTA As OleDbDataAdapter
                If Common.servername = "Access" Then
                    ssql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & aEnrollNumber.ToString("000000000000") & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & adtLog.ToString("yyyy-MM-dd") & "'"
                    adapTA = New OleDbDataAdapter(ssql1, Common.con1)
                    adapTA.Fill(ds2)
                Else
                    ssql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & aEnrollNumber.ToString("000000000000") & "' and convert(varchar, OFFICEPUNCH, 23) = '" & adtLog.ToString("yyyy-MM-dd") & "'"
                    adapT = New SqlDataAdapter(ssql1, Common.con)
                    adapT.Fill(ds2)
                End If

                'adapT.Fill(ds2)
                If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                    IN_OUT = "I"
                Else
                    If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                        IN_OUT = "I"
                    Else
                        IN_OUT = "O"
                    End If
                End If
            ElseIf IN_OUT = "D" Then
                If aInOutMode = 1 Then
                    IN_OUT = "I"
                ElseIf aInOutMode = 2 Then
                    IN_OUT = "O"
                End If
            End If

            ''IOCL
            'Try
            '    Common.JSONCallIOCL(aEnrollNumber.ToString, adtLog, anDeviceID, LocCode, LocName, IN_OUT, anDeviceID)
            'Catch ex As Exception
            '    TextEdit1.Text = ex.Message.ToString.Trim & " For " & aEnrollNumber.ToString & ", " & PunchDate
            'End Try
            '' end IOCL


            ssql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[DownloadedTime]) values('" & aEnrollNumber.ToString("000000000000") & "','" & adtLog.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & anDeviceID & "','" & IN_OUT & "','" & Paycode & "','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "' ) "
            ssql1 = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[DownloadedTime]) values('" & aEnrollNumber.ToString("000000000000") & "','" & adtLog.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & anDeviceID & "','" & IN_OUT & "','" & Paycode & "','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "' ) "
            Try
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(ssql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    Try
                        cmd1 = New OleDbCommand(ssql1, Common.con1)
                        cmd1.ExecuteNonQuery()
                    Catch ex As Exception
                    End Try
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(ssql, Common.con)
                    cmd.ExecuteNonQuery()
                    Try
                        cmd = New SqlCommand(ssql1, Common.con)
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception

                    End Try
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If


                '#Region "IDTECH"  'Manipal
                '                Try 'IDTECH
                '                    ' Json
                '                    Try
                '                        Dim CardType As String = "", CardSRNNO As String = "", PunchType As String = "", Category As String = "", RefNo As String = "", OrgID As String = "", PresentCardNo As String = ""
                '                        Dim MachineID As Int64 = 1

                '                        Dim FinalData As JObject = New JObject
                '                        Dim httpWebRequest = CType(WebRequest.Create("https://in.nexuba.com/S/E2Pro_ASL_Inbound.php"), HttpWebRequest)
                '                        httpWebRequest.ContentType = "application/json"
                '                        httpWebRequest.Method = "POST"
                '                        ServicePointManager.Expect100Continue = True
                '                        ServicePointManager.SecurityProtocol = CType(3072, SecurityProtocolType)
                '                        Dim jsons As String
                '                        Dim PunchDate As DateTime = Convert.ToDateTime(adtLog.ToString())
                '                        Dim jStrong = "{""type"": 1,""data"":{""org_id"":""1046"",""badge_id"":""" & aEnrollNumber & """,""device_id"":""" & machine & """,""swipetime"":""" & PunchDate.ToString("yyyy-MM-dd HH:mm:ss") & """,""security token"":""xyz13aso""}}"
                '                        jsons = JsonConvert.ToString(jStrong)
                '                        Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
                '                            ''EnrollNumber = EnrollNumber.Trim
                '                            'FinalData.Add("type", "1")
                '                            'FinalData.Add("org_id", "1046")
                '                            'FinalData.Add("badge_id", EnrollNumber.Trim)
                '                            'FinalData.Add("device_id", deviceSerialNumber)
                '                            'FinalData.Add("swipetime", PunchDate.ToString("yyyy-MM-dd HH:mm:ss"))
                '                            'FinalData.Add("security token", "xyz13aso")
                '                            ' jsons = FinalData.ToString(Formatting.None)
                '                            streamWriter.Write(jStrong)
                '                            streamWriter.Flush()
                '                            streamWriter.Close()
                '                        End Using
                '                        Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
                '                        Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                '                            Dim result = streamReader.ReadToEnd()
                '                            Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
                '                            Dim sw As StreamWriter = New StreamWriter(fs)
                '                            ' Dim sw As StreamWriter = New StreamWriter(fs)
                '                            sw.BaseStream.Seek(0, SeekOrigin.End)
                '                            sw.WriteLine(vbCrLf & jStrong & ", " & result & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                '                            sw.Flush()
                '                            sw.Close()
                '                        End Using
                '                    Catch ex As Exception
                '                        'TraceService(ex.Message, "JSON")
                '                        Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
                '                        Dim sw As StreamWriter = New StreamWriter(fs)
                '                        ' Dim sw As StreamWriter = New StreamWriter(fs)
                '                        sw.BaseStream.Seek(0, SeekOrigin.End)
                '                        sw.WriteLine(vbCrLf & ex.Message & ", JSON::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                '                        sw.Flush()
                '                        sw.Close()
                '                    End Try
                '                    'end jSON
                '                Catch ex As Exception

                '                End Try
                '#End Region


                If Common.IsParallel = "Y" Then
                    Dim parallelThread As Thread = New Thread(Sub() Common.parallelInsert(aEnrollNumber, Paycode, adtLog, IN_OUT, "N", Name, anDeviceID))
                    parallelThread.Start()
                    parallelThread.IsBackground = True
                End If


                'sms
                If Common.g_SMSApplicable = "" Then
                    Common.Load_SMS_Policy()
                End If
                If Common.g_SMSApplicable = "Y" Then
                    If Common.g_isAllSMS = "Y" Then
                        Try
                            If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then
                                Dim SmsContent As String = ""
                                If Common.g_DeviceWiseInOut = "Y" Then
                                    If IN_OUT = "I" Then
                                        SmsContent = Common.g_InContent1 & " " & adtLog.ToString("yyyy-MM-dd HH:mm") & " " & Common.g_InContent2
                                    ElseIf IN_OUT = "O" Then
                                        SmsContent = Common.g_OutContent1 & " " & adtLog.ToString("yyyy-MM-dd HH:mm") & " " & Common.g_OutContent2
                                    End If
                                Else
                                    SmsContent = Common.g_AllContent1 & " " & adtLog.ToString("yyyy-MM-dd HH:mm") & " " & Common.g_AllContent2
                                End If
                                SmsContent = Name & " " & SmsContent
                                Cn.sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                End If
                'sms end
            Catch ex As Exception
                'MsgBox(ex.Message)
            End Try
            Cn.Remove_Duplicate_Punches(adtLog, Paycode, EId)
            Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PAYCODE = '" & Paycode & "'"
            ds = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSqltmp, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                'Dim mindatetmp As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                    Cn.Process_AllRTC(adtLog.AddDays(-1), Now.Date, Paycode, Paycode, EId)
                Else
                    Cn.Process_AllnonRTC(adtLog, Now.Date, Paycode, Paycode, EId)
                    If Common.EmpGrpArr(EId).SHIFTTYPE = "M" Then
                        Cn.Process_AllnonRTCMulti(adtLog, Now.Date, Paycode, Paycode, EId)
                    End If
                End If
            End If
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
        Else
            Name = "Not registered"
            Paycode = ""
        End If


        Try
            GridView1.AddNewRow()
            Dim rowHandle As Integer = GridView1.GetRowHandle(GridView1.DataRowCount)
            If GridView1.IsNewItemRow(rowHandle) Then
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(0), GridView1.RowCount)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(1), aEnrollNumber.ToString("000000000000"))
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(2), Paycode)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(3), Name)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), VerifyMode)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(5), PunchDate)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(6), PunchTime)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(7), astrRemoteIP)
                GridView1.SetRowCellValue(rowHandle, GridView1.Columns(8), anDeviceID)
            End If
            lblTotal.Text = GridView1.RowCount '("Total : " & lvwLogList.Items.Count)
            Try
                If System.IO.File.Exists("./EmpImages/" & Paycode & ".jpg") Then
                    'VisitorPictureEdit.Image = Image.FromFile("./EmpImages/" & TextEdit2.Text & ".jpg")
                    PictureEdit.LoadAsync("./EmpImages/" & Paycode & ".jpg")
                Else
                    PictureEdit.Image = Nothing
                End If
            Catch ex As Exception
            End Try

            If GridView1.RowCount > 500 Then
                lblTotal.Text = "Total : 0"
                Me.SetupLogListview()
            End If

            ''only for bio with wifi
            'Dim processThread As Thread
            'processThread = New Thread(Sub() Me.threadPrecess(adtLog, astrSerialNo, aEnrollNumber, astrRemoteIP))
            'processThread.Start()
            ''end only for bio with wifi
        Catch ex As Exception
            'MsgBox("Show grid " & ex.Message)
        End Try
        GC.Collect()
        Return True
    End Function
    Private Sub threadPrecess(adtLog As DateTime, astrSerialNo As String, aEnrollNumber As Double, astrRemoteIP As String)   'only for bio with wifi
        Dim ssql As String = "SELECT ID_NO, IN_OUT from tblMachine where MAC_ADDRESS='" & astrSerialNo & "'"
        Dim con As SqlConnection '= Common.con
        Dim con1 As OleDbConnection ' = Common.con1
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            con1 = New OleDbConnection(Common.ConnectionString)
        Else
            con = New SqlConnection(Common.ConnectionString)
        End If
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(ssql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(ssql, con)
            adap.Fill(ds)
        End If
        Dim anDeviceID As String = ""
        Dim IN_OUT As String = ""
        If ds.Tables(0).Rows.Count > 0 Then
            anDeviceID = ds.Tables(0).Rows(0).Item("ID_NO").ToString.Trim
            IN_OUT = ds.Tables(0).Rows(0).Item("IN_OUT").ToString.Trim
        End If

        ssql = "select * from tblemployee, EmployeeGroup where PRESENTCARDNO='" & aEnrollNumber.ToString("000000000000") & "' and EmployeeGroup.GroupId=TblEmployee.EmployeeGroupId"
        '  ssql = "select * from tblemployee where PRESENTCARDNO='" & aEnrollNumber.ToString("000000000000") & "'"
        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(ssql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(ssql, con)
            adap.Fill(ds)
        End If
        Dim Paycode As String = ""
        Dim Name As String = ""
        Dim EId As Integer
        Dim sSql1 As String
        If ds.Tables(0).Rows.Count > 0 Then
            Name = ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim
            Paycode = ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim
            EId = ds.Tables(0).Rows(0).Item("Id")
            If IN_OUT = "B" Then
                Dim ds2 As DataSet = New DataSet
                Dim adapT As SqlDataAdapter
                Dim adapTA As OleDbDataAdapter
                If Common.servername = "Access" Then
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & aEnrollNumber.ToString("000000000000") & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & adtLog.ToString("yyyy-MM-dd") & "'"
                    adapTA = New OleDbDataAdapter(sSql1, con1)
                    adapTA.Fill(ds)
                Else
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & aEnrollNumber.ToString("000000000000") & "' and convert(varchar, OFFICEPUNCH, 23) = '" & adtLog.ToString("yyyy-MM-dd") & "'"
                    adapT = New SqlDataAdapter(sSql1, con)
                    adapT.Fill(ds)
                End If

                adapT.Fill(ds2)
                If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                    IN_OUT = "I"
                Else
                    If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                        IN_OUT = "I"
                    Else
                        IN_OUT = "O"
                    End If
                End If
            End If

            ssql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & aEnrollNumber.ToString("000000000000") & "','" & adtLog.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & anDeviceID & "','" & IN_OUT & "','" & Paycode & "' ) "
            sSql1 = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & aEnrollNumber.ToString("000000000000") & "','" & adtLog.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & anDeviceID & "','" & IN_OUT & "','" & Paycode & "' ) "
            Try
                If Common.servername = "Access" Then
                    If con1.State <> ConnectionState.Open Then
                        con1.Open()
                    End If
                    cmd1 = New OleDbCommand(ssql, con1)
                    cmd1.ExecuteNonQuery()
                    Try
                        cmd1 = New OleDbCommand(sSql1, con1)
                        cmd1.ExecuteNonQuery()
                    Catch ex As Exception
                    End Try
                    If con1.State <> ConnectionState.Closed Then
                        con1.Close()
                    End If
                Else
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    cmd = New SqlCommand(ssql, con)
                    cmd.ExecuteNonQuery()
                    Try
                        cmd = New SqlCommand(sSql1, con)
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception

                    End Try
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End If

                'sms
                If Common.g_SMSApplicable = "" Then
                    Common.Load_SMS_Policy()
                End If
                If Common.g_SMSApplicable = "Y" Then
                    If Common.g_isAllSMS = "Y" Then
                        Try
                            If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then
                                Dim SmsContent As String = ""
                                If Common.g_DeviceWiseInOut = "Y" Then
                                    If IN_OUT = "I" Then
                                        SmsContent = Common.g_InContent1 & " " & adtLog.ToString("yyyy-MM-dd HH:mm") & " " & Common.g_InContent2
                                    ElseIf IN_OUT = "O" Then
                                        SmsContent = Common.g_OutContent1 & " " & adtLog.ToString("yyyy-MM-dd HH:mm") & " " & Common.g_OutContent2
                                    End If
                                Else
                                    SmsContent = Common.g_AllContent1 & " " & adtLog.ToString("yyyy-MM-dd HH:mm") & " " & Common.g_AllContent2
                                End If
                                SmsContent = Name & " " & SmsContent
                                Cn.sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                End If
                'sms end

            Catch ex As Exception

            End Try

            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
        Else
            Name = "Not registered"
            Paycode = ""
        End If

        Cn.Remove_Duplicate_Punches(adtLog, Paycode, EId)
        Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PAYCODE = '" & Paycode & "'"
        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSqltmp, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSqltmp, con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            'Dim mindatetmp As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("OFFICEPUNCH")).ToString("yyyy-MM-dd")
            If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                Cn.Process_AllRTC(adtLog.AddDays(-1), Now.Date, Paycode, Paycode.ToLower, EId)
            Else
                Cn.Process_AllnonRTC(adtLog, Now.Date, Paycode, Paycode, EId)
                If Common.EmpGrpArr(EId).SHIFTTYPE = "M" Then
                    Cn.Process_AllnonRTCMulti(adtLog, Now.Date, Paycode, Paycode, EId)
                End If
            End If
        End If


        'Try
        '    GridView1.AddNewRow()
        '    Dim rowHandle As Integer = GridView1.GetRowHandle(GridView1.DataRowCount)
        '    If GridView1.IsNewItemRow(rowHandle) Then
        '        GridView1.SetRowCellValue(rowHandle, GridView1.Columns(0), GridView1.RowCount)
        '        GridView1.SetRowCellValue(rowHandle, GridView1.Columns(1), aEnrollNumber.ToString("000000000000"))
        '        GridView1.SetRowCellValue(rowHandle, GridView1.Columns(2), Paycode)
        '        GridView1.SetRowCellValue(rowHandle, GridView1.Columns(3), Name)
        '        GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), VerifyMode)
        '        GridView1.SetRowCellValue(rowHandle, GridView1.Columns(5), PunchDate)
        '        GridView1.SetRowCellValue(rowHandle, GridView1.Columns(6), PunchTime)
        '        GridView1.SetRowCellValue(rowHandle, GridView1.Columns(7), astrRemoteIP)
        '        GridView1.SetRowCellValue(rowHandle, GridView1.Columns(8), anDeviceID)
        '    End If
        '    lblTotal.Text = GridView1.RowCount '("Total : " & lvwLogList.Items.Count)
        '    If System.IO.File.Exists("./EmpImages/" & Paycode & ".jpg") Then
        '        'VisitorPictureEdit.Image = Image.FromFile("./EmpImages/" & TextEdit2.Text & ".jpg")
        '        PictureEdit.LoadAsync("./EmpImages/" & Paycode & ".jpg")
        '    Else
        '        PictureEdit.Image = Nothing
        '    End If
        '    If GridView1.RowCount > 500 Then
        '        lblTotal.Text = "Total : 0"
        '        Me.SetupLogListview()
        '    End If

        'Catch ex As Exception

        'End Try

    End Sub
    Private Function ShowGeneralLogDataToGrid(ByVal anCount As Integer, ByVal astrLogId As String, ByVal astrEnrollNumber As String, ByVal astrVerifyMode As String, ByVal astrInOutMode As String, ByVal adtLog As String, ByVal abDrawFlag As Boolean, ByVal astrRemoteIP As String, ByVal anRemotePort As Long, ByVal astrDeviceID As String) As Boolean
        Dim vItem As ListViewItem
        Dim strTmp As String = Nothing
        Dim vDoorMode As Integer = 0
        Dim vIoMode As Integer = 0
        vItem = New ListViewItem
        vItem.Text = Convert.ToString((lvwLogList.Items.Count + 1))
        vItem.SubItems.Add(astrEnrollNumber)
        vItem.SubItems.Add(astrVerifyMode)
        vItem.SubItems.Add(astrInOutMode)
        'strTmp = Convert.ToString(adtLog);
        vItem.SubItems.Add(adtLog)
        vItem.SubItems.Add(astrRemoteIP)
        vItem.SubItems.Add(Convert.ToString(anRemotePort))
        vItem.SubItems.Add(astrDeviceID)
        vItem.SubItems.Add(astrLogId)
        lvwLogList.Items.Add(vItem)
        If (lvwLogList.Items.Count > 12) Then
            lvwLogList.EnsureVisible((lvwLogList.Items.Count - 12))
        End If
        Dim ds As DataSet = New DataSet
        Dim ssql As String = "SELECT ID_NO from tblMachine where LOCATION='" & astrRemoteIP & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        Dim anDeviceID As Long
        If ds.Tables(0).Rows.Count > 0 Then
            anDeviceID = ds.Tables(0).Rows(0).Item("ID_NO").ToString.Trim
        Else
            anDeviceID = astrDeviceID
        End If
        If IsNumeric(astrEnrollNumber) Then
            astrEnrollNumber = Convert.ToDouble(astrEnrollNumber).ToString("000000000000")
        End If
        GridView1.AddNewRow()
        Dim Paycode As String = ""
        Dim rowHandle As Integer = GridView1.GetRowHandle(GridView1.DataRowCount)
        If GridView1.IsNewItemRow(rowHandle) Then
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(0), vItem.Text)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(1), astrEnrollNumber)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(2), Paycode)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(3), Name)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), VerifyMode)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(5), PunchDate)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(6), PunchTime)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(7), astrRemoteIP)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(8), anDeviceID)
        End If

        lblTotal.Text = ("Total : " & lvwLogList.Items.Count)
        If System.IO.File.Exists(My.Application.Info.DirectoryPath & "\EmpImages\" & Paycode & ".jpg") Then
            'VisitorPictureEdit.Image = Image.FromFile("./EmpImages/" & TextEdit2.Text & ".jpg")
            PictureEdit.LoadAsync(My.Application.Info.DirectoryPath & "\EmpImages\" & Paycode & ".jpg")
        Else
            PictureEdit.Image = Nothing
        End If
        If lvwLogList.Items.Count > 500 Then
            lblTotal.Text = "Total : 0"
            Me.SetupLogListview()
        End If
        Return True
    End Function
    Private Sub AxRealSvrOcxTcp1_OnReceiveGLogDataExtend(ByVal sender As Object, ByVal e As AxRealSvrOcxTcpLib._DRealSvrOcxTcpEvents_OnReceiveGLogDataExtendEvent) Handles AxRealSvrOcxTcp1.OnReceiveGLogDataExtend
        ''Dim userid As String '= Convert.ToInt32(e.anSEnrollNumber.ToString("X8"), 16)
        ''If e.anSEnrollNumber < 0 Then
        ''    Dim x As UInt64 = Convert.ToInt64(e.anSEnrollNumber) ' e.anSEnrollNumber.ToString.Substring(1, 10)
        ''    Dim userid1 As double = CDbl(Val("&H" & Hex(x))) ' Convert.ToInt32(x.ToString("X8"), 16) ' Convert.ToInt64(x.ToString, 16)
        ''    'Dim userid1 As UInt32 = CType(x, UInt32)
        ''    userid = userid1
        ''Else
        ''    userid = e.anSEnrollNumber
        ''End If
        'Dim x As String = " Glog " & vbCrLf & "e.anSEnrollNumber" & vbCrLf & " e.anVerifyMode" & vbCrLf & " e.anInOutMode" & vbCrLf & " e.anLogDate" & vbCrLf & " True" & vbCrLf & " e.astrDeviceIP" & vbCrLf & " e.anDevicePort" & vbCrLf & " e.anDeviceID" & vbCrLf & " e.astrSerialNo" & vbCrLf & " e.astrRootIP"
        'MsgBox(x)
        Me.funcShowGeneralLogDataToGrid(e.anSEnrollNumber, e.anVerifyMode, e.anInOutMode, e.anLogDate, True, e.astrDeviceIP, e.anDevicePort, e.anDeviceID, e.astrSerialNo, e.astrRootIP, "Bio")
    End Sub
    Private Sub AxRealSvrOcxTcp1_OnReceiveGLogTextOnDoorOpen(ByVal sender As Object, ByVal e As AxRealSvrOcxTcpLib._DRealSvrOcxTcpEvents_OnReceiveGLogTextOnDoorOpenEvent) Handles AxRealSvrOcxTcp1.OnReceiveGLogTextOnDoorOpen
        TextEdit1.Text = e.astrLogText

        Dim RecText As String = e.astrLogText ' TextEdit1.Text
        Dim s As String = RecText
        Dim z As Char = s((s.Length - 1))
        If z = """" Then
            RecText = s.Substring(0, (s.Length - 1))
        End If

        'MsgBox(e.astrLogText)
        Dim vstrLogId As String
        Dim vstrEnrollNumber As String
        Dim vstrDeviceID As String
        Dim vstrVerifyMode As String
        Dim vstrInOutMode As String
        Dim vstrDate As String
        Dim vDate As String
        Dim vstrResponse As String
        Dim vstrEmergency As String
        Dim vstrIsSupportStringID As String
        Dim jobjTest As JObject = JObject.Parse(RecText)
        vstrLogId = jobjTest("log_id").ToString()
        vstrEnrollNumber = jobjTest("user_id").ToString()
        vstrDeviceID = jobjTest("fk_device_id").ToString()
        vstrVerifyMode = jobjTest("verify_mode").ToString()
        If vstrVerifyMode = "FP" Then
            vstrVerifyMode = 1
        ElseIf vstrVerifyMode.ToLower = "card" Then
            vstrVerifyMode = 3
        ElseIf vstrVerifyMode.ToLower = "face" Then
            vstrVerifyMode = 20
        End If
        vstrInOutMode = jobjTest("io_mode").ToString()
        vstrDate = jobjTest("io_time").ToString()
        vDate = vstrDate.Substring(0, 4) & "-" & vstrDate.Substring(4, 2) & "-" & vstrDate.Substring(6, 2) & " " & vstrDate.Substring(8, 2) & ":" & vstrDate.Substring(10, 2) & ":" & vstrDate.Substring(12, 2)
        fnCount = fnCount + 1
        'ShowGeneralLogDataToGrid(fnCount, vstrLogId, vstrEnrollNumber, vstrVerifyMode, vstrInOutMode, vDate, True, e.astrClientIP, e.anClientPort, vstrDeviceID)
        'txtStatus.Text = "Last Log :{ user_id: " & vstrEnrollNumber & ", io_time :" + vDate & "}"
        showLogImage(e.astrLogImage)
        Dim jobjRespond As JObject = New JObject()
        jobjRespond.Add("log_id", vstrLogId)
        jobjRespond.Add("result", "OK")
        'MsgBox(vstrEnrollNumber & vbCrLf & vstrVerifyMode & vbCrLf & Convert.ToDateTime(vDate))
        'Try
        If vstrVerifyMode = "IDCARD" Then
            vstrVerifyMode = 3
        End If
        Me.funcShowGeneralLogDataToGrid(vstrEnrollNumber, vstrVerifyMode, 1, Convert.ToDateTime(vDate), True, e.astrClientIP, 1, 1, "", "", "Bio")
        'Me.funcShowGeneralLogDataToGrid(vstrEnrollNumber, vstrVerifyMode, 1, Convert.ToDateTime(vDate), True, "", "", "", "", "")
        'Catch ex As Exception

        'End Try
        'Try
        '    vstrEmergency = jobjTest("emergency").ToString()
        '    vstrIsSupportStringID = jobjTest("is_support_string_id").ToString()

        '    If vstrEmergency.Equals("yes") Then
        '        If txtActiveId.Text <> "" AndAlso txtActiveId.Text = vstrEnrollNumber Then
        '            jobjRespond.Add("mode", "open")
        '        Else
        '            jobjRespond.Add("mode", "nothing")
        '        End If
        '    Else
        '        jobjRespond.Add("mode", "nothing")
        '    End If

        'Catch ex As Exception
        '    jobjRespond.Add("mode", "nothing")
        'End Try

        vstrResponse = jobjRespond.ToString()
        AxRealSvrOcxTcp1.SendRtLogResponseV3(e.astrClientIP, e.anClientPort, vstrResponse)
    End Sub
    Private Sub AxRealSvrOcxTcp1_OnReceiveGLogTextAndImage(ByVal sender As Object, ByVal e As AxRealSvrOcxTcpLib._DRealSvrOcxTcpEvents_OnReceiveGLogTextAndImageEvent) Handles AxRealSvrOcxTcp1.OnReceiveGLogTextAndImage
        'MsgBox("test")
    End Sub
    'end for real time listning for BIO




    'Zk listning
    ' Start TCP listening process
    'Private Sub ListenClient()
    '    Try
    '        'If (txtIP.Text.Trim = String.Empty) Then
    '        '    Me.tcp = New TcpListener(Convert.ToInt32(txtPort.Text))
    '        'Else
    '        Me.tcp = New TcpListener(IPAddress.Parse(sysIp), ZKPort)
    '        'End If
    '        Me.tcp.Start()
    '        'Me.WriteText(("Listening on: "  + (Me.tcp.LocalEndpoint + "" & vbCrLf)), Color.DarkRed)
    '        listening = True
    '        While listening
    '            Dim mySocket As Socket = Nothing
    '            ' Block until a device client has connected to the server 
    '            Try
    '                mySocket = Me.tcp.AcceptSocket
    '                Thread.Sleep(500)
    '                Dim bReceive() As Byte = New Byte(((1024 * (1024 * 2))) - 1) {}
    '                Dim i As Integer = mySocket.Receive(bReceive)
    '                Dim dataStr As String = Encoding.ASCII.GetString(bReceive)
    '                'MsgBox(dataStr)
    '                Dim databyte() As Byte = New Byte((i) - 1) {}
    '                Dim j As Integer = 0
    '                'Do While (j < i)
    '                For j = 0 To i - 1
    '                    databyte(j) = bReceive(j)
    '                Next                    
    '                Analysis(databyte, mySocket)
    '            Catch
    '            End Try
    '        End While
    '        Me.tcp.Stop()
    '    Catch
    '        listening = False
    '        'XtraMessageBox.Show(ulf, "<size=10>Invalid Port</size>", "<size=9>Error</size>")
    '    End Try
    'End Sub
    Public Sub attlog(ByVal sBuffer As String, ByVal machine As String)
        Dim conS As SqlConnection '= New SqlConnection(Common.ConnectionString)
        Dim conS1 As OleDbConnection ' = New OleDbConnection(Common.ConnectionString)
        If Common.servername = "Access" Then
            conS1 = New OleDbConnection(Common.ConnectionString)
        Else
            conS = New SqlConnection(Common.ConnectionString)
        End If
        Dim machinestamp As String = sBuffer.Substring((sBuffer.IndexOf("Stamp=") + 6))
        Dim Stamp As String = ""
        Me.getnumber(machinestamp, Stamp)
        ' Get TimeStamp 
        'ATTLOGStamp = Stamp

        'nitin
        Dim sSql As String = "select * from ZKATTLOGStamp where SN='" & machine & "'"
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, conS1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, conS)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            sSql = "Update ZKATTLOGStamp set ATTLOGStamp = '" & Stamp & "' where SN='" & machine & "'"
        Else
            sSql = "insert into ZKATTLOGStamp (SN,ATTLOGStamp) values ('" & machine & "','" & Stamp & "')"
        End If
        If Common.servername = "Access" Then
            If conS1.State <> ConnectionState.Open Then
                conS1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, conS1)
            cmd1.ExecuteNonQuery()
            If conS1.State <> ConnectionState.Closed Then
                conS1.Close()
            End If
        Else
            If conS.State <> ConnectionState.Open Then
                conS.Open()
            End If
            cmd = New SqlCommand(sSql, conS)
            cmd.ExecuteNonQuery()
            If conS.State <> ConnectionState.Closed Then
                conS.Close()
            End If
        End If
        'nitin end

        Dim attindex As Integer = sBuffer.IndexOf("" & vbCrLf & vbCrLf, 1)  ' sBuffer.IndexOf("\r\n\r\n", 1) 
        Dim attstr As String = sBuffer.Substring((attindex + 4))

        attlogprocess(attstr, machine)

        'Dim sBufferTmp As String() = attstr.Split(vbLf)
        'LabelControl1.Text = machine & "  " & sBufferTmp.Length & "  " & Now.ToString("yyyy-MM-dd HH:mm:ss")
        'Application.DoEvents()
    End Sub
    Public Sub attlogprocess(ByVal sBuffer As String, machine As String)
        'Try
        '    Dim attindex As Integer = sBuffer.IndexOf("" & vbLf, 1)
        '    Dim attstr As String = ""
        '    If (attindex > 0) Then
        '        attstr = sBuffer.Substring(0, attindex)
        '    End If
        '    saveattlog(attstr, machine)
        '    Dim endatt As String = sBuffer.Substring((attindex + 1))
        '    If (endatt <> "") Then
        '        attlogprocess(endatt, machine)
        '    End If
        'Catch
        'End Try

        Try
            Dim attstr As String = ""
            Dim sBufferTmp As String() = sBuffer.Split(vbLf)
            'LabelControl1.Text = machine & "  " & sBufferTmp.Length - 1
            For i As Integer = 0 To sBufferTmp.Length - 1
                saveattlog(sBufferTmp(i), machine)
            Next
        Catch ex As Exception

        End Try
    End Sub
    Public Sub saveattlog(ByVal attlog As String, machine As String)
        If attlog.Trim = "" Then
            Exit Sub
        End If
        Dim conS As SqlConnection '= New SqlConnection(Common.ConnectionString)
        Dim conS1 As OleDbConnection ' = New OleDbConnection(Common.ConnectionString)
        If Common.servername = "Access" Then
            conS1 = New OleDbConnection(Common.ConnectionString)
        Else
            conS = New SqlConnection(Common.ConnectionString)
        End If

        Dim EnrollNumber As String = attlog.Substring(0, attlog.IndexOf("" & vbTab, 1))
        If EnrollNumber = "" Then
            Return
        End If
        'EnrollNumber = string.Format("{00000000}");
        'EnrollNumber = EnrollNumber.PadLeft(6, Microsoft.VisualBasic.ChrW(48))
        Dim still1 As String = attlog.Substring((attlog.IndexOf("" & vbTab, 1) + 1))
        Dim atttime As String = still1.Substring(0, still1.IndexOf("" & vbTab, 1))
        Dim still2 As String = still1.Substring((still1.IndexOf("" & vbTab, 1) + 1))
        Dim status As String = still2.Substring(0, still2.IndexOf("" & vbTab, 1))
        Dim still3 As String = still2.Substring((still2.IndexOf("" & vbTab, 1) + 1))
        ' WorkCode and DeviceID only exists on Firmware Professional version
        Dim verifymode As String = ""
        Try
            ' Verify Mode
            verifymode = still3.Substring(0, still3.IndexOf("" & vbTab, 1))
        Catch
            verifymode = still3
        End Try
        'If verifymode = 1 Then
        '    verifymode = "Fp"
        'ElseIf verifymode = 2 Then
        '    verifymode = "Card"
        'End If

        If verifymode = "0" Then
            verifymode = "PIN"
        ElseIf verifymode = "1" Then
            verifymode = "Fp"
        ElseIf verifymode = "2" Then
            verifymode = "Card"
        ElseIf verifymode = "3" Then
            verifymode = "PIN & PW"
        ElseIf verifymode = "4" Then
            verifymode = "PIN & FP"
        ElseIf verifymode = "5" Then
            verifymode = "FP & (PIN & PW)"
        ElseIf verifymode = "6" Then
            verifymode = "FP & Card"
        ElseIf verifymode = "7" Then
            verifymode = "(PIN & PW) & Card"
        ElseIf verifymode = "8" Then
            verifymode = "FP & PW & Card"
        ElseIf verifymode = "9" Then
            verifymode = "(PIN & PW) & FP"
        ElseIf verifymode = "101" Then
            verifymode = "Slave Device"
        ElseIf verifymode = "15" Then
            verifymode = "Face"
        End If


        Dim still4 As String = ""
        Dim workcode As String = ""
        Try
            still4 = still3.Substring((still3.IndexOf("" & vbTab, 1) + 1))
            workcode = still4.Substring(0, still4.IndexOf("" & vbTab, 1)).Trim
        Catch ex As Exception

        End Try
        If Not IsNumeric(workcode.Trim) Then
            workcode = "0"
        End If


        Dim sSql As String
        '''//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        'Indian Code Master
        PunchTime = Convert.ToDateTime(atttime.ToString)
        Dim EnrNum As String
        If IsNumeric(EnrollNumber) Then
            EnrNum = Convert.ToInt64(EnrollNumber).ToString("000000000000")
        Else
            EnrNum = EnrollNumber
        End If



        Dim deviceId As String = ""
        Dim IN_OUT As String = ""
        Dim LEDIP As String = ""
        Dim LocName As String = "" 'IOCL
        Dim LocCode As String = "" 'IOCL

        sSql = "SELECT ID_NO, IN_OUT, LEDIP from tblMachine where MAC_ADDRESS='" & machine & "'"
        'for IOCL
        sSql = "SELECT ID_NO, IN_OUT, branch, BRANCHCODE,LEDIP from tblMachine, tblbranch where MAC_ADDRESS='" & machine & "' and tblMachine.branch=tblbranch.BRANCHNAME"
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, conS1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, conS)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            deviceId = ds.Tables(0).Rows(0).Item("ID_NO").ToString.Trim
            IN_OUT = ds.Tables(0).Rows(0).Item("IN_OUT").ToString.Trim
            LEDIP = ds.Tables(0).Rows(0).Item("LEDIP").ToString.Trim
            LocCode = ds.Tables(0).Rows(0).Item("BRANCHCODE").ToString.Trim
            LocName = ds.Tables(0).Rows(0).Item("branch").ToString.Trim
        End If


        ''only for one customer
        'Try
        '    sSql = "insert into Rawdata (CARDNO, OFFICEPUNCH,Id_No,Inout) values('" & EnrNum & "','" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd HH:mm:ss") & "','" & deviceId & "','" & IN_OUT & "')"
        '    If Common.servername = "Access" Then
        '        If Common.con1.State <> ConnectionState.Open Then
        '            Common.con1.Open()
        '        End If
        '        cmd1 = New OleDbCommand(sSql, Common.con1)
        '        cmd1.ExecuteNonQuery()
        '        If Common.con1.State <> ConnectionState.Closed Then
        '            Common.con1.Close()
        '        End If
        '    Else
        '        If conS.State <> ConnectionState.Open Then
        '            conS.Open()
        '        End If
        '        cmd = New SqlCommand(sSql, conS)
        '        cmd.ExecuteNonQuery()
        '        If conS.State <> ConnectionState.Closed Then
        '            conS.Close()
        '        End If
        '    End If
        'Catch ex As Exception

        'End Try
        ''end for one customer

        If IN_OUT = "B" Then
            Dim ds2 As DataSet = New DataSet
            Dim sSql1 As String '= "select count (CARDNO) from MachineRawPunch where CARDNO = '" & EnrNum & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd") & "'"
            Dim adapAT As OleDbDataAdapter
            Dim adapT As SqlDataAdapter
            If Common.servername = "Access" Then
                sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & EnrNum & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd") & "'"
                adapAT = New OleDbDataAdapter(sSql1, conS1)
                adapAT.Fill(ds2)
            Else
                sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & EnrNum & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd") & "'"
                adapT = New SqlDataAdapter(sSql1, conS)
                adapT.Fill(ds2)
            End If

            If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                IN_OUT = "I"
            Else
                If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                    IN_OUT = "I"
                Else
                    IN_OUT = "O"
                End If
            End If
        ElseIf IN_OUT = "D" Then
            If status = "0" Then
                IN_OUT = "I"
            ElseIf status = "1" Then
                IN_OUT = "O"
            End If
        End If

        sSql = "select * from tblemployee, EmployeeGroup where PRESENTCARDNO='" & EnrNum & "' and EmployeeGroup.GroupId=TblEmployee.EmployeeGroupId"
        'sSql = "select * from tblemployee where PRESENTCARDNO='" & EnrNum & "'"
        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, conS1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, conS)
            adap.Fill(ds)
        End If
        Dim Name As String = ""
        Dim Paycode As String = ""
        Dim EId As Integer
        If ds.Tables(0).Rows.Count > 0 Then
            Name = ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim
            Paycode = ds.Tables(0).Rows(0).Item("Paycode").ToString.Trim
            EId = ds.Tables(0).Rows(0).Item("Id")
            LocCode = ds.Tables(0).Rows(0).Item("COMPANYCODE").ToString.Trim
            ''IOCL
            'Try
            '    Common.JSONCallIOCL(EnrollNumber.ToString, Convert.ToDateTime(atttime.ToString), deviceId, LocCode, LocName, IN_OUT, deviceId)
            'Catch ex As Exception
            '    TextEdit1.Text = ex.Message.ToString.Trim & " For " & EnrollNumber.ToString & ", " & PunchDate
            'End Try
            '' end IOCL

            sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[DownloadedTime],[VerifyMode]) values('" & EnrNum & "','" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','" & workcode & "','" & deviceId & "','" & IN_OUT & "','" & Paycode & "','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','" & verifymode & "' ) "
            Dim sSql1 As String = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[DownloadedTime],[VerifyMode]) values('" & EnrNum & "','" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','" & workcode & "','" & deviceId & "','" & IN_OUT & "','" & Paycode & "','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','" & verifymode & "' ) "
            Try
                If Common.servername = "Access" Then
                    If conS1.State <> ConnectionState.Open Then
                        conS1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, conS1)
                    cmd1.ExecuteNonQuery()
                    Try
                        cmd1 = New OleDbCommand(sSql1, conS1)
                        cmd1.ExecuteNonQuery()
                    Catch ex As Exception

                    End Try
                    If conS1.State <> ConnectionState.Closed Then
                        conS1.Close()
                    End If
                Else
                    If conS.State <> ConnectionState.Open Then
                        conS.Open()
                    End If
                    cmd = New SqlCommand(sSql, conS)
                    cmd.ExecuteNonQuery()
                    Try
                        cmd = New SqlCommand(sSql1, conS)
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception

                    End Try
                    If conS.State <> ConnectionState.Closed Then
                        conS.Close()
                    End If
                End If


                If Common.IsParallel = "Y" Then
                    Dim parallelThread As Thread = New Thread(Sub() Common.parallelInsert(EnrollNumber, Paycode, Convert.ToDateTime(atttime.ToString), IN_OUT, "N", Name, deviceId))
                    parallelThread.Start()
                    parallelThread.IsBackground = True
                End If

                'sms
                If Common.g_SMSApplicable = "" Then
                    Common.Load_SMS_Policy()
                End If
                If Common.g_SMSApplicable = "Y" Then
                    If Common.g_isAllSMS = "Y" Then
                        Try
                            If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then

                                Dim SmsContent As String = ""
                                If Common.g_DeviceWiseInOut = "Y" Then
                                    If IN_OUT = "I" Then
                                        SmsContent = Common.g_InContent1 & " " & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd HH:mm") & " " & Common.g_InContent2
                                    ElseIf IN_OUT = "O" Then
                                        SmsContent = Common.g_OutContent1 & " " & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd HH:mm") & " " & Common.g_OutContent2
                                    End If
                                Else
                                    SmsContent = Common.g_AllContent1 & " " & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd HH:mm") & " " & Common.g_AllContent2
                                End If
                                SmsContent = Name & " " & SmsContent
                                Cn.sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                End If
                'sms end
            Catch ex As Exception

            End Try

#Region "IDTECH"
            'Try 'IDTECH
            '    '' Json
            '    Try
            '        Dim CardType As String = "", CardSRNNO As String = "", PunchType As String = "", Category As String = "", RefNo As String = "", OrgID As String = "", PresentCardNo As String = ""
            '        Dim MachineID As Int64 = 1

            '        Dim FinalData As JObject = New JObject
            '        Dim httpWebRequest = CType(WebRequest.Create("https://in.nexuba.com/S/E2Pro_ASL_Inbound.php"), HttpWebRequest)
            '        httpWebRequest.ContentType = "application/json"
            '        httpWebRequest.Method = "POST"
            '        ServicePointManager.Expect100Continue = True
            '        ServicePointManager.SecurityProtocol = CType(3072, SecurityProtocolType)
            '        Dim jsons As String
            '        Dim PunchDate As DateTime = Convert.ToDateTime(atttime.ToString())
            '        Dim jStrong = "{""type"": 1,""data"":{""org_id"":""1046"",""badge_id"":""" & EnrollNumber & """,""device_id"":""" & machine & """,""swipetime"":""" & PunchDate.ToString("yyyy-MM-dd HH:mm:ss") & """,""security token"":""xyz13aso""}}"
            '        jsons = JsonConvert.ToString(jStrong)
            '        Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
            '            ''EnrollNumber = EnrollNumber.Trim
            '            'FinalData.Add("type", "1")
            '            'FinalData.Add("org_id", "1046")
            '            'FinalData.Add("badge_id", EnrollNumber.Trim)
            '            'FinalData.Add("device_id", deviceSerialNumber)
            '            'FinalData.Add("swipetime", PunchDate.ToString("yyyy-MM-dd HH:mm:ss"))
            '            'FinalData.Add("security token", "xyz13aso")
            '            ' jsons = FinalData.ToString(Formatting.None)
            '            streamWriter.Write(jStrong)
            '            streamWriter.Flush()
            '            streamWriter.Close()
            '        End Using
            '        Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
            '        Using streamReader = New StreamReader(httpResponse.GetResponseStream())
            '            Dim result = streamReader.ReadToEnd()
            '            Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
            '            Dim sw As StreamWriter = New StreamWriter(fs)
            '            ' Dim sw As StreamWriter = New StreamWriter(fs)
            '            sw.BaseStream.Seek(0, SeekOrigin.End)
            '            sw.WriteLine(vbCrLf & jStrong & ", " & result & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
            '            sw.Flush()
            '            sw.Close()
            '        End Using
            '    Catch ex As Exception
            '        'TraceService(ex.Message, "JSON")
            '        Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
            '        Dim sw As StreamWriter = New StreamWriter(fs)
            '        ' Dim sw As StreamWriter = New StreamWriter(fs)
            '        sw.BaseStream.Seek(0, SeekOrigin.End)
            '        sw.WriteLine(vbCrLf & ex.Message & ", JSON::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
            '        sw.Flush()
            '        sw.Close()
            '    End Try
            '    ''end jSON
            'Catch ex As Exception

            'End Try
#End Region
            Dim del As invokeDelegate = Function()
                                            SetText(EnrNum, Paycode, Name, verifymode, Convert.ToDateTime(atttime.ToString), machine, deviceId, LEDIP, EId)
                                        End Function
            Invoke(del)
        Else
            Dim del As invokeDelegate = Function()
                                            SetText(EnrNum, "", "Not registered", verifymode, Convert.ToDateTime(atttime.ToString), machine, deviceId, LEDIP, EId)
                                        End Function
            Invoke(del)
        End If
        'Return
    End Sub
    'to access grid from tread
    Public Delegate Sub SetTextCallback(EnrNum As String, Paycode As String, name As String, verifymode As String, PunchDate As DateTime, machine As String, deviceId As String, LEDIP As String, EId As Integer)
    'ZK show in grid
    Private Sub SetText(EnrNum As String, Paycode As String, name As String, verifymode As String, PunchDate As DateTime, machine As String, deviceId As String, LEDIP As String, EId As Integer)
        ' InvokeRequired required compares the thread ID of the
        ' calling thread to the thread ID of the creating thread.
        ' If these threads are different, it returns true.
        'If Me.GridControl1.InvokeRequired Then
        '    Dim d As SetTextCallback = New SetTextCallback(AddressOf Me.SetText)
        '    Me.Invoke(d, New Object() {EnrNum, Paycode, name, verifymode, PunchDate, machine, deviceId, LEDIP, EId})
        'Else
        'Me.textBox1.Text = Text
        Dim conS As SqlConnection '= New SqlConnection(Common.ConnectionString)
        Dim conS1 As OleDbConnection ' = New OleDbConnection(Common.ConnectionString)
        If Common.servername = "Access" Then
            conS1 = New OleDbConnection(Common.ConnectionString)
        Else
            conS = New SqlConnection(Common.ConnectionString)
        End If
        GridView1.AddNewRow()
        Dim rowHandle As Integer = GridView1.GetRowHandle(GridView1.DataRowCount)
        If GridView1.IsNewItemRow(rowHandle) Then
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(0), GridView1.RowCount)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(1), EnrNum)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(2), Paycode)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(3), name)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), verifymode)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(5), PunchDate.ToString("dd MMM yyyy"))
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(6), PunchDate.ToString("HH:mm"))
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(7), machine)
            GridView1.SetRowCellValue(rowHandle, GridView1.Columns(8), deviceId)
        End If
        If System.IO.File.Exists("./EmpImages/" & Paycode & ".jpg") Then
            'VisitorPictureEdit.Image = Image.FromFile("./EmpImages/" & TextEdit2.Text & ".jpg")
            PictureEdit.LoadAsync("./EmpImages/" & Paycode & ".jpg")
        Else
            PictureEdit.Image = Nothing
        End If
        lblTotal.Text = ("Total : " & GridView1.RowCount)
        If GridView1.RowCount > 500 Then
            lblTotal.Text = "Total : 0"
            Me.SetupLogListview()
        End If

        If Paycode <> "" Then
            Dim sSqltmp As String = "select TblEmployee.PAYCODE,tblEmployee.VehicleNo, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PAYCODE = '" & Paycode & "'"
            Dim ds As DataSet = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSqltmp, conS1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSqltmp, conS)
                adap.Fill(ds)
            End If
            Dim vehicleNo As String
            If ds.Tables(0).Rows.Count > 0 Then
                Cn.Remove_Duplicate_Punches(PunchDate, Paycode, EId)
                vehicleNo = ds.Tables(0).Rows(0).Item("VehicleNo").ToString
                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                    Cn.Process_AllRTC(PunchDate.AddDays(-1), Now.Date, Paycode, Paycode, EId)
                Else
                    Cn.Process_AllnonRTC(PunchDate, Now.Date, Paycode, Paycode, EId)
                    If Common.EmpGrpArr(EId).SHIFTTYPE = "M" Then
                        Cn.Process_AllnonRTCMulti(PunchDate, Now.Date, Paycode, Paycode, EId)
                    End If
                End If
                If LEDIP <> "" Then
                    TmpLED = LEDIP
                    LEDPeDIkhana(vehicleNo, LEDIP)
                End If
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
            End If
        End If
        'End If
    End Sub
    Public Sub oplogprocess(ByVal sBuffer As String)
        Try
            Dim opindex As Integer = sBuffer.IndexOf("" & vbLf, 1)
            Dim opstr As String = ""
            If (opindex > 0) Then
                opstr = sBuffer.Substring(0, opindex)
            End If

            saveoplog(opstr)
            Dim endop As String = sBuffer.Substring((opindex + 1))
            If (endop <> "") Then
                oplogprocess(endop)
            End If
        Catch
        End Try
    End Sub
    ' Display AttLog in customized format    ZK
    Public Sub saveoplog(ByVal oplog As String)
        If (oplog.IndexOf("PLOG", 1) <= 0) Then
            Return
        End If

        Dim Optype As String = oplog.Substring(0, oplog.IndexOf("" & vbTab, 1))
        Dim Optype_tem As String = Optype.Substring(6)
        Dim stillop1 As String = oplog.Substring((oplog.IndexOf("" & vbTab, 1) + 1))
        Dim admin As String = stillop1.Substring(0, stillop1.IndexOf("" & vbTab, 1))
        Dim stillop2 As String = stillop1.Substring((stillop1.IndexOf("" & vbTab, 1) + 1))
        Dim optime As String = stillop2.Substring(0, stillop2.IndexOf("" & vbTab, 1))
        Dim stillop3 As String = stillop2.Substring((stillop2.IndexOf("" & vbTab, 1) + 1))
        Dim opnum1 As String = stillop3.Substring(0, stillop3.IndexOf("" & vbTab, 1))
        Dim stillop4 As String = stillop3.Substring((stillop3.IndexOf("" & vbTab, 1) + 1))
        Dim opnum2 As String = stillop4.Substring(0, stillop4.IndexOf("" & vbTab, 1))
        Dim stillop5 As String = stillop4.Substring((stillop4.IndexOf("" & vbTab, 1) + 1))
        Dim opnum3 As String = stillop5.Substring(0, stillop5.IndexOf("" & vbTab, 1))
        Dim stillop6 As String = stillop5.Substring((stillop5.IndexOf("" & vbTab, 1) + 1))
        Dim opnum4 As String = stillop6
        Dim Optype_tem_de As String = Optype_tem.TrimEnd
        Dim Optype_de As String = ""
        Select Case (Optype_tem_de)
            Case "0"
                Optype_de = "Power ON Device"
            Case "1"
                Optype_de = "Power OFF Device"
            Case "2"
                Optype_de = "Fail Verification"
            Case "3"
                Optype_de = "Generate an Alarm"
            Case "4"
                Optype_de = "Enter Menu"
            Case "5"
                Optype_de = "Modify Configuration"
            Case "6"
                Optype_de = "Enroll FingerPrint"
            Case "7"
                Optype_de = "Enroll Password"
            Case "8"
                Optype_de = "Enroll HID Card"
            Case "9"
                Optype_de = "Delete one User"
            Case "10"
                Optype_de = "Delete FingerPrint"
            Case "11"
                Optype_de = "Delete Password"
            Case "12"
                Optype_de = "Delete RF Card"
            Case "13"
                Optype_de = "Purge Data"
            Case "14"
                Optype_de = "Create MF Card"
            Case "15"
                Optype_de = "Enroll MF Card"
            Case "16"
                Optype_de = "Register MF Card"
            Case "17"
                Optype_de = "Delete MF Card"
            Case "18"
                Optype_de = "Clean MF Card"
            Case "19"
                Optype_de = "Copy Data to Card"
            Case "20"
                Optype_de = "Copy C. D. to Device"
            Case "21"
                Optype_de = "Set New Time"
            Case "22"
                Optype_de = "Factory Setting"
            Case "23"
                Optype_de = "Delete Att. Records"
            Case "24"
                Optype_de = "Clean Admin. Privilege"
            Case "25"
                Optype_de = "Modify Group Settings"
            Case "26"
                Optype_de = "Modify User AC Settings"
            Case "27"
                Optype_de = "Modify Time Zone"
            Case "28"
                Optype_de = "Modify Unlock Settings"
            Case "29"
                Optype_de = "Open Door Lock"
            Case "30"
                Optype_de = "Enroll one User"
            Case "31"
                Optype_de = "Modify FP Template"
            Case "32"
                Optype_de = "Duress Alarm"
            Case "68"
                Optype_de = "User pic"
        End Select

        '' Display oplog in customized format
        'Me.WriteOplog(Optype_de, admin, optime, opnum1, opnum2, opnum3, opnum4)
        'If (tabOptions.SelectedIndex = 1) Then
        '    lblRecords.Text = "OpLogs Count:"
        '    lblCount.Text = lvOpLog.Items.Count.ToString
        'End If

    End Sub
    Public Sub enfplogprocess(ByVal sBuffer As String)
        Try
            Dim enfpindex As Integer = sBuffer.IndexOf("" & vbLf, 1)
            Dim enfpstr As String = ""
            If (enfpindex > 0) Then
                enfpstr = sBuffer.Substring(0, enfpindex)
            End If
            saveenfplog(enfpstr)
            Dim endop As String = sBuffer.Substring((enfpindex + 1))
            If (endop <> "") Then
                enfplogprocess(endop)
            End If
        Catch
        End Try
    End Sub
    Public Sub saveenfplog(ByVal enfplog As String)
        'if (enfplog.IndexOf("PIN", 0) > 0)
        If ((enfplog.IndexOf("FP PIN", 0) >= 0) _
                    AndAlso (enfplog.IndexOf("FID", 0) > 0)) Then
            Dim EnFPid As String = enfplog.Substring(0, enfplog.IndexOf("" & vbTab, 0))
            Dim stillenfp1 As String = enfplog.Substring((enfplog.IndexOf("" & vbTab, 0) + 1))
            Dim enfpnum1 As String = stillenfp1.Substring(0, stillenfp1.IndexOf("" & vbTab, 0))
            Dim stillenfp2 As String = stillenfp1.Substring((stillenfp1.IndexOf("" & vbTab, 0) + 1))
            Dim enfpnum2 As String = stillenfp2.Substring(0, stillenfp2.IndexOf("" & vbTab, 0))
            Dim fplen As String = ""
            fplen = enfpnum2.Substring(5)
            Dim stillenfp3 As String = stillenfp2.Substring((stillenfp2.IndexOf("" & vbTab, 0) + 1))
            Dim enfpnum3 As String = stillenfp3.Substring(0, stillenfp3.IndexOf("" & vbTab, 0))
            Dim stillenfp4 As String = stillenfp3.Substring((stillenfp3.IndexOf("" & vbTab, 0) + 1))
            Dim enfpnum4 As String = stillenfp4.Substring(0, Convert.ToInt32(fplen))
            '' Display enfplog in customized format
            'Me.WriteEnFPlog(EnFPid.Replace("FP PIN=", ""), enfpnum1.Replace("FID=", ""), enfpnum2.Replace("Size=", ""), enfpnum3.Replace("Valid=", ""), enfpnum4.Replace("TMP=", ""))
            'If (tabOptions.SelectedIndex = 3) Then
            '    lblRecords.Text = "Enroll FP Count:"
            '    lblCount.Text = lvEnrollFP.Items.Count.ToString
            'End If
        End If
    End Sub
    ' Get Host IP
    Protected Function GetIP() As String
        Dim ipHost As IPHostEntry = Dns.Resolve(Dns.GetHostName)
        Dim ipAddr As IPAddress = ipHost.AddressList(0)
        Return ipAddr.ToString
    End Function
    'Public Sub cdataProcess(ByVal bReceive As Byte(), ByVal remoteSocket As Socket)
    '    Dim sBuffer As String = Encoding.ASCII.GetString(bReceive)
    '    Dim sHttpVersion As String = ""
    '    Dim sMimeType1 As String = "text/plain"
    '    Dim SendLength As Integer = 0
    '    sHttpVersion = sBuffer.Substring(sBuffer.IndexOf("HTTP", 1), 8)
    '    Dim SN As String = ""
    '    SN = GetDeviceSerialNumber(sBuffer)

    '    If sBuffer.Substring(0, 3) = "GET" Then
    '        Dim Stamp As String = "", OpStamp As String = "", PhotoStamp As String = "", ErrorDelay As String = "30", Delay As String = "15", TransTimes As String = "", TransInterval As String = "", TransFlag As String = "1111101111", Realtime As String = "", Encrypt As String = "", TimeZoneclock As String = ""
    '        Dim machineSN As String = sBuffer.Substring(sBuffer.IndexOf("SN=") + 3)
    '        'Me.getnumber(machineSN, SN)
    '        SN = GetDeviceSerialNumber(sBuffer)


    '        Realtime = "1"
    '        TransTimes = "00:00;14:05"
    '        Encrypt = "0"
    '        TimeZoneclock = "1"
    '        TransInterval = "1"

    '        If OPERLOGStamp.Length > 0 Then
    '            Stamp = OPERLOGStamp
    '        Else
    '            Stamp = "1"
    '        End If

    '        Realtime = "1"
    '        TransTimes = "00:00;14:05"
    '        Encrypt = "0"
    '        TimeZoneclock = "1"
    '        TransInterval = "1"

    '        If USERINFOStamp.Length > 0 Then
    '            Stamp = USERINFOStamp
    '        Else
    '            Stamp = "1"
    '        End If


    '        'nitin
    '        Dim sSql As String = "select * from ZKATTLOGStamp where SN='" & SN & "'"
    '        Dim adapA As OleDbDataAdapter
    '        Dim adap As SqlDataAdapter
    '        Dim ds As DataSet = New DataSet
    '        If Common.servername = "Access" Then
    '            adapA = New OleDbDataAdapter(sSql, Common.con1)
    '            adapA.Fill(ds)
    '        Else
    '            adap = New SqlDataAdapter(sSql, Common.con)
    '            adap.Fill(ds)
    '        End If
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Stamp = ds.Tables(0).Rows(0).Item("ATTLOGStamp").ToString.Trim
    '        Else
    '            Stamp = "1"
    '        End If
    '        'Stamp = "9999"
    '        'nitin end

    '        Realtime = "1"
    '        TransTimes = "00:00;14:05"
    '        Encrypt = "0"
    '        TimeZoneclock = "1"
    '        TransInterval = "1"
    '        Dim Time_Zone As String = "0"
    '        Dim optionConfig As String = "GET OPTION FROM:" & SN & vbCrLf & "Stamp=" & Stamp & vbCrLf & "OpStamp=" & OpStamp & vbCrLf & _
    '           "PhotoStamp=" & PhotoStamp & vbCrLf & "ErrorDelay=" & ErrorDelay & vbCrLf & "Delay=" & Delay & vbCrLf & _
    '           "TransTimes=" & TransTimes & vbCrLf & "TransInterval=" & TransInterval & vbCrLf & "TransFlag=" & TransFlag & vbCrLf & _
    '           "Realtime=" & Realtime & vbCrLf & "Encrypt=" & Encrypt & vbCrLf & "TimeZoneclock=" & TimeZoneclock & vbCrLf & _
    '        "TimeZone=" & Time_Zone & vbCrLf

    '        Dim bOption As Byte() = Encoding.GetEncoding("gb2312").GetBytes(optionConfig)
    '        SendLength = bOption.Length
    '        SendHeader(sHttpVersion, sMimeType1, SendLength, " 200 OK", remoteSocket)
    '        SendToBrowser(bOption, remoteSocket)
    '        remoteSocket.Close()
    '        Return
    '    End If

    '    If sBuffer.Substring(0, 4) = "POST" Then

    '        If sBuffer.IndexOf("Stamp", 1) > 0 AndAlso sBuffer.IndexOf("OPERLOG", 1) < 0 AndAlso sBuffer.IndexOf("ATTLOG", 1) > 0 AndAlso sBuffer.IndexOf("OPLOG", 1) < 0 Then
    '            attlog(sBuffer, SN)
    '        End If

    '        'If sBuffer.IndexOf("Stamp", 1) > 0 AndAlso sBuffer.IndexOf("OPERLOG", 1) > 0 AndAlso sBuffer.IndexOf("OPLOG", 1) > 0 Then
    '        '   oplog(sBuffer)
    '        'End If

    '        'If sBuffer.IndexOf("Stamp", 1) > 0 AndAlso sBuffer.IndexOf("OPERLOG", 1) > 0 AndAlso sBuffer.IndexOf("OPLOG 6", 1) > 0 Then
    '        '    enfplog(sBuffer)
    '        'End If

    '        'If sBuffer.IndexOf("Stamp", 1) > 0 AndAlso sBuffer.IndexOf("USERINFO", 1) > 0 Then
    '        '   usinlog(sBuffer)
    '        'End If

    '        Dim replyok As Byte() = Encoding.ASCII.GetBytes("OK")
    '        SendLength = replyok.Length
    '        SendHeader(sHttpVersion, sMimeType1, SendLength, " 200 OK", remoteSocket)
    '        SendToBrowser(replyok, remoteSocket)
    '        remoteSocket.Close()
    '    End If
    'End Sub
    Public Sub cdataProcess(ByVal bReceive() As Byte, ByVal connection As tcpServer.TcpServerConnection, ByVal SN As String)
        'TraceService("cdataProcess ", " Line on 3216--" );
        Dim sBuffer As String = Encoding.ASCII.GetString(bReceive)
        Dim subReceive As String = sBuffer.Substring(0, 70)
        Dim deviceSerialNumber As String = SN ' Me.GetDeviceSerialNumber(subReceive)
        Dim sHttpVersion As String = ""
        Dim sMimeType1 As String = "text/plain"
        Dim SendLength As Integer = 0
        sHttpVersion = sBuffer.Substring(sBuffer.IndexOf("HTTP", 1), 8)
        ' http version
        ' The first time we use or when we restart the Device
        If (sBuffer.Substring(0, 3) = "GET") Then
            Dim conS As SqlConnection '= New SqlConnection(Common.ConnectionString)
            Dim conS1 As OleDbConnection ' = New OleDbConnection(Common.ConnectionString)
            If Common.servername = "Access" Then
                conS1 = New OleDbConnection(Common.ConnectionString)
            Else
                conS = New SqlConnection(Common.ConnectionString)
            End If
            Try
                Dim TimeZoneclock As String = ""
                Dim Stamp As String = ""
                Dim OpStamp As String = ""
                Dim PhotoStamp As String = "1"
                Dim ErrorDelay As String = "30"
                Dim Delay As String = "15"
                Dim TransTimes As String = ""
                Dim TransInterval As String = ""
                Dim TransFlag As String = "1111101111"
                Dim Realtime As String = ""
                Dim Encrypt As String = ""
                'Dim machineSN As String = sBuffer.Substring((sBuffer.IndexOf("SN=") & 3))
                ' this.getnumber(machineSN, ref SN); // GET OPTION FROM: Serial Number of iclock Device
                'if (ATTLOGStamp.Length > 0)
                '    Stamp = ATTLOGStamp;
                'else
                '    Stamp = "1";//"1"; // If we loose connection during the process, we get again all records from the beginning (we can change that to get only the last missed records)
                Realtime = "1"
                TransTimes = "00:00;14:05"
                Encrypt = "0"
                TimeZoneclock = "1"
                TransInterval = "1"
                'If (OPERLOGStamp.Length > 0) Then
                '    Stamp = OPERLOGStamp
                'Else
                '    Stamp = "1"
                'End If

                ' If we loose connection during the process, we get again all records from the beginning (we can change that to get only the last missed records)
                Realtime = "1"
                TransTimes = "00:00;14:05"
                Encrypt = "0"
                TimeZoneclock = "1"
                TransInterval = "1"
                'If (USERINFOStamp.Length > 0) Then
                '    Stamp = USERINFOStamp
                'Else
                '    Stamp = "1"
                'End If

                '"1"; // If we loose connection during the process, we get again all records from the beginning (we can change that to get only the last missed records)
                Realtime = "1"
                TransTimes = "00:00;14:05"
                Encrypt = "0"
                ' European Time Zone (Set the Time on the Device when we restart the Device)
                ' Time Zone --> In China devices, Time Zone = 8, in European devices, Time Zone = 1
                Dim Time_Zone As String = "0"
                TimeZoneclock = "1"   ' 1 For std
                TransInterval = "1"
                Dim sql As String
                sql = "select * from ZKATTLOGStamp where SN='" & SN & "'"
                Dim ds As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sql, conS1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sql, conS)
                    adap.Fill(ds)
                End If
                If (ds.Tables(0).Rows.Count > 0) Then
                    Stamp = ds.Tables(0).Rows(0)("ATTLOGStamp").ToString.Trim
                    'OpStamp = ds.Tables(0).Rows(0)("USERINFOStamp").ToString.Trim
                    'If sBuffer.IndexOf("options=all", 1) > 0 Then   ' this not sure
                    '    Stamp = ds.Tables(0).Rows(0)("USERINFOStamp").ToString.Trim ' USERINFOStamp
                    'Else 'If (sBuffer.IndexOf("USER PIN", 1) > 0) Or (sBuffer.IndexOf("FP PIN", 1) > 0) Or (sBuffer.IndexOf("FACE PIN", 1) > 0) Then
                    '    Stamp = ds.Tables(0).Rows(0)("ATTLOGStamp").ToString.Trim
                    'End If
                Else
                    Stamp = "1"
                End If
                If Stamp.Trim = "" Then
                    Stamp = "1"
                End If

                Dim optionConfig As String = "GET OPTION FROM:" & SN & vbCrLf & "Stamp=" & Stamp & vbCrLf & "OpStamp=" & OpStamp & vbCrLf & _
                 "PhotoStamp=" & PhotoStamp & vbCrLf & "ErrorDelay=" & ErrorDelay & vbCrLf & "Delay=" & Delay & vbCrLf & _
                 "TransTimes=" & TransTimes & vbCrLf & "TransInterval=" & TransInterval & vbCrLf & "TransFlag=" & TransFlag & vbCrLf & _
                 "Realtime=" & Realtime & vbCrLf & "Encrypt=" & Encrypt & vbCrLf & "TimeZoneclock=" & TimeZoneclock & vbCrLf & _
                 "TimeZone=" & Time_Zone & vbCrLf

                'optionConfig &= optionConfig & "112233";
                Dim bOption() As Byte = Encoding.GetEncoding("gb2312").GetBytes(optionConfig)
                SendLength = bOption.Length
                SendHeader(sHttpVersion, sMimeType1, SendLength, " 200 OK", connection, deviceSerialNumber)
                SendToBrowser(bOption, connection, deviceSerialNumber)
                connection.Socket.Close()
                'tmp
                'TraceService("Closed Successfully", "Line 3154");
                Return
            Catch ex As Exception
                'TraceService(ex.Message, "Line 3329")
                Return
            End Try

        End If

        If (sBuffer.Substring(0, 4) = "POST") Then
            Try
                Dim replyok() As Byte = Encoding.GetEncoding("gb2312").GetBytes("OK") ' Encoding.ASCII.GetBytes("OK")
                SendLength = replyok.Length
                SendHeader(sHttpVersion, sMimeType1, SendLength, " 200 OK", connection, deviceSerialNumber)
                SendToBrowser(replyok, connection, deviceSerialNumber)
                'connection.Socket.Close()
                'tmp
                ' Only PUSH SDK Ver 2.0.1 (In Version 1.0 String for AttLog have diferent format, example: CHECK LOG: stamp=392232960 1       2012-03-14 17:39:00     0       0       0       1)

                Dim DataCheckThread As Thread = New Thread(Sub() DataCheck(sBuffer, deviceSerialNumber))
                DataCheckThread.Start()
                'DataCheckThread.IsBackground = True

            Catch ex As Exception
                'TraceService(ex.Message, "Line 3359")
                Return
            End Try
        End If
    End Sub
    Private Sub DataCheck(sBuffer As String, deviceSerialNumber As String)
        If sBuffer.IndexOf("Stamp", 1) > 0 And sBuffer.IndexOf("OPERLOG", 1) < 0 And sBuffer.IndexOf("ATTLOG", 1) > 0 And sBuffer.IndexOf("OPLOG", 1) < 0 Then
            attlog(sBuffer, deviceSerialNumber)
        End If
    End Sub
    Public Sub Analysis(bReceive As Byte(), connection As tcpServer.TcpServerConnection)
        Dim strReceive As String = Encoding.ASCII.GetString(bReceive)
        Dim temp As String = System.Text.Encoding.Default.GetString(bReceive)
        Console.WriteLine(strReceive)
        Dim subReceive As String = strReceive.Substring(0, 70)
        Dim deviceSerialNumber As String = GetDeviceSerialNumber(strReceive)
        If deviceSerialNumber.Trim = "" Or deviceSerialNumber.Trim = "0" Then
            Return
        End If

        If strReceive.IndexOf("cdata?") > 0 Or strReceive.IndexOf("cdata.aspx?") > 0 Then
            cdataProcess(bReceive, connection, deviceSerialNumber.Trim)
            Return
        End If
        If strReceive.IndexOf("getrequest?") > 0 Or strReceive.IndexOf("getrequest.aspx?") > 0 Then
            getrequestProcess(bReceive, connection, deviceSerialNumber)
            deviceSerialNumber = ""
            Return
        End If

        If strReceive.IndexOf("devicecmd?") > 0 Then
            getrequestProcess(bReceive, connection, deviceSerialNumber)
            'devicecmdProcess(strReceive, deviceSerialNumber)
            deviceSerialNumber = ""
            Return
        End If

        If strReceive.IndexOf("fdata?") > 0 Then
            Dim imgrecindex As Integer = 0

            For i As Integer = 0 To bReceive.Length - 1
                imgrecindex = i
                If CInt(bReceive(i)) = 0 Then Exit For
            Next

            imgrecindex += 1
            getrequestProcess(bReceive, connection, deviceSerialNumber)
            deviceSerialNumber = ""
            'getrequestProcess(bReceive, endsocket)
            Dim imgReceive As Byte() = New Byte(bReceive.Length - imgrecindex - 1) {}
            Array.Copy(bReceive, imgrecindex, imgReceive, 0, bReceive.Length - imgrecindex)
            Dim path As String = System.Environment.CurrentDirectory & "\RealTimeImages"
            PathofImage = path
            path += "\" & strImageNumber.Replace("PIN=", "")
            System.IO.File.WriteAllBytes(path, imgReceive)

            If PathofImage IsNot Nothing AndAlso PathofImage <> "" Then
                Try
                    'picAttPhoto.Image = Image.FromFile(path)
                    'picAttPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
                    'Me.picAttPhoto.Update()
                Catch
                End Try
            End If
            GC.Collect()
            Return
        End If
    End Sub
    Public Sub getnumber(ByVal sBuffer As String, ByRef numberstr As String)
        numberstr = ""
        'For i As Integer = 0 To sBuffer.Length - 1
        '    Try
        '        If sBuffer(i).ToString > 47 AndAlso sBuffer(i).ToString < 58 Then
        '            numberstr += sBuffer(i)
        '        Else
        '            Exit For
        '        End If
        '    Catch ex As Exception

        '    End Try
        'Next
        Try
            numberstr = sBuffer.Substring(0, sBuffer.IndexOf(" "))
        Catch ex As Exception

        End Try
    End Sub
    Public Sub getrequestProcess(ByVal bReceive() As Byte, ByVal connection As tcpServer.TcpServerConnection, ByVal transferTo_New As String)
        Dim sBuffer As String = Encoding.GetEncoding("gb2312").GetString(bReceive)
        Dim sHttpVersion As String = ""
        Dim sMimeType1 As String = "text/plain", cmdstring As String = ""
        Dim SendLength As Integer = 0
        sHttpVersion = sBuffer.Substring(sBuffer.IndexOf("HTTP", 1), 8)
        Dim SN As String = Nothing, machineSN As String = sBuffer.Substring(sBuffer.IndexOf("SN=") + 3)
        'Me.getnumber(machineSN, SN)
        SN = GetDeviceSerialNumber(sBuffer)
        cmdstring = "OK"

        If sBuffer.Substring(0, 3) = "GET" AndAlso sBuffer.Substring(12, 10) = "getrequest" Then

            If sBuffer.IndexOf("INFO") > 0 Then
                cmdstring = "OK"
            ElseIf sBuffer.IndexOf("options") > 0 Then
                cmdstring = "OK"
            ElseIf flagreboot = True Then
                cmdstring = "C:ID1:REBOOT" & vbLf
                flagreboot = False
            ElseIf flagshell = True Then
                cmdstring = "C:ID10:SHELL " & shellinput & vbLf
                flagshell = False
                shellinput = ""
            ElseIf flagcheck = True Then
                cmdstring = "C:20:CHECK" & vbLf
                flagcheck = False
            ElseIf flaginfo = True Then
                cmdstring = "C:ID30:INFO ls" & vbLf
                flaginfo = False
            ElseIf flagoption = True Then
                cmdstring = "C:ID40:SET OPTION " & optioninput & vbLf
                flagoption = False
                optioninput = ""
            ElseIf flagclear = True Then
                cmdstring = "C:ID50:" & clearinput & vbLf
                flagclear = False
            ElseIf flagupduserinfo = True Then
                cmdstring = "C:ID60:" & upduserinfoinput & vbLf
                flagupduserinfo = False
                'ElseIf flagusersms = True Then
                '    cmdstring = "C:ID70:" & usersmsinput & vbLf
                '    flagusersms = False
                'ElseIf flaguserpersonalsms = True Then
                '    cmdstring = "C:ID75:" & userpersonalsmsinput & vbLf
                '    flaguserpersonalsms = False
            ElseIf flagunlock = True Then
                cmdstring = "C:ID80:CHECK" & vbLf
                flagunlock = False
            ElseIf flagdeluserinfo = True Then
                cmdstring = "C:ID65:" & deluserinfoinput & vbLf
                flagdeluserinfo = False
                'ElseIf flagsetfp = True Then
                '    cmdstring = "C:ID45:" & setfpinput & vbLf
                '    flagsetfp = False
                'ElseIf flagdelfp = True Then
                '    cmdstring = "C:ID55:" & delfpinput & vbLf
                '    flagdelfp = False
            End If
        End If

        If sBuffer.Substring(0, 4) = "POST" Then
            If sBuffer.IndexOf("Stamp", 1) > 0 AndAlso (sBuffer.IndexOf("Photo", 1) > 0 OrElse sBuffer.IndexOf("ATTPHOTO", 1) > 0) AndAlso sBuffer.IndexOf("OPLOG", 1) < 0 Then
                ' attpholog(sBuffer)
            End If
        End If

        Dim replyok As Byte() = Encoding.GetEncoding("gb2312").GetBytes(cmdstring)
        SendLength = replyok.Length
        SendHeader(sHttpVersion, sMimeType1, SendLength, " 200 OK", connection, transferTo_New)
        SendToBrowser(replyok, connection, transferTo_New)
        connection.Socket.Close()
        cmdstring = ""
    End Sub
    Public Sub devicecmdProcess(ByVal bReceive As Byte())
        Dim sBuffer As String = Encoding.ASCII.GetString(bReceive)
        Dim strReceive As String = sBuffer

        If strReceive.IndexOf("Return=0") <= 0 Then
            'MessageBox.Show("Error! Please try again!", "OK")
            Return
        End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=Shell") > 0 Then
        '    cmdshell(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=CHECK") > 0 AndAlso strReceive.IndexOf("ID=ID20") > 0 Then
        '    cmdcheck(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=INFO") > 0 Then
        '    cmdinfo(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=SET") > 0 Then
        '    cmdoption(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=CLEAR") > 0 Then
        '    cmdclear(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=DATA") > 0 AndAlso strReceive.IndexOf("ID=ID60") > 0 Then
        '    cmdudpuserinfo(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=DATA") > 0 AndAlso strReceive.IndexOf("ID=ID65") > 0 Then
        '    cmddeluserinfo(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=DATA") > 0 AndAlso strReceive.IndexOf("ID=ID70") > 0 Then
        '    cmdusersms(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=DATA") > 0 AndAlso strReceive.IndexOf("ID=ID75") > 0 Then
        '    cmduserpersonalsms(sBuffer)
        '    Return
        'End If

        'If strReceive.IndexOf("Return=0") > 0 AndAlso strReceive.IndexOf("CMD=CHECK") > 0 AndAlso strReceive.IndexOf("ID=ID80") > 0 Then
        '    cmdunlock(sBuffer)
        '    Return
        'End If
    End Sub
    Public Sub SendHeader(ByVal sHttpVersion As String, ByVal sMIMEHeader As String, ByVal iTotBytes As Integer, ByVal sStatusCode As String, ByRef connection As tcpServer.TcpServerConnection, ByVal deviceSerialNumber As String)
        Dim sBuffer As String = Nothing

        If sMIMEHeader.Length = 0 Then
            sMIMEHeader = "text/html"
        End If

        sBuffer = sBuffer & sHttpVersion & sStatusCode & vbCrLf
        sBuffer = sBuffer & "Server: cx1193719-b" & vbCrLf
        sBuffer = sBuffer & "Content-Type: " & sMIMEHeader & vbCrLf
        sBuffer = sBuffer & "Accept-Ranges: bytes" & vbCrLf
        ''nitin original
        'Dim WeekDay As String = DateTime.UtcNow.DayOfWeek.ToString().Remove(3)  'DateTime.Now.DayOfWeek.ToString().Remove(3) '
        'Dim Month As String = DateTime.UtcNow.Month.ToString()  'DateTime.Now.Month.ToString() '

        'new
        Dim TimeZone As Integer = 330   '0 only for idtech customer
        Dim WeekDay As String = DateTime.UtcNow.AddMinutes(TimeZone).DayOfWeek.ToString.Remove(3)
        Dim Month As String = DateTime.UtcNow.AddMinutes(TimeZone).Month.ToString

        If Month = "1" Then Month = "Jan"
        If Month = "2" Then Month = "Feb"
        If Month = "3" Then Month = "Mar"
        If Month = "4" Then Month = "Apr"
        If Month = "5" Then Month = "May"
        If Month = "6" Then Month = "Jun"
        If Month = "7" Then Month = "Jul"
        If Month = "8" Then Month = "Aug"
        If Month = "9" Then Month = "Sep"
        If Month = "10" Then Month = "Oct"
        If Month = "11" Then Month = "Nov"
        If Month = "12" Then Month = "Dec"
        ''nitin original
        'Dim Hour As String = DateTime.UtcNow.Hour.ToString() 'DateTime.Now.Hour.ToString() ' 
        'Dim Minute As String = DateTime.UtcNow.Minute.ToString() 'DateTime.Now.Minute.ToString() '
        'Dim Second As String = DateTime.UtcNow.Second.ToString()  'DateTime.Now.Second.ToString() '

        'new
        Dim DT As DateTime = DateTime.UtcNow.AddMinutes(TimeZone)
        Dim Hour As String = DT.Hour.ToString
        Dim Minute As String = DT.Minute.ToString
        Dim Second As String = DT.Second.ToString

        If Hour.Length = 1 Then Hour = "0" & Hour
        If Minute.Length = 1 Then Minute = "0" & Minute
        If Second.Length = 1 Then Second = "0" & Second
        'sBuffer = sBuffer & "Date: " & WeekDay & ", " & DateTime.UtcNow.Day & " " & Month & " " & DateTime.UtcNow.Year & " " & Hour & ":" & Minute & ":" & Second & " GMT" & vbCrLf
        sBuffer = sBuffer & "Date: " & WeekDay & ", " & DateTime.Now.Day & " " & Month & " " & DateTime.Now.Year & " " & Hour & ":" & Minute & ":" & Second & " GMT" & vbCrLf
        sBuffer = sBuffer & "Content-Length: " & iTotBytes & vbCrLf & vbCrLf
        Dim bSendData As Byte() = Encoding.GetEncoding("gb2312").GetBytes(sBuffer)
        'SendToBrowser(bSendData, mySocket)
        SendToBrowser(bSendData, connection, deviceSerialNumber)
    End Sub
    Public Sub SendToBrowser(ByVal sData As String, ByRef connection As tcpServer.TcpServerConnection, ByVal deviceSerialNumber As String)
        SendToBrowser(Encoding.GetEncoding("gb2312").GetBytes(sData), connection, deviceSerialNumber)
    End Sub
    Public Sub SendToBrowser(ByVal bSendData() As Byte, ByRef connection As tcpServer.TcpServerConnection, ByVal deviceSerialNumber As String)
        Dim numBytes As Integer = 0
        Try
            If connection.Socket.Connected Then
                If (connection.Socket.Client.Send(bSendData, bSendData.Length, 0) = -1) Then
                    'MessageBox.Show("Socket Error::::", ("Cannot Send Packet***" & deviceSerialNumber))
                Else
                    'MessageBox.Show(numBytes & " Sent");
                End If
            Else
                'MessageBox.Show("Socket Not Connected::::", ("Cannot Send Packet***" & deviceSerialNumber))
            End If
        Catch ex As SocketException
            'MessageBox.Show("Socket Not Connected::::", "Cannot Send Packet***" & deviceSerialNumber)
        End Try
    End Sub
    Public Sub attpholog(ByVal sBuffer As String)
        Dim machineSN As String = sBuffer.Substring(sBuffer.IndexOf("SN=") + 3)
        Dim SN As String = ""
        'Me.getnumber(machineSN, SN)
        SN = GetDeviceSerialNumber(sBuffer)
        Dim machinestamp As String = sBuffer.Substring(sBuffer.IndexOf("Stamp=") + 6)
        Dim Stamp As String = ""
        Me.getnumber(machinestamp, Stamp)
        ATTPHOTOStamp = Stamp
        Dim attphoindex As Integer = sBuffer.IndexOf(vbCrLf & vbCrLf, 1)
        Dim attphostr As String = sBuffer.Substring(attphoindex + 4)
        'for photo
        attphoprocess(attphostr)
    End Sub
    Public Sub attphoprocess(ByVal sBuffer As String)
        Try
            Dim attphoindex As Integer = sBuffer.IndexOf("" & vbLf, 1)
            Dim attphostr As String = ""
            If (attphoindex > 0) Then
                attphostr = sBuffer
            End If
            saveattpho(attphostr)
            Dim endattpho As String = sBuffer.Substring((attphoindex + 1))
            If ((endattpho <> "") _
                        AndAlso (Not (endattpho) Is Nothing)) Then
                attphoprocess(endattpho)
            End If
        Catch ex As System.Exception
        End Try
    End Sub
    Public Sub saveattpho(ByVal attpho As String)
        Dim imgtemp As String = attpho.Substring(0, attpho.IndexOf(vbNullChar, 1))
        Dim pintemp As String = imgtemp.Substring(0, imgtemp.IndexOf("" & vbLf, 1))
        Dim attphostill1 As String = imgtemp.Substring((imgtemp.IndexOf("" & vbLf, 1) + 1))
        Dim sntemp As String = attphostill1.Substring(0, attphostill1.IndexOf("" & vbLf, 1))
        Dim attphostill2 As String = attphostill1.Substring((attphostill1.IndexOf("" & vbLf, 1) + 1))
        Dim sizetemp As String = attphostill2.Substring(0, attphostill2.IndexOf("" & vbLf, 1))
        Dim attphostill3 As String = attphostill2.Substring((attphostill2.IndexOf("" & vbLf, 1) + 1))
        Dim cmdtemp As String = attphostill3
        '' Display AttPhoto in customized format
        'Me.WriteAttPholog(pintemp.Replace("PIN=", ""), sizetemp.Replace("size=", ""), cmdtemp.Replace("CMD=", ""))
        'If (tabOptions.SelectedIndex = 4) Then
        '    lblRecords.Text = "AttPhotoLogs Count:"
        '    lblCount.Text = lvAttPhoto.Items.Count.ToString
        'End If

        strImageNumber = pintemp
    End Sub
    Private Function GetDeviceSerialNumber(ByVal strReceive As String) As String
        Dim SerialNo As String = ""
        Try
            Dim FirstIndex As Integer = 0
            FirstIndex = strReceive.IndexOf("?SN=")
            Dim FistPart As String = strReceive.Substring(FirstIndex, 30)
            Dim SecondIndexs As Integer = 0
            If (FistPart.IndexOf(" ") > 0) Then
                SecondIndexs = FistPart.IndexOf(" ")
            Else
                SecondIndexs = FistPart.IndexOf("&")
            End If

            SerialNo = FistPart.Substring(4, (SecondIndexs - 4))
        Catch ex As Exception
            Dim x = ex.ToString

        End Try

        'MessageBox.Show(SerialNo);
        Return SerialNo
    End Function
    Private Sub openTcpPort()
        Try
            tcpServer1.Close()
            tcpServer1.Port = Convert.ToInt32(ZKPort)
            tcpServer1.Open()
            'displayTcpServerStatus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error")

        End Try
    End Sub
    Protected Function readStream(client As TcpClient) As Byte()
        Try
            Dim stream As NetworkStream = client.GetStream()
            If stream.DataAvailable Then
                Dim data As Byte() = New Byte(client.Available - 1) {}

                Dim bytesRead As Integer = 0
                Try
                    bytesRead = stream.Read(data, 0, data.Length)
                Catch generatedExceptionName As IOException
                End Try

                If bytesRead < data.Length Then
                    Dim lastData As Byte() = data
                    data = New Byte(bytesRead - 1) {}
                    Array.ConstrainedCopy(lastData, 0, data, 0, bytesRead)
                End If
                Return data
            End If
            Return Nothing
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Sub tcpServer1_OnDataAvailable(connection As tcpServer.TcpServerConnection) Handles tcpServer1.OnDataAvailable
        Try
            Thread.Sleep(5000) 'very imp
            Dim databyte As Byte() '= readStream(connection.Socket)
            Try
                databyte = readStream(connection.Socket)
            Catch ex As Exception
                Return
            End Try
            Dim strReceive As String = Encoding.ASCII.GetString(databyte)
            If strReceive.Trim <> "" Then
                If (databyte.Length > 0) Then
                    Analysis(databyte, connection)
                End If
            End If
        Catch ex As Exception
            'TraceService(ex.Message, " Line 358")            
        End Try
    End Sub
    '' Write AttPhoto for iclock Device
    'Public Delegate Sub CallBackAttPhoto(ByVal PhotoPinTemp As String, ByVal PhotoSizeTemp As String, ByVal PhotoCmdTemp As String)
    'Private Sub WriteAttPholog(ByVal PhotoPinTemp As String, ByVal PhotoSizeTemp As String, ByVal PhotoCmdTemp As String)
    '    If Me.lvAttPhoto.InvokeRequired Then
    '        Dim write As CallBackAttPhoto = New CallBackAttPhoto(WriteAttPholog)
    '        Me.Invoke(write, New Object() {PhotoPinTemp, PhotoSizeTemp, PhotoCmdTemp})
    '    Else
    '        lvAttPhoto.Items.Add(PhotoPinTemp)
    '        lvAttPhoto.Items((lvAttPhoto.Items.Count - 1)).SubItems.Add(PhotoSizeTemp)
    '        lvAttPhoto.Items((lvAttPhoto.Items.Count - 1)).SubItems.Add(PhotoCmdTemp)
    '        ' Select the last one (correspond with the Photo)
    '        lvAttPhoto.Items((lvAttPhoto.Items.Count - 1)).Selected = True
    '    End If

    'End Sub
    'zk listening end
    Private Sub SimpleButtonClear_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonClear.Click
        Me.fnCount = 0
        lblTotal.Text = "Total : 0"
        Me.SetupLogListview()
        PictureEdit.Image = Nothing
        TextEdit1.Text = ""
    End Sub
    Private Sub LEDPeDIkhana(VehicleNo As String, LEDIP As String)
        Dim ErrStr As String
        Dim nResult As Long
        Dim hProgram As Long
        Dim ARect As AREARECT
        Dim FProp As FONTPROP
        Dim PProp As PLAYPROP

        Dim DisplayMsg As String = ""
        If VehicleNo = "" Then
            DisplayMsg = "   STOP  "
        End If
        Dim LedCommunicationInfo As New COMMUNICATIONINFO
        LedCommunicationInfo.SendType = 0
        LedCommunicationInfo.IpStr = LEDIP '"192.168.1.99"
        LedCommunicationInfo.LedNumber = 1

        hProgram = LV_CreateProgram(128, 32, 2)

        Call LV_AddProgram(hProgram, 1, 0, 1)

        'ARect.left = 20
        'ARect.top = 0
        'ARect.width = 90
        'ARect.height = 32
        'Call LV_AddImageTextArea(hProgram, 1, 1, ARect, 0)



        '"Arial Black" MS"    "Arial Black"


        If DisplayMsg.Trim = "STOP" Then
            FProp.FontName = "Verdana"
            ARect.left = 20
            ARect.top = 0
            ARect.width = 90
            ARect.height = 32

            Call LV_AddImageTextArea(hProgram, 1, 1, ARect, 0)

            DisplayMsg = "STOP" 'FOR " & vbCrLf & "  CHECK IN"
            FProp.FontColor = COLOR_RED
            FProp.FontSize = 22
            FProp.FontBold = True

        Else
            DisplayMsg = "WELCOME TO LTI" & vbCrLf & VehicleNo.Trim
            FProp.FontName = "Verdana" ' "Verdana"
            ARect.left = 2
            ARect.top = 0
            ARect.width = 125
            ARect.height = 32

            Call LV_AddImageTextArea(hProgram, 1, 1, ARect, 0)
            FProp.FontColor = COLOR_GREEN
            FProp.FontSize = 10
            FProp.FontBold = True
            TimerLEDStop.Enabled = True
            TimerLEDStop.Stop()
            TimerLEDStop.Start()
        End If
        'FProp.FontColor = COLOR_GREEN


        PProp.InStyle = 0
        PProp.DelayTime = 10 '3
        PProp.Speed = 4

        '可以添加多个子项到图文区，如下添加可以选一个或多个添加

        Call LV_AddMultiLineTextToImageTextArea(hProgram, 1, 1, ADDTYPE_STRING, DisplayMsg, FProp, PProp, 2, 0) '通过字符串添加一个多行文本到图文区，参数说明见声明注示
        'Call LV_AddMultiLineTextToImageTextArea(hProgram, 1, 1, ADDTYPE_FILE, "test.rtf", FProp, PProp, 0, 0) '通过rtf文件添加一个多行文本到图文区，参数说明见声明注示
        'Call LV_AddMultiLineTextToImageTextArea(hProgram, 1, 1, ADDTYPE_FILE, "test.txt", FProp, PProp, 0, 0) '通过txt文件添加一个多行文本到图文区，参数说明见声明注示


        nResult = LV_Send(LedCommunicationInfo, hProgram)
        LV_DeleteProgram(hProgram)
        If nResult Then
            ErrStr = Space(256)
            LV_GetError(nResult, 256, ErrStr)
            'MsgBox(ErrStr)
        Else
            'MsgBox("发送成功")
        End If
    End Sub
    Private Sub TimerLEDStop_Tick(sender As System.Object, e As System.EventArgs) Handles TimerLEDStop.Tick
        If TmpLED.Trim <> "" Then
            LEDPeDIkhana("", TmpLED)
        End If
        TimerLEDStop.Enabled = False
    End Sub

    'TW-IR102 iris B100
    Dim _P_ENROLL_CALLBACK As B100API.P_ENROLL_CALLBACK
    Dim _P_MSG_CALLBACK As B100API.P_MSG_CALLBACK
    Dim _P_RECORD_CALLBACK As B100API.P_RECORD_CALLBACK
    Private Sub startTWIR()
        Try
            ''TWIR102Port = 15000
            'Control.CheckForIllegalCrossThreadCalls = False
            ''_P_ENROLL_CALLBACK = New B100API.P_ENROLL_CALLBACK(P_ENROLL_CALLBACK)
            ''_P_MSG_CALLBACK = New B100API.P_MSG_CALLBACK(P_MSG_CALLBACK)
            '_P_RECORD_CALLBACK = New B100API.P_RECORD_CALLBACK(AddressOf P_RECORD_CALLBACK)
            ''_P_RECORD_CALLBACK = New B100API.P_RECORD_CALLBACK(AddressOf (P_RECORD_CALLBACK(ip, dev, userid, sWorksn, sName, idCard, icCard, datetime, recogtype)))
            'If (B100API.F_Init(_P_ENROLL_CALLBACK, _P_MSG_CALLBACK, _P_RECORD_CALLBACK) <> 0) Then
            '    'MessageBox.Show("�1%", "�:")
            'Else
            '    B100API.F_StartLog(1)
            'End If
            'Dim ret As Integer = B100API.F_StartRecord(GetIP(), Integer.Parse(TWIR102Port))
            'If ret = 0 Then

            'Else
            '    XtraMessageBox.Show(ulf, "<size=10>Cannot Open Port For TW-IR102</size>", "<size=9>Error</size>")
            'End If
        Catch ex As Exception
        End Try
    End Sub
    Private Function P_RECORD_CALLBACK(ByVal ip As String, ByVal dev As String, ByVal userid As String, ByVal sWorksn As String, ByVal sName As String, ByVal idCard As String, ByVal icCard As String, ByVal datetime As String, ByVal recogtype As String) As Integer

        'LabelControl1.Text = String.Format("Receive attendance record, device IP: {0}, device: {1}, user ID: {2}, user name: {3}, recognition time: {4}, recognition type: {5}", ip, dev, userid, sName, datetime, recogtype)
        Try
            'SetControlStuta(True)
            'saveattlogs
            Dim conS As SqlConnection '= New SqlConnection(Common.ConnectionString)
            Dim conS1 As OleDbConnection ' = New OleDbConnection(Common.ConnectionString)
            If Common.servername = "Access" Then
                conS1 = New OleDbConnection(Common.ConnectionString)
            Else
                conS = New SqlConnection(Common.ConnectionString)
            End If

            Dim EnrollNumber As String = sWorksn
            If EnrollNumber = "" Then
                Return -1
            End If
            'EnrollNumber = string.Format("{00000000}");
            'EnrollNumber = EnrollNumber.PadLeft(6, Microsoft.VisualBasic.ChrW(48))
            'Dim still1 As String = attlog.Substring((attlog.IndexOf("" & vbTab, 1) + 1))
            Dim atttime As String = datetime 'still1.Substring(0, still1.IndexOf("" & vbTab, 1))
            'Dim still2 As String = still1.Substring((still1.IndexOf("" & vbTab, 1) + 1))
            Dim status As String = "" 'still2.Substring(0, still2.IndexOf("" & vbTab, 1))
            'Dim still3 As String = still2.Substring((still2.IndexOf("" & vbTab, 1) + 1))
            ' WorkCode and DeviceID only exists on Firmware Professional version
            Dim verifymode As String = ""
            Try
                ' Verify Mode
                verifymode = recogtype.ToUpper ' still3.Substring(0, still3.IndexOf("" & vbTab, 1))
            Catch
                verifymode = "" 'still3
            End Try
            Dim sSql As String
            '''//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            'Indian Code Master
            PunchTime = Convert.ToDateTime(atttime.ToString)
            Dim EnrNum As String
            If IsNumeric(EnrollNumber) Then
                EnrNum = Convert.ToInt64(EnrollNumber).ToString("000000000000")
            Else
                EnrNum = EnrollNumber
            End If
            Dim deviceId As String = ""
            Dim IN_OUT As String = ""
            Dim LEDIP As String = ""
            machine = dev
            sSql = "SELECT ID_NO, IN_OUT, LEDIP from tblMachine where MAC_ADDRESS='" & machine & "'"
            Dim ds As DataSet = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, conS)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                deviceId = ds.Tables(0).Rows(0).Item("ID_NO").ToString.Trim
                IN_OUT = ds.Tables(0).Rows(0).Item("IN_OUT").ToString.Trim
                LEDIP = ds.Tables(0).Rows(0).Item("LEDIP").ToString.Trim
            End If

            If IN_OUT = "B" Then
                Dim ds2 As DataSet = New DataSet
                Dim sSql1 As String '= "select count (CARDNO) from MachineRawPunch where CARDNO = '" & EnrNum & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd") & "'"
                Dim adapAT As OleDbDataAdapter
                Dim adapT As SqlDataAdapter
                If Common.servername = "Access" Then
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & EnrNum & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd") & "'"
                    adapAT = New OleDbDataAdapter(sSql1, Common.con1)
                    adapAT.Fill(ds2)
                Else
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & EnrNum & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd") & "'"
                    adapT = New SqlDataAdapter(sSql1, conS)
                    adapT.Fill(ds2)
                End If

                If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                    IN_OUT = "I"
                Else
                    If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                        IN_OUT = "I"
                    Else
                        IN_OUT = "O"
                    End If
                End If
            End If

            sSql = "select * from tblemployee, EmployeeGroup where PRESENTCARDNO='" & EnrNum & "' and EmployeeGroup.GroupId=TblEmployee.EmployeeGroupId"
            'sSql = "select * from tblemployee where PRESENTCARDNO='" & EnrNum & "'"
            ds = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, conS)
                adap.Fill(ds)
            End If
            Dim Paycode As String = ""
            Dim Name As String = ""
            Dim EId As Integer
            If ds.Tables(0).Rows.Count > 0 Then
                Name = ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim
                If Name = "" Then
                    Name = sName
                End If
                Paycode = ds.Tables(0).Rows(0).Item("Paycode").ToString.Trim
                EId = ds.Tables(0).Rows(0).Item("Id")
                sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & EnrNum & "','" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & deviceId & "','" & IN_OUT & "','" & Paycode & "' ) "
                Dim sSql1 As String = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & EnrNum & "','" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & deviceId & "','" & IN_OUT & "','" & Paycode & "' ) "
                Try
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        Try
                            cmd1 = New OleDbCommand(sSql1, Common.con1)
                            cmd1.ExecuteNonQuery()
                        Catch ex As Exception

                        End Try
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If conS.State <> ConnectionState.Open Then
                            conS.Open()
                        End If
                        cmd = New SqlCommand(sSql, conS)
                        cmd.ExecuteNonQuery()
                        Try
                            cmd = New SqlCommand(sSql1, conS)
                            cmd.ExecuteNonQuery()
                        Catch ex As Exception

                        End Try
                        If conS.State <> ConnectionState.Closed Then
                            conS.Close()
                        End If
                    End If

                    If Common.IsParallel = "Y" Then
                        Dim parallelThread As Thread = New Thread(Sub() Common.parallelInsert(EnrollNumber, Paycode, Convert.ToDateTime(atttime.ToString), IN_OUT, "N", Name, deviceId))
                        parallelThread.Start()
                        parallelThread.IsBackground = True
                    End If

                    'sms
                    If Common.g_SMSApplicable = "" Then
                        Common.Load_SMS_Policy()
                    End If
                    If Common.g_SMSApplicable = "Y" Then
                        If Common.g_isAllSMS = "Y" Then
                            Try
                                If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then

                                    Dim SmsContent As String = ""
                                    If Common.g_DeviceWiseInOut = "Y" Then
                                        If IN_OUT = "I" Then
                                            SmsContent = Common.g_InContent1 & " " & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd HH:mm") & " " & Common.g_InContent2
                                        ElseIf IN_OUT = "O" Then
                                            SmsContent = Common.g_OutContent1 & " " & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd HH:mm") & " " & Common.g_OutContent2
                                        End If
                                    Else
                                        SmsContent = Common.g_AllContent1 & " " & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd HH:mm") & " " & Common.g_AllContent2
                                    End If
                                    SmsContent = Name & " " & SmsContent
                                    Cn.sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                                End If
                            Catch ex As Exception

                            End Try
                        End If
                    End If
                    'sms end
                Catch ex As Exception

                End Try
                SetText(EnrNum, Paycode, Name, verifymode, Convert.ToDateTime(atttime.ToString), machine, deviceId, LEDIP, EId)
            Else
                SetText(EnrNum, "", "Not registered", verifymode, Convert.ToDateTime(atttime.ToString), machine, deviceId, LEDIP, EId)
            End If
        Catch ex As Exception
            '_Log.Error(ex.ToString)
        End Try

        Return 0
    End Function
    'End TW-IR102 iris B100
    Private Sub TimerBioEco_Tick(sender As System.Object, e As System.EventArgs) Handles TimerBioEco.Tick
        Dim ret As Int32 = CType(SyLastError.sleSuss, Int32)
        While (ret = CType(SyLastError.sleSuss, Int32))
            Dim devId As UInt32 = 0
            Dim der As SyLogRecordEX = New SyLogRecordEX
            ret = SyFunctions.CmdExtractLastDailyEntry(der, devId)
            If (ret = CType(SyLastError.sleSuss, Int32)) Then
                If ((der.wUserID = 0) Or (der.wUserID > 65534)) Then
                    Continue While
                End If

                Dim sec As Byte = 0
                Dim min As Byte = 0
                Dim hour As Byte = 0
                Dim date1 As Byte = 0
                Dim month As Byte = 0
                Dim year As UInt16 = 0
                Dim kind As Byte = 0
                Dim hasCooperated As Byte = 0
                Dim hasSetup As Byte = 0
                Dim hasOpenDoor As Byte = 0
                Dim hasAlarm As Byte = 0
                Dim operKind As String = ""
                SyFunctions.DecodeSyDailyEntryType(der.cLogType, kind, hasCooperated, hasSetup, hasOpenDoor, hasAlarm)
                SyFunctions.DecodeSyDailyEntryTime(der.sRecordTime, sec, min, hour, date1, month, year)
                Dim dt As DateTime = New DateTime(year, month, date1, hour, min, sec)
                devId = (devId And 65535).ToString
                Me.funcShowGeneralLogDataToGrid(der.wUserID, kind, 0, dt, True, "", 0, devId, "", "", "Bio1Eco")
            End If
        End While
    End Sub

    Private Sub AxFPCLOCK_Svr1_OnReceiveGLogData(sender As Object, e As AxFPCLOCK_SVRLib._DFPCLOCK_SvrEvents_OnReceiveGLogDataEvent) Handles AxFPCLOCK_Svr1.OnReceiveGLogData
        Try
            Dim nResult As Integer = 1
            Dim EnrollNo As String = e.anSEnrollNumber.ToString.Trim
            Dim LogDate As DateTime = Convert.ToDateTime(e.anLogDate.ToString.Trim)
            Dim DeviceIP As String = e.astrDeviceIP.ToString.Trim.Replace(" ", "")
            Dim DeviceID As String = e.vnDeviceID.ToString.Trim
            Dim InOut As String = e.anInOutMode.ToString.Trim
            Dim VefiryMode As String = e.anVerifyMode.ToString.Trim
            Dim VMode As Long = 0
            Me.AxFPCLOCK_Svr1.SendResultandTime(e.linkindex, e.vnDeviceID, e.anSEnrollNumber, nResult)
            If e.anSEnrollNumber = 0 Or e.anSEnrollNumber.ToString = "" Then
                Exit Sub
            End If
            'Select Case (VefiryMode)
            '    Case CType(enumGLogVerifyMode.LOG_FPVERIFY, String)
            '        '1
            '        strTmp = "Fp"
            '    Case CType(enumGLogVerifyMode.LOG_PASSVERIFY, Integer)
            '        '2
            '        strTmp = "Password"
            '    Case CType(enumGLogVerifyMode.LOG_CARDVERIFY, Integer)
            '        '3
            '        strTmp = "Card"
            '    Case CType(enumGLogVerifyMode.LOG_FPPASS_VERIFY, Integer)
            '        '4
            '        strTmp = "Fp+Password"
            '    Case CType(enumGLogVerifyMode.LOG_FPCARD_VERIFY, Integer)
            '        '5
            '        strTmp = "Fp+Card"
            '    Case CType(enumGLogVerifyMode.LOG_PASSFP_VERIFY, Integer)
            '        '6
            '        strTmp = "Password+Fp"
            '    Case CType(enumGLogVerifyMode.LOG_CARDFP_VERIFY, Integer)
            '        '7
            '        strTmp = "Card+Fp"
            '    Case CType(enumGLogVerifyMode.LOG_FACEVERIFY, Integer)
            '        '20
            '        strTmp = "Face"
            '    Case CType(enumGLogVerifyMode.LOG_FACECARDVERIFY, Integer)
            '        '21
            '        strTmp = "Face+Card"
            '    Case CType(enumGLogVerifyMode.LOG_FACEPASSVERIFY, Integer)
            '        '22
            '        strTmp = "Face+Pass"
            '    Case CType(enumGLogVerifyMode.LOG_PASSFACEVERIFY, Integer)
            '        '21
            '        strTmp = "Pass+Face"
            '    Case CType(enumGLogVerifyMode.LOG_CARDFACEVERIFY, Integer)
            '        '22
            '        strTmp = "Card+Face"
            '    Case Else
            '        strTmp = GetStringVerifyMode(CType(aVerifyMode, Integer))
            'End Select
            'VerifyMode = strTmp
            funcShowGeneralLogDataToGrid(EnrollNo, VefiryMode, InOut, LogDate, False, DeviceIP, TF01Port, DeviceID, "", "", "TF-01")




        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ZKAccessGetData()
        Try
            Dim ret As Integer = 0, i As Integer = 0, buffersize As Integer = 256
            Dim str As String = ""
            Dim tmp() As String = Nothing
            Dim buffer(255) As Byte
            If IntPtr.Zero <> h Then
                ret = ZKController.GetRTLog(h, buffer(0), buffersize)
                If ret > 0 Then
                    str = Encoding.Default.GetString(buffer)
                    tmp = str.Split(","c)

                End If

            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub TimerZK_Tick(sender As Object, e As EventArgs) Handles TimerZK.Tick
        ZKAccessGetData()
    End Sub
End Class
