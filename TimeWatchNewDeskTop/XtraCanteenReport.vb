﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraSplashScreen
Imports System.Runtime.CompilerServices
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel
Imports iAS.HorizontalMerging
Imports DevExpress.Export
Imports DevExpress.XtraPrinting
Imports DevExpress.Printing.ExportHelpers
Imports DevExpress.Export.Xl

Public Class XtraCanteenReport
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim WhichReport As Integer
    Dim g_WhereClause As String
    Dim g_SkipAfterDept As Boolean
    Dim g_LinesPerPage As Integer
    Dim mStrProcName As String
    Dim mstrDepartmentCode As String
    Dim mintPageNo As Integer
    Dim mintLine As Integer
    Dim mFileNumber As Integer
    Dim mblnCheckReport As Boolean
    Dim mlngStartingNoOfDays As Long
    Dim mstrFile_Name As String
    Dim mShift As String
    Dim g_MachineRawPunch As String = "admin"
    Dim mCode As String
    Dim g_RepMealtype As String '= "A"
    Private ViewHandler As MyGridViewHandler = Nothing
    'Public Shared Common.tbl As New Data.DataTable()

    Public Sub New()
        InitializeComponent()
         ViewHandler = New MyGridViewHandler(GridView1)
    End Sub
    Private Sub XtraCanteenReport_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        DateEdit1.DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
        DateEdit2.EditValue = Now
        If Common.servername = "Access" Then
            Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            GridControlComp.DataSource = SSSDBDataSet.tblCompany1

            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee1

            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment1

            Me.TblCatagory1TableAdapter1.Fill(Me.SSSDBDataSet.tblCatagory1)
            GridControlCat.DataSource = SSSDBDataSet.tblCatagory1

            Me.TblShiftMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblShiftMaster1)
            GridControlShift.DataSource = SSSDBDataSet.tblShiftMaster1

            Me.TblGrade1TableAdapter1.Fill(Me.SSSDBDataSet.tblGrade1)
            GridControlGrade.DataSource = SSSDBDataSet.tblGrade1

            'Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
            'GridControlBranch.DataSource = SSSDBDataSet.tblbranch1
        Else
            TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            GridControlComp.DataSource = SSSDBDataSet.tblCompany

            'TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee

            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment

            TblCatagoryTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblCatagoryTableAdapter.Fill(Me.SSSDBDataSet.tblCatagory)
            GridControlCat.DataSource = SSSDBDataSet.tblCatagory

            TblShiftMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblShiftMasterTableAdapter.Fill(Me.SSSDBDataSet.tblShiftMaster)
            GridControlShift.DataSource = SSSDBDataSet.tblShiftMaster

            TblGradeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblGradeTableAdapter.Fill(Me.SSSDBDataSet.tblGrade)
            GridControlGrade.DataSource = SSSDBDataSet.tblGrade

            'TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
            'GridControlBranch.DataSource = SSSDBDataSet.tblbranch
        End If
        GridControlEmp.DataSource = Common.EmpNonAdmin
        GridControlBranch.DataSource = Common.LocationNonAdmin
        Common.SetGridFont(GridViewComp, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewEmp, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewCat, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewDept, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewGrade, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewShift, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewBranch, New System.Drawing.Font("Tahoma", 10))
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If DateEdit2.DateTime > DateEdit1.DateTime.AddMonths(1) Then
            XtraMessageBox.Show(ulf, "<size=10>Date difference cannot be more than one month</size>", "<size=9>Error</size>")
            Exit Sub
        End If
      
        g_LinesPerPage = TextEdit1.Text
        If CheckDeptSkip.Checked = True Then
            g_SkipAfterDept = True
        Else
            g_SkipAfterDept = False
        End If
        If SidePanelSelection.Visible = False Then
            g_WhereClause = ""
        End If
        Set_Filter_Crystal()
        If CheckEditBF.Checked = True Then
            g_RepMealtype = "B"
        ElseIf CheckEditD.Checked = True Then
            g_RepMealtype = "D"
        ElseIf CheckEditL.Checked = True Then
            g_RepMealtype = "L"
        Else
            g_RepMealtype = "A"
        End If
        'If Trim(g_WhereClause) = "" Then
        '    g_WhereClause = gcompany & " and " & gdepartment
        'Else
        '    g_WhereClause = g_WhereClause & "and " & gcompany & " and " & gdepartment
        'End If
        If CheckEdit5.Checked = True Then
            If CheckText.Checked = True Then
                Monthly_Consumption("")
                mStrProcName = "Monthly_Performance"
            Else
                'Xl_MonthlyConsumption("")
                Xl_MonthlyConsumptionGrid("")
                mStrProcName = "MonthlyXl_Performance"
            End If
        ElseIf CheckEdit4.Checked = True Then
            If CheckText.Checked = True Then
                Monthly_Consumption_Date("")
                mStrProcName = "Monthly_LateArrivalRegister "
            Else
                'XL_Monthly_Consumption_Date("")
                XL_Monthly_Consumption_DateGrid("")
                mblnCheckReport = False
                'mReportPrintStatus = False
            End If
        ElseIf CheckEdit3.Checked = True Then
            If CheckText.Checked = True Then
                Monthly_Consumption_Two("")
                mStrProcName = "Monthly_EarlyDepartureRegister"
            Else
                'XL_Monthly_Consumption_Two("")
                XL_Monthly_Consumption_TwoGrid("")
                mblnCheckReport = False
                'mReportPrintStatus = False
            End If
        ElseIf CheckEditWorkCode.Checked = True Then
            XL_Monthly_WorkCodeWiseGrid("")
        End If

    End Sub
    Sub Monthly_Consumption(ByVal strsortorder As String)
        Dim strDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strPayCode As String
        Dim strEmpName As String
        Dim strPresentCardNo As String
        Dim dblPresentValue As Double
        Dim dblAbsentValue As Double
        Dim dblLeaveValue As Double
        Dim dblWo_Value As Double
        Dim dblOtDuration As Double
        Dim dblOtAmount As Double
        Dim mFirst As Boolean
        Dim dblHoliday_Value As Double
        Dim mCount As Integer

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1
        If g_RepMealtype = "A" Then
            g_RepMealtype = ""
        Else
            g_RepMealtype = "AND PURPOSE= '" & g_RepMealtype & "'"
        End If
        
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        'If mPurpose <> "" Then
        '    mPurpose = " AND PURPOSE='" & mPurpose & "'"
        'End If
        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        'If strsortorder = "" Then
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "# AND #" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "# " & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        'Rs_Report = Cn.Execute(strsql)
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.<size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        'Rs_Report.Sort = "PayCode,OFFICEPUNCH"
        'Else
        '    'Rs_Report.Sort = "location," & strSortOrder & ", OFFICEPUNCH"
        '    Rs_Report.Sort = strsortorder & ", OFFICEPUNCH"
        'End If

        intFile = FreeFile()

        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

        'With Rs_Report
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
                'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then  'Or mLocation <> Rs_Report!Location
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) & Space(40), CommonReport.g_CompanyNames)
                objWriter.WriteLine()
                objWriter.WriteLine("Page No." & mintPageNo)
                objWriter.WriteLine("                                               Run Date & Time : " & Now.ToString("dd/MM/yyyy HH:mm"))

                objWriter.WriteLine("                  Meals Consumption  Employee Wise from " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy"))
                objWriter.WriteLine("-------------------------------------------------------------------------------------------------")
                objWriter.WriteLine("Sl.  Payroll      Card       Employee Name             Breakfast   Lunch  Dinner   Total        ")
                objWriter.WriteLine("No.               No.                                                                                 ")
                objWriter.WriteLine("--------------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If
            'mLocation = Rs_Report!Location
            strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            strEmpName = Rs_Report.Tables(0).Rows(i).Item("empname").ToString.Trim
            strPresentCardNo = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            dblPresentValue = 0
            dblAbsentValue = 0
            dblLeaveValue = 0
            dblWo_Value = 0
            dblOtDuration = 0
            dblOtAmount = 0
            dblHoliday_Value = 0
            Do While strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                If Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "L" Then
                    dblPresentValue = dblPresentValue + 1
                ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "D" Then
                    dblAbsentValue = dblAbsentValue + 1
                ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "B" Then
                    dblWo_Value = dblWo_Value + 1
                End If
                '.MoveNext()
                'If .EOF Then
                '    Exit Do
                'End If
                i = i + 1
                If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                    GoTo tmp
                    Exit Do
                End If
            Loop
            i = i - 1
tmp:        mCount = mCount + 1
            objWriter.WriteLine(mCount.ToString("000") & "  " & strPayCode.Trim.PadRight(12) & Space(1) & strPresentCardNo.Trim.PadRight(12) & Space(1) & strEmpName.Trim.PadRight(25) & Space(1) & _
                dblWo_Value.ToString("000") & Space(9) & dblPresentValue.ToString("000") & Space(4) & _
                dblAbsentValue.ToString("000") & Space(6) & (dblPresentValue + dblAbsentValue + dblWo_Value).ToString("0000") & Space(5)) '& Length7((dblPresentValue + dblAbsentValue + dblWo_Value) * 10))
            mintLine = mintLine + 1
            mblnCheckReport = True
        Next
        '    Loop
        'End With
        objWriter.Close()
        Try
            Process.Start(mstrFile_Name)
        Catch ex As Exception
            Process.Start("notepad.exe", mstrFile_Name)
        End Try
    End Sub
    Sub Xl_MonthlyConsumption(strsortorder As String)
        Dim strsql As String
        Dim strPayCode As String
        Dim strEmpName As String
        Dim strPresentCardNo As String
        Dim dblPresentValue As Double
        Dim dblAbsentValue As Double
        Dim dblLeaveValue As Double
        Dim dblWo_Value As Double
        Dim dblOtDuration As Double
        Dim dblOtAmount As Double
        Dim dblHoliday_Value As Double
        Dim mCount As Integer


        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets

        Dim v_Late As String
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add

        'If g_SortOrder <> "" Then
        '    strsortorder = g_SortOrder
        'End If
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False

        If g_RepMealtype = "A" Then
            g_RepMealtype = ""
        Else
            g_RepMealtype = "AND PURPOSE= '" & g_RepMealtype & "'"
        End If

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "# AND #" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "# " & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        'Rs_Report = Cn.Execute(strsql)
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.<size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        mCount = 0
        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.ColumnWidth = 10
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Format(Now(), "dd/mm/yyyy HH:MM")
        rowcnt = rowcnt + 2
        xlapp.Cells.Font.Size = 10
        xlapp.Cells(rowcnt, 4) = "Meals Consumption  Employee Wise from : " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 1) = "Sr.No"
        xlapp.Cells(rowcnt, 2) = "PayRoll"
        xlapp.Cells(rowcnt, 3) = "Card No"
        xlapp.Cells(rowcnt, 4) = "Employee Name"
        xlapp.Cells(rowcnt, 6) = "BreakFast  "
        xlapp.Cells(rowcnt, 7) = "Lunch "
        xlapp.Cells(rowcnt, 8) = "Dinner "
        xlapp.Cells(rowcnt, 9) = "Total "
        rowcnt = rowcnt + 2

        'With Rs_Report
        '    .MoveFirst()
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
            '    If mstrDepartmentCode <> !DepartmentCode Then
            '        rowcnt = rowcnt + 2
            '        xlapp.Cells(rowcnt, 1) = "*** Department Code & Name " & !DepartmentCode & " " & !DepartmentName
            '        mstrDepartmentCode = !DepartmentCode
            '        rowcnt = rowcnt + 1
            '    End If
            'End If
            'If Left(strsortorder, 8) = "Division" Or frmSorting.optSectionPaycode.Value Then
            '    If mstrDepartmentCode <> !DivisionCode Then
            '        rowcnt = rowcnt + 2
            '        xlapp.Cells(rowcnt, 1) = "*** Section Code & Name " & !DivisionCode & " " & !DivisionName
            '        mstrDepartmentCode = !DivisionCode
            '        rowcnt = rowcnt + 1
            '    End If
            'End If
            'If Left(strsortorder, 3) = "Cat" Or frmSorting.optCatPaycode.Value Then
            '    If mstrDepartmentCode <> !Cat Then
            '        rowcnt = rowcnt + 2

            '        xlapp.Cells(rowcnt, 1) = "*** Category Code & Name " & !Cat & " " & !CatagoryName
            '        mstrDepartmentCode = !Cat
            '        rowcnt = rowcnt + 1
            '    End If
            'End If
            mCount = mCount + 1

            strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            strEmpName = Rs_Report.Tables(0).Rows(i).Item("empname").ToString.Trim
            strPresentCardNo = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            dblPresentValue = 0
            dblAbsentValue = 0
            dblLeaveValue = 0
            dblWo_Value = 0
            dblOtDuration = 0
            dblOtAmount = 0
            dblHoliday_Value = 0
            Do While strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                If Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "L" Then
                    dblPresentValue = dblPresentValue + 1
                ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "D" Then
                    dblAbsentValue = dblAbsentValue + 1
                ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "B" Then
                    dblWo_Value = dblWo_Value + 1
                End If
                '.MoveNext()
                'If .EOF Then
                '    Exit Do
                'End If
                i = i + 1
                If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                    GoTo tmp
                    Exit Do
                End If
            Loop
            i = i - 1
tmp:        xlapp.Cells(rowcnt, 1) = mCount.ToString("000")
            xlapp.Cells(rowcnt, 2) = "'" & strPayCode
            xlapp.Cells(rowcnt, 3) = "'" & strPresentCardNo
            xlapp.Cells(rowcnt, 4) = strEmpName
            xlapp.Cells(rowcnt, 6) = dblWo_Value.ToString("000")
            xlapp.Cells(rowcnt, 7) = dblPresentValue.ToString("000")
            xlapp.Cells(rowcnt, 8) = dblAbsentValue.ToString("000")
            xlapp.Cells(rowcnt, 9) = (dblPresentValue + dblAbsentValue + dblWo_Value).ToString("0000")

            'mCount = mCount + 1
            rowcnt = rowcnt + 1
            mblnCheckReport = False
            '.MoveNext
            '    Loop
            'End With
        Next
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub Xl_MonthlyConsumptionGrid(strsortorder As String)
        Dim strsql As String
        Dim strPayCode As String
        Dim strEmpName As String
        Dim strPresentCardNo As String
        Dim dblPresentValue As Double
        Dim dblAbsentValue As Double
        Dim dblLeaveValue As Double
        Dim dblWo_Value As Double
        Dim dblOtDuration As Double
        Dim dblOtAmount As Double
        Dim dblHoliday_Value As Double
        Dim mCount As Integer

        Dim rowcnt As Integer
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"
        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False

        If g_RepMealtype = "A" Then
            g_RepMealtype = ""
        Else
            g_RepMealtype = "AND PURPOSE= '" & g_RepMealtype & "'"
        End If

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "# AND #" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "# " & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        'Rs_Report = Cn.Execute(strsql)
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.<size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        mCount = 0
        rowcnt = 1

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayRoll", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("BreakFast", GetType(String))
        Common.tbl.Columns.Add("Lunch", GetType(String))
        Common.tbl.Columns.Add("Dinner", GetType(String))
        Common.tbl.Columns.Add("Total", GetType(String))

        Common.frodatetodatetoReportGrid = "Meals Consumption  Employee Wise from : " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
            '    If mstrDepartmentCode <> !DepartmentCode Then
            '        rowcnt = rowcnt + 2
            '        xlapp.Cells(rowcnt, 1) = "*** Department Code & Name " & !DepartmentCode & " " & !DepartmentName
            '        mstrDepartmentCode = !DepartmentCode
            '        rowcnt = rowcnt + 1
            '    End If
            'End If
            'If Left(strsortorder, 8) = "Division" Or frmSorting.optSectionPaycode.Value Then
            '    If mstrDepartmentCode <> !DivisionCode Then
            '        rowcnt = rowcnt + 2
            '        xlapp.Cells(rowcnt, 1) = "*** Section Code & Name " & !DivisionCode & " " & !DivisionName
            '        mstrDepartmentCode = !DivisionCode
            '        rowcnt = rowcnt + 1
            '    End If
            'End If
            'If Left(strsortorder, 3) = "Cat" Or frmSorting.optCatPaycode.Value Then
            '    If mstrDepartmentCode <> !Cat Then
            '        rowcnt = rowcnt + 2

            '        xlapp.Cells(rowcnt, 1) = "*** Category Code & Name " & !Cat & " " & !CatagoryName
            '        mstrDepartmentCode = !Cat
            '        rowcnt = rowcnt + 1
            '    End If
            'End If
            mCount = mCount + 1

            strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            strEmpName = Rs_Report.Tables(0).Rows(i).Item("empname").ToString.Trim
            strPresentCardNo = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            dblPresentValue = 0
            dblAbsentValue = 0
            dblLeaveValue = 0
            dblWo_Value = 0
            dblOtDuration = 0
            dblOtAmount = 0
            dblHoliday_Value = 0
            Do While strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                If Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "L" Then
                    dblPresentValue = dblPresentValue + 1
                ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "D" Then
                    dblAbsentValue = dblAbsentValue + 1
                ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "B" Then
                    dblWo_Value = dblWo_Value + 1
                End If
                '.MoveNext()
                'If .EOF Then
                '    Exit Do
                'End If
                i = i + 1
                If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                    GoTo tmp
                    Exit Do
                End If
            Loop
            i = i - 1

tmp:        Common.tbl.Rows.Add(mCount.ToString("000"), strPayCode, strPresentCardNo, strEmpName, "", dblWo_Value.ToString("000"), dblPresentValue.ToString("000"), dblAbsentValue.ToString("000"), (dblPresentValue + dblAbsentValue + dblWo_Value).ToString("0000"))
            'mCount = mCount + 1
            rowcnt = rowcnt + 1
            mblnCheckReport = False
            '.MoveNext
            '    Loop
            'End With
        Next

        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub Monthly_Consumption_Date(ByVal strsortorder As String)
        Dim strDeptDivCode As String
        Dim strsql As String
        Dim strPayCode As String
        Dim strEmpName As String
        Dim strPresentCardNo As String
        Dim dblPresentValue As Double
        Dim dblAbsentValue As Double
        Dim dblLeaveValue As Double
        Dim dblWo_Value As Double
        Dim dblOtDuration As Double
        Dim dblOtAmount As Double
        Dim mFirst As Boolean
        Dim dblHoliday_Value As Double
        Dim mCount As Integer
        Dim mDate As Object
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1
        If g_RepMealtype = "A" Then
            g_RepMealtype = ""
        Else
            g_RepMealtype = "AND PURPOSE= '" & g_RepMealtype & "'"
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        'If mPurpose <> "" Then
        '    mPurpose = " AND PURPOSE='" & mPurpose & "'"
        'End If
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                     " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                     " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "# AND #" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "#" & _
                     " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                     " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                     " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "'" & _
                     " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.<size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
                'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then  'Or mLocation <> Rs_Report!Location
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) + Space(40), CommonReport.g_CompanyNames)
                objWriter.WriteLine()
                objWriter.WriteLine("Page No." & mintPageNo)
                objWriter.WriteLine("                                               Run Date & Time : " & Now.ToString("dd/MM/yyyy HH:mm"))

                objWriter.WriteLine("                  Meals Consumption  Employee Wise from " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy"))

                objWriter.WriteLine("-----------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine("Sl.  Date         Paycode      Card         Employee Name             Breakfast   Lunch  Dinner   Total             ")
                objWriter.WriteLine("No.                            No.                                                                             ")
                objWriter.WriteLine("---------------------------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If
            'mLocation = Rs_Report!Location
            strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            strEmpName = Rs_Report.Tables(0).Rows(i).Item("empname").ToString.Trim
            strPresentCardNo = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim

            Do While strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                dblPresentValue = 0
                dblAbsentValue = 0
                dblLeaveValue = 0
                dblWo_Value = 0
                dblOtDuration = 0
                dblOtAmount = 0
                dblHoliday_Value = 0
                mDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")

                Do While mDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                    If Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "L" Then
                        dblPresentValue = dblPresentValue + 1
                    ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "D" Then
                        dblAbsentValue = dblAbsentValue + 1
                    ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "B" Then
                        dblWo_Value = dblWo_Value + 1
                    End If
                    '.MoveNext()
                    'If .EOF Then
                    '    Exit Do
                    'End If
                    i = i + 1
                    If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                        GoTo tmp
                        Exit Do
                    End If
                    If strPayCode <> Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim Then Exit Do
                Loop
                'i = i - 1

tmp:            mCount = mCount + 1
                objWriter.WriteLine(mCount.ToString("000") & "  " & mDate & "   " & strPayCode.Trim.PadRight(12) & Space(1) & strPresentCardNo.Trim.PadRight(12) & Space(1) & strEmpName.Trim.PadRight(25) & Space(1) & _
                     dblWo_Value.ToString("000") & Space(9) & dblPresentValue.ToString("000") & Space(4) & _
                     dblAbsentValue.ToString("000") & Space(6) & ((dblPresentValue + dblAbsentValue + dblWo_Value).ToString("0000")) & Space(5)) '& Length7((dblPresentValue + dblAbsentValue + dblWo_Value) * 10))
                If i >= Rs_Report.Tables(0).Rows.Count - 1 Then
                    Exit Do
                End If
                'If .EOF Then Exit Do
            Loop
            mintLine = mintLine + 1
            mblnCheckReport = True
        Next ' Loop
        objWriter.Close()
        Try
            Process.Start(mstrFile_Name)
        Catch ex As Exception
            Process.Start("notepad.exe", mstrFile_Name)
        End Try
    End Sub
    Sub XL_Monthly_Consumption_Date(strsortorder As String)
        Dim strsql As String
        Dim strPayCode As String
        Dim strEmpName As String
        Dim strPresentCardNo As String
        Dim dblPresentValue As Double
        Dim dblAbsentValue As Double
        Dim dblLeaveValue As Double
        Dim dblWo_Value As Double
        Dim dblOtDuration As Double
        Dim dblOtAmount As Double
        Dim dblHoliday_Value As Double
        Dim mCount As Integer
        Dim mDate As String

        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets

        Dim v_Late As String
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False

        If g_RepMealtype = "A" Then
            g_RepMealtype = ""
        Else
            g_RepMealtype = "AND PURPOSE= '" & g_RepMealtype & "'"
        End If
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                     " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                     " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "# AND #" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "#" & _
                     " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                     " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                     " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "'" & _
                     " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        mCount = 1
        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.ColumnWidth = 10
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Format(Now(), "dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells.Font.Size = 10
        xlapp.Cells(rowcnt, 4) = "Meals Consumption  Employee Wise from : " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 1) = "Sr.No"
        xlapp.Cells(rowcnt, 2) = "Date"
        xlapp.Cells(rowcnt, 3) = "PayRoll"
        xlapp.Cells(rowcnt, 4) = "Card No"
        xlapp.Cells(rowcnt, 5) = "Employee Name"
        xlapp.Cells(rowcnt, 7) = "BreakFast  "
        xlapp.Cells(rowcnt, 8) = "Lunch "
        xlapp.Cells(rowcnt, 9) = "Dinner "
        xlapp.Cells(rowcnt, 10) = "Total "
        rowcnt = rowcnt + 2

        'With Rs_Report
        '    .MoveFirst()
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
            '    If mstrDepartmentCode <> !DepartmentCode Then
            '        rowcnt = rowcnt + 2
            '        xlapp.Cells(rowcnt, 1) = "*** Department Code & Name " & !DepartmentCode & " " & !DepartmentName
            '        mstrDepartmentCode = !DepartmentCode
            '        rowcnt = rowcnt + 1
            '    End If
            'End If
            'If Left(strsortorder, 8) = "Division" Or frmSorting.optSectionPaycode.Value Then
            '    If mstrDepartmentCode <> !DivisionCode Then
            '        rowcnt = rowcnt + 2
            '        xlapp.Cells(rowcnt, 1) = "*** Section Code & Name " & !DivisionCode & " " & !DivisionName
            '        mstrDepartmentCode = !DivisionCode
            '        rowcnt = rowcnt + 1
            '    End If
            'End If
            'If Left(strsortorder, 3) = "Cat" Or frmSorting.optCatPaycode.Value Then
            '    If mstrDepartmentCode <> !Cat Then
            '        rowcnt = rowcnt + 2

            '        xlapp.Cells(rowcnt, 1) = "*** Category Code & Name " & !Cat & " " & !CatagoryName
            '        mstrDepartmentCode = !Cat
            '        rowcnt = rowcnt + 1
            '    End If
            'End If
            ''mCount = mCount + 1

            strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            strEmpName = Rs_Report.Tables(0).Rows(i).Item("empname").ToString.Trim
            strPresentCardNo = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            dblPresentValue = 0
            dblAbsentValue = 0
            dblLeaveValue = 0
            dblWo_Value = 0
            dblOtDuration = 0
            dblOtAmount = 0
            dblHoliday_Value = 0

            Do While strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                dblPresentValue = 0
                dblAbsentValue = 0
                dblLeaveValue = 0
                dblWo_Value = 0
                dblOtDuration = 0
                dblOtAmount = 0
                dblHoliday_Value = 0
                mDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                Do While mDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                    If Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "L" Then
                        dblPresentValue = dblPresentValue + 1
                    ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "D" Then
                        dblAbsentValue = dblAbsentValue + 1
                    ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "B" Then
                        dblWo_Value = dblWo_Value + 1
                    End If
                    '.MoveNext()
                    'If .EOF Then
                    '    Exit Do
                    'End If
                    i = i + 1
                    If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                        GoTo tmp
                        Exit Do
                    End If
                    If strPayCode <> Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim Then Exit Do
                Loop

                'Print #intFile, Format(mCount, "000") & "  " & mDate & "   " & strPayCode & Space(1) & strPresentCardNo & Space(1) & strEmpName & Space(1) & _
                'Format(dblWo_Value, "!@@@") & Space(9) & Format(dblPresentValue, "!@@@") & Space(4) & _
                ' Format(dblAbsentValue, "!@@@") & Space(6) & Format((dblPresentValue + dblAbsentValue + dblWo_Value), "!@@@@") & Space(5) '& Length7((dblPresentValue + dblAbsentValue + dblWo_Value) * 10)



tmp:            xlapp.Cells(rowcnt, 1) = mCount.ToString("000")
                xlapp.Cells(rowcnt, 2) = mDate
                xlapp.Cells(rowcnt, 3) = strPayCode
                xlapp.Cells(rowcnt, 4) = strPresentCardNo
                xlapp.Cells(rowcnt, 5) = strEmpName
                xlapp.Cells(rowcnt, 7) = dblWo_Value.ToString("000")
                xlapp.Cells(rowcnt, 8) = dblPresentValue.ToString("000")
                xlapp.Cells(rowcnt, 9) = dblAbsentValue.ToString("000")
                xlapp.Cells(rowcnt, 10) = (dblPresentValue + dblAbsentValue + dblWo_Value).ToString("0000")

                mCount = mCount + 1
                rowcnt = rowcnt + 1
                mblnCheckReport = False
                If i >= Rs_Report.Tables(0).Rows.Count - 1 Then
                    Exit Do
                End If
            Loop
            '.MoveNext
        Next '  Loop
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub XL_Monthly_Consumption_DateGrid(strsortorder As String)
        Dim strsql As String
        Dim strPayCode As String
        Dim strEmpName As String
        Dim strPresentCardNo As String
        Dim dblPresentValue As Double
        Dim dblAbsentValue As Double
        Dim dblLeaveValue As Double
        Dim dblWo_Value As Double
        Dim dblOtDuration As Double
        Dim dblOtAmount As Double
        Dim dblHoliday_Value As Double
        Dim mCount As Integer
        Dim mDate As String
        Dim rowcnt As Integer
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False

        If g_RepMealtype = "A" Then
            g_RepMealtype = ""
        Else
            g_RepMealtype = "AND PURPOSE= '" & g_RepMealtype & "'"
        End If
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                     " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                     " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "# AND #" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "#" & _
                     " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                     " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                     " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "'" & _
                     " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        mCount = 1
        rowcnt = 1
        Common.frodatetodatetoReportGrid = "Meals Consumption  Employee Wise from : " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("Date", GetType(String))
        Common.tbl.Columns.Add("Paycode", GetType(String))
        Common.tbl.Columns.Add("Card No", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("BreakFast", GetType(String))
        Common.tbl.Columns.Add("Lunch", GetType(String))
        Common.tbl.Columns.Add("Dinner", GetType(String))
        Common.tbl.Columns.Add("Total", GetType(String))


        rowcnt = rowcnt + 2
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            strEmpName = Rs_Report.Tables(0).Rows(i).Item("empname").ToString.Trim
            strPresentCardNo = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            dblPresentValue = 0
            dblAbsentValue = 0
            dblLeaveValue = 0
            dblWo_Value = 0
            dblOtDuration = 0
            dblOtAmount = 0
            dblHoliday_Value = 0

            Do While strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                dblPresentValue = 0
                dblAbsentValue = 0
                dblLeaveValue = 0
                dblWo_Value = 0
                dblOtDuration = 0
                dblOtAmount = 0
                dblHoliday_Value = 0
                mDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                Do While mDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                    If Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "L" Then
                        dblPresentValue = dblPresentValue + 1
                    ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "D" Then
                        dblAbsentValue = dblAbsentValue + 1
                    ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "B" Then
                        dblWo_Value = dblWo_Value + 1
                    End If
                    '.MoveNext()
                    'If .EOF Then
                    '    Exit Do
                    'End If
                    i = i + 1
                    If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                        GoTo tmp
                        Exit Do
                    End If
                    If strPayCode <> Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim Then Exit Do
                Loop

tmp:            Common.tbl.Rows.Add(mCount.ToString("000"), mDate, strPayCode, strPresentCardNo, strEmpName, "", dblWo_Value.ToString("000"), dblPresentValue.ToString("000"), dblAbsentValue.ToString("000"), (dblPresentValue + dblAbsentValue + dblWo_Value).ToString("0000"))
                'xlapp.Cells(rowcnt, 1) = mCount.ToString("000")
                'xlapp.Cells(rowcnt, 2) = mDate
                'xlapp.Cells(rowcnt, 3) = strPayCode
                'xlapp.Cells(rowcnt, 4) = strPresentCardNo
                'xlapp.Cells(rowcnt, 5) = strEmpName
                'xlapp.Cells(rowcnt, 7) = dblWo_Value.ToString("000")
                'xlapp.Cells(rowcnt, 8) = dblPresentValue.ToString("000")
                'xlapp.Cells(rowcnt, 9) = dblAbsentValue.ToString("000")
                'xlapp.Cells(rowcnt, 10) = (dblPresentValue + dblAbsentValue + dblWo_Value).ToString("0000")

                mCount = mCount + 1
                rowcnt = rowcnt + 1
                mblnCheckReport = False
                If i >= Rs_Report.Tables(0).Rows.Count - 1 Then
                    Exit Do
                End If
            Loop
            '.MoveNext
        Next '  Loop
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub Monthly_Consumption_Two(ByVal strsortorder As String)
        Dim strDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strPayCode As String
        Dim strEmpName As String
        Dim strPresentCardNo As String
        Dim dblPresentValue As Double
        Dim dblAbsentValue As Double
        Dim dblLeaveValue As Double
        Dim dblWo_Value As Double
        Dim dblOtDuration As Double
        Dim dblOtAmount As Double
        Dim mFirst As Boolean
        Dim dblHoliday_Value As Double
        Dim mCount As Integer
        Dim CTR As Integer
        Dim mDate As Object
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1
        If g_RepMealtype = "A" Then
            g_RepMealtype = ""
        Else
            g_RepMealtype = "AND PURPOSE= '" & g_RepMealtype & "'"
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                     " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                     " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "# AND #" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "# " & _
                     " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                    " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                    " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "' " & _
                    " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                    " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.<size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        intFile = FreeFile()
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
                'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then  'Or mLocation <> Rs_Report!Location
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) + Space(40), CommonReport.g_CompanyNames)
                objWriter.WriteLine("")
                objWriter.WriteLine("Page No." & mintPageNo)
                objWriter.WriteLine("                                               Run Date & Time : " & Now.ToString("dd/MM/yyyy HH:mm"))

                objWriter.WriteLine("             More than one Meal Consumption  Employee Wise From Date : " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & "    To Date: " & DateEdit2.DateTime.ToString("dd/MM/yyyy"))
                objWriter.WriteLine("---------------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine("Sl.  Payroll      Card         Employee Name             Date       Breakfast   Lunch  Dinner   Total     Amount   ")
                objWriter.WriteLine("No.               No.                                                                                   (in Rupees) ")
                objWriter.WriteLine("--------------------------------------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If
            'mLocation = Rs_Report!Location
            strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            strEmpName = Rs_Report.Tables(0).Rows(i).Item("empname").ToString.Trim
            strPresentCardNo = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            Do While strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                mDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                dblPresentValue = 0
                dblAbsentValue = 0
                dblLeaveValue = 0
                dblWo_Value = 0
                dblOtDuration = 0
                dblOtAmount = 0
                dblHoliday_Value = 0
                CTR = 0
                Do While mDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                    If Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "L" Then
                        dblPresentValue = dblPresentValue + 1
                    ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "D" Then
                        dblAbsentValue = dblAbsentValue + 1
                    ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "B" Then
                        dblWo_Value = dblWo_Value + 1
                    End If
                    '.MoveNext()
                    'If .EOF Then
                    '    Exit Do
                    'End If
                    i = i + 1
                    If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                        GoTo tmp
                        Exit Do
                    End If
                    If strPayCode <> Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim Then Exit Do
                Loop

tmp:            If dblPresentValue + dblAbsentValue + dblWo_Value > 1 Then
                    mCount = mCount + 1
                    objWriter.WriteLine(mCount.ToString("000") & "  " & strPayCode.ToString.PadRight(12) & Space(1) & strPresentCardNo.ToString.PadRight(12) & Space(1) & strEmpName.Trim.PadRight(25) & Space(1) & mDate & " " & _
                         dblWo_Value.ToString("000") & Space(9) & dblPresentValue.ToString("000") & Space(4) & _
                         dblAbsentValue.ToString("000") & Space(6) & ((dblPresentValue + dblAbsentValue + dblWo_Value).ToString("0000")))
                    mintLine = mintLine + 1
                End If
                If i >= Rs_Report.Tables(0).Rows.Count - 1 Then
                    Exit Do
                End If
            Loop
        Next
        mblnCheckReport = True
        objWriter.Close()
        Try
            Process.Start(mstrFile_Name)
        Catch ex As Exception
            Process.Start("notepad.exe", mstrFile_Name)
        End Try
    End Sub
    Sub XL_Monthly_Consumption_TwoGrid(strsortorder As String)
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intPrintLineCounter As Integer
        Dim intFile As Integer
        Dim strsql As String
        Dim strPayCode As String
        Dim strEmpName As String
        Dim strPresentCardNo As String
        Dim strDepartmentCode As String
        Dim dblPresentValue As Double
        Dim dblAbsentValue As Double
        Dim dblLeaveValue As Double
        Dim dblWo_Value As Double
        Dim dblOtDuration As Double
        Dim dblOtAmount As Double
        Dim blnDeptAvailable As Boolean, mFirst As Boolean, TempWhereStr As String
        Dim dblHoliday_Value As Double
        Dim mLocation As String
        Dim mCount As Integer
        Dim mDate As String
        Dim CTR As Integer

        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets

        Dim v_Late As String
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add

        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False

        If g_RepMealtype = "A" Then
            g_RepMealtype = ""
        Else
            g_RepMealtype = "AND PURPOSE= '" & g_RepMealtype & "'"
        End If

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                     " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                     " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "# AND #" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "# " & _
                     " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo,tblcanteen.* " & _
                    " from tblCatagory,tblDivision,  tblcanteen,tblEmployee,tblCompany,tblDepartment" & _
                    " Where tblCatagory.Cat = tblEmployee.Cat And tblcanteen.CARDNO = tblEmployee.PRESENTCARDNO And tblcanteen.officepunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd 23:59:59") & "' " & _
                    " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode  " & _
                    " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " " & g_RepMealtype & " order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.<size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        mCount = 1
        rowcnt = 1

        Common.frodatetodatetoReportGrid = "Meals Consumption  Employee Wise from : " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        'xlapp.Cells(rowcnt, 1) = "Sr.No"
        'xlapp.Cells(rowcnt, 2) = "PayRoll"
        'xlapp.Cells(rowcnt, 3) = "Card No"
        'xlapp.Cells(rowcnt, 4) = "Employee Name"
        'xlapp.Cells(rowcnt, 6) = "Date"
        'xlapp.Cells(rowcnt, 7) = "BreakFast  "
        'xlapp.Cells(rowcnt, 8) = "Lunch "
        'xlapp.Cells(rowcnt, 9) = "Dinner "
        'xlapp.Cells(rowcnt, 10) = "Total "


        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Date", GetType(String))
        Common.tbl.Columns.Add("BreakFast", GetType(String))
        Common.tbl.Columns.Add("Lunch", GetType(String))
        Common.tbl.Columns.Add("Dinner", GetType(String))
        Common.tbl.Columns.Add("Total", GetType(String))

        rowcnt = rowcnt + 2

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            strEmpName = Rs_Report.Tables(0).Rows(i).Item("empname").ToString.Trim
            strPresentCardNo = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            dblPresentValue = 0
            dblAbsentValue = 0
            dblLeaveValue = 0
            dblWo_Value = 0
            dblOtDuration = 0
            dblOtAmount = 0
            dblHoliday_Value = 0

            Do While strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                dblPresentValue = 0
                dblAbsentValue = 0
                dblLeaveValue = 0
                dblWo_Value = 0
                dblOtDuration = 0
                dblOtAmount = 0
                dblHoliday_Value = 0
                CTR = 0
                mDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                Do While mDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                    If Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "L" Then
                        dblPresentValue = dblPresentValue + 1
                    ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "D" Then
                        dblAbsentValue = dblAbsentValue + 1
                    ElseIf Rs_Report.Tables(0).Rows(i).Item("purpose").ToString.Trim = "B" Then
                        dblWo_Value = dblWo_Value + 1
                    End If
                    '.MoveNext()
                    'If .EOF Then
                    '    Exit Do
                    'End If
                    i = i + 1
                    If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                        GoTo tmp
                        Exit Do
                    End If
                    If strPayCode <> Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim Then Exit Do
                Loop

tmp:            If dblPresentValue + dblAbsentValue + dblWo_Value > 1 Then
                    Common.tbl.Rows.Add(mCount.ToString("000"), strPayCode, strPresentCardNo, strEmpName, "", mDate, dblWo_Value.ToString("000"), dblPresentValue.ToString("000"), dblAbsentValue.ToString("000"), (dblPresentValue + dblAbsentValue + dblWo_Value).ToString("0000"))
                    'xlapp.Cells(rowcnt, 1) = Format(mCount, "000")
                    'xlapp.Cells(rowcnt, 2) = "'" & strPayCode
                    'xlapp.Cells(rowcnt, 3) = "'" & strPresentCardNo
                    'xlapp.Cells(rowcnt, 4) = strEmpName
                    'xlapp.Cells(rowcnt, 6) = "'" & mDate
                    'xlapp.Cells(rowcnt, 7) = Format(dblWo_Value, "!@@@")
                    'xlapp.Cells(rowcnt, 8) = Format(dblPresentValue, "!@@@")
                    'xlapp.Cells(rowcnt, 9) = Format(dblAbsentValue, "!@@@")
                    'xlapp.Cells(rowcnt, 10) = Format((dblPresentValue + dblAbsentValue + dblWo_Value), "!@@@@")
                End If
                mCount = mCount + 1
                rowcnt = rowcnt + 1
                mblnCheckReport = False
                If i >= Rs_Report.Tables(0).Rows.Count - 1 Then
                    Exit Do
                End If
            Loop
        Next '  Loop
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub XL_Monthly_WorkCodeWiseGrid(ByVal strsortorder As String)
        Dim strsql As String
        Dim strPayCode As String
        'Dim strEmpName As String
        'Dim strPresentCardNo As String
        'Dim dblPresentValue As Double
        'Dim dblAbsentValue As Double
        'Dim dblLeaveValue As Double
        'Dim dblWo_Value As Double
        'Dim dblOtDuration As Double
        'Dim dblOtAmount As Double
        'Dim dblHoliday_Value As Double
        Dim mCount As Integer
        'Dim mDate As String
        Dim CTR As Integer
        Dim dsWC As DataSet = New DataSet
        Dim rowcnt As Integer    

        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False

        If g_RepMealtype = "A" Then
            g_RepMealtype = ""
        Else
            g_RepMealtype = "AND PURPOSE= '" & g_RepMealtype & "'"
        End If

        strsql = "select * from TblCanteenWorkCode"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(dsWC)
        Else
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(dsWC)
        End If
        If dsWC.Tables(0).Rows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No WorkCode Available.<size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        For i As Integer = 0 To dsWC.Tables(0).Rows.Count - 1
            Common.tbl.Columns.Add(dsWC.Tables(0).Rows(i).Item("WorkCodeName").ToString.Trim, GetType(String))
        Next

        'If Rs_Report.Tables(0).Rows.Count < 1 Then
        '    XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.<size>", "<size=9>iAS</size>")
        '    Exit Sub
        'End If

        mCount = 1
        rowcnt = 1

        Common.frodatetodatetoReportGrid = "WorkCode Wise from : " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1

        Dim frmdate As DateTime = DateEdit1.DateTime
        Dim toDate As DateTime = DateEdit2.DateTime
        Dim rowCount As Integer = 0
        While frmdate <= toDate
            Common.tbl.Rows.Add(frmdate.ToString("dd/MM/yyyy"))
            rowCount = rowCount + 1
            Rs_Report = New DataSet
            If Common.servername = "Access" Then
                strsql = "SELECT tblCatagory.CatagoryName, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   " & _
                        "tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo, " & _
                        "tblcanteen.*, TblCanteenWorkCode.WorkCodeName  from TBLCANTEEN, TblCanteenWorkCode, TblEmployee, tblCatagory, tblDepartment, tblCompany" & _
                        " Where TblEmployee.PRESENTCARDNO=TBLCANTEEN.CARDNO and TBLCANTEEN.WorkCode=TblCanteenWorkCode.WorkCode and " & _
                        "tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode and " & _
                        "TblEmployee.CAT=tblCatagory.CAT And tblcanteen.officepunch Between #" & frmdate.ToString("yyyy-MM-dd 00:00:00") & "# AND #" & frmdate.ToString("yyyy-MM-dd 23:59:59") & "# " & _
                         " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "  order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "SELECT tblCatagory.CatagoryName, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   " & _
                        "tblemployee.PayCode,tblEmployee.EmpName,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblEmployee.PresentCardNo, " & _
                        "tblcanteen.*, TblCanteenWorkCode.WorkCodeName  from TBLCANTEEN, TblCanteenWorkCode, TblEmployee, tblCatagory, tblDepartment, tblCompany where " & _
                        "TblEmployee.PRESENTCARDNO=TBLCANTEEN.CARDNO and TBLCANTEEN.WorkCode=TblCanteenWorkCode.WorkCode and " & _
                        "tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode and " & _
                        "TblEmployee.CAT=tblCatagory.CAT And tblcanteen.officepunch Between '" & frmdate.ToString("yyyy-MM-dd 00:00:00") & "' AND '" & frmdate.ToString("yyyy-MM-dd 23:59:59") & "' " & _
                        " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "  order by tblEmployee.PayCode, tblcanteen.OFFICEPUNCH "
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            'If Rs_Report.Tables(0).Rows.Count = 0 Then
            '    GoTo tmp
            'End If

            For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
                strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                Common.tbl.Rows.Add()
                Common.tbl.DefaultView(rowCount)("PayCode") = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                Common.tbl.DefaultView(rowCount)("Employee Name") = Rs_Report.Tables(0).Rows(i).Item("empname").ToString.Trim
                For m As Integer = 0 To dsWC.Tables(0).Rows.Count - 1
                    Common.tbl.DefaultView(rowCount)(dsWC.Tables(0).Rows(m).Item("WorkCodeName").ToString.Trim) = "0"
                Next


                Do While strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim                  
                    Common.tbl.DefaultView(rowCount)(Rs_Report.Tables(0).Rows(i).Item("WorkCodeName").ToString.Trim) = Convert.ToDouble(Common.tbl.DefaultView(rowCount)(Rs_Report.Tables(0).Rows(i).Item("WorkCodeName").ToString.Trim)) + 1

                    i = i + 1
                    If i >= Rs_Report.Tables(0).Rows.Count - 1 Then
                        GoTo tmp
                        Exit Do
                    End If
                    If strPayCode <> Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim Then Exit Do
                Loop
                rowCount = rowCount + 1
            Next '  Loop
tmp:        frmdate = frmdate.AddDays(1)
            Common.tbl.Rows.Add("")
            rowCount = rowCount + 1

        End While
      

        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub



    Private Sub Set_Filter_Crystal() 'Optional ByVal Selection As String
        'On Error GoTo ErrorGen
        'Dim i As Integer
        Dim mCompanyString As String, mDepartmentString As String, mCatagoryString As String
        Dim mEmployeeString As String, r_CompantStr As String, mShiftString As String, mLocationString As String
        Dim mDivisionString As String, mGradeString As String, mActive As String

        mCompanyString = ""
        mActive = ""
        mDepartmentString = ""
        mCatagoryString = ""
        mEmployeeString = ""
        mShiftString = ""
        mDivisionString = ""
        mGradeString = ""
        mLocationString = ""
        Dim g_CompanyNames As String = ""
        'For CompanyCode
        Dim selectedRowsComp As Integer() = GridViewComp.GetSelectedRows()
        Dim resultComp As Object() = New Object(selectedRowsComp.Length - 1) {}
        For i As Integer = 0 To selectedRowsComp.Length - 1
            Dim rowHandle As Integer = selectedRowsComp(i)
            If Not GridViewComp.IsGroupRow(rowHandle) Then
                resultComp(i) = GridViewComp.GetRowCellValue(rowHandle, "COMPANYCODE")
                If i <> 0 Then
                    mCompanyString = mCompanyString + " OR "
                End If
                mCompanyString = mCompanyString + "tblEmployee.CompanyCode = "
                mCompanyString = mCompanyString + "'" + resultComp(i) + "'"
                g_CompanyNames = g_CompanyNames & GridViewComp.GetRowCellValue(rowHandle, "COMPANYNAME") & ", "
            End If
        Next
        'For i = 0 To (LstCompanyTarget.ListCount - 1)
        '    LstCompanyTarget.ListIndex = i
        '    If i <> 0 Then
        '        mCompanyString = mCompanyString + " OR "
        '    End If
        '    mCompanyString = mCompanyString + "tblEmployee.CompanyCode = "
        '    mCompanyString = mCompanyString + "'" + Left(LstCompanyTarget.Text, 3) + "'"
        '    g_CompanyNames = g_CompanyNames & Trim(Mid(LstCompanyTarget.Text, 6)) & ", "
        'Next i

        'For DepartmentCode
        Dim selectedRowsDept As Integer() = GridViewDept.GetSelectedRows()
        Dim resultDept As Object() = New Object(selectedRowsDept.Length - 1) {}
        For i As Integer = 0 To selectedRowsDept.Length - 1
            Dim rowHandle As Integer = selectedRowsDept(i)
            If Not GridViewDept.IsGroupRow(rowHandle) Then
                resultDept(i) = GridViewDept.GetRowCellValue(rowHandle, "DEPARTMENTCODE")
                If i <> 0 Then
                    mDepartmentString = mDepartmentString + " OR "
                End If
                mDepartmentString = mDepartmentString + "tblEmployee.DepartmentCode = "
                mDepartmentString = mDepartmentString + "'" + resultDept(i) + "'"
            End If
        Next

        'For CatagoryCode
        Dim selectedRowsCat As Integer() = GridViewCat.GetSelectedRows()
        Dim resultCat As Object() = New Object(selectedRowsCat.Length - 1) {}
        For i As Integer = 0 To selectedRowsCat.Length - 1
            Dim rowHandle As Integer = selectedRowsCat(i)
            If Not GridViewCat.IsGroupRow(rowHandle) Then
                resultCat(i) = GridViewCat.GetRowCellValue(rowHandle, "CAT")
                If i <> 0 Then
                    mCatagoryString = mCatagoryString + " OR "
                End If
                mCatagoryString = mCatagoryString + "tblEmployee.CAT = "
                mCatagoryString = mCatagoryString + "'" + resultCat(i) + "'"
            End If
        Next

        ' For Division Code
        'For i = 0 To (lstDivisionTarget.ListCount - 1)
        '    lstDivisionTarget.ListIndex = i
        '    If i <> 0 Then
        '        mDivisionString = mDivisionString + " OR "
        '    End If
        '    mDivisionString = mDivisionString + "tblEmployee.DivisionCode = "
        '    mDivisionString = mDivisionString + "'" + Left(lstDivisionTarget.Text, 3) + "'"
        'Next i

        ' For Grade Code
        Dim selectedRowsGrade As Integer() = GridViewGrade.GetSelectedRows()
        Dim resultGrade As Object() = New Object(selectedRowsGrade.Length - 1) {}
        For i As Integer = 0 To selectedRowsGrade.Length - 1
            Dim rowHandle As Integer = selectedRowsGrade(i)
            If Not GridViewGrade.IsGroupRow(rowHandle) Then
                resultGrade(i) = GridViewGrade.GetRowCellValue(rowHandle, "GradeCode")
                If i <> 0 Then
                    mGradeString = mGradeString + " OR "
                End If
                mGradeString = mGradeString + "tblEmployee.GradeCode = "
                mGradeString = mGradeString + "'" + resultGrade(i) + "'"
            End If
        Next

        'For EmployeeCode  - Check for late arrival
        Dim selectedRowsEmp As Integer() = GridViewEmp.GetSelectedRows()
        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
        For i As Integer = 0 To selectedRowsEmp.Length - 1
            Dim rowHandle As Integer = selectedRowsEmp(i)
            If Not GridViewEmp.IsGroupRow(rowHandle) Then
                resultEmp(i) = GridViewEmp.GetRowCellValue(rowHandle, "PAYCODE")
                Dim emp As String = resultEmp(i)
                If i <> 0 Then
                    mEmployeeString = mEmployeeString + " OR "
                End If
                mEmployeeString = mEmployeeString & "tblEmployee.Paycode  = "
                mEmployeeString = mEmployeeString & "'" & emp & "'"
            End If
        Next

        'For Shift
        Dim selectedRowsShift As Integer() = GridViewShift.GetSelectedRows()
        Dim resultShift As Object() = New Object(selectedRowsShift.Length - 1) {}
        For i As Integer = 0 To selectedRowsShift.Length - 1
            Dim rowHandle As Integer = selectedRowsShift(i)
            If Not GridViewShift.IsGroupRow(rowHandle) Then
                resultShift(i) = GridViewShift.GetRowCellValue(rowHandle, "SHIFT")
                If i <> 0 Then
                    mShiftString = mShiftString + " OR "
                End If
                If WhichReport = 1 Then
                    mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
                Else
                    mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
                End If
                mShiftString = mShiftString + "'" + resultShift(i) + "'"
            End If
        Next

        'For Location
        Dim selectedRowsLocation As Integer() = GridViewBranch.GetSelectedRows()
        Dim resultLocation As Object() = New Object(selectedRowsLocation.Length - 1) {}
        For i As Integer = 0 To selectedRowsLocation.Length - 1
            Dim rowHandle As Integer = selectedRowsLocation(i)
            If Not GridViewBranch.IsGroupRow(rowHandle) Then
                resultLocation(i) = GridViewBranch.GetRowCellValue(rowHandle, "BRANCHCODE")
                If i <> 0 Then
                    mLocationString = mLocationString + " OR "
                End If
                If WhichReport = 1 Then
                    mLocationString = mLocationString + "tblEmployee.BRANCHCODE = "
                Else
                    mLocationString = mLocationString + "tblEmployee.BRANCHCODE = "
                End If
                mLocationString = mLocationString + "'" + resultLocation(i) + "'"
            End If
        Next
        'For i = 0 To (LstShiftTarget.ListCount - 1)
        '    LstShiftTarget.ListIndex = i
        '    If i <> 0 Then
        '        mShiftString = mShiftString + " OR "
        '    End If
        '    If WhichReport = 1 Then
        '        'If g_Report_view Then
        '        mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
        '        'Else
        '        '    mShiftString = mShiftString + "tblTimeRegisterd.ShiftAttended = "
        '        'End If
        '    Else
        '        'If g_Report_view Then
        '        mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
        '        'Else
        '        '    mShiftString = mShiftString + "tblTimeRegisterd.ShiftAttended = "
        '        'End If
        '    End If
        '    mShiftString = mShiftString + "'" + Left(LstShiftTarget.Text, 3) + "'"
        'Next i

        If CheckEdit1.Checked And CheckEdit2.Checked Then
            mActive = ""
        Else
            If CheckEdit1.Checked Then
                mActive = "tblEmployee.Active = 'Y'"
            End If
            If CheckEdit2.Checked Then
                mActive = "tblEmployee.Active = 'N'"
            End If
        End If
        Dim mCondition As String
        'Dim g_WhereClause As String
        mCondition = IIf(Len(Trim(mEmployeeString)) = 0, "(", "(" & Trim(mEmployeeString) & ") AND (") & _
                     IIf(Len(Trim(mCatagoryString)) = 0, "", " " & Trim(mCatagoryString) & ") AND (") & _
                     IIf(Len(Trim(mDepartmentString)) = 0, "", " " & Trim(mDepartmentString) & ") AND (") & _
                     IIf(Len(Trim(mCompanyString)) = 0, "", " " & Trim(mCompanyString) & ") AND (") & _
                     IIf(Len(Trim(mShiftString)) = 0, "", " " & Trim(mShiftString) & ") AND (") & _
                     IIf(Len(Trim(mGradeString)) = 0, "", " " & Trim(mGradeString) & ") AND (") & _
                     IIf(Len(Trim(mLocationString)) = 0, "", " " & Trim(mLocationString) & ") AND (") & _
                     IIf(Len(Trim(mActive)) = 0, "", " " & Trim(mActive) & ") AND (")

        If mCondition = "(" Then
            mCondition = ""
        End If
        If Not Len(Trim(mCondition)) = 0 Then
            mCondition = mCondition.Remove(mCondition.Trim.Length - 5)
            'mCondition = LTrim(mCondition, Len(Trim(mCondition)) - 5)
        End If
        g_WhereClause = mCondition
        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation, "TimeWatch")
    End Sub
    Private Sub PopupContainerEditEmp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditEmp.QueryResultValue
        Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("PAYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditComp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditComp.QueryResultValue
        Dim selectedRows() As Integer = GridViewComp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewComp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("COMPANYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditDept_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditDept.QueryResultValue
        Dim selectedRows() As Integer = GridViewDept.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewDept.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("DEPARTMENTCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditShift_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditShift.QueryResultValue
        Dim selectedRows() As Integer = GridViewShift.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewShift.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("SHIFT"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditCat_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditCat.QueryResultValue
        Dim selectedRows() As Integer = GridViewCat.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewCat.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("CAT"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditGrade_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditGrade.QueryResultValue
        Dim selectedRows() As Integer = GridViewGrade.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewGrade.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("GradeCode"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditLocation_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditLocation.QueryResultValue
        Dim selectedRows() As Integer = GridViewBranch.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewBranch.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("BRANCHCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Function Length7(ByVal dblA As Double) As String
        Dim strB As String '* 7
        strB = Format(dblA, "0.00")
        Length7 = strB  '.Substring(0, 6)
        'MsgBox("Length7 " & Length7)
    End Function
    Function Length5(ByVal dblA As Double) As String
        Dim strB As String '* 5
        strB = Format(dblA, "0.00")
        Length5 = strB  '.Substring(0, 5)
        'MsgBox("Length5 " & Length5)
    End Function
    Sub CreateTableMachine()
        'On Error GoTo ErrorGen
        'On Local Error Resume Next
        Dim strsql As String
        Dim rsSysObjects As DataSet = New DataSet 'ADODB.Recordset
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter

        'Removing the table if it is already in database    START
        If Common.servername = "Access" Then
            strsql = "Drop table " & g_MachineRawPunch
            cmd1 = New OleDbCommand(strsql, Common.con1)
            cmd1.ExecuteNonQuery()
            'Cn.Execute(strsql)
        Else
            strsql = "Select name from sysObjects where name = '" & g_MachineRawPunch & "'"
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(rsSysObjects)
            'rsSysObjects = Cn.Execute(strsql)
            If rsSysObjects.Tables(0).Rows.Count > 0 Then
                strsql = "Drop table " & g_MachineRawPunch
                cmd = New SqlCommand(strsql, Common.con)
                cmd.ExecuteNonQuery()
                'Cn.Execute(strsql)
            End If
            'rsSysObjects.Close()
        End If
        'Removing the table if it is already in database    End

        strsql = "Create Table " & g_MachineRawPunch & _
            "(PAYCODE Char(10) Null, CARDNO Char(8) Null, " & _
            " EMPNAME Char(25) Null, PUNCH1 Char(5) Null, " & _
            " PUNCH2 Char(5) Null, PUNCH3 Char(5) Null, " & _
            " PUNCH4 Char(5) Null, DATEOFFICE DateTime Null, " & _
            " PUNCH1M Char(3) Null, PUNCH2M Char(3) Null, " & _
            " PUNCH3M Char(3) Null, PUNCH4M Char(3) Null, " & _
            " COMPANYCODE Char(3) Null, DEPARTMENTCODE Char(3) Null, " & _
            " CAT Char(3) Null,INOUT Char(1) Null)"

        If Common.servername = "Access" Then
            Common.con1.Open()
            cmd1 = New OleDbCommand(strsql, Common.con1)
            cmd1.ExecuteNonQuery()
            Common.con1.Close()
        Else
            Common.con.Open()
            cmd = New SqlCommand(strsql, Common.con)
            cmd.ExecuteNonQuery()
            Common.con.Close()
        End If
        'Cn.Execute(strsql)
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation, "TimeWatch")
        '        Screen.MousePointer = vbDefault
    End Sub
    Sub DropTableMachine()
        'On Error GoTo ErrorGen
        Dim strsql As String
        strsql = "Drop Table " & g_MachineRawPunch
        If Common.servername = "Access" Then
            Common.con1.Open()
            cmd1 = New OleDbCommand(strsql, Common.con1)
            cmd1.ExecuteNonQuery()
            Common.con1.Close()
        Else
            Common.con.Open()
            cmd = New SqlCommand(strsql, Common.con)
            cmd.ExecuteNonQuery()
            Common.con.Close()
        End If
        'Cn.Execute(strsql)
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation, "TimeWatch")
        'Screen.MousePointer = vbDefault
    End Sub
    Sub MachinePunch(mFirstDate As Date, mSecondDate As Date)
        'On Error GoTo ErrorGen
        Dim rsEmp As DataSet = New DataSet 'ADODB.Recordset, 
        Dim rsMrp As DataSet = New DataSet 'ADODB.Recordset
        Dim RS As DataSet = New DataSet
        Dim mF_Date As Date
        Dim mS_Date As Date

        Dim sSql As String = "Select * From " & g_MachineRawPunch
        rsMrp = New DataSet 'ADODB.Recordset

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter

        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsMrp)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsMrp)
        End If
        'rsMrp.Open(sSql, Cn, adOpenKeyset, adLockBatchOptimistic)

        sSql = "Select PayCode, PresentCardNo, EmpName, CompanyCode, DepartmentCode, Cat From tblEmployee Order By PayCode"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsEmp)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsEmp)
        End If
        'rsEmp = Cn.Execute(sSql)

        'With rsEmp
        '    .MoveFirst()
        '    Do While Not .EOF
        For i As Integer = 0 To rsEmp.Tables(0).Rows.Count - 1
            mF_Date = mFirstDate
            mS_Date = mSecondDate
            Do While mF_Date <= mS_Date
                If Common.servername = "Access" Then
                    sSql = "Select * from MachineRawPunch Where " & _
                        " OfficePunch between #" & mF_Date.ToString("dd-MM-yyyy 00:00:00") & "# and #" & mF_Date.ToString("dd-MM-yyyy 23:59:59") & _
                        "# And paycode = '" & rsEmp.Tables(0).Rows(i).Item("paycode").ToString.Trim & "' Order By  OfficePunch"
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(RS)
                Else
                    sSql = "Select * from MachineRawPunch Where " & _
                            " OfficePunch between '" & mF_Date.ToString("dd-MM-yyyy 00:00:00") & "' and '" & mF_Date.ToString("dd-MM-yyyy 23:59:59") & _
                            "' And paycode = '" & rsEmp.Tables(0).Rows(i).Item("paycode").ToString.Trim & "' Order By  OfficePunch"
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(RS)
                End If
                'Rs = Cn.Execute(sSql)

                Dim paycode As String
                Dim CardNo As String
                Dim EmpName As String
                Dim companycode As String
                Dim DepartmentCode As String
                Dim Cat As String
                Dim DATEOFFICE As String
                Dim PUNCH1 As String
                Dim punch1m As String
                Dim PUNCH2 As String = ""
                Dim punch2m As String = ""
                Dim PUNCH3 As String = ""
                Dim punch3m As String = ""
                Dim PUNCH4 As String = ""
                Dim punch4m As String = ""
                For j As Integer = 0 To RS.Tables(0).Rows.Count - 1
                    'Do While Not RS.EOF
                    paycode = rsEmp.Tables(0).Rows(i).Item("paycode").ToString.Trim
                    CardNo = rsEmp.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
                    EmpName = rsEmp.Tables(0).Rows(i).Item("EmpName").ToString.Trim
                    companycode = rsEmp.Tables(0).Rows(i).Item("companycode").ToString.Trim
                    DepartmentCode = rsEmp.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                    Cat = rsEmp.Tables(0).Rows(i).Item("Cat").ToString.Trim
                    DATEOFFICE = Convert.ToDateTime(RS.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("DD/MM/YYYY")
                    PUNCH1 = Convert.ToDateTime(RS.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm")
                    punch1m = IIf(RS.Tables(0).Rows(i).Item("IsManual").ToString.Trim = "Y", "M", " ") & "-" & IIf(RS.Tables(0).Rows(i).Item("inout").ToString.Trim = "", RS.Tables(0).Rows(i).Item("inout").ToString.Trim, " ")

                    'RS.MoveNext()
                    Continue For
                    If j > RS.Tables(0).Rows.Count - 1 Then
                        PUNCH2 = Convert.ToDateTime(RS.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm")
                        punch2m = IIf(RS.Tables(0).Rows(i).Item("IsManual").ToString.Trim = "Y", "M", " ") & "-" & IIf(RS.Tables(0).Rows(i).Item("inout").ToString.Trim = "", RS.Tables(0).Rows(i).Item("inout").ToString.Trim, " ")
                    Else
                        'rsMrp.Update()
                        Exit For
                    End If
                    'RS.MoveNext()
                    Continue For
                    If j > RS.Tables(0).Rows.Count - 1 Then
                        PUNCH3 = Convert.ToDateTime(RS.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm")
                        punch3m = IIf(RS.Tables(0).Rows(i).Item("IsManual").ToString.Trim = "Y", "M", " ") & "-" & IIf(RS.Tables(0).Rows(i).Item("inout").ToString.Trim = "", RS.Tables(0).Rows(i).Item("inout").ToString.Trim, " ")
                    Else
                        'rsMrp.Update()
                        Exit For
                    End If
                    'RS.MoveNext()
                    Continue For
                    If j > RS.Tables(0).Rows.Count - 1 Then
                        PUNCH4 = Convert.ToDateTime(RS.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm")
                        punch4m = IIf(RS.Tables(0).Rows(i).Item("IsManual").ToString.Trim = "Y", "M", " ") & "-" & IIf(RS.Tables(0).Rows(i).Item("inout").ToString.Trim = "", RS.Tables(0).Rows(i).Item("inout").ToString.Trim, " ")
                    Else
                        'rsMrp.Update()
                        Exit For
                    End If
                    'RS.MoveNext()
                    'rsMrp.Update()
                    'Loop
                Next

                Dim sSqltmp As String = "insert into " & g_MachineRawPunch & " (paycode,CardNo,EmpName,companycode,DepartmentCode,Cat,DATEOFFICE,PUNCH1,punch1m,PUNCH2,punch2m,PUNCH3,punch3m,PUNCH4,punch4m)" & _
                    "values('" & paycode & "','" & CardNo & "','" & EmpName & "','" & companycode & "','" & DepartmentCode & "','" & Cat & "','" & DATEOFFICE & "','" & PUNCH1 & "','" & punch1m & "','" & PUNCH2 & "','" & punch2m & "','" & PUNCH3 & "','" & punch3m & "','" & PUNCH4 & "','" & punch4m & "')"
                If Common.servername = "Access" Then
                    Common.con1.Open()
                    cmd1 = New OleDbCommand(sSqltmp, Common.con1)
                    cmd1.ExecuteNonQuery()
                    Common.con1.Close()
                Else
                    Common.con.Open()
                    cmd = New SqlCommand(sSqltmp, Common.con)
                    cmd.ExecuteNonQuery()
                    Common.con.Close()
                End If
                mF_Date = mF_Date.AddDays(1)
            Loop
            '.MoveNext()
            'Loop
        Next
        'End With
        'rsMrp.UpdateBatch()
        'rsMrp.Close()
        'rsEmp.Close()

        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation, "TimeWatch")
        '        Screen.MousePointer = vbDefault
    End Sub
    Private Sub CheckExcel_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckExcel.CheckedChanged
        If CheckExcel.Checked = True Then
            XtraShortOrder.ShowDialog()
        End If
    End Sub
    Private Sub CheckExcel_VisibleChanged(sender As System.Object, e As System.EventArgs) Handles CheckExcel.VisibleChanged
        If CheckExcel.Visible = False Then
            CheckText.Checked = True
        End If
    End Sub
    Private Sub DateEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles DateEdit1.Leave
        DateEdit2.DateTime = DateEdit1.DateTime.AddMonths(1).AddDays(-1)
    End Sub
    Private Sub DateEdit2_Leave(sender As System.Object, e As System.EventArgs) Handles DateEdit2.Leave
        If DateEdit2.DateTime < DateEdit1.DateTime Then
            DateEdit2.DateTime = DateEdit1.DateTime.AddMonths(1).AddDays(-1)
        End If
    End Sub
    Private Sub SimpleButton5_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton5.Click
        'DevExpress.Export.ExportSettings.DefaultExportType = ExportType.WYSIWYG   'to get as it is from grid
        DevExpress.Export.ExportSettings.DefaultExportType = DevExpress.Export.ExportType.DataAware
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
        'GridView1.ExportToXlsx(mstrFile_Name)


        ''for row color
        'Dim op As New XlsExportOptionsEx()
        'AddHandler op.CustomizeCell, AddressOf op_CustomizeCell
        'GridView1.ExportToXls(mstrFile_Name, op)
        'System.Diagnostics.Process.Start(mstrFile_Name)
        ''end for row color



        'for header text 'nitin
        ' Ensure that the data-aware export mode is enabled.
        ExportSettings.DefaultExportType = ExportType.DataAware
        ' Create a new object defining how a document is exported to the XLSX format.
        Dim options = New XlsxExportOptionsEx()
        ' Specify a name of the sheet in the created XLSX file.
        options.SheetName = "Sheet1"

        ' Subscribe to export customization events. 
        'AddHandler options.CustomizeSheetSettings, AddressOf options_CustomizeSheetSettings
        AddHandler options.CustomizeSheetHeader, AddressOf options_CustomizeSheetHeader
        'AddHandler options.CustomizeCell, AddressOf options_CustomizeCell
        'AddHandler options.CustomizeSheetFooter, AddressOf options_CustomizeSheetFooter
        'AddHandler options.AfterAddRow, AddressOf options_AfterAddRow


        ' Export the grid data to the XLSX format.
        GridControl1.ExportToXlsx(mstrFile_Name, options)
        ' Open the created document.
        Process.Start(mstrFile_Name)
        'End for header text 'nitin



        'Dim sr As New StreamReader(mstrFile_Name)
        'Dim text As String = sr.ReadToEnd()
        'sr.Close()
        'Dim sw As New StreamWriter(mstrFile_Name)
        'Dim rundatetime As String = "         Company Name:" & CommonReport.g_CompanyNames
        'Dim caption2 As String = "           Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")

        'sw.WriteLine(rundatetime)
        'sw.WriteLine(caption2)
        'sw.WriteLine("Monthly Performance from " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy"))
        'sw.WriteLine("")
        'sw.WriteLine(text) ' Add original content back
        'sw.Close()

        'Process.Start(mstrFile_Name)
    End Sub
    Private Sub GridView1_RowStyle(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles GridView1.RowStyle
        Dim View As GridView = CType(sender, GridView)
        If (e.RowHandle >= 0) Then
            Dim priority As String = View.GetRowCellDisplayText(e.RowHandle, View.Columns("Date"))
            If (priority = "Present") Then
                e.Appearance.BackColor = Color.FromArgb(150, Color.LightCoral)
                e.Appearance.BackColor2 = Color.Green
                e.Appearance.ForeColor = Color.White
            End If

        End If
    End Sub
    Private Sub op_CustomizeCell(ea As DevExpress.Export.CustomizeCellEventArgs)
        Dim View As GridView = CType(GridView1, GridView)
        Dim priority As String = View.GetRowCellDisplayText(ea.RowHandle, View.Columns("Date"))
        If (priority = "Present") Then
            ea.Formatting.BackColor = Color.Green
            ea.Formatting.Font.Color = Color.White
            ea.Handled = True
        End If
        'If ea.ColumnFieldName = "Date" Then
        '    ea.Formatting.BackColor = Color.Pink
        '    ea.Handled = True
        'End If
    End Sub
    Private Sub CheckEditWorkCode_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditWorkCode.CheckedChanged
        If CheckEditWorkCode.Checked = True Then
            PanelControl2.Enabled = False
            CheckExcel.Checked = True
            CheckText.Visible = False
        Else
            PanelControl2.Enabled = True
            CheckExcel.Visible = True
        End If
    End Sub
#Region "#CustomizeSheetHeaderEvent"
    Private Delegate Sub AddCells(ByVal e As ContextEventArgs, ByVal formatFirstCell As XlFormattingObject, ByVal formatSecondCell As XlFormattingObject)
    Private methods As Dictionary(Of Integer, AddCells) = CreateMethodSet()

    Private Sub options_CustomizeSheetHeader(ByVal e As ContextEventArgs)
        ' Specify cell formatting. 
        Dim formatFirstCell = CreateXlFormattingObject(True, 12)
        Dim formatSecondCell = CreateXlFormattingObject(True, 12)
        ' Add new rows displaying custom information. 
        For i = 0 To 2
            Dim addCellMethod As AddCells = Nothing
            If methods.TryGetValue(i, addCellMethod) Then
                addCellMethod(e, formatFirstCell, formatSecondCell)
            Else
                e.ExportContext.AddRow()
            End If
        Next i
        ' Merge specific cells.
        MergeCells(e)
        '' Add an image to the top of the document.
        'Dim file = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Resources.1.jpg")
        'If file IsNot Nothing Then
        '    Dim imageToHeader = New Bitmap(Image.FromStream(file))
        '    Dim imageToHeaderRange = New XlCellRange(New XlCellPosition(0, 0), New XlCellPosition(5, 7))
        '    e.ExportContext.MergeCells(imageToHeaderRange)
        '    e.ExportContext.InsertImage(imageToHeader, imageToHeaderRange)
        'End If
        'e.ExportContext.MergeCells(New XlCellRange(New XlCellPosition(0, 8), New XlCellPosition(5, 8)))
    End Sub

    Private Shared Function CreateMethodSet() As Dictionary(Of Integer, AddCells)
        Dim dictionary = New Dictionary(Of Integer, AddCells)()
        dictionary.Add(1, AddressOf ReportTitleRow)
        dictionary.Add(2, AddressOf ReportTitleFromDateRow)
        'dictionary.Add(11, AddressOf AddPhoneRow)
        'dictionary.Add(12, AddressOf AddFaxRow)
        'dictionary.Add(13, AddressOf AddEmailRow)
        Return dictionary
    End Function
    Private Shared Sub ReportTitleFromDateRow(ByVal e As ContextEventArgs, ByVal formatFirstCell As XlFormattingObject, ByVal formatSecondCell As XlFormattingObject)
        'Dim AddressLocationCityCell = CreateCell("Los Angeles CA 90731 USA", formatSecondCell)
        'e.ExportContext.AddRow({Nothing, Nothing, AddressLocationCityCell})

        'nitin
        Dim ReportTitleFromDate = CreateCell("Monthly Performance from ", formatSecondCell)
        e.ExportContext.AddRow({Nothing, Nothing, Nothing, Nothing, ReportTitleFromDate})
    End Sub
    Private Shared Sub ReportTitleRow(ByVal e As ContextEventArgs, ByVal formatFirstCell As XlFormattingObject, ByVal formatSecondCell As XlFormattingObject)
        'Dim AddressCellName = CreateCell("Address: ", formatFirstCell)
        'Dim AddresssCellLocation = CreateCell("807 West Paseo Del Mar", formatSecondCell)
        'e.ExportContext.AddRow({AddressCellName, Nothing, AddresssCellLocation})


        'nitin
        Dim ReportTitle = CreateCell("Company Name:" & CommonReport.g_CompanyNames, formatFirstCell)
        'Dim AddresssCellLocation = CreateCell("807 West Paseo Del Mar", formatSecondCell)
        e.ExportContext.AddRow({Nothing, Nothing, Nothing, Nothing, ReportTitle})
    End Sub
    ' Create a new cell with a specified value and format settings.
    Private Shared Function CreateCell(ByVal value As Object, ByVal formatCell As XlFormattingObject) As CellObject
        Return New CellObject With {.Value = value, .Formatting = formatCell}
    End Function
    ' Merge specific cells.
    Private Shared Sub MergeCells(ByVal e As ContextEventArgs)
        'MergeCells(e, 0, 1, 5, 1)
        'MergeCells(e, 0, 9, 1, 10)
        'MergeCells(e, 2, 10, 5, 10)
        'MergeCells(e, 0, 11, 1, 11)
        'MergeCells(e, 2, 11, 5, 11)
        'MergeCells(e, 0, 12, 1, 12)
        'MergeCells(e, 2, 12, 5, 12)
        'MergeCells(e, 0, 13, 1, 13)
        'MergeCells(e, 2, 13, 5, 13)
        'MergeCells(e, 0, 14, 5, 14)
    End Sub
    Private Shared Sub MergeCells(ByVal e As ContextEventArgs, ByVal left As Integer, ByVal top As Integer, ByVal right As Integer, ByVal bottom As Integer)
        e.ExportContext.MergeCells(New XlCellRange(New XlCellPosition(left, top), New XlCellPosition(right, bottom)))
    End Sub
    ' Specify a cell's alignment and font settings. 
    Private Shared Function CreateXlFormattingObject(ByVal bold As Boolean, ByVal size As Double) As XlFormattingObject
        Dim cellFormat = New XlFormattingObject With { _
            .Font = New XlCellFont With {.Bold = bold, .Size = size}, _
            .Alignment = New XlCellAlignment With {.RelativeIndent = 10, .HorizontalAlignment = XlHorizontalAlignment.Center, .VerticalAlignment = XlVerticalAlignment.Center} _
        }
        Return cellFormat
    End Function
#End Region ' #CustomizeSheetHeaderEvent
End Class