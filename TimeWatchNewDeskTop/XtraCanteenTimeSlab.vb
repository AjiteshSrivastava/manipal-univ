﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Text.RegularExpressions

Public Class XtraCanteenTimeSlab
    Dim ulf As UserLookAndFeel
    Dim cmd1 As OleDbCommand
    Dim cmd As SqlCommand
    Public Shared GpId As String
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
        If Common.servername = "Access" Then
            'Me.TblTimeSlab1TableAdapter1.Fill(Me.SSSDBDataSet.tblTimeSlab1)
        Else
            TblTimeSlabTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
        End If
    End Sub
    Private Sub XtraCanteenTimeSlab_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100
        'not to do in all pages
        Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        Common.SplitterPosition = SplitContainerControl1.SplitterPosition
        If Common.servername = "Access" Then
            Me.TblTimeSlab1TableAdapter1.Fill(Me.SSSDBDataSet.tblTimeSlab1)
            GridControl1.DataSource = SSSDBDataSet.tblTimeSlab1
        Else
            Me.TblTimeSlabTableAdapter.Fill(Me.SSSDBDataSet.tblTimeSlab)
            GridControl1.DataSource = SSSDBDataSet.tblTimeSlab
        End If
        loadData()
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            GpId = row("Shift").ToString.Trim
        Catch ex As Exception
            GpId = ""
        End Try
        e.Allow = False
        XtraCanteenTimeSlabEdit.ShowDialog()
        If Common.servername = "Access" Then
            Me.TblTimeSlab1TableAdapter1.Fill(Me.SSSDBDataSet.tblTimeSlab1)
            GridControl1.DataSource = SSSDBDataSet.tblTimeSlab1
        Else
            Me.TblTimeSlabTableAdapter.Fill(Me.SSSDBDataSet.tblTimeSlab)
            GridControl1.DataSource = SSSDBDataSet.tblTimeSlab
        End If
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
            End If
        End If
    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        Me.TblTimeSlab1TableAdapter1.Update(Me.SSSDBDataSet.tblTimeSlab1)
        Me.TblTimeSlabTableAdapter.Update(Me.SSSDBDataSet.tblTimeSlab)
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        Me.TblTimeSlab1TableAdapter1.Update(Me.SSSDBDataSet.tblTimeSlab1)
        Me.TblTimeSlabTableAdapter.Update(Me.SSSDBDataSet.tblTimeSlab)
    End Sub

    Private Sub setDefault()
        TextEditBFEnd.Text = "00:00"
        TextEditBFStrt.Text = "00:00"
        TextEditDEnd.Text = "00:00"
        TextEditDStrt.Text = "00:00"
        TextEditLEnd.Text = "00:00"
        TextEditLStrt.Text = "00:00"
    End Sub
    Private Sub loadData()
        Dim sSql As String = "select * from tblTimeSlab"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            TextEditBFStrt.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("BStart").ToString.Trim).ToString("HH:mm")
            TextEditBFEnd.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("BEnd").ToString.Trim).ToString("HH:mm")
            'TextEditLStrt.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("LStart").ToString.Trim).ToString("HH:mm")
            'TextEditLEnd.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("LEnd").ToString.Trim).ToString("HH:mm")
            'TextEditDStrt.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("DStart").ToString.Trim).ToString("HH:mm")
            'TextEditDEnd.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("DEnd").ToString.Trim).ToString("HH:mm")
        Else
            setDefault()
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim Shift As String = "01"
        Dim BStart As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditBFStrt.Text.Trim)
        Dim BEnd As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditBFEnd.Text.Trim)
        Dim LStart As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditLStrt.Text.Trim)
        Dim LEnd As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditLEnd.Text.Trim)
        Dim DStart As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditDStrt.Text.Trim)
        Dim DEnd As DateTime = Convert.ToDateTime(Now.ToString("yyyy-MM-dd") & " " & TextEditDEnd.Text.Trim)
        Dim sSql As String = "select * from tblTimeSlab"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            'insert
            sSql = "insert into tblTimeSlab (Shift, [BStart], [BEnd], [LStart], [LEnd], [DStart], [DEnd]) values('" & Shift & "', '" & BStart.ToString("yyyy-MM-dd HH:mm:ss") & "','" & BEnd.ToString("yyyy-MM-dd HH:mm:ss") & "','" & LStart.ToString("yyyy-MM-dd HH:mm:ss") & "','" & LEnd.ToString("yyyy-MM-dd HH:mm:ss") & "','" & DStart.ToString("yyyy-MM-dd HH:mm:ss") & "','" & DEnd.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        Else
            'update
            sSql = "Update tblTimeSlab set BStart='" & BStart.ToString("yyyy-MM-dd HH:mm:ss") & "', BEnd='" & BEnd.ToString("yyyy-MM-dd HH:mm:ss") & "', LStart='" & LStart.ToString("yyyy-MM-dd HH:mm:ss") & "', LEnd='" & LEnd.ToString("yyyy-MM-dd HH:mm:ss") & "', DStart='" & DStart.ToString("yyyy-MM-dd HH:mm:ss") & "', DEnd='" & DEnd.ToString("yyyy-MM-dd HH:mm:ss") & "'"
        End If

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfuly</size>", "<size=9>Success</size>")
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Dim Status As String
        If ToggleSwitch1.IsOn = True Then
            Status = "Y"
        Else
            Status = "N"
        End If
        Dim sSql As String = "update tblMealSettings set Allowed='" & Status & "'"
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Common.g_MealApplicable = Status
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfuly</size>", "<size=9>Success</size>")
    End Sub
End Class
