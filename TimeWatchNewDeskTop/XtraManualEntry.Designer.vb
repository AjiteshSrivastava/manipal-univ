﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraManualEntry
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridLevelNode2 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraManualEntry))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.SidePanel3 = New DevExpress.XtraEditors.SidePanel()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colOFFICEPUNCH = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colOFFICEPUNCH1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colP_DAY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISMANUAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDateOFFICE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTATUS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTATTENDED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLATEARRIVAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.EARLYDEPARTURE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIN1_date = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colIN1_time = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colOUT2_date = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOUT2_time = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOUT1_date = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOUT1_time = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIN2_date = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIN2_time = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHOURSWORKED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDURATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.TextOT = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonExcel = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboPurpose = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboPNYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboPNMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboPNDate = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboFNYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboFNMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboFNDate = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.ToggleSwitch1 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRemark = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblTimeRegisterBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblTimeRegisterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblTimeRegisterTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblTimeRegisterTableAdapter()
        Me.MachineRawPunchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MachineRawPunchTableAdapter = New iAS.SSSDBDataSetTableAdapters.MachineRawPunchTableAdapter()
        Me.TblLeaveMaster1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter()
        Me.BehaviorManager1 = New DevExpress.Utils.Behaviors.BehaviorManager(Me.components)
        Me.TblEmployeeTableAdapter = New iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployee1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        Me.SidePanel3.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit3.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel2.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit4.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.TextOT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboPurpose.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboPNYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboPNMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboPNDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboFNYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboFNMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboFNDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRemark.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblTimeRegisterBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblTimeRegisterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MachineRawPunchBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 568)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'SidePanel3
        '
        Me.SidePanel3.AllowResize = False
        Me.SidePanel3.Controls.Add(Me.GroupControl2)
        Me.SidePanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanel3.Location = New System.Drawing.Point(679, 132)
        Me.SidePanel3.Name = "SidePanel3"
        Me.SidePanel3.Size = New System.Drawing.Size(486, 436)
        Me.SidePanel3.TabIndex = 15
        Me.SidePanel3.Text = "SidePanel3"
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.GroupControl2.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl2.Controls.Add(Me.GridControl2)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(486, 436)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Punches"
        '
        'GridControl2
        '
        Me.GridControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl2.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl2.Location = New System.Drawing.Point(2, 25)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit2, Me.RepositoryItemDateEdit3})
        Me.GridControl2.Size = New System.Drawing.Size(482, 409)
        Me.GridControl2.TabIndex = 0
        Me.GridControl2.UseEmbeddedNavigator = True
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colOFFICEPUNCH, Me.colOFFICEPUNCH1, Me.colP_DAY, Me.colISMANUAL})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView2.OptionsBehavior.Editable = False
        '
        'colOFFICEPUNCH
        '
        Me.colOFFICEPUNCH.Caption = "Punch Date"
        Me.colOFFICEPUNCH.ColumnEdit = Me.RepositoryItemDateEdit2
        Me.colOFFICEPUNCH.FieldName = "OFFICEPUNCH"
        Me.colOFFICEPUNCH.Name = "colOFFICEPUNCH"
        Me.colOFFICEPUNCH.Visible = True
        Me.colOFFICEPUNCH.VisibleIndex = 0
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'colOFFICEPUNCH1
        '
        Me.colOFFICEPUNCH1.Caption = "Punch Time"
        Me.colOFFICEPUNCH1.ColumnEdit = Me.RepositoryItemDateEdit3
        Me.colOFFICEPUNCH1.FieldName = "OFFICEPUNCH"
        Me.colOFFICEPUNCH1.Name = "colOFFICEPUNCH1"
        Me.colOFFICEPUNCH1.Visible = True
        Me.colOFFICEPUNCH1.VisibleIndex = 1
        '
        'RepositoryItemDateEdit3
        '
        Me.RepositoryItemDateEdit3.AutoHeight = False
        Me.RepositoryItemDateEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit3.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit3.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit3.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit3.Name = "RepositoryItemDateEdit3"
        '
        'colP_DAY
        '
        Me.colP_DAY.Caption = "P Day"
        Me.colP_DAY.FieldName = "P_DAY"
        Me.colP_DAY.Name = "colP_DAY"
        Me.colP_DAY.Visible = True
        Me.colP_DAY.VisibleIndex = 2
        '
        'colISMANUAL
        '
        Me.colISMANUAL.Caption = "Is Manual"
        Me.colISMANUAL.FieldName = "ISMANUAL"
        Me.colISMANUAL.Name = "colISMANUAL"
        Me.colISMANUAL.Visible = True
        Me.colISMANUAL.VisibleIndex = 3
        '
        'SidePanel2
        '
        Me.SidePanel2.AllowResize = False
        Me.SidePanel2.Controls.Add(Me.GroupControl1)
        Me.SidePanel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.SidePanel2.Location = New System.Drawing.Point(0, 132)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(679, 436)
        Me.SidePanel2.TabIndex = 14
        Me.SidePanel2.Text = "SidePanel2"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.GridControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(678, 436)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Processed Data"
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        GridLevelNode2.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode2})
        Me.GridControl1.Location = New System.Drawing.Point(2, 25)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.RepositoryItemDateEdit4})
        Me.GridControl1.Size = New System.Drawing.Size(674, 409)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDateOFFICE, Me.colSTATUS, Me.colSHIFTATTENDED, Me.colLATEARRIVAL, Me.EARLYDEPARTURE, Me.colIN1_date, Me.colIN1_time, Me.colOUT2_date, Me.colOUT2_time, Me.colOUT1_date, Me.colOUT1_time, Me.colIN2_date, Me.colIN2_time, Me.colHOURSWORKED, Me.colOTDURATION})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        '
        'colDateOFFICE
        '
        Me.colDateOFFICE.Caption = "Date"
        Me.colDateOFFICE.FieldName = "DateOFFICE"
        Me.colDateOFFICE.Name = "colDateOFFICE"
        Me.colDateOFFICE.Visible = True
        Me.colDateOFFICE.VisibleIndex = 0
        Me.colDateOFFICE.Width = 100
        '
        'colSTATUS
        '
        Me.colSTATUS.Caption = "Status"
        Me.colSTATUS.FieldName = "STATUS"
        Me.colSTATUS.Name = "colSTATUS"
        Me.colSTATUS.Visible = True
        Me.colSTATUS.VisibleIndex = 1
        '
        'colSHIFTATTENDED
        '
        Me.colSHIFTATTENDED.Caption = "Shift Attended"
        Me.colSHIFTATTENDED.FieldName = "SHIFTATTENDED"
        Me.colSHIFTATTENDED.Name = "colSHIFTATTENDED"
        Me.colSHIFTATTENDED.Visible = True
        Me.colSHIFTATTENDED.VisibleIndex = 2
        Me.colSHIFTATTENDED.Width = 100
        '
        'colLATEARRIVAL
        '
        Me.colLATEARRIVAL.Caption = "Late Arrival"
        Me.colLATEARRIVAL.FieldName = "LATEARRIVAL"
        Me.colLATEARRIVAL.Name = "colLATEARRIVAL"
        Me.colLATEARRIVAL.Visible = True
        Me.colLATEARRIVAL.VisibleIndex = 3
        Me.colLATEARRIVAL.Width = 100
        '
        'EARLYDEPARTURE
        '
        Me.EARLYDEPARTURE.Caption = "Early Departure"
        Me.EARLYDEPARTURE.FieldName = "EARLYDEPARTURE"
        Me.EARLYDEPARTURE.Name = "EARLYDEPARTURE"
        Me.EARLYDEPARTURE.Visible = True
        Me.EARLYDEPARTURE.VisibleIndex = 4
        Me.EARLYDEPARTURE.Width = 100
        '
        'colIN1_date
        '
        Me.colIN1_date.Caption = "IN Date"
        Me.colIN1_date.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colIN1_date.FieldName = "IN1"
        Me.colIN1_date.Name = "colIN1_date"
        Me.colIN1_date.Visible = True
        Me.colIN1_date.VisibleIndex = 5
        Me.colIN1_date.Width = 100
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'colIN1_time
        '
        Me.colIN1_time.Caption = "In Time"
        Me.colIN1_time.ColumnEdit = Me.RepositoryItemDateEdit4
        Me.colIN1_time.FieldName = "IN1"
        Me.colIN1_time.Name = "colIN1_time"
        Me.colIN1_time.Visible = True
        Me.colIN1_time.VisibleIndex = 6
        '
        'RepositoryItemDateEdit4
        '
        Me.RepositoryItemDateEdit4.AutoHeight = False
        Me.RepositoryItemDateEdit4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit4.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit4.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit4.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit4.Name = "RepositoryItemDateEdit4"
        '
        'colOUT2_date
        '
        Me.colOUT2_date.Caption = "Out Date"
        Me.colOUT2_date.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colOUT2_date.FieldName = "OUT2"
        Me.colOUT2_date.Name = "colOUT2_date"
        Me.colOUT2_date.Visible = True
        Me.colOUT2_date.VisibleIndex = 7
        Me.colOUT2_date.Width = 100
        '
        'colOUT2_time
        '
        Me.colOUT2_time.Caption = "Out Time"
        Me.colOUT2_time.ColumnEdit = Me.RepositoryItemDateEdit4
        Me.colOUT2_time.FieldName = "OUT2"
        Me.colOUT2_time.Name = "colOUT2_time"
        Me.colOUT2_time.Visible = True
        Me.colOUT2_time.VisibleIndex = 8
        '
        'colOUT1_date
        '
        Me.colOUT1_date.Caption = "LunchOut Date"
        Me.colOUT1_date.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colOUT1_date.FieldName = "OUT1"
        Me.colOUT1_date.Name = "colOUT1_date"
        Me.colOUT1_date.Visible = True
        Me.colOUT1_date.VisibleIndex = 9
        Me.colOUT1_date.Width = 100
        '
        'colOUT1_time
        '
        Me.colOUT1_time.Caption = "LunchOut Time"
        Me.colOUT1_time.ColumnEdit = Me.RepositoryItemDateEdit4
        Me.colOUT1_time.FieldName = "OUT1"
        Me.colOUT1_time.Name = "colOUT1_time"
        Me.colOUT1_time.Visible = True
        Me.colOUT1_time.VisibleIndex = 10
        '
        'colIN2_date
        '
        Me.colIN2_date.Caption = "LunchIn Date"
        Me.colIN2_date.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colIN2_date.FieldName = "IN2"
        Me.colIN2_date.Name = "colIN2_date"
        Me.colIN2_date.Visible = True
        Me.colIN2_date.VisibleIndex = 11
        Me.colIN2_date.Width = 100
        '
        'colIN2_time
        '
        Me.colIN2_time.Caption = "LunchIn Time"
        Me.colIN2_time.ColumnEdit = Me.RepositoryItemDateEdit4
        Me.colIN2_time.FieldName = "IN2"
        Me.colIN2_time.Name = "colIN2_time"
        Me.colIN2_time.Visible = True
        Me.colIN2_time.VisibleIndex = 12
        '
        'colHOURSWORKED
        '
        Me.colHOURSWORKED.Caption = "Worked Hours"
        Me.colHOURSWORKED.FieldName = "HOURSWORKED"
        Me.colHOURSWORKED.Name = "colHOURSWORKED"
        Me.colHOURSWORKED.Visible = True
        Me.colHOURSWORKED.VisibleIndex = 13
        Me.colHOURSWORKED.Width = 100
        '
        'colOTDURATION
        '
        Me.colOTDURATION.Caption = "OT DURATION"
        Me.colOTDURATION.FieldName = "OTDURATION"
        Me.colOTDURATION.Name = "colOTDURATION"
        Me.colOTDURATION.Visible = True
        Me.colOTDURATION.VisibleIndex = 14
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.TextOT)
        Me.SidePanel1.Controls.Add(Me.LabelControl10)
        Me.SidePanel1.Controls.Add(Me.SimpleButtonExcel)
        Me.SidePanel1.Controls.Add(Me.ComboPurpose)
        Me.SidePanel1.Controls.Add(Me.LabelControl9)
        Me.SidePanel1.Controls.Add(Me.LabelControl8)
        Me.SidePanel1.Controls.Add(Me.ComboPNYear)
        Me.SidePanel1.Controls.Add(Me.ComboPNMonth)
        Me.SidePanel1.Controls.Add(Me.ComboPNDate)
        Me.SidePanel1.Controls.Add(Me.ComboFNYear)
        Me.SidePanel1.Controls.Add(Me.ComboFNMonth)
        Me.SidePanel1.Controls.Add(Me.ComboFNDate)
        Me.SidePanel1.Controls.Add(Me.LookUpEdit1)
        Me.SidePanel1.Controls.Add(Me.ToggleSwitch1)
        Me.SidePanel1.Controls.Add(Me.LabelControl7)
        Me.SidePanel1.Controls.Add(Me.SimpleButton1)
        Me.SidePanel1.Controls.Add(Me.LabelControl6)
        Me.SidePanel1.Controls.Add(Me.TextEdit3)
        Me.SidePanel1.Controls.Add(Me.LabelControl5)
        Me.SidePanel1.Controls.Add(Me.DateEdit2)
        Me.SidePanel1.Controls.Add(Me.LabelControl3)
        Me.SidePanel1.Controls.Add(Me.TextEdit2)
        Me.SidePanel1.Controls.Add(Me.LabelControl4)
        Me.SidePanel1.Controls.Add(Me.DateEdit1)
        Me.SidePanel1.Controls.Add(Me.LabelControl2)
        Me.SidePanel1.Controls.Add(Me.TextEditRemark)
        Me.SidePanel1.Controls.Add(Me.LabelControl1)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel1.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1165, 132)
        Me.SidePanel1.TabIndex = 13
        Me.SidePanel1.Text = "SidePanel1"
        '
        'TextOT
        '
        Me.TextOT.EditValue = "00:00"
        Me.TextOT.Location = New System.Drawing.Point(697, 15)
        Me.TextOT.Name = "TextOT"
        Me.TextOT.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextOT.Properties.Appearance.Options.UseFont = True
        Me.TextOT.Properties.Mask.EditMask = "HH:mm"
        Me.TextOT.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextOT.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextOT.Properties.MaxLength = 5
        Me.TextOT.Size = New System.Drawing.Size(72, 20)
        Me.TextOT.TabIndex = 41
        Me.TextOT.Visible = False
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(614, 18)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl10.TabIndex = 42
        Me.LabelControl10.Text = "Over Time"
        Me.LabelControl10.Visible = False
        '
        'SimpleButtonExcel
        '
        Me.SimpleButtonExcel.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonExcel.Appearance.Options.UseFont = True
        Me.SimpleButtonExcel.Location = New System.Drawing.Point(826, 18)
        Me.SimpleButtonExcel.Name = "SimpleButtonExcel"
        Me.SimpleButtonExcel.Size = New System.Drawing.Size(85, 23)
        Me.SimpleButtonExcel.TabIndex = 9
        Me.SimpleButtonExcel.Text = "Excel Upload"
        '
        'ComboPurpose
        '
        Me.ComboPurpose.Location = New System.Drawing.Point(393, 88)
        Me.ComboPurpose.Name = "ComboPurpose"
        Me.ComboPurpose.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboPurpose.Properties.Appearance.Options.UseFont = True
        Me.ComboPurpose.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboPurpose.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboPurpose.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboPurpose.Properties.Items.AddRange(New Object() {"Official", "Personal", "Out Door"})
        Me.ComboPurpose.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboPurpose.Size = New System.Drawing.Size(114, 20)
        Me.ComboPurpose.TabIndex = 6
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(333, 91)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(44, 14)
        Me.LabelControl9.TabIndex = 40
        Me.LabelControl9.Text = "Purpose"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(541, 91)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(41, 14)
        Me.LabelControl8.TabIndex = 39
        Me.LabelControl8.Text = "Remark"
        '
        'ComboPNYear
        '
        Me.ComboPNYear.Enabled = False
        Me.ComboPNYear.Location = New System.Drawing.Point(544, 47)
        Me.ComboPNYear.Name = "ComboPNYear"
        Me.ComboPNYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboPNYear.Properties.Appearance.Options.UseFont = True
        Me.ComboPNYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboPNYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboPNYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboPNYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboPNYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboPNYear.TabIndex = 38
        '
        'ComboPNMonth
        '
        Me.ComboPNMonth.Enabled = False
        Me.ComboPNMonth.Location = New System.Drawing.Point(472, 47)
        Me.ComboPNMonth.Name = "ComboPNMonth"
        Me.ComboPNMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboPNMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboPNMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboPNMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboPNMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboPNMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboPNMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboPNMonth.TabIndex = 37
        '
        'ComboPNDate
        '
        Me.ComboPNDate.Enabled = False
        Me.ComboPNDate.Location = New System.Drawing.Point(424, 47)
        Me.ComboPNDate.Name = "ComboPNDate"
        Me.ComboPNDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboPNDate.Properties.Appearance.Options.UseFont = True
        Me.ComboPNDate.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboPNDate.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboPNDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboPNDate.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboPNDate.Size = New System.Drawing.Size(42, 20)
        Me.ComboPNDate.TabIndex = 36
        '
        'ComboFNYear
        '
        Me.ComboFNYear.Location = New System.Drawing.Point(544, 15)
        Me.ComboFNYear.Name = "ComboFNYear"
        Me.ComboFNYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboFNYear.Properties.Appearance.Options.UseFont = True
        Me.ComboFNYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboFNYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboFNYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboFNYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboFNYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboFNYear.TabIndex = 4
        '
        'ComboFNMonth
        '
        Me.ComboFNMonth.Location = New System.Drawing.Point(472, 15)
        Me.ComboFNMonth.Name = "ComboFNMonth"
        Me.ComboFNMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboFNMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboFNMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboFNMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboFNMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboFNMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboFNMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboFNMonth.TabIndex = 3
        '
        'ComboFNDate
        '
        Me.ComboFNDate.Location = New System.Drawing.Point(424, 15)
        Me.ComboFNDate.Name = "ComboFNDate"
        Me.ComboFNDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboFNDate.Properties.Appearance.Options.UseFont = True
        Me.ComboFNDate.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboFNDate.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboFNDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboFNDate.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboFNDate.Size = New System.Drawing.Size(42, 20)
        Me.ComboFNDate.TabIndex = 2
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.Location = New System.Drawing.Point(99, 15)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PAYCODE", "PAYCODE", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EMPNAME", "Name")})
        Me.LookUpEdit1.Properties.DataSource = Me.TblEmployeeBindingSource
        Me.LookUpEdit1.Properties.DisplayMember = "PAYCODE"
        Me.LookUpEdit1.Properties.MaxLength = 12
        Me.LookUpEdit1.Properties.NullText = ""
        Me.LookUpEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEdit1.Properties.ValueMember = "PAYCODE"
        Me.LookUpEdit1.Size = New System.Drawing.Size(163, 20)
        Me.LookUpEdit1.TabIndex = 1
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ToggleSwitch1
        '
        Me.ToggleSwitch1.Enabled = False
        Me.ToggleSwitch1.Location = New System.Drawing.Point(191, 86)
        Me.ToggleSwitch1.Name = "ToggleSwitch1"
        Me.ToggleSwitch1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch1.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch1.Properties.OffText = "No"
        Me.ToggleSwitch1.Properties.OnText = "Yes"
        Me.ToggleSwitch1.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch1.TabIndex = 24
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(18, 91)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(121, 14)
        Me.LabelControl7.TabIndex = 23
        Me.LabelControl7.Text = "Round the clock work"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Enabled = False
        Me.SimpleButton1.Location = New System.Drawing.Point(826, 47)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(85, 23)
        Me.SimpleButton1.TabIndex = 8
        Me.SimpleButton1.Text = "Post Punch"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Appearance.Options.UseForeColor = True
        Me.LabelControl6.Location = New System.Drawing.Point(626, 18)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(44, 14)
        Me.LabelControl6.TabIndex = 22
        Me.LabelControl6.Text = "           "
        '
        'TextEdit3
        '
        Me.TextEdit3.EditValue = "00:00"
        Me.TextEdit3.Location = New System.Drawing.Point(697, 47)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit3.Properties.Appearance.Options.UseFont = True
        Me.TextEdit3.Properties.Mask.EditMask = "HH:mm"
        Me.TextEdit3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit3.Properties.MaxLength = 5
        Me.TextEdit3.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit3.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(614, 50)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(65, 14)
        Me.LabelControl5.TabIndex = 21
        Me.LabelControl5.Text = "Punch Time"
        '
        'DateEdit2
        '
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Enabled = False
        Me.DateEdit2.Location = New System.Drawing.Point(438, 47)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit2.Properties.Appearance.Options.UseFont = True
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Size = New System.Drawing.Size(110, 20)
        Me.DateEdit2.TabIndex = 20
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(333, 50)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl3.TabIndex = 19
        Me.LabelControl3.Text = "Punch Date"
        '
        'TextEdit2
        '
        Me.TextEdit2.Enabled = False
        Me.TextEdit2.Location = New System.Drawing.Point(99, 47)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEdit2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEdit2.Properties.MaxLength = 12
        Me.TextEdit2.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit2.TabIndex = 18
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(18, 50)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(71, 14)
        Me.LabelControl4.TabIndex = 17
        Me.LabelControl4.Text = "Card Number"
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(438, 15)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit1.Properties.Appearance.Options.UseFont = True
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Size = New System.Drawing.Size(110, 20)
        Me.DateEdit1.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(333, 18)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(72, 14)
        Me.LabelControl2.TabIndex = 15
        Me.LabelControl2.Text = "Process From"
        '
        'TextEditRemark
        '
        Me.TextEditRemark.Location = New System.Drawing.Point(611, 88)
        Me.TextEditRemark.Name = "TextEditRemark"
        Me.TextEditRemark.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRemark.Properties.Appearance.Options.UseFont = True
        Me.TextEditRemark.Properties.MaxLength = 12
        Me.TextEditRemark.Size = New System.Drawing.Size(227, 20)
        Me.TextEditRemark.TabIndex = 7
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(18, 18)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl1.TabIndex = 13
        Me.LabelControl1.Text = "PayCode"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(77, 568)
        Me.MemoEdit1.TabIndex = 4
        '
        'TblTimeRegisterBindingSource1
        '
        Me.TblTimeRegisterBindingSource1.DataMember = "tblTimeRegister"
        Me.TblTimeRegisterBindingSource1.DataSource = Me.SSSDBDataSet
        '
        'TblTimeRegisterBindingSource
        '
        Me.TblTimeRegisterBindingSource.DataMember = "tblTimeRegister"
        Me.TblTimeRegisterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblTimeRegisterTableAdapter
        '
        Me.TblTimeRegisterTableAdapter.ClearBeforeFill = True
        '
        'MachineRawPunchBindingSource
        '
        Me.MachineRawPunchBindingSource.DataMember = "MachineRawPunch"
        Me.MachineRawPunchBindingSource.DataSource = Me.SSSDBDataSet
        '
        'MachineRawPunchTableAdapter
        '
        Me.MachineRawPunchTableAdapter.ClearBeforeFill = True
        '
        'TblLeaveMaster1TableAdapter1
        '
        Me.TblLeaveMaster1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'XtraManualEntry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraManualEntry"
        Me.Size = New System.Drawing.Size(1250, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        Me.SidePanel3.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit3.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel2.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit4.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        CType(Me.TextOT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboPurpose.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboPNYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboPNMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboPNDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboFNYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboFNMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboFNDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRemark.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblTimeRegisterBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblTimeRegisterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MachineRawPunchBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRemark As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SidePanel3 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblTimeRegisterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblTimeRegisterTableAdapter As iAS.SSSDBDataSetTableAdapters.tblTimeRegisterTableAdapter
    Friend WithEvents TblTimeRegisterBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents colDateOFFICE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTATUS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTATTENDED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIN1_date As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIN2_date As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOUT1_date As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLATEARRIVAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents MachineRawPunchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents colOFFICEPUNCH As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents colOFFICEPUNCH1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents colP_DAY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents MachineRawPunchTableAdapter As iAS.SSSDBDataSetTableAdapters.MachineRawPunchTableAdapter
    Friend WithEvents TblLeaveMaster1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colIN1_time As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOUT2_date As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOUT2_time As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOUT1_time As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIN2_time As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitch1 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents BehaviorManager1 As DevExpress.Utils.Behaviors.BehaviorManager
    Friend WithEvents EARLYDEPARTURE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHOURSWORKED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployeeTableAdapter As iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblEmployee1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents ComboPNYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboPNMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboPNDate As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboFNYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboFNMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboFNDate As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboPurpose As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonExcel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colISMANUAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDURATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TextOT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
End Class
