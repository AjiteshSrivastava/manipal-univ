﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors

Public Class XtraDashBoardClickGrid
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim ulf As UserLookAndFeel
    Dim adap As SqlDataAdapter
    Dim adapA As OleDbDataAdapter
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
    End Sub
    Private Sub XtraDashBoardClickGrid_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If Common.dashBoardClick = "WO" Then
            laodWOList()
        End If
    End Sub
    Private Sub laodWOList()
        GridControl1.DataSource = Nothing
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("Paycode")
        dt.Columns.Add("Present Card No")
        dt.Columns.Add("Employee Name")

        Dim WhereClause As String = ""
        'If Common.USERTYPE <> "A" Then
        '    Dim emp() As String = Common.Auth_Branch.Split(",")
        '    Dim ls As New List(Of String)()
        '    For x As Integer = 0 To emp.Length - 1
        '        ls.Add(emp(x).Trim)
        '    Next
        '    If WhereClause = "" Then
        '        WhereClause = " TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
        '    Else
        '        WhereClause = WhereClause & " and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
        '    End If
        'End If

        If Common.USERTYPE <> "A" Then
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next

            Dim com() As String = Common.auth_comp.Split(",")
            Dim lsC As New List(Of String)()
            For x As Integer = 0 To com.Length - 1
                lsC.Add(com(x).Trim)
            Next


            If WhereClause = "" Then
                WhereClause = "and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsC.ToArray()) & "') and  TBLEmployee.ACTIVE='Y'"
            Else
                WhereClause = WhereClause & " and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsC.ToArray()) & "') and  TBLEmployee.ACTIVE='Y'"
            End If
        End If
        Application.DoEvents()
        Dim c As Common = New Common()
        Dim sSql As String '= "select * from tblTimeRegister WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and PRESENTVALUE > 0"
        Dim ds As DataSet = New DataSet
        ds = New DataSet
        If Common.servername = "Access" Then
            sSql = "select TblEmployee.PAYCODE, TblEmployee.PRESENTCARDNO, TblEmployee.EMPNAME from tblTimeRegister, TblEmployee WHERE FORMAT(DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and WO_VALUE>0 and tblTimeRegister.PAYCODE=TblEmployee.PAYCODE " & WhereClause
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            sSql = "select TblEmployee.PAYCODE, TblEmployee.PRESENTCARDNO, TblEmployee.EMPNAME from tblTimeRegister, TblEmployee WHERE DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and WO_VALUE>0 and tblTimeRegister.PAYCODE=TblEmployee.PAYCODE " & WhereClause
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If

        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                dt.Rows.Add(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PRESENTCARDNO").ToString.Trim, ds.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim)
            Next
        Else
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
            Me.Close()
        End If
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridControl1.DataSource = dt
        GridView1.Columns(2).Width = 125

    End Sub
End Class