﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Runtime.InteropServices
Imports System.Security.Cryptography
Imports System.Text
Imports System.Threading.Tasks

Namespace B100Demo
    Public Class B100API
        Public Delegate Function P_ENROLL_CALLBACK(ByVal dev As String, ByVal userid As String, ByVal eyeKind As Integer, ByVal irisTemplate As String, ByVal enrollResult As Integer) As Integer
        Public Delegate Function P_MSG_CALLBACK(ByVal dev As String, ByVal operation As String, ByVal msg As String, ByVal level As Integer) As Integer
        Public Delegate Function P_RECORD_CALLBACK(ByVal ip As String, ByVal dev As String, ByVal userid As String, ByVal sWorksn As String, ByVal sName As String, ByVal idCard As String, ByVal icCard As String, ByVal datetime As String, ByVal recogtype As String) As Integer
        '  <DllImport("B100.dll", EntryPoint:="F_EnrollIrisAuto", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_EnrollIrisAuto Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal userid As String, ByVal staffid As String, ByVal userName As String, ByVal eye As Integer, ByVal strMode As Integer) As Integer
        ' <DllImport("B100.dll", EntryPoint:="F_EnrollIris", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_EnrollIris Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal userid As String, ByVal staffid As String, ByVal userName As String, ByVal eye As Integer, ByVal resultBuf As IntPtr, ByVal resultLen As Integer) As Integer
        '<DllImport("B100.dll", EntryPoint:="F_SendUser", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_SendUser Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal userinfo As String, ByVal resultBuf As IntPtr, ByVal resultLen As Integer) As Integer
        '<DllImport("B100.dll", EntryPoint:="F_Init", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_Init Lib "B100.dll" (ByVal enrollFunc As P_ENROLL_CALLBACK, ByVal msgFunc As P_MSG_CALLBACK, ByVal recordFunc As P_RECORD_CALLBACK) As Integer
        '<DllImport("B100.dll", EntryPoint:="F_StartLog", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Sub F_StartLog Lib "B100.dll" (ByVal enabled As Integer)
        '<DllImport("B100.dll", EntryPoint:="F_StartRecord", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_StartRecord Lib "B100.dll" (ByVal ip As String, ByVal port As Integer) As Integer
        '<DllImport("B100.dll", EntryPoint:="F_StopRecord", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_StopRecord Lib "B100.dll" () As Integer
        '<DllImport("B100.dll", EntryPoint:="F_SetMode", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_SetMode Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal Mode As Integer, ByVal resultBuf As IntPtr, ByVal resultLen As Integer) As Integer
        '<DllImport("B100.dll", EntryPoint:="F_DeleteUsers", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_DeleteUsers Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal userCount As Integer, ByVal dataType As String, ByVal userIdList As String, ByVal resultBuf As IntPtr, ByVal resultLen As Integer) As Integer
        '<DllImport("B100.dll", EntryPoint:="F_DeleteUsers", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_DeleteUsers Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal userIdList As String, ByVal resultBuf As IntPtr, ByVal resultLen As Integer) As Integer
        '<DllImport("B100.dll", EntryPoint:="F_EraseAllUsers", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_EraseAllUsers Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal resultBuf As IntPtr, ByVal resultLen As Integer) As Integer
        '<DllImport("B100.dll", EntryPoint:="F_GetDevVerion", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_GetDevVerion Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal resultBuf As IntPtr, ByVal resultLe As Integer) As Integer
        '<DllImport("B100.dll", EntryPoint:="F_GetDevSerial", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_GetDevSerial Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal resultBuf As IntPtr, ByVal resultLe As Integer) As Integer
        ' <DllImport("B100.dll", EntryPoint:="F_ReadCard", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_ReadCard Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal cmd As Integer, ByVal resultBuf As IntPtr, ByVal resultLen As Integer) As Integer
        '<DllImport("B100.dll", EntryPoint:="F_SendMultUser", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_SendMultUser Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal userinfo As String, ByVal resultBuf As IntPtr, ByVal resultLen As Integer) As Integer
        ' <DllImport("B100.dll", EntryPoint:="F_GetUsersNumberCard", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_GetUsersNumberCard Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal userBuf As IntPtr, ByVal bufLen As Integer, ByVal resultBuf As IntPtr, ByVal resultLen As Integer) As Integer
        '<DllImport("B100.dll", EntryPoint:="F_GetEnrollStatus", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_GetEnrollStatus Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal userid As String, ByVal userNam As String, ByVal eye As Integer, ByVal resultBuf As IntPtr, ByVal resultLen As Integer) As Integer
        '<DllImport("B100.dll", EntryPoint:="F_GetEnrollIrisResult", CallingConvention:=CallingConvention.Cdecl)>
        Public Declare Function F_GetEnrollIrisResult Lib "B100.dll" (ByVal a20IP As String, ByVal a20Port As Integer, ByVal userId As String, ByVal staffid As String, ByVal username As String, ByVal eye As Integer, ByVal irisTemplate As IntPtr, ByVal irislen As Integer, ByVal resultBuf As IntPtr, ByVal resultLen As Integer) As Integer
    End Class
End Namespace
