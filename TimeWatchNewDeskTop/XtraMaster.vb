﻿Imports System.Resources
Imports System.Globalization
Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports DevExpress.XtraEditors
Imports DevExpress.LookAndFeel
Imports System.Net.Mail
Imports DevExpress.Skins.XtraForm
Imports DevExpress.Utils
Imports DevExpress.Skins
Imports System.Threading

Public Class XtraMaster
    Dim ulf As UserLookAndFeel
    Public Shared realTIme As String
    Public Sub New()
        InitializeComponent()

        Me.WindowState = FormWindowState.Maximized
        'Me.Top = (My.Computer.Screen.WorkingArea.Height / 2) - (Me.Height / 2)
        'Me.Left = (My.Computer.Screen.WorkingArea.Width / 2) - (Me.Width / 2)
        'Me.Width = My.Computer.Screen.WorkingArea.Width
        'Me.Height = My.Computer.Screen.WorkingArea.Height
        'PanelControl2.Width = My.Computer.Screen.WorkingArea.Width
        'PanelControl2.Height = My.Computer.Screen.WorkingArea.Height - SidePanel1.Height
        'Common.tmpwidth = PanelControl2.Width
        'Common.tmpheight = PanelControl2.Height

        Me.Width = My.Computer.Screen.WorkingArea.Width
        Me.Height = My.Computer.Screen.WorkingArea.Height
        PanelControl1.Width = My.Computer.Screen.WorkingArea.Width
        PanelControl1.Height = OfficeNavigationBar1.Height - My.Computer.Screen.WorkingArea.Height
        OfficeNavigationBar1.Width = My.Computer.Screen.WorkingArea.Width
        PanelControl1.Width = My.Computer.Screen.WorkingArea.Width
        PanelControl1.Height = My.Computer.Screen.WorkingArea.Height - 70
        PanelControl2.Width = My.Computer.Screen.WorkingArea.Width
        PanelControl2.Height = My.Computer.Screen.WorkingArea.Height - SidePanel1.Height
    End Sub
    'Protected Overrides Function CreateFormBorderPainter() As DevExpress.Skins.XtraForm.FormPainter
    '    Dim formCaptionAlignment As HorzAlignment = HorzAlignment.Center
    '    Return New CustomFormPainter(Me, LookAndFeel, formCaptionAlignment)
    'End Function
    Private Sub XtraMaster_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Dim msg As New Message()
        'AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        Me.WindowState = FormWindowState.Maximized
        'Me.Top = (My.Computer.Screen.WorkingArea.Height / 2) - (Me.Height / 2)
        'Me.Left = (My.Computer.Screen.WorkingArea.Width / 2) - (Me.Width / 2)
        Me.Width = My.Computer.Screen.WorkingArea.Width
        Me.Height = My.Computer.Screen.WorkingArea.Height
        PanelControl1.Width = My.Computer.Screen.WorkingArea.Width
        PanelControl1.Height = OfficeNavigationBar1.Height - My.Computer.Screen.WorkingArea.Height

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraMaster).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")
        'Me.Text = Common.res_man.GetString("timewatch", Common.cul)
        Me.Text = "Integrated Attendance System"
        LabelControl2.Text = " Application Login Time " & Now.ToString("dd MMM yyyy HH:mm")
        LabelControlUser.Text = Common.USER_R
        Application.DoEvents()
        'OfficeNavigationBar1.Items.Item(0).Text = Common.res_man.GetString("home_title", Common.cul)
        'OfficeNavigationBar1.Items.Item(1).Text = Common.res_man.GetString("device_tl", Common.cul)
        'OfficeNavigationBar1.Items.Item(2).Text = Common.res_man.GetString("master_tl", Common.cul)
        'OfficeNavigationBar1.Items.Item(3).Text = Common.res_man.GetString("employee_tl", Common.cul)
        'OfficeNavigationBar1.Items.Item(4).Text = Common.res_man.GetString("dataprocess_tl", Common.cul)
        'OfficeNavigationBar1.Items.Item(5).Text = Common.res_man.GetString("leavemgmt_tl", Common.cul)
        'OfficeNavigationBar1.Items.Item(6).Text = Common.res_man.GetString("reports_tl", Common.cul)

        OfficeNavigationBar1.Width = My.Computer.Screen.WorkingArea.Width
        PanelControl1.Width = My.Computer.Screen.WorkingArea.Width
        PanelControl1.Height = My.Computer.Screen.WorkingArea.Height - 70

        PanelControl2.Width = My.Computer.Screen.WorkingArea.Width
        PanelControl2.Height = My.Computer.Screen.WorkingArea.Height - SidePanel1.Height
        PanelControl2.Controls.Clear()
        Dim form As UserControl = New XtraHomeNew 'XtraHome
        form.Dock = DockStyle.Fill
        PanelControl2.Controls.Add(form)

        If Common.Main = "Y" Then
            OfficeNavigationBar1.Items.Item(2).Visible = True
        Else
            OfficeNavigationBar1.Items.Item(2).Visible = False
        End If
        If Common.Admin = "Y" Then
            OfficeNavigationBar1.Items.Item(7).Visible = True
        Else
            OfficeNavigationBar1.Items.Item(7).Visible = False
        End If
        If Common.Reports = "Y" Then
            OfficeNavigationBar1.Items.Item(8).Visible = True
        Else
            OfficeNavigationBar1.Items.Item(8).Visible = False
        End If
        'If Common.Payroll = "Y" Then
        '    OfficeNavigationBar1.Items.Item(7).Visible = True
        'Else
        '    OfficeNavigationBar1.Items.Item(7).Visible = False
        'End If
        If Common.Leave = "Y" Then
            OfficeNavigationBar1.Items.Item(6).Visible = True
        Else
            OfficeNavigationBar1.Items.Item(6).Visible = False
        End If
        If Common.V_Transaction = "Y" Then
            OfficeNavigationBar1.Items.Item(5).Visible = True
        Else
            OfficeNavigationBar1.Items.Item(5).Visible = False
        End If

        If Common.Payroll = "Y" Then
            OfficeNavigationBar1.Items.Item(9).Visible = True
        Else
            OfficeNavigationBar1.Items.Item(9).Visible = False
        End If
        Application.DoEvents()
        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = " Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tb" & _
        "lsetup )"
        If Common.servername = "Access" Then
            sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
            adapAc = New OleDbDataAdapter(sSql, Common.con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, Common.con)
            adapS.Fill(Rs)
        End If


        ''CreateEmpPunchDwnld
        'Try
        '    Dim tmp = Rs.Tables(0).Rows(0).Item("CreateEmpPunchDwnld").ToString.Trim
        'Catch ex As Exception
        '    sSql = "ALTER TABLE tblsetup ADD CreateEmpPunchDwnld char(1)"
        '    If Common.servername = "Access" Then
        '        If Common.con1.State <> ConnectionState.Open Then
        '            Common.con1.Open()
        '        End If
        '        Dim cmd1 As OleDbCommand
        '        cmd1 = New OleDbCommand(sSql, Common.con1)
        '        cmd1.ExecuteNonQuery()
        '        cmd1 = New OleDbCommand("update tblSetup set CreateEmpPunchDwnld = 'N'", Common.con1)
        '        cmd1.ExecuteNonQuery()
        '        If Common.con1.State <> ConnectionState.Closed Then
        '            Common.con1.Close()
        '        End If
        '    Else
        '        If Common.con.State <> ConnectionState.Open Then
        '            Common.con.Open()
        '        End If
        '        Dim cmd As SqlCommand
        '        cmd = New SqlCommand(sSql, Common.con)
        '        cmd.ExecuteNonQuery()
        '        cmd = New SqlCommand("update tblSetup set CreateEmpPunchDwnld = 'N'", Common.con)
        '        cmd.ExecuteNonQuery()
        '        If Common.con.State <> ConnectionState.Closed Then
        '            Common.con.Close()
        '        End If
        '    End If
        'End Try

        ''StartRealTime
        'Try
        '    realTIme = Rs.Tables(0).Rows(0).Item("StartRealTime").ToString.Trim
        'Catch ex As Exception
        '    sSql = "ALTER TABLE tblsetup ADD StartRealTime char(1)"
        '    If Common.servername = "Access" Then
        '        If Common.con1.State <> ConnectionState.Open Then
        '            Common.con1.Open()
        '        End If
        '        Dim cmd1 As OleDbCommand
        '        cmd1 = New OleDbCommand(sSql, Common.con1)
        '        cmd1.ExecuteNonQuery()
        '        cmd1 = New OleDbCommand("update tblSetup set StartRealTime = 'Y'", Common.con1)
        '        cmd1.ExecuteNonQuery()
        '        If Common.con1.State <> ConnectionState.Closed Then
        '            Common.con1.Close()
        '        End If
        '    Else
        '        If Common.con.State <> ConnectionState.Open Then
        '            Common.con.Open()
        '        End If
        '        Dim cmd As SqlCommand
        '        cmd = New SqlCommand(sSql, Common.con)
        '        cmd.ExecuteNonQuery()
        '        cmd = New SqlCommand("update tblSetup set StartRealTime = 'Y'", Common.con)
        '        cmd.ExecuteNonQuery()
        '        If Common.con.State <> ConnectionState.Closed Then
        '            Common.con.Close()
        '        End If
        '    End If
        '    realTIme = "Y"
        'End Try
       
        ''sSql = "ALTER TABLE tblTimeRegister ALTER COLUMN PAYCODE LongText"
        ''If Common.servername = "Access" Then
        ''    If Common.con1.State <> ConnectionState.Open Then
        ''        Common.con1.Open()
        ''    End If
        ''    Dim cmd1 As OleDbCommand

        ''    'cmd1 = New OleDbCommand("select top 1 * from tblTimeRegister", Common.con1)
        ''    'Dim rdr As OleDbDataReader = cmd1.ExecuteReader()
        ''    'Dim Type As System.Type = rdr.GetFieldType(0)
        ''    'MsgBox(Type.MemberType)

        ''    cmd1 = New OleDbCommand(sSql, Common.con1)
        ''    cmd1.ExecuteNonQuery()
        ''    If Common.con1.State <> ConnectionState.Closed Then
        ''        Common.con1.Close()
        ''    End If
        ''Else
        ''    'If Common.con.State <> ConnectionState.Open Then
        ''    '    Common.con.Open()
        ''    'End If
        ''    'Dim cmd As SqlCommand
        ''    'cmd = New SqlCommand(sSql, Common.con)
        ''    'cmd.ExecuteNonQuery()
        ''    'If Common.con.State <> ConnectionState.Closed Then
        ''    '    Common.con.Close()
        ''    'End If
        ''End If

        If Rs.Tables(0).Rows(0).Item("Visitor").ToString.Trim = "Y" Then
            If Common.Visitor = "Y" Then
                OfficeNavigationBar1.Items.Item(11).Visible = True
            Else
                OfficeNavigationBar1.Items.Item(11).Visible = False
            End If
        Else
            OfficeNavigationBar1.Items.Item(11).Visible = False
        End If

        If Rs.Tables(0).Rows(0).Item("canteen").ToString.Trim = "Y" Then
            If Common.canteen = "Y" Then
                OfficeNavigationBar1.Items.Item(10).Visible = True
            Else
                OfficeNavigationBar1.Items.Item(10).Visible = False
            End If
        Else
            OfficeNavigationBar1.Items.Item(10).Visible = False
        End If


        ''shiftearly, shiftlate
        'sSql = "select shiftearly, shiftlate from tblShiftMaster"
        'Dim Rs1 = New DataSet
        'If Common.servername = "Access" Then
        '    Try
        '        adapAc = New OleDbDataAdapter(sSql, Common.con1)
        '        adapAc.Fill(Rs1)
        '    Catch ex As Exception
        '        sSql = "ALTER TABLE tblShiftMaster ADD ShiftEarly int, ShiftLate int "
        '        If Common.con1.State <> ConnectionState.Open Then
        '            Common.con1.Open()
        '        End If
        '        Dim cmd1 As OleDbCommand
        '        cmd1 = New OleDbCommand(sSql, Common.con1)
        '        cmd1.ExecuteNonQuery()
        '        cmd1 = New OleDbCommand("update tblShiftMaster set ShiftEarly = 0, ShiftLate = 0", Common.con1)
        '        cmd1.ExecuteNonQuery()
        '        If Common.con1.State <> ConnectionState.Closed Then
        '            Common.con1.Close()
        '        End If
        '    End Try
        'Else
        '    Try
        '        adapS = New SqlDataAdapter(sSql, Common.con)
        '        adapS.Fill(Rs1)
        '    Catch ex As Exception
        '        sSql = "ALTER TABLE tblShiftMaster ADD ShiftEarly int, ShiftLate int "
        '        If Common.con.State <> ConnectionState.Open Then
        '            Common.con.Open()
        '        End If
        '        Dim cmd As SqlCommand
        '        cmd = New SqlCommand(sSql, Common.con)
        '        cmd.ExecuteNonQuery()
        '        cmd = New SqlCommand("update tblShiftMaster set ShiftEarly = 0, ShiftLate = 0", Common.con)
        '        cmd.ExecuteNonQuery()
        '        If Common.con.State <> ConnectionState.Closed Then
        '            Common.con.Close()
        '        End If
        '    End Try
        'End If


        ''CommKey
        'If Common.servername = "Access" Then
        '    If Common.con1.State <> ConnectionState.Open Then
        '        Common.con1.Open()
        '    End If
        '    Dim cmd1 As OleDbCommand
        '    cmd1 = New OleDbCommand("update tblMachine set CommKey = 0 where (DeviceType ='ZK(TFT)' or DeviceType ='Bio-1Pro/ATF305Pro/ATF686Pro') and commkey is NULL", Common.con1)
        '    cmd1.ExecuteNonQuery()
        '    If Common.con1.State <> ConnectionState.Closed Then
        '        Common.con1.Close()
        '    End If
        'Else
        '    If Common.con.State <> ConnectionState.Open Then
        '        Common.con.Open()
        '    End If
        '    Dim cmd As SqlCommand
        '    cmd = New SqlCommand("update tblMachine set CommKey = 0 where (DeviceType ='ZK(TFT)' or DeviceType ='Bio-1Pro/ATF305Pro/ATF686Pro') and commkey is NULL", Common.con)
        '    cmd.ExecuteNonQuery()
        '    If Common.con.State <> ConnectionState.Closed Then
        '        Common.con.Close()
        '    End If
        'End If

        ''MARKMISSASHALFDAY
        'sSql = "select MARKMISSASHALFDAY from tblEmployeeShiftMaster"
        'Rs1 = New DataSet
        'Try
        '    If Common.servername = "Access" Then
        '        adapAc = New OleDbDataAdapter(sSql, Common.con1)
        '        adapAc.Fill(Rs1)
        '    Else
        '        adapS = New SqlDataAdapter(sSql, Common.con)
        '        adapS.Fill(Rs1)
        '    End If
        'Catch ex As Exception
        '    sSql = "ALTER TABLE tblEmployeeShiftMaster ADD MARKMISSASHALFDAY char(1)"
        '    Dim sSql1 As String = "ALTER TABLE EmployeeGroup ADD MARKMISSASHALFDAY char(1)"
        '    If Common.servername = "Access" Then
        '        If Common.con1.State <> ConnectionState.Open Then
        '            Common.con1.Open()
        '        End If
        '        Dim cmd1 As OleDbCommand
        '        cmd1 = New OleDbCommand(sSql, Common.con1)
        '        cmd1.ExecuteNonQuery()
        '        cmd1 = New OleDbCommand(sSql1, Common.con1)
        '        cmd1.ExecuteNonQuery()
        '        cmd1 = New OleDbCommand("update tblEmployeeShiftMaster set MARKMISSASHALFDAY='N'", Common.con1)
        '        cmd1.ExecuteNonQuery()
        '        cmd1 = New OleDbCommand("update EmployeeGroup set MARKMISSASHALFDAY='N'", Common.con1)
        '        cmd1.ExecuteNonQuery()
        '        If Common.con1.State <> ConnectionState.Closed Then
        '            Common.con1.Close()
        '        End If
        '    Else
        '        If Common.con.State <> ConnectionState.Open Then
        '            Common.con.Open()
        '        End If
        '        Dim cmd As SqlCommand
        '        cmd = New SqlCommand(sSql, Common.con)
        '        cmd.ExecuteNonQuery()
        '        cmd = New SqlCommand(sSql1, Common.con)
        '        cmd.ExecuteNonQuery()
        '        cmd = New SqlCommand("update tblEmployeeShiftMaster set MARKMISSASHALFDAY='N'", Common.con)
        '        cmd.ExecuteNonQuery()
        '        cmd = New SqlCommand("update EmployeeGroup set MARKMISSASHALFDAY='N'", Common.con)
        '        cmd.ExecuteNonQuery()
        '        If Common.con.State <> ConnectionState.Closed Then
        '            Common.con.Close()
        '        End If
        '    End If
        'End Try

        TimerDataProcess.Enabled = True
        'PanelControl2.Controls.Clear()
        'Dim form As UserControl = New XtraHomeNew 'XtraHome
        'form.Dock = DockStyle.Fill
        'PanelControl2.Controls.Add(form)
        'form.Show()
    End Sub
    'Private Sub OfficeNavigationBar1_ItemClick(sender As System.Object, e As DevExpress.XtraBars.Navigation.NavigationBarItemEventArgs) Handles OfficeNavigationBar1.ItemClick
    '    'MsgBox(e.Item.ToString)
    '    If e.Item.ToString = "Text = '" & Common.res_man.GetString("home_title", Common.cul) & "'" Then
    '        PanelControl1.Controls.Clear()
    '        Dim form As UserControl = New XtraHome
    '        'form.TopLevel = False
    '        'form.FormBorderStyle = FormBorderStyle.None
    '        form.Dock = DockStyle.Fill
    '        PanelControl1.Controls.Add(form)
    '        form.Show()
    '    End If
    '    If e.Item.ToString = "Text = '" & Common.res_man.GetString("device_tl", Common.cul) & "'" Then
    '        PanelControl1.Controls.Clear()
    '        Dim form As UserControl = New XtraDevice
    '        'form.TopLevel = False
    '        'form.FormBorderStyle = FormBorderStyle.None
    '        form.Dock = DockStyle.Fill
    '        PanelControl1.Controls.Add(form)
    '        form.Show()
    '    End If
    '    If e.Item.ToString = "Text = '" & Common.res_man.GetString("master_tl", Common.cul) & "'" Then
    '        PanelControl1.Controls.Clear()
    '        Dim form As UserControl = New XtraMasterMenu
    '        'form.TopLevel = False
    '        'form.FormBorderStyle = FormBorderStyle.None
    '        form.Dock = DockStyle.Fill
    '        PanelControl1.Controls.Add(form)
    '        form.Show()
    '    End If
    '    If e.Item.ToString = "Text = '" & Common.res_man.GetString("employee_tl", Common.cul) & "'" Then
    '        PanelControl1.Controls.Clear()
    '        Dim form As UserControl = New XtraEmployee
    '        'form.TopLevel = False
    '        'form.FormBorderStyle = FormBorderStyle.None
    '        form.Dock = DockStyle.Fill
    '        PanelControl1.Controls.Add(form)
    '        form.Show()
    '    End If
    '    If e.Item.ToString = "Text = '" & Common.res_man.GetString("dataprocess_tl", Common.cul) & "'" Then
    '        PanelControl1.Controls.Clear()
    '        Dim form As UserControl = New XtraDataProcess
    '        'form.TopLevel = False
    '        'form.FormBorderStyle = FormBorderStyle.None
    '        form.Dock = DockStyle.Fill
    '        PanelControl1.Controls.Add(form)
    '        form.Show()
    '    End If
    'End Sub
    Private Sub XtraMaster_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If e.CloseReason = CloseReason.UserClosing Then
            If XtraMessageBox.Show(ulf, "<size=10>Are you sure to close the application?</size>", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.Yes Then
                e.Cancel = True
            End If
        End If
    End Sub
    Private Sub OfficeNavigationBar1_SelectedItemChanged(sender As System.Object, e As DevExpress.XtraBars.Navigation.NavigationBarItemEventArgs) Handles OfficeNavigationBar1.SelectedItemChanged
        'MsgBox(e.Item.ToString)
        If e.Item.ToString = "Text = '" & Common.res_man.GetString("home_title", Common.cul) & "'" Then
            'PanelControl1.Controls.Clear()
            PanelControl2.Controls.Clear()
            Dim form As UserControl = New XtraHomeNew 'XtraHome
            'form.TopLevel = False
            'form.FormBorderStyle = FormBorderStyle.None
            form.Dock = DockStyle.Fill
            PanelControl2.Controls.Add(form)
            form.Show()
        End If
        If e.Item.ToString = "Text = '" & Common.res_man.GetString("device_tl", Common.cul) & "'" Then
            PanelControl2.Controls.Clear()
            Dim form As UserControl = New XtraDevice
            'form.TopLevel = False
            'form.FormBorderStyle = FormBorderStyle.None
            form.Dock = DockStyle.Fill
            PanelControl2.Controls.Add(form)
            form.Show()
        End If
        If e.Item.ToString = "Text = '" & Common.res_man.GetString("master_tl", Common.cul) & "'" Then
            PanelControl2.Controls.Clear()
            Dim form As UserControl = New XtraMasterMenu
            'form.TopLevel = False
            'form.FormBorderStyle = FormBorderStyle.None
            form.Dock = DockStyle.Fill
            PanelControl2.Controls.Add(form)
            form.Show()
        End If
        If e.Item.ToString = "Text = '" & Common.res_man.GetString("employee_tl", Common.cul) & "'" Then
            PanelControl2.Controls.Clear()
            Dim form As UserControl = New XtraEmployee
            'form.TopLevel = False
            'form.FormBorderStyle = FormBorderStyle.None
            form.Dock = DockStyle.Fill
            PanelControl2.Controls.Add(form)
            form.Show()
        End If
        If e.Item.ToString = "Text = '" & Common.res_man.GetString("dataprocess_tl", Common.cul) & "'" Then
            PanelControl2.Controls.Clear()
            Dim form As UserControl = New XtraDataProcess
            'form.TopLevel = False
            'form.FormBorderStyle = FormBorderStyle.None
            form.Dock = DockStyle.Fill
            PanelControl2.Controls.Add(form)
            form.Show()
        End If
        If e.Item.ToString = "Text = '" & Common.res_man.GetString("reports_tl", Common.cul) & "'" Then
            PanelControl2.Controls.Clear()
            Dim form As UserControl = New XtraReportMenu 'New XtraReports '
            'form.TopLevel = False
            'form.FormBorderStyle = FormBorderStyle.None
            form.Dock = DockStyle.Fill
            PanelControl2.Controls.Add(form)
            form.Show()
        End If
        If e.Item.ToString = "Text = '" & Common.res_man.GetString("leavemgmt_tl", Common.cul) & "'" Then
            PanelControl2.Controls.Clear()
            Dim form As UserControl = New XtraLeaveMgmt
            'form.TopLevel = False
            'form.FormBorderStyle = FormBorderStyle.None
            form.Dock = DockStyle.Fill
            PanelControl2.Controls.Add(form)
            form.Show()
        End If
        If e.Item.ToString = "Text = 'Admin'" Then
            PanelControl2.Controls.Clear()
            Dim form As UserControl = New XtraAdminMenu
            'form.TopLevel = False
            'form.FormBorderStyle = FormBorderStyle.None
            form.Dock = DockStyle.Fill
            PanelControl2.Controls.Add(form)
            form.Show()
        End If
        If e.Item.ToString = "Text = 'Transaction'" Then
            PanelControl2.Controls.Clear()
            Dim form As UserControl = New XtraTransactionMenu
            'form.TopLevel = False
            'form.FormBorderStyle = FormBorderStyle.None
            form.Dock = DockStyle.Fill
            PanelControl2.Controls.Add(form)
            form.Show()
        End If
        If e.Item.ToString = "Text = 'Payroll'" Then
            PanelControl2.Controls.Clear()
            Dim form As UserControl = New XtraPayrollMgmtMenu
            'form.TopLevel = False
            'form.FormBorderStyle = FormBorderStyle.None
            form.Dock = DockStyle.Fill
            PanelControl2.Controls.Add(form)
            form.Show()
        End If
        If e.Item.ToString = "Text = 'Canteen'" Then
            PanelControl2.Controls.Clear()
            Dim form As UserControl = New XtraCanteenMenu
            'form.TopLevel = False
            'form.FormBorderStyle = FormBorderStyle.None
            form.Dock = DockStyle.Fill
            PanelControl2.Controls.Add(form)
            form.Show()
        End If
        If e.Item.ToString = "Text = 'Visitor'" Then
            PanelControl2.Controls.Clear()
            Dim form As UserControl = New XtraVisitorMenu
            'form.TopLevel = False
            'form.FormBorderStyle = FormBorderStyle.None
            form.Dock = DockStyle.Fill
            PanelControl2.Controls.Add(form)
            form.Show()
        End If
    End Sub
    Private Sub XtraMaster_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            If Application.OpenForms().OfType(Of XtraRealTimePunches).Any Then  'to check real time is runing or not
                XtraRealTimePunches.Close()
            End If
            Application.Exit()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub TimerShowClock_Tick(sender As System.Object, e As System.EventArgs) Handles TimerShowClock.Tick
        LabelControlTime.Text = Now.ToString("HH:mm")
    End Sub
    Private Sub TimerReportEmail_Tick(sender As System.Object, e As System.EventArgs) Handles TimerReportEmail.Tick
        'ToastNotificationsManager1.ShowNotification(ToastNotificationsManager1.Notifications(0))
        TimerReportEmail.Enabled = False
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim ds1 As DataSet = New DataSet
        Dim dsEmail As DataSet = New DataSet

        Dim sSql As String = "select * from ReportEmailSetting"
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            TimerReportEmail.Enabled = True
            Exit Sub
        End Try

        If ds.Tables(0).Rows.Count > 0 Then
            If Now.ToString("HH:mm") = ds.Tables(0).Rows(0).Item("SentTime").ToString.Trim Then
                Dim Server As String
                Dim senderDB As String
                Dim type As String
                Dim username As String
                Dim password As String
                Dim IsEnable As Boolean
                Dim Port As String
                Dim SenderName As String
                Dim CCMail As String = ""
                sSql = "select * from tblMailSetup"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(dsEmail)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(dsEmail)
                End If
                If dsEmail.Tables(0).Rows.Count > 0 Then
                    Server = dsEmail.Tables(0).Rows(0).Item("Server").ToString.Trim
                    senderDB = dsEmail.Tables(0).Rows(0).Item("sender").ToString.Trim
                    type = dsEmail.Tables(0).Rows(0).Item("type").ToString.Trim
                    username = dsEmail.Tables(0).Rows(0).Item("username").ToString.Trim
                    password = dsEmail.Tables(0).Rows(0).Item("password").ToString.Trim
                    If dsEmail.Tables(0).Rows(0).Item("IsEnable").ToString.Trim = "Y" Then
                        IsEnable = True
                    Else
                        IsEnable = False
                    End If
                    Port = dsEmail.Tables(0).Rows(0).Item("Port").ToString.Trim
                    SenderName = dsEmail.Tables(0).Rows(0).Item("SenderName").ToString.Trim
                    CCMail = dsEmail.Tables(0).Rows(0).Item("CCMail").ToString.Trim
                Else
                    TimerReportEmail.Enabled = True
                    Exit Sub
                End If

                Dim daily As XtraReportsDaily = New XtraReportsDaily
                Common.ReportMail = True

                Dim locationArr() As String = ds.Tables(0).Rows(0).Item("BranchCode").ToString.Trim.Split(",")
                Dim reportType() As String = ds.Tables(0).Rows(0).Item("ReportType").ToString.Trim.Split(",")

                For k As Integer = 0 To locationArr.Length - 1
                    Common.whereClauseEmail = " tblEmployee.BRANCHCODE = '" & locationArr(k).Trim & "'"
                    sSql = "select Email from tblbranch where BRANCHCODE = '" & locationArr(k).Trim & "'"
                    ds1 = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(ds1)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(ds1)
                    End If
                    For i As Integer = 0 To reportType.Length - 1
                        If reportType(i).Trim = "Daily Attendance" Then
                            Common.reportName = "DailyAttendance" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
                            daily.SpotXl_AttendanceGrid("")

                            Dim Smtp_Server As New SmtpClient
                            Dim e_mail As New MailMessage()
                            Dim attachment As System.Net.Mail.Attachment
                            Smtp_Server.UseDefaultCredentials = False

                            'Smtp_Server.Credentials = New Net.NetworkCredential(("nittslam@gmail.com"), ("umawidnitinbs4"))
                            Smtp_Server.Credentials = New Net.NetworkCredential(senderDB, password)
                            Smtp_Server.Port = Convert.ToInt16(Port) '587
                            Smtp_Server.EnableSsl = IsEnable 'True
                            Smtp_Server.Host = Server '"smtp.gmail.com"
                            e_mail = New MailMessage()
                            'e_mail.From = New MailAddress("nittslam@gmail.com")
                            e_mail.From = New MailAddress(senderDB)
                            'e_mail.To.Add("nittslam@gmail.com")
                            Try
                                e_mail.To.Add(ds1.Tables(0).Rows(0).Item("Email").ToString.Trim)
                                If CCMail.Trim <> "" Then
                                    e_mail.CC.Add(CCMail)
                                End If
                                e_mail.IsBodyHtml = True
                                e_mail.IsBodyHtml = True

                                e_mail.Subject = "Daily Attendance Report"
                                e_mail.Body = "Please find the attached Daily Attendance Report for date " & Now.ToString("dd/MM/yyyy")
                                attachment = New System.Net.Mail.Attachment(My.Application.Info.DirectoryPath & "\Reports\" & Common.reportName)
                                e_mail.Attachments.Add(attachment)
                                Smtp_Server.Send(e_mail)
                            Catch ex As Exception
                            End Try

                        End If
                        If reportType(i).Trim = "Daily Late Arrival" Then
                            Common.reportName = "DailyLateArrival" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
                            daily.SpotXl_LateArrivalGrid("")

                            Dim Smtp_Server As New SmtpClient
                            Dim e_mail As New MailMessage()
                            Dim attachment As System.Net.Mail.Attachment
                            Smtp_Server.UseDefaultCredentials = False

                            'Smtp_Server.Credentials = New Net.NetworkCredential(("nittslam@gmail.com"), ("umawidnitinbs4"))
                            Smtp_Server.Credentials = New Net.NetworkCredential(senderDB, password)
                            Smtp_Server.Port = Convert.ToInt16(Port) '587
                            Smtp_Server.EnableSsl = IsEnable 'True
                            Smtp_Server.Host = Server '"smtp.gmail.com"
                            e_mail = New MailMessage()
                            'e_mail.From = New MailAddress("nittslam@gmail.com")
                            e_mail.From = New MailAddress(senderDB)
                            'e_mail.To.Add("nittslam@gmail.com")
                            Try
                                e_mail.To.Add(ds1.Tables(0).Rows(0).Item("Email").ToString.Trim)
                                If CCMail.Trim <> "" Then
                                    e_mail.CC.Add(CCMail)
                                End If
                                e_mail.IsBodyHtml = True
                                e_mail.IsBodyHtml = True

                                e_mail.Subject = "Daily Late Arrival"
                                e_mail.Body = "Please find the attached Daily Late Arrival Report for date " & Now.ToString("dd/MM/yyyy")
                                attachment = New System.Net.Mail.Attachment(My.Application.Info.DirectoryPath & "\Reports\" & Common.reportName)
                                e_mail.Attachments.Add(attachment)
                                Smtp_Server.Send(e_mail)
                            Catch ex As Exception
                            End Try

                        End If
                        If reportType(i).Trim = "Daily Absenteeism" Then
                            Common.reportName = "DailyAbsenteeism" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
                            daily.SpotXl_AbsenteeismGrid("")

                            Dim Smtp_Server As New SmtpClient
                            Dim e_mail As New MailMessage()
                            Dim attachment As System.Net.Mail.Attachment
                            Smtp_Server.UseDefaultCredentials = False
                            'Smtp_Server.Credentials = New Net.NetworkCredential(("nittslam@gmail.com"), ("umawidnitinbs4"))
                            Smtp_Server.Credentials = New Net.NetworkCredential(senderDB, password)
                            Smtp_Server.Port = Convert.ToInt16(Port) '587
                            Smtp_Server.EnableSsl = IsEnable 'True
                            Smtp_Server.Host = Server '"smtp.gmail.com"
                            e_mail = New MailMessage()
                            'e_mail.From = New MailAddress("nittslam@gmail.com")
                            e_mail.From = New MailAddress(senderDB)
                            'e_mail.To.Add("nittslam@gmail.com")
                            Try
                                e_mail.To.Add(ds1.Tables(0).Rows(0).Item("Email").ToString.Trim)
                                If CCMail.Trim <> "" Then
                                    e_mail.CC.Add(CCMail)
                                End If
                                e_mail.IsBodyHtml = True
                                e_mail.IsBodyHtml = True

                                e_mail.Subject = "Daily Absenteeism"
                                e_mail.Body = "Please find the attached Daily Absenteeism Report for date " & Now.ToString("dd/MM/yyyy")
                                attachment = New System.Net.Mail.Attachment(My.Application.Info.DirectoryPath & "\Reports\" & Common.reportName)
                                e_mail.Attachments.Add(attachment)
                                Smtp_Server.Send(e_mail)
                            Catch ex As Exception
                            End Try

                        End If
                        If reportType(i).Trim = "Daily Present" Then
                            Common.reportName = "DailyPresent" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
                            daily.SpotXl_PresentGrid("")

                            Dim Smtp_Server As New SmtpClient
                            Dim e_mail As New MailMessage()
                            Dim attachment As System.Net.Mail.Attachment
                            Smtp_Server.UseDefaultCredentials = False
                            'Smtp_Server.Credentials = New Net.NetworkCredential(("nittslam@gmail.com"), ("umawidnitinbs4"))
                            Smtp_Server.Credentials = New Net.NetworkCredential(senderDB, password)
                            Smtp_Server.Port = Convert.ToInt16(Port) '587
                            Smtp_Server.EnableSsl = IsEnable 'True
                            Smtp_Server.Host = Server '"smtp.gmail.com"
                            e_mail = New MailMessage()
                            'e_mail.From = New MailAddress("nittslam@gmail.com")
                            e_mail.From = New MailAddress(senderDB)
                            'e_mail.To.Add("nittslam@gmail.com")

                            Try
                                e_mail.To.Add(ds1.Tables(0).Rows(0).Item("Email").ToString.Trim)
                                If CCMail.Trim <> "" Then
                                    e_mail.CC.Add(CCMail)
                                End If
                                e_mail.IsBodyHtml = True
                                e_mail.IsBodyHtml = True

                                e_mail.Subject = "Daily Present"
                                e_mail.Body = "Please find the attached Daily Present Report for date " & Now.ToString("dd/MM/yyyy")
                                attachment = New System.Net.Mail.Attachment(My.Application.Info.DirectoryPath & "\Reports\" & Common.reportName)
                                e_mail.Attachments.Add(attachment)
                                Smtp_Server.Send(e_mail)
                            Catch ex As Exception
                            End Try

                        End If
                        If reportType(i).Trim = "Daily Performance" Then
                            Common.reportName = "DailyPerformance" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
                            daily.DailyXl_PerformanceGrid("")

                            Dim Smtp_Server As New SmtpClient
                            Dim e_mail As New MailMessage()
                            Dim attachment As System.Net.Mail.Attachment
                            Smtp_Server.UseDefaultCredentials = False
                            'Smtp_Server.Credentials = New Net.NetworkCredential(("nittslam@gmail.com"), ("umawidnitinbs4"))
                            Smtp_Server.Credentials = New Net.NetworkCredential(senderDB, password)
                            Smtp_Server.Port = Convert.ToInt16(Port) '587
                            Smtp_Server.EnableSsl = IsEnable 'True
                            Smtp_Server.Host = Server '"smtp.gmail.com"
                            e_mail = New MailMessage()
                            'e_mail.From = New MailAddress("nittslam@gmail.com")
                            e_mail.From = New MailAddress(senderDB)
                            'e_mail.To.Add("nittslam@gmail.com")

                            Try
                                e_mail.To.Add(ds1.Tables(0).Rows(0).Item("Email").ToString.Trim)
                                If CCMail.Trim <> "" Then
                                    e_mail.CC.Add(CCMail)
                                End If
                                e_mail.IsBodyHtml = True
                                e_mail.IsBodyHtml = True

                                e_mail.Subject = "Daily Performance"
                                e_mail.Body = "Please find the attached Daily Performance Report for date " & Now.AddDays(-1).ToString("dd/MM/yyyy")
                                attachment = New System.Net.Mail.Attachment(My.Application.Info.DirectoryPath & "\Reports\" & Common.reportName)
                                e_mail.Attachments.Add(attachment)
                                Smtp_Server.Send(e_mail)
                            Catch ex As Exception
                            End Try
                        End If
                    Next
                    Common.whereClauseEmail = ""
                Next

                Dim msg As New Message("Mail Sent", "Daily report has been sent to Location mail ID")
                AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
            End If
        End If
        Common.ReportMail = False
        TimerReportEmail.Enabled = True
    End Sub
    Private Sub TimerDataProcess_Tick(sender As System.Object, e As System.EventArgs) Handles TimerDataProcess.Tick
        TimerDataProcess.Enabled = False
        Dim conSQL As SqlConnection
        Dim con1Access As OleDbConnection
        If Common.servername = "Access" Then
            con1Access = New OleDbConnection(Common.ConnectionString)
        Else
            conSQL = New SqlConnection(Common.ConnectionString)
        End If

        'Common.getSerialNo()
        'Common.loadEmp()
        'Common.loadCompany()
        'Common.loadLocation()
        'Common.loadDevice()

        If Common.LogDownLoadCounter = 1 Then
            XtraAutoDownloadLogs.ShowDialog()
        End If
        Dim comclass As Common = New Common
        Dim msg As New Message("Data Process", "Data Process is running in background")


        If Common.AutoProcess = "Y" Then
            AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim sSql As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId"
            '"select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Access)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, conSQL)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    LabelControlCount.Text = "Processing " & i + 1 & " of " & ds.Tables(0).Rows.Count
                    Application.DoEvents()
                    Dim pay As String = ds.Tables(0).Rows(i).Item("PAYCODE").ToString
                    If ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                        comclass.Process_AllRTC(Now.AddDays(-2), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString, ds.Tables(0).Rows(i).Item("PAYCODE").ToString, ds.Tables(0).Rows(i).Item("Id"))
                    Else
                        comclass.Process_AllnonRTC(Now.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString, ds.Tables(0).Rows(i).Item("PAYCODE").ToString, ds.Tables(0).Rows(i).Item("Id"))
                        If Common.EmpGrpArr(ds.Tables(0).Rows(i).Item("Id")).SHIFTTYPE = "M" Then
                            comclass.Process_AllnonRTCMulti(Now.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString, ds.Tables(0).Rows(i).Item("PAYCODE").ToString, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                    End If
                Next
                LabelControlCount.Text = ""
                Application.DoEvents()
            End If
            If AlertControl1.AlertFormList.Count > 0 Then
                AlertControl1.AlertFormList(0).Close()
            End If
            msg = New Message("Data Process", "Data Process finished")
            AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
        End If
        LabelControlCount.Text = ""
        Application.DoEvents()
        If realTIme = "Y" Then
            XtraRealTimePunches.Show()
        End If
        LabelControlStatus.Text = ""
        Application.DoEvents()

        If Common.online = "Y" Then
            comclass.Convertdat_Cloud()
            LabelControlStatus.Text = ""
            Application.DoEvents()
        End If

        'AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
        TimerSMS.Enabled = True

        If Common.TimerDur <> "" Then
            TimerCloud.Interval = Convert.ToInt32(Common.TimerDur) * 60 * 1000
            TimerCloud.Enabled = True
        End If
        If Common.AutoDwnDur <> "0" Then
            TimerAutoDownload.Interval = Common.AutoDwnDur * 60000
            TimerAutoDownload.Enabled = True
        End If
    End Sub
    Private Sub TimerSMS_Tick(sender As System.Object, e As System.EventArgs) Handles TimerSMS.Tick
        TimerSMS.Enabled = False
        Dim cn As Common = New Common()
        If Common.g_SMSApplicable = "" Then
            Common.Load_SMS_Policy()
        End If
        If Common.g_SMSApplicable = "Y" Then
            If Common.g_isAbsentSMS = "Y" Then
                If Common.g_AbsMsgFor = "C" Then
                    cn.SendAbsentMS_CurrentDt()
                Else
                    cn.SendAbsentSMS()
                End If
            End If
        End If
        TimerSMS.Enabled = True
    End Sub
    Private Sub TimerCloud_Tick(sender As System.Object, e As System.EventArgs) Handles TimerCloud.Tick
        TimerCloud.Enabled = False
        If Common.online = "Y" Then
            TimerCloud.Interval = Convert.ToInt32(Common.TimerDur) * 60 * 1000
            Dim comclass As Common = New Common
            comclass.Convertdat_Cloud()
            LabelControlStatus.Text = ""
            Application.DoEvents()
        End If
        TimerCloud.Enabled = True
    End Sub
    Private Sub TimerAutoDownload_Tick(sender As System.Object, e As System.EventArgs) Handles TimerAutoDownload.Tick
        Common.LogDownLoadCounter = 1
        XtraAutoDownloadLogs.Show()
    End Sub
    Private Sub TimerAutpBK_Tick(sender As System.Object, e As System.EventArgs) Handles TimerAutpBK.Tick
        TimerAutpBK.Enabled = False
        If Common.IsAutoBackUp = True Then
            If Now.ToString("yyyy-MM-dd HH:mm") = Common.NextAutoBackUpDateTime Then
                Dim startdate As DateTime = Now.AddDays(-Common.AutoBackUpForDays)
                Dim enddate As DateTime = Now
                Dim msg As New Message("Data BackUp", "Data BackUp is running in background")
                AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
                Common.backUpTxtDat(startdate, enddate, Common.BackUpType.ToString.Trim, Common.BackUpPath, Common.DeleteAfterAutoBackUp)

                Dim sSql As String = "update BackUpData set LastBackUpDate='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                Dim cmd As SqlCommand
                Dim cmd1 As OleDbCommand
                Try
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                Catch ex As Exception
                    Me.Cursor = Cursors.Default                   
                End Try

                If Common.AccessAutoBackUpPath <> "" Then
                    Dim sourceFile As String = My.Application.Info.DirectoryPath & "\TimeWatch.mdb"
                    Dim destibation As String = Common.AccessAutoBackUpPath & "\TimeWatch.mdb"
                    My.Computer.FileSystem.CopyFile(sourceFile, destibation, Microsoft.VisualBasic.FileIO.UIOption.AllDialogs, Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)
                End If
                msg = New Message("Data BackUp", "Data BackUp is Successful")
                AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
            End If
        End If
        TimerAutpBK.Enabled = True
    End Sub

    'Private Sub OfficeNavigationBar1_QueryPeekFormContent(sender As System.Object, e As DevExpress.XtraBars.Navigation.QueryPeekFormContentEventArgs) Handles OfficeNavigationBar1.QueryPeekFormContent
    '    If e.Item Is NavigationBarItem2 Then
    '        e.Control = New DeviceHoverMenu()
    '    End If
    'End Sub
End Class
Public Class Message
    Public Sub New(caption As String, Message As String)
        Me.Caption = caption '"LILA-Supermercado"
        Me.Text = Message '"Carrera 52 con Ave. Bolívar #65-98 Llano Largo"
        Me.Image = Nothing ' My.Resources.opportunities_32x32
    End Sub
    Public Property Caption() As String
    Public Property Text() As String
    Public Property Image() As Image
End Class
Public Class CustomFormPainter
    Inherits FormPainter

    Public Sub New(ByVal owner As Control, ByVal provider As DevExpress.Skins.ISkinProvider)
        MyBase.New(owner, provider)
    End Sub

    Private _CaptionAlignment As HorzAlignment = HorzAlignment.Default
    Public Property CaptionAlignment() As HorzAlignment
        Get
            Return _CaptionAlignment
        End Get
        Set(ByVal value As HorzAlignment)
            _CaptionAlignment = value
        End Set
    End Property
    Public Sub New(ByVal owner As XtraMaster, ByVal provider As DevExpress.LookAndFeel.UserLookAndFeel, ByVal captionAlignment As HorzAlignment)
        MyBase.New(owner, provider)
        Me.CaptionAlignment = captionAlignment
    End Sub
    Protected Overrides Sub DrawText(ByVal cache As DevExpress.Utils.Drawing.GraphicsCache)
        Dim text As String = text
        If text Is Nothing OrElse text.Length = 0 OrElse TextBounds.IsEmpty Then
            Return
        End If
        Dim appearance As New AppearanceObject(GetDefaultAppearance())
        appearance.TextOptions.Trimming = Trimming.EllipsisCharacter
        appearance.TextOptions.HAlignment = CaptionAlignment
        If AllowHtmlDraw Then
            DrawHtmlText(cache, appearance)
            Return
        End If
        Dim r As Rectangle = RectangleHelper.GetCenterBounds(TextBounds, New Size(TextBounds.Width, CalcTextHeight(cache.Graphics, appearance)))
        DrawTextShadow(cache, appearance, r)
        cache.DrawString(text, appearance.Font, appearance.GetForeBrush(cache), r, appearance.GetStringFormat())
    End Sub
End Class