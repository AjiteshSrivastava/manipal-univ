﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraFormulaSetupEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEditFrmCode = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButtonBasic = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButtonDA = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonHRA = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonCONV = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonMED = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonPRE = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonABS = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonHLD = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonLate = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonEARLY = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonESI = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditFORM = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButtonOT = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonMON_DAY = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonOT_RATE = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonT_EARLY = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonT_LATE = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonTDAYS = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonLEAVE = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonOTHER_LV = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonPL_EL = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonSL = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonCL = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButtonTDS = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonDEDUCT_10 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonDEDUCT_9 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonDEDUCT_8 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonDEDUCT_7 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonDEDUCT_6 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonDEDUCT_5 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonDEDUCT_4 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonDEDUCT_3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonDEDUCT_2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonDEDUCT_1 = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl5 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonEARN_10 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonEARN_9 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonEARN_8 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonEARN_7 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonEARN_6 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonEARN_5 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonEARN_4 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonEARN_3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonEARN_2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonEARN_1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonIF = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton5 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton6 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton7 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton8 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton9 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton10 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonReset = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonClose = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.ComboBoxEditFrmCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.TextEditFORM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl5.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.PanelControl5)
        Me.GroupControl1.Controls.Add(Me.PanelControl4)
        Me.GroupControl1.Controls.Add(Me.PanelControl3)
        Me.GroupControl1.Controls.Add(Me.PanelControl2)
        Me.GroupControl1.Location = New System.Drawing.Point(13, 13)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(598, 386)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Select Formula Values"
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControl2.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl2.Controls.Add(Me.SimpleButton10)
        Me.GroupControl2.Controls.Add(Me.SimpleButton9)
        Me.GroupControl2.Controls.Add(Me.SimpleButton8)
        Me.GroupControl2.Controls.Add(Me.SimpleButton7)
        Me.GroupControl2.Controls.Add(Me.SimpleButton6)
        Me.GroupControl2.Controls.Add(Me.SimpleButton5)
        Me.GroupControl2.Controls.Add(Me.SimpleButton4)
        Me.GroupControl2.Controls.Add(Me.SimpleButton3)
        Me.GroupControl2.Controls.Add(Me.SimpleButtonIF)
        Me.GroupControl2.Location = New System.Drawing.Point(617, 13)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(87, 386)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Oprations"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.TextEditFORM)
        Me.PanelControl1.Controls.Add(Me.ComboBoxEditFrmCode)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Location = New System.Drawing.Point(13, 405)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(691, 64)
        Me.PanelControl1.TabIndex = 2
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(5, 22)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Formula Code"
        '
        'ComboBoxEditFrmCode
        '
        Me.ComboBoxEditFrmCode.EditValue = "A"
        Me.ComboBoxEditFrmCode.Location = New System.Drawing.Point(85, 19)
        Me.ComboBoxEditFrmCode.Name = "ComboBoxEditFrmCode"
        Me.ComboBoxEditFrmCode.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditFrmCode.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditFrmCode.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditFrmCode.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditFrmCode.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditFrmCode.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEditFrmCode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditFrmCode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEditFrmCode.Size = New System.Drawing.Size(49, 20)
        Me.ComboBoxEditFrmCode.TabIndex = 37
        '
        'SimpleButtonBasic
        '
        Me.SimpleButtonBasic.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonBasic.Appearance.Options.UseFont = True
        Me.SimpleButtonBasic.Location = New System.Drawing.Point(5, 5)
        Me.SimpleButtonBasic.Name = "SimpleButtonBasic"
        Me.SimpleButtonBasic.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonBasic.TabIndex = 0
        Me.SimpleButtonBasic.Text = "BASIC "
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.SimpleButtonESI)
        Me.PanelControl2.Controls.Add(Me.SimpleButtonEARLY)
        Me.PanelControl2.Controls.Add(Me.SimpleButtonLate)
        Me.PanelControl2.Controls.Add(Me.SimpleButtonHLD)
        Me.PanelControl2.Controls.Add(Me.SimpleButtonABS)
        Me.PanelControl2.Controls.Add(Me.SimpleButtonPRE)
        Me.PanelControl2.Controls.Add(Me.SimpleButtonMED)
        Me.PanelControl2.Controls.Add(Me.SimpleButtonCONV)
        Me.PanelControl2.Controls.Add(Me.SimpleButtonHRA)
        Me.PanelControl2.Controls.Add(Me.SimpleButtonDA)
        Me.PanelControl2.Controls.Add(Me.SimpleButtonBasic)
        Me.PanelControl2.Location = New System.Drawing.Point(19, 35)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(135, 326)
        Me.PanelControl2.TabIndex = 1
        '
        'SimpleButtonDA
        '
        Me.SimpleButtonDA.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDA.Appearance.Options.UseFont = True
        Me.SimpleButtonDA.Location = New System.Drawing.Point(5, 34)
        Me.SimpleButtonDA.Name = "SimpleButtonDA"
        Me.SimpleButtonDA.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonDA.TabIndex = 1
        Me.SimpleButtonDA.Text = "DA "
        '
        'SimpleButtonHRA
        '
        Me.SimpleButtonHRA.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonHRA.Appearance.Options.UseFont = True
        Me.SimpleButtonHRA.Location = New System.Drawing.Point(5, 63)
        Me.SimpleButtonHRA.Name = "SimpleButtonHRA"
        Me.SimpleButtonHRA.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonHRA.TabIndex = 2
        Me.SimpleButtonHRA.Text = "HRA "
        '
        'SimpleButtonCONV
        '
        Me.SimpleButtonCONV.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonCONV.Appearance.Options.UseFont = True
        Me.SimpleButtonCONV.Location = New System.Drawing.Point(5, 92)
        Me.SimpleButtonCONV.Name = "SimpleButtonCONV"
        Me.SimpleButtonCONV.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonCONV.TabIndex = 3
        Me.SimpleButtonCONV.Text = "CONV "
        '
        'SimpleButtonMED
        '
        Me.SimpleButtonMED.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonMED.Appearance.Options.UseFont = True
        Me.SimpleButtonMED.Location = New System.Drawing.Point(5, 121)
        Me.SimpleButtonMED.Name = "SimpleButtonMED"
        Me.SimpleButtonMED.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonMED.TabIndex = 4
        Me.SimpleButtonMED.Text = "MED "
        '
        'SimpleButtonPRE
        '
        Me.SimpleButtonPRE.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonPRE.Appearance.Options.UseFont = True
        Me.SimpleButtonPRE.Location = New System.Drawing.Point(5, 150)
        Me.SimpleButtonPRE.Name = "SimpleButtonPRE"
        Me.SimpleButtonPRE.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonPRE.TabIndex = 5
        Me.SimpleButtonPRE.Text = "PRE "
        '
        'SimpleButtonABS
        '
        Me.SimpleButtonABS.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonABS.Appearance.Options.UseFont = True
        Me.SimpleButtonABS.Location = New System.Drawing.Point(5, 179)
        Me.SimpleButtonABS.Name = "SimpleButtonABS"
        Me.SimpleButtonABS.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonABS.TabIndex = 6
        Me.SimpleButtonABS.Text = "ABS "
        '
        'SimpleButtonHLD
        '
        Me.SimpleButtonHLD.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonHLD.Appearance.Options.UseFont = True
        Me.SimpleButtonHLD.Location = New System.Drawing.Point(5, 208)
        Me.SimpleButtonHLD.Name = "SimpleButtonHLD"
        Me.SimpleButtonHLD.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonHLD.TabIndex = 7
        Me.SimpleButtonHLD.Text = "HLD "
        '
        'SimpleButtonLate
        '
        Me.SimpleButtonLate.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonLate.Appearance.Options.UseFont = True
        Me.SimpleButtonLate.Location = New System.Drawing.Point(5, 237)
        Me.SimpleButtonLate.Name = "SimpleButtonLate"
        Me.SimpleButtonLate.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonLate.TabIndex = 8
        Me.SimpleButtonLate.Text = "LATE "
        '
        'SimpleButtonEARLY
        '
        Me.SimpleButtonEARLY.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonEARLY.Appearance.Options.UseFont = True
        Me.SimpleButtonEARLY.Location = New System.Drawing.Point(5, 266)
        Me.SimpleButtonEARLY.Name = "SimpleButtonEARLY"
        Me.SimpleButtonEARLY.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonEARLY.TabIndex = 9
        Me.SimpleButtonEARLY.Text = "EARLY "
        '
        'SimpleButtonESI
        '
        Me.SimpleButtonESI.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonESI.Appearance.Options.UseFont = True
        Me.SimpleButtonESI.Location = New System.Drawing.Point(5, 295)
        Me.SimpleButtonESI.Name = "SimpleButtonESI"
        Me.SimpleButtonESI.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonESI.TabIndex = 10
        Me.SimpleButtonESI.Text = "ESI "
        Me.SimpleButtonESI.Visible = False
        '
        'TextEditFORM
        '
        Me.TextEditFORM.Location = New System.Drawing.Point(140, 20)
        Me.TextEditFORM.Name = "TextEditFORM"
        Me.TextEditFORM.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditFORM.Properties.Appearance.Options.UseFont = True
        Me.TextEditFORM.Size = New System.Drawing.Size(535, 20)
        Me.TextEditFORM.TabIndex = 38
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.SimpleButtonOT)
        Me.PanelControl3.Controls.Add(Me.SimpleButtonMON_DAY)
        Me.PanelControl3.Controls.Add(Me.SimpleButtonOT_RATE)
        Me.PanelControl3.Controls.Add(Me.SimpleButtonT_EARLY)
        Me.PanelControl3.Controls.Add(Me.SimpleButtonT_LATE)
        Me.PanelControl3.Controls.Add(Me.SimpleButtonTDAYS)
        Me.PanelControl3.Controls.Add(Me.SimpleButtonLEAVE)
        Me.PanelControl3.Controls.Add(Me.SimpleButtonOTHER_LV)
        Me.PanelControl3.Controls.Add(Me.SimpleButtonPL_EL)
        Me.PanelControl3.Controls.Add(Me.SimpleButtonSL)
        Me.PanelControl3.Controls.Add(Me.SimpleButtonCL)
        Me.PanelControl3.Location = New System.Drawing.Point(160, 35)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(135, 326)
        Me.PanelControl3.TabIndex = 2
        '
        'SimpleButtonOT
        '
        Me.SimpleButtonOT.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonOT.Appearance.Options.UseFont = True
        Me.SimpleButtonOT.Location = New System.Drawing.Point(5, 295)
        Me.SimpleButtonOT.Name = "SimpleButtonOT"
        Me.SimpleButtonOT.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonOT.TabIndex = 10
        Me.SimpleButtonOT.Text = "O.T. "
        Me.SimpleButtonOT.Visible = False
        '
        'SimpleButtonMON_DAY
        '
        Me.SimpleButtonMON_DAY.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonMON_DAY.Appearance.Options.UseFont = True
        Me.SimpleButtonMON_DAY.Location = New System.Drawing.Point(5, 266)
        Me.SimpleButtonMON_DAY.Name = "SimpleButtonMON_DAY"
        Me.SimpleButtonMON_DAY.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonMON_DAY.TabIndex = 9
        Me.SimpleButtonMON_DAY.Text = "MON_DAY "
        '
        'SimpleButtonOT_RATE
        '
        Me.SimpleButtonOT_RATE.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonOT_RATE.Appearance.Options.UseFont = True
        Me.SimpleButtonOT_RATE.Location = New System.Drawing.Point(5, 237)
        Me.SimpleButtonOT_RATE.Name = "SimpleButtonOT_RATE"
        Me.SimpleButtonOT_RATE.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonOT_RATE.TabIndex = 8
        Me.SimpleButtonOT_RATE.Text = "OT_RATE "
        '
        'SimpleButtonT_EARLY
        '
        Me.SimpleButtonT_EARLY.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonT_EARLY.Appearance.Options.UseFont = True
        Me.SimpleButtonT_EARLY.Location = New System.Drawing.Point(5, 208)
        Me.SimpleButtonT_EARLY.Name = "SimpleButtonT_EARLY"
        Me.SimpleButtonT_EARLY.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonT_EARLY.TabIndex = 7
        Me.SimpleButtonT_EARLY.Text = "T_EARLY "
        '
        'SimpleButtonT_LATE
        '
        Me.SimpleButtonT_LATE.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonT_LATE.Appearance.Options.UseFont = True
        Me.SimpleButtonT_LATE.Location = New System.Drawing.Point(5, 179)
        Me.SimpleButtonT_LATE.Name = "SimpleButtonT_LATE"
        Me.SimpleButtonT_LATE.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonT_LATE.TabIndex = 6
        Me.SimpleButtonT_LATE.Text = "T_LATE "
        '
        'SimpleButtonTDAYS
        '
        Me.SimpleButtonTDAYS.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonTDAYS.Appearance.Options.UseFont = True
        Me.SimpleButtonTDAYS.Location = New System.Drawing.Point(5, 150)
        Me.SimpleButtonTDAYS.Name = "SimpleButtonTDAYS"
        Me.SimpleButtonTDAYS.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonTDAYS.TabIndex = 5
        Me.SimpleButtonTDAYS.Text = "TDAYS "
        '
        'SimpleButtonLEAVE
        '
        Me.SimpleButtonLEAVE.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonLEAVE.Appearance.Options.UseFont = True
        Me.SimpleButtonLEAVE.Location = New System.Drawing.Point(5, 121)
        Me.SimpleButtonLEAVE.Name = "SimpleButtonLEAVE"
        Me.SimpleButtonLEAVE.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonLEAVE.TabIndex = 4
        Me.SimpleButtonLEAVE.Text = "LEAVE "
        '
        'SimpleButtonOTHER_LV
        '
        Me.SimpleButtonOTHER_LV.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonOTHER_LV.Appearance.Options.UseFont = True
        Me.SimpleButtonOTHER_LV.Location = New System.Drawing.Point(5, 92)
        Me.SimpleButtonOTHER_LV.Name = "SimpleButtonOTHER_LV"
        Me.SimpleButtonOTHER_LV.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonOTHER_LV.TabIndex = 3
        Me.SimpleButtonOTHER_LV.Text = "OTHER_LV"
        '
        'SimpleButtonPL_EL
        '
        Me.SimpleButtonPL_EL.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonPL_EL.Appearance.Options.UseFont = True
        Me.SimpleButtonPL_EL.Location = New System.Drawing.Point(5, 63)
        Me.SimpleButtonPL_EL.Name = "SimpleButtonPL_EL"
        Me.SimpleButtonPL_EL.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonPL_EL.TabIndex = 2
        Me.SimpleButtonPL_EL.Text = "PL_EL "
        '
        'SimpleButtonSL
        '
        Me.SimpleButtonSL.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSL.Appearance.Options.UseFont = True
        Me.SimpleButtonSL.Location = New System.Drawing.Point(5, 34)
        Me.SimpleButtonSL.Name = "SimpleButtonSL"
        Me.SimpleButtonSL.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonSL.TabIndex = 1
        Me.SimpleButtonSL.Text = "S.L. "
        '
        'SimpleButtonCL
        '
        Me.SimpleButtonCL.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonCL.Appearance.Options.UseFont = True
        Me.SimpleButtonCL.Location = New System.Drawing.Point(5, 5)
        Me.SimpleButtonCL.Name = "SimpleButtonCL"
        Me.SimpleButtonCL.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonCL.TabIndex = 0
        Me.SimpleButtonCL.Text = "C.L. "
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.SimpleButtonTDS)
        Me.PanelControl4.Controls.Add(Me.SimpleButtonDEDUCT_10)
        Me.PanelControl4.Controls.Add(Me.SimpleButtonDEDUCT_9)
        Me.PanelControl4.Controls.Add(Me.SimpleButtonDEDUCT_8)
        Me.PanelControl4.Controls.Add(Me.SimpleButtonDEDUCT_7)
        Me.PanelControl4.Controls.Add(Me.SimpleButtonDEDUCT_6)
        Me.PanelControl4.Controls.Add(Me.SimpleButtonDEDUCT_5)
        Me.PanelControl4.Controls.Add(Me.SimpleButtonDEDUCT_4)
        Me.PanelControl4.Controls.Add(Me.SimpleButtonDEDUCT_3)
        Me.PanelControl4.Controls.Add(Me.SimpleButtonDEDUCT_2)
        Me.PanelControl4.Controls.Add(Me.SimpleButtonDEDUCT_1)
        Me.PanelControl4.Location = New System.Drawing.Point(301, 35)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(135, 326)
        Me.PanelControl4.TabIndex = 3
        '
        'SimpleButtonTDS
        '
        Me.SimpleButtonTDS.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonTDS.Appearance.Options.UseFont = True
        Me.SimpleButtonTDS.Location = New System.Drawing.Point(5, 295)
        Me.SimpleButtonTDS.Name = "SimpleButtonTDS"
        Me.SimpleButtonTDS.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonTDS.TabIndex = 10
        Me.SimpleButtonTDS.Text = "TDS "
        '
        'SimpleButtonDEDUCT_10
        '
        Me.SimpleButtonDEDUCT_10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDEDUCT_10.Appearance.Options.UseFont = True
        Me.SimpleButtonDEDUCT_10.Location = New System.Drawing.Point(5, 266)
        Me.SimpleButtonDEDUCT_10.Name = "SimpleButtonDEDUCT_10"
        Me.SimpleButtonDEDUCT_10.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonDEDUCT_10.TabIndex = 9
        Me.SimpleButtonDEDUCT_10.Text = "DEDUCT_10 "
        '
        'SimpleButtonDEDUCT_9
        '
        Me.SimpleButtonDEDUCT_9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDEDUCT_9.Appearance.Options.UseFont = True
        Me.SimpleButtonDEDUCT_9.Location = New System.Drawing.Point(5, 237)
        Me.SimpleButtonDEDUCT_9.Name = "SimpleButtonDEDUCT_9"
        Me.SimpleButtonDEDUCT_9.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonDEDUCT_9.TabIndex = 8
        Me.SimpleButtonDEDUCT_9.Text = "DEDUCT_9 "
        '
        'SimpleButtonDEDUCT_8
        '
        Me.SimpleButtonDEDUCT_8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDEDUCT_8.Appearance.Options.UseFont = True
        Me.SimpleButtonDEDUCT_8.Location = New System.Drawing.Point(5, 208)
        Me.SimpleButtonDEDUCT_8.Name = "SimpleButtonDEDUCT_8"
        Me.SimpleButtonDEDUCT_8.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonDEDUCT_8.TabIndex = 7
        Me.SimpleButtonDEDUCT_8.Text = "DEDUCT_8 "
        '
        'SimpleButtonDEDUCT_7
        '
        Me.SimpleButtonDEDUCT_7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDEDUCT_7.Appearance.Options.UseFont = True
        Me.SimpleButtonDEDUCT_7.Location = New System.Drawing.Point(5, 179)
        Me.SimpleButtonDEDUCT_7.Name = "SimpleButtonDEDUCT_7"
        Me.SimpleButtonDEDUCT_7.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonDEDUCT_7.TabIndex = 6
        Me.SimpleButtonDEDUCT_7.Text = "DEDUCT_7 "
        '
        'SimpleButtonDEDUCT_6
        '
        Me.SimpleButtonDEDUCT_6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDEDUCT_6.Appearance.Options.UseFont = True
        Me.SimpleButtonDEDUCT_6.Location = New System.Drawing.Point(5, 150)
        Me.SimpleButtonDEDUCT_6.Name = "SimpleButtonDEDUCT_6"
        Me.SimpleButtonDEDUCT_6.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonDEDUCT_6.TabIndex = 5
        Me.SimpleButtonDEDUCT_6.Text = "DEDUCT_6 "
        '
        'SimpleButtonDEDUCT_5
        '
        Me.SimpleButtonDEDUCT_5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDEDUCT_5.Appearance.Options.UseFont = True
        Me.SimpleButtonDEDUCT_5.Location = New System.Drawing.Point(5, 121)
        Me.SimpleButtonDEDUCT_5.Name = "SimpleButtonDEDUCT_5"
        Me.SimpleButtonDEDUCT_5.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonDEDUCT_5.TabIndex = 4
        Me.SimpleButtonDEDUCT_5.Text = "DEDUCT_5 "
        '
        'SimpleButtonDEDUCT_4
        '
        Me.SimpleButtonDEDUCT_4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDEDUCT_4.Appearance.Options.UseFont = True
        Me.SimpleButtonDEDUCT_4.Location = New System.Drawing.Point(5, 92)
        Me.SimpleButtonDEDUCT_4.Name = "SimpleButtonDEDUCT_4"
        Me.SimpleButtonDEDUCT_4.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonDEDUCT_4.TabIndex = 3
        Me.SimpleButtonDEDUCT_4.Text = "DEDUCT_4 "
        '
        'SimpleButtonDEDUCT_3
        '
        Me.SimpleButtonDEDUCT_3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDEDUCT_3.Appearance.Options.UseFont = True
        Me.SimpleButtonDEDUCT_3.Location = New System.Drawing.Point(5, 63)
        Me.SimpleButtonDEDUCT_3.Name = "SimpleButtonDEDUCT_3"
        Me.SimpleButtonDEDUCT_3.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonDEDUCT_3.TabIndex = 2
        Me.SimpleButtonDEDUCT_3.Text = "DEDUCT_3 "
        '
        'SimpleButtonDEDUCT_2
        '
        Me.SimpleButtonDEDUCT_2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDEDUCT_2.Appearance.Options.UseFont = True
        Me.SimpleButtonDEDUCT_2.Location = New System.Drawing.Point(5, 34)
        Me.SimpleButtonDEDUCT_2.Name = "SimpleButtonDEDUCT_2"
        Me.SimpleButtonDEDUCT_2.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonDEDUCT_2.TabIndex = 1
        Me.SimpleButtonDEDUCT_2.Text = "DEDUCT_2 "
        '
        'SimpleButtonDEDUCT_1
        '
        Me.SimpleButtonDEDUCT_1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDEDUCT_1.Appearance.Options.UseFont = True
        Me.SimpleButtonDEDUCT_1.Location = New System.Drawing.Point(5, 5)
        Me.SimpleButtonDEDUCT_1.Name = "SimpleButtonDEDUCT_1"
        Me.SimpleButtonDEDUCT_1.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonDEDUCT_1.TabIndex = 0
        Me.SimpleButtonDEDUCT_1.Text = "DEDUCT_1 "
        '
        'PanelControl5
        '
        Me.PanelControl5.Controls.Add(Me.SimpleButton1)
        Me.PanelControl5.Controls.Add(Me.SimpleButtonEARN_10)
        Me.PanelControl5.Controls.Add(Me.SimpleButtonEARN_9)
        Me.PanelControl5.Controls.Add(Me.SimpleButtonEARN_8)
        Me.PanelControl5.Controls.Add(Me.SimpleButtonEARN_7)
        Me.PanelControl5.Controls.Add(Me.SimpleButtonEARN_6)
        Me.PanelControl5.Controls.Add(Me.SimpleButtonEARN_5)
        Me.PanelControl5.Controls.Add(Me.SimpleButtonEARN_4)
        Me.PanelControl5.Controls.Add(Me.SimpleButtonEARN_3)
        Me.PanelControl5.Controls.Add(Me.SimpleButtonEARN_2)
        Me.PanelControl5.Controls.Add(Me.SimpleButtonEARN_1)
        Me.PanelControl5.Location = New System.Drawing.Point(442, 35)
        Me.PanelControl5.Name = "PanelControl5"
        Me.PanelControl5.Size = New System.Drawing.Size(135, 326)
        Me.PanelControl5.TabIndex = 4
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(5, 295)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButton1.TabIndex = 10
        Me.SimpleButton1.Text = "TDS "
        Me.SimpleButton1.Visible = False
        '
        'SimpleButtonEARN_10
        '
        Me.SimpleButtonEARN_10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonEARN_10.Appearance.Options.UseFont = True
        Me.SimpleButtonEARN_10.Location = New System.Drawing.Point(5, 266)
        Me.SimpleButtonEARN_10.Name = "SimpleButtonEARN_10"
        Me.SimpleButtonEARN_10.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonEARN_10.TabIndex = 9
        Me.SimpleButtonEARN_10.Text = "EARN_10 "
        '
        'SimpleButtonEARN_9
        '
        Me.SimpleButtonEARN_9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonEARN_9.Appearance.Options.UseFont = True
        Me.SimpleButtonEARN_9.Location = New System.Drawing.Point(5, 237)
        Me.SimpleButtonEARN_9.Name = "SimpleButtonEARN_9"
        Me.SimpleButtonEARN_9.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonEARN_9.TabIndex = 8
        Me.SimpleButtonEARN_9.Text = "EARN_9 "
        '
        'SimpleButtonEARN_8
        '
        Me.SimpleButtonEARN_8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonEARN_8.Appearance.Options.UseFont = True
        Me.SimpleButtonEARN_8.Location = New System.Drawing.Point(5, 208)
        Me.SimpleButtonEARN_8.Name = "SimpleButtonEARN_8"
        Me.SimpleButtonEARN_8.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonEARN_8.TabIndex = 7
        Me.SimpleButtonEARN_8.Text = "EARN_8 "
        '
        'SimpleButtonEARN_7
        '
        Me.SimpleButtonEARN_7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonEARN_7.Appearance.Options.UseFont = True
        Me.SimpleButtonEARN_7.Location = New System.Drawing.Point(5, 179)
        Me.SimpleButtonEARN_7.Name = "SimpleButtonEARN_7"
        Me.SimpleButtonEARN_7.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonEARN_7.TabIndex = 6
        Me.SimpleButtonEARN_7.Text = "EARN_7 "
        '
        'SimpleButtonEARN_6
        '
        Me.SimpleButtonEARN_6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonEARN_6.Appearance.Options.UseFont = True
        Me.SimpleButtonEARN_6.Location = New System.Drawing.Point(5, 150)
        Me.SimpleButtonEARN_6.Name = "SimpleButtonEARN_6"
        Me.SimpleButtonEARN_6.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonEARN_6.TabIndex = 5
        Me.SimpleButtonEARN_6.Text = "EARN_6 "
        '
        'SimpleButtonEARN_5
        '
        Me.SimpleButtonEARN_5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonEARN_5.Appearance.Options.UseFont = True
        Me.SimpleButtonEARN_5.Location = New System.Drawing.Point(5, 121)
        Me.SimpleButtonEARN_5.Name = "SimpleButtonEARN_5"
        Me.SimpleButtonEARN_5.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonEARN_5.TabIndex = 4
        Me.SimpleButtonEARN_5.Text = "EARN_5 "
        '
        'SimpleButtonEARN_4
        '
        Me.SimpleButtonEARN_4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonEARN_4.Appearance.Options.UseFont = True
        Me.SimpleButtonEARN_4.Location = New System.Drawing.Point(5, 92)
        Me.SimpleButtonEARN_4.Name = "SimpleButtonEARN_4"
        Me.SimpleButtonEARN_4.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonEARN_4.TabIndex = 3
        Me.SimpleButtonEARN_4.Text = "EARN_4 "
        '
        'SimpleButtonEARN_3
        '
        Me.SimpleButtonEARN_3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonEARN_3.Appearance.Options.UseFont = True
        Me.SimpleButtonEARN_3.Location = New System.Drawing.Point(5, 63)
        Me.SimpleButtonEARN_3.Name = "SimpleButtonEARN_3"
        Me.SimpleButtonEARN_3.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonEARN_3.TabIndex = 2
        Me.SimpleButtonEARN_3.Text = "EARN_3 "
        '
        'SimpleButtonEARN_2
        '
        Me.SimpleButtonEARN_2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonEARN_2.Appearance.Options.UseFont = True
        Me.SimpleButtonEARN_2.Location = New System.Drawing.Point(5, 34)
        Me.SimpleButtonEARN_2.Name = "SimpleButtonEARN_2"
        Me.SimpleButtonEARN_2.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonEARN_2.TabIndex = 1
        Me.SimpleButtonEARN_2.Text = "EARN_2 "
        '
        'SimpleButtonEARN_1
        '
        Me.SimpleButtonEARN_1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonEARN_1.Appearance.Options.UseFont = True
        Me.SimpleButtonEARN_1.Location = New System.Drawing.Point(5, 5)
        Me.SimpleButtonEARN_1.Name = "SimpleButtonEARN_1"
        Me.SimpleButtonEARN_1.Size = New System.Drawing.Size(125, 23)
        Me.SimpleButtonEARN_1.TabIndex = 0
        Me.SimpleButtonEARN_1.Text = "EARN_1 "
        '
        'SimpleButtonIF
        '
        Me.SimpleButtonIF.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonIF.Appearance.Options.UseFont = True
        Me.SimpleButtonIF.Location = New System.Drawing.Point(15, 40)
        Me.SimpleButtonIF.Name = "SimpleButtonIF"
        Me.SimpleButtonIF.Size = New System.Drawing.Size(56, 23)
        Me.SimpleButtonIF.TabIndex = 1
        Me.SimpleButtonIF.Text = "IF "
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton3.Appearance.Options.UseFont = True
        Me.SimpleButton3.Location = New System.Drawing.Point(15, 69)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(56, 23)
        Me.SimpleButton3.TabIndex = 2
        Me.SimpleButton3.Text = "[ "
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton4.Appearance.Options.UseFont = True
        Me.SimpleButton4.Location = New System.Drawing.Point(15, 98)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(56, 23)
        Me.SimpleButton4.TabIndex = 3
        Me.SimpleButton4.Text = "] "
        '
        'SimpleButton5
        '
        Me.SimpleButton5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton5.Appearance.Options.UseFont = True
        Me.SimpleButton5.Location = New System.Drawing.Point(15, 127)
        Me.SimpleButton5.Name = "SimpleButton5"
        Me.SimpleButton5.Size = New System.Drawing.Size(56, 23)
        Me.SimpleButton5.TabIndex = 4
        Me.SimpleButton5.Text = "( "
        '
        'SimpleButton6
        '
        Me.SimpleButton6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton6.Appearance.Options.UseFont = True
        Me.SimpleButton6.Location = New System.Drawing.Point(15, 156)
        Me.SimpleButton6.Name = "SimpleButton6"
        Me.SimpleButton6.Size = New System.Drawing.Size(56, 23)
        Me.SimpleButton6.TabIndex = 5
        Me.SimpleButton6.Text = ") "
        '
        'SimpleButton7
        '
        Me.SimpleButton7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton7.Appearance.Options.UseFont = True
        Me.SimpleButton7.Location = New System.Drawing.Point(15, 185)
        Me.SimpleButton7.Name = "SimpleButton7"
        Me.SimpleButton7.Size = New System.Drawing.Size(56, 23)
        Me.SimpleButton7.TabIndex = 6
        Me.SimpleButton7.Text = "+ "
        '
        'SimpleButton8
        '
        Me.SimpleButton8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton8.Appearance.Options.UseFont = True
        Me.SimpleButton8.Location = New System.Drawing.Point(15, 214)
        Me.SimpleButton8.Name = "SimpleButton8"
        Me.SimpleButton8.Size = New System.Drawing.Size(56, 23)
        Me.SimpleButton8.TabIndex = 7
        Me.SimpleButton8.Text = "- "
        '
        'SimpleButton9
        '
        Me.SimpleButton9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton9.Appearance.Options.UseFont = True
        Me.SimpleButton9.Location = New System.Drawing.Point(15, 243)
        Me.SimpleButton9.Name = "SimpleButton9"
        Me.SimpleButton9.Size = New System.Drawing.Size(56, 23)
        Me.SimpleButton9.TabIndex = 8
        Me.SimpleButton9.Text = "* "
        '
        'SimpleButton10
        '
        Me.SimpleButton10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton10.Appearance.Options.UseFont = True
        Me.SimpleButton10.Location = New System.Drawing.Point(15, 272)
        Me.SimpleButton10.Name = "SimpleButton10"
        Me.SimpleButton10.Size = New System.Drawing.Size(56, 23)
        Me.SimpleButton10.TabIndex = 9
        Me.SimpleButton10.Text = "/ "
        '
        'SimpleButtonReset
        '
        Me.SimpleButtonReset.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonReset.Appearance.Options.UseFont = True
        Me.SimpleButtonReset.Location = New System.Drawing.Point(446, 482)
        Me.SimpleButtonReset.Name = "SimpleButtonReset"
        Me.SimpleButtonReset.Size = New System.Drawing.Size(82, 23)
        Me.SimpleButtonReset.TabIndex = 3
        Me.SimpleButtonReset.Text = "Reset"
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(534, 482)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(82, 23)
        Me.SimpleButtonSave.TabIndex = 4
        Me.SimpleButtonSave.Text = "Save"
        '
        'SimpleButtonClose
        '
        Me.SimpleButtonClose.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonClose.Appearance.Options.UseFont = True
        Me.SimpleButtonClose.Location = New System.Drawing.Point(622, 482)
        Me.SimpleButtonClose.Name = "SimpleButtonClose"
        Me.SimpleButtonClose.Size = New System.Drawing.Size(82, 23)
        Me.SimpleButtonClose.TabIndex = 5
        Me.SimpleButtonClose.Text = "Close"
        '
        'XtraFormulaSetupEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(725, 517)
        Me.Controls.Add(Me.SimpleButtonClose)
        Me.Controls.Add(Me.SimpleButtonSave)
        Me.Controls.Add(Me.SimpleButtonReset)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraFormulaSetupEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.ComboBoxEditFrmCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.TextEditFORM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEditFrmCode As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButtonBasic As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SimpleButtonEARLY As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonLate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonHLD As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonABS As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonPRE As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonMED As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonCONV As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonHRA As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonDA As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonESI As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditFORM As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SimpleButtonOT As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonMON_DAY As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonOT_RATE As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonT_EARLY As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonT_LATE As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonTDAYS As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonLEAVE As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonOTHER_LV As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonPL_EL As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonSL As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonCL As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SimpleButtonTDS As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonDEDUCT_10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonDEDUCT_9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonDEDUCT_8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonDEDUCT_7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonDEDUCT_6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonDEDUCT_5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonDEDUCT_4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonDEDUCT_3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonDEDUCT_2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonDEDUCT_1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl5 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonEARN_10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonEARN_9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonEARN_8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonEARN_7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonEARN_6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonEARN_5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonEARN_4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonEARN_3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonEARN_2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonEARN_1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonIF As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonReset As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonClose As DevExpress.XtraEditors.SimpleButton
End Class
