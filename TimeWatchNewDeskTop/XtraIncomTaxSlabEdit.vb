﻿Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Net.NetworkInformation
Imports System.Text.RegularExpressions

Public Class XtraIncomTaxSlabEdit
    Dim ulf As UserLookAndFeel
    Dim ID_NO As Integer
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        'If TextEditLowerLimit.Text.Trim = "" Then
        '    XtraMessageBox.Show(ulf, "<size=10>Company Name cannot be empty</size>", "<size=9>Error</size>")
        '    TextEditLowerLimit.Select()
        '    Exit Sub
        'End If
        'If MemoEditAddress.Text.Trim = "" Then
        '    XtraMessageBox.Show(ulf, "<size=10>Company Address cannot be empty</size>", "<size=9>Error</size>")
        '    MemoEditAddress.Select()
        '    Exit Sub
        'End If
        'If TextEditContact.Text.Trim = "" Then
        '    XtraMessageBox.Show(ulf, "<size=10>Company Contact cannot be empty</size>", "<size=9>Error</size>")
        '    TextEditContact.Select()
        '    Exit Sub
        'End If
        'If TextEditPercent.Text.Trim = "" Then
        '    XtraMessageBox.Show(ulf, "<size=10>Company Email cannot be empty</size>", "<size=9>Error</size>")
        '    TextEditPercent.Select()
        '    Exit Sub
        'End If

        'If emailaddresscheck(TextEditPercent.Text.Trim) = False Then
        '    XtraMessageBox.Show(ulf, "<size=10>Invalid Email </size>", "<size=9>Error</size>")
        '    TextEditPercent.Select()
        '    Exit Sub
        'End If

        Me.Cursor = Cursors.WaitCursor
        Dim LOWERLIMIT As String = TextEditLowerLimit.Text.Trim
        Dim UPPERLIMIT As String = TextEditUpperLimit.Text.Trim
        Dim PERCENT16 As String = TextEditPercent.Text.Trim
        Dim FIXED As String
        Dim gender As String
        If ComboBoxEditGender.SelectedIndex = 0 Then
            gender = "M"
        Else
            gender = "F"
        End If

        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand

        Dim sSql1 As String
        If XtraIncomTaxSlab.ITax_ID_NO = "" Then
            sSql1 = "insert into pay_taxscale (ID_NO,LOWERLIMIT,UPPERLIMIT,PERCENT16,FIXED,gender) " & _
                "values ('" & ID_NO & "','" & LOWERLIMIT & "','" & UPPERLIMIT & "','" & PERCENT16 & "','0','" & gender & "')"
        Else
            sSql1 = "UPDATE pay_taxscale SET LOWERLIMIT='" & LOWERLIMIT & "', UPPERLIMIT='" & UPPERLIMIT & "', PERCENT16='" & PERCENT16 & "' , gender='" & gender & "' where ID_NO='" & ID_NO & "'"
        End If
        If Common.servername = "Access" Then
            Try
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql1, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Catch ex As Exception
               If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            End Try
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql1, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=10>iAS</size>")
        Me.Close()       
    End Sub

    Private Sub XtraCompanyInfo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        'MAC =  getMacAddress()
       
        GetDefault()

    End Sub
    Private Sub GetDefault()
        If XtraIncomTaxSlab.ITax_ID_NO = "" Then
            TextEditLowerLimit.Text = "0"
            TextEditUpperLimit.Text = "0"
            TextEditPercent.Text = "0"
            ComboBoxEditGender.SelectedIndex = 0
            Dim sSql As String = "Select * from pay_taxscale order by id_no desc"
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim dsL As DataSet = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(dsL)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(dsL)
            End If
            If dsL.Tables(0).Rows.Count = 0 Then
                ID_NO = 1
            Else
                ID_NO = dsL.Tables(0).Rows(0)("ID_NO") + 1
            End If
        Else
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim dsL As DataSet = New DataSet
            Dim sSql As String = "select * from pay_taxscale where ID_NO = " & XtraIncomTaxSlab.ITax_ID_NO
            'If Common.checkOpenedFromLicense = "Admin" Then
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(dsL)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(dsL)
            End If

            If dsL.Tables(0).Rows.Count > 0 Then
                TextEditLowerLimit.Text = dsL.Tables(0).Rows(0).Item("LOWERLIMIT").ToString.Trim
                TextEditUpperLimit.Text = dsL.Tables(0).Rows(0).Item("UPPERLIMIT").ToString.Trim
                TextEditPercent.Text = dsL.Tables(0).Rows(0).Item("PERCENT16").ToString.Trim
                If dsL.Tables(0).Rows(0).Item("gender").ToString.Trim = "M" Then
                    ComboBoxEditGender.SelectedIndex = 0
                Else
                    ComboBoxEditGender.SelectedIndex = 1
                End If
            End If           
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
End Class