﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Text
Imports System.Security.Cryptography
Imports System.Configuration

Public Class EncyDcry
    Public Shared Function Encrypt(ByVal toEncrypt As String, ByVal useHashing As Boolean) As String
        Dim keyArray As Byte()
        Dim toEncryptArray As Byte() = UTF8Encoding.UTF8.GetBytes(toEncrypt)
        Dim settingsReader As System.Configuration.AppSettingsReader = New AppSettingsReader()
        Dim key As String = "NTimeWatchA@2020"
        If useHashing Then
            Dim hashmd5 As MD5CryptoServiceProvider = New MD5CryptoServiceProvider()
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key))
            hashmd5.Clear()
        Else
            keyArray = UTF8Encoding.UTF8.GetBytes(key)
        End If
        Dim tdes As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider()
        tdes.Key = keyArray
        tdes.Mode = CipherMode.ECB
        tdes.Padding = PaddingMode.PKCS7
        Dim cTransform As ICryptoTransform = tdes.CreateEncryptor()
        Dim resultArray As Byte() = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length)
        tdes.Clear()
        Return Convert.ToBase64String(resultArray, 0, resultArray.Length)
    End Function
    Public Shared Function Decrypt(ByVal cipherString As String, ByVal useHashing As Boolean) As String
        Try
            Dim keyArray As Byte()
            Dim toEncryptArray As Byte() = Convert.FromBase64String(cipherString)
            Dim settingsReader As System.Configuration.AppSettingsReader = New AppSettingsReader()
            Dim key As String = "NTimeWatchA@2020"
            If useHashing Then
                Dim hashmd5 As MD5CryptoServiceProvider = New MD5CryptoServiceProvider()
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key))
                hashmd5.Clear()
            Else
                keyArray = UTF8Encoding.UTF8.GetBytes(key)
            End If
            Dim tdes As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider()
            tdes.Key = keyArray
            tdes.Mode = CipherMode.ECB
            tdes.Padding = PaddingMode.PKCS7
            Dim cTransform As ICryptoTransform = tdes.CreateDecryptor()
            Dim resultArray As Byte() = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length)
            tdes.Clear()
            Return UTF8Encoding.UTF8.GetString(resultArray)
        Catch ex As Exception
            Return ""
        End Try
    End Function
End Class
