﻿Imports System
Imports System.Windows.Forms
Imports System.Runtime.InteropServices
Imports ComDllsSample_CS

Public Class GlobalConstants
	Public Const gstrNoDevice As String = "No Connection"

	Public Const NO_DEVICE_CONNECTION_ERROR_CODE As Integer = -1023
	Public Const USER_ROLE_COMBO_EMPTY_ERROR_CODE As Integer = -1024

	Public Const USER_ID_LENGTH As Short = 32
	Public Const ACCESS_GROUP_NAME_LENGTH As Short = 16
	Public Const HOLIDAY_NAME_LENGTH As Short = 16
	Public Const MAX_ACCESSGROUP_COUNT As Short = 50
	Public Const MAX_HOLIDAY_COUNT As Short = 50
	Public Const MAX_BELLCOUNT_DAY As Short = 24
	Public Const MAX_PASSCTRLGROUP_COUNT As Short= 50
	Public Const MAX_PASSCTRL_COUNT As Short = 7
	Public Const MAX_USERPASSINFO_COUNT As Short = 3
	Public Const MAX_GROUPPASSINFO_COUNT As Short = 3
	Public Const MAX_GROUPMATCHINFO_COUNT As Short = 10
	Public Const NEWS_EXTEND As Short = 2
	Public Const NEWS_STANDARD As Short = 1

	Public Const SIZE_BELLINFO As Short = 72
	Public Const SIZE_PASSTIME As Short = 4
	Public Const SIZE_PASSCRLTIME As Short = 28
	Public Const SIZE_USERPASSINFO As Short = 3
	Public Const SIZE_GROUPPASSINFO As Short = 3
	Public Const SIZE_GROUPMATCHINFO As Short = 20

	Public Const MAX_SHIFTCOUNT As Short = 24
	Public Const MAX_POSTCOUNT As Short = 16
	Public Const NAME_BYTE_SIZE As Short = 14
	Public Const MAX_SHIFTDAY As Short = 32

	Public Const MAX_SHIFT_COUNT As Short = 24
	Public Const MAX_DEPT_COUNT As Short = 16
	Public Const NAME_BYTE_COUNT As Short = 128
	Public Const MAX_DAY_IN_MONTH As Short = 32

	Public Const SIZE_USER_INFO_V2 As Short = 184
	Public Const SIZE_USER_INFO_V3 As Short = 196

	Public Const VER2_USER_INFO As Short = 2
	Public Const VER3_USER_INFO As Short = 3

	Public Const SIZE_DEPT_SHIFT_INFO_V2 As Short = 2476
	Public Const VER2_DEPT_SHIFT_INFO As Short = 2

	Public Const USER_ROLE_NAME_BYTE_COUNT As Short = 64
End Class


'******************************************************************/
'*                            Structure                           */
'******************************************************************/

Friend Structure BELLINFO
	<MarshalAs(UnmanagedType.ByValArray, SizeConst:=GlobalConstants.MAX_BELLCOUNT_DAY)>
	Public mValid() As Byte
	<MarshalAs(UnmanagedType.ByValArray, SizeConst:=GlobalConstants.MAX_BELLCOUNT_DAY)>
	Public mHour() As Byte
	<MarshalAs(UnmanagedType.ByValArray, SizeConst:=GlobalConstants.MAX_BELLCOUNT_DAY)>
	Public mMinute() As Byte
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.MAX_BELLCOUNT_DAY)>
	Public mDuration() As Byte
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.MAX_BELLCOUNT_DAY)>
	Public mSunday() As Byte
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.MAX_BELLCOUNT_DAY)>
	Public mMonday() As Byte
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.MAX_BELLCOUNT_DAY)>
	Public mTuesday() As Byte
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.MAX_BELLCOUNT_DAY)>
	Public mWednesday() As Byte
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.MAX_BELLCOUNT_DAY)>
	Public mThursday() As Byte
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.MAX_BELLCOUNT_DAY)>
	Public mFriday() As Byte
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.MAX_BELLCOUNT_DAY)>
	Public mSaturday() As Byte
End Structure '264byte

Friend Structure SETBELLINFO
	Public mBellID As Byte
	Public mValid As Byte
	Public mHour As Byte
	Public mMinute As Byte
	Public mDuration As Byte
	Public mSunday As Byte
	Public mMonday As Byte
	Public mTuesday As Byte
	Public mWednesday As Byte
	Public mThursday As Byte
	Public mFriday As Byte
	Public mSaturday As Byte

End Structure '12byte

Friend Structure SHIFTCYCLEINFO
	Public Sunday As Byte
	Public Monday As Byte
	Public Tuesday As Byte
	Public Wednesday As Byte
	Public Thursday As Byte
	Public Friday As Byte
	Public Saturday As Byte
	Public Reserved As Byte
End Structure '8byte


Friend Structure ACCESSGROUPINFO
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.ACCESS_GROUP_NAME_LENGTH)>
	Public GroupName() As Byte
	Public GroupNo As Byte
	Public TimeGroupID1 As Byte
	Public TimeGroupID2 As Byte
	Public TimeGroupID3 As Byte
	Public DelFlag As Byte
	Public Reserved As Byte
	Public Sub Init()
		GroupName = New Byte(GlobalConstants.ACCESS_GROUP_NAME_LENGTH - 1){}
	End Sub
End Structure ' 22 byte

Friend Structure ACCESSGROUPSINFO
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.MAX_ACCESSGROUP_COUNT)>
	Public AccessGroupInfo() As ACCESSGROUPINFO
End Structure ' 22 * 50 = 1100 byte

Friend Structure GENERALDATE
	Public Month As Byte
	Public Day As Byte
End Structure '2byte

Friend Structure HOLIDAYINFO
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.HOLIDAY_NAME_LENGTH)>
	Public HolidayName() As Byte
	Public HolidayNo As Byte
	Public StartDate As GENERALDATE
	Public EndDate As GENERALDATE
	Public TimeGroupID As Byte
	Public DelFlag As Byte
	Public Reserved As Byte
	Public Sub Init()
		HolidayName = New Byte(GlobalConstants.HOLIDAY_NAME_LENGTH - 1){}
	End Sub
End Structure ' 24byte

Friend Structure HOLIDAYSINFO
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.MAX_HOLIDAY_COUNT)>
	Public HolidayInfo() As HOLIDAYINFO
End Structure ' 24 * 50 = 1200 byte

'--- Pass Control Time ---
Friend Structure PASSTIME
	Public StartHour As Byte
	Public StartMinute As Byte
	Public EndHour As Byte
	Public EndMinute As Byte
End Structure '4byte

		'--- Pass Control Time Infomation ---
Friend Structure PASSCTRLTIME
	<MarshalAs(UnmanagedType.ByValArray, SizeConst:=GlobalConstants.MAX_PASSCTRL_COUNT)>
	Public PassCtrlTime() As PASSTIME
End Structure '28byte

Friend Structure USERPASSINFO
	<MarshalAs(UnmanagedType.ByValArray, SizeConst:=GlobalConstants.MAX_USERPASSINFO_COUNT)>
	Public UserPassID() As Byte
End Structure
'3byte

Friend Structure GROUPPASSINFO
	<MarshalAs(UnmanagedType.ByValArray, SizeConst:=GlobalConstants.MAX_GROUPPASSINFO_COUNT)>
	Public GroupPassID() As Byte
End Structure
'3byte


Friend Structure GROUPMATCHINFO
	<MarshalAs(UnmanagedType.ByValArray, SizeConst:=GlobalConstants.MAX_GROUPMATCHINFO_COUNT)>
	Public GroupMatch() As Short
End Structure
'20byte
Friend Structure SHIFT_TIME_SLOT
	Public AMStartH As Byte ' AM time(hour)
	Public AMStartM As Byte ' AM min(minute)
	Public AMEndH As Byte ' AM time(hour)
	Public AMEndM As Byte ' AM min(minute)
	Public PMStartH As Byte ' PM time(hour)
	Public PMStartM As Byte ' PM min(minute)
	Public PMEndH As Byte ' PM time(hour)
	Public PMEndM As Byte ' PM min(minute)
	Public OVStartH As Byte ' OV time(hour)
	Public OVStartM As Byte ' OV min(minute)
	Public OVEndH As Byte ' OV time(hour)
	Public OVEndM As Byte ' OV min(minute)

	Public Sub Init()
		AMStartH = 0
		AMStartM = 0
		AMEndH = 0
		AMEndM = 0

		PMStartH = 0
		PMStartM = 0
		PMEndH = 0
		PMEndM = 0

		OVStartH = 0
		OVStartM = 0
		OVEndH = 0
		OVEndM = 0
	End Sub
End Structure ' 12 bytes


Friend Structure SHIFT_INFO
	'public Int32 Size;                      // 0, 4
	'public Int32 Ver;                       // 4, 4
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.MAX_SHIFT_COUNT)>
	Public ShiftTime() As SHIFT_TIME_SLOT ' 8, 288 (=12*24)

	Public Sub Init()
		'Size = 296;
		'Ver = 3;
		ShiftTime = New SHIFT_TIME_SLOT(GlobalConstants.MAX_SHIFT_COUNT - 1){}
	End Sub
End Structure ' size = 288 bytes

'20byte
Friend Structure SET_SHIFT_INFO
	Public ShiftID As Byte
	Public AMStartH As Byte ' AM time(hour)
	Public AMStartM As Byte ' AM min(minute)
	Public AMEndH As Byte ' AM time(hour)
	Public AMEndM As Byte ' AM min(minute)
	Public PMStartH As Byte ' PM time(hour)
	Public PMStartM As Byte ' PM min(minute)
	Public PMEndH As Byte ' PM time(hour)
	Public PMEndM As Byte ' PM min(minute)
	Public OVStartH As Byte ' OV time(hour)
	Public OVStartM As Byte ' OV min(minute)
	Public OVEndH As Byte ' OV time(hour)
	Public OVEndM As Byte ' OV min(minute)
	Public Reserved As Byte ' OV min(minute)
End Structure ' 14byte


Friend Structure DEPT_NAME
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.NAME_BYTE_SIZE)>
	Public PostName() As Byte

	Public Sub Init()
		PostName = New Byte(GlobalConstants.NAME_BYTE_SIZE - 1){}
	End Sub
End Structure ' 14 bytes


Friend Structure DEPT_INFO
	'public Int32 Size;                      // 0, 4
	'public Int32 Ver;                       // 4, 4
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.MAX_DEPT_COUNT)>
	Public PostInfo() As DEPT_NAME ' 8, 224 (=14*16)

	Public Sub Init()
		'Size = 232;
		'Ver = 3;
		PostInfo = New DEPT_NAME(GlobalConstants.MAX_DEPT_COUNT - 1){}
	End Sub
End Structure ' size = 224 bytes

Friend Structure SET_DEPT_INFO
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.NAME_BYTE_SIZE)>
	Public PostName() As Byte
	Public PostID As Byte
	Public Reserved As Byte
	Public Sub Init()
		PostName = New Byte(GlobalConstants.NAME_BYTE_SIZE - 1){}
	End Sub
End Structure ' 16 bytes


Friend Structure ENROLL_DATA_STRING_ID
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.NAME_BYTE_COUNT)>
	Public UserName() As Byte ' 128
	Public Card As UInt32 ' 8
	Public Password As UInt32 ' 16
	Public FpCount As Byte ' 20
	Public FaceIndex As Byte ' 18
	Public Privilege As Byte ' 22
	Public Reserved As Byte ' 24
	Public Sub Init()
		UserName = New Byte(GlobalConstants.NAME_BYTE_COUNT - 1){}
	End Sub
End Structure

Friend Structure USER_NAME_STRING_ID
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.NAME_BYTE_COUNT)>
	Public UserName() As Byte ' 128
	Public Sub Init()
		UserName = New Byte(GlobalConstants.NAME_BYTE_COUNT - 1){}
	End Sub
End Structure

Friend Structure ENROLL_SETTING_INFO_STRING_ID
	'[MarshalAs(UnmanagedType.ByValArray, SizeConst = GlobalConstants.USER_ID_LENGTH)]
	'public byte[] UserId;
	Public AccessGroupID As Byte
	Public ShiftID As Byte
	Public DuressFpCount As Byte
	Public DailyPunchCount As Byte
	Public VaildFromStartYear As Byte
	Public VaildFromStartMonth As Byte
	Public VaildFromStartDay As Byte
	Public VaildFromEndYear As Byte
	Public VaildFromEndMonth As Byte
	Public VaildFromEndDay As Byte
	'public void Init()
	'{
	'    UserId = new byte[GlobalConstants.USER_ID_LENGTH];
	'}
End Structure ' size = 10 bytes

Friend Structure DAYLIGHT_SAVING_DATA
	Public nSummerStartMonth As Byte
	Public nSummerStartDay As Byte
	Public nSummerStartHour As Byte
	Public nSummerStartMinute As Byte
	Public nSummerEndMonth As Byte
	Public nSummerEndDay As Byte
	Public nSummerEndHour As Byte
	Public nSummerEndMinute As Byte
	Public nWinterStartMonth As Byte
	Public nWinterStartDay As Byte
	Public nWinterStartHour As Byte
	Public nWinterStartMinute As Byte
	Public nWinterEndMonth As Byte
	Public nWinterEndDay As Byte
	Public nWinterEndHour As Byte
	Public nWinterEndMinute As Byte
	Public nSeasonMode As Byte
	Public nReserved As Byte
End Structure

Friend Structure DEFINE_USER_ROLE
	<MarshalAs(UnmanagedType.ByValArray, SizeConst := GlobalConstants.USER_ROLE_NAME_BYTE_COUNT)>
	Public UserRoleName() As Byte '64
	Public UserRoleNo As Byte '65
	Public ManUser As Byte '66
	Public ManUser_EnrollUser As Byte '67
	Public ManUser_EditUser As Byte '68
	Public ManUser_DownUser As Byte '69
	Public ManUser_UpUser As Byte '70
	Public System As Byte '71
	Public System_DevNo As Byte '72
	Public System_Lang As Byte '73
	Public System_AttStatus As Byte '74
	Public System_Verify As Byte '75
	Public System_Alarm As Byte '76
	Public System_DateTime As Byte '77
	Public System_Default As Byte '78
	Public System_Upgrade As Byte '79
	Public System_SysInfo As Byte '80
	Public System_AutoTest As Byte '81
	Public System_Shift As Byte '82
	Public DataMan As Byte '83
	Public DataMan_DelAllData As Byte '84
	Public DataMan_DelUsers As Byte '85
	Public DataMan_DelAttRecords As Byte '86
	Public DataMan_DownReport As Byte '87
	Public DataMan_DownLogs As Byte '88
	Public Comm As Byte '89
	Public Comm_Mode As Byte '90
	Public Comm_Ethernet As Byte '91
	Public Comm_Wifi As Byte '92
	Public Comm_Serial As Byte '93
	Public Comm_Wiegand As Byte '94
	Public Comm_RealTime As Byte '95
	Public Comm_ServerConf As Byte '96
	Public Access As Byte '97
	Public Access_CtrlOption As Byte '98
	Public Access_TimeZone As Byte '99
	Public Access_Groups As Byte '100
	Public Access_UserRole As Byte '101
	Public Access_Holiday As Byte '102
	Public Usb As Byte '103
	Public Usb_Down As Byte '104
	Public Usb_Up As Byte '105
	Public Usb_Options As Byte '106
	Public Attend As Byte '107
	Public Attend_Record As Byte '108
	Public Person As Byte '109
	Public Person_Interface As Byte '110
	Public Person_Volume As Byte '111
	Public Person_VerifyScreenTime As Byte '112
	Public Person_PunchInterval As Byte '113
	Public Person_FKey As Byte '114
	'public byte Reserved;                   //114
End Structure

Friend Structure FKEY_MAPPING_INFO
	Public AttStatus As Byte
	Public DownloadReport As Byte
	Public Comm As Byte
	Public EnrollUser As Byte
	Public AccessTimeZone As Byte
	Public VerifyMode As Byte
	Public EditUser As Byte
	Public AccessGroup As Byte
	Public Home As Byte
	Public Reserved As Byte
End Structure ' 10 byte

'=============== USBModel Kind  ===============//

'=============== Protocol Type ===============//
Public Enum enumProtocolType
	PROTOCOL_TCPIP = 0 ' TCP/IP
	PROTOCOL_UDP = 1 ' UDP
End Enum

'=============== Backup Number Constant ===============//
Public Enum enumBackupNumberType
	BACKUP_FP_0 = 0 ' Finger 0
	BACKUP_FP_1 = 1 ' Finger 1
	BACKUP_FP_2 = 2 ' Finger 2
	BACKUP_FP_3 = 3 ' Finger 3
	BACKUP_FP_4 = 4 ' Finger 4
	BACKUP_FP_5 = 5 ' Finger 5
	BACKUP_FP_6 = 6 ' Finger 6
	BACKUP_FP_7 = 7 ' Finger 7
	BACKUP_FP_8 = 8 ' Finger 8
	BACKUP_FP_9 = 9 ' Finger 9
	BACKUP_FP_10 = 10 ' Finger 10
	BACKUP_FP_11 = 11 ' Finger 11
	BACKUP_FP_12 = 12 ' Finger 12
	BACKUP_FP_13 = 13 ' Finger 13
	BACKUP_FP_14 = 14 ' Finger 14
	BACKUP_FP_15 = 15 ' Finger 15
	BACKUP_FP_16 = 16 ' Finger 16
	BACKUP_FP_17 = 17 ' Finger 17
	BACKUP_FP_18 = 18 ' Finger 18
	BACKUP_FP_19 = 19 ' Finger 19

	BACKUP_PSW = 20 ' Password
	BACKUP_CARD = 21 ' Card
	BACKUP_FACE = 22 ' Face
	BACKUP_PALMVEIN_0 = 23
	BACKUP_PALMVEIN_1 = 24
	BACKUP_PALMVEIN_2 = 25
	BACKUP_PALMVEIN_3 = 26

	BACKUP_IRIS_0 = 27
	BACKUP_IRIS_1 = 28
	BACKUP_IRIS_2 = 29

	BACKUP_VEIN_0 = 30 ' Vein 0
	BACKUP_VEIN_9 = 39 ' Vein 0

	BACKUP_PHOTO = 40 ' Photo 0

	BACKUP_VFACE = 50 ' Photo 0

	BACKUP_MAX = 51 ' Photo 0
End Enum

'public enum enumBackupNumberType
'{
'    BACKUP_FP_0 = 0,        // Finger 0
'    BACKUP_FP_1 = 1,        // Finger 1
'    BACKUP_FP_2 = 2,        // Finger 2
'    BACKUP_FP_3 = 3,        // Finger 3
'    BACKUP_FP_4 = 4,        // Finger 4
'    BACKUP_FP_5 = 5,        // Finger 5
'    BACKUP_FP_6 = 6,        // Finger 6
'    BACKUP_FP_7 = 7,        // Finger 7
'    BACKUP_FP_8 = 8,        // Finger 8
'    BACKUP_FP_9 = 9,        // Finger 9
'    BACKUP_PSW = 10,        // Password
'    BACKUP_CARD = 11,       // Card
'    BACKUP_FACE = 12,       // Face
'    BACKUP_PALMVEIN_0 = 13,
'    BACKUP_PALMVEIN_1 = 14,
'    BACKUP_PALMVEIN_2 = 15,
'    BACKUP_PALMVEIN_3 = 16,
'    BACKUP_VEIN_0 = 20,     // Vein 0
'    BACKUP_VFACE = 30,     // VFace 0
'};
'=============== VerifyMode of GeneralLogData ===============//
Public Enum enumGLogVerifyMode
	LOG_FPVERIFY = 1 ' Fp Verify
	LOG_PASSVERIFY = 2 ' Pass Verify
	LOG_CARDVERIFY = 3 ' Card Verify
	LOG_FPPASS_VERIFY = 4 ' Pass+Fp Verify
	LOG_FPCARD_VERIFY = 5 ' Card+Fp Verify
	LOG_PASSFP_VERIFY = 6 ' Pass+Fp Verify
	LOG_CARDFP_VERIFY = 7 ' Card+Fp Verify
	LOG_JOB_NO_VERIFY = 8 ' Job number Verify
	LOG_CARDPASS_VERIFY = 9 ' Card+Pass Verify
	LOG_CLOSE_DOOR = 10 ' Door Close
	LOG_OPEN_HAND = 11 ' Hand Open
	LOG_PROG_OPEN = 12 ' Open by PC
	LOG_PROG_CLOSE = 13 ' Close by PC
	LOG_OPEN_IREGAL = 14 ' Iregal Open
	LOG_CLOSE_IREGAL = 15 ' Iregal Close
	LOG_OPEN_COVER = 16 ' Cover Open
	LOG_CLOSE_COVER = 17 ' Cover Close

	LOG_FACEVERIFY = 20 ' Face Verify
	LOG_FACECARDVERIFY = 21 ' Face+Card Verify
	LOG_FACEPASSVERIFY = 22 ' Face+Pass Verify
	LOG_CARDFACEVERIFY = 23 ' Card+Face Verify
	LOG_PASSFACEVERIFY = 24 ' Pass+Face Verify
	LOG_FACEFPVERIFY = 25 ' Face + FP Verify

	LOG_OPEN_DOOR = 32 ' Door Open
	LOG_OPEN_THREAT = 48 ' Door Open as threat
End Enum
'=============== IOMode of GeneralLogData ===============//
Public Enum enumGLogIOMode
	LOG_MODE_IO = 0 ' General
	LOG_MODE_IN1 = 1 ' IN1
	LOG_MODE_OUT1 = 2 ' OUT1
	LOG_MODE_IN2 = 3 ' IN2
	LOG_MODE_OUT2 = 4 ' OUT2
	LOG_MODE_IN3 = 5 ' IN3
	LOG_MODE_OUT3 = 6 ' OUT3
	LOG_MODE_IN4 = 7 ' IN3
	LOG_MODE_OUT4 = 8 ' OUT3
	LOG_MODE_IN5 = 9 ' IN3
	LOG_MODE_OUT6 = 10 ' OUT3
End Enum
'=============== Machine Privilege ===============//
Public Enum enumMachinePrivilege
	MP_NONE = 0 ' General user
	MP_ALL = 1 ' Admin
	MP_ROLE1 = 6 ' UserRole1
	MP_ROLE2 = 7 ' UserRole2
	MP_ROLE3 = 8 ' UserRole3
	MP_ROLE4 = 9 ' UserRole4
	MP_ROLE5 = 10 ' UserRole5
End Enum

'=============== Index of  GetDeviceInfo ===============//
Public Enum enumGetDeviceInfo
	DI_MANAGERS = 1 ' Numbers of Manager
	DI_MACHINENUM = 2 ' Device ID
	DI_LANGAUGE = 3 ' Language
	DI_GLOG_WARNING = 6
	DI_REVERIFYTIME = 8 ' Verify Interval Time
	DI_VERIFYMODE = 11
	DI_BELLCOUNT = 15

	DI_DATE_FORMAT = 22
	DI_POWERKEYUSE = 27 'nabi modify

	DI_DOOR_OPENTIME = 28 ' #define __DI_DOOROPENTIME              28
	DI_DOOR_SENSORKIND = 30 ' #define __DI_DOORSENSORKIND          30
	DI_DOOR_SENSORTIME = 31 ' #define __DI_LOCKDELAY              31
	DI_VOLUME = 86
	DI_SLEEPTIME = 91
	DI_SHOWRESULTTIME = 102
	DI_SHOWADVERTTIME = 103
	DI_BELLUSED = 107
	DI_ALLOW_LATETIME = 110
	DI_ALLOW_EARLYTIME = 111
	DI_TIMEZONE = 113
	DI_DOOR_ANTIPASS = 130
	DI_DOOR_ALARMDELAY = 131
	DI_DOOR_MULTIUSERS = 132
	DI_USE_ALARM1 = 141
	DI_USEIOMODE = 153
	DI_DOOR_STATUS = 154
	DI_USEWORKCODE = 156
	DI_TIME_FORMAT = 157 'nabi added
	DI_DAYLIGHT_SAVING_TIME = 158 'nabi added
	DI_DAYLIGHT_SAVING_MODE = 159 'nabi added
	DI_DAYLIGHT_START_MONTH = 160 'nabi added
	DI_DAYLIGHT_START_WEEK = 161 'nabi added
	DI_DAYLIGHT_START_DAY = 162 'nabi added
	DI_DAYLIGHT_START_TIME = 163 'nabi added
	DI_DAYLIGHT_END_MONTH = 164 'nabi added
	DI_DAYLIGHT_END_WEEK = 165 'nabi added
	DI_DAYLIGHT_END_DAY = 166 'nabi added
	DI_DAYLIGHT_END_TIME = 167 'nabi added
	DI_DAYLIGHT_SUMMER_START_DATE = 168 'nabi added
	DI_DAYLIGHT_SUMMER_END_DATE = 169 'nabi added
	DI_DAYLIGHT_SUMMER_START_TIME = 170 'nabi added
	DI_DAYLIGHT_SUMMER_END_TIME = 171 'nabi added
	DI_ATT_STATUS = 172 'nabi added
	DI_REPORT_DOWNLOAD = 173 'nabi added
	DI_COMM_SETTING = 174 'nabi added
	DI_ENROLL_USER = 175 'nabi added
	DI_ACCESS_TIME_ZONE = 176 'nabi added
	DI_VERIFY_MODE = 177 'nabi added
	DI_EDIT_USER = 178 'nabi added
	DI_ACCESS_GROUP = 179 'nabi added

	DI_ATT_STATUS_SETTING = 180 'nabi added
	DI_GO_HOME = 181 'nabi added
	DI_ENCRYPT_LOGS = 182 'nabi added
	DI_DELETE_LOGS = 183 'nabi added
	DI_CYCLIC_DELETE_ATT_DATA = 184 'nabi added

	DI_DAYLIGHT_WINTER_START_DATE = 185 'nabi added
	DI_DAYLIGHT_WINTER_END_DATE = 186 'nabi added
	DI_DAYLIGHT_WINTER_START_TIME = 187 'nabi added
	DI_DAYLIGHT_WINTER_END_TIME = 188 'nabi added
	DI_DAYLIGHT_SEASON = 189 'nabi added
	DI_MENU_TIME = 190 'nabi added
	DI_DAYLIGHT_SUMMER_START = 191 'nabi added
	DI_DAYLIGHT_SUMMER_END = 192 'nabi added
	DI_DAYLIGHT_WINTER_START = 193 'nabi added
	DI_DAYLIGHT_WINTER_END = 194 'nabi added
	DI_WEEK_ATTEND_DAYS = 195 'nabi added
	DI_SERVERMODE = 196 'nabi added
	DI_FACEENABLE = 197 'nabi added

	NEW_LOG_COUNT = 300 'nabi added
	ALL_LOG_COUNT = 301 'nabi added

	DI_REVERIFYTIMEBYSECOND
End Enum
'=============== Baudrate = value of DI_RSCOM_BPS ===============//
Public Enum enumBaudrate
	BPS_9600 = 3
	BPS_19200 = 4
	BPS_38400 = 5
	BPS_57600 = 6
	BPS_115200 = 7
End Enum



'=============== Error code ===============//
Public Enum enumErrorCode
	RETURN_SUCCESS = 1
	RETURN_DATAARRAY_END = -7
	RETURN_DATAARRAY_NONE = -8
End Enum
Public Enum enumVerifyKind
	VK_NONE = 0
	VK_FP = 1
	VK_PASS = 2
	VK_CARD = 3
	VK_FACE = 4
	VK_FINGERVEIN = 5
	VK_IRIS = 6
	VK_PALMVEIN = 7
	VK_VOICE = 8
	VK_VFACE = 9
End Enum

Public Enum enumGLogDoorMode

	LOG_CLOSE_DOOR = 1 ' Door Close
	LOG_OPEN_HAND = 2 ' Hand Open
	LOG_PROG_OPEN = 3 ' Open by PC
	LOG_PROG_CLOSE = 4 ' Close by PC
	LOG_OPEN_IREGAL = 5 ' Illegal Open
	LOG_CLOSE_IREGAL = 6 ' Illegal Close
	LOG_OPEN_COVER = 7 ' Cover Open
	LOG_CLOSE_COVER = 8 ' Cover Close
	LOG_OPEN_DOOR = 9 ' Door Open
	LOG_OPEN_DOOR_THREAT = 10 ' Door Open
	LOG_FORCE_OPEN_DOOR = 11 ' Door Open
	LOG_FORCE_CLOSE_DOOR_ = 12 ' Door Open
End Enum


Public Enum enumFKeyMode
	KEY_NONE = 0
	KEY_F1 = 1
	KEY_F2 = 2
	KEY_F3 = 3
	KEY_F4 = 4
End Enum