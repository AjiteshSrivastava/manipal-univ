﻿Imports System.Resources
Imports System.Globalization
Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports DevExpress.XtraEditors
Imports DevExpress.LookAndFeel
Imports System.Net.Mail
Imports DevExpress.Skins.XtraForm
Imports DevExpress.Utils
Imports DevExpress.Skins
Imports System.Threading
Imports System.Text.RegularExpressions
Imports iAS.Common

Public Class XtraMasterDashBoard
    Dim ulf As UserLookAndFeel
    Public Shared realTIme As String
    Public Sub New()
        InitializeComponent()
        'Me.WindowState = FormWindowState.Maximized
        'Me.Width = My.Computer.Screen.WorkingArea.Width
        'Me.Height = My.Computer.Screen.WorkingArea.Height
        'PanelControl1.Width = My.Computer.Screen.WorkingArea.Width
        'PanelControl1.Height = OfficeNavigationBar1.Height - My.Computer.Screen.WorkingArea.Height
        'OfficeNavigationBar1.Width = My.Computer.Screen.WorkingArea.Width
        'PanelControl1.Width = My.Computer.Screen.WorkingArea.Width
        'PanelControl1.Height = My.Computer.Screen.WorkingArea.Height - 70
        'SidePanelMainFormShow.Width = My.Computer.Screen.WorkingArea.Width
        'SidePanelMainFormShow.Height = My.Computer.Screen.WorkingArea.Height - SidePanel1.Height
    End Sub
    'Protected Overrides Function CreateFormBorderPainter() As DevExpress.Skins.XtraForm.FormPainter
    '    Dim formCaptionAlignment As HorzAlignment = HorzAlignment.Center
    '    Return New CustomFormPainter(Me, LookAndFeel, formCaptionAlignment)
    'End Function
    Private Sub XtraMaster_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
      
        'Dim msg As New Message()
        'AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        Me.WindowState = FormWindowState.Maximized
        Me.MaximizedBounds = Screen.GetWorkingArea(Me)
        'Me.Top = (My.Computer.Screen.WorkingArea.Height / 2) - (Me.Height / 2)
        'Me.Left = (My.Computer.Screen.WorkingArea.Width / 2) - (Me.Width / 2)
        'Me.Width = My.Computer.Screen.WorkingArea.Width
        'Me.Height = My.Computer.Screen.WorkingArea.Height
        'PanelControl1.Width = My.Computer.Screen.WorkingArea.Width
        'PanelControl1.Height = OfficeNavigationBar1.Height - My.Computer.Screen.WorkingArea.Height

        'for this page only where barcontrol is being used
        Common.NavHeight = PanelControl1.Height
        Common.NavWidth = PanelControl1.Width
        Common.SplitterPosition = (Common.NavWidth) * 85 / 100

        SidePanelMenu.Height = StandaloneBarDockControl1.Height
        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraMaster).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")
        'Me.Text = Common.res_man.GetString("timewatch", Common.cul)
        'Me.Text = "Integrated Attendance System"
        LabelControl2.Text = " Application Login Time " & Now.ToString("dd MMM yyyy HH:mm")
        LabelControlUser.Text = Common.USER_R
        Application.DoEvents()

        ''OfficeNavigationBar1.Width = My.Computer.Screen.WorkingArea.Width
        'PanelControl1.Width = My.Computer.Screen.WorkingArea.Width
        'PanelControl1.Height = My.Computer.Screen.WorkingArea.Height - 70

        'SidePanelMainFormShow.Width = My.Computer.Screen.WorkingArea.Width
        'SidePanelMainFormShow.Height = My.Computer.Screen.WorkingArea.Height - SidePanel1.Height
        SidePanelMainFormShow.Controls.Clear()

        Dim form As UserControl = New XtraHomeNew 'XtraHome
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        SidePanelTitle.Visible = False  'for XtraHome

        ''check full payroll License
        'If Common.DbMac = Common.SysyemMac And Common.UserKey = Common.SysyemMac Then
        '    Dim UserKeyNum As Double = XtraCompanyInfo.Num(Common.SysyemMac)
        '    Dim LicenseNum As Double = (UserKeyNum * 9999) + Convert.ToDouble(1234567890) * 12345
        '    Dim licenseStr As String = Regex.Replace(LicenseNum.ToString, ".{5}", "$0-").TrimEnd(CChar("-"))
        '    If licenseStr = Common.License Then
        '        Common.IsFullPayroll = True
        '    Else
        '        Common.IsFullPayroll = False
        '    End If
        'Else
        Common.IsFullPayroll = False
        'End If
        If Common.IsFullPayroll = True Then
            BarButtonItemPEmp.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            BarButtonItemPSetup.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            BarButtonItemFormula.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            BarButtonItemPayPro.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            BarButtonItemPerMain.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            BarButtonItemLoan.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            BarButtonItemPayReports.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        Else
            BarButtonItemPEmp.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            BarButtonItemPSetup.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            BarButtonItemFormula.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            BarButtonItemPayPro.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            BarButtonItemPerMain.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            BarButtonItemLoan.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            BarButtonItemPayReports.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        End If
        'end check full payroll License



        If Common.Main = "Y" Then
            BarSubItemMaster.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            If Common.Company = "Y" Then
                BarButtonItemComp.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            Else
                BarButtonItemComp.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If

            If Common.Department = "Y" Then
                BarButtonItemDept.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            Else
                BarButtonItemDept.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If

            If Common.Section = "Y" Then 'emp grp
                BarButtonItemEmpGrp.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            Else
                BarButtonItemEmpGrp.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If

            If Common.Grade = "Y" Then
                BarButtonItemGrade.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            Else
                BarButtonItemGrade.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If
            If Common.Category = "Y" Then
                BarButtonItemCat.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            Else
                BarButtonItemCat.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If
            If Common.Shift = "Y" Then
                BarButtonItemShift.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            Else
                BarButtonItemShift.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If
            If Common.Employee = "Y" Then
                BarButtonItemEmp.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            Else
                BarButtonItemEmp.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If
        Else
            BarSubItemMaster.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If
        If Common.Admin = "Y" Then
            BarSubItemAdmin.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarSubItemAdmin.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If
        If Common.Reports = "Y" Then
            BarSubItemReports.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarSubItemReports.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If
        If Common.Leave = "Y" Then
            BarSubItemLeaveMgmt.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarSubItemLeaveMgmt.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If
        If Common.V_Transaction = "Y" Then
            BarSubItemTrans.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarSubItemTrans.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.Payroll = "Y" Then
            BarSubItemPayRoll.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarSubItemPayRoll.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.UserPrevilege = "Y" Then
            BarButtonItemUManage.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemUManage.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If
        'new user mgmt
        If Common.DeviceMgmt = "Y" Then
            BarSubItemDevice.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarSubItemDevice.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If
        If Common.LogMgmt = "Y" Then
            BarButtonItemLogs.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemLogs.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If
        If Common.UserSetupTemplate = "Y" Then
            BarButtonItemUser.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemUser.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.DBSetting = "Y" Then
            BarButtonItemDB.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemDB.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.SMSSetting = "Y" Then
            BarButtonItemSMS.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemSMS.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.BulkSMS = "Y" Then
            BarButtonItemBSMS.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemBSMS.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.EmailSetting = "Y" Then
            BarButtonItemEmail.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemEmail.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.BackUpSetting = "Y" Then
            BarButtonItemBackUp.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemBackUp.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.ParallelSetting = "Y" Then
            BarButtonItemPDB.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemPDB.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.Branch = "Y" Then
            BarButtonItemLocation.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemLocation.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.MonthlyReport = "Y" Then
            BarButtonItemMReports.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemMReports.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.DalyReport = "Y" Then
            BarButtonItemDReports.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemDReports.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.LeaveReport = "Y" Then
            BarButtonItemLReports.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemLReports.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.Manual_Attendance = "Y" Then
            BarButtonItemManual.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemManual.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.HoliDay = "Y" Then
            BarButtonItemHoliday.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemHoliday.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.DataMaintenance = "Y" Then
            BarButtonItemDataMaint.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemDataMaint.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Common.LeaveAccuralAuto = "Y" Then
            BarButtonItemLeaveIncr.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            BarButtonItemLeaveIncr.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If
        'end new user mgmt


        'Application.DoEvents()
        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = " Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tb" & _
        "lsetup )"
        If Common.servername = "Access" Then
            sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
            adapAc = New OleDbDataAdapter(sSql, Common.con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, Common.con)
            adapS.Fill(Rs)
        End If


        If Rs.Tables(0).Rows(0).Item("Visitor").ToString.Trim = "Y" Then
            If Common.Visitor = "Y" Then
                'OfficeNavigationBar1.Items.Item(11).Visible = True
                BarSubItemVisitor.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            Else
                'OfficeNavigationBar1.Items.Item(11).Visible = False
                BarSubItemVisitor.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If
        Else
            'OfficeNavigationBar1.Items.Item(11).Visible = False
            BarSubItemVisitor.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If

        If Rs.Tables(0).Rows(0).Item("canteen").ToString.Trim = "Y" Then
            If Common.canteen = "Y" Then
                'OfficeNavigationBar1.Items.Item(10).Visible = True
                BarSubItemCanteen.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            Else
                'OfficeNavigationBar1.Items.Item(10).Visible = False
                BarSubItemCanteen.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If
        Else
            'OfficeNavigationBar1.Items.Item(10).Visible = False
            BarSubItemCanteen.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If


        TimerDataProcess.Enabled = True

    End Sub
    Private Sub XtraMaster_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If e.CloseReason = CloseReason.UserClosing Then
            If XtraMessageBox.Show(ulf, "<size=10>Are you sure to close the application?</size>", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.Yes Then
                e.Cancel = True
            End If
        End If
    End Sub    
    Private Sub XtraMaster_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            If Application.OpenForms().OfType(Of XtraRealTimePunches).Any Then  'to check real time is runing or not
                XtraRealTimePunches.Close()
            End If
            Application.Exit()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub TimerShowClock_Tick(sender As System.Object, e As System.EventArgs) Handles TimerShowClock.Tick
        LabelControlTime.Text = Now.ToString("HH:mm")
    End Sub
    Private Sub TimerReportEmail_Tick(sender As System.Object, e As System.EventArgs) Handles TimerReportEmail.Tick
        'ToastNotificationsManager1.ShowNotification(ToastNotificationsManager1.Notifications(0))
        TimerReportEmail.Enabled = False
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim ds1 As DataSet = New DataSet
        Dim dsEmail As DataSet = New DataSet

        Dim sSql As String = "select * from ReportEmailSetting"
        Try
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
        Catch ex As Exception
            TimerReportEmail.Enabled = True
            Exit Sub
        End Try

        If ds.Tables(0).Rows.Count > 0 Then
            If Now.ToString("HH:mm") = ds.Tables(0).Rows(0).Item("SentTime").ToString.Trim Then
                Dim Server As String
                Dim senderDB As String
                Dim type As String
                Dim username As String
                Dim password As String
                Dim IsEnable As Boolean
                Dim Port As String
                Dim SenderName As String
                Dim CCMail As String = ""
                sSql = "select * from tblMailSetup"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(dsEmail)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(dsEmail)
                End If
                If dsEmail.Tables(0).Rows.Count > 0 Then
                    Server = dsEmail.Tables(0).Rows(0).Item("Server").ToString.Trim
                    senderDB = dsEmail.Tables(0).Rows(0).Item("sender").ToString.Trim
                    type = dsEmail.Tables(0).Rows(0).Item("type").ToString.Trim
                    username = dsEmail.Tables(0).Rows(0).Item("username").ToString.Trim
                    password = dsEmail.Tables(0).Rows(0).Item("password").ToString.Trim
                    If dsEmail.Tables(0).Rows(0).Item("IsEnable").ToString.Trim = "Y" Then
                        IsEnable = True
                    Else
                        IsEnable = False
                    End If
                    Port = dsEmail.Tables(0).Rows(0).Item("Port").ToString.Trim
                    SenderName = dsEmail.Tables(0).Rows(0).Item("SenderName").ToString.Trim
                    CCMail = dsEmail.Tables(0).Rows(0).Item("CCMail").ToString.Trim
                Else
                    TimerReportEmail.Enabled = True
                    Exit Sub
                End If

                Dim daily As XtraReportsDaily = New XtraReportsDaily
                Common.ReportMail = True

                Dim locationArr() As String = ds.Tables(0).Rows(0).Item("BranchCode").ToString.Trim.Split(",")
                Dim reportType() As String = ds.Tables(0).Rows(0).Item("ReportType").ToString.Trim.Split(",")

                For k As Integer = 0 To locationArr.Length - 1
                    Common.whereClauseEmail = " tblEmployee.BRANCHCODE = '" & locationArr(k).Trim & "'"
                    sSql = "select Email from tblbranch where BRANCHCODE = '" & locationArr(k).Trim & "'"
                    ds1 = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(ds1)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(ds1)
                    End If
                    For i As Integer = 0 To reportType.Length - 1
                        If reportType(i).Trim = "Daily Attendance" Then
                            Common.reportName = "DailyAttendance" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
                            daily.SpotXl_AttendanceGrid("")

                            Dim Smtp_Server As New SmtpClient
                            Dim e_mail As New MailMessage()
                            Dim attachment As System.Net.Mail.Attachment
                            Smtp_Server.UseDefaultCredentials = False

                            'Smtp_Server.Credentials = New Net.NetworkCredential(("nittslam@gmail.com"), ("umawidnitinbs4"))
                            Smtp_Server.Credentials = New Net.NetworkCredential(senderDB, password)
                            Smtp_Server.Port = Convert.ToInt16(Port) '587
                            Smtp_Server.EnableSsl = IsEnable 'True
                            Smtp_Server.Host = Server '"smtp.gmail.com"
                            e_mail = New MailMessage()
                            'e_mail.From = New MailAddress("nittslam@gmail.com")
                            e_mail.From = New MailAddress(senderDB)
                            'e_mail.To.Add("nittslam@gmail.com")
                            Try
                                e_mail.To.Add(ds1.Tables(0).Rows(0).Item("Email").ToString.Trim)
                                If CCMail.Trim <> "" Then
                                    e_mail.CC.Add(CCMail)
                                End If
                                e_mail.IsBodyHtml = True
                                e_mail.IsBodyHtml = True

                                e_mail.Subject = "Daily Attendance Report"
                                e_mail.Body = "Please find the attached Daily Attendance Report for date " & Now.ToString("dd/MM/yyyy")
                                attachment = New System.Net.Mail.Attachment(My.Application.Info.DirectoryPath & "\Reports\" & Common.reportName)
                                e_mail.Attachments.Add(attachment)
                                Smtp_Server.Send(e_mail)
                            Catch ex As Exception
                            End Try

                        End If
                        If reportType(i).Trim = "Daily Late Arrival" Then
                            Common.reportName = "DailyLateArrival" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
                            daily.SpotXl_LateArrivalGrid("")

                            Dim Smtp_Server As New SmtpClient
                            Dim e_mail As New MailMessage()
                            Dim attachment As System.Net.Mail.Attachment
                            Smtp_Server.UseDefaultCredentials = False

                            'Smtp_Server.Credentials = New Net.NetworkCredential(("nittslam@gmail.com"), ("umawidnitinbs4"))
                            Smtp_Server.Credentials = New Net.NetworkCredential(senderDB, password)
                            Smtp_Server.Port = Convert.ToInt16(Port) '587
                            Smtp_Server.EnableSsl = IsEnable 'True
                            Smtp_Server.Host = Server '"smtp.gmail.com"
                            e_mail = New MailMessage()
                            'e_mail.From = New MailAddress("nittslam@gmail.com")
                            e_mail.From = New MailAddress(senderDB)
                            'e_mail.To.Add("nittslam@gmail.com")
                            Try
                                e_mail.To.Add(ds1.Tables(0).Rows(0).Item("Email").ToString.Trim)
                                If CCMail.Trim <> "" Then
                                    e_mail.CC.Add(CCMail)
                                End If
                                e_mail.IsBodyHtml = True
                                e_mail.IsBodyHtml = True

                                e_mail.Subject = "Daily Late Arrival"
                                e_mail.Body = "Please find the attached Daily Late Arrival Report for date " & Now.ToString("dd/MM/yyyy")
                                attachment = New System.Net.Mail.Attachment(My.Application.Info.DirectoryPath & "\Reports\" & Common.reportName)
                                e_mail.Attachments.Add(attachment)
                                Smtp_Server.Send(e_mail)
                            Catch ex As Exception
                            End Try

                        End If
                        If reportType(i).Trim = "Daily Absenteeism" Then
                            Common.reportName = "DailyAbsenteeism" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
                            daily.SpotXl_AbsenteeismGrid("")

                            Dim Smtp_Server As New SmtpClient
                            Dim e_mail As New MailMessage()
                            Dim attachment As System.Net.Mail.Attachment
                            Smtp_Server.UseDefaultCredentials = False
                            'Smtp_Server.Credentials = New Net.NetworkCredential(("nittslam@gmail.com"), ("umawidnitinbs4"))
                            Smtp_Server.Credentials = New Net.NetworkCredential(senderDB, password)
                            Smtp_Server.Port = Convert.ToInt16(Port) '587
                            Smtp_Server.EnableSsl = IsEnable 'True
                            Smtp_Server.Host = Server '"smtp.gmail.com"
                            e_mail = New MailMessage()
                            'e_mail.From = New MailAddress("nittslam@gmail.com")
                            e_mail.From = New MailAddress(senderDB)
                            'e_mail.To.Add("nittslam@gmail.com")
                            Try
                                e_mail.To.Add(ds1.Tables(0).Rows(0).Item("Email").ToString.Trim)
                                If CCMail.Trim <> "" Then
                                    e_mail.CC.Add(CCMail)
                                End If
                                e_mail.IsBodyHtml = True
                                e_mail.IsBodyHtml = True

                                e_mail.Subject = "Daily Absenteeism"
                                e_mail.Body = "Please find the attached Daily Absenteeism Report for date " & Now.ToString("dd/MM/yyyy")
                                attachment = New System.Net.Mail.Attachment(My.Application.Info.DirectoryPath & "\Reports\" & Common.reportName)
                                e_mail.Attachments.Add(attachment)
                                Smtp_Server.Send(e_mail)
                            Catch ex As Exception
                            End Try

                        End If
                        If reportType(i).Trim = "Daily Present" Then
                            Common.reportName = "DailyPresent" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
                            daily.SpotXl_PresentGrid("")

                            Dim Smtp_Server As New SmtpClient
                            Dim e_mail As New MailMessage()
                            Dim attachment As System.Net.Mail.Attachment
                            Smtp_Server.UseDefaultCredentials = False
                            'Smtp_Server.Credentials = New Net.NetworkCredential(("nittslam@gmail.com"), ("umawidnitinbs4"))
                            Smtp_Server.Credentials = New Net.NetworkCredential(senderDB, password)
                            Smtp_Server.Port = Convert.ToInt16(Port) '587
                            Smtp_Server.EnableSsl = IsEnable 'True
                            Smtp_Server.Host = Server '"smtp.gmail.com"
                            e_mail = New MailMessage()
                            'e_mail.From = New MailAddress("nittslam@gmail.com")
                            e_mail.From = New MailAddress(senderDB)
                            'e_mail.To.Add("nittslam@gmail.com")

                            Try
                                e_mail.To.Add(ds1.Tables(0).Rows(0).Item("Email").ToString.Trim)
                                If CCMail.Trim <> "" Then
                                    e_mail.CC.Add(CCMail)
                                End If
                                e_mail.IsBodyHtml = True
                                e_mail.IsBodyHtml = True

                                e_mail.Subject = "Daily Present"
                                e_mail.Body = "Please find the attached Daily Present Report for date " & Now.ToString("dd/MM/yyyy")
                                attachment = New System.Net.Mail.Attachment(My.Application.Info.DirectoryPath & "\Reports\" & Common.reportName)
                                e_mail.Attachments.Add(attachment)
                                Smtp_Server.Send(e_mail)
                            Catch ex As Exception
                            End Try

                        End If
                        If reportType(i).Trim = "Daily Performance" Then
                            Common.reportName = "DailyPerformance" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
                            daily.DailyXl_PerformanceGrid("")

                            Dim Smtp_Server As New SmtpClient
                            Dim e_mail As New MailMessage()
                            Dim attachment As System.Net.Mail.Attachment
                            Smtp_Server.UseDefaultCredentials = False
                            'Smtp_Server.Credentials = New Net.NetworkCredential(("nittslam@gmail.com"), ("umawidnitinbs4"))
                            Smtp_Server.Credentials = New Net.NetworkCredential(senderDB, password)
                            Smtp_Server.Port = Convert.ToInt16(Port) '587
                            Smtp_Server.EnableSsl = IsEnable 'True
                            Smtp_Server.Host = Server '"smtp.gmail.com"
                            e_mail = New MailMessage()
                            'e_mail.From = New MailAddress("nittslam@gmail.com")
                            e_mail.From = New MailAddress(senderDB)
                            'e_mail.To.Add("nittslam@gmail.com")

                            Try
                                e_mail.To.Add(ds1.Tables(0).Rows(0).Item("Email").ToString.Trim)
                                If CCMail.Trim <> "" Then
                                    e_mail.CC.Add(CCMail)
                                End If
                                e_mail.IsBodyHtml = True
                                e_mail.IsBodyHtml = True

                                e_mail.Subject = "Daily Performance"
                                e_mail.Body = "Please find the attached Daily Performance Report for date " & Now.AddDays(-1).ToString("dd/MM/yyyy")
                                attachment = New System.Net.Mail.Attachment(My.Application.Info.DirectoryPath & "\Reports\" & Common.reportName)
                                e_mail.Attachments.Add(attachment)
                                Smtp_Server.Send(e_mail)
                            Catch ex As Exception
                            End Try
                        End If
                    Next
                    Common.whereClauseEmail = ""
                Next

                Dim msg As New Message("Mail Sent", "Daily report has been sent to Location mail ID")
                AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
            End If
        End If
        Common.ReportMail = False
        TimerReportEmail.Enabled = True
    End Sub
    Private Sub TimerDataProcess_Tick(sender As System.Object, e As System.EventArgs) Handles TimerDataProcess.Tick
        TimerDataProcess.Enabled = False
        Dim conSQL As SqlConnection
        Dim con1Access As OleDbConnection
        If Common.servername = "Access" Then
            con1Access = New OleDbConnection(Common.ConnectionString)
        Else
            conSQL = New SqlConnection(Common.ConnectionString)
        End If

        'Common.getSerialNo()
        'Common.loadEmp()
        'Common.loadCompany()
        'Common.loadLocation()
        'Common.loadDevice()

        If Common.LogDownLoadCounter = 1 Then
            XtraAutoDownloadLogs.ShowDialog()
        End If
        Dim comclass As Common = New Common
        Dim msg As New Message("Data Process", "Data Process is running in background")


        If Common.AutoProcess = "Y" Then
            AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim sSql As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId"
            '"select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Access)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, conSQL)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    LabelControlCount.Text = "Processing " & i + 1 & " of " & ds.Tables(0).Rows.Count
                    Application.DoEvents()
                    Dim pay As String = ds.Tables(0).Rows(i).Item("PAYCODE").ToString
                    If ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                        comclass.Process_AllRTC(Now.AddDays(-2), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString, ds.Tables(0).Rows(i).Item("PAYCODE").ToString, ds.Tables(0).Rows(i).Item("Id"))
                    Else
                        comclass.Process_AllnonRTC(Now.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString, ds.Tables(0).Rows(i).Item("PAYCODE").ToString, ds.Tables(0).Rows(i).Item("Id"))
                        If Common.EmpGrpArr(ds.Tables(0).Rows(i).Item("Id")).SHIFTTYPE = "M" Then
                            comclass.Process_AllnonRTCMulti(Now.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString, ds.Tables(0).Rows(i).Item("PAYCODE").ToString, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                    End If
                Next
                LabelControlCount.Text = ""
                Application.DoEvents()
            End If
            If AlertControl1.AlertFormList.Count > 0 Then
                AlertControl1.AlertFormList(0).Close()
            End If
            msg = New Message("Data Process", "Data Process finished")
            AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
        End If
        LabelControlCount.Text = ""
        Application.DoEvents()
        If realTIme = "Y" Then
            XtraRealTimePunches.Show()
        End If
        LabelControlStatus.Text = ""
        Application.DoEvents()

        If Common.online = "Y" Then
            comclass.Convertdat_Cloud()
            LabelControlStatus.Text = ""
            Application.DoEvents()
        End If

        'AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
        TimerSMS.Enabled = True
        If Common.online = "Y" Then
            If Common.TimerDur <> "" And Common.TimerDur <> "0" Then
                TimerCloud.Interval = Convert.ToInt32(Common.TimerDur) * 60 * 1000
                TimerCloud.Enabled = True
            End If
        End If

        If Common.AutoDwnDur <> "0" Then
            TimerAutoDownload.Interval = Common.AutoDwnDur * 60000
            TimerAutoDownload.Enabled = True
        End If
        If Common.IsIOCL = "Y" And Common.IOCLInterval > 0 Then
            TimerIOCL.Interval = Common.IOCLInterval * 60000
            TimerIOCL.Enabled = True
        End If
    End Sub
    Private Sub TimerSMS_Tick(sender As System.Object, e As System.EventArgs) Handles TimerSMS.Tick
        TimerSMS.Enabled = False
        Dim cn As Common = New Common()
        If Common.g_SMSApplicable = "" Then
            Common.Load_SMS_Policy()
        End If
        If Common.g_SMSApplicable = "Y" Then
            If Common.g_isAbsentSMS = "Y" Then
                If Common.g_AbsMsgFor = "C" Then
                    cn.SendAbsentMS_CurrentDt()
                Else
                    cn.SendAbsentSMS()
                End If
            End If
        End If
        TimerSMS.Enabled = True
    End Sub
    Private Sub TimerCloud_Tick(sender As System.Object, e As System.EventArgs) Handles TimerCloud.Tick
        TimerCloud.Enabled = False
        If Common.online = "Y" Then
            TimerCloud.Interval = Convert.ToInt32(Common.TimerDur) * 60 * 1000
            'MsgBox(TimerCloud.Interval)
            Dim comclass As Common = New Common
            comclass.Convertdat_Cloud()
            'comclass.Convertdat_WDMS()
            LabelControlStatus.Text = ""
            Application.DoEvents()
        End If
        TimerCloud.Enabled = True
    End Sub
    Private Sub TimerAutoDownload_Tick(sender As System.Object, e As System.EventArgs) Handles TimerAutoDownload.Tick
        Common.LogDownLoadCounter = 1
        XtraAutoDownloadLogs.Show()
    End Sub
    Private Sub TimerAutpBK_Tick(sender As System.Object, e As System.EventArgs) Handles TimerAutpBK.Tick
        TimerAutpBK.Enabled = False
        If Common.IsAutoBackUp = True Then
            If Now.ToString("yyyy-MM-dd HH:mm") = Common.NextAutoBackUpDateTime Then
                Dim startdate As DateTime = Now.AddDays(-Common.AutoBackUpForDays)
                Dim enddate As DateTime = Now
                Dim msg As New Message("Data BackUp", "Data BackUp is running in background")
                AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
                Common.backUpTxtDat(startdate, enddate, Common.BackUpType.ToString.Trim, Common.BackUpPath, Common.DeleteAfterAutoBackUp)

                Dim sSql As String = "update BackUpData set LastBackUpDate='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                Dim cmd As SqlCommand
                Dim cmd1 As OleDbCommand
                Try
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                Catch ex As Exception
                    Me.Cursor = Cursors.Default
                End Try

                If Common.AccessAutoBackUpPath <> "" Then
                    Dim sourceFile As String = My.Application.Info.DirectoryPath & "\TimeWatch.mdb"
                    Dim destibation As String = Common.AccessAutoBackUpPath & "\TimeWatch.mdb"
                    My.Computer.FileSystem.CopyFile(sourceFile, destibation, Microsoft.VisualBasic.FileIO.UIOption.AllDialogs, Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)
                End If
                msg = New Message("Data BackUp", "Data BackUp is Successful")
                AlertControl1.Show(Me, msg.Caption, msg.Text, "", msg.Image, msg)
            End If
        End If
        TimerAutpBK.Enabled = True
    End Sub
    Private Sub BarButtonItemLogs_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemLogs.ItemClick
        XtraCommunicationForm.ShowDialog()
    End Sub
    Private Sub BarButtonItemUser_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemUser.ItemClick
        XtraFringerDataMgmt.ShowDialog()
    End Sub
    Private Sub BarButtonItemReal_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemReal.ItemClick
        XtraRealTimePunches.Show()
    End Sub
    Private Sub BarButtonItemComp_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemComp.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Company"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraCompany
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemLocation_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemLocation.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Location"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraBranch
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemDept_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemDept.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Department"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraDeparment
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemShift_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemShift.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Shift"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraShift
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemGrade_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemGrade.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Grade"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraGrade
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemEmpGrp_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemEmpGrp.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Employee Group"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraEmployeeGroup
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemBank_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemBank.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Bank"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraBankMaster
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemDisp_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemDisp.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Dispensary"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraDespansary
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemCat_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCat.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Catrgory"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraCategory
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemEmp_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemEmp.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Employee"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraEmployee
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemManual_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemManual.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Manual Entry"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraManualEntry
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemDataMaint_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemDataMaint.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Data Maintenance"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraDataMaintenance
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemHoliday_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemHoliday.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Holiday Entry"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraHolidayEntry
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemLMaster_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemLMaster.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Leave Master"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraLeaveMaster
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemlAcc_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemlAcc.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Leave Accrual"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraLeaveAccrual
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemLApp_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemLApp.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Leave Application"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraLeaveApplication
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemCSettings_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCSettings.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Common Setting"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraTimeOfficePolicyNew
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemDPro_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemDPro.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Data Process"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraDataProcess
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemDB_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemDB.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Database Setting"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraDBSetting
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemSMS_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemSMS.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " SMS Setting"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraSMSSetting
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemBSMS_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemBSMS.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Bulk SMS"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraBulkSMS
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemEmail_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemEmail.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Email Setting"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraReportEmailSetting
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemUManage_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemUManage.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " User Manage"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraUserMgmt
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemBackUp_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemBackUp.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " BackUp Setting"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraDBBackUpSetting
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemPDB_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemPDB.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Parallel Database"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraParallelSetting
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemDMgmt_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemDMgmt.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Device Management"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraDevice
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemDReports_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemDReports.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Daily Reports"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraReportsDaily
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemMReports_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemMReports.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Monthly Reports"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraReportsMonthly
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemLReports_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemLReports.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Leave Reports"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraLeaveReport
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemCreports_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCreports.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Customised Report"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraCustomisedReportEdit
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemPEmp_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemPEmp.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Employee Setup"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraEmployeePayroll
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemPSetup_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemPSetup.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Payroll Setup"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraPayrollSetup
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemFormula_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemFormula.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Formula Setup"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraFormulaSetup
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemPayPro_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemPayPro.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Payroll Processing"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraPayrollProcess
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemPerMain_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemPerMain.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Performance Maintain"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraPayMaintenance
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemLoan_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemLoan.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Loan/Advance Setup"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraLoanAdvance
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub BarButtonItemPayReports_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemPayReports.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Reports"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraReportsPay
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemLeaveIncr_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemLeaveIncr.ItemClick
        XtraLeaveIncrement.ShowDialog()
    End Sub

    Private Sub BarSubItemPayRoll_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarSubItemPayRoll.ItemClick
        If Common.IsFullPayroll = True Then
            SidePanelTitle.Visible = False
            LabelTitle.Text = " "
            SidePanelMainFormShow.Controls.Clear()
            Dim form As UserControl = New XtraFullPayrollMenuMaster
            'form.TopLevel = False
            'form.FormBorderStyle = FormBorderStyle.None
            form.Dock = DockStyle.Fill
            SidePanelMainFormShow.Controls.Add(form)
            form.Show()
        End If
    End Sub

    Private Sub BarButtonItemHome_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemHome.ItemClick
        SidePanelTitle.Visible = False  'for XtraHome
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraHomeNew 'XtraHome
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItem2_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem2.ItemClick
        XtraCompanyInfo.ShowDialog()
    End Sub

    Private Sub BarButtonItem6_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        XtraAppInfo.ShowDialog()
    End Sub

    Private Sub BarButtonItemSlab_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemSlab.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Meal Time Slab"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraCanteenTimeSlab
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemMealMenu_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemMealMenu.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Meal Menu"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraCanteenMealMenu
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemCanReports_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCanReports.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Canteen Reports"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraCanteenReport
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemVEntry_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemVEntry.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Visitor Entry"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraVisitorEntry
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub BarButtonItemVHistory_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemVHistory.ItemClick
        SidePanelTitle.Visible = True
        LabelTitle.Text = " Visitor History"
        SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraVisitorGrid
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub

    Private Sub TimerIOCL_Tick(sender As System.Object, e As System.EventArgs) Handles TimerIOCL.Tick
        TimerIOCL.Enabled = False
        If Common.IsIOCL = "Y" And Common.IOCLInterval > 0 Then
            Dim conSQL As SqlConnection
            Dim con1Access As OleDbConnection
            Dim cmd1 As OleDbCommand
            Dim cmd As SqlCommand
            If Common.servername = "Access" Then
                con1Access = New OleDbConnection(Common.ConnectionString)
            Else
                conSQL = New SqlConnection(Common.ConnectionString)
            End If
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim sSql As String = "SELECT m.*, c.companycode, c.companyname, mc.branch from MachineRawPunch m, TblEmployee e, tblCompany c, tblMachine mc " & _
                                " where(m.PAYCODE = e.PAYCODE And e.COMPANYCODE = c.COMPANYCODE And m.MC_NO = mc.ID_NO)" & _
                                " and (m.IsIOCLCheck <> 'Y' or IsIOCLCheck IS NULL) order by OFFICEPUNCH"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Access)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, conSQL)
                adap.Fill(ds)
            End If

            If ds.Tables(0).Rows.Count > 0 Then
                Dim IOCLFinal() As IOCLDATA
                Dim IOCL As IOCLDATA = New IOCLDATA()
                Dim IOCLList As New List(Of IOCLDATA)()
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    IOCL = New IOCLDATA()
                    IOCL.RdrNum = ds.Tables(0).Rows(i).Item("MC_NO").ToString.Trim
                    IOCL.EMP = ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim
                    If ds.Tables(0).Rows(i).Item("INOUT").ToString.Trim = "I" Then
                        IOCL.IO = 1
                    ElseIf ds.Tables(0).Rows(i).Item("INOUT").ToString.Trim = "O" Then
                        IOCL.IO = 2
                    End If
                    IOCL.Site = ds.Tables(0).Rows(i).Item("companycode").ToString.Trim
                    IOCL.RdrName = ds.Tables(0).Rows(i).Item("branch").ToString.Trim
                    IOCL.Date = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss")
                    IOCLList.Add(IOCL)
                Next
                IOCLFinal = IOCLList.ToArray
                If Common.JSONCallIOCLStuct(IOCLFinal) Then
                    If Common.servername = "Access" Then
                        If con1Access.State <> ConnectionState.Open Then
                            con1Access.Open()
                        End If
                    Else
                        If conSQL.State <> ConnectionState.Open Then
                            conSQL.Open()
                        End If
                    End If
                    For i As Integer = 0 To IOCLFinal.Length - 1
                        sSql = "update MachineRawPunch SET isioclcheck = 'Y' where Paycode ='" & IOCLFinal(i).EMP & "' and OFFICEPUNCH='" & IOCLFinal(i).Date & "'"
                        If Common.servername = "Access" Then
                            cmd1 = New OleDbCommand(sSql, con1Access)
                            cmd1.ExecuteNonQuery()
                        Else
                            cmd = New SqlCommand(sSql, conSQL)
                            cmd.ExecuteNonQuery()
                        End If
                    Next
                    If Common.servername = "Access" Then
                        If con1Access.State <> ConnectionState.Closed Then
                            con1Access.Close()
                        End If
                    Else
                        If conSQL.State <> ConnectionState.Closed Then
                            conSQL.Close()
                        End If
                    End If
                End If
            End If
        End If
        TimerIOCL.Enabled = True
    End Sub
End Class

