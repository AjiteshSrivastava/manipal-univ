﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraTimeZoneZK
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridLevelNode2 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode3 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblMachineTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        Me.TblMachine1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        Me.TblEmployee1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblEmployeeTableAdapter = New iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLOCATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeviceType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcommkey = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditTZ1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditTZ2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditTZ3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.btnSaveTZ = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnGetTZ = New DevExpress.XtraEditors.SimpleButton()
        Me.PopupContainerControlEmp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlEmp = New DevExpress.XtraGrid.GridControl()
        Me.GridViewEmp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPRESENTCARDNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditEmp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchGate4 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleSwitchGate1 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchGate2 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleSwitchGate3 = New DevExpress.XtraEditors.ToggleSwitch()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditTZ1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditTZ2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditTZ3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlEmp.SuspendLayout()
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.ToggleSwitchGate4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchGate1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchGate2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchGate3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.GridControl3)
        Me.GroupControl4.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl4.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(884, 263)
        Me.GroupControl4.TabIndex = 56
        Me.GroupControl4.Text = "Time Zones"
        '
        'GridControl3
        '
        Me.GridControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl3.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl3.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl3.EmbeddedNavigator.Buttons.Remove.Visible = False
        GridLevelNode2.RelationName = "Level1"
        Me.GridControl3.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode2})
        Me.GridControl3.Location = New System.Drawing.Point(2, 23)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(880, 238)
        Me.GridControl3.TabIndex = 1
        Me.GridControl3.UseEmbeddedNavigator = True
        Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.GridControl3
        Me.GridView3.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView3.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView3.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridView3.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView3.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView3.OptionsView.ColumnAutoWidth = False
        Me.GridView3.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblMachineTableAdapter
        '
        Me.TblMachineTableAdapter.ClearBeforeFill = True
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.GridControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 267)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(464, 220)
        Me.GroupControl1.TabIndex = 57
        Me.GroupControl1.Text = "Device List"
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblMachineBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        GridLevelNode3.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode3})
        Me.GridControl1.Location = New System.Drawing.Point(2, 23)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(460, 195)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID_NO, Me.colLOCATION, Me.colbranch, Me.colDeviceType, Me.colA_R, Me.colcommkey})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colID_NO, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colID_NO
        '
        Me.colID_NO.Caption = "Controller Id"
        Me.colID_NO.FieldName = "ID_NO"
        Me.colID_NO.Name = "colID_NO"
        Me.colID_NO.Visible = True
        Me.colID_NO.VisibleIndex = 1
        Me.colID_NO.Width = 87
        '
        'colLOCATION
        '
        Me.colLOCATION.Caption = "Device IP"
        Me.colLOCATION.FieldName = "LOCATION"
        Me.colLOCATION.Name = "colLOCATION"
        Me.colLOCATION.Visible = True
        Me.colLOCATION.VisibleIndex = 2
        Me.colLOCATION.Width = 120
        '
        'colbranch
        '
        Me.colbranch.Caption = "Location"
        Me.colbranch.FieldName = "branch"
        Me.colbranch.Name = "colbranch"
        Me.colbranch.Visible = True
        Me.colbranch.VisibleIndex = 3
        Me.colbranch.Width = 70
        '
        'colDeviceType
        '
        Me.colDeviceType.Caption = "Device Type"
        Me.colDeviceType.FieldName = "DeviceType"
        Me.colDeviceType.Name = "colDeviceType"
        Me.colDeviceType.Visible = True
        Me.colDeviceType.VisibleIndex = 4
        Me.colDeviceType.Width = 72
        '
        'colA_R
        '
        Me.colA_R.FieldName = "A_R"
        Me.colA_R.Name = "colA_R"
        '
        'colcommkey
        '
        Me.colcommkey.Caption = "Comm Key"
        Me.colcommkey.FieldName = "commkey"
        Me.colcommkey.Name = "colcommkey"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(20, 79)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl1.TabIndex = 59
        Me.LabelControl1.Text = "Time Zone ID 1"
        '
        'TextEditTZ1
        '
        Me.TextEditTZ1.Location = New System.Drawing.Point(111, 76)
        Me.TextEditTZ1.Name = "TextEditTZ1"
        Me.TextEditTZ1.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditTZ1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditTZ1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditTZ1.Properties.MaxLength = 3
        Me.TextEditTZ1.Size = New System.Drawing.Size(53, 20)
        Me.TextEditTZ1.TabIndex = 60
        '
        'TextEditTZ2
        '
        Me.TextEditTZ2.Location = New System.Drawing.Point(111, 102)
        Me.TextEditTZ2.Name = "TextEditTZ2"
        Me.TextEditTZ2.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditTZ2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditTZ2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditTZ2.Properties.MaxLength = 3
        Me.TextEditTZ2.Size = New System.Drawing.Size(53, 20)
        Me.TextEditTZ2.TabIndex = 62
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(20, 105)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl2.TabIndex = 61
        Me.LabelControl2.Text = "Time Zone ID 2"
        '
        'TextEditTZ3
        '
        Me.TextEditTZ3.Location = New System.Drawing.Point(111, 128)
        Me.TextEditTZ3.Name = "TextEditTZ3"
        Me.TextEditTZ3.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditTZ3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditTZ3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditTZ3.Properties.MaxLength = 3
        Me.TextEditTZ3.Size = New System.Drawing.Size(53, 20)
        Me.TextEditTZ3.TabIndex = 64
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(20, 131)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl3.TabIndex = 63
        Me.LabelControl3.Text = "Time Zone ID 3"
        '
        'btnSaveTZ
        '
        Me.btnSaveTZ.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnSaveTZ.Appearance.Options.UseFont = True
        Me.btnSaveTZ.Location = New System.Drawing.Point(680, 493)
        Me.btnSaveTZ.Name = "btnSaveTZ"
        Me.btnSaveTZ.Size = New System.Drawing.Size(104, 23)
        Me.btnSaveTZ.TabIndex = 66
        Me.btnSaveTZ.Text = "Set TimeZone"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(790, 493)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(68, 23)
        Me.SimpleButton1.TabIndex = 65
        Me.SimpleButton1.Text = "Close"
        '
        'btnGetTZ
        '
        Me.btnGetTZ.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnGetTZ.Appearance.Options.UseFont = True
        Me.btnGetTZ.Location = New System.Drawing.Point(482, 269)
        Me.btnGetTZ.Name = "btnGetTZ"
        Me.btnGetTZ.Size = New System.Drawing.Size(104, 23)
        Me.btnGetTZ.TabIndex = 67
        Me.btnGetTZ.Text = "Get TimeZone"
        Me.btnGetTZ.Visible = False
        '
        'PopupContainerControlEmp
        '
        Me.PopupContainerControlEmp.Controls.Add(Me.GridControlEmp)
        Me.PopupContainerControlEmp.Location = New System.Drawing.Point(2, 493)
        Me.PopupContainerControlEmp.Name = "PopupContainerControlEmp"
        Me.PopupContainerControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlEmp.TabIndex = 68
        '
        'GridControlEmp
        '
        Me.GridControlEmp.DataSource = Me.TblEmployeeBindingSource
        Me.GridControlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlEmp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlEmp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlEmp.MainView = Me.GridViewEmp
        Me.GridControlEmp.Name = "GridControlEmp"
        Me.GridControlEmp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit1})
        Me.GridControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlEmp.TabIndex = 6
        Me.GridControlEmp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewEmp})
        '
        'GridViewEmp
        '
        Me.GridViewEmp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPAYCODE, Me.colPRESENTCARDNO, Me.colEMPNAME})
        Me.GridViewEmp.GridControl = Me.GridControlEmp
        Me.GridViewEmp.Name = "GridViewEmp"
        Me.GridViewEmp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.Editable = False
        Me.GridViewEmp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewEmp.OptionsSelection.MultiSelect = True
        Me.GridViewEmp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewEmp.OptionsView.ColumnAutoWidth = False
        '
        'colPAYCODE
        '
        Me.colPAYCODE.Caption = "Paycode"
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        Me.colPAYCODE.Visible = True
        Me.colPAYCODE.VisibleIndex = 1
        '
        'colPRESENTCARDNO
        '
        Me.colPRESENTCARDNO.Caption = "Present Card No"
        Me.colPRESENTCARDNO.FieldName = "PRESENTCARDNO"
        Me.colPRESENTCARDNO.Name = "colPRESENTCARDNO"
        Me.colPRESENTCARDNO.Visible = True
        Me.colPRESENTCARDNO.VisibleIndex = 2
        Me.colPRESENTCARDNO.Width = 125
        '
        'colEMPNAME
        '
        Me.colEMPNAME.Caption = "Name"
        Me.colEMPNAME.FieldName = "EMPNAME"
        Me.colEMPNAME.Name = "colEMPNAME"
        Me.colEMPNAME.Visible = True
        Me.colEMPNAME.VisibleIndex = 3
        Me.colEMPNAME.Width = 125
        '
        'RepositoryItemTimeEdit1
        '
        Me.RepositoryItemTimeEdit1.AutoHeight = False
        Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(14, 32)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl4.TabIndex = 69
        Me.LabelControl4.Text = "Select Employee"
        '
        'PopupContainerEditEmp
        '
        Me.PopupContainerEditEmp.Location = New System.Drawing.Point(111, 29)
        Me.PopupContainerEditEmp.Name = "PopupContainerEditEmp"
        Me.PopupContainerEditEmp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditEmp.Properties.PopupControl = Me.PopupContainerControlEmp
        Me.PopupContainerEditEmp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditEmp.TabIndex = 70
        Me.PopupContainerEditEmp.ToolTip = "Leave blank if want for all Employees"
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.LabelControl9)
        Me.GroupControl3.Controls.Add(Me.LabelControl6)
        Me.GroupControl3.Controls.Add(Me.ToggleSwitchGate4)
        Me.GroupControl3.Controls.Add(Me.ToggleSwitchGate1)
        Me.GroupControl3.Controls.Add(Me.LabelControl5)
        Me.GroupControl3.Controls.Add(Me.PopupContainerEditEmp)
        Me.GroupControl3.Controls.Add(Me.LabelControl1)
        Me.GroupControl3.Controls.Add(Me.LabelControl4)
        Me.GroupControl3.Controls.Add(Me.TextEditTZ1)
        Me.GroupControl3.Controls.Add(Me.LabelControl2)
        Me.GroupControl3.Controls.Add(Me.TextEditTZ2)
        Me.GroupControl3.Controls.Add(Me.LabelControl3)
        Me.GroupControl3.Controls.Add(Me.TextEditTZ3)
        Me.GroupControl3.Location = New System.Drawing.Point(480, 298)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(378, 187)
        Me.GroupControl3.TabIndex = 72
        Me.GroupControl3.Text = "Assign TimeZone To Employee"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(186, 105)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl9.TabIndex = 79
        Me.LabelControl9.Text = "Exit"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(186, 78)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(29, 14)
        Me.LabelControl6.TabIndex = 76
        Me.LabelControl6.Text = "Entry"
        '
        'ToggleSwitchGate4
        '
        Me.ToggleSwitchGate4.Location = New System.Drawing.Point(241, 104)
        Me.ToggleSwitchGate4.Name = "ToggleSwitchGate4"
        Me.ToggleSwitchGate4.Properties.OffText = "Off"
        Me.ToggleSwitchGate4.Properties.OnText = "On"
        Me.ToggleSwitchGate4.Size = New System.Drawing.Size(95, 24)
        Me.ToggleSwitchGate4.TabIndex = 73
        '
        'ToggleSwitchGate1
        '
        Me.ToggleSwitchGate1.Location = New System.Drawing.Point(241, 74)
        Me.ToggleSwitchGate1.Name = "ToggleSwitchGate1"
        Me.ToggleSwitchGate1.Properties.OffText = "Off"
        Me.ToggleSwitchGate1.Properties.OnText = "On"
        Me.ToggleSwitchGate1.Size = New System.Drawing.Size(95, 24)
        Me.ToggleSwitchGate1.TabIndex = 72
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(116, 57)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(35, 14)
        Me.LabelControl5.TabIndex = 71
        Me.LabelControl5.Text = "(1-50)"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(519, 502)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(33, 14)
        Me.LabelControl8.TabIndex = 78
        Me.LabelControl8.Text = "Gate3"
        Me.LabelControl8.Visible = False
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(339, 498)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(33, 14)
        Me.LabelControl7.TabIndex = 77
        Me.LabelControl7.Text = "Gate2"
        Me.LabelControl7.Visible = False
        '
        'ToggleSwitchGate2
        '
        Me.ToggleSwitchGate2.Location = New System.Drawing.Point(381, 494)
        Me.ToggleSwitchGate2.Name = "ToggleSwitchGate2"
        Me.ToggleSwitchGate2.Properties.OffText = "Off"
        Me.ToggleSwitchGate2.Properties.OnText = "On"
        Me.ToggleSwitchGate2.Size = New System.Drawing.Size(95, 24)
        Me.ToggleSwitchGate2.TabIndex = 75
        Me.ToggleSwitchGate2.Visible = False
        '
        'ToggleSwitchGate3
        '
        Me.ToggleSwitchGate3.Location = New System.Drawing.Point(536, 487)
        Me.ToggleSwitchGate3.Name = "ToggleSwitchGate3"
        Me.ToggleSwitchGate3.Properties.OffText = "Off"
        Me.ToggleSwitchGate3.Properties.OnText = "On"
        Me.ToggleSwitchGate3.Size = New System.Drawing.Size(95, 24)
        Me.ToggleSwitchGate3.TabIndex = 74
        Me.ToggleSwitchGate3.Visible = False
        '
        'XtraTimeZoneZK
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(884, 523)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.ToggleSwitchGate3)
        Me.Controls.Add(Me.PopupContainerControlEmp)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.ToggleSwitchGate2)
        Me.Controls.Add(Me.btnGetTZ)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.btnSaveTZ)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraTimeZoneZK"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditTZ1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditTZ2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditTZ3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlEmp.ResumeLayout(False)
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.ToggleSwitchGate4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchGate1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchGate2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchGate3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblMachineTableAdapter As iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents TblMachine1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents TblEmployee1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployeeTableAdapter As iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLOCATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeviceType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colA_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditTZ1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditTZ2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditTZ3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnSaveTZ As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnGetTZ As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PopupContainerControlEmp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlEmp As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewEmp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditEmp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents colPRESENTCARDNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents colcommkey As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ToggleSwitchGate2 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleSwitchGate3 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleSwitchGate4 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleSwitchGate1 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
End Class
